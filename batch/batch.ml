(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: batch.ml
 **       entry point of the automatic regression tester
 ** Xavier Rival, 2011/09/29 *)
open Config_sig
open Data_structures
open Flags
open Lib
open Rt_sig


module Log =
  Logger.Make(struct let section = "batch___" and level = Log_level.DEBUG end)


(** Flags *)
(* over-riding analysis fields *)
let flag_silent:        bool option ref = ref None
let flag_very_silent:   bool option ref = ref None
(* de-activating timing (for easier text log comparisons) *)
let no_timing: bool ref = ref false
(* dump log file to stdout in addition to the test's log file *)
let stdout_dump: bool ref = ref false

let dot_all: bool option ref = ref None

(** Utilities for printing *)
let magenta_color = Color.(to_string Bold ^ to_string Magenta)
let reset_color = Color.(to_string Reset)

(** Parsing *)
let config_parser =
  Lib.read_from_file "batch file" Config_parser.file Config_lexer.token

(** Auxilliary functions *)
let default_test =
  { (** Options related to the inputs of the analysis *)
    sp_src_file   = "";
    sp_header     = "memcad.h";
    sp_idirs      = "";
    sp_old_parser = false;
    sp_clangmacros = false;
    (** Options to be passed to analysis *)
    sp_malloc_non0 = false;
    sp_ind_file   = "";
    sp_auto_ind   = false;
    sp_no_prev_fields = false;
    sp_main_fun   = !mainfun;
    sp_rec_calls  = false;
    sp_silent     = false;
    sp_very_silent= false;
    sp_verbose    = !test_verbose;
    sp_pp_add     = [];
    sp_pp_rem     = [];
    sp_dot_all    = false;
    sp_dump_ops   = !flag_dump_ops;
    sp_timing     = [];
    (** Abstract domain: symbolic variables constraints *)
    sp_numdom     = !nd_selector;
    sp_submem     = !enable_submem;
    sp_subis      = StringSet.empty;
    sp_dynenv     = true;
    sp_red_mode   = !reduction_mode_selector;
    sp_red_ecells = !flag_reduce_quick_eval;
    sp_setdom     = !sd_selector;
    sp_seqdom     = !enable_seqdom;
    sp_colv_info  = !flag_colv_info;
    sp_colv_len   = !enable_colv_len;
    sp_colv_bnd   = !enable_colv_bnd;
    sp_array      = !enable_array_domain;
    sp_eq_pack    = !enable_eq_pack;
    (** Abstract domain and inductive definitions *)
    sp_indpars    = !flag_indpars_analysis;
    sp_domstruct  = !shapedom_struct;
    (** Iteration strategy, widening *)
    sp_disj       = !disj_selector;
    sp_thrwide    = true;
    sp_thrs       = IntSet.empty;
    sp_unary_abs  = !do_unary_abstraction;
    sp_unrolls      = 0;
    sp_type_unfolds = 1;
    sp_dirweak    = !dweak_iters;
    sp_join_iters = !join_iters;
    sp_fast_iir   = true;
    sp_part_lfps  = !part_through_lfps;
    sp_sel_widen  = !sel_widen;
    (** Regression testing elements *)
    sp_excluded   = false;
    sp_category   = [ ];
    sp_issue      = "";
    sp_assert_tot = 0;
    sp_assert_ok  = 0;
    sp_time_out   = 5;
    (** Spec files *)
    sp_specfile   = None;
    (** Temporary, undocumented *)
    sp_expjinst   = false;
    sp_goal_sel   = None;
    sp_goal_rem   = [ ] }

(** Revision letters conversion to/from integers *)
let rev_to_int: char option -> int = function
  | None -> -1
  | Some c -> int_of_char c - int_of_char 'a'
let rev_from_int (i: int): char option =
  if i < 0 then None
  else if i < 26 then Some (char_of_int (int_of_char 'a' + i))
  else Log.fatal_exn "invalid revision number"
let rev_fpr (fmt: form): char option -> unit = function
  | None -> ()
  | Some c -> F.fprintf fmt "%c" c

(** Traduction of a parsed config file *)
let translate_tests (pconf: pconfig_file): tests =
  let extract_list (ctx: string): string list -> string = function
    | [ ] -> Log.fatal_exn "error %s: [ ]" ctx
    | [ s ] -> s
    | s :: _ -> Log.fatal_exn "error %s: %s..." ctx s in
  let aux_yesno (l: string list): bool =
    match extract_list "yes-no" l with
    | "yes" -> true
    | "no"  -> false
    | s     -> Log.fatal_exn "unbound yes-no: %s" s in
  let aux_onoff (l: string list): bool =
    match extract_list "on-off" l with
    | "on"  -> true
    | "off" -> false
    | s     -> Log.fatal_exn "unbound on-off: %s" s in
  let aux_numdom (l: string list): num_dom =
    match extract_list "domain" l with
    | "box" -> ND_box
    | "oct" -> ND_oct
    | "pol" -> ND_pol
    | s     -> Log.fatal_exn "unbound num domain %s" s in
  let aux_setdom (l: string list): set_dom =
    match extract_list "domain" l with
    | "none" -> SD_none
    | "on"
    | "lin"  -> SD_lin
    | "quicr"-> SD_quicr
    | "setr_lin" | "SETr_lin" -> SD_setr "lin"
    | "setr_bdd" | "SETr_bdd" -> SD_setr "bdd"
    | s     -> Log.fatal_exn "unbound set domain (%s)" s in
  let aux_redmod (l: string list): reduction_mode =
    match extract_list "redmod" l with
    | "manual"             -> Rm_manual
    | "disabled"           -> Rm_disabled
    | "minimal"            -> Rm_minimal
    | "on_read"            -> Rm_on_read
    | "on_read_and_unfold" -> Rm_on_read_and_unfold
    | "maximal"            -> Rm_maximal
    | s -> Log.fatal_exn "undefined reduction mod: %s" s in
  let aux_subind (l: string list): StringSet.t =
    StringSet.singleton (extract_list "subind" l) in
  let aux_category: string -> test_category = function
    | "valid"
    | "validated" -> TC_valid
    | "wait"      -> TC_wait
    | "exp"       -> TC_exp
    | s           -> TC_other s in
  let aux_categories: string list -> test_category list =
    List.map aux_category in
  let aux_field (t: test_spec) (f: field): test_spec =
    (* note: to make maintenance easier, it is better to keep the following
     *       string patters sorted in alphabetical order *)
    match f.fname, f.fval with
    | "array", F_strings s       -> { t with sp_array = aux_onoff s }
    | "asserts_proved", F_int n  -> { t with sp_assert_ok = n }
    | "asserts_total", F_int n   -> { t with sp_assert_tot = n }
    | "auto_ind", F_strings s    -> { t with sp_auto_ind = aux_onoff s }
    | "category", F_strings s    -> { t with sp_category = aux_categories s }
    | "clangmacros", F_strings s -> { t with sp_clangmacros = aux_yesno s }
    | "colv_info", F_strings s   -> { t with sp_colv_info = aux_onoff s}
    | "colv_length", F_strings s -> { t with sp_colv_len = aux_onoff s}
    | "colv_bounds", F_strings s -> { t with sp_colv_bnd = aux_onoff s}
    | "dirweak", F_int n         -> { t with sp_dirweak = n }
    | "disj", F_strings s        -> { t with sp_disj = aux_onoff s }
    | "domain", F_strings s      -> { t with sp_numdom = aux_numdom s }
    | "domstruct", F_qstring s   -> { t with sp_domstruct = s }
    | "dot_all", F_strings s     -> { t with sp_dot_all = aux_onoff s }
    | "dump_ops", F_strings s    -> { t with sp_dump_ops = aux_onoff s }
    | "dynenv", F_strings s      -> { t with sp_dynenv = aux_onoff s }
    | "eq_pack", F_strings s     -> { t with sp_eq_pack = aux_onoff s }
    | "excluded", F_strings s    -> { t with sp_excluded = aux_yesno s }
    | "expjinst", F_strings s    -> { t with sp_expjinst = aux_yesno s }
    | "fast_iir", F_strings s    -> { t with sp_fast_iir = aux_onoff s }
    | "file", F_qstring s        -> { t with sp_src_file = s }
    | "goal_rem", F_qstring s    -> { t with sp_goal_rem = s :: t.sp_goal_rem }
    | "goal_sel", F_qstring s    -> { t with sp_goal_sel = Some s }
    | "header", F_qstring s      -> { t with sp_header = s }
    | "idirs", F_qstring s       -> { t with sp_idirs = s }
    | "indfile", F_qstring s     -> { t with sp_ind_file = s }
    | "indpars", F_strings s     -> { t with sp_indpars = aux_onoff s }
    | "issue", F_qstring s       -> { t with sp_issue = s }
    | "join_iters", F_int n      -> { t with sp_join_iters = n }
    | "main_fun", F_qstring s    -> { t with sp_main_fun = s }
    | "malloc_non0", F_strings s -> { t with sp_malloc_non0 = aux_onoff s }
    | "no_prev_fields", F_strings s-> { t with sp_no_prev_fields = aux_onoff s }
    | "old_parser", F_strings s  -> { t with sp_old_parser = aux_onoff s }
    | "part_lfps", F_strings s   -> { t with sp_part_lfps = aux_onoff s }
    | "rec_calls", F_strings s   -> { t with sp_rec_calls = aux_onoff s }
    | "red_ecells", F_strings s  -> { t with sp_red_ecells = aux_onoff s }
    | "red_mode", F_strings s    -> { t with sp_red_mode = aux_redmod s }
    | "sel_widen", F_strings s   -> { t with sp_sel_widen = aux_onoff s }
    | "seqdom", F_strings s      -> { t with sp_seqdom = aux_onoff s }
    | "set_on", F_qstring s      -> { t with sp_pp_add = s :: t.sp_pp_add }
    | "set_off", F_qstring s     -> { t with sp_pp_rem = s :: t.sp_pp_rem }
    | "setdom", F_strings s      -> { t with sp_setdom = aux_setdom s }
    | "silent", F_strings s      -> { t with sp_silent = aux_onoff s }
    | "specfile", F_qstring s    -> { t with sp_specfile = Some s }
    | "submem", F_strings s      -> { t with sp_submem = aux_onoff s }
    | "submem_ind", F_strings s  -> { t with sp_subis = aux_subind s }
    | "thr_widen", F_strings s   -> { t with sp_thrwide = aux_onoff s }
    | "thr_w_add", F_int n       -> { t with sp_thrs = IntSet.add n t.sp_thrs }
    | "timeout", F_int n         -> { t with sp_time_out = n }
    | "timing", F_qstring s      -> { t with sp_timing = s :: t.sp_timing }
    | "unary_abs", F_strings s   -> { t with sp_unary_abs = aux_onoff s }
    | "unrolls", F_int n         -> { t with sp_unrolls = n }
    | "type_unfolds", F_int n    -> { t with sp_type_unfolds = n }
    | "very_silent", F_strings s -> { t with sp_very_silent = aux_onoff s }
    | _ -> Log.fatal_exn "unsupported_field: %s" f.fname in
  let aux_fields: test_spec -> fields -> test_spec =
    List.fold_left aux_field in
  let rec aux (acc: test_spec RevMap.t) (t: entry list): test_spec RevMap.t =
    let rem, acc =
      List.fold_left
        (fun (accrem, accm) (ent: entry) ->
          try
            let prev_test =
              match ent.ent_extend with
              | None -> default_test
              | Some basis -> RevMap.find basis accm in
            let trans = aux_fields prev_test ent.ent_fields in
            accrem, RevMap.add ent.ent_code trans accm
          with Not_found -> ent :: accrem, accm
        ) ([ ], acc) t in
    let lrem = List.length rem in
    if lrem = 0 then acc
    else
      if List.length t = lrem then
        begin
          List.iter
            (fun e ->
              Log.info "unresolved: (%d,...)" (fst e.ent_code)
            ) t;
          Log.fatal_exn "cycle in the definition of entries"
        end
      else aux acc rem in
  let acc = aux RevMap.empty pconf in
  (* Remove all entries that refer neither to a .c file nor to a .spec file *)
  RevMap.fold
    (fun ec ent acc ->
      if String.length ent.sp_src_file = 0
          && ent.sp_specfile = None then RevMap.remove ec acc
      else acc
    ) acc acc

(** Generation of runnable analysis command and options from test definition *)
let spec_to_options (t: test_spec) pipe_name : string list =
  let lopts = ref [ "-named-pipe"; pipe_name ] in
  let add_opt  (opt: string): unit = lopts := opt :: !lopts in
  let add_opts (opts: string list): unit = lopts := opts @ !lopts in
  (* input file *)
  if String.length t.sp_src_file != 0 then
    add_opts [ "-a" ; t.sp_src_file ];
  (* header file *)
  if t.sp_header <> "" then
    add_opts [ "-header" ; t.sp_header ];
  (* include dirs *)
  if t.sp_idirs <> "" then
    add_opts [ "-idirs" ; t.sp_idirs ];
  if t.sp_old_parser <> !use_old_parser then
    if t.sp_old_parser then
      add_opt "-old-parser"
    else add_opt "-clang-parser";
  (* inductive definitions *)
  if t.sp_ind_file <> "" then
    add_opts [ (*"-a" ::*) "-use-ind" ; t.sp_ind_file ]
  else if t.sp_auto_ind then
    add_opts [ (*"-a" ::*) "-auto-ind" ]
  else
    ( );
  if t.sp_no_prev_fields then
    add_opt "-no-prev-fields";
  (* main function *)
  if String.compare t.sp_main_fun !mainfun != 0 then
    add_opts [ "-main" ; t.sp_main_fun ];
  (* clang macros *)
  if t.sp_clangmacros then
    add_opt "-clang-macros";
  (* handling of recursive calls *)
  if t.sp_rec_calls != !rec_calls then
    add_opt "-rec-calls";
  (* malloc semantics restriction *)
  if t.sp_malloc_non0 then
    add_opt "-malloc-non0";
  (* inductive definitions analysis *)
  if t.sp_indpars != !flag_indpars_analysis then
    add_opt "-ind-analysis";
  (* whether to dump domain individual operations (e.g., set domain) *)
  if t.sp_dump_ops != !flag_dump_ops && t.sp_dump_ops then
    add_opt "-dump-ops";
  (* timing modules *)
  List.iter (fun s -> add_opts [ "-timing" ; s ]) t.sp_timing;
  (* shape domain *)
  if String.compare t.sp_domstruct !shapedom_struct != 0 then
    add_opts [ "-shape-dom" ; ("\""^t.sp_domstruct^"\"") ];
  (* numerical domain *)
  if t.sp_numdom != !nd_selector then
    add_opt (F.asprintf "-nd-%a" num_dom_fpr t.sp_numdom);
  (* sub-memory abstraction *)
  if t.sp_submem != !enable_submem && t.sp_submem then
    add_opt "-add-submem";
  (* sub-memory indutive definitions *)
  if t.sp_subis != StringSet.empty then
    StringSet.iter (fun s -> add_opts [ "-submem-ind" ; s ]) t.sp_subis;
  (* dynamic environment *)
  if t.sp_dynenv != !enable_dynenv then
    add_opt (if t.sp_dynenv then "-dynenv-yes" else "-dynenv-no");
  (* Array domain *)
  if t.sp_array != !enable_array_domain then
    add_opt (if t.sp_array then "-array-on" else "-array-off");
  (* Equation pack *)
  if t.sp_eq_pack != !enable_eq_pack then
    add_opt (if t.sp_array then "-eq-pack-on" else "-eq-pack-off");
  (* disjunctions *)
  if t.sp_disj != !disj_selector then
    add_opt (if t.sp_disj then "-disj-on" else "-disj-off");
  (* set domain *)
  if t.sp_setdom != !sd_selector then
    begin
      let o =
        match t.sp_setdom with
        | SD_none   -> "-setd-off"
        | SD_lin    -> "-setd-lin"
        | SD_quicr  -> "-setd-quicr"
        | SD_setr n -> Printf.sprintf "-setd-r-%s" n in
      add_opt o
    end;
  (* seq domain *)
  if t.sp_seqdom != !enable_seqdom && t.sp_seqdom then
    add_opt "-seqd-on";
  if t.sp_colv_info != !flag_colv_info && t.sp_colv_info then
    add_opt "-colv-info";
  if t.sp_colv_bnd != !enable_colv_bnd && t.sp_colv_bnd then
    add_opt "-colv-bnd";
  if t.sp_colv_len != !enable_colv_len && t.sp_colv_info then
    add_opt "-colv-len";
  (* threshold widening activation *)
  if t.sp_thrwide != !widen_do_thr then
    add_opt (if t.sp_thrwide then "-w-thr" else "-w-no-thr");
  (* threshold widening addition *)
  IntSet.iter
    (fun i ->
      add_opts [ "-w-add-thr" ; string_of_int i ];
    ) t.sp_thrs;
  (* unary abstraction *)
  if t.sp_unary_abs != !do_unary_abstraction then
    add_opt (if t.sp_unary_abs then "-unary-abs" else "-no-unary-abs");
  (* unrolls *)
  if t.sp_unrolls != !unrolls then
    add_opts [ "-unrolls" ; string_of_int t.sp_unrolls ];
  (* type unfolding *)
  if t.sp_type_unfolds != !type_unfolds then
    add_opts [ "-type-unfolds" ; string_of_int t.sp_type_unfolds ];
  (* directed weakening *)
  if t.sp_dirweak != !dweak_iters then
    add_opts [ "-dw-iters" ; string_of_int t.sp_dirweak ];
  (* join iters *)
  if t.sp_join_iters != !join_iters then
    add_opts [ "-j-iters" ; string_of_int t.sp_join_iters ];
  (* partitioning through loops *)
  if t.sp_part_lfps != !part_through_lfps then
    add_opt (if t.sp_part_lfps then "-part-lfps" else "-no-part-lfps");
  (* sel widen through loops *)
  if t.sp_sel_widen != !sel_widen then
    add_opt (if t.sp_sel_widen then "-sel-widen" else "-no-sel-widen");
  (* fast ind-ind rule *)
  if t.sp_fast_iir != !do_quick_ind_ind_mt then
    add_opt
      (if t.sp_fast_iir then Log.fatal_exn "fast-iir setting"
       else "-no-fast-iir");
  (* reduction mode *)
  if t.sp_red_mode != !reduction_mode_selector then
    add_opt (F.asprintf "-red-%a" red_mode_fpr t.sp_red_mode);
  (* reduction of equal cells *)
  if t.sp_red_ecells != !flag_reduce_quick_eval then
    add_opt
      (if t.sp_red_ecells then "-set-on reduce_quick_eval"
      else "-set-off reduce_quick_eval");
  (* silent mode (batch flag overrides rt.txt) *)
  let b =
    match !flag_silent with
    | None -> t.sp_silent
    | Some b -> b in
  if b then add_opt "-silent";
  (* very silent mode (batch flag overrides rt.txt) *)
  let b =
    match !flag_very_silent with
    | None -> t.sp_very_silent
    | Some b -> b in
  if b then add_opt "-very-silent";
  if !dot_all = Some true || !dot_all = None && t.sp_dot_all then
    add_opt "-dot-all";
  (* pretty-printing *)
  List.iter (fun s -> add_opts [ "-set-on" ; s ]) t.sp_pp_add;
  List.iter (fun s -> add_opts [ "-set-off" ; s ]) t.sp_pp_rem;
  (* spec files *)
  begin
    match t.sp_specfile with
    | None -> ()
    | Some s -> add_opts [ "-spec" ; s ]
  end;
  (* temporary, experimental jinst *)
  if t.sp_expjinst then
    add_opt "-tmp-expjinst";
  (* spec goal (select and remove) *)
  if Option.is_some t.sp_goal_sel then
    add_opts [ "-goal-sel" ; Option.get t.sp_goal_sel ];
  List.iter (fun s -> add_opts [ "-goal-rem" ; s ]) t.sp_goal_rem;
  (* result *)
  !lopts

(** Management and pretty-printing of the results *)
let pp_results
    (stress_test: int)          (* repeat number *)
    (validated: bool option)    (* whether it is about validated tests *)
    (tot_duration: float)       (* duration of tests *)
    (tests: test_spec RevMap.t) (* specs *)
    (m: test_result RevMap.t)   (* results *): unit =
  let cnt_to = ref 0
  and cnt_ko = ref 0
  and cnt_ok = ref 0 in
  let sep = String.make 79 '=' in
  begin
    match validated with
    | None -> F.printf "\n\n%s\nSpecific tests:\n" sep
    | Some v ->
        F.printf "\n\n%s\n%s:\n" sep
          (if v then "Already validated tests" else "Experimental tests")
  end;
  F.printf "%s\n" sep;
  RevMap.iter
    (fun (i,c) (tr: test_result) ->
      let ts = RevMap.find (i,c) tests in
      let r, msgl, msgr =
        match tr with
        | Tr_timeout -> cnt_to, "timeout", ""
        | Tr_fail    -> cnt_ko, "failed", ""
        | Tr_ok s    ->
            let msgl =
              Printf.sprintf "passed (%d/%d ok)" s.ts_assert_ok ts.sp_assert_tot
            and msgr = Printf.sprintf "%.3fs" s.ts_time in
            cnt_ok, msgl, msgr in
      incr r;
      let ii = mk_strpp RevOrd.t_fpr (i,c) in
      let ii = str_lflush 7 ii in
      let msgl = str_lflush 20 msgl in
      let msgr = str_rflush  9 msgr in
      let s_issue =
        let li = String.length ts.sp_issue in
        if li > 0 then
          Printf.sprintf "%s%s"
            (String.make (24 - String.length ts.sp_src_file) ' ')
            (String.sub ts.sp_issue 0 (min li 25))
        else "" in
      let src_file =
        if ts.sp_src_file = "" then
          match ts.sp_specfile with
          | None -> ""
          | Some s -> s
        else ts.sp_src_file in
      let s_end = F.asprintf "%s%s" src_file s_issue in
      let s_end =
        if String.length s_end > 37 then String.sub s_end 0 37
        else s_end in
      F.printf "%s:  %s %s  %s\n" ii msgl msgr s_end
    ) m;
  F.printf "%s\n" sep;
  let timeout_str =
    let count = string_of_int !cnt_to in
    if !cnt_to <> 0 then magenta_color ^ count ^ reset_color
    else count in
  let failed_str =
    let count = string_of_int !cnt_ko in
    if !cnt_ko <> 0 then magenta_color ^ count ^ reset_color
    else count in
  F.printf "Passed:  %d\n" !cnt_ok;
  F.printf "Timeout: %s\n" timeout_str;
  F.printf "Failed:  %s\n" failed_str;
  F.printf "Time:    %.3f sec\n" (tot_duration /. float_of_int stress_test);
  F.printf "%s\n\n" sep;
  if validated = Some true && (!cnt_to != 0 || !cnt_ko != 0) then
    Log.error "REGRESSION: Some validated tests failed"

(** Checking results *)
let is_success (tr: test_result) =
  match tr with
  | Tr_ok _ -> true
  | Tr_fail | Tr_timeout -> false

(** For continuous integration *)
let pp_results_junit
    fmt
    (test_suite_name: string)
    (stress_test: int)          (* repeat number *)
    (tot_duration: float)       (* duration of tests *)
    (tests: test_spec RevMap.t) (* specs *)
    (m: test_result RevMap.t)   (* results *): unit =
  let (test_count, failure_count) =
    RevMap.fold (fun _ (tr: test_result) (test_count, failure_count) ->
      let test_count = succ test_count in
      let failure_count =
        if is_success tr then
          failure_count
        else
          succ failure_count in
      (test_count, failure_count)) m (0, 0) in
  let format_test_result fmt (tr: test_result) =
    match tr with
    | Tr_ok s ->
        Format.fprintf fmt {|<system-out>passed (%d ok)</system-out>|}
          s.ts_assert_ok
    | Tr_fail ->
        Format.fprintf fmt
          {|<failure message="failed" type="ERROR">failed</failure>|}
    | Tr_timeout ->
        Format.fprintf fmt
          {|<failure message="timeout" type="ERROR">timeout</failure>|} in
  let format_test_cases fmt =
    RevMap.iter (fun test (tr: test_result) ->
      let id = mk_strpp RevOrd.t_fpr test in
      let ts = RevMap.find test tests in
      let time =
        match tr with
        | Tr_ok s -> Some s.ts_time
        | Tr_fail | Tr_timeout -> None in
      Format.fprintf fmt {|
        <testcase id="%s" classname="" name="%s" file="%s" time="%a">
          %a
        </testcase>
      |} id id ts.sp_src_file (Format.pp_print_option Format.pp_print_float)
         time
        format_test_result tr) m in
  let time = tot_duration /. float_of_int stress_test in
  Format.fprintf fmt {|<?xml version="1.0"?>
    <testsuites name="%s" tests="%d" failures="%d" time="%f">
      <testsuite name="%s" tests="%d" failures="%d" time="%f">
        %t
      </testsuite>
    </testsuites>
  |} test_suite_name test_count failure_count time
    test_suite_name test_count failure_count time
    format_test_cases

(** Management of the pipes *)
let close_pipe pipe_name: unit =
  try
    Unix.unlink pipe_name
  with
  | exc ->
      Log.info "Pipe closure failed: %s\n" (Printexc.to_string exc)
let open_pipe pipe_name: unit =
  try
    Unix.mkfifo pipe_name 0o600;
  with
  | Unix.Unix_error (Unix.EEXIST, _, _) ->
      begin
        try (* the pipe already exists; remove it and retry *)
          close_pipe pipe_name;
          Unix.mkfifo pipe_name 0o600
        with
        | exc ->
            Log.info "Pipe opening failed: %s" (Printexc.to_string exc)
      end
  | exc ->
      Log.info "Pipe opening failed: %s" (Printexc.to_string exc)

(** Preparation of a test *)
let prepare_test
    (stress_test: int) (* repeat number *)
    ((ntest, rev): int * char option) (* test number and revision *)
    (t: test_spec)     (* test spec *)
    : (int -> int) * string array * string * string =
  let div_stress_test =
    if stress_test = 1 then fun n -> n
    else
      fun n ->
        if n mod stress_test = 0 then n / stress_test
        else Log.fatal_exn "division by repeat num" in
  (* Computing command line options for the analyzer *)
  let pipe_name =
    Format.asprintf "pipe_name-%d%a" ntest
      (Format.pp_print_option Format.pp_print_char) rev in
  let lopts =
    let l = spec_to_options t pipe_name in
    let l = if !no_timing then "-no-timing" :: l else l in
    let l =
      if stress_test = 1 then l
      else "-stress-test" :: string_of_int stress_test :: l in
    "analyze" :: l in
  let aopts = Array.of_list lopts in
  let out_filename =
    let f s =
      let str = (Filename.chop_extension s) in
      Format.asprintf "%s-%d%a.log" str ntest
        (Format.pp_print_option Format.pp_print_char) rev in
    if t.sp_src_file != "" then f t.sp_src_file
    else
      match t.sp_specfile with
      | None -> Log.fatal_exn "Invalid entry, lacking an input file"
      | Some s -> f s in
  (div_stress_test, aopts, out_filename, pipe_name)

(** Run of a test *)
let run_test
    (analyzer: string)                (* analyzer program *)
    (stress_test: int)                (* repeat number *)
    ((ntest, rev): int * char option) (* test number and revision *)
    (t: test_spec)                    (* test spec *)
    : float * test_result =
  Log.info "RT: Forking, task [%a]" RevOrd.t_fpr (ntest,rev);
  let div_stress_test, aopts, out_filename, pipe_name =
    prepare_test stress_test (ntest, rev) t in
  let out_file =
    if flag_debug_open_file then
      Log.info "About to open file: %s" out_filename;
    Unix.openfile out_filename
      [ Unix.O_WRONLY ; Unix.O_CREAT ; Unix.O_TRUNC ] 0o600 in
  Log.info "Command line:";
  (* allow users to copy-paste the command and run it directly *)
  let command = String.concat " " (Array.to_list aopts) in
  Log.info "./%s" command;
  flush stdout;
  let t0 = Timer.cpu_timestamp () in
  let child =
    Unix.create_process analyzer aopts
      (Unix.descr_of_in_channel stdin) out_file out_file in
  (* Detection of timeout cases is not at all obvious:
   *  => difficulty:
   *     we would like either to get the output of the process (without
   *     waiting for timeout) or to kill it at timeout and have the
   *     information that it did timeout
   *  => fix attempt:
   *     use SIGABRT *)
  let timeout =
    Unix.open_process_in
      (Format.asprintf "sleep %d && kill -6 %d" (stress_test * t.sp_time_out)
         child) in
  let duration, result =
    try
      let pid, status = Unix.waitpid [ ] child in
      Unix.kill (Unix.process_in_pid timeout) Sys.sigkill;
      close_in timeout;
      let t1 = Timer.cpu_timestamp () in
      let duration = t1 -. t0 in
      assert (pid = child);
      let passed, res_timeout =
        match status with
        | Unix.WEXITED i ->
            begin
              if i = 0 then
                Log.info "Status exit: %d" i
              else
                Log.error "%sStatus exit: %d%s" magenta_color i reset_color
            end;
            i = 0, false
        | Unix.WSIGNALED i ->
            Log.error "%sStatus signal: %d%s" magenta_color i reset_color;
            false, i = -1
        | Unix.WSTOPPED i  ->
            Log.error "%sStatus stopped: %d%s" magenta_color i reset_color;
            false, false in
      Log.info "RT: child terminated its task (%.3f)" duration;
      let res =
        if passed then
          try
            (* read information sent by the child on the channel *)
            if flag_debug_open_file then
              Log.info "About to open pipe: %s" pipe_name;
            let file = Unix.openfile pipe_name [ Unix.O_RDONLY ] 0o600 in
            let chan = Unix.in_channel_of_descr file in
            let s0 = input_line chan in
            let s1 = input_line chan in
            let i0 = int_of_string s0 in
            let i1 = int_of_string s1 in
            Log.info "Read: ok=%d ko=%d" i0 i1;
            Unix.close file;
            Unix.unlink pipe_name;
            let i0 = div_stress_test i0 in
            let i1 = div_stress_test i1 in
            if i0 + i1 = t.sp_assert_tot && i0 = t.sp_assert_ok then
              Tr_ok { ts_assert_ok = i0;
                      ts_assert_ko = i1;
                      ts_time      = duration /. float_of_int stress_test }
            else Tr_fail
          with
          | exc ->
              Log.info "Exception during reading of the results: %s\n"
                (Printexc.to_string exc);
              Tr_fail
        else if res_timeout then Tr_timeout
        else Tr_fail in
      duration, res
    with
    | Unix.Unix_error (Unix.EINTR, _, _) ->
        (* TODO:
         * XR: I think this does not work anymore
         *    => by sending a different signal and observing child
         *       ending as "Signaled (-1)" *)
        (* waitpid interrupted as the child was killed, due to a timeout *)
        Log.error "%schild was terminated%s" magenta_color reset_color;
        (* return the time-out information *)
        0., Tr_timeout in
  Unix.close out_file;
  if !stdout_dump then
    ignore(Unix.system ("cat " ^ out_filename));
  (duration, result)

(** Main function *)
let batch () =
  Logger_glob.set_log_level "batch" Log_level.INFO;
  (* FBR: test on the fly log level change *)
  (* Setting which experiments to run *)
  let flag_pure_regtest: bool ref = ref false in
  let flag_pure_expe:    bool ref = ref false in
  let flag_all_test:     bool ref = ref false in
  let flag_fail_on_failure: bool ref = ref false in
  (* name of the analyzer binary *)
  let analyzer: string ref = ref "bin/analyze" in
  (* name of the file describing all tests to perform *)
  let filename: string ref = ref "rt.txt" in
  (* unique test to run (if none, will run all tests) *)
  let test_ids: (int * char option) list ref = ref [ ] in
  (* category of tests to run *)
  let category: string ref = ref "" in
  (* number of repeats for stress-test *)
  let stress_test: int ref = ref 1 in
  (* sequential or parallel run of tests *)
  let ncores = ref 1 in (* sequential by default *)
  (* setting option fields *)
  let optset (r: bool option ref) (b: bool) =
    Arg.Unit (fun () -> r := Some b) in
  (* JUnit output *)
  let junit = ref None in
  Arg.parse
    [ (* --> basic test suite options *)
      "-in-file", Arg.Set_string filename, "FILE use tests from FILE";
      ("-analyzer", Arg.Set_string analyzer,
       "FILE path to the analyzer executable");
      "-all-test", Arg.Set flag_all_test, "run all tests";
      (* --> options related to performance settings *)
      ("-stress-test", Arg.Set_int stress_test,
       "N repeat each test N times");
      ("-ncores", Arg.Set_int ncores,
       "N run tests in parallel on N cores max");
      (* --> options to override specific settings in the test suite *)
      "-silent",    optset flag_silent true,  "enable silent mode";
      "-very-silent", optset flag_very_silent true, "very silent checking";
      "-no-silent", optset flag_silent false, "disable silent mode";
      "-no-very-silent", optset flag_very_silent false, "very silent checking";
      "-no-timing", Arg.Set no_timing, "disable timing info display";
      (* --> intended for developper use *)
      "-dot-all",    optset dot_all true,  "output all states in graphviz";
      "-stdout", Arg.Set stdout_dump, "cat logfile to stdout too";
      ("-junit", Arg.String (fun s -> junit := Some s),
       "JUnit XML file target");
      ("-fail-on-failure", Arg.Set flag_fail_on_failure,
       "fail with exit code 1 if there is any failure");
      (* --> options to run a specific category of tests *)
      "-pure-regtest", Arg.Set flag_pure_regtest, "only run validated tests";
      "-pure-expe", Arg.Set flag_pure_expe, "only run experimental tests";
      ("-pure-cat", Arg.Set_string category,
       "CAT only run tests in category CAT"); ]
    (fun s ->
      try test_ids :=
        let ic =
          let n = String.length s in assert (0 < n);
          let last = String.get s (n-1) in
          let ilast = int_of_char last in
          if int_of_char '0' <= ilast && ilast <= int_of_char '9' then
            (* this is just a number *)
            int_of_string s, None
          else
            (* this is a number + a revision index *)
            int_of_string (String.sub s 0 (n-1)), Some last in
        ic :: !test_ids
      with _ ->
        Log.info "failed to read int argument";
        exit 1
    ) "Batch regression testing" ;
  assert (!ncores > 0);
  if !flag_all_test then
    begin
      flag_pure_regtest := true;
      flag_pure_expe    := true;
    end;
  let pp_results validated time spec results =
    pp_results !stress_test validated time spec results;
    match !junit with
    | None -> ()
    | Some junit ->
        let chan = open_out junit in
        Fun.protect (fun () ->
          let fmt = Format.formatter_of_out_channel chan in
          pp_results_junit fmt junit !stress_test time spec results;
          Format.pp_print_flush fmt ())
        ~finally:(fun () -> close_out chan) in
  (* parsing and translation *)
  let p_config = config_parser !filename in
  let tests =
    let tt = translate_tests p_config in
    match !test_ids with
    | [ ] -> tt
    | ics ->
        try
          List.fold_left
            (fun acc ic ->
              RevMap.add ic (RevMap.find ic tt) acc
            ) RevMap.empty ics
        with Not_found -> RevMap.empty in
  (* running of analyses *)
  let run_tests (category: test_category) (tests: test_spec RevMap.t)
      : float * test_result RevMap.t =
    let eq_category c0 c1 =
      match c0, c1 with
      | TC_valid, TC_valid | TC_exp, TC_exp -> true
      | TC_other s0, TC_other s1 -> String.compare s0 s1 = 0
      | _, _ -> false in
    let pool = Domainslib.Task.setup_pool ~num_domains:(!ncores - 1) () in
    let result =
      Domainslib.Task.run pool (fun () ->
        let promises =
          RevMap.fold
            (fun i t acc->
              let cats = List.filter (eq_category category) t.sp_category in
              if cats != [ ] then
                let promise =
                  Domainslib.Task.async pool (fun () ->
                    run_test !analyzer !stress_test i t) in
                (i, promise) :: acc
              else acc
            ) tests [] in
          List.fold_left
            (fun (accd, accr) (i, promise) ->
              let d, r = Domainslib.Task.await pool promise in
              accd +. d, RevMap.add i r accr
            ) (0., RevMap.empty) promises) in
    Domainslib.Task.teardown_pool pool;
    result in
  let exit_with_code (tests: test_result RevMap.t list) =
    if !flag_fail_on_failure &&
         List.exists (fun rev_map ->
         RevMap.exists (fun _ (tr: test_result) -> not (is_success tr)) rev_map)
           tests
    then
      exit 1
    else
      exit 0 in
  if !test_ids = [ ] then
    (* general run: all tests in a category *)
    begin
      (* removal of excluded tests *)
      let tests =
        RevMap.fold
          (fun ic t acc ->
            if t.sp_excluded then acc
            else RevMap.add ic t acc
          ) tests RevMap.empty in
      (* tests which were already validated *)
      let out_valid =
        if not !flag_pure_regtest then 0., RevMap.empty
        else run_tests TC_valid tests in
      (* experimental tests *)
      let out_expe =
        if not !flag_pure_expe then 0., RevMap.empty
        else run_tests TC_exp tests in
      (* category specific tests *)
      let out_other =
        if String.length !category = 0 then 0., RevMap.empty
        else run_tests (TC_other !category) tests in
      if not !flag_pure_expe && snd out_valid != RevMap.empty then
        pp_results (Some true)  (fst out_valid) tests (snd out_valid);
      if not !flag_pure_regtest && snd out_expe != RevMap.empty then
        pp_results (Some false) (fst out_expe)  tests (snd out_expe);
      if snd out_other != RevMap.empty then
        pp_results (Some false) (fst out_other) tests (snd out_other);
      exit_with_code [snd out_valid; snd out_expe; snd out_other]
    end
  else
    begin
      let out_t, out_r =
        RevMap.fold
          (fun i t (accd, accr) ->
            let d, r = run_test !analyzer !stress_test i t in
            accd +. d, RevMap.add i r accr
          ) tests (0., RevMap.empty) in
      pp_results None out_t tests out_r;
      exit_with_code [out_r]
    end

let () = batch ()
