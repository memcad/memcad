#-------------------------------------------------------------------#
# makefile for a domain library version                             #
#-------------------------------------------------------------------#
# For wc
ML_ML=  bin/increase_arity.ml \
	c_frontend/c_parser.mly \
	c_frontend/c_lexer.mll \
	c_frontend/mc_parser.mly \
	c_frontend/mc_lexer.mll \
	analyze/domsel_parser.mly \
	analyze/domsel_lexer.mll \
	sd/ind_lexer.mll \
	sd/ind_parser.mly \
	analyze/analyze.ml \
	analyze/transform.ml \
	batch/batch.ml \
	batch/rt_sig.ml \
	c_analyze/c_analysis_sig.ml \
	c_analyze/c_analysis_timing.ml \
	c_analyze/c_analyze.ml \
	c_analyze/c_ind_infer.ml \
	c_analyze/c_ind_infer.mli \
	c_analyze/c_pre_analyze.ml \
	c_analyze/c_pre_analyze.mli \
	c_analyze/c_process.ml \
	c_analyze/c_process.mli \
	c_analyze/dom_c.ml \
	c_frontend/ast_sig.ml \
	c_frontend/ast_utils.ml \
	c_frontend/ast_utils.mli \
	c_frontend/c_sig.ml \
	c_frontend/c_utils.ml \
	c_frontend/c_utils.mli \
	lib/aa_maps.ml \
	lib/aa_maps.mli \
	lib/aa_sets.ml \
	lib/aa_sets.mli \
	lib/bi_fun.ml \
	lib/bi_fun.mli \
	lib/config_lexer.mll \
	lib/config_parser.mly \
	lib/config_sig.ml \
	lib/data_structures.ml \
	lib/flags.ml \
	lib/flags.mli \
	lib/graph.ml \
	lib/keygen.ml \
	lib/keygen.mli \
	lib/latex_utils.ml \
	lib/latex_utils.mli \
	lib/lib.ml \
	lib/lib.mli \
	lib/refcount.ml \
	lib/refcount.mli \
	lib/regular_expr.ml \
	lib/regular_expr.mli \
	lib/timer.ml \
	lib/timer.mli \
	lib/union_find.ml \
	lib/union_find.mli \
	nd/bound_set.ml \
	nd/bound_set.mli \
	nd/bound_sig.ml \
	nd/bounds.ml \
	nd/bounds.mli \
	nd/dom_no_set.ml \
	nd/dom_set.ml \
	nd/dom_val_num.ml \
	nd/nd_add_bottom.ml \
	nd/nd_add_diseqs.ml \
	nd/nd_add_dyn_svenv.ml \
	nd/nd_apron.ml \
	nd/nd_sig.ml \
	nd/nd_utils.ml \
	nd/nd_utils.mli \
	nd/off_impl.ml \
	nd/off_impl.mli \
	nd/off_linexpr.ml \
	nd/off_linexpr.mli \
	nd/off_sig.ml \
	nd/offs.ml \
	nd/offs.mli \
	nd/quicr_dump.ml \
	nd/quicr_lin.ml \
	nd/seq_dom.ml \
	nd/seq_utils.ml \
	nd/seq_utils.mli \
	nd/set_dump.ml \
	nd/set_lin.ml \
	nd/set_quicr.ml \
	nd/set_sig.ml \
	nd/set_utils.ml \
	nd/set_utils.mli \
	nd/svenv_sig.ml \
	nd/svenv_utils.ml \
	nd/svenv_utils.mli \
	nd/vd_sig.ml \
	nd/vd_utils.ml \
	nd/vd_utils.mli \
	sd/array_ind_sig.ml \
	sd/array_ind_utils.ml \
	sd/array_ind_utils.mli \
	sd/array_node.ml \
	sd/array_node.mli \
	sd/array_ppred_sig.ml \
	sd/array_ppred_utils.ml \
	sd/array_ppred_utils.mli \
	sd/block_frag.ml \
	sd/block_frag.mli \
	sd/constrain_ops.ml \
	sd/constrain_ops.mli \
	sd/constrain_rules.ml \
	sd/constrain_rules.mli \
	sd/constrain_sig.ml \
	sd/constrain_utils.ml \
	sd/constrain_utils.mli \
	sd/disj_sig.ml \
	sd/disj_utils.ml \
	sd/disj_utils.mli \
	sd/dom_dictionary.ml \
	sd/dom_disj.ml \
	sd/dom_env.ml \
	sd/dom_eqs.ml \
	sd/dom_eqs.mli \
	sd/dom_mem_exprs.ml \
	sd/dom_mem_low_flat.ml \
	sd/dom_mem_low_list.ml \
	sd/dom_mem_low_prod.ml \
	sd/dom_mem_low_sep.ml \
	sd/dom_mem_low_sum_sep.ml \
	sd/dom_mem_low_shape.ml \
	sd/dom_no_disj.ml \
	sd/dom_sig.ml \
	sd/dom_subm_graph.ml \
	sd/dom_subm_sig.ml \
	sd/dom_timing.ml \
	sd/dom_utils.ml \
	sd/dom_utils.mli \
	sd/dom_val_array.ml \
	sd/dom_val_maya.ml \
	sd/dom_val_subm.ml \
	sd/gen_dom.ml \
	sd/gen_is_le.ml \
	sd/gen_is_le.mli \
	sd/gen_join.ml \
	sd/gen_join.mli \
	sd/graph_abs.ml \
	sd/graph_abs.mli \
	sd/graph_algos.ml \
	sd/graph_algos.mli \
	sd/graph_dir_weak.ml \
	sd/graph_dir_weak.mli \
	sd/graph_encode.ml \
	sd/graph_is_le.ml \
	sd/graph_is_le.mli \
	sd/graph_join.ml \
	sd/graph_join.mli \
	sd/graph_materialize.ml \
	sd/graph_materialize.mli \
	sd/graph_sig.ml \
	sd/graph_strategies.ml \
	sd/graph_strategies.mli \
	sd/graph_utils.ml \
	sd/graph_utils.mli \
	sd/graph_visu.ml \
	sd/graph_visu.mli \
	sd/graph_visu_atom.ml \
	sd/graph_visu_common.ml \
	sd/graph_visu_common.mli \
	sd/graph_visu_sig.ml \
	sd/ind_analysis.ml \
	sd/ind_analysis.mli \
	sd/ind_form_sig.ml \
	sd/ind_form_utils.ml \
	sd/ind_form_utils.mli \
	sd/ind_sig.ml \
	sd/ind_utils.ml \
	sd/ind_utils.mli \
	sd/inst_sig.ml \
	sd/inst_utils.ml \
	sd/inst_utils.mli \
	sd/list_abs.ml \
	sd/list_abs.mli \
	sd/list_inst.ml \
	sd/list_inst.mli \
	sd/list_is_le.ml \
	sd/list_is_le.mli \
	sd/list_join.ml \
	sd/list_join.mli \
	sd/list_mat.ml \
	sd/list_mat.mli \
	sd/list_sig.ml \
	sd/list_utils.ml \
	sd/list_utils.mli \
	sd/list_visu.ml \
	sd/list_visu.mli \
	sd/printable_graph.ml \
	sd/printable_graph.mli \
	sd/spec_sig.ml \
	sd/spec_utils.ml \
	sd/spec_utils.mli \
	sd/statistics.ml \
	sd/statistics.mli

BIN_PREFIX = bin
BIN_ANALYZE = $(BIN_PREFIX)/analyze
BIN_BATCH = $(BIN_PREFIX)/batch
BIN_DANALYZE = $(BIN_PREFIX)/danalyze
BIN_DBATCH = $(BIN_PREFIX)/dbatch

#-------------------------------------------------------------------#
# Main targets					                    #
#-------------------------------------------------------------------#
.PHONY: all
all: analyze batch

# Normal mode; native code
.PHONY: analyze
analyze: $(BIN_ANALYZE)

# Dune targets are phony because dependency analysis is delegated to dune
.PHONY: $(BIN_ANALYZE)
$(BIN_ANALYZE):
	dune build --sandbox=none analyze/analyze.exe
	mkdir -p $(BIN_PREFIX)
	ln -sf ../_build/default/analyze/analyze.exe $(BIN_ANALYZE)

.PHONY: batch
batch: $(BIN_BATCH)

.PHONY: $(BIN_BATCH)
$(BIN_BATCH):
	dune build batch/batch.exe
	mkdir -p $(BIN_PREFIX)
	ln -sf ../_build/default/batch/batch.exe $(BIN_BATCH)

# In debug mode
.PHONY: danalyze
danalyze: $(BIN_DANALYZE)

.PHONY: $(BIN_DANALYZE)
$(BIN_DANALYZE):
	dune build analyze/analyze.bc
	mkdir -p $(BIN_PREFIX)
	ln -sf ../_build/default/analyze/analyze.bc $(BIN_DANALYZE)

.PHONY: dbatch
dbatch: $(BIN_DBATCH)

.PHONY: $(BIN_DBATCH)
$(BIN_DBATCH):
	dune build batch/batch.bc
	mkdir -p $(BIN_PREFIX)
	ln -sf ../_build/default/batch/batch.bc $(BIN_DBATCH)

#-------------------------------------------------------------------#
# Testing targets                                                   #
#-------------------------------------------------------------------#
BATCH_OPT::=
.PHONY: rt analyze
# (p|)(rt|ex)(p|)
# - p: pure
# - rt: regression tests   (already   validated)
#   ex: experimental tests (still not validated)
# - p: perf mode (disable all pretty-printing)
brokenarm: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat brokenarm
broken: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat broken
hack: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat hack
todobdd: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat todobdd
sas: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat sas
sastest: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat sastest
rt: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -all-test
tests: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file tests.txt -pure-cat comp
mtests: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file tests.txt -pure-cat mini
rtp: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -all-test -very-silent
prt: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-regtest
prtst: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-regtest -very-silent -stress-test 100
prtp: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-regtest -very-silent -junit prtp.xml
veriamos: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file veriamos.txt -pure-cat veriamos
seq: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat seq -very-silent
sorted: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat sorted -very-silent
tofix: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat tofix -very-silent
sas23: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat sas23 -very-silent
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) sas23
seqt: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file re.txt -pure-cat seq -dot-all
linux: analyze batch
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) linux
freertos: analyze batch
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) freertos
# Other targets
selwiden: analyze batch # regression tests that take significant time
	$(BIN_BATCH) $(BATCH_OPT) -in-file bigrt.txt -pure-cat selwiden
popl17: analyze batch # regression tests that take significant time
	$(BIN_BATCH) $(BATCH_OPT) -in-file bigrt.txt -pure-cat popl17
poplm17: analyze batch # regression tests that take significant time
	$(BIN_BATCH) $(BATCH_OPT) -in-file bigrt.txt -pure-cat poplm17
prtpnt: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-regtest -very-silent -no-timing
pex: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-expe
pexp: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-expe -very-silent
lincheck: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat lincheck
tvl: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat tvl
# test for array domain
pcva: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat validarray
pcea: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat arraypred
pcas: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat arrayshape
# test for the specification format
spec: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat spec
# unit tests for join_paths
jptest: analyze
	qtest -o sd/jptest.ml extract sd/graph_encode.ml
	obuild build exe-jptest
	./jptest
	echo "(* DO NOT EDIT THIS FILE *)" > sd/jptest.ml

# Temporary:
rtl: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -pure-cat explist
# debug (deprecated)
vmcai13: analyze batch bench/vmcai-ex-00.c
	$(BIN_BATCH) 5002a 5002c 5002d
# set tests already validated
sprtp: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat validset -very-silent
spex: analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat expset
# tests for separated summaries
sumsep:	analyze batch
	$(BIN_BATCH) $(BATCH_OPT) -in-file rt.txt -pure-cat sumsep

.PHONY: memcad-codes-prtp
memcad-codes-prtp: analyze batch
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) prtp

.PHONY: memcad-codes-bigrt
bigrt: memcad-codes-bigrt
memcad-codes-bigrt: analyze batch
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) bigrt

.PHONY: memcad-codes
memcad-codes: analyze batch
	$(MAKE) -C $(MEMCADCODES) \
	  BIN_BATCH=$(PWD)/$(BIN_BATCH) BIN_ANALYZE=$(PWD)/$(BIN_ANALYZE) all-tests

#-------------------------------------------------------------------#
# Auxilliary stuffs						    #
#-------------------------------------------------------------------#
.PHONY: edit wedit wc clean pull tags
edit:
	emacs --background-color=Black --foreground-color=White &
wedit:
	emacs --background-color=White --foreground-color=Black makefile &
wc:
	wc $(ML_ML)
gclean: # graphs cleaning
	rm -f *.dot *.pdf *.c-*N*-D*.enc*.txt

clean: gclean
	dune clean
	rm -rf */*.o */*.cmo */*.cmi */*.cmx */*.cma */*.cmxa */*.cmo \
	  */*.cmi */*.cmt */*.annot */*~ */*.output depend config dconfig \
	  *.pdf *.dot bench/log-*.tex bench/*/*.log bench/*/*/*.log \
	  bench/*/*/*/*.log bench/*/*/*/*/*.log dbatch danalyze log.log \
	  bin/analyze bin/batch \
	  prtp.xml bigrt.xml data/*.dot data/*.txt data/*.strace

exclean:
	rm -rf bench/*.log bench/*/*log

dataclean:
	rm -rf data/*.dot data/*.txt data/*.strace

#-------------------------------------------------------------------#
# GIT            						    #
#-------------------------------------------------------------------#
lpull:
	git pull ../../pull/memcad
mpull:
	git pull rival@mezcal.ens.fr:git/memcad.git master
mpush:
	git push rival@mezcal.ens.fr:git/memcad.git master
bpull:
	git pull /import/absint3/rival/git/memcad.git master
bpush:
	git push /import/absint3/rival/git/memcad.git master

#-------------------------------------------------------------------#
# DOCUMENTATION #
#-------------------------------------------------------------------#
doc: all
	make -C doc doc

#-------------------------------------------------------------------#
# INSTALL                                                           #
#-------------------------------------------------------------------#
.PHONY: install
install:
	dune build @install
	dune install
