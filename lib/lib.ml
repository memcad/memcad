(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: lib.ml
 **       library functions
 ** Xavier Rival, 2011/05/19 *)
open Data_structures
open Flags


(** Error report (for module lib) *)
module Log =
  Logger.Make(struct let section = "lib_____" and level = Log_level.DEBUG end)


(** Counters *)
let count_assert_ok: int ref = ref 0
let count_assert_ko: int ref = ref 0

(** Counters state: (ok, ko) *)
let count_assert_get_state (): int * int = !count_assert_ok, !count_assert_ko

(** Export support *)
let show_sep (): unit =
  Log.info
    "\n\n=======================================================\n"

(** Logging of assertion results *)
let log_assert (b: bool): unit =
  if flag_log_assert then
    incr (if b then count_assert_ok else count_assert_ko)


(** Exit after displaying a status message, including warnings *)
let show_end_status_report (time_spent: float): unit =
  show_sep ();
  if !flag_pp_timing then
    Log.info "Final status report (%.4f sec spent)" time_spent;
  if flag_log_assert then
    begin
      Log.info "- assertions proved:\t %d (context)" !count_assert_ok;
      Log.info "- assertions not proved: %d (context)"  !count_assert_ko;
    end
let pipe_end_status_report ( ): unit =
  Log.info "Called writing pipe_end_status_report procedure!";
  if !use_pipe then
    let file =
      if flag_debug_open_file then
        Log.info "About to open (pipe for report): %s" !pipe_name;
      Unix.openfile !pipe_name
        [ Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC ] 0o600 in
    let chan = Unix.out_channel_of_descr file in
    Log.info "Writing results over pipe %s..." !pipe_name;
    Log.info "Sending data: %d, %d..." !count_assert_ok !count_assert_ko;
    Printf.fprintf chan "%d\n%d\n" !count_assert_ok !count_assert_ko;
    Log.info "Results written over the pipe!";
    flush chan;
    Unix.close file


(** Exceptions to stop computations *)
exception Stop
exception StopMsg of string
exception FailMsg of string (* failure with message; conservatively ignored *)
exception True
exception False


(** Support for parsing *)

(* Printing tokens (or error) positions *)
let format_lexbuf_position (fmt : Format.formatter)
    (lexbuf: Lexing.lexbuf): unit =
  if lexbuf.lex_start_p.pos_lnum = lexbuf.lex_curr_p.pos_lnum then
    Format.fprintf fmt "line %d columns %d-%d"
      (lexbuf.lex_start_p.pos_lnum)
      (lexbuf.lex_start_p.pos_cnum - lexbuf.lex_start_p.pos_bol)
      (lexbuf.lex_curr_p.pos_cnum - lexbuf.lex_curr_p.pos_bol)
  else
    Format.fprintf fmt "line %d column %d to line %d column %d"
      (lexbuf.lex_start_p.pos_lnum)
      (lexbuf.lex_start_p.pos_cnum - lexbuf.lex_start_p.pos_bol)
      (lexbuf.lex_curr_p.pos_lnum)
      (lexbuf.lex_curr_p.pos_cnum - lexbuf.lex_curr_p.pos_bol)

(* Generic parsing function *)
let read_from_lexbuf
    (kind: string)
    (loc: string)
    (f_parse: (Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
    (f_lexe: Lexing.lexbuf -> 'b)
    (lexbuf: Lexing.lexbuf): 'a =
  try f_parse f_lexe lexbuf
  with
  | e ->
      Log.fatal_exn "Error during %s parsing, at %a in %s: %s\n"
        kind format_lexbuf_position lexbuf loc (Printexc.to_string e)

(* Generic "from file" parsing function *)
let read_from_file
    (kind: string)
    (f_parse: (Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
    (f_lexe: Lexing.lexbuf -> 'b)
    (filename: string): 'a =
  if flag_debug_open_file then
    Log.info "About to open (gen: %s): %s" kind filename;
  let file = Unix.openfile filename [ Unix.O_RDONLY ] 0o644 in
  let channel = Unix.in_channel_of_descr file in
  let lexbuf = Lexing.from_channel channel in
  read_from_lexbuf kind (Printf.sprintf "file %s" filename)
    f_parse f_lexe lexbuf

(* Generic "from string" parsing function *)
let read_from_string
    (kind: string)
    (f_parse: (Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
    (f_lexe: Lexing.lexbuf -> 'b)
    (s: string): 'a =
  read_from_lexbuf kind "string" f_parse f_lexe (Lexing.from_string s)


(** String operations *)
(* make printers *)
let mk_strpp (fp: form -> 'a -> unit) (x: 'a): string =
  Format.asprintf "%a" fp x
let mk_bufpp (fp: form -> 'a -> unit) (buf: Buffer.t) (x: 'a): unit =
  Printf.bprintf buf "%s" (mk_strpp fp x)
(* Split based on some character *)
let str_split char s =
  let i = String.index s char in
  String.sub s 0 i, String.sub s (i + 1) (String.length s - i - 1)
(* Generate series of spaces *)
let mk_space (i: int) = String.make i ' '
(* Flush left and right, up to a given length *)
let str_lflush (len: int) (str: string): string =
  let l = String.length str in
  if l > len then
    if len > 3 then F.asprintf "%s..." (String.sub str 0 (len-3))
    else String.make len '.'
  else F.asprintf "%s%s" str (mk_space (len - l))
let str_rflush (len: int) (str: string): string =
  let l = String.length str in
  if l > len then
    if len > 3 then F.asprintf "...%s" (String.sub str (l - len + 3) (len-3))
    else String.make len '.'
  else F.asprintf "%s%s" (mk_space (len - l)) str
(* Length of string representation *)
let rec posint_len (i: int): int =
  if i < 0 then Log.fatal_exn "posing_len applied to negative"
  else if i < 10 then 1
  else 1 + posint_len (i / 10)


(** Support for pretty-printing *)
(* basic types *)
let int_fpr fmt i = F.fprintf fmt "%d" i
let string_fpr fmt s = F.fprintf fmt "%s" s
(* printing of a list with separators *)
let gen_list_fpr (empty: string) (f: form -> 'a -> unit) (sep: string)
    (fmt: form) (l: 'a list): unit =
  match l with
  | [ ] -> F.fprintf fmt "%s" empty
  | [ a ] -> f fmt a
  | a :: ll ->
      f fmt a;
      List.iter (F.fprintf fmt "%s%a" sep f) ll
(* printing one item per line, with indentation *)
let gen_list_items_fpr ?(iind: int = 0) ?(lret: bool = false)
    (f: form -> 'a -> unit) (fmt: form) (l: 'a list): unit =
  let ind = String.make iind ' ' in
  let rec aux = function
    | [ ] -> ()
    | [ a ] ->
        F.fprintf fmt "%s%a" ind f a;
        if lret then F.fprintf fmt "\n"
    | a :: l ->
        F.fprintf fmt "%s%a\n" ind f a;
        aux l in
  aux l
let gen_list_items_first_fpr ?(iind: int * int = (0, 0)) ?(lret: bool = false)
    (f: form -> 'a -> unit) (fmt: form) (l: 'a list): unit =
  match l with
  | [ ] -> ()
  | a :: l ->
      let ind0 = String.make (fst iind) ' ' in
      let iind = snd iind in
      F.fprintf fmt "%s%a\n%a" ind0 f a (gen_list_items_fpr ~iind ~lret f) l
(* printing options *)
let option_fpr (mt: string) (f: form -> 'a -> unit)
    (fmt: form) (x: 'a option): unit =
  match x with
  | None -> F.fprintf fmt "%s" mt
  | Some x -> f fmt x
(* printing a list of integers *)
let intlist_fpr (fmt: form) (l: int list): unit =
  F.fprintf fmt "[%a]" (gen_list_fpr "" int_fpr ";") l
(* printing a list of strings *)
let stringlist_fpr (sep: string) (fmt: form) (l: string list): unit =
  gen_list_fpr "" string_fpr sep fmt l
(* printing a set of integers *)
let intset_fpr (fmt: form): IntSet.t -> unit =
  F.fprintf fmt "{ %a }" (IntSet.t_fpr "; ")
(* printing nothing (for default arguments) *)
let unit_fpr (fmt: form) () = ()


(** IO *)
let with_in_file (fn: string) (f: in_channel -> 'a): 'a =
  let input = open_in fn in
  let res = f input in
  close_in input;
  res
let with_out_file (fn: string) (f: out_channel -> 'a): 'a =
  let output = open_out fn in
  let res = f output in
  close_out output;
  res
(* retrieve line number 'n' from file named 'fn'
 * n.b. the first line is number 1 (as in text editors) *)
let get_line (fn: string) (n: int): string =
  if n <= 0 then failwith "lib.ml: get_line: n <= 0";
  let rec loop input i =
    if i = 1 then
      input_line input
    else
      let (_: string) = input_line input in
      loop input (i - 1) in
  with_in_file fn (fun input -> loop input n)


(** System *)
let run_command (cmd: string): Unix.process_status =
  Log.info "running: %s" cmd;
  if !Flags.flag_enable_ext_export_all
      && String.length cmd > 3
      && String.sub cmd 0 3 = "dot" then
    Unix.WEXITED 0
  else
    Unix.system cmd


(** Output format *)
type output_format = (* might be extended with Tikz for LaTeX *)
  | Out_dot of string list * string list
