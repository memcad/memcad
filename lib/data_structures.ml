(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: data_structures.ml
 **       basic declarations of data structures
 ** Xavier Rival, 2011/05/19 *)

(** Printing shortcuts *)
module F = Format
type form = F.formatter

(** Interface to introspect the abstract domain configuration *)
module type NAME = sig
  val module_name: string
end
module type CONFIG_FPR = sig
  val config_fpr: form -> unit -> unit
end
module type INTROSPECT = sig
  include NAME
  include CONFIG_FPR
end

(** Tuples *)
let fst3 (a, _, _) = a
let trd3 (_, _, a) = a

(** Option types *)
module Option =
  struct
    include Option
    let get_default (def: 'a): 'a option -> 'a = function
      | None -> def
      | Some x -> x

    let t_fpr ?(emp: string = "??")
        (f: form -> 'a -> unit) (fmt: form): 'a option -> unit = function
      | None -> Format.fprintf fmt "%s" emp
      | Some x -> f fmt x
  end

(** Maps and Sets with printers *)
module type OrderedType =
  sig
    include Set.OrderedType
    val t_fpr: form -> t -> unit
  end
module type SET =
  sig
    include Set.S
    val to_list: t -> elt list
    val t_fpr:  string (* separator *) -> form -> t -> unit
    val pop_min: t -> elt * t
  end
module type MAP =
  sig
    include Map.S
    val t_fpr: string -> (form -> 'a -> unit) -> form -> 'a t -> unit
    val t_fpr_line: iind:(int * int) -> ?lret: bool
      -> ?kfpr:(form -> key -> unit)
        -> (form -> 'a -> unit) -> form -> 'a t -> unit
    val update: key -> 'a -> 'a t -> 'a t
    val keys: 'a t -> key list
    val values: 'a t -> 'a list
    (* Find functions with specific behavior if key absent *)
    val find_err: string -> key -> 'a t -> 'a
    val find_val: 'a -> key -> 'a t -> 'a
    val find_comp: (unit -> 'a) -> key -> 'a t -> 'a
    (* Union, for compatibility *)
    val union: (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t
  end
module SetMake = functor (O: OrderedType) ->
  struct
    include Set.Make( O )
    let to_list (t: t): elt list =
      fold (fun a acc -> a :: acc) t [ ]
    let t_fpr (sep: string) (fmt: form) (t: t): unit =
      let b = ref true in
      iter
        (fun i ->
          F.fprintf fmt "%s%a" (if !b then "" else sep) O.t_fpr i;
          b := false
        ) t
    (* return the min element of 's' and 's' without it *)
    let pop_min s =
      let mini = min_elt s in
      let others = remove mini s in
      (mini, others)
  end
module MapMake = functor (O: OrderedType) ->
  struct
    include Map.Make( O )
    let t_fpr (sep: string) (f_fpr: form -> 'a -> unit) (fmt: form) (t: 'a t)
        : unit =
      let b = ref true in
      iter
        (fun i x ->
          let locsep = if !b then "" else sep in
          b := false;
          F.fprintf fmt "%s%a => %a" locsep O.t_fpr i f_fpr x
        ) t
    let t_fpr_line ~(iind: int * int) ?(lret: bool = false)
        ?(kfpr: form -> key -> unit = O.t_fpr)
        (f_fpr: form -> 'a -> unit) (fmt: form) (t: 'a t): unit =
      let fst_line = ref true in
      let ind0 = String.make (fst iind) ' '
      and indn = String.make (snd iind) ' ' in
      iter
        (fun i x ->
          if !fst_line then
            begin
              F.fprintf fmt "%s%a => %a" ind0 kfpr i f_fpr x;
              fst_line := false
            end
          else
            F.fprintf fmt "\n%s%a => %a" indn kfpr i f_fpr x
        ) t;
      if lret && t != empty then F.fprintf fmt "\n"
    let update k v m =
      add k v (remove k m)
    (* Find functions with specific behavior if key absent *)
    exception MapErr of string
    let find_err (msg: string) (k: key) (t: 'a t): 'a =
      try find k t
      with Not_found -> raise (MapErr (Printf.sprintf "Map.Not_found: %s" msg))
    let find_val (v: 'a) (k: key) (t: 'a t): 'a =
      try find k t with Not_found -> v
    let find_comp (f: unit -> 'a) (k: key) (t: 'a t): 'a =
      try find k t with Not_found -> f ( )
    let keys (m: 'a t): key list =
      List.map fst (bindings m)
    let values (m: 'a t): 'a list =
      List.map snd (bindings m)
    (* Union, for compatibility *)
    let union f m0 m1 =
      fold
        (fun k x0 acc ->
          if mem k m1 then
            let x1 = find k m1 in
            match f k x0 x1 with
            | Some x -> add k x acc
            | None   -> raise (MapErr "union=>none")
          else add k x0 acc
        ) m0 m1
  end


(** Construction of standard data-structures *)
module IntOrd =
  struct
    type t = int
    let compare = Stdlib.compare
    let t_fpr fmt = F.fprintf fmt "%d"
  end
module StringOrd =
  struct
    type t = string
    let compare = compare
    let t_fpr fmt = F.fprintf fmt "%s"
  end

module IntMap =
  struct
    include MapMake(IntOrd)
    let of_list l =
      List.fold_left (fun acc (k, v) -> add k v acc) empty l
  end
module IntSet    =
  struct
    include SetMake(IntOrd)
  end
module StringMap = MapMake(StringOrd)
module StringSet =
  struct
    include SetMake(StringOrd)
    let of_list (l: string list) =
      List.fold_left (fun acc s -> add s acc) empty l
  end

module PairOrd (X: OrderedType) (Y: OrderedType): OrderedType
with type t = X.t * Y.t =
  struct
    type t = X.t * Y.t
    let compare (x0, y0) (x1, y1) =
      let cx = X.compare x0 x1 in
      if cx = 0 then
        Y.compare y0 y1
      else
        cx
    let t_fpr fmt (x, y) =
      F.fprintf fmt "(%a,%a)" X.t_fpr x Y.t_fpr y
  end

(** Pairs of integers; used for graphs *)
module type PSET =
  sig
    include SET
    val node_labels: t -> string list
  end
module type LSET =
  sig
    include SET
    val node_label: t -> string
  end

module IntPairOrd = PairOrd( IntOrd )( IntOrd )
module IntPairMap = MapMake( IntPairOrd )
module IntPairSet =
  struct
    include SetMake( IntPairOrd )
    (* convert a PairSet.t to a graph node label string;
     * creates a string such as "{o0@n231, o0@n243}" *)
    let node_label (ps: t): string =
      let buff = Buffer.create 80 in
      let started = ref false in
      Buffer.add_string buff "{";
      iter
        (fun (offs, uid) ->
          if !started then Buffer.add_string buff ", "
          else started := true;
          Buffer.add_string buff (Printf.sprintf "o%d@n%d" offs uid)
        ) ps;
      Buffer.add_string buff "}";
      Buffer.contents buff
  end

(* Set of pairs set *)
module PairOrdSet =
  struct
    type t = IntPairSet.t
    let compare t1 t2 =
      IntPairSet.compare t1 t2
    let t_fpr fmt t = F.fprintf fmt "{%a}" (IntPairSet.t_fpr ",") t
  end
module PairSetSet =
  struct
    include SetMake( PairOrdSet )
    let node_labels pss =
      fold
        (fun ps acc ->
          (IntPairSet.node_label ps) :: acc
        ) pss []
  end
module PairSetMap = MapMake( PairOrdSet )

(** Polymorphic maps *)
module PMap = Aa_maps
module PSet = Aa_sets

module List =
  struct
    include List
    (* find first index of x in l or raise Not_found *)
    let index x l =
      let rec loop i = function
        | [] -> raise Not_found
        | y :: ys ->
            if x = y then i
            else loop (i + 1) ys in
      loop 0 l
    let fold = fold_left
    let fold_lefti (f: int -> 'a -> 'b -> 'a) (x: 'a) (l: 'b list): 'a =
      let _i, res =
        List.fold_left
          (fun (i, acc) x -> (i + 1, f i acc x))
          (0, x) l in
      res
    let nthd (f: unit -> 'a) (l: 'a list) (n: int): 'a =
      try List.nth l n
      with Failure _ -> f ( )
    let map_flatten (f: 'a -> 'b list) (l: 'a list): 'b list =
      List.flatten (List.map f l)
  end

module String =
  struct
    include String
    (* rm first char of a string, if possible *)
    let pop s =
      if s = "" then
        raise (Invalid_argument "String.pop")
      else
        String.sub s 1 (String.length s - 1)
  end


module type KeygenS = sig
  type key

  module Set : SET with type elt = key

  (** Structure to keep free keys *)
  type t

  (** Functions over t *)
  (* Sanity check *)
  val sanity_check: string -> Set.t -> t -> unit
  (* Empty *)
  val empty: t
  (* Pretty-printing *)
  val t_fpr: form -> t -> unit
  (* Generate a new key *)
  val gen_key: t -> t * key
  (* Use a key *)
  val use_key: t -> key -> t
  (* Is a key used *)
  val key_is_used: t -> key -> bool
  (* Release a key *)
  val release_key: t -> key -> t
  (* Get all nodes (for debug) *)
  val get_nodes: t -> Set.t
  (* Equality test (expensive) *)
  val equal: t -> t -> bool
  (* Join:
   * - next key is the max of the next keys of both inputs;
   * - the two sets account for the keys added to both arguments *)
  val join: t -> t -> t * IntSet.t * IntSet.t
end
