(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: lib.mli
 **       library functions
 ** Xavier Rival, 2011/05/19 *)
open Data_structures

(** Logging of assertion results *)
val log_assert: bool -> unit

(** Export support *)
val show_sep: unit -> unit

(** Counters state: (ok, ko) *)
val count_assert_get_state: unit -> int * int

(** Exit after displaying a status message, including warnings *)
val show_end_status_report: float -> unit
val pipe_end_status_report: unit  -> unit

(** Exceptions to stop computations *)
exception Stop
exception StopMsg of string
exception FailMsg of string (* failure with message; conservatively ignored *)
exception True
exception False


(** Support for parsing *)
(* Generic parsing function *)
val read_from_file:
    string -> ((Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
      -> (Lexing.lexbuf -> 'b) -> string -> 'a
val read_from_string:
    string -> ((Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
      -> (Lexing.lexbuf -> 'b) -> string -> 'a

(** String operations *)
(* Split based on some character *)
val str_split: char -> string -> string * string
(* Generate series of spaces *)
val mk_space: int -> string
(* Flush left and right, up to a given length *)
val str_lflush: int -> string -> string
val str_rflush: int -> string -> string
(* Length of string representation *)
val posint_len: int -> int

(** Support for pretty-printing *)
(* basic types *)
val int_fpr: form -> int -> unit
val string_fpr: form -> string -> unit
(* make printers *)
val mk_strpp: (form -> 'a -> unit) -> 'a -> string
val mk_bufpp: (form -> 'a -> unit) -> Buffer.t -> 'a -> unit
(* printing options *)
val option_fpr: string -> (form -> 'a -> unit) -> form -> 'a option -> unit
(* printing a list of integers *)
val intlist_fpr: form -> int list -> unit
(* printing a list of strings *)
val stringlist_fpr: string -> form -> string list -> unit
(* printing of a list with separators *)
val gen_list_fpr: string -> (form -> 'a -> unit) -> string
  -> form -> 'a list -> unit
(* printing one item per line, with indentation *)
val gen_list_items_fpr: ?iind:int -> ?lret:bool -> (form -> 'a -> unit)
  -> form -> 'a list -> unit
(* printing one item per line, with indentation *)
val gen_list_items_first_fpr: ?iind:int*int -> ?lret:bool
  -> (form -> 'a -> unit) -> form -> 'a list -> unit
(* printing a set of integers *)
val intset_fpr: form -> IntSet.t -> unit
(* printing nothing (for default arguments) *)
val unit_fpr: form -> unit -> unit

(** IO *)
val with_in_file: string -> (in_channel -> 'a) -> 'a
val with_out_file: string -> (out_channel -> 'a) -> 'a
val get_line: string -> int -> string

(** System *)
val run_command: string -> Unix.process_status

(** Output format *)
type output_format = (* might be extended with Tikz for LaTeX *)
  (* produces a .dot file for all or only the listed variable names *)
  | Out_dot of string list * string list (* (variable names, display options) *)
