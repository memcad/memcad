(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Huisong Li, Jiangchao Liu,
 **          Pascal Sotin, Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_shape_sym_index.ml
 **       functor that encapsulates dom_bare_shape( ) functor
 ** Antoine Toubhans, 03/29/2013 *)
open Data_structures
open Flags
open Lib

open Ast_sig
open Ast_utils
open C_sig
open Dom_sig
open Dom_shape_base
open Graph_sig
open Graph_utils
open Ind_sig
open Nd_sig
open Offs
open Sym_val
open Sym_val_constrain
(* Apron library needed here *)
open Apron

(** Error report *)
let todo = gen_todo "dom_shape_sym_index"
let error = gen_error "dom_shape_sym_index"
let warn = gen_warn "dom_shape_sym_index"

(** Extraction from table *)    
(* use rev_map, the order doesn't matter *)
let ss_get_uni_table: 'a Sep_selector.t uni_table -> 'a uni_table = 
  List.rev_map Sep_selector.get_leaf
let ss_get_bin_table: 'a Sep_selector.t bin_table -> 'a bin_table = 
  List.rev_map 
    (fun (x, y) -> Sep_selector.get_leaf x, Sep_selector.get_leaf y)
let ss_get_tri_table: 'a Sep_selector.t tri_table -> 'a tri_table = 
  List.rev_map 
    (fun (x, y, z) -> 
      Sep_selector.get_leaf x, 
      Sep_selector.get_leaf y,
      Sep_selector.get_leaf z)
let ss_get_hint_us (hu: 'a Sep_selector.t hint_us): 'a hint_us = 
  { hus_live = ss_get_uni_table hu.hus_live }
let ss_get_hint_bs (hb: 'a Sep_selector.t hint_bs): 'a hint_bs = 
  { hbs_live = ss_get_bin_table hb.hbs_live }
    
(** The shape domain: embedding of dom_bare_shape functor
 **   ALL integer symbolic value are replaced
 **   by Leaf of int sym_val constructor, e.g. :
 **   [ SV -> int ] o [ Abstract function int -> int ] o [ int -> SV ] *)
module Dom_shape = functor (D: DOM_SHAPE) ->
  (struct
    module Dv = D.Dv

    (** Type of abstract values *)
    type t = D.t

    (** Domain initialization to a set of inductive definitions *)
    (* Domain initialization to a set of inductive definitions *)
    let init_inductives: StringSet.t -> unit = D.init_inductives
    let inductive_is_allowed: string -> bool = D.inductive_is_allowed

    (** Fixing sets of keys *)
    let pending_addrem (t: t): t * addrem = 
      let t, ipa = D.pending_addrem t in
      let pa = 
        { ar_add = Sym_index_set.mk_leaves ipa.iar_add;
          ar_rem = Sym_index_set.mk_leaves ipa.iar_rem } in
      t, pa 

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t = D.bot
    let is_bot: t -> bool = D.is_bot
    (* Top element, with empty set of roots *)
    let top: unit -> t = D.top
    (* Pretty-printing *)
    let t_2stri: string -> t -> string = D.t_2stri
    let t_2str: t -> string = D.t_2str
    let ppi_t: string -> out_channel -> t -> unit = D.ppi_t
    let pp_t: out_channel -> t -> unit = D.pp_t
    (* Latex output *)
    let ltx_output (roots: (sym_index, string) PMap.t)
        (chan: out_channel) (x: t): unit = 
      let roots0 =
        PMap.fold (fun x -> PMap.add (Sep_selector.get_leaf x)) 
          roots PMap.empty in
      D.ltx_output roots0 chan x

    (** Management of cells and values *)
    (* Add a node, with a newly generated id *)
    let add_fresh_node ?(attr: node_attribute = Attr_none) (nt: ntyp)
        (na: nalloc) (x: t): sym_index * t =
      let x, t = D.add_fresh_node ~attr: attr nt na x in
      Sep_selector.mk_leaf x, t

    (** Management of the memory: add/destroy cells *)
    let add_cell ((x_src, o_src): off_sym_index) 
        (x_dst: sym_index): int -> t -> t = 
      let x_src0 = Sep_selector.get_leaf x_src 
      and x_dst0 = Sep_selector.get_leaf x_dst in
      D.add_cell (x_src0, o_src) x_dst0
    let rem_cell (t: t) (x: sym_index): t =
      (* takes node representing address *)
      D.rem_cell t (Sep_selector.get_leaf x)
    (* Garbage collection *)
    let gc (roots: sym_index uni_table): t -> t = 
      let roots0 = ss_get_uni_table roots in
      D.gc roots0
    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    let is_le (roots_rel: sym_index bin_table) (tl: t) (tr: t)
        : is_le_mapping option = 
      let roots_rel0 = ss_get_bin_table roots_rel in
      match D.is_le roots_rel0 tl tr with
      | Some map -> Some (Sym_index_mapping.t_from_node_mapping map)
      | None     -> None
    (* Generic comparison (does both join and widening) *)
    let join 
        (jk: join_kind) 
        (ohb: sym_index hint_bs option)
        (tt: sym_index tri_table)
        (tl: t) (tr: t): t * join_mapping = 
      let ohb0 = 
        match ohb with
        | Some hb -> Some (ss_get_hint_bs hb)
        | None    -> None
      and tt0 = ss_get_tri_table tt in
      let t, map_l, map_r = D.join jk ohb0 tt0 tl tr in
      let join_map = 
        { jm_l = Sym_index_mapping.t_from_node_mapping map_l;
          jm_r = Sym_index_mapping.t_from_node_mapping map_r } in
      t, join_map
    (* Directed weakening; over-approximates only the right element *)
    let directed_weakening 
        (ohb: sym_index hint_bs option)
        (tt: sym_index tri_table)
        (t1: t) (t2: t): t * directed_weakening_mapping =
      let ohb0 = 
        match ohb with
        | Some hb -> Some (ss_get_hint_bs hb)
        | None    -> None
      and tt0 = ss_get_tri_table tt in
      let t, map_r = D.directed_weakening ohb0 tt0 t1 t2 in
      t, Sym_index_mapping.t_from_node_mapping map_r
    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    let local_abstract (ohu: sym_index hint_us option) (x: t): t =
      let ohu0 = 
        match ohu with
        | Some hu -> Some (ss_get_hint_us hu)
        | None    -> None in
      D.local_abstract ohu0 x

    (** Reduction tools: read/enforce constrains *)
    let read_node (n: node) (cs: Constrain_shape.t): Constrain_shape.t = 
      match n.n_e with
      | Hemp -> cs
      | Hpt bf -> 
          Block_frag.fold 
            (fun bnd pt cs ->
              let i_o = Bounds.to_offs bnd 
              and i_x = Sep_selector.mk_leaf n.n_i 
              and j_x = Sep_selector.mk_leaf (fst pt.pe_dest) 
              and j_o = snd pt.pe_dest in
              Constrain_shape.add_pointsto (i_x, i_o) (j_x, j_o) cs)
            bf cs
      | Hind ie -> 
          (* 0) get the generic constrains that can be deduced *)
          let ind_cstr = Ind_analysis.ind_preds_find ie.ie_ind.i_name in
          (* 1) build the map from the ids into the constrains of ind_cstr
           * to the arguments of the inductive edge *)
          let map = (* 1.0: source node *)
            PMap.add 
              ind_cstr.Constrain_sig.cstri_main 
              (Sep_selector.mk_leaf n.n_i)
              PMap.empty in
          let map = (* 1.1: pointer arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map ind_cstr.Constrain_sig.cstri_ppars ie.ie_args.ia_ptr in
          let map = (* 1.2: numerical arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map ind_cstr.Constrain_sig.cstri_ipars ie.ie_args.ia_int in
          (* 2) add the constrains into cs *)
          Constrain_shape.add_constrains 
            map ind_cstr.Constrain_sig.cstri_cstr cs 
      | Hseg se ->
          (* 0. get the generic constrains that can be deduced *)
          let seg_cstr = Ind_analysis.seg_preds_find se.se_ind.i_name in
          (* 1. build the map from the ids into the constrains of ind_cstr
           *    to the arguments of the inductive edge *)
          let map = (* 1.0: source node *)
            PMap.add seg_cstr.Constrain_sig.cstrs_src 
              (Sep_selector.mk_leaf n.n_i) PMap.empty in
          let map = (* 1.1: pointer source arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map seg_cstr.Constrain_sig.cstrs_sppars se.se_sargs.ia_ptr in
          let map = (* 1.2: numerical source arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map seg_cstr.Constrain_sig.cstrs_sipars se.se_sargs.ia_int in
          let map = (* 1.3 destination node *)
            PMap.add seg_cstr.Constrain_sig.cstrs_dst
              (Sep_selector.mk_leaf se.se_dnode) map in
          let map = (* 1.4: pointer destination arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map seg_cstr.Constrain_sig.cstrs_dppars se.se_dargs.ia_ptr in
          let map = (* 1.5: numerical destination arguments *)
            List.fold_left2
              (fun map i x -> PMap.add i (Sep_selector.mk_leaf x) map)
              map seg_cstr.Constrain_sig.cstrs_dipars se.se_dargs.ia_int in
          (* 2. add the constrains into cs *)
          Constrain_shape.add_constrains 
            map seg_cstr.Constrain_sig.cstrs_cstr cs 
    
    (* read the whole graph *)
    let read (t: t) (cs: Constrain_shape.t): Constrain_shape.t = 
      if !flag_debug_reduction 
      then Printf.printf "Reduction: reading constrains in dom_shape...\n";
      IntMap.fold
        (fun i n cs ->
          (* 0. read shape constrains *)
          let cs = read_node n cs in
          (* 1. read numerical constrains, for now =0 or !=0 *)
          let cond_null: n_cons = Nc_cons (Tcons1.EQ, Ne_var i, Ne_csti 0)
          and cond_nnull: n_cons =
            Nc_cons (Tcons1.DISEQ, Ne_var i, Ne_csti 0) in
          if Dv.sat cond_null (D.get_num t) then
            Constrain_shape.add_null (Sep_selector.mk_leaf i) cs
          else
            if Dv.sat cond_nnull (D.get_num t) then
              Constrain_shape.add_not_null (Sep_selector.mk_leaf i) cs
            else
              cs
        ) (D.get_shape t).g_g cs

    (* read constrains from an element focusing on a left value *)
    let read_localize
        (tl: sym_index tlval)
        (t: t) (cs: Constrain_shape.t): Constrain_shape.t = 
      todo "read_localize"

    let enforce 
        (cs: Constrain_shape.t)
        (t: t): (Constrain_shape.t * t * Sym_index_mapping.t) list = 
      if !flag_debug_reduction then
        Printf.printf "Reduction: enforcing constrains in dom_shape...\n";
      (* 0. deals with node that are discovered as equal to NULL *)
      let null_cl = Constrain_shape.find_nulls cs in
      let t0 = (* set all the nodes to NULL in the numerical part *)
        PSet.fold
          (fun i t -> 
            let cons = Nc_cons (Tcons1.EQ, Ne_var i, Ne_csti 0) in 
            D.set_num t (Dv.guard true cons (D.get_num t))
              (*{ t with D.t_num = Dv.guard true cons t.D.t_num }*)
          ) (Sym_index_set.get_leaves null_cl) t in
      (* 1. enforce equalities between nodes *)
      (* 1.1. enforce into t *)
      let eq_classes = Constrain_shape.find_eq_classes cs in
      let eqreds = (* naive construction of eqreds for red_equal_cells*)
        List.fold_left
          (fun eqreds eqcl ->
            let ids = Sym_index_set.get_leaves eqcl in
            let i = PSet.choose ids in (* should not fail, #eq_class > 0 *) 
            let ids = PSet.remove i ids in
            PSet.fold (fun j -> PairSet.add (i, j)) ids eqreds
          ) PairSet.empty eq_classes in
      let t1, renaming = D.red_equal_cells eqreds t0 in      
      (* 1.2. make the mapping: old_t -> reduced_t *)
      let map1 = Sym_index_mapping.t_from_renaming renaming in      
      (* 1.3. make the new constrains, here only the cs_map component changes *)
      let cs1 = 
        let cs_map = 
          IntMap.fold
            (fun i0 j0 csm ->
              try
                let i = Sep_selector.mk_leaf i0
                and j = Sep_selector.mk_leaf j0 in
                let xi = Sym_index_map.find i csm
                and xj = Sym_index_map.find j csm in
                assert( xi = xj );
                Sym_index_map.remove i csm
              with Not_found -> error "in enforce: shall not happen (1)")
            renaming cs.Constrain_shape.cs_map in
        let open Constrain_shape in
        { cs_cstr = cs.cs_cstr;
          cs_map = cs_map } in
      (* 2. ... nothing for now. 
       * Equality is the only information we're able to use.
       * Later, we could enforce constrains of the for X.(@o)*|>> Y *)
      (* -) finish the job *)
      let l_t = [ cs1, t1, map1 ] in
      List.filter (fun (cs, t, _) -> not (D.is_bot t)) l_t
        
    (* enforce some constrains which concerns a left value into an element *)
    let enforce_localize
        (i: Constrain_sig.id)
        (cs: Constrain_shape.t) 
        (t: t): (Constrain_shape.t * t * Sym_index_mapping.t) list = 
      todo "enforce_localize"

    (** Analysis control *)
    (* Reduction + node relocalization *)
    let reduce_localize (tl: sym_index tlval) (t: t): t option =
    (*  warn "reduce_localize called at wrong level"; *)
      if is_bot t then None else Some t

    (* Eager reduction *)
    let reduce_eager (t: t): t list = 
    (*  warn "reduce_eager called at wrong level"; *)
      if is_bot t then [] else [ t ]

    (** Transfer functions for the analysis *)
    (* Assignment *)
    let assign (tl: sym_index tlval) (e: sym_index texpr) 
        (t: t) (cstr: Constrain_shape.t): t list result * Constrain_shape.t =
      (* at that level, do nothing with the constrains part *)
      let tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl 
      and e0 = Ast_utils.map_texpr Sep_selector.get_leaf e in
      try
        let tl = D.assign tl0 e0 t in
        Rt_success tl, cstr
      with (* TODO: catch stuffs more precisely *)
      | Exc_asgn_lval _ ->
          Printf.printf "in dom_shape: assign failed ! [ issue on l-val ]\n";
          Rt_failed, cstr
      | Exc_asgn_rval _ ->
          Printf.printf "in dom_shape: assign failed ! [ issue on r-val ]\n";
          Rt_failed, cstr
    (* Condition test *)
    let guard (b: bool) (e: sym_index texpr) 
        (t: t) (cstr: Constrain_shape.t): t list result * Constrain_shape.t = 
      (* at that level, do nothing with the constrains part *)
      let e0 = Ast_utils.map_texpr Sep_selector.get_leaf e in
      try
        let tl = D.guard b e0 t in
        Rt_success tl, cstr
      with
      | Failure mess -> (* TODO: catch stuffs more precisely *)
          Printf.printf "in guard: \"%s\" caught\n" mess;
          Rt_failed, cstr
    (* Checking that a constraint is satisfied; returns over-approx sat *)
    let sat (e: sym_index texpr): t -> bool = 
      let e0 = Ast_utils.map_texpr Sep_selector.get_leaf e in
      D.sat e0
    (* Allocation *)
    let allocate (tl: sym_index tlval) (e: sym_index texpr): t -> t =
      let tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl 
      and e0 = Ast_utils.map_texpr Sep_selector.get_leaf e in
      D.allocate tl0 e0
    (* Deallocation *)
    let deallocate (tl: sym_index tlval): t -> t =
      let tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl in
      D.deallocate tl0

    (** Unfolding, assuming and checking inductive edges *)
    (* Unfold *)
    let ind_unfold (udir: unfold_dir) (tl: sym_index tlval): t -> t list =
      let tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl in
      D.ind_unfold udir tl0
    (* Assume construction *)
    let ind_assume (icall: sym_index tlval gen_ind_call) (tl: sym_index tlval)
        (x: t): t = 
      let icall0 = 
        Dom_utils.map_gen_ind_call 
          (Ast_utils.map_tlval Sep_selector.get_leaf) icall
      and tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl in
      D.ind_assume icall0 tl0 x
    (* Check construction, that an inductive be there *)
    let ind_check (icall: sym_index tlval gen_ind_call) 
        (tl: sym_index tlval) (t: t): bool =
      (* first, checks wether the inductive is allowed *)
      if inductive_is_allowed icall.ic_name then
        let icall0 = 
          Dom_utils.map_gen_ind_call 
            (Ast_utils.map_tlval Sep_selector.get_leaf) icall
        and tl0 = Ast_utils.map_tlval Sep_selector.get_leaf tl in
        D.ind_check icall0 tl0 t
      else false
  end: DOM_SHAPE_IDX)
