(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Huisong Li, Jiangchao Liu,
 **          Pascal Sotin, Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: notes.ml
 ** Xavier Rival *)
(** Dom_sig *)
module type DOM_VALUE
module type DOM_SHAPE
module type DOM_SHAPE_AST
module type DOM_ENV
module type DOM_DISJ

(** Dom_disj *)
module Dom_disj: DOM_ENV -> DOM_DISJ
(** Dom_no_disj *)
module Dom_no_disj: DOM_ENV -> DOM_DISJ

(** Dom_env *)
module Dom_env: DOM_SHAPE_AST -> DOM_ENV

(** Dom_shape_ast! *)
module Dom_shape_ast: DOM_SHAPE -> DOM_SHAPE_AST

(** Dom_shape_base! *)
module Dom_shape_base: DOM_VALUE -> DOM_SHAPE
(** Dom_shape_flat! *)
module Dom_shape_flat: DOM_VALUE -> DOM_SHAPE
(** Dom_shape_prod! *)
module Dom_shape_prod: DOM_SHAPE -> DOM_SHAPE -> DOM_SHAPE
(** Dom_shape_sep! *)
module Dom_shape_sep: DOM_VALUE -> DOM_SHAPE -> DOM_SHAPE -> DOM_SHAPE

(** Dom_val_subm *)
module Make_Val_Subm: DOM_VALUE -> SUBMEM_SIG -> DOM_VALUE




(** Dom_shape_ast_base *)
module Dom_shape_ast_base: DOM_VALUE -> DOM_SHAPE_AST
(** Dom_shape_ast_flat *)
module Dom_shape_flat: DOM_VALUE -> DOM_SHAPE_AST
(** Dom_shape_ast_prod *)
module Dom_shape_prod: DOM_SHAPE_AST -> DOM_SHAPE_AST -> DOM_SHAPE_AST
(** Dom_shape_ast_sep *)
module Dom_shape_sep: DOM_VALUE -> DOM_SHAPE_AST -> DOM_SHAPE_AST -> DOM_SHAPE_AST
