(* to create a valid command line from an array of options *)
let unwind_options opts =
  let protect_string s =
    let n = String.length s in
    assert(n > 0);
    if s.[0] = '\"' && s.[n - 1] = '\"' then
      "'" ^ s ^ "'"
    else s in
  List.fold_left
    (fun acc opt -> acc ^ " " ^ (protect_string opt))
    "" (Array.to_list opts)

