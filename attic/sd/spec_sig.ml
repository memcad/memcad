(* Parsed specification and proof goals *)
type spec =
    { (* all inductive predicates *)
      s_ind_list:  ind list ;
      (* inductives precedence *)
      s_ind_prec:  string list; (* maybe turn into a string to int map *)
      (* binding of types to inductives *)
      s_ind_tbind: string StringMap.t;

      (* TODO: add more fields here:
       * - add some Hoare triples to prove
       * - add some variables definitions if any;
       *   and add a type to each definition
       * - ?
       *)
    }
