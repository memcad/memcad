(*let fresh_map_iargs = fresh_map_args Ntint*)

(* TODO: the functions below are not used...
 *   For now, we do not remove them, but we may if the are indeed useless *)
(*
let fresh_map_colargs  (ck: col_kind) (ill: sv list)
    (ls: le_state): sv list * le_state =
  let col = match ck with
    | CK_set -> Ct_set None
    | CK_seq -> Ct_seq None in
  let lspars, sinj, rg2 =
    List.fold_left
      (fun (acclr, acci, accg) il ->
        let nir, ngr = colv_add_fresh col accg in
        nir :: acclr, Col_utils.CEmb.add nir il ck acci, ngr
      ) ([ ], ls.ls_cur_cvi, ls.ls_cur_r) ill in
  List.rev lspars,
  { ls with
    ls_cur_r = rg2 ;
    ls_cur_cvi = sinj }
let fresh_map_sargs = fresh_map_colargs CK_set
let fresh_map_qargs = fresh_map_colargs CK_seq

let fresh_map_all_args (al: ind_args)  (ls: le_state)
    : sv list * sv list * sv list * sv list * le_state =
  let lppars, ls = fresh_map_args Ntaddr al.ia_ptr ls in
  let lipars, ls = fresh_map_iargs al.ia_int ls in
  let lspars, ls = fresh_map_sargs al.ia_set ls in
  let lqpars, ls = fresh_map_qargs al.ia_seq ls in
  let lspars, lqpars = assert false in
  lppars, lipars, lspars, lqpars, ls
*)
