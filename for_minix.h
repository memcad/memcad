#define NULL ((void *)0)
void _memcad (char*);
void assert (int);
void exit (int);
void prints (const char*, ...);
int printf (const char *, ...);
int printk(const char*, ...);
void std_err (const char *);
int atoi (const char*);
int close(int);
void panic(char*, int);
int read(int, char*, int);
int write(int, char*, int);
