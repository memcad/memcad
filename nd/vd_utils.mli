(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: vd_utils.mli
 **       value domain layers utilities
 ** Xavier Rival, 2022/04/14 *)
open Data_structures
open Sv_def

open Ast_sig
open Col_sig
open Vd_sig

(** Functions to prove constraints satisfied *)
val vsat_empty: vsat

(** Pretty-printing *)
val gen_ind_intp_fpr: (form -> 'a -> unit) -> form -> 'a gen_ind_intp -> unit
val gen_ind_pars_fpr: (form -> 'a -> unit) -> form -> 'a gen_ind_pars -> unit
val gen_ind_call_fpr: (form -> 'a -> unit) -> form -> 'a gen_ind_call -> unit
val colv_fpr: col_kind -> form -> sv -> unit
val colvset_fpr: col_kind -> form -> SvSet.t -> unit
val col_kinds_fpr: form -> col_kinds -> unit
