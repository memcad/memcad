(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Thierry Martinez, Pascal Sotin, Antoine Toubhans, Pippijn
 **          Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sv_def.ml
 **       opaque definition and operations over symbolic variables
 **       signatures of the symbolic variables description
 ** 2020/11/15 *)


(** Symbolic variables *)

(* Type of an SV: what kind of concrete data it represents in the abstract *)
type ntyp =
  | Ntaddr  (* address *)
  | Ntint   (* integer value *)
  | Ntraw   (* raw series of bits, may be e.g., array space *)
  | Ntset   (* set parameter (maybe change type) *)
  | Ntseq   (* sequence parameter *)

(* Abstract type of symbolic variable used throughout the analyzer *)
type sv = int


(** Modules *)

(* Order, sets, maps, generator over SVs *)
module SvOrd =
  struct
    type t = sv
    let compare = Stdlib.compare
    let t_fpr (fmt: Data_structures.form) (o: t): unit =
      Data_structures.F.fprintf fmt "%d" o
  end
module SvGen = Keygen
module SvSet = Data_structures.IntSet
module SvMap = Data_structures.IntMap

(* Order, sets, maps, generator over pairs of SVs *)
module SvPrSet = Data_structures.IntPairSet
module SvPrMap = Data_structures.IntPairMap
module IntSvPrOrd = Data_structures.IntPairOrd
module IntSvPrSet = Data_structures.IntPairSet

(* Order, sets, maps, generator over sets of SVs *)
module IntSvPrSetSet = Data_structures.PairSetSet
module IntSvPrSetMap = Data_structures.PairSetMap

(* A few operations on sets and maps *)
let sv_of_int_set = Fun.id
let sv_of_int_map = Fun.id
let set_fpr = Lib.intset_fpr


(** Refl definition for equality reasoning over types (sv == int) *)
type ('a, 'b) eq = Refl : ('a, 'a) eq
let eq : (sv, int) eq = Refl
