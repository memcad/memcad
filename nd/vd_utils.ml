(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: vd_utils.mli
 **       value domain layers utilities
 ** Xavier Rival, 2022/04/14 *)
open Data_structures
open Sv_def
open Lib

open Ast_sig
open Col_sig
open Vd_sig

open Seq_utils
open Set_utils
open Sv_utils

(** Functions to prove constraints satisfied *)
let vsat_empty: vsat =
  { vs_num = (fun _ -> false);
    vs_set = (fun _ -> false);
    vs_seq = (fun _ -> false) }

(** Pretty-printing *)
let gen_ind_intp_fpr (type a) (f: form -> a -> unit) (fmt: form)
    : a gen_ind_intp -> unit = function
  | Ii_const c -> Format.fprintf fmt "%i" c
  | Ii_lval x -> Format.fprintf fmt "%a" f x
let gen_ind_pars_fpr (type a) (f: form -> a -> unit) (fmt: form)
    (pars: a gen_ind_pars): unit =
  Format.fprintf fmt "( %a | %a | %a | %a )"
    (gen_list_fpr "" f ", ") pars.ic_ptr
    (gen_list_fpr "" (gen_ind_intp_fpr f) ", ") pars.ic_int
    (gen_list_fpr "" Ast_utils.colvar_fpr ", ") pars.ic_set
    (gen_list_fpr "" Ast_utils.colvar_fpr ", ") pars.ic_seq
let gen_ind_call_fpr (type a) (f: form -> a -> unit) (fmt: form)
    (ic: a gen_ind_call): unit =
  match ic.ic_pars with
  | None -> Format.fprintf fmt "%s" ic.ic_name
  | Some pars ->
      Format.fprintf fmt "%s.%a" ic.ic_name
        (gen_ind_pars_fpr f) pars

let colv_fpr (k: col_kind) (fmt: form) (sv: sv): unit =
  match k with
  | CK_set -> F.fprintf fmt "S[%a]" sv_fpr sv
  | CK_seq -> F.fprintf fmt "Q[%a]" sv_fpr sv
let colvset_fpr = function
  | CK_set -> setvset_fpr
  | CK_seq -> seqvset_fpr
let col_kinds_fpr (fmt: form) (c: col_kinds): unit =
  let sets, seqs =
    SvMap.fold
      (fun i k (accset, accseq) ->
        match k with
        | CK_set -> SvSet.add i accset, accseq
        | CK_seq -> accset, SvSet.add i accseq
      ) c (SvSet.empty, SvSet.empty) in
  match sets = SvSet.empty, seqs = SvSet.empty with
  | true, true -> ( )
  | true, false -> seqvset_fpr fmt seqs
  | false, true -> setvset_fpr fmt sets
  | false, false -> F.fprintf fmt "%a; %a" setvset_fpr sets seqvset_fpr seqs
