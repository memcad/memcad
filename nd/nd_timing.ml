(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_timing.ml
 **       Timing of value abstract domains (numeric, set)
 ** Xavier Rival, 2016/07/19 *)
open Sv_def
open Timer

open Nd_sig
open Set_sig

(**  Timing support for numeric domains (layount NUM_NB),
 **  including Apron *)
module Dom_num_nb_timing = functor (Dn: DOM_NUM_NB) ->
  (struct
    module T = Timer.Timer_Mod( struct let name = Dn.module_name end )
    type t = Dn.t
    let module_name = (Dn.module_name)^"_timing"
    let config_fpr = T.app2 "Nb.config_fpr" Dn.config_fpr
    let t_fpri = T.app3 "Nb.t_fpri" Dn.t_fpri
    let top = Dn.top
    let is_bot = T.app1 "Nb.is_bot" Dn.is_bot
    let bound_variable = T.app2 "Nb.bound_variable" Dn.bound_variable
    let vars_srename x y = T.app2 "Nb.vars_srename" Dn.vars_srename x y
    let expand = T.app3 "Nb.expand" Dn.expand
    let compact = T.app3 "Nb.compact" Dn.compact
    let meet = T.app2 "Nb.meet" Dn.meet
    let sv_forget = T.app2 "Nb.forget" Dn.sv_forget
    let simplify_n_expr = T.app1 "Nb.simplify_n_expr" Dn.simplify_n_expr
    let sat = T.app2 "Nb.sat" Dn.sat
    let assign = T.app3 "Nb.assign" Dn.assign
    let guard ?(no_apron=false) = T.app3 "Nb.guard" (Dn.guard ~no_apron)
    let widen = T.app2 "Nb.widen" Dn.widen
    let join = T.app2 "Nb.join" Dn.join
    let nodes_filter = T.app2 "Nb.nodes_filter" Dn.nodes_filter
    let is_le = T.app2 "Nb.is_le" Dn.is_le
    let check_nodes = T.app2 "Nb.check_nodes" Dn.check_nodes
    let get_svs = T.app1 "Nb.get_svs" Dn.get_svs
    let sv_add = T.app2 "Nb.sv_add" Dn.sv_add
    let sv_rem = T.app2 "Nb.sv_rem" Dn.sv_rem
    let get_eq_class = T.app2 "Nb.get_eq_class" Dn.get_eq_class
    let get_upper_svs = T.app2 "get_upper_svs" Dn.get_upper_svs
    let get_lower_svs = T.app2 "get_lower_svs" Dn.get_lower_svs
  end: DOM_NUM_NB)

(** Timing support for set domains *)
module Dom_set_timing = functor (S: DOM_SET) ->
  (struct
    module T = Timer.Timer_Mod( struct let name = "SET" end )
    let module_name = "dom_set_timing"
    let config_fpr = T.app2 "config_fpr" S.config_fpr
    type t = S.t
    let bot = S.bot
    let is_bot = T.app1 "is_bot" S.is_bot
    let top = S.top
    let t_fpri = T.app3 "Nb.t_fpri" S.t_fpri
    let check_nodes ~(svs: SvSet.t) ~(setvs: SvSet.t) =
      T.app1 "check_nodes" (S.check_nodes ~svs ~setvs)
    let sv_add = T.app2 "sv_add" S.sv_add
    let sv_rem = T.app2 "sv_rem" S.sv_rem
    let setv_is_root = T.app2 "setv_is_root" S.setv_is_root
    let setv_col_root = T.app1 "setv_col_root" S.setv_col_root
    let setv_add ?(root = false) ?(kind = None) ?(name = None) =
      T.app2 "setv_add" (S.setv_add ~root ~kind ~name)
    let setv_rem = T.app2 "setv_rem" S.setv_rem
    let is_le = T.app2 "is_le" S.is_le
    let weak_bnd = T.app2 "weak_bnd" S.weak_bnd
    let join = T.app2 "join" S.join
    let set_guard ?(nsat = None) = T.app2 "set_guard" (S.set_guard ~nsat:nsat)
    let set_sat = T.app2 "set_sat" S.set_sat
    let sv_forget = T.app2 "sv_forget" S.sv_forget
    let symvars_srename = T.app4 "symvars_srename" S.symvars_srename
    let sve_sync_top_down = T.app2 "sve_sync_top_down" S.sve_sync_top_down
    let symvars_filter = T.app3 "symvars_filter" S.symvars_filter
  end: DOM_SET)
