(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: svenv_utils.ml
 **       operations on symbolic variables environments
 ** Xavier Rival, 2014/01/12 *)
open Data_structures
open Sv_def

open Svenv_sig

open Nd_utils
open Sv_utils

(** Pretty printing *)
let svenv_mod_fpri (ind: string) (fmt: form) (svm: svenv_mod) : unit =
  let pp fmt (sv, ntyp) = F.fprintf fmt "%a: %a" nsv_fpr sv ntyp_fpr ntyp in
  F.fprintf fmt "%ssvm_add: { %a }\n" ind (PMap.t_fpr "" "; " pp) svm.svm_add;
  F.fprintf fmt "%ssvm_rem: { %a }\n" ind (PSet.t_fpr "; " nsv_fpr) svm.svm_rem;
  F.fprintf fmt "%ssvm_mod: { %a }\n" ind (PSet.t_fpr "; " nsv_fpr) svm.svm_mod

(** Envmod structure *)
(* Generic application of modifications in an svenv_mod *)
let svenv_mod_doit
    (fadd: sv -> ntyp -> 'a -> 'a)
    (frem: sv -> 'a -> 'a)
    (fmod: sv -> 'a -> 'a) (svm: svenv_mod) (x: 'a): 'a =
  let x = PMap.fold fadd svm.svm_add x in
  let x = PSet.fold frem svm.svm_rem x in
  PSet.fold fmod svm.svm_mod x
