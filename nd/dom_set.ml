(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_set.ml
 **       Functor adding a set domain over value properties
 ** Huisong Li, Xavier Rival, 2014/08/12 *)
open Data_structures
open Lib
open Offs
open Sv_def

open Ast_sig
open Nd_sig
open Svenv_sig
open Col_sig
open Seq_sig
open Set_sig
open Vd_sig

open Set_utils
open Col_utils
open Sv_utils


(** Error handling *)
module Log =
  Logger.Make(struct let section = "d_set___" and level = Log_level.DEBUG end)

(* Internal sanity check flag (temporary) *)
let do_check = true

module DBuild =
  functor ( M: DOM_VALUE ) ->
    functor ( S: DOM_SET ) ->
  (struct
    let module_name = "dom_set"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%s -> %s\n%a%a"
        module_name M.module_name module_name S.module_name
        M.config_fpr () S.config_fpr ()

    (** Type of abstract values *)
    (* An abstract element is a conjunction of a value abstraction and of
     * a set abstraction *)
    type t =
        { t_v:       M.t;         (* value information *)
          t_s:       S.t;         (* set information *)
          t_setvs:   SvSet.t;     (* set of all valid setvs *)
          t_info:    colv_info SvMap.t (* maps COLVs to their info SVs *) }

    (** Domain initialization *)
    (* Domain initialization to a set of inductive definitions *)
    let init_inductives: SvGen.t -> StringSet.t -> SvGen.t =
      M.init_inductives

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t =
      { t_v     = M.bot;
        t_s     = S.bot;
        t_setvs = SvSet.empty;
        t_info  = SvMap.empty; }

    (* one of the under domains become bot will result a bot *)
    let is_bot (x: t): bool =
      (M.is_bot x.t_v) || (S.is_bot x.t_s)

    (* Top element *)
    let top: t =
      { t_v     = M.top;
        t_s     = S.top;
        t_setvs = SvSet.empty;
        t_info  = SvMap.empty; }
    (* Pretty-printing *)
    let t_fpri (sn: string SvMap.t) (ind: string) (fmt: form) (x: t): unit =
      F.fprintf fmt "%sSETVs: %a\n%sINFO:\n" ind setvset_fpr x.t_setvs ind;
      SvMap.iter
        (fun setv ci ->
          F.fprintf fmt "%s  %a => %a\n" ind setv_fpr setv colv_info_fpr ci
        ) x.t_info;
      let nind = "  "^ind in
      F.fprintf fmt "%sNumerical Part:\n%a%sSet Part:\n%a" ind
        (M.t_fpri sn nind) x.t_v ind (S.t_fpri sn nind) x.t_s

    (** Management of symbolic variables *)
    (* For sanity check *)
    let check_nodes ~(svs: SvSet.t) ~(colvs: col_kinds) (t: t): bool =
      let expected_setvs =
        SvMap.fold
          (fun colv ck acc ->
            match ck with
            | CK_set -> SvSet.add colv acc
            | CK_seq -> acc
          ) colvs SvSet.empty in
      let missing = SvSet.diff expected_setvs t.t_setvs
      and redundant = SvSet.diff t.t_setvs expected_setvs in
      if missing != SvSet.empty then
        Log.warn "check_nodes, missing SETVs, %a" setvset_fpr missing;
      if redundant != SvSet.empty then
        Log.warn "check_nodes, redundant SETVs, %a" setvset_fpr redundant;
      M.check_nodes svs t.t_v && S.check_nodes ~svs ~setvs:expected_setvs t.t_s

    (** Symbolic variables **)
    (* Add symbolic variables to num or set domain, mark used to choose *
     * where to add symbolic variable *)
    let sv_add ?(mark: bool = true) (i: sv) (x: t): t =
      if mark then { x with t_v = M.sv_add i x.t_v }
      else { x with t_s = snd (S.sv_add i x.t_s) }

    (* Remove symbolic variables *)
    let sv_rem (i: sv) (t: t): t =
      { t with
        t_v = M.sv_rem i t.t_v;
        t_s = S.sv_rem i t.t_s; }

    (** Collection variables **)
    (* add collection variable with the given name *)
    let colv_add
        (ck: col_kind)
        ~(root: bool)
        ?(kind: set_par_type option = None)
        ?(name: string option = None) (i: sv)
        (info: colv_info option) (t: t): t =
      let loc_debug = false in
      if !Flags.flag_dbg_dom_set && loc_debug then
        Log.info "colv_add@set: %a%s" setv_fpr i
          (if root then "(root)" else "");
      match ck with
      | CK_set ->
          if do_check && SvSet.mem i t.t_setvs then
            Log.fatal_exn "setv_add: setv %a is already present" setv_fpr i;
          let t =
            match info with
            | None -> t
            | Some ({min; max; size} as info) ->
                { t with t_info = SvMap.add i info t.t_info }
                |> sv_add min |> sv_add max |> sv_add size in
          let t_s = S.setv_add ~root:root ~kind:kind ~name:name i t.t_s in
          { t with
            t_s     = t_s;
            t_setvs = SvSet.add i t.t_setvs }
      | CK_seq -> Log.fatal_exn "colv_add, CK_seq: no domain for sequences"
    (* removal of a collection variable *)
    let colv_rem (i: sv) (t: t): t =
      if SvSet.mem i t.t_setvs then
        { t with
          t_s     = S.setv_rem i t.t_s;
          t_setvs = SvSet.remove i t.t_setvs;
          t_info  = SvMap.remove i t.t_info; }
      else (* Change, will be seq variables, maybe *)
        begin
          Log.warn "setv_rem: setv S%a is not present" sv_fpr i;
          t
        end
    (* check whether a collection variable is root *)
    let colv_is_root (i: sv) (t: t): bool =
      S.setv_is_root i t.t_s
    (* collect root set variables *)
    let colv_get_roots (t: t): SvSet.t =
      S.setv_col_root t.t_s

    (* The signatures of the functions below is likely to change with sets *)

    (* Renaming (e.g., post join), mark is used to decide rename set domain *
     * or rename both num and set domains *)
    let symvars_srename
        ?(mark: bool = true)
        (om: (Offs.t * sv) OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (opt_colv_map: colv_map_complete option)
        (t: t): t =
      match opt_colv_map with
      | None ->
          let setvs = t.t_setvs in
          let info = colv_info_rename nm opt_colv_map CK_set t.t_info in
          if mark then
            { t_v     = M.symvars_srename om nm t.t_v;
              t_s     = S.symvars_srename om nm None t.t_s;
              t_setvs = setvs;
              t_info  = info }
          else
            { t with
              t_s     = S.symvars_srename om nm None t.t_s;
              t_setvs = setvs }
      | Some { cmc_set = setv_map; cmc_seq = _ } ->
          let info = colv_info_rename nm opt_colv_map CK_set t.t_info in
          let setvs, setv_map =
            SvSet.fold
              (fun i (acc, accm) ->
                try
                  let x, y = SvMap.find i setv_map.sm_map in
                  SvSet.add x (SvSet.union y acc), accm
                with Not_found ->
                  if do_check then Log.warn "unmapped setv: %a" setv_fpr i;
                  acc, { accm with sm_rem = SvSet.add i accm.sm_rem }
              ) t.t_setvs (SvSet.empty, setv_map) in
          if mark then
            { t_v     = M.symvars_srename om nm t.t_v;
              t_s     = S.symvars_srename om nm (Some setv_map) t.t_s;
              t_setvs = setvs;
              t_info  = info }
          else
            { t with
              t_s     = S.symvars_srename om nm (Some setv_map) t.t_s;
              t_setvs = setvs;
              t_info  = info }


    (* Synchronization of the SV environment *)
    let sve_sync_top_down (svm: svenv_mod) (t: t): t =
      { t with
        t_v = M.sve_sync_top_down svm t.t_v;
        t_s = S.sve_sync_top_down svm t.t_s; }

    (* Check the symbolic vars correspond exactly to given set *)
    let symvars_check (s: SvSet.t) (x: t): bool =
      M.symvars_check s x.t_v && true

    (* Removes all symbolic vars and set variables
     * that are not in a given set *)
    let symvars_filter (s: SvSet.t) ?(col_vars: SvSet.t = SvSet.empty)
        (t: t): t =
      { t_v     = M.symvars_filter s t.t_v;
        t_s     = S.symvars_filter s col_vars t.t_s;
        t_setvs = SvSet.inter col_vars t.t_setvs;
        t_info  = SvMap.filter (fun setv _ -> SvSet.mem setv col_vars) t.t_info }

    (* Merging into a new variable, arguments:
     *  . the stride of the structure being treated
     *  . node serving as a base address of a block;
     *  . node serving as a new block abstraction (as a sequence of bits)
     *  . old contents of the block, that is getting merged
     *  . offsets of external pointers into the block to build an
     *    environment *)
    let symvars_merge (stride: int) (base: sv) (sv: sv)
        (block_desc: (Bounds.t * Offs.svo * Offs.size) list)
        (extptrs: OffSet.t) (x: t): t =
      { x with t_v = M.symvars_merge stride base sv block_desc extptrs x.t_v }

    (** Comparison and join operators *)

    (* Comparison *)
    let is_le (x0: t) (x1: t) (sat_diseq: sv -> sv -> bool): bool =
      (M.is_le x0.t_v x1.t_v sat_diseq) && (S.is_le x0.t_s x1.t_s)

    (* Upper bound: serves as join and widening *)
    let upper_bnd ?hint (jk: join_kind) (t0: t) (t1: t): t =
      if do_check && not (SvSet.equal t0.t_setvs t1.t_setvs) then
        Log.fatal_exn "upper_bnd: distinct sets of setvs\n  { %a } != { %a }"
          setvset_fpr t0.t_setvs setvset_fpr t1.t_setvs;
      let s =
        match jk with
        | Jjoin -> S.join t0.t_s t1.t_s
        | Jwiden -> S.weak_bnd t0.t_s t1.t_s
        | _ -> Log.fatal_exn "join kind not implemented" in
      { t_v     = M.upper_bnd jk t0.t_v t1.t_v;
        t_s     = s;
        t_setvs = t0.t_setvs;
        t_info  = t0.t_info}

    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat (x: t) (c: n_cons): bool =
      (* maybe some reduction will be eventually needed *)
      M.sat x.t_v c

    (** Set satisfiability *)
    let set_sat (sc: set_cons) (t: t): bool =
      if do_check then
        begin
          let setvs = set_cons_setvs sc in
          if not (SvSet.subset setvs t.t_setvs) then
            Log.fatal_exn "set_sat, unbound setvs {%a} (avail: {%a}); %a"
              setvset_fpr setvs setvset_fpr t.t_setvs set_cons_fpr sc;
        end;
      S.set_sat sc t.t_s

    (** Condition test *)
    let guard ?(no_apron=false) (b: bool) (c: n_cons) (t: t): t =
      if !Flags.flag_dbg_dom_set then
        Log.info "guard: %b, %a" b Nd_utils.n_cons_fpr c;
      (* maybe some reduction will be eventually needed *)
      { t with t_v = M.guard ~no_apron b c t.t_v }

    (** Set condition test *)
    let set_guard (sc: set_cons) (t: t): t =
      if !Flags.flag_dbg_dom_set then
        Log.info "set_guard,call: %a" set_cons_fpr sc;
      if do_check then
        begin
          let setvs = set_cons_setvs sc in
          if not (SvSet.subset setvs t.t_setvs) then
            Log.fatal_exn "set_guard, unbound setvs {%a} (avail: {%a}); %a"
              setvset_fpr setvs setvset_fpr t.t_setvs set_cons_fpr sc;
        end;
      let nsat = Some (M.sat t.t_v) in
      let t = { t with t_s = S.set_guard ~nsat sc t.t_s; } in
      if !Flags.flag_dbg_dom_set then
        Log.info "set_guard,ret:\n%a" (t_fpri SvMap.empty "  ") t;
      t

    (** Seq satisfiability and condition test *)
    (* Either done at level above, or conservatively ignored *)
    let seq_sat (_: seq_cons) (_: t): bool = false
    let seq_guard (_: seq_cons) (t: t): t = t

    (** Assignment *)
    let assign (i: sv) (e: n_expr) (t: t): t =
      (* remove any information known about i *)
      { t with
        t_v = M.assign i e t.t_v;
        t_s = S.sv_rem i t.t_s }

    (* Assignment inside a sub-memory *)
    let write_sub (sd: sub_dest) (size: int) (rv: n_rval) (t: t): t =
      { t with
        t_v = M.write_sub sd size rv t.t_v ;
        t_s = Log.todo_exn "write_sub" }

    (** Utilities for the abstract domain *)
    let simplify_n_expr (t: t) (e: n_expr): n_expr =
      M.simplify_n_expr t.t_v e

    (** Array domain specific functions *)
    (* Add an array content node *)
    let sv_array_add (i:sv) (size: int) (fields: int list) (t:t): t =
      { t with
        t_v = M.add_array_node i size fields t.t_v;
        t_s = Log.todo_exn "sv_array_add" }
    (* Add an array address node *)
    let sv_array_address_add (id: sv) (t: t): t =
      { t with
        t_v = M.add_array_address id t.t_v;
        t_s = Log.todo_exn "sv_array_add" }
    (* Checks wheter this node is the address of an array node *)
    let is_array_address (id: sv) (t: t): bool =
      M.is_array_address id t.t_v
    (* Dereference an array cell in experision,
     * this function may cause disjunction *)
    let sv_array_deref (id: sv) (off: Offs.t) (t: t): (t * sv) list =
      let vlist = M.array_node_deref id off t.t_v in
      List.map
        (fun (v,i) ->
          { t_v     = v;
            t_s     = Log.todo_exn "sv_array_deref";
            t_setvs = Log.todo_exn "sv_array_deref";
            t_info  = Log.todo_exn "sv_array_deref" }, i
        ) vlist
    (* Dereference an array cell in l-value,
     * no disjunction is created since it merges groups *)
    let sv_array_materialize (id: sv) (off: Offs.t) (t: t): t * sv =
      let v,i = M.array_materialize id off t.t_v in
      { t with
        t_v = v;
        t_s = Log.todo_exn "sv_array_materialize" }, i

    (** Sub-memory specific functions *)
    (* Checks whether a node is of sub-memory type *)
    let is_submem_address (i: sv) (x: t): bool =
      M.is_submem_address i x.t_v
    let is_submem_content (i: sv) (x: t): bool =
      M.is_submem_content i x.t_v

    (* Read of a value inside a submemory block *)
    let submem_read (sat: n_cons -> bool) (addr: sv) (off: Offs.t) (sz: int)
        (x: t): Offs.svo =
      M.submem_read sat addr off sz x.t_v
    let submem_deref (sat: n_cons -> bool) (addr: sv) (off: Offs.t) (sz: int)
        (x: t): Offs.svo =
      M.submem_deref sat addr off sz x.t_v

    (* Localization of a node in a sub-memory *)
    let submem_localize (ig: sv) (x: t): sub_localize =
      M.submem_localize ig x.t_v

    (* Binding of an offset in a sub-memory *)
    let submem_bind (isub: sv) (iglo: sv) (off: Offs.t) (x: t): t * Offs.svo =
      let v, o = M.submem_bind isub iglo off x.t_v in
      { x with t_v = v }, o

    (* Unfolding *)
    let unfold (cont: sv) (n: sv) (udir: unfold_dir) (x: t)
        : (sv SvMap.t * t) list =
      let l = M.unfold cont n udir x.t_v in
      List.map (fun (m, v) -> m, { x with t_v = v }) l

    (* Check predicates on array *)
    let check (op: vcheck_op) (x: t): bool =
      M.check op x.t_v

    (* Assumptions on array *)
    let assume (op: vassume_op) (x: t): t =
      { x with t_v = M.assume op x.t_v }

    (** Summarzing dimensions related operations *)
    (* Expand the constraints on one dimension to another *)
    let expand (oid: sv) (nid: sv) (x: t): t =
      { x with t_v = M.expand oid nid x.t_v }
    (* Upper bound of the constraits of two dimensions *)
    let compact (lid: sv) (rid: sv) (x: t): t =
      { x with t_v = M.compact lid rid x.t_v }
    (* Conjunction *)
    let meet (x0: t) (x1: t): t =
      { x0 with t_v = M.meet x0.t_v x1.t_v }
    (* Forget the information about an SV *)
    let sv_forget (id: sv) (x: t): t =
      { x with t_v = M.sv_forget id x.t_v}
    (* Export of range information *)
    let sv_bound (id: sv) (x: t): interval =
      M.bound_variable id x.t_v
    (* Extract the set of all SVs *)
    let get_svs (x: t): SvSet.t =
      M.get_svs x.t_v
    (* Extract all SVs that are equal to a given SV *)
    let get_eq_class (id: sv) (x: t): SvSet.t =
      M.get_eq_class id x.t_v
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      M.get_upper_svs id x.t_v
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      M.get_lower_svs id x.t_v
  end: DOM_VALCOL)
