(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: set_sig.ml
 **       set domain interface
 ** Huisong Li, Xavier Rival, 2014/08/22 *)
open Data_structures
open Offs
open Sv_def

open Ast_sig
open Nd_sig
open Col_sig
open Svenv_sig


(** Type of set constraints *)
type sid = sv (* SETV (set var id) *)
(* - const: parameters that never change along chains of calls
 * - mono: parameters that can be weakened by taking superset
 * - add:  parameters with additive monoid properties over segments/inds concat
 * - head: parameters describing the sum of SVs that are origin of same
 *         inductive edges in the whole call unfolding *)
type set_par_type =
    { st_const: bool; (* "const":   propagates through inductino *)
      st_add:   bool; (* "add":     additive over segment/inductive comp. *)
      st_head:  bool; (* "head":    set of all the inductive sub-calls roots *)
      st_mono:  bool; (* "montone": can always be weakened to bigger *) }
type set_expr =
  | S_var of sid                       (* E *)
  | S_node of sv                       (* singleton: {\alpha} *)
  | S_uplus of set_expr * set_expr     (* E0 \uplus F1 *)
  | S_union of set_expr * set_expr     (* E0 \union F1 *)
  | S_empty                            (* empty set *)
type set_cons =
  | S_mem of sv * set_expr          (* \alpha0 \in E1 *)
  | S_eq of set_expr * set_expr     (* E0 = E1 *)
  | S_subset of set_expr * set_expr (* E0 \subset E1 *)


module type DOM_SET =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (* Bottom element (empty context) *)
    val bot: t
    val is_bot: t -> bool
    (* Top element (empty context) *)
    val top: t
    (* Pretty-printing, with indentation *)
    val t_fpri: sv_namer -> string -> form -> t -> unit
    (** Management of symbolic variables *)
    (* For sanity check *)
    val check_nodes: svs:SvSet.t -> setvs:SvSet.t -> t -> bool
    (* Symbolic variables *)
    val sv_add: sv -> t -> sv * t
    val sv_rem: sv -> t -> t
    (* check if a set var root *)
    val setv_is_root: sv -> t -> bool
    (* collect root set variables*)
    val setv_col_root: t -> SvSet.t
    (* Set variables *)
    val setv_add: ?root: bool -> ?kind: set_par_type option
      -> ?name: string option -> sv -> t -> t
    val setv_rem: sv -> t -> t
    (** Comparison and join operators *)
    (* Comparison *)
    val is_le: t -> t -> bool
    (* Weak upper bound: widening *)
    val weak_bnd: t -> t -> t
    (* Upper bound: join *)
    val join: t -> t -> t
    (** Set satisfiability *)
    val set_sat: set_cons -> t -> bool
    (** Set condition test *)
    val set_guard: ?nsat:(n_cons -> bool) option -> set_cons -> t -> t
    (** Forget (if the meaning of the sv changes) *)
    val sv_forget: sv -> t -> t (* will be used for assign *)
    (** Renaming (e.g., post join) *)
    val symvars_srename: (Offs.t * sv) OffMap.t -> (sv * Offs.t) node_mapping
      -> colv_mapping option -> t -> t
    (* Synchronization of the SV environment*)
    val sve_sync_top_down: svenv_mod -> t -> t
    (* Removes all symbolic vars that are not in a given set *)
    (* may change a little bit *)
    val symvars_filter: SvSet.t -> SvSet.t -> t -> t
  end
