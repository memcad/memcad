(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_utils.ml
 **       utilities for numerical domain
 ** Xavier Rival, 2011/07/03 *)
open Apron
open Data_structures
open Lib
open Sv_def

open Nd_sig
(* External: Apron components *)

open Sv_utils

(** Imrpovements for later:
 ** - is it possible to generate the widening steps once for all,
 **   for all variables
 ** - packing in the numeric domain **)


(** Error report *)
module Log =
  Logger.Make(struct let section = "nd_uts__" and level = Log_level.DEBUG end)


(** Debugging *)
let loc_debug = false


(** Managing Apron variables *)
let make_var (n: sv): Var.t =
  Var.of_string (F.asprintf "|%a|" sv_fpr n)


(** SV printers *)
(* Generic function for sets *)
let gen_svset_fpr (c: string) (f: form -> sv -> unit) fmt : SvSet.t -> unit =
  let b = ref true in
  SvSet.iter
    (fun i ->
      F.fprintf fmt "%s%a" (if !b then "" else c) f i;
      b := false
    )
let gen_svmap_fpr (c: string) (f: form -> sv -> unit) (g: form -> 'a -> unit)
    (fmt: form): 'a SvMap.t -> unit =
  let b = ref true in
  SvMap.iter
    (fun i x ->
      let locsep = if !b then "" else c in
      b := false;
      F.fprintf fmt "%s%a => %a" locsep f i g x
    )
(* Nodes. *)
let nsv_fpr (fmt: form) (i: sv): unit = F.fprintf fmt "N[%a]" sv_fpr i
(* Sets of nodes *)
let nsvset_fpr : form -> SvSet.t -> unit = gen_svset_fpr ", " nsv_fpr
(* Variables *)
let var_2int (v: Var.t): int =
  let str = Var.to_string v in
  let n = String.length str in
  if n <= 2 then Log.fatal_exn "inadequate variable name: %s" str;
  try int_of_string (String.sub str 1 (n-2))
  with _ -> Log.fatal_exn "non parseable variable name: %s" str
let var_2sv (v: Var.t): sv =
  sv_unsafe_of_int (var_2int v)
let var_fpr (fmt: form) (v: Var.t): unit = nsv_fpr fmt (var_2sv v)
let var_fpr_namer (sn: sv_namer) (fmt: form) (v: Var.t): unit =
  let i = var_2sv v in
  try F.fprintf fmt "%s" (SvMap.find i sn) with Not_found -> nsv_fpr fmt i

(* Turning interval into string *)
let interv_fpr (fmt: form) (i: interval): unit =
  let inf =
    match i.intv_inf with
    | None -> "]-oo"
    | Some a -> Printf.sprintf "[%d" a in
  let sup =
    match i.intv_sup with
    | None -> "+oo["
    | Some a -> Printf.sprintf "%d]" a in
  F.fprintf fmt "%s,%s" inf sup


(** Apron interface helpers *)
(* Turning Apron level 0 structures into strings *)
let coeff_fpr (fmt: form) (c: Coeff.t): unit =
  match c with
  | Coeff.Scalar scal -> F.fprintf fmt "%s" (Scalar.to_string scal)
  | Coeff.Interval _ -> Log.fatal_exn "pp_coeff-interval"
let unop_fpr (fmt: form) (u: Texpr0.unop): unit =
  match u with
  | Texpr0.Neg ->  F.fprintf fmt "-"
  | Texpr0.Cast -> F.fprintf fmt "cast"
  | Texpr0.Sqrt -> F.fprintf fmt "sqrt"
let binop_fpr (fmt: form) (b: Texpr0.binop): unit =
  match b with
  | Texpr0.Add -> F.fprintf fmt "+"
  | Texpr0.Sub -> F.fprintf fmt "-"
  | Texpr0.Mul -> F.fprintf fmt "*"
  | Texpr0.Div -> F.fprintf fmt "/"
  | Texpr0.Mod -> F.fprintf fmt "%c" '%'
  | Texpr0.Pow -> F.fprintf fmt "^"
let cons_op_fpr (fmt: form) (typ: Lincons0.typ): unit =
  match typ with
  | Lincons1.EQ    -> F.fprintf fmt "="
  | Lincons1.DISEQ -> F.fprintf fmt "!="
  | Lincons1.SUP   -> F.fprintf fmt ">"
  | Lincons1.SUPEQ -> F.fprintf fmt ">="
  | Lincons1.EQMOD s -> F.fprintf fmt "==(%s)" (Scalar.to_string s)
let cons_trailer_fpr (fmt: form) (typ: Lincons0.typ): unit =
  match typ with
  | Lincons1.EQ    -> F.fprintf fmt " = 0"
  | Lincons1.DISEQ -> F.fprintf fmt " != 0"
  | Lincons1.SUP   -> F.fprintf fmt " > 0"
  | Lincons1.SUPEQ -> F.fprintf fmt " >= 0"
  | Lincons1.EQMOD s -> F.fprintf fmt " == 0 (%s)" (Scalar.to_string s)
(* Utility to clean Apron structures *)
let iscoeff (c: Coeff.t) (v: float): bool =
  match c with
  | Coeff.Scalar sc ->
      begin
        match sc with
        | Scalar.Float f ->
            abs_float (f -. v) <= 1e-5
        | Scalar.Mpqf  f ->
            abs_float ((Mpqf.to_float f) -. v) <= 1e-5
        | Scalar.Mpfrf f ->
            abs_float ((Mpfrf.to_float ~round:Mpfr.Near f) -. v) <= 1e-5
      end
  | _ -> false
let rec texpr1_clean (e: Texpr1.expr) : Texpr1.expr =
  match e with
  | Texpr1.Unop (Texpr0.Neg, te, a, b) ->
      begin
        match texpr1_clean te with
        | Texpr1.Unop (Texpr0.Neg, te2, _, _) -> te2
        | re                                  ->
            Texpr1.Unop (Texpr0.Neg, re, a, b)
      end
  | Texpr1.Binop (Texpr0.Add, te1, te2, a, b) ->
      begin
        match (texpr1_clean te1, texpr1_clean te2) with
        | (Texpr1.Cst (Coeff.Scalar (Scalar.Float s1)),
           Texpr1.Cst (Coeff.Scalar (Scalar.Float s2))) ->
             Texpr1.Cst (Coeff.Scalar (Scalar.Float (s1 +. s2)))
        | (Texpr1.Cst cf, nte) when iscoeff cf 0.0 -> nte
        | (nte, Texpr1.Cst cf) when iscoeff cf 0.0-> nte
        | (re1, re2) -> Texpr1.Binop (Texpr0.Add, re1, re2, a, b)
      end
  | Texpr1.Binop (Texpr0.Sub, te1, te2, a, b) ->
      begin
        match (texpr1_clean te1, texpr1_clean te2) with
        | (Texpr1.Cst (Coeff.Scalar (Scalar.Float s1)),
           Texpr1.Cst (Coeff.Scalar (Scalar.Float s2))) ->
             Texpr1.Cst (Coeff.Scalar (Scalar.Float (s1 -. s2)))
        | (nte, Texpr1.Cst cf) when iscoeff cf 0.0 -> nte
        | (re1,re2) -> Texpr1.Binop (Texpr0.Sub, re1, re2, a, b)
      end
  | Texpr1.Binop (Texpr0.Mul, te1, te2, a, b) ->
      begin
        match (texpr1_clean te1, texpr1_clean te2) with
        | (Texpr1.Cst (Coeff.Scalar (Scalar.Float s1)),
           Texpr1.Cst (Coeff.Scalar (Scalar.Float s2))) ->
             Texpr1.Cst (Coeff.Scalar (Scalar.Float (s1 *. s2)))
        | (Texpr1.Cst cf, nte) when iscoeff cf 1.0 -> nte
        | (nte, Texpr1.Cst cf) when iscoeff cf 1.0 -> nte
        | (re1,re2) -> Texpr1.Binop (Texpr0.Sub, re1, re2, a, b)
      end
  | Texpr1.Binop (Texpr0.Div, te1, te2, a, b) ->
      begin
        match (texpr1_clean te1, texpr1_clean te2) with
        | (Texpr1.Cst (Coeff.Scalar (Scalar.Float s1)),
           Texpr1.Cst ((Coeff.Scalar (Scalar.Float s2)) as cf)) ->
             if not (iscoeff cf 0.0) then
               Texpr1.Cst (Coeff.Scalar (Scalar.Float (s1 /. s2)))
             else e
        | (nte, Texpr1.Cst cf) when iscoeff cf 1.0 -> nte
        | (re1,re2) -> Texpr1.Binop (Texpr0.Sub, re1, re2, a, b)
      end
  | Texpr1.Unop (op, te, a, b) ->
      Texpr1.Unop (op, texpr1_clean te, a, b)
  | Texpr1.Binop (op, te1, te2, a, b) ->
      Texpr1.Binop (op, texpr1_clean te1, texpr1_clean te2, a, b)
  | _ -> e
(* Turning Apron level 1 structures into strings *)
let texpr1_fpr (sn: sv_namer) (fmt: form) (te: Texpr1.t): unit =
  let rec aux_expr (fmt: form) (e: Texpr1.expr): unit =
    match e with
    | Texpr1.Cst c -> coeff_fpr fmt c
    | Texpr1.Var v -> var_fpr_namer sn fmt v
    | Texpr1.Unop (u, te0, _, _) ->
        F.fprintf fmt "%a (%a)" unop_fpr u aux_expr te0
    | Texpr1.Binop (b, te0, te1, _, _) ->
        F.fprintf fmt "(%a) %a (%a)" aux_expr te0 binop_fpr b aux_expr te1 in
  aux_expr fmt (texpr1_clean (Texpr1.to_expr te))
(* Debug pp for environments *)
let environment_fpr (sn: sv_namer) (fmt: form) (env: Environment.t): unit =
  let ai, af = Environment.vars env in
  assert (Array.length af = 0);
  let isbeg = ref true in
  F.fprintf fmt "[|";
  Array.iter
    (fun (v: Var.t) ->
      let sep =
        if !isbeg then
          begin
            isbeg := false;
            ""
          end
        else "; " in
      F.fprintf fmt "%s%a" sep (var_fpr_namer sn) v
    ) ai;
  F.fprintf fmt "|]"


(* Extraction of integer variables *)
let get_ivars (env: Environment.t): Var.t array =
  let ivars, fvars = Environment.vars env in
  if Array.length fvars > 0 then Log.fatal_exn "unimp: floating-point vars";
  ivars

(* Coefficients *)
let coeff_zero = Coeff.s_of_int 0

(* Extraction of coefficients from constraints *)
let extract_coeff_from_cons (sn: sv_namer) v cons =
  try Lincons1.get_coeff cons v
  with exn ->
    Log.warn "get_coeff, var %a, exn %s"
      (var_fpr_namer sn) v (Printexc.to_string exn);
    Coeff.s_of_int 0

(* Turns a set of octagon constraints into a compact string *)
let octconsarray_fpri (sn: sv_namer) (ind: string)
    (fmt: form) (a: Lincons1.earray): unit =
  (* extraction of the integer variables *)
  let env = a.Lincons1.array_env in
  let ivars = get_ivars env in
  (* extracted octagon structure *)
  let nv = Array.length ivars in
  let oct = Array.make_matrix (2*nv) (2*nv) None in
  (* flag that will be set to true if not oct structure *)
  let notoct = ref false in
  (* matrix structure *)
  let m =
    let m = ref StringMap.empty in
    Array.iteri
      (fun i v -> m := StringMap.add (mk_strpp (var_fpr_namer sn) v) i !m)
      ivars;
    !m in
  let find_var v =
    try StringMap.find (mk_strpp (var_fpr_namer sn) v) m
    with Not_found -> notoct := true ; 0 in
  (* filling the matrix *)
  let ac1 =
    Array.mapi
      (fun i _ -> Lincons1.array_get a i)
      a.Lincons1.lincons0_array in
  Array.iter
    (fun cons ->
      let lcos =
        Array.fold_left
          (fun acc v ->
            let c = extract_coeff_from_cons sn v cons in
            if Coeff.is_zero c then acc
            else (c, v) :: acc
        ) [ ] ivars in
      let lcos =
        List.map
          (fun (c, v) ->
            if Coeff.equal_int c 1 then 1, v
            else if Coeff.equal_int c (-1) then -1, v
            else
              begin
                notoct := true;
                0, v
              end
          ) lcos in
      let cons = mk_strpp coeff_fpr (Lincons1.get_cst cons) in
      match lcos with
      | [ ] -> ( )
      | [ c, v ] ->
          let n = find_var v in
          let i = if c = 1 then n + nv else n in
          oct.(i).(i) <- Some cons
      | [ c0, v0 ; c1, v1 ] ->
          let n0 = find_var v0 and n1 = find_var v1 in
          let i0 = if c0 = 1 then n0 + nv else n0 in
          let i1 = if c1 = 1 then n1 + nv else n1 in
          oct.(i0).(i1) <- Some cons
      | _ -> notoct := true
    ) ac1;
  (* printing the matrix *)
  Array.iter (fun v -> F.fprintf fmt "%.5a " (var_fpr_namer sn) v) ivars;
  Array.iter (fun v -> F.fprintf fmt "%.5a " (var_fpr_namer sn) v) ivars;
  F.fprintf fmt "\n";
  Array.iter
    (fun a ->
      Array.iter
        (function
          | None ->   F.fprintf fmt "  +oo "
          | Some c -> F.fprintf fmt "%.5s " c
        ) a;
      F.fprintf fmt "\n"
    ) oct

(* Turns a set of constraints into a string *)
let linconsarray_fpri (sn: sv_namer) (ind: string) (fmt: form)
    (a: Lincons1.earray)
    : unit =
  (* extraction of the integer variables *)
  let env = a.Lincons1.array_env in
  let ivars = get_ivars env in
  (* pretty-printing of a constraint *)
  let f_lincons (cons: Lincons1.t): unit =
    if Lincons1.is_unsat cons then
      F.fprintf fmt "%sUNSAT\n" ind
    else
      (* print non zero coefficients *)
      let mt = ref false in
      F.fprintf fmt "%s" ind;
      Array.iter
        (fun v ->
          let c = extract_coeff_from_cons sn v cons in
          if not (Coeff.is_zero c) then
            begin
              let var = mk_strpp (var_fpr_namer sn) v in
              let str_ext =
                if iscoeff c 1.0 then F.asprintf "%s" var
                else F.asprintf "%a . %s" coeff_fpr c var in
              if !mt then
                F.fprintf fmt " + %s" str_ext
              else
                begin
                  mt := true;
                  F.fprintf fmt "%s" str_ext
                end
            end
        ) ivars;
      (* print the constant *)
      let cst = Lincons1.get_cst cons in
      let d0 = mk_strpp coeff_fpr cst in
      if !mt then
        if iscoeff cst 0.0 then ()
        else F.fprintf fmt " + %s" d0
      else
        F.fprintf fmt "%s" d0;
      (* print the relation *)
      F.fprintf fmt " %a\n" cons_trailer_fpr (Lincons1.get_typ cons) in
  (* Array of cons1 *)
  let ac1 =
    Array.mapi
      (fun i _ -> Lincons1.array_get a i)
      a.Lincons1.lincons0_array in
  Array.iter f_lincons ac1

(** Numeric domain expressions *)
(* Conversion into strings and pretty-printing *)
let rec n_expr_fpr (fmt: form): n_expr -> unit = function
  | Ne_rand -> F.fprintf fmt "rand"
  | Ne_csti i -> F.fprintf fmt "%d" i
  | Ne_cstc c -> F.fprintf fmt "%c" c
  | Ne_var iv -> F.fprintf fmt "|%a|" nsv_fpr iv
  | Ne_bin (b, e0, e1) ->
      F.fprintf fmt "(%a) %a (%a)" n_expr_fpr e0 binop_fpr b n_expr_fpr e1
let n_cons_fpr (fmt: form): n_cons -> unit = function
  | Nc_rand -> F.fprintf fmt "rand"
  | Nc_bool b -> F.fprintf fmt "%b" b
  | Nc_cons (c, e0, e1) ->
      F.fprintf fmt "%a %a %a" n_expr_fpr e0 cons_op_fpr c n_expr_fpr e1

(* Translation into Apron expressions, with environment *)
let translate_n_var (env: Environment.t) (i: sv): Texpr1.t =
  try Texpr1.var env (make_var i)
  with exn ->
    Log.fatal_exn "Apron,Texpr1.var: %a; %s" nsv_fpr i (Printexc.to_string exn)
let rec translate_n_expr (env: Environment.t): n_expr -> Texpr1.t = function
  | Ne_rand -> Log.fatal_exn "translate_n_expr: rand"
  | Ne_csti i -> Texpr1.cst env (Coeff.s_of_int i)
  | Ne_cstc c -> Texpr1.cst env (Coeff.s_of_int (Char.code c))
  | Ne_var iv -> translate_n_var env iv
  | Ne_bin (b, e0, e1) ->
      Texpr1.binop b (translate_n_expr env e0)
        (translate_n_expr env e1) Texpr1.Int Texpr1.Near
let translate_n_cons (env: Environment.t): n_cons -> Tcons1.t = function
  | Nc_rand -> Log.fatal_exn "translate_n_cons: rand"
  | Nc_bool b ->
      if b then (* tautology constraint 0 = 0 *)
        let ex = translate_n_expr env (Ne_csti 0) in
        Tcons1.make ex Tcons1.EQ
      else (* anti-tautology constraint 1 = 0 *)
        let ex = translate_n_expr env (Ne_csti 1) in
        Tcons1.make ex Tcons1.EQ
  | Nc_cons (c, e0, e1) ->
      (*   e0 (c) e1    is translated into    e0 - e1 (c) 0   *)
      let ex = translate_n_expr env (Ne_bin (Texpr1.Sub, e0, e1)) in
      Tcons1.make ex c

(* Map translations *)
let rec n_expr_map (f: sv -> sv) (e: n_expr) =
  match e with
  | Ne_rand | Ne_cstc _ | Ne_csti _ -> e
  | Ne_var iv -> Ne_var (f iv)
  | Ne_bin (b, e0, e1) -> Ne_bin (b, n_expr_map f e0, n_expr_map f e1)
let n_cons_map (f: sv -> sv) (c: n_cons) =
  match c with
  | Nc_rand | Nc_bool _ -> c
  | Nc_cons (b, e0, e1) -> Nc_cons (b, n_expr_map f e0, n_expr_map f e1)

(* Substitutions *)
let rec n_expr_subst (f: sv -> n_expr) (e: n_expr): n_expr =
  match e with
  | Ne_rand | Ne_cstc _ | Ne_csti _ -> e
  | Ne_var i -> f i
  | Ne_bin (b, e0, e1) -> Ne_bin (b, n_expr_subst f e0, n_expr_subst f e1)
let n_cons_subst (f: sv -> n_expr): n_cons -> n_cons = function
  | (Nc_rand | Nc_bool _) as e -> e
  | Nc_cons (b, e0, e1) -> Nc_cons (b, n_expr_subst f e0, n_expr_subst f e1)

(* Iterators (for searching variables or verifying variables properties) *)
let rec n_expr_fold (f: sv -> 'a -> 'a): n_expr -> 'a -> 'a =
  let rec aux (e: n_expr) (acc: 'a): 'a =
    match e with
    | Ne_rand | Ne_cstc _ | Ne_csti _ -> acc
    | Ne_var iv -> f iv acc
    | Ne_bin (_, e0, e1) -> aux e1 (aux e0 acc) in
  aux
let n_cons_fold (f: sv -> 'a -> 'a) (c: n_cons) (acc: 'a): 'a =
  match c with
  | Nc_rand -> acc
  | Nc_bool _ -> acc
  | Nc_cons (b, e0, e1) -> n_expr_fold f e1 (n_expr_fold f e0 acc)

(* Check absence of Ne_rand *)
let rec n_expr_no_rand (e: n_expr): bool =
  match e with
  | Ne_rand -> false
  | Ne_cstc _ | Ne_csti _ | Ne_var _ -> true
  | Ne_bin (_, e0, e1) -> n_expr_no_rand e0 && n_expr_no_rand e1

(* Negation of an n_cons *)
let neg_n_cons: n_cons -> n_cons option = function
  | Nc_rand -> Some Nc_rand
  | Nc_bool b -> Some (Nc_bool (not b))
  | Nc_cons (Tcons1.EQ, e0, e1) -> Some (Nc_cons (Tcons1.DISEQ, e0, e1))
  | Nc_cons (Tcons1.DISEQ, e0, e1) -> Some (Nc_cons (Tcons1.EQ, e0, e1))
  | Nc_cons (Tcons1.SUP, e0, e1) -> Some (Nc_cons (Tcons1.SUPEQ, e1, e0))
  | Nc_cons (Tcons1.SUPEQ, e0, e1) -> Some (Nc_cons (Tcons1.SUP, e1, e0))
  | c ->
      Log.warn "neg_n_cons, unsupported: %a\n" n_cons_fpr c;
      None

(* Simplification of constraints, and pairs of expressions bound by an equality
 * or disequality constraint *)
let rec n_expr_simplify ((e0, e1): n_expr * n_expr): n_expr * n_expr =
  match e0, e1 with
  | Ne_bin (Texpr1.Mul, Ne_csti i0, ee0),
    Ne_bin (Texpr1.Mul, Ne_csti i1, ee1) ->
      if i0 = i1 then n_expr_simplify (ee0, ee1)
      else e0, e1
  | Ne_bin (Texpr1.Add, Ne_var i0, ee0),
    Ne_bin (Texpr1.Add, Ne_var i1, ee1) ->
      if i0 = i1 then n_expr_simplify (ee0, ee1)
      else if ee0 = ee1 then Ne_var i0, Ne_var i1
      else e0, e1
  | _ -> e0, e1
let n_cons_simplify (c: n_cons): n_cons =
  match c with
  | Nc_cons (Tcons1.DISEQ as op, e0, e1)
  | Nc_cons (Tcons1.EQ    as op, e0, e1) ->
      let ee0, ee1 = n_expr_simplify (e0, e1) in
      if ee0 == e0 && ee1 = e1 then c
      else Nc_cons (op, ee0, ee1)
  | _ -> c

(* Decomposition to turn into a pair (base, offset),
 * when known to be an address
 * (may return None, if no decompisition is found) *)
let n_expr_decomp_base_off (e: n_expr): (sv * n_expr) option =
  match e with
  | Ne_bin (Texpr1.Add, Ne_var i0, e1) -> Some (i0, e1)
  | _ -> None

(** Widening utilities: make constraints for widening thresholds *)
let make_widening_thresholds (env: Environment.t): Lincons1.earray =
  let avi, avf = Environment.vars env in
  assert (Array.length avf = 0);
  let nvars = Array.length avi
  and nthr = IntSet.cardinal !Flags.widen_thresholds in
  let ncons = 2 * nvars * nthr in
  let ea = Lincons1.array_make env ncons in
  let ctr: int ref = ref 0 in
  let push_cstr (lc: Lincons1.t): unit =
    Lincons1.array_set ea !ctr lc ;
    incr ctr in
  Array.iter
    (fun (v: Var.t) ->
      IntSet.iter
        (fun thr ->
          (* we should add two constraints:
           *  - x + c >= 0, i.e., x <= c
           *    x + c >= 0, i.e., x >= -c *)
          (* positive constraint:  - x + c >= 0 *)
          let lep = Linexpr1.make env in
          Linexpr1.set_list lep
            [ Coeff.s_of_int (-1), v ] (Some (Coeff.s_of_int thr));
          let lcp = Lincons1.make lep Lincons1.SUPEQ in
          push_cstr lcp;
          (* negative constraint:    x + c >= 0 *)
          let len = Linexpr1.make env in
          Linexpr1.set_list len
            [ Coeff.s_of_int 1, v ] (Some (Coeff.s_of_int thr));
          let lcn = Lincons1.make len Lincons1.SUPEQ in
          push_cstr lcn;
        ) !Flags.widen_thresholds
    ) avi;
  if loc_debug then
    Log.info "Widening threshold:\n%a" (linconsarray_fpri SvMap.empty "  ") ea;
  ea


(** Mapping functions *)

(* Empty mapping *)
let node_mapping_empty: 'a node_mapping =
  { nm_map    = SvMap.empty;
    nm_rem    = SvSet.empty;
    nm_suboff = (fun _ -> true) }

(* Addition of a new node *)
let add_to_mapping (i: sv) (j: sv) (m: 'a node_mapping)
    : 'a node_mapping =
  let map =
    try
      let k, s = SvMap.find i m.nm_map in
      (* If the mapping already bind i to j, we return it unmodified *)
      if j = k || SvSet.mem j s then m.nm_map
      else
        (* We prefer identity mapping *)
        if i = j then
          SvMap.add i (j, SvSet.add k s) m.nm_map
        else
          SvMap.add i (k, SvSet.add j s) m.nm_map ;
    with
    | Not_found -> SvMap.add i (j, SvSet.empty) m.nm_map in
  { m with
    nm_map = map ;
    nm_rem = SvSet.remove i m.nm_rem }

(* Pretty-printing *)
let node_mapping_fpr (fmt: form) (m: 'a node_mapping): unit =
  F.fprintf fmt "   [[Mapping:\n";
  SvMap.iter
    (fun i (j, s) ->
      F.fprintf fmt "        %a -> %a;%a\n" nsv_fpr i nsv_fpr j nsvset_fpr s
    ) m.nm_map;
  F.fprintf fmt "     Removed: %a]]\n" nsvset_fpr m.nm_rem


(** Decomposition *)

(* Splitting of an expression into a pair formed of a variable
 * and a linear combination of factors *)
exception No_decomp of string
let decomp_lin (e: n_expr): sv * n_expr =
  let rec aux (e: n_expr): sv option * n_expr option =
    match e with
    | Ne_csti _ -> None, Some e
    | Ne_var i -> Some i, None
    | Ne_bin (Texpr1.Add, e0, e1) ->
        let v0, ee0 = aux e0 and v1, ee1 = aux e1 in
        let v =
          match v0, v1 with
          | None, v | v, None -> v
          | _, _ -> raise (No_decomp "decomp_lin: too many vars") in
        let ee =
          match ee0, ee1 with
          | None, ee | ee, None -> ee
          | Some eee0, Some eee1 -> Some (Ne_bin (Texpr1.Add, eee0, eee1)) in
        v, ee
    | Ne_bin (Texpr1.Sub, e0, e1) ->
        let v0, ee0 = aux e0 and v1, ee1 = aux e1 in
        let v =
          match v0, v1 with
          | v, None -> v
          | _, _ -> raise (No_decomp "decomp_lin: too many vars") in
        let ee =
          match ee0, ee1 with
          | ee, None -> ee
          | None, Some ee -> Some (Ne_bin (Texpr1.Sub, Ne_csti 0, ee))
          | Some eee0, Some eee1 -> Some (Ne_bin (Texpr1.Sub, eee0, eee1)) in
        v, ee
    | _ -> None, Some e in
  match aux e with
  | Some v, Some ee -> v, ee
  | Some v, None -> v, Ne_csti 0
  | _, _ -> raise (No_decomp "decomp_lin failed")
let decomp_lin_opt (e: n_expr): (sv * n_expr) option =
  try Some (decomp_lin e)
  with No_decomp _ -> None


(** Atoms used in add_eqs and add_diseqs *)

(* Constraints involve atoms *)
type atom =
  | Acst of int (* stands for integer constant *)
  | Anode of sv (* stands for a graph node *)
(* Ordered structure *)
module AtomOrd =
  struct
    type t = atom
    let compare (a0: atom) (a1: atom): int =
      match a0, a1 with
      | Acst i0, Acst i1 ->
          i0 - i1
      | Anode i0, Anode i1 ->
          sv_compare i0 i1
      | Acst _, Anode _ -> 1
      | Anode _, Acst _ -> -1
  end
module AtomSet = Set.Make( AtomOrd )
module AtomMap = Map.Make( AtomOrd )
(* Pretty-printing *)
let atom_fpr (sn: sv_namer) (fmt: form) (a: atom): unit =
  match a with
  | Acst i -> F.fprintf fmt "%d" i
  | Anode i ->
      try F.fprintf fmt "%s" (SvMap.find i sn)
      with Not_found -> nsv_fpr fmt i
let atomset_fpr (sn: sv_namer) (fmt: form) (s: AtomSet.t): unit =
  let is_beg = ref true in
  AtomSet.iter
    (fun a ->
      let sep =
        if !is_beg then
          begin
            is_beg := false;
            ""
          end
        else "; " in
      F.fprintf fmt "%s%a" sep (atom_fpr sn) a
    ) s
(* Conversion of n_expressions to atom *)
let n_expr_2atom: n_expr -> atom option = function
  | Ne_csti i -> Some (Acst i)
  | Ne_var i -> Some (Anode i)
  | _ -> None
