(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: svenv_utils.ml
 **       operations on symbolic variables environments
 ** Xavier Rival, 2014/01/12 *)
open Data_structures
open Sv_def

open Svenv_sig

(** Pretty printing *)
val svenv_mod_fpri : string -> form -> svenv_mod -> unit

(** Envmod structure *)
(* Generic application of modifications in an svenv_mod *)
val svenv_mod_doit: (sv -> ntyp -> 'a -> 'a) -> (sv -> 'a -> 'a)
  -> (sv -> 'a -> 'a) -> svenv_mod -> 'a -> 'a
