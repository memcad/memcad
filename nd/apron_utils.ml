(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: apron_utils.ml
 **       utilities on abstract syntax trees
 ** Xavier Rival, 2011/05/27 *)
open Data_structures
open Flags
open Lib
open Apron
open Ast_sig
open Nd_sig
open Ast_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "apron_uts_" and level = Log_level.DEBUG end)

(** Utilities on types *)
let field_2off (f: field): Offs.t =
  match f.f_off with
  | None   -> Offs.of_string f.f_name
  | Some i -> Offs.of_field (f.f_name, i)

(** Translation of operators *)
let tr_bin_op: bin_op -> Texpr1.binop = function
  | Badd -> Texpr1.Add
  | Bsub -> Texpr1.Sub
  | Bmul -> Texpr1.Mul
  | Bdiv -> Texpr1.Div
  | Bmod -> Texpr1.Mod
  | b -> Log.fatal_exn "not arithmetic binop: %a" bin_op_fpr b

(** Translation of conditions *)
(* Utilities *)
let texpr_is_rand_cell: 'a texpr -> bool = function
  | Erand, _ -> true
  | _ -> false
let texpr_is_const: 'a texpr -> bool = function
  | Ecst _, _ -> true
  | _ -> false
let make_apron_op: bin_op -> Lincons0.typ = function
  | Bgt -> Tcons1.SUP   (* e0 - e1 > 0 *)
  | Bge -> Tcons1.SUPEQ (* e0 - e1 >= 0 *)
  | Beq -> Tcons1.EQ    (* e0 - e1 = 0 *)
  | Bne -> Tcons1.DISEQ (* e0 - e1 != 0 *)
  | _   -> Log.fatal_exn "impossible branch"
(* Generic function
 * (actually, cannot be used as is in dom_shape_flat *)
let gen_tr_tcond
    (f: 'a -> int texpr -> 'a * n_expr)
    (t: 'a) (c: int texpr): 'a * n_cons =
  match fst c with
  | Ecst (Cint i) -> (* 0: false, otherwise: true *)
      t, Nc_bool (i != 0)
  | Ebin ((Bgt | Bge | Beq | Bne) as op, e0, e1) ->
      if (texpr_is_rand_cell e0 && texpr_is_const e1
        || texpr_is_const e0 && texpr_is_rand_cell e1) then
        t, Nc_rand
      else
        let aop = make_apron_op op in
        let t, te0 = f t e0 in
        let t, te1 = f t e1 in
        t, Nc_cons (aop, te0, te1)
  | Elval _ -> (* amounts to e != 0 *)
      let r, te = f t c in
      let te_zero = Ne_csti 0 in
      t, Nc_cons (Tcons1.DISEQ, te, te_zero)
  | Ebin (Band, e0, e1) -> Log.todo_exn "tr_cond &&"
  | _ -> Log.todo_exn "tr_tcond: %a" itexpr_fpr c
