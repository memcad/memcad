(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_utils.mli
 ** utilities for numerical domain
 ** Xavier Rival, 2011/07/03 *)
open Data_structures
open Lib
open Sv_def

open Nd_sig
(* External: Apron components *)
open Apron

(** Managing Apron variables *)
val make_var: sv -> Var.t

(** SV printers *)
(* Generic function for sets *)
val gen_svset_fpr: string -> (form -> sv -> unit) -> form -> SvSet.t -> unit
val gen_svmap_fpr: string -> (form -> sv -> unit) -> (form -> 'a -> unit)
  ->  form -> 'a SvMap.t -> unit
(* Nodes *)
val nsv_fpr: form -> sv -> unit
(* Sets of nodes *)
val nsvset_fpr: form -> SvSet.t -> unit
(* Variables *)
val var_2int: Var.t -> int
val var_2sv: Var.t -> sv
val var_fpr: form -> Var.t -> unit

(* Turning interval into string *)
val interv_fpr: form -> interval -> unit

(** Apron interface helpers *)
(* Turning Apron level 0 structures into strings *)
val coeff_fpr: form -> Coeff.t -> unit
val unop_fpr:  form -> Texpr0.unop -> unit
val binop_fpr: form -> Texpr0.binop -> unit
(* Turning Apron level 1 structures into strings *)
val texpr1_fpr: sv_namer -> form -> Texpr1.t -> unit
(* Debug pp for environments *)
val environment_fpr: sv_namer -> form -> Environment.t -> unit
(* Turns a set of octagon constraints into a compact string *)
val octconsarray_fpri: sv_namer -> string -> form -> Lincons1.earray -> unit
(* Turns a set of constraints into a string *)
val linconsarray_fpri: sv_namer -> string -> form -> Lincons1.earray -> unit

(** Numeric domain expressions *)
(* Conversion into strings and pretty-printing *)
val n_expr_fpr: form -> n_expr -> unit
val n_cons_fpr: form -> n_cons -> unit
(* Translation into Apron expressions, with environment *)
val translate_n_expr: Environment.t -> n_expr -> Texpr1.t
val translate_n_cons: Environment.t -> n_cons -> Tcons1.t
(* Map translations *)
val n_expr_map: (sv -> sv) -> n_expr -> n_expr
val n_cons_map: (sv -> sv) -> n_cons -> n_cons
(* Substitutions *)
val n_expr_subst: (sv -> n_expr) -> n_expr -> n_expr
val n_cons_subst: (sv -> n_expr) -> n_cons -> n_cons
(* Iterators (for searching variables or verifying variables properties) *)
val n_expr_fold: (sv -> 'a -> 'a) -> n_expr -> 'a -> 'a
val n_cons_fold: (sv -> 'a -> 'a) -> n_cons -> 'a -> 'a
(* Check absence of Ne_rand *)
val n_expr_no_rand: n_expr -> bool
(* Negation of an n_cons *)
val neg_n_cons: n_cons -> n_cons option
(* Simplification of constraints, and pairs of expressions bound by an equality
 * or disequality constraint *)
val n_expr_simplify: n_expr * n_expr -> n_expr * n_expr
val n_cons_simplify: n_cons -> n_cons
(* Decomposition to turn into a pair (base, offset),
 * when known to be an address
 * (may return None, if no decompisition is found) *)
val n_expr_decomp_base_off: n_expr -> (sv * n_expr) option

(** Widening utilities: make constraints for widening thresholds *)
val make_widening_thresholds: Environment.t -> Lincons1.earray

(** Mapping functions *)
(* Empty mapping *)
val node_mapping_empty: 'a node_mapping
(* Addition of a new node *)
val add_to_mapping: sv -> sv -> 'a node_mapping -> 'a node_mapping
(* Pretty-printing *)
val node_mapping_fpr: form -> 'a node_mapping -> unit

(** Decomposition *)
(* Splitting of an expression into a pair formed of a variable
 * and a linear combination of factors *)
val decomp_lin: n_expr -> sv * n_expr
val decomp_lin_opt: n_expr -> (sv * n_expr) option

(** Atoms used in add_eqs and add_diseqs *)
(* Constraints involve atoms *)
type atom =
  | Acst of int (* stands for integer constant *)
  | Anode of sv (* stands for a graph node *)
(* Ordered structure *)
module AtomOrd: (Set.OrderedType with type t = atom)
(* Dictionaries *)
module AtomSet: (Set.S with type elt = atom)
module AtomMap: (Map.S with type key = atom)
(* Pretty-printing *)
val atom_fpr: sv_namer -> form -> atom -> unit
val atomset_fpr: sv_namer -> form -> AtomSet.t -> unit
(* Conversion of n_expressions to atom *)
val n_expr_2atom: n_expr -> atom option
