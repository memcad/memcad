(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: seq_utils.ml
 **       utilities for seq domain
 ** Josselin Giet, 2019/04/12 *)
open Data_structures
open Lib
open Sv_def

open Nd_sig
open Vd_sig
open Ast_sig
open Seq_sig

open Sv_utils


module Log =
  Logger.Make(struct let section = "q_uts___" and level = Log_level.DEBUG end)

(** Print function *)

(* Pretty-printing *)
let seqv_fpr (fmt: form) (sv: sv): unit = F.fprintf fmt "Q[%a]" sv_fpr sv
let seqvset_fpr : form -> SvSet.t -> unit = Nd_utils.gen_svset_fpr ", " seqv_fpr
let seqvmap_fpr (c: string) = Nd_utils.gen_svmap_fpr c seqv_fpr
let rec seq_expr_fpr (fmt: form) (e: seq_expr): unit =
  match e with
  | Seq_empty -> F.fprintf fmt "[ ]"
  | Seq_val v -> F.fprintf fmt "N[%a]" sv_fpr v
  | Seq_var x -> seqv_fpr fmt x
  | Seq_Concat l -> (gen_list_fpr "[ ]" seq_expr_fpr ".") fmt l
  | Seq_sort s -> F.fprintf fmt "sort(%a)" seq_expr_fpr s

let rec seq_expr_mrk_fpr (fmt: form) (e: seq_expr_mrk): unit =
  match e with
  | Seq_mrk_empty -> F.fprintf fmt "[ ]"
  | Seq_mrk_val v -> F.fprintf fmt "N[%a]" sv_fpr v
  | Seq_mrk_var x -> seqv_fpr fmt x
  | Seq_mrk_Concat l -> (gen_list_fpr "[ ]" seq_expr_mrk_fpr ".") fmt l
  | Seq_mrk_sort s -> F.fprintf fmt "sort(%a)" seq_expr_mrk_fpr s
  | Seq_mrk_mrk(s1,s2) -> F.fprintf fmt "(%a,%a)" sv_fpr s1 sv_fpr s2
  | Seq_mrk_mrk_empty -> F.fprintf fmt "( , )"

let rec seq_cons_fpr (fmt: form) (s: seq_cons): unit =
  match s with
  | Seq_equal (s1, s2) ->
      F.fprintf fmt "%a = %a" seq_expr_fpr s1 seq_expr_fpr s2

let rec seq_cons_mrk_fpr (fmt: form) (s: seq_cons_mrk): unit =
  match s with
  | Seq_equal_mrk (s1, s2) ->
      F.fprintf fmt "%a = %a" seq_expr_mrk_fpr s1 seq_expr_mrk_fpr s2

let seq_par_type_fpr (fmt: form) (t: seq_par_type): unit =
  let f b x s = if b then StringSet.add x s else s in
  let s = StringSet.empty
    |> f t.sq_head "Head"
    |> f t.sq_add "Add"
    |> f t.sq_const "Const" in
  F.fprintf fmt "{%a}" (StringSet.t_fpr ";") s
let cut_fpr (fmt: form): cut -> unit = function
  | Val sv -> sv_fpr fmt sv
  | Seq_vars seqvs -> (gen_list_fpr "" seqv_fpr ",") fmt seqvs

let cutted_expr_fpr (fmt: form) (cexpr: cutted_expr): unit =
  (gen_list_fpr "" cut_fpr "|") fmt cexpr


(** Basic utility function *)


(** [make_equality sv sv'] returns the constraint [sv == sv']*)
let make_equality (seqv1: sv) (seqv2: sv) : seq_cons =
  Seq_equal (Seq_var seqv1, Seq_var seqv2)

(** [gen_eq sl ol map] returns [map{ol_i |-> Seq_var sl_i}] *)
let gen_eq (sl: sv list) (ol: sv list) (m: seq_expr SvMap.t) =
  assert (List.length sl = List.length ol);
  List.fold_left2
    (fun acc ii io ->
      SvMap.add io (Seq_var ii) acc
    ) m  sl ol

(** [is_atom expr] returns [true] iff [expr] is an atom i.e. it is not the
 * result of a concatenation *)
let is_atom = function
  | Seq_Concat _ -> false
  | _ -> true

let rec flatten = function
  | Seq_empty -> []
  | Seq_val _ | Seq_var _ as e-> [e]
  | Seq_sort e ->
  begin
    let e = e |> remove_sort |> normalise in
    match e with
    | Seq_empty -> []
    | Seq_val sv -> [Seq_val sv]
    | _ -> [Seq_sort e]
  end
  | Seq_Concat l ->
    List.fold_left (fun acc expr -> acc @ (flatten expr)) [] l

and remove_sort = function
  | Seq_sort e -> remove_sort e
  | Seq_Concat e -> Seq_Concat (List.map remove_sort e)
  | Seq_empty | Seq_val _ | Seq_var _ as e -> e

(** [normalise expr] returns normalised expression equal to [expr]:
  - flatten possibly nested [Seq_Concat]
  - flatten possibly nested [Seq_Sort]
  - [Seq_Concat([])] -> [Seq_empty]
  - [Seq_Concat([x])] -> x (when [x] is [Seq_var], [Seq_val], [Seq_sort])
  - [Seq_sort([])] -> [Seq_empty]
  - [Seq_sort([Ni])] -> [Ni] *)
and normalise expr =
  match flatten expr with
  | [] -> Seq_empty
  | [x] -> x
  | l ->
    assert (List.for_all (fun e -> e <> Seq_empty) l);
    Seq_Concat l

let rec seq_to_seq_mrk (seq: seq_expr) =
  let seq = normalise seq in
  match seq with
  | Seq_empty -> Seq_mrk_empty
  | Seq_var id' -> Seq_mrk_var id'
  | Seq_val sv -> Seq_mrk_val sv
  | Seq_Concat lst -> Seq_mrk_Concat (List.map (fun x -> seq_to_seq_mrk x) lst)
  | Seq_sort sq -> Seq_mrk_sort (seq_to_seq_mrk sq)

(** [seq_cons_to_seq_cons_mrk c] transforms [c] into seq_mrk constraint but
    does not add any marker. *)
let seq_cons_to_seq_cons_mrk = function
  Seq_equal(s1, s2) -> Seq_equal_mrk(seq_to_seq_mrk s1, seq_to_seq_mrk s2)

(** [subst_with_mrk_aux in_sort seq id seqmrk] replaces [id] in [seq] by [seqmrk]
    The auxiliary flag [in_sort] is used internally to ensure such substitution
    does not occur in [Seq_sort] function    *)
let rec subst_with_mrk_aux in_sort (seq: seq_expr) (id: qid) (seqmrk: seq_expr_mrk) =
  let seq = normalise seq in
  match seq with
  | Seq_empty -> Seq_mrk_empty
  | Seq_var id' ->
    if id = id' then
      let _ = if in_sort then
        Log.fatal "invalid inductive defintion (sort inside a complexe seq_expr)"
      in seqmrk
    else Seq_mrk_var id'
  | Seq_val sv -> Seq_mrk_val sv
  | Seq_Concat lst ->
    Seq_mrk_Concat (List.map (fun x -> subst_with_mrk_aux in_sort x id seqmrk) lst)
  | Seq_sort sq -> Seq_mrk_sort (subst_with_mrk_aux true sq id seqmrk)

let rec subst_with_mrk_mrk_aux in_sort (seq: seq_expr_mrk) (id: qid) (seqmrk: seq_expr_mrk) =
  match seq with
  | Seq_mrk_empty -> Seq_mrk_empty
  | Seq_mrk_var id' ->
    if id = id' then
      let _ = if in_sort
        then Log.fatal "invalid inductive defintion (sort inside a complexe seq_expr)"
      in seqmrk
    else Seq_mrk_var id'
  | Seq_mrk_val sv -> Seq_mrk_val sv
  | Seq_mrk_Concat lst ->
    Seq_mrk_Concat (List.map (fun x -> subst_with_mrk_mrk_aux in_sort x id seqmrk) lst)
  | Seq_mrk_sort sq -> Seq_mrk_sort (subst_with_mrk_mrk_aux true sq id seqmrk)
  | Seq_mrk_mrk (q1, q2) -> if (q1 = id) || (q2 = id) then assert false else seq
  | Seq_mrk_mrk_empty -> seq

(** [subst_with_mrk seq id seqmrk] replaces [id] in [seq] by [seqmrk]. *)
let rec subst_with_mrk = (subst_with_mrk_aux false)

(** [subst_with_mrk_mrk seq id seqmrk] replaces [id] in [seq] by [seqmrk]. *)
let rec subst_with_mrk_mrk = (subst_with_mrk_mrk_aux false)

let rec seq_mrk_to_seq seq_expr_mrk =
  match seq_expr_mrk with
  | Seq_mrk_empty -> Seq_empty
  | Seq_mrk_var id' -> Seq_var id'
  | Seq_mrk_val sv -> Seq_val sv
  | Seq_mrk_Concat lst -> Seq_Concat (List.map (fun x -> seq_mrk_to_seq x ) lst)
  | Seq_mrk_sort sq -> Seq_sort (seq_mrk_to_seq sq)
  | Seq_mrk_mrk _ -> assert false
  | Seq_mrk_mrk_empty -> assert false

let seq_cons_subst (s: seq_cons) (id: qid) (seqmrk: seq_expr_mrk) =
  match s with
  | Seq_equal(s1, s2) ->
    Seq_equal_mrk(subst_with_mrk s1 id seqmrk, subst_with_mrk s2 id seqmrk)

let seq_cons_mrk_subst (s: seq_cons_mrk) (id: qid) (seqmrk: seq_expr_mrk) =
  match s with
  | Seq_equal_mrk(s1, s2) ->
    Seq_equal_mrk(subst_with_mrk_mrk s1 id seqmrk, subst_with_mrk_mrk s2 id seqmrk)

let rec subst_seqcons_list_aux (seq: seq_cons_mrk) (l1:qid list) (l2: seq_expr_mrk list) =
  match l1, l2 with
  | h1 :: t1, h2 :: t2 ->
    let s = seq_cons_mrk_subst seq h1 h2 in subst_seqcons_list_aux s t1 t2
  | [], [] -> seq
  | _ -> assert false

(** [subst_seqcons_list seq l_v l_e] returns [seq[v1/e1]...[vn/en]] where
    [l_v = [v1; ...; vn]] and [l_e = [v1; ...; vn]]. *)
let rec subst_seqcons_list (seq: seq_cons) (l1: qid list) (l2: seq_expr_mrk list) =
  match l1, l2 with
  | h1 :: t1, h2 :: t2 ->
    let s = seq_cons_subst seq h1 h2 in subst_seqcons_list_aux s t1 t2
  | [], [] -> seq_cons_to_seq_cons_mrk seq
  | _ -> assert false (**)


(*precondition: seq_expr_mrk is flat*)
(*let rec seq_mrk_split_aux lst (acc: seq_expr list) : seq_expr list * seq_expr list =
  match lst with
    | Seq_mrk_mrk(q1, q2) :: lst' ->
      (acc @ [(Seq_var q1)]), (Seq_var q2 :: List.map seq_mrk_to_seq lst')
    | x :: lst' -> seq_mrk_split_aux lst' (acc @ [seq_mrk_to_seq x])
    | [] -> (acc,[])*)

let rec seq_mrk_split_aux lst (acc: seq_expr list) : seq_expr list * seq_expr list =
  match lst with
    | Seq_mrk_mrk(q1, q2) :: lst' ->
      List.rev (Seq_var q1 :: acc), Seq_var q2 :: List.map seq_mrk_to_seq lst'
    | Seq_mrk_mrk_empty :: lst' ->
      List.rev acc, List.map seq_mrk_to_seq lst'
    | x :: lst' -> seq_mrk_split_aux lst' (seq_mrk_to_seq x :: acc)
    | [] -> (List.rev acc,[])

(*precondition: seq_expr_mrk is flat and the sequence contains at most one Seq_mrk_mrk, otherwise error*)
let seq_mrk_split (seq_expr_mrk: seq_expr_mrk) =
  match seq_expr_mrk with
  | Seq_mrk_empty -> Seq_empty, Seq_empty
  | Seq_mrk_var id' -> Seq_var id', Seq_empty
  | Seq_mrk_val sv -> Seq_val sv, Seq_empty
  | Seq_mrk_Concat lst ->
    let l1, l2 = seq_mrk_split_aux lst [] in
    normalise (Seq_Concat l1), normalise (Seq_Concat l2)
  | Seq_mrk_sort sq -> assert false
  | Seq_mrk_mrk (q1, q2) -> Seq_var q1, Seq_var q2
  | Seq_mrk_mrk_empty -> Seq_empty, Seq_empty

let split = function
  Seq_equal_mrk(s1, s2) ->
    let s1l, s1r = seq_mrk_split s1 in
    let s2l, s2r = seq_mrk_split s2 in
    match s1l, s1r, s2l, s2r with
    (*left-hand side and right-hand side do not contain marker*)
    | s1', Seq_empty, s2', Seq_empty -> [Seq_equal(s1', s2')]
    | s1l', s1r', s2l', s2r' -> [Seq_equal(s1l', s2l'); Seq_equal(s1r', s2r')]


let split_list ls = ls |> List.map_flatten split

let rec remove_empty empty expr =
  let rec aux = function
    | Seq_empty -> Seq_empty
    | Seq_val _ as e -> e
    | Seq_var v when SvSet.mem v empty -> Seq_empty
    | Seq_var _ as e -> e
    | Seq_sort e -> Seq_sort (aux e)
    | Seq_Concat l ->
      let l = List.map aux l in
      match l with
      | [] -> Seq_empty
      | _ -> Seq_Concat l
  in expr |> aux |> normalise


exception SidemostAtomIsSort

(** [empty_leftmost expr] returns [atom, expr'] s.t. [expr = atom·expr'] *)
let empty_leftmost (expr: seq_expr) : seq_expr * seq_expr =
  match flatten expr with
  | [] -> Seq_empty, Seq_empty
  | (Seq_sort _)::_ -> raise SidemostAtomIsSort
  | x::t -> x, normalise (Seq_Concat t)

(** [right_atom_val expr] returns [true] iff the rightmost atomic expression
 * is a value *)
let rec right_atom_val expr =
  let rec get_last = function
    | [] -> None
    | (Seq_sort _)::[]-> raise SidemostAtomIsSort
    | x::[] -> Some x
    | _::q -> get_last q in
  match expr with
  | Seq_sort _
  | Seq_empty
  | Seq_var _ -> false
  | Seq_val _ -> true
  | Seq_Concat l ->
    match get_last l with
    | Some (Seq_val _) -> true
    | _ -> false

  (** [empty_rightmost expr] returns [atom, expr'] s.t. [expr = expr'·atom] *)
let rec empty_rightmost expr =
  let rec aux acc = function
  | [] -> Seq_empty, Seq_empty
  | x::[] -> x, Seq_Concat (List.rev acc) |> normalise
  | x::q -> aux (x::acc) q in
  aux [] @@ flatten expr

(** [empty_sidemost_double b expr1 expr2] returns
    [atom1, expr1', atom2, expr2'] s.t:
    - if [b] is [true] [atom*] are the leftmost,
    - the rightmost, otherwise  *)
let empty_sidemost_double side expr1 expr2 =
  let (atom1, expr1'), (atom2, expr2') =
    if side then
      empty_leftmost expr1,
      empty_leftmost expr2
    else
      empty_rightmost expr1,
      empty_rightmost expr2 in
  atom1, expr1', atom2, expr2'

(* Map translation *)
(** [q_cons_map f g cons] returns the same contraint as cons but apply [f] to
 SETv and [g] to value in [cons] *)
let rec q_expr_map_aux (f: Seq_sig.qid -> seq_expr) (g: sv -> seq_expr)
    (e: seq_expr) =
  match e with
  | Seq_var v -> f v
  | Seq_val v -> g v
  | Seq_empty -> Seq_empty
  | Seq_Concat l ->
      let l = List.map (q_expr_map_aux f g) l in
      Seq_Concat l |> normalise
  | Seq_sort q -> Seq_sort (q_expr_map_aux f g q)

let q_expr_map_aux f g e = q_expr_map_aux f g e |> normalise (*to remove*)

let rec q_cons_map_aux (f: Seq_sig.qid -> seq_expr) (g: sv -> seq_expr)
    (s: seq_cons) =
  match s with
  | Seq_equal (q1, q2) ->
      Seq_equal (q_expr_map_aux f g q1, q_expr_map_aux f g q2)

let q_cons_map (f: Seq_sig.qid -> Seq_sig.qid) (g: sv -> sv) (s: seq_cons) =
  q_cons_map_aux (fun x -> Seq_var (f x)) (fun sv -> Seq_val (g sv)) s
let q_expr_map (f: Seq_sig.qid -> Seq_sig.qid) (g: sv -> sv) (e: seq_expr) =
  q_expr_map_aux (fun x -> Seq_var (f x)) (fun sv -> Seq_val (g sv)) e


(** Set of setv that appear *)
let seq_expr_seqvs, seq_cons_seqvs =
  let rec aux_expr s = function
    | Seq_var v -> SvSet.add v s
    | Seq_val _ | Seq_empty -> s
    | Seq_Concat l -> List.fold_left aux_expr SvSet.empty l
    | Seq_sort q -> aux_expr s q in
  let aux_cons s = function
    | Seq_equal (q1, q2) -> aux_expr (aux_expr s q1) q2 in
  aux_expr SvSet.empty, aux_cons SvSet.empty

(** [seq_expr_seqvs_out_sort expr] returns the set of sequence variables
    occurring in [expr] excepted those under a [Seq_sort] *)
let seq_expr_seqvs_out_sort =
  let rec aux acc = function
  | Seq_val _ | Seq_empty | Seq_sort _ -> acc
  | Seq_var seqv -> SvSet.add seqv acc
  | Seq_Concat l ->
    List.fold_left aux acc l in
  aux SvSet.empty

let seq_expr_svs, seq_cons_svs =
  let rec aux_expr s = function
    | Seq_val v -> SvSet.add v s
    | Seq_var _ | Seq_empty -> s
    | Seq_Concat l -> List.fold_left aux_expr SvSet.empty l
    | Seq_sort q -> aux_expr s q in
  let aux_cons s = function
    | Seq_equal (q1, q2) -> aux_expr (aux_expr s q1) q2 in
  aux_expr SvSet.empty, aux_cons SvSet.empty

let rec simplify_expr expr =
  match expr with
  | Seq_Concat l ->
    let l = List.filter ( (<>) Seq_empty) l in
    Seq_Concat l
  | Seq_sort x -> Seq_sort (simplify_expr x)
  | Seq_empty
  | Seq_var _
  | Seq_val _ -> expr
let simplify_cons cons =
  match cons with
  | Seq_equal (x1, x2) when x1 == x2 || x1 = x2 -> None
  | Seq_equal (x1, x2) -> Some (Seq_equal (simplify_expr x1, simplify_expr x2))

  (** [seq_expr_not_empty expr] returns [true] iff [expr] is a non-empty
      sequence. i.e. it contains at least on value node *)
let rec seq_expr_not_empty = function
  | Seq_val _ -> true
  | Seq_var _ | Seq_empty -> false
  | Seq_Concat l -> List.exists seq_expr_not_empty l
  | Seq_sort e -> seq_expr_not_empty e

  (** [factorize seqv expr expr'] replaces all subsequences equal to [expr] in
    [expr'] by [seqv].
    The boolean returned value is [true]
    iff at least one rewriting has been performed. *)
let factorize seqv expr_sv expr : seq_expr * bool =
  if expr_sv = Seq_empty then expr, false else
  let f_expr = flatten expr in
  let f_expr_sv = flatten expr_sv in
  (** [is_prefix e e'] returns
      - [Some e''] if [e = e'.e'']
      - [None] otherwise *)
  let rec is_prefix exp exp_suff =
    match exp, exp_suff with
    | e , [] -> Some e
    | t::q, t'::q' when t = t' -> is_prefix q q'
    | _ , _ -> None
  in
  let changed = ref false in
  let rec aux expr =
    match expr, is_prefix expr f_expr_sv with
    | [], _ -> Seq_empty
    |  _, Some expr' ->
      let _ = changed := true in
      Seq_Concat [Seq_var seqv; aux expr']
    | t::q, None -> Seq_Concat [t; aux q]
  in
  let res = normalise @@ aux f_expr in
  res, !changed


(** Pruning some SEQVs from a list of seq constraints *)
(* This function should return an equivalent set of constraints, where
 * some SEQVs are removed (it is used for is_le) *)
let seq_cons_prune_seqvs (torem: SvSet.t) (lc: seq_cons list): seq_cons list =
  Log.debug
    "We want to remove the seqvs : {%a}\nIn the following constraints :\n\t%a"
    (SvSet.t_fpr "; ") torem (gen_list_fpr "[]" seq_cons_fpr "\n\t") lc;
  (* 1st heuristic: if we have a containt Q[i] = exp *
   * and we have to remove i, we
   * replace every occurence of i by expr*)
  let find_def_replace (seqv: sv) (lc: seq_cons list) =
    let is_def acc = function
      | Seq_equal (Seq_var i, Seq_sort (Seq_var j))
        when i = seqv && i = j -> acc
      | Seq_equal (Seq_sort (Seq_var j), Seq_var i)
        when i = seqv && i = j -> acc
      | Seq_equal (Seq_var i, expr) when i = seqv -> Some expr
      | Seq_equal (expr, Seq_var i) when i = seqv -> Some expr
      | _ -> acc in
    let expr = List.fold_left is_def None lc in
    match expr with
    | None -> lc
    | Some expr ->
      (* this assertion test that seqv does not occur in expr.
       * It would be cyclic constraint of seqv *)
      let _ = assert (not (SvSet.mem seqv (seq_expr_seqvs expr))) in
      let lc =
        List.map
          (q_cons_map_aux
             (fun v -> if v = seqv then expr else Seq_var v)
             (fun i -> Seq_val i)) lc in
      List.fold_left
        (fun acc cons ->
          match simplify_cons cons with
          | None -> acc
          | Some cons -> cons :: acc)
        [] lc in
  let lc = SvSet.fold find_def_replace torem lc in
  Log.debug "We obtain the following constraints :\n\t%a"
    (gen_list_fpr "[]" seq_cons_fpr "\n\t") lc;
  (* Final sanity check : JG:TODO to remove ....*)
  List.iter
    (fun cons ->
      let set = seq_cons_seqvs cons in
      let inter = SvSet.inter torem set in
      if SvSet.is_empty inter then ()
      else
        Log.fatal "in seqcons : %a\n\t{%a} are not removed"
          seq_cons_fpr cons (SvSet.t_fpr "; ") inter
    ) lc;
  lc

let replace_expr (e: seq_expr) (inst: seq_expr SvMap.t)
    (nmap: sv SvMap.t): seq_expr =
  q_expr_map_aux
    (fun seqv -> SvMap.find seqv inst)
    (fun sv -> Seq_val (SvMap.find sv nmap))
    e

let replace_cons
  (seq_cons: seq_cons list)
  (inst: seq_expr SvMap.t)
  (nmap: sv SvMap.t)
  : seq_cons list =
  List.fold_left
    (fun acc_cons cons ->
      let new_cons = q_cons_map_aux
        (fun seqv -> SvMap.find seqv inst)
        (fun sv -> Seq_val (SvMap.find sv nmap))
        cons in
    new_cons::acc_cons)
  [] seq_cons


let rec is_le_instantiate
  (seq_cons: seq_cons list)
  (inst: seq_expr SvMap.t)
  (nmap: sv SvMap.t)
  : seq_expr SvMap.t
  * seq_cons list =
  let add_def_seq (colv: sv) (def: seq_expr) inst seqcons :
      seq_expr SvMap.t * seq_cons list =
      Log.debug "%a = %a" seqv_fpr colv seq_expr_fpr def;
    if SvMap.mem colv inst then
      let seqcons = Seq_equal (Seq_var colv, def)::seqcons in
      inst, seqcons
    else
    let seqvs = seq_expr_seqvs def in
    if SvSet.for_all (fun seqv -> SvMap.mem seqv inst) seqvs then
      let def = q_expr_map_aux
        (fun seqv -> SvMap.find seqv inst)
        (fun sv -> Seq_val (SvMap.find sv nmap))
        def in
      let inst = SvMap.add colv def inst in
      inst, seqcons
    else
      let seqcons = Seq_equal (Seq_var colv, def)::seqcons in
      inst, seqcons in
  let aux (inst, cons): seq_cons -> (seq_expr SvMap.t * seq_cons list) =
    function
    | Seq_equal (Seq_var i, Seq_var j) ->
      if SvMap.mem i inst
      then add_def_seq j (Seq_var i) inst cons
      else add_def_seq i (Seq_var j) inst cons
    | Seq_equal (Seq_var i, def)
    | Seq_equal (def, Seq_var i) -> add_def_seq i def inst cons
    | Seq_equal (_, _) as con ->
      let seqcons = con::cons in
      inst, seqcons in
  let inst, seq_cons_r = List.fold_left aux (inst, [ ]) seq_cons in
  if List.length seq_cons = List.length seq_cons_r then
    inst, seq_cons
  else
    is_le_instantiate seq_cons_r inst nmap

let check_non_inst (seqctr: seq_cons list) (inst: seq_expr SvMap.t)
    : SvSet.t =
  let f_add i setv map =
    try  SvMap.add i (SvSet.union setv (SvMap.find i map)) map
    with Not_found -> SvMap.add i setv map in
  (* compute the dependence of non instantiated set variables *)
  let a_dep i inst se map =
    if SvMap.mem i inst then map
    else
      let setv = seq_expr_seqvs se in
      f_add i setv map in
  let dep =
    List.fold_left
      (fun acc con ->
        match con with
        | Seq_equal (Seq_var i, Seq_var j) ->
            if SvMap.mem j inst
            then a_dep i inst (Seq_var j) acc
            else a_dep j inst (Seq_var i) acc
        | Seq_equal (Seq_var i, sr) -> a_dep i inst sr acc
        | Seq_equal (sl, Seq_var j) -> a_dep j inst sl acc
        | Seq_equal (sl, sr) -> acc
      ) SvMap.empty seqctr in
  let sc_inst, de_inst =
    SvMap.fold
      (fun k set (accl, accr) ->
         SvSet.add k accl, SvSet.union set accr)
      dep (SvSet.empty, SvSet.empty) in
  let de_inst = SvSet.diff de_inst sc_inst in
  let de_inst =
    SvSet.diff de_inst
      (SvMap.fold (fun k _ acc -> SvSet.add k acc) inst SvSet.empty) in
  if !Flags.flag_dbg_is_le_shape then
    Log.force "Non-Instantiated seq variables: { %a }"
      (SvSet.t_fpr ";") de_inst;
  de_inst

let split_cons_fresh (seqctr: seq_cons list) (seqv: SvSet.t)
    : seq_cons list * seq_cons list =
  List.fold_left
    (fun (accs, acc_f) ele ->
      if SvSet.inter seqv (seq_cons_seqvs ele) = SvSet.empty then
        ele :: accs, acc_f
      else
        accs, ele :: acc_f
    ) ([], []) seqctr

(** Check properties of set parameters *)
let seq_par_type_is_const (st: seq_par_type): bool = st.sq_const
let seq_par_type_is_add (st: seq_par_type): bool = st.sq_add || st.sq_head

(** Information utilities *)
(** [get_cons_len info expr] returns a numerical expression corresponding
    to the size of [expr]. *)
let rec get_expr_len (info: colv_info SvMap.t) (expr: seq_expr) : n_expr =
  match expr with
  | Seq_empty -> Ne_csti 0
  | Seq_val _ -> Ne_csti 1
  | Seq_var seqv ->
    let len = (SvMap.find seqv info).size in
    Ne_var len
  | Seq_Concat ([]) -> Ne_csti 0
  | Seq_Concat (t::q) ->
    let t_len = get_expr_len info t in
    let aux (acc: n_expr) (e: seq_expr) =
      let e_len = get_expr_len info e in
      Ne_bin (Add, acc, e_len) in
    List.fold_left aux t_len q
  | Seq_sort e -> get_expr_len info e
(** [get_cons_len info cons] returns the numerical constraint  *)
let rec get_cons_len (info: colv_info SvMap.t) (cons: seq_cons) : n_cons =
  match cons with
  | Seq_equal (e1, e2) ->
    let e1_len = get_expr_len info e1 in
    let e2_len = get_expr_len info e2 in
    Nc_cons (EQ, e1_len, e2_len)

let rec to_set_expr (e: seq_expr) : Set_sig.set_expr =
  match e with
  | Seq_val sv -> Set_sig.S_node sv
  | Seq_var seqv -> Set_sig.S_var seqv
  | Seq_Concat []
  | Seq_empty -> Set_sig.S_empty
  | Seq_sort e
  | Seq_Concat [e] -> to_set_expr e
  | Seq_Concat (e::l) ->
    let e = to_set_expr e in
    List.fold_left
      (fun acc e_seq ->
        let e_set = to_set_expr e_seq in
        Set_sig.S_uplus (acc, e_set))
      e l

let to_set_cons (c: seq_cons) : Set_sig.set_cons =
  match c with
  | Seq_equal (e1, e2) ->
    let e1 = to_set_expr e1 in
    let e2 = to_set_expr e2 in
    Set_sig.S_eq (e1, e2)

let cut_expr (exprs: seq_expr list): cutted_expr option =
  let add_cut (cut: sv list) (cuts: cutted_expr): cutted_expr =
    if cut = [] then cuts else
    (Seq_vars (List.rev cut))::cuts in
  let rec aux cuts current_cut = function
    | Seq_empty::q -> aux cuts current_cut q
    | Seq_var seqv::q -> aux cuts (seqv::current_cut) q
    | Seq_val sv::q ->
        let cuts = add_cut current_cut cuts in
        aux ((Val sv)::cuts) [] q
    | Seq_Concat l1::l2 -> aux cuts current_cut (l1@l2)
    | Seq_sort _::_ -> assert false
    | [] ->
      add_cut current_cut cuts in
  try aux [] [] exprs |> List.rev |> Option.some
  with _ -> None

module MSet = struct

  type t = {
    mset_seqv : int SvMap.t ;
    mset_sv   : int SvMap.t ;
  }

  let t_fpri (ind: string) (fmt: form) (mset: t) : unit =
    gen_list_fpr ""
      (fun fmt (seqv, n) ->
        Format.fprintf fmt "%a => %i" seqv_fpr seqv n)
      ind
      fmt
      (SvMap.bindings mset.mset_seqv);
    Format.fprintf fmt "\n";
    gen_list_fpr ""
      (fun fmt (sv, n) ->
        Format.fprintf fmt "%a => %i" sv_fpr sv n)
      ind
      fmt
      (SvMap.bindings mset.mset_sv);
    Format.fprintf fmt "\n"

  let empty = { mset_sv = SvMap.empty; mset_seqv = SvMap.empty }

  let is_empty_seqv (mset: t) : bool =
    SvMap.is_empty mset.mset_seqv

  let is_empty_sv (mset: t) : bool =
    SvMap.is_empty mset.mset_sv

  let is_empty (mset: t) : bool =
    SvMap.is_empty mset.mset_sv &&
    SvMap.is_empty mset.mset_seqv

  let mem_seqv (mset: t) (seqv: sv) : int =
    SvMap.find_opt seqv mset.mset_seqv
    |> Option.get_default 0

  let mem_sv (mset: t) (sv: sv) : int =
    SvMap.find_opt sv mset.mset_sv
    |> Option.get_default 0

  let rm_seqv (seqv: sv) (mset: t) : t =
    let nb = SvMap.find seqv mset.mset_seqv in
    let mset_seqv = if nb = 1
      then SvMap.remove seqv mset.mset_seqv
      else SvMap.add seqv (nb -1) mset.mset_seqv in
    { mset with mset_seqv }

  let rec add_expr (e: seq_expr) (mset: t) : t =
    match e with
    | Seq_empty -> mset
    | Seq_val sv ->
      let nb = SvMap.find_opt sv mset.mset_sv |> Option.get_default 0 in
      let mset_sv = SvMap.add sv (nb + 1) mset.mset_sv in
      { mset with mset_sv }
    | Seq_sort e -> add_expr e mset
    | Seq_var seqv ->
      let nb = SvMap.find_opt seqv mset.mset_seqv |> Option.get_default 0 in
      let mset_seqv = SvMap.add seqv (nb + 1) mset.mset_seqv in
      { mset with mset_seqv }
    | Seq_Concat l ->
      List.fold (fun acc e -> add_expr e acc) mset l

  let of_expr (e: seq_expr) =
    add_expr e empty

  let sym_diff (mset1: t) (mset2: t) : t * t =
    let aux (x: int SvMap.t) (y: int SvMap.t) : int SvMap.t =
      SvMap.fold
        ( fun key nb_x acc -> match SvMap.find_opt key y with
          | None -> SvMap.add key nb_x acc
          | Some nb_y when nb_y < nb_x -> SvMap.add key (nb_x - nb_y) acc
          | Some _ -> acc )
        x SvMap.empty in
    { mset_seqv = aux mset1.mset_seqv mset2.mset_seqv ;
      mset_sv   = aux mset1.mset_sv mset2.mset_sv },
    { mset_seqv = aux mset2.mset_seqv mset1.mset_seqv ;
      mset_sv   = aux mset2.mset_sv mset1.mset_sv }

end
