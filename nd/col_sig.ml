(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: col_sig.ml
 **       collection domain interface
 ** Josselin Giet, 2021/10/15 *)
open Data_structures
open Offs
open Sv_def

open Ast_sig
open Nd_sig

(** Support for collection variables in is_le, join algorithms *)
(* Mapping from one join argument into the result
 * - sm_kind:
 *     None means we are looking at SV
 *     Some k corresponds to some kind of COLVs (Set or Seq)
 * - sm_map:
 *     map a COLV into: a COLV + a set of other COLVs
 *     (renaming performed in two steps: first rename; second add equalities);
 * - sm_rem:
 *     COLVs to remove *)
(* JG: add typing information ? *)
type colv_mapping =
    { sm_kind:   col_kind option ;
      sm_map:    (sv * SvSet.t) SvMap.t ;
      sm_rem:    SvSet.t ;  }
type colv_map_complete =
    { cmc_set: colv_mapping;
      cmc_seq: colv_mapping; }

(* Embedding for is_le: one way mapping *)
type colv_emb = (sv, sv * col_kind) Aa_maps.t

(** Embedding for is_le: two ways mapping *)
type colv_embedding_comp = (SvSet.t * col_kind) SvMap.t
type colv_embedding =
    { n_img: (SvSet.t * col_kind) SvMap.t;
      (* direct image: right to left *)
      n_pre: (SvSet.t * col_kind) SvMap.t
      (* pre-image, used to detect siblings, left to right *) }

(** Instantiation work *)
exception Non_inst

(** Dictionaries of collection variables *)
(* Exception to be launched when support for collection required but absent *)
exception Col_unsup
(* Set of collection variables with tag showing their type *)
type col_kinds = col_kind SvMap.t

