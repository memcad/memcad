(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_add_dyn_svenv.ml
 **       Set of symbolic variables is dealt dynamically:
 **       Variable are added in the underlying numerical domain
 **       only when it is required
 ** Antoine Toubhans, 2013/09/30 *)
open Data_structures
open Lib
open Sv_def

open Nd_sig

open Nd_utils
open Sv_utils


module Log =
  Logger.Make(struct let section = "nd_+dsve" and level = Log_level.DEBUG end)

(** Functor *)
module Add_dyn_svenv = functor (D: DOM_NUM_NB) ->
  (struct
    let module_name = "nd_add_dyn_svenv"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()
    type t =
        { t_u: D.t;     (* underlying numerical *)
          t_s: SvSet.t (* not yet added in underlying env *) }
    (* Bottom element *)
    let is_bot (x: t): bool = D.is_bot x.t_u
    (* Top element *)
    let top: t =
      { t_u = D.top;
        t_s = SvSet.empty }
    (* Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) fmt (x: t): unit =
      D.t_fpri sn ind fmt x.t_u;
      if !Flags.flag_dbg_synnumenv then
        F.fprintf fmt "%sNot yet added in env: { %s }\n" ind
	  (SvSet.fold
	     (fun key acc ->
	       let s =
                 try SvMap.find key sn
                 with _ -> mk_strpp nsv_fpr key in
	       if String.length acc < 1 then s
	       else acc^","^s
	     ) x.t_s "")
    (* Variables management *)
    let sv_add (i: sv) (x: t): t =
      { x with
        t_s = SvSet.add i x.t_s }
    let sv_rem (i: sv) (x: t): t =
      if SvSet.mem i x.t_s then { x with t_s = SvSet.remove i x.t_s }
      else { x with t_u = D.sv_rem i x.t_u }
    (* Move a node from t_s component to underlying numerical component *)
    let activate_node (i: sv) (x: t): t =
      if SvSet.mem i x.t_s then
        { t_u = D.sv_add i x.t_u;
          t_s = SvSet.remove i x.t_s }
      else x
    let vars_srename (nr: 'a node_mapping) (x: t): t =
      let map, rem, s, a =
        SvSet.fold
          (fun i (m, r, s, a) ->
            (* If node i (that is not activated yet) has to be renamed... *)
            if SvMap.mem i m then
              let j, sj = SvMap.find i m in
              if SvSet.is_empty sj then
                (* ... we remove i from the mapping and add the image to the
                 * set of un activated svs. *)
                SvMap.remove i m, r, SvSet.add j s, a
              else
                (* If i is mapped to several svs, we add it to the t_u value,
                 * Then the equality constraints between the image svs will
                 * be stored. *)
                m, r, s, SvSet.add i a
            (* Or if it has to be removed, we remove it from nm_rem *)
            else if SvSet.mem i r then
              m, SvSet.remove i r, s, a
            else (* Else it remains non-activated *)
              m, r, SvSet.add i s, a
          ) x.t_s (nr.nm_map, nr.nm_rem, SvSet.empty, SvSet.empty) in
      let t_u = SvSet.fold D.sv_add a x.t_u in
      { t_u = D.vars_srename { nr with
                               nm_map = map;
                               nm_rem = rem } t_u;
        t_s = s }
    let check_nodes (s: SvSet.t) (x: t): bool =
      D.check_nodes (SvSet.diff s x.t_s) x.t_u
    let nodes_filter (nkeep: SvSet.t) (x: t): t =
      { t_u = D.nodes_filter (SvSet.diff nkeep x.t_s) x.t_u;
        t_s = SvSet.inter x.t_s nkeep }

    (** Comparison and Join operators *)
    let is_le (x0: t) (x1: t) (sat_diseq: sv -> sv -> bool): bool =
      try
        let svs_to_remove =
          SvSet.inter (D.get_svs x0.t_u) (SvSet.diff x1.t_s x0.t_s) in
        let x0_u = SvSet.fold D.sv_rem svs_to_remove x0.t_u in
        let x0_u = SvSet.fold D.sv_add (SvSet.diff x0.t_s x1.t_s) x0_u in
        let b = D.is_le x0_u x1.t_u sat_diseq in
        b
      with e -> Log.info "is_le found: exc %s" (Printexc.to_string e); raise e

    let join (x0: t) (x1: t): t =
      let x0_weak = SvSet.fold D.sv_rem (SvSet.diff x1.t_s x0.t_s) x0.t_u
      and x1_weak = SvSet.fold D.sv_rem (SvSet.diff x0.t_s x1.t_s) x1.t_u in
      { t_u = D.join x0_weak x1_weak;
        t_s = SvSet.union x0.t_s x1.t_s }

    let widen (x0: t) (x1: t): t =
      let x0_weak = SvSet.fold D.sv_rem (SvSet.diff x1.t_s x0.t_s) x0.t_u
      and x1_weak = SvSet.fold D.sv_rem (SvSet.diff x0.t_s x1.t_s) x1.t_u in
      { t_u = D.widen x0_weak x1_weak;
        t_s = SvSet.union x0.t_s x1.t_s }

    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat (c: n_cons) (x: t): bool =
      match c with
      | Nc_cons (EQ, Ne_var v1, Ne_var v2) when v1 = v2 -> true
      | Nc_cons (_, Ne_var sv, _) when SvSet.mem sv x.t_s -> false
      | Nc_cons (_, _, Ne_var sv) when SvSet.mem sv x.t_s -> false
      | _ ->
      let x = n_cons_fold activate_node c x in
      D.sat c x.t_u

    (** Transfer functions *)
    let assign (dst: sv) (expr: n_expr) (x: t): t =
      match expr with
      | Ne_rand ->
          if SvSet.mem dst x.t_s then x
          else { x with
                 t_u = D.assign dst Ne_rand x.t_u }
      | _ ->
          let x = activate_node dst x in
          let x = n_expr_fold activate_node expr x in
          { x with
            t_u = D.assign dst expr x.t_u }
    let guard ?(no_apron=false) (b: bool) (c: n_cons) (x: t): t =
      match c with
      | Nc_rand -> x
      | _ ->
          let x = n_cons_fold activate_node c x in
          { x with
            t_u = D.guard ~no_apron b c x.t_u }

    (** Utilities for the abstract domain *)
    let simplify_n_expr (x: t) (e: n_expr): n_expr =
      let x = n_expr_fold activate_node e x in
      D.simplify_n_expr x.t_u e

    (** Summarizing dimensions related operations *)
    (* Expand the constraints on one dimension to another *)
    let expand (id: sv) (nid: sv) (x: t): t =
      if SvSet.mem id x.t_s then { x with t_s = SvSet.add nid x.t_s }
      else { x with t_u = D.expand id nid x.t_u }
    (* Upper bound of the constraits of two dimensions *)
    let compact (lid: sv) (rid: sv) (x: t): t =
      match SvSet.mem lid x.t_s, SvSet.mem rid x.t_s with
      | false,false -> { x with t_u = D.compact lid rid x.t_u }
      | false, true -> { t_u = D.sv_rem lid x.t_u;
                         t_s = SvSet.add lid (SvSet.remove rid x.t_s) }
      |  true,false -> { x with t_u = D.sv_rem rid x.t_u }
      |  true, true -> { x with t_s = SvSet.remove rid x.t_s }

    (** Conjunction *)
    let meet (lx: t) (rx: t): t =
      let lx_strong =
        SvSet.fold D.sv_add (SvSet.diff lx.t_s rx.t_s) lx.t_u
      and rx_strong =
        SvSet.fold D.sv_add (SvSet.diff rx.t_s lx.t_s) rx.t_u in
      { t_u = D.meet lx_strong rx_strong;
        t_s = SvSet.inter lx.t_s rx.t_s }

    (** Forget the information on a dimension *)
    let sv_forget (id: sv) (x: t): t =
      if SvSet.mem id x.t_s then x
      else
        { t_u = D.sv_rem id x.t_u;
          t_s = SvSet.add id x.t_s }

    (** Export of range information *)
    (* the bound of a variable *)
    let bound_variable (dim: sv) (x: t): interval =
      if SvSet.mem dim x.t_s then
        { intv_inf = None;
          intv_sup = None; }
      else
        D.bound_variable dim x.t_u

    (** Extract the set of all SVs *)
    let get_svs (x: t): SvSet.t =
      SvSet.union (D.get_svs x.t_u) x.t_s

    (** Extract all SVs that are equal to a given SV *)
    let get_eq_class (i: sv) (x: t): SvSet.t =
      if SvSet.mem i x.t_s then (SvSet.singleton i)
      else D.get_eq_class i x.t_u
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      if SvSet.mem id x.t_s then (SvSet.singleton id)
      else D.get_upper_svs id x.t_u
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      if SvSet.mem id x.t_s then (SvSet.singleton id)
      else D.get_lower_svs id x.t_u
  end: DOM_NUM_NB)
