(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: apron_utils.mli
 **       utilities on abstract syntax trees
 ** Xavier Rival, 2011/05/27 *)
open Apron
open Ast_sig
open Nd_sig

(** Utilities on types *)
val field_2off: field -> Offs.t

(** Translation of operators *)
(*val tr_uni_op: uni_op -> Texpr1.unop*)
val tr_bin_op: bin_op -> Texpr1.binop

(** Translation of conditions *)
(* Utilities *)
val texpr_is_rand_cell: 'a texpr -> bool
val texpr_is_const: 'a texpr -> bool
val make_apron_op: bin_op -> Lincons0.typ
(* Generic function
 * (actually, cannot be used as is in dom_shape_flat *)
val gen_tr_tcond: ('a -> int texpr -> 'a * n_expr)
  -> 'a -> int texpr -> 'a * n_cons
