(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rilet, Francois Berenger, Huisong Li, Jiangchao Liu,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_set.ml
 **       Functor adding a set domain over letue properties
 ** Josselin Giet, Xavier Rival, 2019/12/09 *)
open Data_structures
open Lib
open Offs
open Sv_def

open Ast_sig
open Nd_sig
open Svenv_sig
open Col_sig
open Set_sig
open Seq_sig
open Vd_sig

open Col_utils
open Seq_utils
open Sv_utils


(** Error handling *)
module Log =
  Logger.Make(struct let section = "seq_empt" and level = Log_level.DEBUG end)

module Equiv = struct

  type t =
    { img: sv SvMap.t ;
      post: SvSet.t SvMap.t }

  let empty : t  =
    { img = SvMap.empty ;
      post = SvMap.empty }

  let fpri (ind: string) (fmt: form) (rel: t): unit =
    SvMap.iter
      (fun i equals_i ->
        if SvSet.cardinal equals_i = 1 then () else
        F.fprintf fmt "%s%a => %a\n" ind seqv_fpr i (SvSet.t_fpr " ~ ") equals_i
      ) rel.post

  (** [get_repr seqv rel] returns
      the representative of class containing [seqv] *)
  let get_repr ({img; _} : t) seqv : sv =
    try SvMap.find seqv img
    with Not_found ->
      Log.fatal_exn "Cannot find representativ of %a" seqv_fpr seqv

  (** [get_all_repr rel] returns
      the set of all class representative in [rel] *)
  let get_all_rep ({post; _} : t) : SvSet.t =
    SvMap.fold
      (fun key _  acc-> SvSet.add key acc)
      post SvSet.empty

  (** [get_eq_class seqv x] returns the equality class containing [seqv] in [x].
      *)
  let get_eq_class (seqv: sv) (x: t): SvSet.t =
    let repr = get_repr x seqv in
    SvMap.find repr x.post

  let are_equiv seqv1 seqv2 ({ img}: t) =
    SvMap.find seqv1 img = SvMap.find seqv2 img

  (** [is_repe seqv rel] returns [true] if [seqv] is a class representative *)
  let is_repr seqv (rel : t) : bool =
    seqv = (get_repr rel seqv)

  (** [assert_wf rel] verify that [rel] satisfy some invariants:
      - ∀ [v] ↦ [repr] ∈ img, [v] ∈ [post(rep)]
      - ∀ [repr] ↦ [class] ∈ [post], ∀ [v] ∈ [class], [img(v)] = [repr] *)
  let assert_wf ?(label = "") ({img; post}: t) : unit =
    let iter_img sv repr =
      match SvMap.find_opt repr post with
      | Some set when SvSet.mem sv set -> ()
      | Some set ->
        Log.fatal_exn "Assert_wf(%s)\n%a --(img)--> %a --(post)--> { %a }"
          label sv_fpr sv sv_fpr repr (SvSet.t_fpr "; ") set
      | None ->
        Log.fatal_exn "Assert_wf(%s)\n%a --(img)--> %a --(post)--> _"
          label sv_fpr sv sv_fpr repr
    in
    let iter_set_post repr set sv =
      match SvMap.find_opt sv img with
      | Some repr' when repr = repr' -> ()
      | Some repr' ->
        Log.fatal_exn "Assert_wf(%s)\n%a --(post)--> { %a } %a --(img)--> %a"
          label sv_fpr repr (SvSet.t_fpr "; ") set sv_fpr sv sv_fpr repr'
      | None ->
        Log.fatal_exn "Assert_wf(%s)\n%a --(post)--> { %a } %a --(img)--> _"
          label sv_fpr repr (SvSet.t_fpr "; ") set sv_fpr sv in
    let iter_post repr set =
      SvSet.iter (iter_set_post repr set) set in
    SvMap.iter iter_img img;
    SvMap.iter iter_post post

  let add_equiv (v1: sv) (v2: sv) ({img; post} as rel: t) : t =
    let mem_v1 = SvMap.mem v1 img in
    let mem_v2 = SvMap.mem v2 img in
    match mem_v1, mem_v2 with
    | true, true ->
        let rep_v1 = SvMap.find v1 img
        and rep_v2 = SvMap.find v2 img in
        if rep_v1 = rep_v2 then
          rel (* If there are already in the same class, we do nothing *)
        else (* otherwise we merge the two classes *)
          let classe =
            SvSet.union (SvMap.find rep_v1 post) (SvMap.find rep_v2 post) in
          let rep = SvSet.min_elt classe in
          let img =
            SvSet.fold (fun v img -> SvMap.add v rep img) classe img in
          let post = post
            |> SvMap.remove rep_v1
            |> SvMap.remove rep_v2
            |> SvMap.add rep classe in
          { img  = img ;
            post = post }
    | true, false ->
        let rep = SvMap.find v1 img in
        let img = SvMap.add v2 rep img in
        let classe = SvMap.find rep post
          |> SvSet.add v2 in
        let post = SvMap.add rep classe post in
        { img = img ;
          post = post }
    | false, true ->
        let rep = SvMap.find v2 img in
        let img = SvMap.add v1 rep img in
        let classe = SvMap.find rep post
          |> SvSet.add v1 in
        let post = SvMap.add rep classe post in
        { img = img ;
          post = post }
    | false, false ->
        let rep = min v1 v2 in
        let img = img
          |> SvMap.add v1 rep
          |> SvMap.add v2 rep in
        let classe = SvSet.of_list [ v1; v2 ] in
        let post = SvMap.add rep classe post in
        { img = img ;
          post = post }

  (** [add v rel] adds [v] in the relation [rel]
      It's alone in its class. *)
  let add (v: sv) ({img; post} : t) : t =
    if !Flags.flag_dbg_dom_seq && SvMap.mem v img then
      Log.fatal_exn "%a is already in relation" sv_fpr v;
    let classe = SvSet.singleton v in
    let img = SvMap.add v v img in
    let post = SvMap.add v classe post in
    {img; post}

  (** [rem sv rel] removes [sv] in [rel] and return the class representant if it
   * is defined *)
  let rem (v: sv) ({img; post} : t) : (sv option) * t =
    if (not @@ SvMap.mem v img) then
      Log.fatal_exn "%a is not in relation" sv_fpr v
    else
      let rep = SvMap.find v img in
      let img = SvMap.remove v img in
      let classe = SvMap.find rep post |> SvSet.remove v in
      if v = rep then
        if SvSet.is_empty classe then
          None, { img = img; post = SvMap.remove v post }
        else
          let new_rep = SvSet.min_elt classe in
          let img =
            SvSet.fold (fun v0 img -> SvMap.add v0 new_rep img) classe img in
          let post = post
            |> SvMap.remove v
            |> SvMap.add new_rep classe in
          Some new_rep, { img = img ; post = post }
      else
        let post = SvMap.add rep classe post in
        Some rep, { img = img ; post = post }

  let rem v rel =
    if !Flags.flag_dbg_dom_seq then begin
      Log.debug "Equiv.rem (%a)\n%a" sv_fpr v (fpri "  PRE   ") rel;
      assert_wf ~label:"Equiv.rem pre" rel;
    end;
    let repr, rel = rem v rel in
    if !Flags.flag_dbg_dom_seq then begin
      Log.debug "Equiv.rem (%a)\n%a" sv_fpr v (fpri "  POST  ") rel;
      assert_wf ~label:"Equiv.rem post" rel;
    end;
    repr, rel

  let join (r1: t) (r2: t) : t =
    let merge_class c1 c2 acc =
      let c_res = SvSet.inter c1 c2 in
      if SvSet.cardinal c_res = 0 then acc else
      let repr = SvSet.min_elt c_res in
      let img = SvSet.fold
        (fun seqv img_acc -> SvMap.add seqv repr img_acc)
        c_res acc.img in
      let post = SvMap.add repr c_res acc.post in
      {img; post}
    in
    SvMap.fold
      (fun seqv1 c1 acc ->
        SvMap.fold
          (fun seqv2 c2 acc -> merge_class c1 c2 acc)
          r2.post acc
      )
      r1.post empty

  let is_le (r1: t) (r2: t) : bool =
    (* since we deals we constraints we have to check that all cnstraints in r2
     * are also in r1 *)
    SvMap.for_all
      (fun key classe ->
        try
          let repr = SvMap.find key r1.img in
          let classe' = SvMap.find repr r1.post in
          SvSet.subset classe classe'
        with _ -> false)
      r2.post

  let rename (f: sv -> sv) (r: t) : t =
    let img, post =
      SvMap.fold
        (fun key classe (img, post) ->
          let classe' = SvSet.map f classe in
          let rep = SvSet.min_elt classe' in
          let post = SvMap.add rep classe' post in
          let img =
            SvSet.fold
              (fun seqv acc -> SvMap.add seqv rep acc)
              classe' img in
          img, post)
        r.post (SvMap.empty, SvMap.empty) in
    { img = img; post = post }
end

module DummyBuild =
  functor (D: DOM_VALCOL) ->
  (struct
    let module_name = "dom_seq"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()

    (** Type for abstract elements *)
    (* In the module, the abstract values are exactly the ones in the
     * underlying domain *)
    type t =
      { mem:           D.t;
        (** The setvalue domain part *)
        seqvs:         SvSet.t;
        (** The set of SEQVs currently declared *)
        root_seqvs:    SvSet.t;
        (** root_SEQV, i.e. those declared by the [decl_seqvars] directive *)
        seq_def:       (seq_expr list) SvMap.t;
        (** a mapping SEQV ↦ definition of the SEQV *)
        node_alias:    sv SvMap.t;
        (** a mapping SV ↦ SEQV s.t. q[SEQV] = N[SV] *)
        info:          colv_info SvMap.t;
        (** a mapping SEQV ↦ {min: size; max}(SEQV) *)
        bounds:        (SvSet.t * SvSet.t) SvMap.t;
        (** a mapping SEQV -> {SV_SET; SV_SET}*)
        empty_seq:     SvSet.t;
        (** the set of SEQV equals to [] *)
        non_empty_seq: SvSet.t;
        (** the SEQV that are not non-empty, i.e. it contains at least one value
         *)
        sorted:        SvSet.t;
        (** The set of sorted sequences *)
        rel:           Equiv.t;
        (** The equivalence relation V/= *)
        new_seqvs:     SvSet.t;
        (** newly added seqvs *)
        need_bnds_sat: bool;
        (** A flag to avoid unnecessary calls to {saturate_bounds}*)
      }

    let init_inductives: SvGen.t -> StringSet.t -> SvGen.t =
      D.init_inductives

    let bot: t =
      { mem           = D.bot;
        seqvs         = SvSet.empty;
        root_seqvs    = SvSet.empty;
        seq_def       = SvMap.empty;
        node_alias    = SvMap.empty;
        info          = SvMap.empty;
        bounds        = SvMap.empty;
        empty_seq     = SvSet.empty;
        non_empty_seq = SvSet.empty;
        sorted        = SvSet.empty;
        rel           = Equiv.empty;
        new_seqvs     = SvSet.empty;
        need_bnds_sat = false; }

    let is_bot: t -> bool = fun {mem; _} -> D.is_bot mem

    let top: t =
      { mem           = D.top;
        seqvs         = SvSet.empty;
        root_seqvs    = SvSet.empty;
        seq_def       = SvMap.empty;
        node_alias    = SvMap.empty;
        info          = SvMap.empty;
        bounds        = SvMap.empty;
        empty_seq     = SvSet.empty;
        non_empty_seq = SvSet.empty;
        sorted        = SvSet.empty;
        rel           = Equiv.empty;
        new_seqvs     = SvSet.empty;
        need_bnds_sat = false; }

    (** [fpri_seq off x] rturns a string containing only the sequnce part
     ** of the abstract value [x] with the offset [off] at each new line *)
    let fpri_seq (ind: string) (fmt: form) (x: t): unit =
      if is_bot x then F.fprintf fmt "%sBOTTOM" ind
      else
        begin
          F.fprintf fmt "%sseqv          = { %a }\n" ind seqvset_fpr x.seqvs;
          F.fprintf fmt "%sroot seqv     = { %a }\n" ind seqvset_fpr
            x.root_seqvs;
          F.fprintf fmt "%sempty_seq     = { %a }\n" ind seqvset_fpr x.empty_seq;
          F.fprintf fmt "%snon_empty_seq = { %a }\n" ind seqvset_fpr
            x.non_empty_seq;
          F.fprintf fmt "%ssorted        = { %a }\n%sequiv:\n%a" ind
            seqvset_fpr x.sorted ind (Equiv.fpri ind) x.rel;
          F.fprintf fmt "%sdefs:\n" ind;
          SvMap.iter
            (fun key defs ->
              List.iter
                (fun expr ->
                  F.fprintf fmt "%s  %a := %a\n"
                    ind seqv_fpr key seq_expr_fpr expr)
                defs
            ) x.seq_def;
      (*
      Format.fprintf fmt "%sinformations:\n" ind;
      SvMap.iter
        (fun key info ->
          F.fprintf fmt "%s  %a |-> %a\n" ind
            seqv_fpr key Col_utils.colv_info_fpr info)
        x.info;
      Format.fprintf fmt "%sbounds:\n" ind;
      SvMap.iter
        (fun key (low, up) ->
          F.fprintf fmt "%s  %a |-> LOW: { %a } UP: { %a }\n" ind
            seqv_fpr key (SvSet.t_fpr "; ") low (SvSet.t_fpr "; ") up)
        x.bounds;
        *)
          Format.fprintf fmt "%salias:\n" ind;
          SvMap.iter
            (fun sv seqv ->
              F.fprintf fmt "%s  N[%a] := %a\n" ind sv_fpr sv seqv_fpr seqv
            ) x.node_alias;
          if !Flags.flag_dbg_dom_seq then
            begin
              Format.fprintf fmt "%sneed_bounds_sat: %b\n" ind x.need_bnds_sat;
              Format.fprintf fmt "%snew_seqvs: %a\n" ind seqvset_fpr x.new_seqvs
            end
      end
    let t_fpri (svn: sv_namer) (ind: string) (fmt: form) (x: t): unit =
      if is_bot x then F.fprintf fmt "%sBOTTOM" ind
      else
        let new_ind = ind^"  " in
        Format.fprintf fmt "%a%sSequence Part:\n%a"
          (D.t_fpri svn ind) x.mem
          ind
          (fpri_seq new_ind) x

    (** [assert_wf x] verify that [x] satisfy some invariants :
      - the relation is well formed as well
      - All seqv occuring in defs are class representative of the relation
      - All alias are folded. i.e: if [sv] ↦ [seqv] ∈ [alias],
        then [sv] should not occur in definition (except the one of the alias)
      - There is no definition [Qi := Qj] (they should be in the relation).
      - If ter is a definition [Qi := vj], then vj should have Qj for alias.
      - All seqvs have information and no informtion variable should repeat.
        (this is not true for use in list domain) *)
    let assert_wf ?(label="") (x: t) : unit =
      if is_bot x then () else
      let sv_support = D.get_svs x.mem in
      let wf_def seqv (expr: seq_expr) =
        let _ = match expr with
        | Seq_var seqv' ->
          Log.fatal_exn
            "%s\nConstraint %a := %a should be in rel not in seq_def"
            label seqv_fpr seqv seqv_fpr seqv'
        | Seq_val v when not @@ SvMap.mem v x.node_alias ->
          Log.fatal_exn
            "%s\nDefinition %a := N[%a] should also be in alias"
            label seqv_fpr seqv sv_fpr v
        | _ -> () in
        let seqvs = seq_expr_seqvs expr in
        if SvSet.mem seqv seqvs then
          Log.fatal_exn "%s\n Constraint %a := %a is cyclic"
            label seqv_fpr seqv seq_expr_fpr expr;
        let seqvs = SvSet.add seqv seqvs in
        let svs = seq_expr_svs expr in
        SvSet.iter
          (fun seqv' -> if Equiv.is_repr seqv' x.rel then () else
            Log.fatal_exn
              "%s\nin definition %a := %a, %a is not a class representative "
              label seqv_fpr seqv seq_expr_fpr expr sv_fpr seqv')
          seqvs;
        SvSet.iter
          (fun sv ->
            match SvMap.find_opt sv x.node_alias with
            | None -> ()
            (* This pattern excludes alias definitions *)
            | Some seqv' when
                seqv = seqv'
                && SvSet.cardinal svs = 1
                && SvSet.cardinal seqvs = 1 -> ()
            | Some seqv' ->
                Log.fatal_exn "%s\nin definition %a := %a, %a has for alias %a"
                  label
                  seqv_fpr seqv seq_expr_fpr expr
                  sv_fpr sv seqv_fpr seqv')
          svs in
      let iter_defs seqv defs =
        if defs = [] then
          Log.fatal_exn "%s\nDefinitions of %a is an empty list"
            label seqv_fpr seqv;
          List.iter (wf_def seqv) defs in
      let iter_alias sv seqv =
        let defs = SvMap.find_opt seqv x.seq_def |> Option.get_default [] in
        if not @@ Equiv.is_repr seqv x.rel then
          Log.fatal_exn
            "%s\nin alias N[%a] := %a, %a is not a class representative "
            label sv_fpr sv seqv_fpr seqv seqv_fpr seqv
        else if not @@ List.exists ( (=) (Seq_val sv)) defs then
          Log.fatal_exn
            "%s\n in alias N[%a] := %a, N[%a] is not a definition of %a"
            label sv_fpr sv seqv_fpr seqv sv_fpr sv seqv_fpr seqv
        else () in
      let check_info x =
        if SvMap.is_empty x.info then () else
        (* JG: this trick is to discriminate use of of seq_dom in list domain *)
        let add_disjoint info set =
          if SvSet.mem info set then
            Log.fatal_exn "%s\ninformation parameter %a used several times"
              label sv_fpr info;
          SvSet.add info set in
        let sv_support = D.get_svs x.mem in
        let support, _ = SvMap.fold
          (fun key ({min; max; size}) (acc_seqv, acc_info) ->
            if not @@ SvSet.mem min sv_support then
              Log.fatal_exn
                "%s\nmin info of %a: %a is not in sv_support : { %a }"
                label seqv_fpr key sv_fpr min (SvSet.t_fpr "; ") sv_support;
            if not @@ SvSet.mem size sv_support then
              Log.fatal_exn
                "%s\nsize info of %a: %a is not in sv_support : { %a }"
                label seqv_fpr key sv_fpr size (SvSet.t_fpr "; ") sv_support;
            if not @@ SvSet.mem max sv_support then
              Log.fatal_exn
                "%s\nmax info of %a: %a is not in sv_support : { %a }"
                label seqv_fpr key sv_fpr max (SvSet.t_fpr "; ") sv_support;
            SvSet.add key acc_seqv,
            acc_info
            |> add_disjoint min |> add_disjoint max |> add_disjoint size)
          x.info SvSet.(empty, empty) in
        if not @@ SvSet.equal support x.seqvs then
          Log.fatal_exn
            "%s\nSupport of info and seqvs do not coincide\n{ %a } <> { %a }"
            label
            (SvSet.t_fpr "; ") support
            (SvSet.t_fpr "; ") x.seqvs in
      let check_bounds x =
        let support = SvMap.fold
          (fun key (low, up) acc ->
            if not @@ SvSet.subset low sv_support then
              Log.fatal_exn
                "%s\ninconsistent low bound of %a: { %a } !⊆ { %a }"
                label seqv_fpr key
                (SvSet.t_fpr "; ") low
                (SvSet.t_fpr "; ") sv_support;
            if not @@ SvSet.subset up sv_support then
              Log.fatal_exn
                "%s\ninconsistent up bound of %a: { %a } !⊆ { %a }"
                label seqv_fpr key
                (SvSet.t_fpr "; ") up
                (SvSet.t_fpr "; ") sv_support;
            SvSet.add key acc)
          x.bounds SvSet.empty in
        if not @@ SvSet.equal support x.seqvs then
          Log.fatal_exn
            "%s\nSupport of bounds and seqvs do not coincide\n{ %a } <> { %a }"
            label
            (SvSet.t_fpr "; ") support
            (SvSet.t_fpr "; ") x.seqvs in
      let check_len x =
        if SvMap.is_empty x.info || not !Flags.enable_colv_len then () else
        let seqvs = SvSet.diff x.seqvs x.new_seqvs in
        SvSet.iter
          (fun seqv ->
            let { size } = SvMap.find seqv x.info in
            let cons = Nc_cons (SUPEQ, Ne_var size, Ne_csti 0) in
            if not @@ D.sat x.mem cons then
              Log.fatal_exn "%s\n size of %a (%a) is not positive"
              label seqv_fpr seqv sv_fpr size)
          seqvs in
      let () =
        if SvSet.is_empty x.empty_seq then () else
        let seqv = SvSet.choose x.empty_seq in
        let equals = Equiv.get_eq_class seqv x.rel in
        if not @@ SvSet.equal equals x.empty_seq then
          Log.fatal_exn "%s\nempty_seq { %a } not consistent with rel { %a }"
            label seqvset_fpr x.empty_seq seqvset_fpr equals in
      Equiv.assert_wf ~label x.rel;
      SvMap.iter iter_defs x.seq_def;
      SvMap.iter iter_alias x.node_alias;
      check_info x;
      check_bounds x;
      check_len x

    let assert_wf ?(label="") (x:t) =
      try assert_wf ~label x
      with exn ->
        Log.fatal "%a" (fpri_seq "  ") x;
        Printexc.(raise_with_backtrace exn
        @@ get_raw_backtrace ())

    (** [assert_wf_bin x1 x2] verify that [x1] & [x2] satisfy some invariants:
        - both should be well formed,
        - they should have the same set of root seqvs
        - they should have the same set of seqv *
        - all information variables should coincide between [x1] and [x2] *)
    let assert_wf_bin ?(label="") (x1: t) (x2: t): unit =
      if is_bot x1 || is_bot x2 then () else begin
      assert_wf ~label:(label^" left part") x1;
      assert_wf ~label:(label^" right part") x2;
      if not @@ SvSet.equal x1.root_seqvs x2.root_seqvs then
        Log.fatal_exn
          "%s\nSupport of root seqvs do not coincide\n{ %a } <> { %a }"
          label
            (SvSet.t_fpr "; ") x1.root_seqvs
            (SvSet.t_fpr "; ") x2.root_seqvs;
      if SvMap.is_empty x1.info && SvMap.is_empty x2.info then () else
      (* JG: this trick is to discriminate use of of seq_dom in list domain *)
      if not @@ SvSet.equal x1.seqvs x2.seqvs then
        Log.fatal_exn
          "%s\nSupport of seqvs do not coincide\n{ %a } <> { %a }"
          label
            (SvSet.t_fpr "; ") x1.seqvs
            (SvSet.t_fpr "; ") x2.seqvs;
      SvMap.iter
        (fun key info1 ->
          let info2 = SvMap.find key x2.info in
          if info1 <> info2 then
            Log.fatal_exn
              "%s\ninfo of Q[%a] are not equals:\n  { %a } <> { %a }"
              label sv_fpr key
              Col_utils.colv_info_fpr info1
              Col_utils.colv_info_fpr info2)
        x1.info
      end

    (** [match_info x1 x2] rewrites [x1] and [x2] to have matching information.
        When we use {symvars_srename} we may incorrectly rename info variable,
        since the correct one is equal to the cosen one. *)
    let match_info (x1: t) (x2: t) : t * t =
      if SvMap.is_empty x1.info && SvMap.is_empty x2.info then x1, x2 else
      let find_equal seqv sv1 sv2 =
        if sv1 = sv2 then sv1 else
        let svs1 = D.get_eq_class sv1 x1.mem in
        let svs2 = D.get_eq_class sv2 x2.mem in
        let svs = SvSet.inter svs1 svs2 in
        let _ = if SvSet.is_empty svs then
          Log.fatal_exn "Connot fid common information for %a" seqv_fpr seqv in
        SvSet.choose svs in
      let info = SvSet.fold
        (fun seqv info ->
          let info1 = SvMap.find seqv x1.info in
          let info2 = SvMap.find seqv x2.info in
          let min = find_equal seqv info1.min info2.min in
          let max = find_equal seqv info1.max info2.max in
          let size = find_equal seqv info1.size info2.size in
          SvMap.add seqv {min; max; size} info)
        x1.seqvs
        SvMap.empty in
      { x1 with info}, {x2 with info}


    (** [is_extremum x sv] returns [true] iff. [sv] is the min/max value
        of some colv in [x]. *)
    let is_extremum (x: t) (sv: sv) =
      SvMap.exists
        (fun _ {min; max; _ } -> sv = min || sv = max )
        x.info

    (** [empty_new_seqvs x]:
        - for each Q ∈ [x.new_seqvs], it assumes the constraint [size(Q) ≥ 0]
        - set [x.new_seqvs] to the empty set.

      {b Attention} Do not call this function from [sat] or [is_le] since the
      modification may be lost and numerical [guard] are too expansive to be
      performed superfluously *)
    let empty_new_seqvs (x: t) : t =
      let aux seqv acc =
        let { size } = SvMap.find seqv x.info in
        let cons = Nc_cons (SUPEQ, Ne_var size, Ne_csti 0) in
        D.guard true cons acc in
      let mem =
        if SvMap.is_empty x.info || not !Flags.enable_colv_len then
           x.mem
        else SvSet.fold aux x.new_seqvs x.mem in
      { x with mem; new_seqvs = SvSet.empty }

    (** [add_is_lt low up x] guard numerical constraint [low] < [up] in [x]  *)
    let add_is_lt sv_low sv_up (x: D.t) =
      if sv_low = sv_up then raise Bottom else
      let cons =
        (* Nc_cons (Apron.Tcons1.SUPEQ, Ne_var sv_up, Ne_bin (Add, Ne_var sv_low, Ne_csti 1)) in *)
        Nc_cons (Apron.Tcons1.SUP, Ne_var sv_up, Ne_var sv_low) in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "add_is_le: %a" Nd_utils.n_cons_fpr cons in
      D.guard ~no_apron:false true cons x

    (** [sv_is_lt x low up] returns [true] iff [x] ⊨ [low] < [up] *)
    let sv_is_lt (x: t) sv_low sv_up : bool =
      if sv_low = sv_up then false else
      let cons =
        (* Nc_cons (Apron.Tcons1.SUPEQ, Ne_var sv_up, Ne_bin (Add, Ne_var sv_low, Ne_csti 1)) in *)
        Nc_cons (Apron.Tcons1.SUP, Ne_var sv_up, Ne_var sv_low) in
      let res = D.sat x.mem cons in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "sv_is_le: %a => %b" Nd_utils.n_cons_fpr cons res in
      res

    (** [add_is_le low up x] guard numerical constraint [low] ≤ [up] in [x]  *)
    let add_is_le sv_low sv_up (x: D.t) =
      if sv_low = sv_up then x else
      let cons = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var sv_up, Ne_var sv_low) in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "add_is_le: %a" Nd_utils.n_cons_fpr cons in
      D.guard ~no_apron:true true cons x

    (** [sv_is_le x low up] returns [true] iff [x] ⊨ [low] ≤ [up] *)
    let sv_is_le (x: t) sv_low sv_up : bool =
      if sv_low = sv_up then true else
      let cons = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var sv_up, Ne_var sv_low) in
      let res = D.sat x.mem cons in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "sv_is_le: %a => %b" Nd_utils.n_cons_fpr cons res in
      res

    (** [saturate_bounds x] finds all possible sv that are bounds of seqvs *)
    let rec saturate_bounds (x: t) : t =
      if SvMap.is_empty x.info then x else
      let iterate = ref false in
      let treat_seqv seqv (acc: t) : t =
        let _ = if !Flags.flag_dbg_dom_seq then
          Log.debug "Treating seqv: %a" seqv_fpr seqv in
        let eq_class = Equiv.get_eq_class seqv x.rel in
        let {min; max; _ } = SvMap.find seqv x.info in
        (* We merge the bounds of equal seqvs *)
        let low_prev, up_prev = SvSet.fold
          (fun seqv (low_acc, up_acc) ->
            let low, up = SvMap.find seqv x.bounds in
            SvSet.union low_acc low, SvSet.union up_acc up)
          eq_class (SvSet.empty, SvSet.empty) in
        (* Infer bounds from min/max info *)
        let low =D.get_lower_svs min x.mem in
        let up  = D.get_upper_svs max x.mem in
        let _ = if !Flags.flag_dbg_dom_seq then
          Log.debug "Bounds infered from\n  min: { %a }\n  max: { %a }"
          Nd_utils.nsvset_fpr low
          Nd_utils.nsvset_fpr up in
        (* infer bounds from definitions and other bounds *)
        let low, up = List.fold_left
          (fun (low, up) def ->
            (* We collect all seqv occuring free in def *)
            let seqvs = def |> seq_expr_seqvs
              |> SvSet.filter (fun seqv -> not @@ SvSet.mem seqv x.empty_seq)
              |> SvSet.to_list in
            if seqvs = [] then (low, up) else
            (* we find their bounds *)
            let bounds = List.map
             (fun seqv -> SvMap.find seqv acc.bounds)
              seqvs in
            (* We compute the intersection of all these bounds *)
            let low_seqvs, up_seqvs = List.fold_left
              (fun (acc_low, acc_up) (low, up) ->
                SvSet.inter acc_low low,
                SvSet.inter acc_up up)
              (List.hd bounds) (List.tl bounds) in
            let svs = seq_expr_svs def in
            (* And we check if they are also bounds of svs in def *)
            let low_svs = SvSet.filter
              (fun low -> SvSet.for_all (fun sv -> sv_is_le acc low sv) svs)
              low_seqvs in
            let up_svs = SvSet.filter
              (fun up  -> SvSet.for_all (fun sv -> sv_is_le acc sv  up) svs)
              up_seqvs in
            SvSet.union low low_svs, SvSet.union up up_svs)
          (low, up)
          (SvMap.find_opt seqv acc.seq_def |> Option.get_default []) in
        let _ = if !Flags.flag_dbg_dom_seq then
          Log.debug "Bounds infered from def\n  low: { %a }\n  up: { %a }"
          Nd_utils.nsvset_fpr low
          Nd_utils.nsvset_fpr up in
        (* We check if we found new bounds *)
        let low_add = SvSet.diff low low_prev in
        let up_add = SvSet.diff up up_prev in
        let mem = if not @@ SvSet.mem seqv x.non_empty_seq then acc.mem else
          acc.mem
          |> SvSet.fold (fun low -> add_is_le low min) low_add
          |> SvSet.fold (fun up -> add_is_le max up) up_add in
        let _ = if not @@ SvSet.(is_empty low_add && is_empty up_add) then
          iterate := true in
        let bounds = SvSet.fold
          (fun seqv acc -> SvMap.add seqv (low, up) acc)
          eq_class
          acc.bounds in
        { acc with bounds; mem } in
      let support = Equiv.get_all_rep x.rel in
      let res = SvSet.fold treat_seqv support x in
      if !iterate then saturate_bounds res else
      begin
        if !Flags.flag_dbg_dom_seq then begin
          assert_wf ~label:"post saturate_bounds" res;
          Log.debug "saturate_bounds result:\n%a"
            (t_fpri SvMap.empty "  " ) res
        end;
        res
      end

    let saturate_bounds (x: t) : t =
      let x = empty_new_seqvs x in
      if not x.need_bnds_sat || not !Flags.enable_colv_bnd then x else
      let res = saturate_bounds x in
      { res with need_bnds_sat = false }

    (** type used to store some cardinal informations. *)
    type defs_cardinal = {
      colv_card: (int * int list) SvMap.t;
      (** map [seqv] -> [card_v, l_card] s.t :
        + [card_v] is the number of different expressions equals to [seqv]
          this means that we can enumerate the expressions
          from [0] to [card_v-1]
        + [l_card = [n1,...]] is the list storing the number [ni]
          of expressions equals to the i^{th} definition of [seqv]
        *)
      sv_card: (sv list) SvMap.t;
      (** map [sv] -> [sv_i] when [sv_i] is the list of symbolic variables
          known to be equals to [sv]*)
    }

    let defs_cardinal_fpr ind form card =
      let pp_colv form (card, card_list) =
        Format.fprintf form "%i, [%a]" card
          (gen_list_fpr " " (fun fmt -> Format.fprintf fmt "%i") "; ")
          card_list in
      let sep = "\n"^ind^"  "  in
      Format.fprintf form "%scolv_card:%s%a\n%ssv_card:%s%a"
        ind sep (SvMap.t_fpr sep pp_colv) card.colv_card
        ind sep (SvMap.t_fpr sep sv_list_fpr) card.sv_card


    let rec compute_card_colv (x: t) (colv: sv) (aux: defs_cardinal)
      : defs_cardinal=
      if SvMap.mem colv aux.colv_card then aux else
      let aux, l = match SvMap.find_opt colv x.seq_def with
        | None -> aux, []
        | Some (Seq_empty::[]) -> aux, []
        | Some defs ->
          List.fold_left
            (fun (aux, acc) def ->
              let aux, card = compute_card_expr x def aux in
              aux, card::acc)
            (aux, [])
            defs in
      let l = List.rev l in
      (* We start at one because [seqv] is also a valid expression *)
      let card = List.fold_left (+) 1 l in
      let colv_card = SvMap.add colv (card, l) aux.colv_card in
      { aux with colv_card }

    and compute_card_sv (x: t) (sv: sv) (aux: defs_cardinal) =
      match SvMap.find_opt sv aux.sv_card with
      | Some l -> aux, List.length l
      | None ->
        let equals = [ sv ] in
        let sv_card = SvMap.add sv equals aux.sv_card in
        { aux with sv_card }, List.length equals

    and compute_card_expr (x: t) (e: seq_expr) (aux: defs_cardinal)
      : defs_cardinal * int =
      match e with
      | Seq_var colv ->
        let aux = compute_card_colv x colv aux in
        aux, SvMap.find colv aux.colv_card |> fst
      | Seq_empty -> aux, 1
      | Seq_val sv -> compute_card_sv x sv aux
      | Seq_sort _ -> aux, 1 (* We don't want to iter on sorted seq *)
      | Seq_Concat l ->
        List.fold_left
          (fun (aux, acc) e ->
            let aux, card = compute_card_expr x e aux in
            aux, card * acc )
          (aux, 1)
          l

    (** [compute_card x] computes all the cardinal information needed
        to iterate on an expression *)
    let compute_card (x: t) =
      let repr = Equiv.get_all_rep x.rel in
      let empty_card = { colv_card = SvMap.empty; sv_card = SvMap.empty } in
      SvSet.fold
        (fun colv acc -> compute_card_colv x colv acc)
        repr
        empty_card


    (** [iterate_on_def x card e n] computes the [n]^{th} «synonym» of [e],
        according to definition stored in [x].
        - We don't consider the constraints in the [x.rel] part.
        - If [n = 0] the result is [e] *)
    let iterate_on_def (x: t) (card: defs_cardinal) (e: seq_expr) (n: int)
      : seq_expr option =
      let rec aux_iter e n =
      match e with
      | _ when n = 0 -> Some e (* n = 0 should always return identity! *)
      | Seq_empty _ -> None
      | Seq_val sv ->
        let l = SvMap.find sv card.sv_card in
        List.nth l n
        |> fun sv' -> Seq_val sv'
        |> Option.some
      | Seq_var colv ->
        if not @@ Equiv.is_repr colv x.rel then None else
        let card_colv, l_card = SvMap.find colv card.colv_card in
        if n >= card_colv then None else
        let n = n - 1 in
        let defs = SvMap.find colv x.seq_def in
        let rec find_def defs l_card n =
          match defs, l_card with
          | [], []           (* Impossible since n < card_colv *)
          | _, [] | [], _ -> (* Impossible since both lists have same length *)
            assert false
          | def::_, card_def::_ when n < card_def ->
            aux_iter def n
          | _::defs, card_def::l_card ->
            let n = n - card_def in
            find_def defs l_card n in
        find_def defs l_card n
      | Seq_Concat l ->
        let aux (n, acc) e =
          let card_atom =
          match e with
          | Seq_val sv ->
            SvMap.find sv card.sv_card |> List.length
          | Seq_var colv ->
            SvMap.find colv card.colv_card |> fst
          | Seq_Concat _ | Seq_empty -> assert false
          (* These cases should not occurs
             because the expression is in canonical form. *)
          | Seq_sort _ -> assert false in
            (n / card_atom, (n mod card_atom)::acc)
        in
        let n, l_n = List.fold_left aux (n, []) l in
        if n > 0 then None else
        let l_n =  List.rev l_n in
        let l_res = List.map2 aux_iter l l_n in
        if List.exists Option.is_none l_res then None else
        l_res
        |> List.map Option.get
        |> fun l -> Seq_Concat l
        |> normalise
        |> Option.some
      | Seq_sort _ -> Log.todo_exn "JG: treat sort %s" __LOC__ in
      let res =
        try aux_iter e n
        with exn ->
          Log.debug "Catching exception %s" (Printexc.to_string exn); None in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "iter [%i] %a:\n  => %a" n
          seq_expr_fpr e
          (Option.t_fpr seq_expr_fpr) res in
      res

    (** [nf_expr expr x] rewrites [expr] to satisfy invariants of [x].
        - all seqvs in output are classes représentants
        - if an sv as an alias then it is replaced by it.
        - try to fold [expr] according to definitions in [x.seq_def] *)
    let nf_expr (x: t) (expr: seq_expr) : seq_expr =
      let _ = if false && !Flags.flag_dbg_dom_seq then
        Log.debug "nf_expr %a" (fpri_seq "  " ) x in
      let rec fold_def (e: seq_expr) : seq_expr =
        let fold_list colv (expr, acc) def =
          let expr, rewritten = factorize colv def expr  in
          expr, acc || rewritten in
        let fold_map colv defs (expr, acc) =
          List.fold_left (fold_list colv) (expr, acc) defs in
        let expr, rewritten = SvMap.fold fold_map x.seq_def (e, false) in
        if false && !Flags.flag_dbg_dom_seq then
          Log.debug "fold_def: %a -> %a (%b)" seq_expr_fpr e seq_expr_fpr expr
            rewritten;
        if rewritten then fold_def expr else expr in
      let rewrite_seqv seqv = Seq_var (Equiv.get_repr x.rel seqv) in
      let rewrite_sv sv =
        match SvMap.find_opt sv x.node_alias with
        | Some seqv -> Seq_var seqv
        | None -> Seq_val sv in
    expr
    |> q_expr_map_aux rewrite_seqv rewrite_sv
    |> fold_def

    (** [nf_cons cons x] rewrites [cons] to satisfy invariants of [x]. *)
    let nf_cons (x: t) (Seq_equal (e1, e2): seq_cons) : seq_cons =
      let e1' = nf_expr x e1 in
      let e2' = nf_expr x e2 in
      Seq_equal (e1', e2')

    (** [add_def seqv expr x] adds definition [seqv := expr] in [x]*)
    let add_def (seqv: sv) (expr: seq_expr) (x: t) : t =
      let def_seqv = match SvMap.find_opt seqv x.seq_def with
        | None -> []
        | Some l -> l in
      if List.mem expr def_seqv then x else
      let seq_def = SvMap.add seqv (expr::def_seqv) x.seq_def in
      { x with seq_def }

    (** [add_bound seqv sv is_lower x] adds [sv] as:
        - a lower bound of [seqv] if [is_lower] is [true]
        - an upper bound of [seqv], otherwise *)
    let add_bound (seqv: sv) (sv: sv) (is_lower: bool) (x: t) : t =
      let lower, upper = SvMap.find_opt seqv x.bounds
        |> Option.get_default (SvSet.empty, SvSet.empty) in
      if is_lower then
        let lower = SvSet.add sv lower in
        {x with bounds = SvMap.add seqv (lower, upper) x.bounds}
      else
        let upper = SvSet.add sv upper in
        {x with bounds = SvMap.add seqv (lower, upper) x.bounds}

    let check_nodes ~(svs: SvSet.t) ~(colvs: col_kinds) ({mem; _}: t) =
      Log.todo "check_nodes: needs to check for SEQVs";
      D.check_nodes ~svs ~colvs mem

    let sv_add ?(mark: bool = true) (sv: sv) ({mem; _} as x: t) =
      { x with mem = D.sv_add ~mark:mark sv mem }

    (** [sv_discard sv x] discard all constraints containing node [sv].
        It does not remove [sv] from the [mem] part of [x]. *)
    let sv_discard (sv: sv) (x: t) : t =
      let filter_expr seqv expr =
        let res = not @@ SvSet.mem sv @@ seq_expr_svs expr in
        if not res && !Flags.flag_dbg_dom_seq then
          Log.debug "sv_discard dropping definition %a = %a"
            seqv_fpr seqv seq_expr_fpr expr;
        res
      in
      let eqs =
        D.get_eq_class sv x.mem
        |> SvSet.remove sv
        |> SvSet.filter (fun sv -> not @@ is_extremum x sv) in
      let seq_def, node_alias =
        if SvSet.cardinal eqs > 0 then
          let sv' = SvSet.choose eqs in
          let rename_sv i = if i = sv then sv' else i in
          SvMap.map
            (List.map @@ q_expr_map (fun seqv -> seqv) rename_sv)
            x.seq_def,
          match SvMap.find_opt sv x.node_alias with
          | None -> x.node_alias
          | Some seqv ->
            x.node_alias
            |> SvMap.remove sv
            |> SvMap.add sv' seqv
        else
          x.seq_def
          |> SvMap.mapi (fun seqv -> List.filter @@ filter_expr seqv)
          |> SvMap.filter (fun _ -> (<>) [] ),
          SvMap.remove sv x.node_alias in
      let bounds = SvMap.map
        (fun (low, up) ->
          SvSet.filter ( (<>) sv ) low,
          SvSet.filter ( (<>) sv ) up)
        x.bounds in
      {x with node_alias; seq_def; bounds}

    let sv_rem (sv: sv) (x: t) =
      let x = saturate_bounds x in
      let x = sv_discard sv x in
      { x with mem = D.sv_rem sv x.mem }

    let get_max (seqv: sv) (x: t): sv =
      match SvMap.find_opt seqv x.info with
      | None -> assert false
      | Some { max } -> max

    let get_min (seqv: sv) (x: t): sv =
      match SvMap.find_opt seqv x.info with
      | None -> assert false
      | Some { min } -> min

    let get_size (seqv: sv) (x: t): sv =
      match SvMap.find_opt seqv x.info with
      | None -> assert false
      | Some { size } -> size

    (** [has_same_length colv1 colv2 x] returns [true] iff
        [x.mem]|= size([colv1]) == size([colv2]) *)
    let has_same_length (colv1: sv) (colv2: sv) (x: t) : bool =
      try
        let l1 = get_size colv1 x in
        let l2 = get_size colv2 x in
        let n_cons = Nc_cons (EQ, Ne_var l1, Ne_var l2) in
        D.sat x.mem n_cons
      with Assert_failure _ -> false

    (** [depends_on x seqv expr] returns [true] iff
        [expr] depends on [seqv] (even indirectly)*)
    let rec depends_on (x: t) (seqv: sv) (expr: seq_expr) : bool =
      let depends_on_seqv seqv' =
        let defs = SvMap.find_opt seqv' x.seq_def |> Option.get_default [] in
        List.exists (depends_on x seqv) defs in
      let seqvs = seq_expr_seqvs expr in
      SvSet.mem seqv seqvs || SvSet.exists depends_on_seqv seqvs

    (** [simpilfy x] returns a simplified abstract value equals to [x] *
     - There is no S_i = S_i definitions
     - Has some heuristics to dectect _|_ abstract value *)
    let simplify (x: t) =
      let filter_defs key defs =
        List.filter ( (<>) (Seq_var key)) defs in
      let defs = SvMap.fold
        (fun key defs acc ->
          match filter_defs key defs with
          | [] -> acc
          | defs -> SvMap.add key defs acc)
        x.seq_def SvMap.empty in
      let x = { x with seq_def = defs } in
      if not @@ SvSet.is_empty @@ SvSet.inter x.empty_seq x.non_empty_seq then
        { x with mem = D.bot }
      else x

    (** Collection variables **)
    (* add collection variable with the given name *)
    let colv_add
        (ck: col_kind)
        ~(root: bool)
        ?(kind: set_par_type option = None)
        ?(name: string option = None)
        (i: sv) (info: colv_info option) (t: t): t =
      match ck with
      | CK_set ->
          { t with
            mem = D.colv_add ck ~root:root ~kind:kind ~name:name i info t.mem }
      | CK_seq ->
          Log.debug "Adding a seq var: %s -> %a (root:%b) (%a)"
            (match name with | None -> "?" | Some s -> s) sv_fpr i root
            (Option.t_fpr Col_utils.colv_info_fpr) info;
          assert (not (SvSet.mem i t.seqvs));
          let mem =
            D.colv_add CK_set ~root:root ~kind:kind ~name:name i info t.mem in
          let rel = Equiv.add i t.rel in
          let seqvs = SvSet.add i t.seqvs in
          let root_seqvs = if root
            then SvSet.add i t.root_seqvs
            else t.root_seqvs in
          let info, mem, bounds =
            match info with
            | None ->
                t.info,
                mem,
                SvMap.add i (SvSet.empty, SvSet.empty) t.bounds
            | Some ({min; max; size} as info) ->
              SvMap.add i info t.info,
              mem
              |> D.sv_add min
              |> D.sv_add max
              |> D.sv_add size,
              SvMap.add i (SvSet.singleton min, SvSet.singleton max) t.bounds
          in
          let new_seqvs = SvSet.add i t.new_seqvs in
          { t with
            mem;
            rel;
            seqvs;
            root_seqvs;
            info ;
            bounds ;
            new_seqvs }
    (* removal of a collection variable (works both for SETv and SEQv *)
    let colv_rem (colv: sv) (t: t): t =
      if !Flags.flag_dbg_dom_seq then begin
        Log.debug "Removing a seq var: %a\nseqvs = { %a }" sv_fpr colv
          (SvSet.t_fpr "; ") t.seqvs;
          assert_wf t;
      end;
      if not @@ SvSet.mem colv t.seqvs
        then { t with mem = D.colv_rem colv t.mem} else
      let t = saturate_bounds t in
      let is_repr = Equiv.is_repr colv t.rel in
      let rep, rel = Equiv.rem colv t.rel in
      let def = SvMap.find_opt colv t.seq_def
        |> Option.get_default [] in
      let defs = SvMap.remove colv t.seq_def in
      let defs, alias =
        match rep, def with
        (* if colv was not a class representive, we have nothng to do *)
        | _, _ when not is_repr ->
          defs, SvMap.empty
        (* if we have a new class representative,
           we use it to rewrite all definitions *)
        | Some rep, def ->
          if !Flags.flag_dbg_dom_seq then
            Log.debug "Rewriting Q[%a]<-Q[%a]"
            sv_fpr colv sv_fpr rep;
          let replace seqv0 =
            if seqv0 = colv then rep else  seqv0 in
          let rewrite expr =
            expr |> q_expr_map replace (fun sv -> sv) |> normalise in
          let defs = SvMap.map (List.map rewrite) defs in
          if def = []
            then defs, SvMap.empty
            else SvMap.add rep def defs, SvMap.empty
        (* Otherwise we use a defintion of colv to rewrite other definitions *)
        | None, (expr_def::q) ->
          if !Flags.flag_dbg_dom_seq then
            Log.debug "Rewriting Q[%a]<-%a"
            sv_fpr colv seq_expr_fpr expr_def;
          if !Flags.flag_dbg_dom_seq && q <> [] then
            Log.fatal "We are dropping the following constraints:\n  %a"
              (gen_list_fpr "" seq_expr_fpr "\n  ") q;
          let replace seqv0 =
            if colv = seqv0 then expr_def
            else Seq_var seqv0 in
          let rewrite def =
            def
            |> q_expr_map_aux replace (fun x -> Seq_val x)
            |> normalise in
          (*rewrite can create alias or rel*)
          let defs, alias =
            SvMap.fold
              (fun key l (acc1, acc2) ->
                let l, acc2 =
                  (List.fold
                    (fun (acc1, acc2) e ->
                      let e = rewrite e in
                      match empty_leftmost e with
                      | Seq_val v, Seq_empty -> e :: acc1, SvMap.add v key acc2
                      | exception SidemostAtomIsSort | _ -> e :: acc1, acc2)
                    ([], acc2)) l in
                  SvMap.add key l acc1, acc2)
              defs (SvMap.empty, SvMap.empty) in
          defs, alias
        (* If nothing of the above apply, we discard all definition with colv *)
        | None, [] ->
          if !Flags.flag_dbg_dom_seq then
            Log.debug
              "No candidate expr! Dropping all constraints containing Q[%a]"
              sv_fpr colv;
          let filter_defs seqv def =
            let res = not @@ SvSet.mem colv (seq_expr_seqvs def) in
            let _ =
              if not res && !Flags.flag_dbg_dom_seq then
                Log.warn "  Dropping %a = %a"
                  seqv_fpr seqv seq_expr_fpr def in
            res in
          SvMap.fold
            (fun key defs acc ->
              match List.filter (filter_defs key) defs with
              | [] -> acc
              | defs -> SvMap.add key defs acc)
            (SvMap.remove colv t.seq_def) SvMap.empty, SvMap.empty in
      let alias =
        SvMap.union
          (fun _ x _ -> Some x)
          alias
          (match rep with
          | _ when not is_repr -> t.node_alias
          | Some rep ->
              let replace seqv0 =
                if seqv0 = colv then rep else seqv0 in
              SvMap.map replace t.node_alias
          | None ->
              SvMap.filter (fun _ -> (<>) colv) t.node_alias) in
      let x =
        { t with
          seqvs         = SvSet.remove colv t.seqvs ;
          root_seqvs    = SvSet.remove colv t.root_seqvs ;
          seq_def       = defs;
          info          = SvMap.remove colv t.info ;
          bounds        = SvMap.remove colv t.bounds ;
          empty_seq     = SvSet.remove colv t.empty_seq;
          non_empty_seq = SvSet.remove colv t.non_empty_seq;
          sorted        = SvSet.remove colv t.sorted ;
          rel           = rel;
          node_alias    = alias;
          mem           = D.colv_rem colv t.mem } in
      let x = simplify x in
      if !Flags.flag_dbg_dom_seq then begin
        Log.debug "result:\n%a" (fpri_seq "  ") x;
        assert_wf x;
      end;
      x
    (* check whether a collection variable is root *)
    let colv_is_root (i: sv) (t: t): bool =
      SvSet.mem i t.root_seqvs
      || (try D.colv_is_root i t.mem with Col_unsup -> false)
    (* collect root set variables *)
    let colv_get_roots (t: t): SvSet.t =
      let uroots = try D.colv_get_roots t.mem with Col_unsup -> SvSet.empty in
      SvSet.union uroots t.root_seqvs

    let sve_sync_top_down (sv_mod: svenv_mod) (x: t) =
      let x = if PSet.is_empty sv_mod.svm_rem
        then x
        else saturate_bounds x in
      let x = x
        |> PSet.fold sv_discard sv_mod.svm_rem
        |> PSet.fold sv_discard sv_mod.svm_mod in
      { x with mem = D.sve_sync_top_down sv_mod x.mem }

    (** [is_empty x seqv] returns [true] if it can be proven
        [x ⊨ seqv = []].
        This function loops if there are cyclic constraints. *)
    let rec is_empty x seqv =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling is_empty Q[%a]" sv_fpr seqv;
      if SvSet.mem seqv x.empty_seq then true else
      if SvSet.mem seqv x.non_empty_seq then false else
      let using_size =
        SvMap.mem seqv x.info &&
        let {size; } = SvMap.find seqv x.info in
        let cons = Nc_cons (EQ, Ne_var size, Ne_csti 0) in
        D.sat x.mem cons in
      if using_size then true else
      let def_empty expr =
        let seqvs = seq_expr_seqvs expr in
        let svs = seq_expr_svs expr in
        SvSet.is_empty svs && SvSet.for_all (is_empty x) seqvs in
      let defs =
        SvMap.find_opt seqv x.seq_def
        |> Option.get_default [] in
      List.exists def_empty defs

    (** [is_empty x seqv] returns [true] if it can be proven
        [x ⊨ seqv ≠ []].
        This function loops if there is cyclic contraints. *)
    let rec is_non_empty x seqv =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling is_non_empty Q[%a]" sv_fpr seqv;
      if SvSet.mem seqv x.non_empty_seq then true else
      if SvSet.mem seqv x.empty_seq then false else
      let using_size =
        SvMap.mem seqv x.info &&
        let {size; } = SvMap.find seqv x.info in
        let cons = Nc_cons (SUP, Ne_var size, Ne_csti 0) in
        D.sat x.mem cons in
      if using_size then true else
      let def_non_empty expr =
        let seqvs = seq_expr_seqvs expr in
        let svs = seq_expr_svs expr in
        (not @@ SvSet.is_empty svs) || SvSet.exists (is_non_empty x) seqvs in
      let defs =
        SvMap.find_opt seqv x.seq_def
        |> Option.get_default [] in
      List.exists def_non_empty defs

    exception Found of (sv * seq_expr list)

    (** [have_same_content x e1 e2] checks if
        x.set |= { [e1] } == { [e2] }*)
    let have_same_content (x: t) (e1: seq_expr) (e2: seq_expr) : bool =
      let find_candidat (mset: MSet.t) =
        SvMap.iter
          (fun seqv defs ->
            if MSet.mem_seqv mset seqv > 0 then raise (Found (seqv, defs)))
          x.seq_def in
      let rec use_mset (mset1: MSet.t) (mset2: MSet.t) : bool =
        let mset1, mset2 = MSet.sym_diff mset1 mset2 in
        if MSet.is_empty mset1 && MSet.is_empty mset2 then true else
        if MSet.is_empty_seqv mset1 && MSet.is_empty_seqv mset2 then false else
        (* TODO check for equality class *)
        match find_candidat mset1 with
        | exception Found (seqv, defs) ->
          let mset1 = MSet.rm_seqv seqv mset1 in
          List.exists (fun def -> use_mset (MSet.add_expr def mset1) mset2) defs
        | () -> match find_candidat mset2 with
        | exception Found (seqv, defs) ->
          let mset2 = MSet.rm_seqv seqv mset2 in
          List.exists (fun def -> use_mset mset1 (MSet.add_expr def mset2)) defs
        | () -> false in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling have_same_content:\n  e1: %a\n  e2: %a"
           seq_expr_fpr e1 seq_expr_fpr e2 in
      let res =
        use_mset (MSet.of_expr e1) (MSet.of_expr e2) ||
        D.set_sat (S_eq (to_set_expr e1, to_set_expr e2)) x.mem in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug " -> Result: %b" res in
      res

    (** [ordered x seqv1 seqv2] checks if [seqv1.seqv2] is sorted:
        + if [max(seqv1)] ≤ [min(seqv2)]
        + if ∃ α ∈ up(seqv1), β ∈ down(seqv2), α ≤ β *)
    let ordered (x: t) (seqv1: sv) (seqv2: sv): bool =
      if is_empty x seqv1 || is_empty x seqv2 then true else
      let up1  = snd @@ SvMap.find seqv1 x.bounds in
      let low2 = fst @@ SvMap.find seqv2 x.bounds in
      not @@ SvSet.disjoint up1 low2
      (* ||
      let max1 = (SvMap.find seqv1 x.info).max in
      let min2 = (SvMap.find seqv2 x.info).min in
      sv_is_le x max1 min2
      ||
      SvSet.exists
        ( fun up ->
          SvSet.exists
            (fun low -> sv_is_le x up low )
          low2 )
        up1 *)

    (** [correctly_ordered x seqvs] checks if the list of seqvs is sorted.
        If [seqvs] = [Q_1...Q_n],
        then it checks wheather ∀ i < j, {!ordered}[Q_i, Q_j] *)
    let rec correctly_ordered (x: t): sv list -> bool = function
      | [] -> true
      | seqv::q ->
        List.for_all (ordered x seqv) q && correctly_ordered x q

    let rec is_sorted (x: t) (seqv: sv) : bool =
      if SvSet.mem seqv x.sorted then true else
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "Checking sortedness of %a" seqv_fpr seqv in
      let defs = SvMap.find_opt seqv x.seq_def |> Option.get_default [] in
      List.exists (is_sorted_expr x) defs

    and is_sorted_expr (x: t) (expr: seq_expr) : bool =
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "Checking sortedness of %a" seq_expr_fpr expr in
      if SvMap.is_empty x.info then false else
      match normalise expr with
      | Seq_sort _ | Seq_empty | Seq_val _ -> true
      | Seq_var seqv -> is_sorted x seqv
      | Seq_Concat exprs ->
        match cut_expr exprs with
        | None -> false
        | Some cuts -> ordered_cuts x cuts

    and ordered_cuts x cuts : bool =
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "Checking sortedness of cut: %a" cutted_expr_fpr cuts in
      let check_low sv seqv =
        if SvSet.mem seqv x.empty_seq then true else
        let min = (SvMap.find seqv x.info).min in
        if sv_is_le x sv min then true else
        let lows = fst @@ SvMap.find seqv x.bounds in
        SvSet.exists (fun low -> sv_is_le x sv low ) lows in
      let check_up sv seqv =
        if SvSet.mem seqv x.empty_seq then true else
        let max = (SvMap.find seqv x.info).max in
        if sv_is_le x max sv then true else
        let ups  = snd @@ SvMap.find seqv x.bounds in
        SvSet.exists (fun up -> sv_is_le x up sv ) ups in
      match cuts with
      | [] -> true
      | [Val _] -> true
      | [Seq_vars seqvs] ->
          List.for_all (is_sorted x) seqvs
          && correctly_ordered x seqvs
      | [Val sv; Seq_vars seqvs] ->
          List.for_all (is_sorted x) seqvs
          && List.for_all (check_low sv) seqvs
          && correctly_ordered x seqvs
      | Seq_vars seqvs::Val up::q ->
          List.for_all (is_sorted x) seqvs
          && List.for_all (check_up up) seqvs
          && correctly_ordered x seqvs
          && ordered_cuts x (Val up::q)
      | Val low::Seq_vars seqvs::Val up::q ->
          List.for_all (is_sorted x) seqvs
          && sv_is_le x low up
          && List.for_all (check_up up) seqvs
          && List.for_all (check_low low) seqvs
          && correctly_ordered x seqvs
          && ordered_cuts x (Val up::q)
      | Val low::Val up::q ->
          sv_is_le x low up
          && ordered_cuts x (Val up::q)
      (* Having two consecutives concatenation of seqvs is prohibited *)
      | Val _:: Seq_vars _:: Seq_vars _::_
      | Seq_vars _::Seq_vars _::_ -> assert false

    let is_sorted_expr (x: t) (e: seq_expr) : bool =
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling is_sorted_expr: %a" seq_expr_fpr e in
      let res = is_sorted_expr x e in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug " -> Result: %b" res in
      res

    let sat (x: t) (cons: n_cons) : bool =
      D.sat x.mem cons

    let set_sat (cons: set_cons) (x: t) : bool =
      D.set_sat cons x.mem

    (** [seq_equals e1 e2 x] returns [true]
     *    iff it can be proven that [e1] = [e2] in the abstract value [x] *)
    let rec seq_equals (e1: seq_expr) (e2: seq_expr) (x: t) =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "  e1 = %a\n  e2 = %a"
          seq_expr_fpr e1 seq_expr_fpr e2;
      if e1 = e2 then true else
      match empty_sidemost_double true e1 e2 with
      (* N1·Q1 = N2·Q2 <=> N1 = N2 && Q1 = Q2 *)
      | Seq_val n1, e1', Seq_val n2, e2' ->
          let n_cons = Nc_cons (EQ, Ne_var n1, Ne_var n2) in
          (sat x n_cons) &&
          (seq_equals e1' e2' x)
      (* Qi ~ Qj && E1 = E2 => Qi.E1 = Qj.E2  *)
      | Seq_var seqv1, e1, Seq_var seqv2, e2
          when Equiv.are_equiv seqv1 seqv2 x.rel ->
          seq_equals e1 e2 x
      (* Trying to inline definitions:
         JG: now inlining the leftmost seqv is enough ! *)
      | Seq_var seqv1, e1', _, _ when SvMap.mem seqv1 x.seq_def ->
        let try_def def =
          let e1 = Seq_Concat [def; e1'] |> normalise in
          seq_equals e1 e2 x in
        let defs = SvMap.find seqv1 x.seq_def in
        List.exists try_def defs
      | _, _, Seq_var seqv2, e2' when SvMap.mem seqv2 x.seq_def ->
        let try_def def =
          let e2 = Seq_Concat [def; e2'] |> normalise in
          seq_equals e1 e2 x in
        let defs = SvMap.find seqv2 x.seq_def in
        List.exists try_def defs
      (* the two cases below may seem redundant with the inline of definitions.
         But, since [is_empty] use infomrations in the numerical domain,
         We can infer that a seqv is empty by looking at its size info,
         even if we don't have the definition [Qi = []] in [x] *)
      (* Q1 = [] && E1' = E2 => Q1.E1' = E2 *)
      | Seq_var seqv1, e1', _, _ when is_empty x seqv1 ->
        seq_equals e1' e2 x
      (* Q2 = [] && E1 = E2' => E1 = Q2.E2' *)
      | _, _, Seq_var seqv2, e2' when is_empty x seqv2 ->
        seq_equals e1 e2' x
      | _ ->  false
      | exception SidemostAtomIsSort ->
        match e1, e2 with
        | Seq_sort e, e' | e', Seq_sort e ->
          is_sorted_expr x e' &&
          have_same_content x e e'
        | _ -> assert false


    let seq_sat (cons: seq_cons) (t: t) : bool =
      if !Flags.flag_dbg_dom_seq then begin
        Log.debug "Calling seq_sat\ncons: %a\nx = \n%a"
          seq_cons_fpr cons (fpri_seq "  ") t;
        assert_wf ~label:"seq_sat pre" t;
      end;
      if is_bot t then true else
      let res = match nf_cons t cons with
        (* N[i] = sorted( N[i] ) *)
        | Seq_equal (Seq_val sv, Seq_sort (Seq_val sv'))
            when sv = sv' -> true
        (* Q[i] = sorted( Q[i] ) <=> Q[i] ∈ t.sorted *)
        | Seq_equal (Seq_var seqv, Seq_sort (Seq_var seqv'))
            when seqv = seqv' -> SvSet.mem seqv t.sorted
        | Seq_equal (Seq_var seqv, Seq_sort e)
          when SvSet.mem seqv t.sorted &&
          seq_equals (Seq_var seqv) e t ->
            true
        | Seq_equal (Seq_var seqv, Seq_var seqv')
            when Equiv.are_equiv seqv seqv' t.rel ->
              true
        | Seq_equal (Seq_var seqv, e)
            when SvSet.mem seqv t.sorted
              && (is_sorted_expr t e)
              && (have_same_content t (Seq_var seqv) e) -> true
        (* General case *)
        | Seq_equal (e1, e2) -> seq_equals e1 e2 t in
      if !Flags.flag_dbg_dom_seq then Log.debug "Result = %b" res;
      res

    let symvars_check (s: SvSet.t) (x: t) =
      D.symvars_check s x.mem

    let symvars_filter (s: SvSet.t)
        ?(col_vars: SvSet.t = SvSet.empty)
        (x: t) =
      let to_rem = SvSet.diff x.seqvs col_vars in
      let x = SvSet.fold colv_rem to_rem x in
      { x with mem = D.symvars_filter s ~col_vars:col_vars x.mem }

    let symvars_merge (stride: int)
        (base: sv)
        (sv: sv)
        (block_desc: (Bounds.t * Offs.svo * Offs.size) list)
        (extptrs: OffSet.t)
        (x: t): t =
      { x with mem = D.symvars_merge stride base sv block_desc extptrs x.mem }

    let rec aux_unify ?(hint=SvPrSet.empty) (x1: t) (x2: t) (e1: seq_expr) (e2: seq_expr) (side: bool)
        : seq_expr option =
      let concat (size: bool) (e: seq_expr) (e': seq_expr option)
        : seq_expr option =
        if size then
        Option.map (fun expr -> Seq_Concat [e; expr] |> normalise) e'
        else
        Option.map (fun expr -> Seq_Concat [expr; e] |> normalise) e' in
      if e1 = e2 then Some e1 else
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "  e1 = %a\n  e2 = %a"
        seq_expr_fpr e1 seq_expr_fpr e2 in
      (* Since it will be difficult to implement backtracking,
         the order of the rewriting rules is important *)
      match empty_sidemost_double side e1 e2 with
      | Seq_var seqv1, e1', Seq_var seqv2, e2' when seqv1 = seqv2 ->
        let res' = aux_unify ~hint x1 x2 e1' e2' true in
        concat side (Seq_var seqv1) res'
      | Seq_val sv1, e1', Seq_val sv2, e2' ->
        let set1 = D.get_eq_class sv1 x1.mem in
        let set2 = D.get_eq_class sv2 x2.mem in
        let set = SvSet.inter set1 set2 in
        if SvSet.is_empty set then None else
        let sv = SvSet.choose set in
        let res' = aux_unify ~hint x1 x2 e1' e2' true in
        concat side (Seq_val sv) res'
      | Seq_var seqv1, e1', Seq_var seqv2, e2'
        when is_empty x2 seqv1 &&
             is_empty x1 seqv2 &&
             hint |> SvPrSet.is_empty |> not ->
        let contains =
          fun x y -> SvPrSet.exists (fun (e1, e2) -> e1 = x && e2 = y) hint in
        if contains seqv1 seqv2 && side || (not (contains seqv1 seqv2) && not side) then
          let res' = aux_unify ~hint x1 x2 e1' e2 true in
          concat side (Seq_var seqv1) res'
        else
          let res' = aux_unify ~hint x1 x2 e1 e2' true in
          concat side (Seq_var seqv2) res'
     | Seq_var seqv1, e1', _, _ when is_empty x2 seqv1 ->
        let res' = aux_unify ~hint x1 x2 e1' e2 true in
        if is_empty x1 seqv1 then
          res'
        else
          concat side (Seq_var seqv1) res'
      | _, _, Seq_var seqv2, e2' when is_empty x1 seqv2 ->
        let res' = aux_unify ~hint x1 x2 e1 e2' true in
        if is_empty x2 seqv2 then
          res'
        else
          concat side (Seq_var seqv2) res'
      | Seq_var seqv1, e1', Seq_var seqv2, e2'
      when Equiv.are_equiv seqv1 seqv2 x1.rel ->
        let res' = aux_unify ~hint x1 x2 e1' e2' true in
        concat side (Seq_var seqv2) res'
      | Seq_var seqv1, e1', Seq_var seqv2, e2'
      when Equiv.are_equiv seqv1 seqv2 x2.rel ->
        let res' = aux_unify ~hint x1 x2 e1' e2' true in
        concat side (Seq_var seqv1) res'
      | _ when side -> aux_unify ~hint x1 x2 e1 e2 false
      | _ | exception SidemostAtomIsSort -> None

    (** [unify e1 e2 x1 x2] returns [Some e] s.t. [e] over-approximate [e1] in
    [x1] and [e2] in [x2]. It returns [None] when such over-approximate cannot
    be found *)
    (** It works as follows: for each side [e_i] we try to see if the expression
      is an over-approximate of the other one [e_{1-i}]. There is two steps:
      - We try to remove in [e_i] the SEQV that are equals to [] in [x_i']
      - We try to replace SEQV in [e_i] by their definition in [x_i']
    *)
    let unify ?(hint=SvPrSet.empty) (x1: t) (x2: t) (seqv:sv) (e1: seq_expr) (e2: seq_expr)
        : seq_expr option =
      let rec simplify x expr =
        remove_empty x.empty_seq expr in
      (* rename each seqv by its representant in V/= *)
      let rename x e =
        let rename_seqv seqv =
          try SvMap.find seqv x.seq_def |> List.hd
          with Not_found |  Failure _ -> Seq_var seqv in
        q_expr_map_aux rename_seqv (fun sv -> Seq_val sv) e in
      if !Flags.flag_dbg_dom_seq then
        Log.debug
          "Trying to unify definition on seq_var : Q[%a]\n  e1 = %a\n  e2 = %a"
          sv_fpr seqv seq_expr_fpr e1 seq_expr_fpr e2;
      let e1 = normalise e1
      and e2 = normalise e2 in
      let res =
      if e1 = e2 then Some e1
      else
        (* e1 -> e2 *)
        let e1' = normalise @@ simplify x2 e1 in
        if e1' = e2 then Some e1
        else
          let e1' = normalise @@ rename x2 e1' in
        if e1' = e2 then Some e1
      else
        (* e2 -> e1 *)
        let e2' = normalise @@ simplify x1 e2 in
        if e2' = e1 then Some e2
        else
          let e2' = normalise @@ rename x1 e2' in
        if e2' = e1 then Some e2
      else aux_unify ~hint x1 x2 e1 e2 true in
      let _ = match res with
      | None -> Log.debug "-> res = _\n"
      | Some e -> Log.debug "-> res = %a\n" seq_expr_fpr e in
      res

    (** [equal_to_sort seqv defs other acc] implements the following heuristic:

        If:
          + defs |= Q_i = Sort(Q_j)
          + other |= Q_i = Q_j && is_sorted(Q_i)

        Then
          + defs \/ other |= Q_i = Sort(Q_j) *)
    let equal_to_sort seqv (defs: seq_expr list) (other: t) acc
        : seq_expr list =
      if not @@ is_sorted other seqv then acc else
      List.fold_left
        (fun acc def -> match def with
          | Seq_sort (Seq_var seqv')
          when Equiv.are_equiv seqv seqv' other.rel ->
            (Seq_sort (Seq_var seqv'))::acc
          | _ -> acc)
        acc defs

    (** [infer_strictly x1 x2] adds strict bound constraints as follows:
          + If: [x1] |= [Q = []] and [x2] |= [bnd > max( Q )]
          + Then: [x1] |= [bnd > max( Q )]*)
    let infer_strictly (x1: t) (x2: t) : t =
      let f op1 op2 mem =
        if sv_is_lt x2 op2 op1 then
          add_is_lt op2 op1 mem
        else mem in
      let mem = SvSet.fold
        (fun x acc ->
          if SvSet.mem x x2.empty_seq then acc else
            let inf_bound, sup_bound = SvMap.find x x2.bounds in
            if SvMap.is_empty x2.info then acc else
            let {min; max; _ } = SvMap.find x x2.info in
            let is_not_empty_info x =
              not @@
              SvMap.exists
                (fun q {min; max; _ } ->
                SvSet.mem q x1.empty_seq && (min = x || max = x) ) x1.info in
            acc |>
            SvSet.fold
              (fun i acc -> f min i acc)
              (inf_bound |> SvSet.filter is_not_empty_info) |>
            SvSet.fold
              (fun i acc -> f i max acc)
              (sup_bound |> SvSet.filter is_not_empty_info)
        )
        x1.empty_seq x1.mem in
      {x1 with mem = mem}

    let rec eq_mod_empty (x: t) (e1: seq_expr) (e2: seq_expr) : bool =
      match empty_sidemost_double true e1 e2 with
      | Seq_empty, Seq_empty, Seq_empty, Seq_empty -> true
      | Seq_empty, e1', Seq_empty, e2' -> eq_mod_empty x e1' e2'
      | Seq_val v, e1', Seq_val v', e2' when v = v' -> eq_mod_empty x e1' e2'
      | Seq_empty, e1', Seq_var qid, e2' | Seq_var qid,  e1', Seq_empty, e2'
        when SvSet.exists (fun x -> x = qid) x.empty_seq ->
        eq_mod_empty x e1' e2'
      | (Seq_var qid as e1h), e1', (Seq_var qid' as e2h), e2' when
        eq_mod_empty x e1h Seq_empty && eq_mod_empty x e2h Seq_empty ->
        eq_mod_empty x e1' e2'
      | Seq_var qid, e1', Seq_var qid', _ when eq_mod_empty x (Seq_var qid) Seq_empty ->
        eq_mod_empty x e1' e2
      | Seq_var qid, _, Seq_var qid', e2' when eq_mod_empty x (Seq_var qid') Seq_empty ->
        eq_mod_empty x e1 e2'
      | exception SidemostAtomIsSort | _ -> false

    let upper_bnd ?(hint=SvPrSet.empty) (k: join_kind) (x1: t) (x2: t) : t =
      let x1, x2 = match_info x1 x2 in
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf_bin ~label:"upper_bnd pre" x1 x2;
        Log.debug "Treating %s in seq_domain\n-left:\n%a\n-right:\n%a"
          (match k with Jjoin -> "⊔" | Jwiden -> "∇" | Jdweak -> "weak" )
          (fpri_seq "  ") x1 (fpri_seq "  ") x2;
      end;
      if is_bot x1 then x2 else if is_bot x2 then x1 else
      let _ = assert (x1.root_seqvs = x2.root_seqvs) in
      let x1, x2 = infer_strictly x1 x2, infer_strictly x2 x1 in
      let seqvs = SvSet.inter x1.seqvs x2.seqvs in
      let rel = Equiv.join x1.rel x2.rel  in
      (* since most field are constraints in the abstract value, the join is the
       * intersection of this constraints *)
      let alias =
        SvMap.fold
          (fun sv seqv acc ->
            if (not @@ SvMap.mem sv x2.node_alias) then acc
            else if seqv <> SvMap.find sv x2.node_alias then acc
            else SvMap.add sv seqv acc)
          x1.node_alias SvMap.empty in
      let sorted = SvSet.inter x1.sorted x2.sorted in
      let get_defs seqv x =
        let seqv = Equiv.get_repr x.rel seqv in
        let def = SvMap.find_opt seqv x.seq_def |> Option.get_default [] in
        let eq_class =
          Equiv.get_eq_class seqv x.rel
          |> SvSet.remove seqv
          |> SvSet.filter (fun sv -> Equiv.is_repr sv rel) in
        SvSet.fold (fun seqv acc -> (Seq_var seqv)::acc) eq_class def in
      let unify_defs seqv =
        if !Flags.flag_dbg_dom_seq then Log.debug "Treating Q[%a]" sv_fpr seqv;
        let defs_1 = get_defs seqv x1 in
        let defs_2 = get_defs seqv x2 in
        let acc = equal_to_sort seqv defs_1 x2 [] in
        let acc = equal_to_sort seqv defs_2 x1 acc in
        List.fold_left
          (fun acc e1 ->
            List.fold_left
              (fun acc e2 ->
                match unify ~hint x1 x2 seqv e1 e2 with
                | None | Some (Seq_var _) -> acc
                | Some e when List.mem e acc -> acc
                | Some e -> e::acc)
              acc defs_2)
          acc defs_1 in
      let def =
        SvSet.fold
          (fun seqv acc ->
            let defs = unify_defs seqv in
            if defs = []
              then acc
              else SvMap.add seqv defs acc)
          Equiv.(get_all_rep rel) SvMap.empty in
      let bounds = SvSet.fold
        (fun seqv acc ->
          let low1, up1 = SvMap.find seqv x1.bounds in
          let low2, up2 = SvMap.find seqv x2.bounds in
          let low, up =
            if is_empty x1 seqv (* && k <> Jwiden *) then
              low2, up2
            else if is_empty x2 seqv then
              low1, up1
            else
              SvSet.inter low1 low2, SvSet.inter up1 up2 in
          SvMap.add seqv (low, up) acc)
        x1.seqvs SvMap.empty in
      let mem = D.upper_bnd k x1.mem x2.mem in
      let mem: D.t = if SvMap.is_empty x1.info then mem else
        (* We only saturate bounds for seqv that was an empty sequence on one
         * side. *)
        let empty_one_side = SvSet.filter
          (fun seqv ->
            SvSet.mem seqv x1.empty_seq <> SvSet.mem seqv x2.empty_seq)
          x1.seqvs in
        SvSet.fold
          (fun seqv mem  ->
            let low, up = SvMap.find seqv bounds in
            let {min; max; _ } = SvMap.find seqv x1.info in
            mem
            (* assume low <= min *)
            |> SvSet.fold (fun low -> add_is_le low min) low
            (* assume max <= up *)
            |> SvSet.fold (fun up -> add_is_le max up) up)
        empty_one_side mem in
      let x =
        { x1 with
          seqvs = seqvs ;
          node_alias = alias ;
          seq_def = def ;
          mem = mem ;
          empty_seq = SvSet.inter x1.empty_seq x2.empty_seq ;
          non_empty_seq = SvSet.inter x1.non_empty_seq x2.non_empty_seq ;
          sorted = sorted ;
          bounds = bounds ;
          rel = rel } in
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf_bin ~label:"upper_bnd post left" x1 x;
        assert_wf_bin ~label:"upper_bnd post right" x2 x;
        Log.debug "Result =\n%a" (fpri_seq "  ") x;
      end;
      x

    let set_guard (cons: set_cons) (x: t) =
      { x with mem = D.set_guard cons x.mem }

    (** [infer_from_sorted min max seqv expr x]
        tries to infer some numerical constraints knowing that [expr]
         (= [seqv]) is sorted and [min] and [max] are its bounds.
        - [seqv = ... Q_1 ... Q_2 ...] => [max(Q_1) <= min(Q_2)]
        - [seqv = ... Q_1 ... [d] ... Q_2 ...] => [max(Q_1) <= d <= min(Q_2)] *)
    let infer_from_sorted (min: sv) (max: sv) (seqv: sv) (e: seq_expr) (x: t) =
      (** [aux_forward first last_bound bounds e x]
        @param first equals [true] iff we only encountered:
        - empty sequences variables before
        - no atomic values.
        @param bounds contains all lower bounds of the new sequences:
        - previous atoms,
        - max values of previous sequence variables,
        - the minimum value of [seqv]
        - the [last_bound] parameter.
        @param last_bound is the most precise lower bound
        @param e is the remaining of the expression to visit
        @param x the accumulator abstract state  *)
      let rec aux_forward (first: bool) (last_bound: sv) (bounds: SvSet.t)
        (e: seq_expr) (x: t) =
        match empty_leftmost e with
        | Seq_val n, e ->
          let bounds = SvSet.add n bounds in
          let cons_bound =
            if first
              then Nc_cons (EQ, Ne_var n, Ne_var last_bound)
              else Nc_cons (SUPEQ, Ne_var n, Ne_var last_bound) in
          let mem = D.guard true cons_bound x.mem in
          { x with mem }
          |> aux_forward false n bounds e
        | Seq_var seqv, e ->
          let {min; max; _} = SvMap.find seqv x.info in
          let lows, ups = SvMap.find seqv x.bounds in
          (* We only add num inequalities on bounds we didn't know previously *)
          let to_add = SvSet.diff bounds lows in
          let lows = SvSet.union lows bounds in
          let mem = SvSet.fold
            (fun bound mem -> add_is_le bound min mem)
            to_add
            x.mem in
          let x = { x with mem } in
          let x = { x with bounds = SvMap.add seqv (lows, ups) x.bounds } in
          let bounds = SvSet.add max bounds in
          let x, last =
            (* if [seqv != []], we can use [max(seqv)]
              as a more precise last_bound *)
            if not @@ is_non_empty x seqv then x, last_bound else
            let mem = if first then
              (* If [Q = Q0._] and Q0 != [], then minQ = minQ0*)
              let cons = Nc_cons (EQ, Ne_var min, Ne_var last_bound) in
              D.guard true cons mem
            else mem in
            { x with mem }, max in
          let first = first && is_empty x seqv in
          x |> aux_forward first last bounds e
        | Seq_sort _, e -> aux_forward false last_bound bounds e x
        | Seq_empty, _ | exception SidemostAtomIsSort -> x
        | Seq_Concat _, _ -> assert false in
      let rec aux_backward (first: bool) (last_bound: sv) (bounds: SvSet.t)
        (e: seq_expr) (x: t) =
        match empty_rightmost e with
        | Seq_val n, e ->
          let bounds = SvSet.add n bounds in
          let cons_bound =
            if first
              then Nc_cons (EQ, Ne_var last_bound, Ne_var n)
              else Nc_cons (SUPEQ, Ne_var last_bound, Ne_var n) in
          let mem = D.guard true cons_bound x.mem in
          { x with mem }
          |> aux_backward false n bounds e
        | Seq_var seqv, e ->
          let {min; max; _} = SvMap.find seqv x.info in
          let lows, ups = SvMap.find seqv x.bounds in
          (* We only add num inequalities on bounds we didn't know previously *)
          let to_add = SvSet.diff bounds ups in
          let ups = SvSet.union ups bounds in
          let mem = SvSet.fold
            (fun bound mem -> add_is_le max bound mem)
            to_add
            x.mem in
          let x = { x with mem } in
          let x = { x with bounds = SvMap.add seqv (lows, ups) x.bounds } in
          let bounds = SvSet.add min bounds in
          let x, last =
            if not @@ is_non_empty x seqv then x, last_bound else
            (* if [seqv != []], we can use [min(seqv)]
              as a more precise last_bound *)
            let mem = if first then
              (* If [Q = Q0._] and Q0 != [], then maxQ = maxQ0*)
              let cons = Nc_cons (EQ, Ne_var last_bound, Ne_var max) in
              D.guard true cons x.mem
              else mem in
            { x with mem }, min in
          let first = first && is_empty x seqv in
          aux_backward first last bounds e x
        | Seq_sort _, e -> aux_backward false last_bound bounds e x
        | Seq_empty, _ | exception SidemostAtomIsSort -> x
        | Seq_Concat _, _ -> assert false in
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "  Calling infer_from_sorted %a = %a [%a; %a]"
        seqv_fpr seqv seq_expr_fpr e sv_fpr min sv_fpr max in
      if SvMap.is_empty x.info || not !Flags.enable_colv_bnd then x else
      x
      |> aux_forward  true min (SvSet.singleton min) e
      |> aux_backward true max (SvSet.singleton max) e

    (** [reduce_empty_non_empty x] reduces [x] to [bot] if
        {emmpty_seq} n {non_empty_seq} ≠ ∅*)
    let reduce_empty_non_empty x =
      if not @@ SvSet.disjoint x.empty_seq x.non_empty_seq
        then raise Bottom
        else x

    (** exception used when we find a interesting constrint in
      detect_cycling *)
    exception PathFound of (sv * seq_expr) list

    (** [guard_non_empty seqv x] tags [seqv] as non_empty.
        and forall definitions S = E, (if seqv \in fv(E)), recursviely *)
    let rec guard_non_empty seqv x =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling guard_non_empty Q[%a]" sv_fpr seqv;
      if is_bot x || SvSet.mem seqv x.empty_seq then raise Bottom else
      if SvSet.mem seqv x.non_empty_seq then x else
      let non_empty_seq =
        Equiv.get_eq_class seqv x.rel
        |> SvSet.union x.non_empty_seq in
      let mem = if SvMap.is_empty x.info then x.mem else
        let {size; min; max} = SvMap.find seqv x.info in
        let low, up = SvMap.find seqv x.bounds in
        let cons_l = Nc_cons (DISEQ, Ne_var size, Ne_csti 0) in
        x.mem
        |> D.guard true cons_l
        |> add_is_le min max
        |> SvSet.fold (fun low -> add_is_le low min) low
        |> SvSet.fold (fun up -> add_is_le max up) up in
      let x = { x with non_empty_seq; mem } in
      let in_def def =
        SvSet.mem seqv @@ seq_expr_seqvs def in
      let x = SvMap.fold
        (fun seqv' defs x ->
          if List.exists in_def defs
            then guard_non_empty seqv' x
            else x)
        x.seq_def x in
      x |> reduce_empty_non_empty

    (** [check_unique e sv x] assuming that [e] has the following form
      [S1__Si[x]S1'__Sj'], it reurns [true] iff.
        + for all [i], [x] |= [max(Si) < sv]
        + for all [j], [x] |= [max(Sj') > sv]
      It returns [false] on other cases.
      It does not require [e] to be sorted. *)
    (* Can be genralized s.t. [max(Si) < sv || min(Si) > sv] on the two sides *)
    let rec check_unique (e: seq_expr) (sv: sv) (x: t) =
      let rec aux side e =
        (* The booleann argument is:
          + [false] if we are on the left part of the expression
          + [true] if we are on the right part. *)
        match empty_leftmost e with
        | Seq_val _, _ when side -> false
        | Seq_val v , e' -> aux true e'
        | Seq_var v, e' when SvSet.mem v x.empty_seq -> aux side e'
        | Seq_var v, e' ->
        begin
          if SvMap.is_empty x.info then false else
          let {min; max; _} = SvMap.find v x.info in
          match side with
          | false ->
              (sv_is_lt x max sv)  && aux side e'
          | true ->
              (sv_is_lt x sv min ) && aux side e'
        end
        | Seq_empty, Seq_empty -> true
        | Seq_empty, e' -> aux side e'
        | exception SidemostAtomIsSort | _ -> false in
      aux false e

    (** [guard_right_most s1 s2 e v x] apply the following rule:
        + Assuming that [x |= v < min(S2)]
        + If [s1.[v].s2 = e] where [e = e1.[v].e2] and [x |= v ∉ e2]
        + Then [s1 = e1] and [s2 = e2] *)
    (* Can be generlized as a side_most rule for both sides *)
    let rec guard_right_most (s1: sv) (s2: sv) (e: seq_expr) (v: sv) (x :t) =
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "guard_right_most called %a %a" sv_fpr s1 sv_fpr s2 in
      let rec aux side l1 l2 e =
        match empty_leftmost e with
        | Seq_empty, Seq_empty ->
          let _ = if !Flags.flag_dbg_dom_seq then
            Log.debug "guard_right_most triggered %a %a" sv_fpr s1 sv_fpr s2 in
          x
          |> guard_def s1 (normalise (Seq_Concat (List.rev l1)))
          |> guard_def s2 (normalise (Seq_Concat (List.rev l2)))
        | Seq_val v', e2 when v' = v && side -> aux false l1 l2 e2
        | e1, e2 when side -> aux true (e1 :: l1) l2 e2
        | Seq_val v', e2 ->
            if sv_is_lt x v v' then
              aux false l1 (Seq_val v' :: l2) e2
            else x
        | Seq_empty, e2 ->
          aux false l1 l2 e2
        | Seq_var s, e2 ->
            let {min; max; _} = SvMap.find s x.info in
            if sv_is_lt x v min  then
              aux false l1 (Seq_var s :: l2) e2
            else x
        | exception SidemostAtomIsSort | _ -> x in
      aux true [] [] e

    (** [guard_in e1 e2 x] tries to infer some proprties knowing that
        [e1 = e2] *)
    and guard_in e1 e2 x : t =
      let _ = if !Flags.flag_dbg_dom_seq then
        Log.debug "guard_in called on %a, %a" seq_expr_fpr e1 seq_expr_fpr e2 in
      let has_one_val l =
        1 = List.fold_left
          (fun i x -> match x with Seq_val _ -> i+1 | _ -> i) 0 l in
      match e1, e2 with
      | Seq_Concat l1, Seq_Concat l2 when
        List.length l1 >= 3 && List.length l2 >= 3
        && has_one_val l1  && has_one_val l2 ->
          let f x = match x with Seq_val _ -> true | _ -> false in
          let s1 = List.find f l1 in
          let s2 = List.find f l2 in
          if List.length l1 = 3 && s1 = s2 then
            match l1 with
            | Seq_var s1 :: Seq_val v :: Seq_var s2 :: [] ->
                let {min; max; _} = SvMap.find s2 x.info in
                if sv_is_lt x v min then
                  guard_right_most s1 s2 e2 v x
                else
                  x
            | _ -> x
          else
            (match s1, s2 with
            | Seq_val v1, Seq_val v2 ->
              if check_unique e2 v1 x then
                let _ = if !Flags.flag_dbg_dom_seq then
                  Log.debug "guard_unique triggered on %a, %a"
                    seq_expr_fpr e1 seq_expr_fpr e2 in
                let cons = Nc_cons (EQ, Ne_var v1, Ne_var v2) in
                let mem = D.guard true cons x.mem in
                {x with mem }
              else x
            | _ -> x)
      | _ -> x

    (** [guard_empty seqv x] adds constraints that [seqv == []].
        \forall Qi \in fv(E), [Q == []], recursivly*)
    and guard_empty (seqv: sv) (x: t) =
      let re_guard_expr x =
        SvMap.fold
          (fun key l xacc ->
            match l with
            | [] -> xacc
            | hd :: tl ->
              (List.fold_left (fun acc x -> guard_expr hd x acc) xacc tl)
            ) x.seq_def x in
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling guard_empty Q[%a] { %a }" sv_fpr seqv seqvset_fpr x.empty_seq;
      if is_bot x || SvSet.mem seqv x.non_empty_seq then raise Bottom else
      if SvSet.mem seqv x.empty_seq then x else
      let old_empty_seq = x.empty_seq in
      let x = { x with empty_seq = SvSet.add seqv old_empty_seq } in
      if not @@ SvSet.is_empty old_empty_seq then
        let seqv_empty = SvSet.choose old_empty_seq in
        x
        |> guard_equal seqv seqv_empty
        |> re_guard_expr
      else
        let mem = if SvMap.is_empty x.info then x.mem else
          let seqv_l = (SvMap.find seqv x.info).size in
          let cons_l = Nc_cons (EQ, Ne_var seqv_l, Ne_csti 0) in
          let set_cons = to_set_cons (Seq_equal (Seq_empty, Seq_var seqv)) in
          x.mem
          |> D.guard true cons_l
          |> D.set_guard set_cons in
        let empty_seq = Equiv.get_eq_class seqv x.rel in
        let x = { x with mem; empty_seq } in
        let x = add_def seqv Seq_empty x in
        let defs = SvMap.find seqv x.seq_def in
        List.fold_left guard_empty_expr x defs
        |> infer_empty
        |> guard_sorted seqv
        |> re_guard_expr


    and guard_empty_expr x seq_expr =
      if is_bot x then raise Bottom else
      match normalise seq_expr with
      | Seq_var seqv' -> guard_empty seqv' x
      | Seq_empty -> x
      | Seq_val v -> raise Bottom
      | Seq_Concat l ->
        List.fold_left guard_empty_expr x l
      | Seq_sort e -> guard_empty_expr x e

    (** [guard_sorted seqv x] adds constraints that [seqv] is sortedin [x]:
        - if [seqv] != [], it assumes min(seqv) <= max(seqv)
        - calls [infer_from_sorted] on all the definitions of [seqv]
        - assumes if [seqv] = [E(seqv')], then is calls recursively on [seqv']
        *)
    and guard_sorted (seqv: sv) (x: t) : t =
      if is_bot x then raise Bottom else
      let seqv = Equiv.get_repr x.rel seqv in
      if SvSet.mem seqv x.sorted then x else
      let class_eqs = Equiv.get_eq_class seqv x.rel in
      let sorted = SvSet.union x.sorted class_eqs in
      let x = { x with sorted } in
      let x = if is_non_empty x seqv && SvMap.mem seqv x.info then
        let {min; max; _} = SvMap.find seqv x.info in
        let card = compute_card x in
        let rec iter n acc =
          match iterate_on_def x card (Seq_var seqv) n with
          | None -> acc
          | Some e ->
            acc |> infer_from_sorted min max seqv e |> iter (n+1) in
        iter 1 x
      else
        x in
      let defs = SvMap.find_opt seqv x.seq_def |> Option.get_default [] in
      let seqvs = List.fold_left
        (fun acc def -> seq_expr_seqvs_out_sort def |> SvSet.union acc)
        SvSet.empty
        defs in
      SvSet.fold guard_sorted seqvs x

    (** [infer_empty x] tries to infer simple constraints appearing when we substitute empty seqv by [[]].
        Simple constraints are:
        - Qi = []
        - Qi = Qj
        - Qi = Nj
        We don't remove any constraints *)
    and infer_empty (x: t) : t =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling infer_empty";
      if is_bot x then raise Bottom else
      let rewrite_seqv seqv =
        if SvSet.mem seqv x.empty_seq
          then Seq_empty
          else Seq_var seqv in
      let rewrite expr =
        expr
        |> q_expr_map_aux rewrite_seqv (fun sv -> Seq_val sv)
        |> normalise in
      let iter_defs seqv acc def =
        match rewrite def with
        | Seq_Concat _ -> acc
        | Seq_empty | Seq_val _ | Seq_var _ | Seq_sort _ as expr ->
            Seq_equal (Seq_var seqv, expr)::acc in
      let infer =
        SvMap.fold
          (fun seqv defs acc ->
            List.fold_left (iter_defs seqv) acc defs)
          x.seq_def [] in
      if !Flags.flag_dbg_dom_seq then
        Log.debug "  Inferred constraints:\n  %a"
        (gen_list_fpr "_" seq_cons_fpr "\n  ") infer;
      let guard_cons x = function
        | Seq_equal (Seq_var seqv, Seq_var seqv') -> guard_equal seqv seqv' x
        | Seq_equal (Seq_var seqv, Seq_empty) -> guard_empty seqv x
        | Seq_equal (Seq_var seqv, Seq_val sv) -> guard_alias sv seqv x
        | Seq_equal (Seq_var seqv, Seq_sort _) -> guard_sorted seqv x
        | _ -> assert false in
      List.fold_left guard_cons x infer


    (** [remove_cyclic x] remove all cyclic constraint in [x].
        When we find a cycle of constraints we inline them.
        We get a constraint [Qi = E] with Qi ∈ fv(E).
        - If there an symbolic variable: we reduce the abstract value to bottom.
        - otherwise we set deduce that ∀ Qj ∈ fv(E) \ [{Qi}], [Qj = []].
        We remove the last constraint in the cycle.
        *)
    and remove_cyclic (x: t) : t =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling detect_cycling";
      if is_bot x then raise Bottom else
      let rec get_cons path =
        match path with
        | (seqv, def)::_ when SvSet.mem seqv (seq_expr_seqvs def) ->
          let cons = seqv, def in
          cons
        | (seqv, def)::(seqv', def')::q ->
          let rewrite_seqv seqv0 =
            if seqv0 = seqv then def else Seq_var seqv0 in
          let def'' = q_expr_map_aux rewrite_seqv (fun sv -> Seq_val sv) def' in
          let path = (seqv', def'')::q in
            get_cons path
        | [] | _::_ -> assert false in
      let rec aux visited path to_visit =
        let visited = SvSet.add to_visit visited in
        match SvMap.find_opt to_visit x.seq_def with
        | None -> ()
        | Some defs ->
          List.iter
            (fun def ->
              let seqvs = seq_expr_seqvs def in
              let path = (to_visit, def)::path in
              if SvSet.disjoint seqvs visited then
                SvSet.iter (aux visited path) seqvs
              else
                raise (PathFound path))
            defs in
      try
        SvSet.iter (aux SvSet.empty []) x.seqvs;
        if !Flags.flag_dbg_dom_seq then
          Log.debug "No cycle found !";
        x
      with PathFound path ->
        (* Firs we remove the last definition *)
        let seqv, def = List.hd path in
        if !Flags.flag_dbg_dom_seq then
          Log.debug "  Cycle found ! Removing constraint\n  Q[%a] = %a"
            sv_fpr seqv seq_expr_fpr def;
        let defs = SvMap.find seqv x.seq_def |> List.filter ( (<>) def ) in
        let seq_def = if defs = []
          then SvMap.remove seqv x.seq_def
          else SvMap.add seqv defs x.seq_def in
        let x = { x with seq_def } in
        (* Then we use the path to infer other constraints *)
        let seqv, def = get_cons path in
        let seqvs = seq_expr_seqvs def in
        let _ = assert (SvSet.mem seqv seqvs) in
        let svs = seq_expr_svs def in
        if not @@ SvSet.is_empty svs then raise Bottom else
        let seqvs = SvSet.remove seqv seqvs in
        SvSet.fold guard_empty seqvs x
        |> remove_cyclic

    (** [fold_expr map x] tries to reduce all definitions in [x] according to
        the mappings [seqv_r |-> expr_r]. *)
    and fold_expr (to_rewrite: seq_expr SvMap.t) (x: t) : t =
      match SvMap.choose_opt to_rewrite with
      | None -> x
      | Some (seqv, Seq_var seqv') ->
        let to_rewrite = SvMap.remove seqv to_rewrite in
        let x = guard_equal seqv seqv' x in
        fold_expr to_rewrite x
        (* Call guard_equal*)
      | Some (seqv, Seq_val sv') ->
        (* This case occurs when we match an alias definition: we can skip it
           since this alias is already folded.
           Indded this case cannot be triggered by rewriting + recursiv call *)
        let to_rewrite = SvMap.remove seqv to_rewrite in
        fold_expr to_rewrite x
      | Some (seqv_r, expr_r) ->
        if !Flags.flag_dbg_dom_seq then
          Log.debug "Calling fold_expr Q[%a] <- %a"
            sv_fpr seqv_r seq_expr_fpr expr_r;
        let to_rewrite = SvMap.remove seqv_r to_rewrite in
        assert (expr_r <> (Seq_var seqv_r));
        let seq_def, to_rewrite = SvMap.fold
          (fun seqv defs (seq_def, to_rewrite) ->
            if seqv = seqv_r
              (* We ignore the definitions of seqv_r. *)
              then (SvMap.add seqv defs seq_def, to_rewrite)
            else
            let defs, to_rewrite =
              List.fold_left
                (fun (defs, to_rewrite) expr ->
                  let expr', rewritten = factorize seqv_r expr_r expr in
                  let to_rewrite = if rewritten
                    then SvMap.add seqv expr' to_rewrite
                    else to_rewrite in
                  let defs = match expr' with
                    | Seq_var s -> assert (s = seqv_r); defs
                      (* JG: We should infer that seqv=seqv_r*)
                    | _ ->
                      if List.for_all
                        (fun e -> not @@ eq_mod_empty x e expr') defs then expr' :: defs
                      else defs in
                  defs, to_rewrite)
                ([], to_rewrite) defs in
            SvMap.add seqv defs seq_def, to_rewrite)
          x.seq_def (SvMap.empty, to_rewrite) in
        fold_expr to_rewrite {x with seq_def}

    (** [guard_alias sv seqv x] adds the constraint [S[seqv] == N[sv]] in [x].
        - adds sv |-> seqv in alias
        - rewrite all constraints: Defs(N[sv] <- Q[seqv]) *)
    and guard_alias (sv: sv) (seqv: sv) (x: t) : t =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling guard_alias Q[%a] = N[%a]"
          sv_fpr seqv sv_fpr sv;
      if is_bot x then raise Bottom else
      match SvMap.find_opt sv x.node_alias with
      | Some seqv' -> guard_equal seqv seqv' x
      | None ->
      let mem =
        if SvMap.is_empty x.info then x.mem else
        let info = SvMap.find seqv x.info in
        let cons_l = Nc_cons (EQ, Ne_var info.size, Ne_csti 1) in
        let cons_max = Nc_cons (EQ, Ne_var info.min, Ne_var sv) in
        let cons_min = Nc_cons (EQ, Ne_var info.max, Ne_var sv) in
        let cons_set = to_set_cons (Seq_equal (Seq_val sv, Seq_var seqv)) in
        x.mem
        |> D.guard true cons_l
        |> D.guard true cons_max
        |> D.guard true cons_min
        |> D.set_guard cons_set in
      let replace = q_expr_map_aux
        (fun seqv -> Seq_var seqv)
        (fun node -> if node = sv then Seq_var seqv else Seq_val node) in
      let seq_def = SvMap.map (List.map replace) x.seq_def in
      { x with
        mem;
        seq_def;
        node_alias = SvMap.add sv seqv x.node_alias }
      |> add_bound seqv sv true
      |> add_bound seqv sv false
      |> add_def seqv (Seq_val sv)
      |> remove_cyclic
      |> guard_non_empty seqv
      |> guard_sorted seqv

    (** [guard_expr e1 e2 x] tries to infer some constraints by assuming
      [e1 == e2]
      - [v1].e1' == [v2].e2' |- v1 == v2 & e1' == e2'
      - Q1.e1' == Q2.e2' & Q1 == Q2 |- e1' == e2'
      - Q1.[] == Q2.[] |- Q1 == Q2
      - Q1.[] == [v1].[] |- Q1 == [v1]
      WE have the same for the rightmost element *)
    and guard_expr (e1: seq_expr) (e2: seq_expr) (x: t) : t =
      (* [side] is
        -[true] when we want to explore the left side,
        -[false] when we want to explore the right side
         (the left one has already been checked without success)*)
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling guard_expr\n  %a = %a"
        seq_expr_fpr e1 seq_expr_fpr e2;
      if is_bot x then raise Bottom else
      if eq_mod_empty x e1 e2 then x else
      let x = guard_in e1 e2 x |> guard_in e2 e1 in
      let rec aux side expr1 expr2 x =
        match (empty_sidemost_double side expr1 expr2) with
        (* e == [] |- \forall Q \in fv(e), Q = [] *)
        | Seq_empty, _, e, e'
        | e, e', Seq_empty, _ ->
          let x = guard_empty_expr x e in
          guard_empty_expr x e'
        (* [v1].e1' == [v2].e2' |- v1 == v2 & e1' == e2' *)
        | Seq_val n1, e1', Seq_val n2, e2' ->
          let cons = Nc_cons (EQ, Ne_var n1, Ne_var n2) in
          let mem = if n1 = n2 || D.sat x.mem cons
            then x.mem
            else D.guard true cons x.mem in
          let x = { x with mem } in
          guard_expr e1' e2' x
        (* - Q1.e1' == Q2.e2' & Q1 == Q2 |- e1' == e2' *)
        | Seq_var n1, e1', Seq_var n2, e2'
          when Equiv.are_equiv n1 n2 x.rel ->
          guard_expr e1' e2' x
        (* - Q1.e1' == Q2.e2' & |Q1| == |Q2| |- Q1 == Q2 & e1' == e2' *)
        | Seq_var n1, e1', Seq_var n2, e2'
          when has_same_length n1 n2 x ->
          x
          |> guard_equal n1 n2
          |> guard_expr e1' e2'
        (* - Q1.[] == Q2.[] |- Q1 == Q2 *)
        | Seq_var n1, Seq_empty, Seq_var n2, Seq_empty ->
          guard_equal n1 n2 x
        (* - Q1.[] == [v1].[] |- Q1 == [v1] *)
        | Seq_val n, Seq_empty, Seq_var seqv, Seq_empty
        | Seq_var seqv, Seq_empty, Seq_val n, Seq_empty ->
          guard_alias n seqv x
        (* - Q1.e1' == e2 & Q1 == [] |- e1' == e2 *)
        | Seq_var n1, e1', _, _
          when SvSet.mem n1 x.empty_seq ->
          guard_expr e1' e2 x
        (* - e1 == Q2.e2' & Q2 == [] |- e1 == e2' *)
        | _, _, Seq_var n2, e2'
          when SvSet.mem n2 x.empty_seq ->
          guard_expr e1 e2' x
        (* - Q1.[] == E2 |- Q1 == E2 *)
        | Seq_var seqv, Seq_empty,_, _ ->
          guard_def seqv e2 x
        (* - E1 == Q2.[] |- Q2 == E1 *)
        | _, _, Seq_var seqv, Seq_empty ->
          guard_def seqv e1 x
        | _, _, _, _ | exception SidemostAtomIsSort ->
          if side
            then aux false expr1 expr2 x
            else x in
      aux true e1 e2 x

    (** [guard_defs seqv expr x] apply {!guard_expr} between [expr] and
        all definition of [seqv] in [x]. *)
    and guard_defs seqv expr x =
      List.fold_left
        (fun x def -> guard_expr expr def x)
        x
        (SvMap.find_opt seqv x.seq_def |> Option.get_default [])

    (** [guard_equals seqv1 seqv2 x] add constraint [seqv1 == seqv2] in[x].
        It merges their definition (and apply [guard_expr]).
        It performs some reduction if one of seqv is empty/non-empty *)
    and guard_equal seqv1 seqv2 x =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "Calling guard_equal Q[%a] = Q[%a]"
        sv_fpr seqv1 sv_fpr seqv2;
      if is_bot x then bot else
      if seqv1 = seqv2 || Equiv.are_equiv seqv1 seqv2 x.rel then x else
      (* We add size constraints *)
      let mem = if SvMap.is_empty x.info then x.mem else
        let {min = min1; max = max1; size = size1} = SvMap.find seqv1 x.info in
        let {min = min2; max = max2; size = size2} = SvMap.find seqv2 x.info in
        let mem =
          if !Flags.enable_colv_bnd then
            x.mem (* JG: Check soundness with empty sequence problem! *)
            |> D.guard ~no_apron:true true
                (Nc_cons (EQ, Ne_var min1, Ne_var min2))
            |> D.guard ~no_apron:true true
                (Nc_cons (EQ, Ne_var max1, Ne_var max2))
          else x.mem in
        let mem =
          if !Flags.enable_colv_len then
          (* Since size constraints cannot be handled solely by lighter numerical
           * domains, we rely have to on apron for them ! *)
            D.guard true (Nc_cons (EQ, Ne_var size1, Ne_var size2)) mem
          else mem in
        mem in
      let mem =
        let set_cons = to_set_cons (Seq_equal (Seq_var seqv1, Seq_var seqv2)) in
        D.set_guard set_cons mem in
      let x = {x with mem } in
      let x = if is_sorted x seqv1 || is_sorted x seqv2
        then x |> guard_sorted seqv1 |> guard_sorted seqv2
        else x in
      (* We update the relation *)
      let rel = Equiv.add_equiv seqv1 seqv2 x.rel in
      let x = { x with rel } in
      (* We rewrite all definitions according to new class representative *)
      let rep = Equiv.get_repr rel seqv1 in
      let rewrite_seqv seqv =
        if seqv = seqv1 || seqv = seqv2 then rep else seqv in
      let rewrite expr = q_expr_map rewrite_seqv (fun sv -> sv) expr in
      let def1 = SvMap.find_opt seqv1 x.seq_def |> Option.get_default [] in
      let def2 = SvMap.find_opt seqv2 x.seq_def |> Option.get_default [] in
      let seq_def =
        x.seq_def
        |> SvMap.remove seqv1
        |> SvMap.remove seqv2
        |> SvMap.map (List.map rewrite) in
      let x = { x with seq_def } |> remove_cyclic in
      (* We rewrite the alias *)
      let node_alias = SvMap.map rewrite_seqv x.node_alias in
      let x = { x with node_alias } in
      (* We perform some reduction on the definitions of seqv1 & seqv2 *)
      let x = match def1, def2 with
        | [], [] -> x
        | [], def | def, [] -> (* def <> [] *)
          let def = List.map rewrite def in
          let seq_def = SvMap.add rep def x.seq_def in
          { x with seq_def } |> remove_cyclic
        | def, def' ->
          let def = List.map rewrite def in
          let def' = List.map rewrite def' in
          let seq_def = SvMap.add rep def x.seq_def in
          let x = { x with seq_def } |> remove_cyclic in
          List.fold_left
            (fun acc expr ->
              acc
              |> guard_defs rep expr
              |> add_def rep expr)
            x def'
          |> remove_cyclic in
      (* We merge the bounds of the two *)
      let bounds =
        let low1, up1 = SvMap.find seqv1 x.bounds in
        let low2, up2 = SvMap.find seqv2 x.bounds in
        let low = SvSet.union low1 low2 in
        let up = SvSet.union up1 up2 in
        x.bounds
        |> SvMap.add seqv1 (low, up)
        |> SvMap.add seqv2 (low, up) in
      let x = { x with bounds } in
      let x = if SvSet.mem seqv1 x.sorted || SvSet.mem seqv2 x.sorted then
        let sorted = SvSet.union x.sorted (Equiv.get_eq_class seqv1 x.rel) in
        {x with sorted }
        else x in
      (* If one seqv is empty then both should be *)
      let x =
        if SvSet.mem seqv2 x.empty_seq || SvSet.mem seqv1 x.empty_seq then
          (* We already merged equivalence class of seqv1 and seqv2 *)
          let empty_seq = Equiv.get_eq_class seqv1 x.rel in
          let mem = x.mem
            |> D.set_guard (S_eq (S_var seqv1, S_empty))
            |> D.set_guard (S_eq (S_var seqv2, S_empty)) in
          { x with empty_seq; mem }
        else x in
      (* If one seqv is non-empty then both should be *)
      let x =
        if SvSet.mem seqv2 x.non_empty_seq then
          guard_non_empty seqv1 x
        else if SvSet.mem seqv1 x.non_empty_seq then
          guard_non_empty seqv2 x
        else x in
      x

    (** [guard_upper_def def seqv x] tries to reapply {!guard_expr}
        on all possible definition of seqvs' depanding on [seqv] in [x].
        The [def] argument allows to check which definition
        possibly results of the unfolding of the newy added definition.*)
    and guard_upper_def (seqv: sv) (def: seq_expr) (x: t) : t =
      if is_bot x then bot else
      let card = compute_card x in
      let seqvs_def = seq_expr_seqvs def in
      let svs_def = seq_expr_svs def in
      let is_from_unfold def' =
        SvSet.subset seqvs_def @@ (seq_expr_seqvs def')
        && SvSet.subset svs_def @@ (seq_expr_svs def') in
      let contains_seqv def' =
        SvSet.mem seqv @@ seq_expr_seqvs def' in
      let depending = SvSet.filter
        (fun seqv' -> depends_on x seqv (Seq_var seqv'))
        (Equiv.get_all_rep x.rel) in
      let iterate_on_seqv (seqv': sv) (x: t) =
        let get_def = iterate_on_def x card (Seq_var seqv') in
        let aux_sorted =
          if SvSet.mem seqv' x.sorted && SvMap.mem seqv' x.info then
            let { min; max; _ } = SvMap.find seqv' x.info in
            fun def acc -> infer_from_sorted min max seqv' def acc
          else
            fun _ acc -> acc in
        let rec iterate n from_unfold all_def acc =
          match get_def n with
          (* We have iterated on all definitions: we stop *)
          | None -> acc
          (* When the definition contains the original seqv, it is of no use to
             us. *)
          | Some def_n when contains_seqv def_n ->
            iterate (n+1) from_unfold all_def acc
          (* When the definition returned def_n contains all the seqv in def,
             this means that def_n results probably of the unfolding of colv=def,
             so we compare def_n with all previously memoized definitions.
             and we add it in the from_unfold list *)
          | Some def_n when is_from_unfold def_n ->
            let acc = List.fold_left (fun acc def' -> guard_expr def_n def' acc)
              acc
              all_def in
            let acc = aux_sorted def_n acc in
            iterate (n+1) (def_n::from_unfold) (def_n::all_def) acc
          (* Otherwise, we compare the new definition with all definitions in
             the from_unfold list *)
          | Some def_n ->
            let acc = List.fold_left (fun acc def' -> guard_expr def_n def' acc)
              acc
              from_unfold in
            iterate (n+1) from_unfold (def_n::all_def) acc in
        iterate 1 [] [] x in
      SvSet.fold iterate_on_seqv depending x

    and guard_def (seqv: sv) (expr: seq_expr) (x: t) : t =
      if !Flags.flag_dbg_dom_seq then
        Log.debug "calling guard_def Q[%a] = %a"
        sv_fpr seqv seq_expr_fpr expr;
      if is_bot x then raise Bottom else
      let expr = nf_expr x expr in
      let already_known =
        expr = Seq_var seqv in
      if already_known then x else
      match expr with | Seq_var seqv' -> guard_equal seqv seqv' x | _ ->
      let x =
        x
        |> add_def seqv expr
        |> remove_cyclic
        |> fold_expr (SvMap.singleton seqv expr)
        |> remove_cyclic (*try remove*) in
      let x = if is_empty x seqv
        then guard_empty seqv x
        else x in
      let x = if is_non_empty x seqv
        then guard_non_empty seqv x
        else x in
      let mem = if SvMap.is_empty x.info then x.mem else
        let seqv_l = (SvMap.find seqv x.info).size in
        let expr_l = get_expr_len x.info expr in
        let cons = Nc_cons (EQ, Ne_var seqv_l, expr_l) in
        let expr = remove_empty x.empty_seq expr in
        let cons_set = to_set_cons (Seq_equal (Seq_var seqv, expr)) in
        let mem =
          if !Flags.enable_colv_len then
            D.guard true cons x.mem
          else x.mem in
        D.set_guard cons_set mem in
      let x = { x with mem } in
      let x = if SvMap.is_empty x.info then x else
        let {min; max; _ } = SvMap.find seqv x.info in
        let mem = SvSet.fold
          (fun sv mem -> mem |> add_is_le min sv |> add_is_le sv max)
          (seq_expr_svs expr)
          x.mem in
        let low, up = SvMap.find seqv x.bounds in
        let bounds, mem = SvSet.fold
          (fun seqv' (bounds, mem) ->
            let low', up' = SvMap.find seqv' bounds in
            let low' = SvSet.union low low' in
            let up' = SvSet.union up up' in
            let bounds = SvMap.add seqv' (low', up') bounds in
            let {min = min'; max = max'} = SvMap.find seqv' x.info in
            let mem = mem |> add_is_le min min' |> add_is_le max' max in
            bounds, mem)
          (seq_expr_seqvs expr)
          (x.bounds, mem) in
        { x with mem; bounds } in
      let x =
        if is_sorted_expr x expr then
          guard_sorted seqv x
        else if is_sorted x seqv && not @@ SvMap.is_empty x.info then
          let {min; max; _} = SvMap.find seqv x.info in
          let x = infer_from_sorted min max seqv expr x in
          let seqvs = seq_expr_seqvs_out_sort expr in
          SvSet.fold guard_sorted seqvs x
        else x in
      (* If one seqv in the new definition is empty then we perform reductions
         of infer_empty function *)
      let x = if SvSet.disjoint x.empty_seq (seq_expr_seqvs expr)
        then infer_empty x
        else x in
      let equal_sorted =
        if SvMap.is_empty x.info || is_sorted x seqv then SvSet.empty else
        SvSet.fold
          (fun sorted acc ->
            if have_same_content x (Seq_var sorted) expr
              then SvSet.add sorted acc
              else acc)
          x.sorted SvSet.empty in
      let equal_sorted = SvSet.elements equal_sorted in
      let x = match equal_sorted with
      | [] -> x
      | sorted0::q ->
        let sorted0 = Equiv.get_repr x.rel sorted0 in
        let x = guard_def sorted0 (Seq_sort (Seq_var seqv)) x in
        List.fold_left
          (fun x sorted -> guard_equal sorted0 sorted x) x q in
      guard_upper_def seqv expr x

    let guard ?(no_apron=false) (b: bool) (cons: n_cons) (x: t) =
      let x = empty_new_seqvs x in
      let x = { x with
        mem = D.guard ~no_apron b cons x.mem ;
        need_bnds_sat = true } in
      match cons with
      | Nc_cons(EQ, Ne_var e1, Ne_var e2) ->
        SvMap.fold
          (fun key l x ->
            let l =
              List.filter
                (fun e -> match e with
                  | Seq_Concat l ->
                    List.exists (fun x -> x = Seq_val e1 || x = Seq_val e2 ) l
                  | _ -> false)
                l in
              let _, x =
               List.fold_left
                (fun (l,x) e ->
                  match l with
                  | _ :: tl -> (tl, List.fold (fun x e' -> guard_expr e e' x) x tl)
                  | _ -> ([], x)) (l, x) l
                  in x)
          x.seq_def
          x
      | _ -> x

    let guard ?(no_apron=false) b c x =
      try guard ~no_apron b c x
      with Bottom -> bot

    let seq_guard (cons: seq_cons) (x: t) =
      if is_bot x then raise Bottom else
      match nf_cons x cons with
      (* S[i] = S[j] *)
      | Seq_equal (Seq_var v1, Seq_var v2) ->
          guard_equal v1 v2 x
      (* S[i] = [] *)
      (* We only add the definition, without removing S[i] in previous
       * definitions since it can be a hint for upper bound *)
      | Seq_equal (Seq_var v, Seq_empty)
      | Seq_equal (Seq_empty, Seq_var v) ->
          guard_empty v x
      (* S[i] = N[j] *)
      (* We add the alias N[j] |-> S[i]
       * and we replace each occurence of n[j] by S[i] in previous definitions
       * We don't lose any information but it is a huge hint for upper-bnd *)
      | Seq_equal (Seq_var seqv, Seq_val sv)
      | Seq_equal (Seq_val sv, Seq_var seqv) ->
          x
          |> guard_defs seqv (Seq_val sv)
          |> guard_alias sv seqv
      (* S[i] = sort(S[i]) *)
      | Seq_equal (Seq_var v, Seq_sort (Seq_var v0))
      | Seq_equal (Seq_sort (Seq_var v0), Seq_var v) when v = v0 ->
          guard_sorted v x
      (* S[i] = ... *)
      | Seq_equal (Seq_var v, expr)
      | Seq_equal (expr, Seq_var v) ->
          guard_def v expr x
      | _ as cons ->
          Log.fatal_exn "The following constraint is not a definition :\n%a"
            seq_cons_fpr cons

    let seq_guard (cons: seq_cons) (x: t) =
      if !Flags.flag_dbg_dom_seq then begin
        Log.debug "Seq guard\ncons: %a\nx:\n%a"
          seq_cons_fpr cons (fpri_seq "  ") x;
        assert_wf ~label:"seq_guard pre" x;
      end;
      let x = empty_new_seqvs x in
      let res =
        try seq_guard cons x
        with Bottom -> bot in
      if !Flags.flag_dbg_dom_seq then begin
        Log.debug "Result:\n%a"
          (t_fpri SvMap.empty "  ") res;
        assert_wf ~label:"seq_guard post" res;
      end;
      { res with need_bnds_sat = true }

    let symvars_srename
        ?(mark: bool = true)
        (om: (Offs.t * sv) OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (opt_colv_map: colv_map_complete option)
        (t: t): t =
      (* First of all: we saturate the bounds before we lose some info *)
      let t = if SvSet.is_empty nm.nm_rem
        then t
        else saturate_bounds t in
      (* First we remove all variables *)
      let t = match opt_colv_map with
        | None -> t
        | Some {cmc_seq = {sm_rem; _ }; _} ->
          SvSet.fold colv_rem sm_rem t in
      let t = SvSet.fold sv_discard nm.nm_rem t in
      (* Then, we do the renaiming part *)
      let rename_sv sv =
        try fst @@ SvMap.find sv nm.nm_map
        with Not_found -> sv in
      match opt_colv_map with
      | None ->
          let rename = q_expr_map (fun x -> x) rename_sv in
          let defs =
            SvMap.fold
              (fun key def acc ->
                let def' = List.map rename def in
                SvMap.add key def' acc)
              t.seq_def SvMap.empty in
          let alias =
            SvMap.fold
              (fun sv seqv acc ->
                let sv' = rename_sv sv in
                SvMap.add sv' seqv acc)
              t.node_alias SvMap.empty in
          let info = colv_info_rename nm opt_colv_map CK_seq t.info in
          let bounds =
            SvMap.map
              (fun (low, up) -> SvSet.map rename_sv low, SvSet.map rename_sv up)
              t.bounds in
          let mem = D.symvars_srename ~mark:mark om nm opt_colv_map t.mem in
          { t with
            mem = mem ;
            seq_def = defs ;
            node_alias = alias;
            info ;
            bounds }
      | Some { cmc_set = setv_mapping; cmc_seq = seqv_mapping} ->
          let rename_seqv seqv =
            try fst @@ SvMap.find seqv seqv_mapping.sm_map
            with Not_found -> Log.fatal_exn "%s\n%a Not_found in\n%a" __LOC__
              seqv_fpr seqv
              Col_utils.colv_mapping_fpr seqv_mapping in
          let t = (* tmp fix *)
            let to_rem = SvSet.filter
              (fun seqv -> not @@ SvMap.mem seqv seqv_mapping.sm_map )
              t.seqvs in
            if SvSet.is_empty to_rem then t else
            SvSet.fold colv_rem to_rem t in
          let rel = Equiv.rename rename_seqv t.rel in
          let info = colv_info_rename nm opt_colv_map CK_seq t.info in
          let bounds = SvMap.fold
              (fun colv (low, up) acc ->
                let colv = rename_seqv colv in
                let low = SvSet.map rename_sv low in
                let up = SvSet.map rename_sv up in
                SvMap.add colv (low, up) acc)
            t.bounds SvMap.empty in
          let seqvs = SvSet.map rename_seqv t.seqvs in
          let sorted = SvSet.map rename_seqv t.sorted in
          let empty_seq = SvSet.map rename_seqv t.empty_seq in
          let non_empty_seq = SvSet.map rename_seqv t.non_empty_seq in
          (* The remaining of the abstract value must be renamed while respecting
             the class-representative-only invariant *)
          let rename_seqv seqv =
            seqv |> rename_seqv |> Equiv.get_repr rel in
          let rename = q_expr_map rename_seqv rename_sv in
          let seq_def = SvMap.fold
            (fun key def acc ->
              let def' = List.map rename def
              and key' = rename_seqv key in
              SvMap.add key' def' acc)
            t.seq_def SvMap.empty in
          let node_alias = SvMap.fold
            (fun sv seqv acc ->
              let sv' = rename_sv sv
              and seqv' = rename_seqv seqv in
              SvMap.add sv' seqv' acc)
            t.node_alias SvMap.empty in
          let mem =
            (* We don't update the field setv_mapping.sm_rem
              since seqv are already discared in colv_rem *)
            let sm_map =
              SvMap.fold
              (fun seqv (nseqv, others) acc ->
                (* Should be true since seqv & setv have the same keygen *)
                let _ = assert (not @@ SvMap.mem seqv acc) in
                SvMap.add seqv (nseqv, others) acc)
              seqv_mapping.sm_map
              setv_mapping.sm_map in
            let setv_mapping = {setv_mapping with sm_map } in
            let opt_colv_map =
              Some {cmc_set = setv_mapping; cmc_seq = seqv_mapping} in
            D.symvars_srename ~mark:mark om nm opt_colv_map t.mem in
          let t =
            { t with
              seqvs ;
              sorted ;
              empty_seq ;
              non_empty_seq ;
              mem ;
              node_alias ;
              rel ;
              seq_def ;
              info ;
              bounds } in
          let t =
            (* This case can be encountererd if there is cyclic definitions *)
            SvMap.fold
              (fun _ (seqv, equals) acc ->
                let _ = assert (SvSet.disjoint acc.seqvs equals) in
                let seqvs = SvSet.union equals acc.seqvs in
                SvSet.fold
                  (fun seqv' acc ->
                    let cons = Seq_equal (Seq_var seqv, Seq_var seqv') in
                    let info = match SvMap.find_opt seqv acc.info with
                      | None -> acc.info
                      | Some cinfo -> SvMap.add seqv' cinfo acc.info in
                    seq_guard cons {acc with info})
                  equals { acc with seqvs})
              seqv_mapping.sm_map t in
          t

    let symvars_srename
        ?(mark: bool = true)
        (om: (Offs.t * sv) OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (opt_colv_map: colv_map_complete option)
        (t: t): t =
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf ~label:"symvars_srename pre" t;
        Log.debug "x:\n%a" (fpri_seq "  PRE  ") t;
        Log.debug "Node Mapping:\n%a" Nd_utils.node_mapping_fpr nm;
        if Option.is_some  opt_colv_map then
          Log.debug "Seq Mapping:\n%a"
            Col_utils.colv_mapping_fpr Option.(get opt_colv_map).cmc_seq ;
      end;
      let res = symvars_srename ~mark om nm opt_colv_map t in
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf ~label:"symvars_srename post" res;
        Log.debug "x:\n%a" (fpri_seq "  POST ") res;
      end;
      res

    let is_le (x1: t) (x2: t) (f: sv -> sv -> bool) : bool =
      let x1, x2 = match_info x1 x2 in
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf_bin ~label:"is_le pre" x1 x2;
        Log.debug "Treating is_le in seq_domain\n-left:\n%a\n-right:\n%a"
          (fpri_seq "  ") x1 (fpri_seq "  ") x2;
      end;
      (* Utility function to know which part of the inclusion fails/suceeds *)
      let res_log test res =
        if !Flags.flag_dbg_dom_seq then Log.debug "Result (%s): %b" test res;
        res in
      let is_le_empty () =
        let is_empty seqv =
          SvSet.mem seqv x1.empty_seq ||
          let cons = Seq_equal (Seq_var seqv, Seq_empty) in
          seq_sat cons x1 in
        SvSet.for_all is_empty x2.empty_seq
        |> res_log "empty" in
      let is_le_nempty () =
        SvSet.subset x2.non_empty_seq x1.non_empty_seq
        |> res_log "non_empty" in
      (* TODO maybe add the possiblity to do a seq_sat when this fails *)
      let is_le_rel () =
        Equiv.is_le x1.rel x2.rel
        |> res_log "rel" in
      let is_le_def () =
        let test_seqv seqv defs2 =
          let defs1 = SvMap.find_opt seqv x1.seq_def |> Option.get_default [] in
          List.for_all
            (fun def2 ->
              List.mem def2 defs1 ||
                let cons = Seq_equal (Seq_var seqv, def2) in
                seq_sat cons x1)
            defs2 in
        SvMap.for_all test_seqv x2.seq_def
        |> res_log "definitions" in
      let is_le_num () =
        D.is_le x1.mem x2.mem f
        |> res_log "num" in
      is_le_empty ()
      && is_le_nempty ()
      && is_le_rel ()
      && is_le_def ()
      && is_le_num ()

    let assign (sv: sv) (expr: n_expr) (x: t) : t =
      { x with mem = D.assign sv expr x.mem }

    let write_sub (sd: sub_dest) (size: int) (rv: n_rval) (t: t): t =
      { t with mem = D.write_sub sd size rv t.mem }

    let simplify_n_expr (t: t) (e: n_expr): n_expr =
      D.simplify_n_expr t.mem e

    let sv_array_add (i:sv) (size: int) (fields: int list) (t:t): t =
      { t with mem = D.sv_array_add i size fields t.mem }

    let sv_array_address_add (id: sv) (t: t): t =
      { t with mem = D.sv_array_address_add id t.mem }

    let is_array_address (id: sv) (t: t): bool =
      D.is_array_address id t.mem

    let sv_array_deref (id: sv) (off: Offs.t) (t: t): (t * sv) list =
      D.sv_array_deref id off t.mem
      |> List.map (fun (x, i) -> ({ t with mem = x}, i))

    let sv_array_materialize (id: sv) (off: Offs.t) (t: t): t * sv =
      let x, i = D.sv_array_materialize id off t.mem in
      { t with mem = x }, i

    let is_submem_address (i: sv) (t: t): bool =
      D.is_submem_address i t.mem

    let is_submem_content (i: sv) (t: t): bool =
      D.is_submem_content i t.mem

    let submem_read (sat: n_cons -> bool) (addr: sv) (off: Offs.t) (sz: int)
        (x: t): Offs.svo =
      D.submem_read sat addr off sz x.mem

    let submem_deref (sat: n_cons -> bool) (addr: sv) (off: Offs.t) (sz: int)
        (x: t): Offs.svo =
      D.submem_deref sat addr off sz x.mem

    let submem_localize (ig: sv) (x: t): sub_localize =
      D.submem_localize ig x.mem

    let submem_bind (isub: sv) (iglo: sv) (off: Offs.t) (t: t): t * Offs.svo =
      let x, node = D.submem_bind isub iglo off t.mem in
      { t with mem = x }, node

    let check (op: vcheck_op) (x: t): bool =
      D.check op x.mem

    let assume (op: vassume_op) (x: t): t =
      { x with mem = D.assume op x.mem }

    let unfold (cont: sv) (n: sv) (udir: unfold_dir) (t: t)
        : (sv SvMap.t * t) list =
      D.unfold cont n udir t.mem
      |> List.map (fun (set, x) -> set, { t with mem = x })

    let expand (oid: sv) (sv: sv) (x: t): t =
      { x with mem = D.expand oid sv x.mem }

    let compact (lid: sv) (rid: sv) (x: t): t =
      { x with mem = D.compact lid rid x.mem }

    let meet (x1: t) (x2: t) : t =
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf_bin ~label:"meet pre" x1 x2;
        Log.debug "Treating ⊓ in seq_domain\n-left:\n%a\n-right:\n%a"
          (fpri_seq "  ") x1 (fpri_seq "  ") x2;
      end;
      let _ = assert (x1.root_seqvs = x2.root_seqvs) in
      let def = SvMap.union (fun _ e1 e2 -> Some (e1@e2)) x1.seq_def x2.seq_def in
      let alias =
        SvMap.union (fun _ e1 e2 -> Some e2) x1.node_alias x2.node_alias in
      let x = { x1 with
          mem = D.meet x1.mem x2.mem ;
          seqvs = SvSet.union x1.seqvs x2.seqvs ;
          seq_def = def ;
          node_alias = alias ;
          empty_seq = SvSet.union x1.empty_seq x2.empty_seq ;
          non_empty_seq = SvSet.union x1.non_empty_seq x2.non_empty_seq } in
      if !Flags.flag_dbg_dom_seq then begin
        assert_wf_bin ~label:"meet post left" x1 x;
        assert_wf_bin ~label:"meet post right" x2 x;
        Log.debug "Result =\n%a" (fpri_seq "  ") x;
      end;
      x

    let sv_forget (sv: sv) (x: t) : t =
      { x with mem = D.sv_forget sv x.mem }

    let sv_bound (sv: sv) (x: t) : interval =
      D.sv_bound sv x.mem

    let get_svs (x: t) : SvSet.t =
      D.get_svs x.mem

    let get_eq_class (sv: sv) (x: t) : SvSet.t =
      D.get_eq_class sv x.mem

    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      D.get_upper_svs id x.mem
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      D.get_lower_svs id x.mem
  end: DOM_VALCOL)
