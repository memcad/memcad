(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_add_ineqs.ml
 **       add inequalities (x<y) with more efficient representation
 ** Xavier Rival, 2022/12/06 *)
open Apron
open Data_structures
open Lib
open Sv_def

open Nd_sig

open Nd_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "nd_+ineq" and level = Log_level.DEBUG end)


(** Constructor functors:
 **  Basic abstract domain, just for strict inequalities *)
module Add_ineqs = functor (D: DOM_NUM_NB) ->
  (struct
    let module_name = "nd_add_ineqs"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()
    (** Internal flags *)
    let do_sanity_checks = false
    (** Abstract elements type *)
    type t =
        { t_u:   D.t;             (* underlying *)
          t_inf: SvSet.t SvMap.t; (* x => S where forall y in S, y < x *)
          t_sup: SvSet.t SvMap.t; (* x => S where forall y in S, y > x *) }
    (** Bottom element (might be detected) *)
    let is_bot (t: t): bool = D.is_bot t.t_u
    (** Top element *)
    let top = { t_u   = D.top;
                t_inf = SvMap.empty;
                t_sup = SvMap.empty }
    (** Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) (fmt: form) (t: t): unit =
      let hasineq =
        try
          let f m =
            SvMap.iter (fun _ s -> if s != SvSet.empty then raise Stop) m in
          f t.t_inf; false
        with Stop -> true in
      D.t_fpri sn ind fmt t.t_u;
      if hasineq then
        begin
          F.fprintf fmt "%sInequalities:\n" ind;
          SvMap.iter
            (fun sv svs ->
              F.fprintf fmt "%s  %a < %a\n" ind nsv_fpr sv nsvset_fpr svs
            ) t.t_inf
        end
    (** Low level utilities *)
    let svsetmap_get (sv: sv) (m: SvSet.t SvMap.t): SvSet.t =
      try SvMap.find sv m with Not_found -> SvSet.empty
    (** Sanity check *)
    let sanity_check (msg: string) (t: t): unit =
      if do_sanity_checks then
        let error ste =
          Log.info "Sanity check:\n%a" (t_fpri SvMap.empty "  ") t;
          Log.fatal_exn "Sanity check issue <%s:%s>" msg ste in
        (* checking that the SVs that appear all exist *)
        let svs = D.get_svs t.t_u in
        let f st m =
          SvMap.iter
            (fun sv s ->
              if not (SvSet.mem sv svs) then
                begin
                  Log.info "sv: %a" nsv_fpr sv;
                  error (F.asprintf "%s:sv" st)
                end;
              if not (SvSet.subset s svs) then error (F.asprintf "%s:svs" st);
            ) m in
        f "inf" t.t_inf;
        f "sup" t.t_sup;
        (* checking that the two maps are dual *)
        let f st m0 m1 =
          SvMap.iter
            (fun svl s0 ->
              SvSet.iter
                (fun svr ->
                  let s1 = svsetmap_get svr m1 in
                  if not (SvSet.mem svl s1) then error (F.asprintf "dual:%s" st)
                ) s0
            ) m0 in
        f "inf=>sup" t.t_inf t.t_sup;
        f "sup=>inf" t.t_sup t.t_inf
    (** Utility functions *)
    let sv_drop_info (i: sv) (t: t): t =
      let f m = SvMap.map (fun a -> SvSet.remove i a) (SvMap.remove i m) in
      { t with
        t_inf = f t.t_inf ;
        t_sup = f t.t_sup }
    let ctrs_meet (m0: SvSet.t SvMap.t) (m1: SvSet.t SvMap.t): SvSet.t SvMap.t =
      SvMap.fold
        (fun sv svs0 acc ->
          try
            let svs1 = SvMap.find sv m1 in
            SvMap.add sv (SvSet.inter svs0 svs1) acc
          with Not_found -> acc
        ) m0 SvMap.empty
    let t_get_inf (sv: sv) (t: t): SvSet.t = svsetmap_get sv t.t_inf
    let t_get_sup (sv: sv) (t: t): SvSet.t = svsetmap_get sv t.t_sup
    (* add constraint sv0 < sv1 *)
    let rec ctr_add_inf (sv0: sv) (sv1: sv) (t: t): t =
      let sv0sup = t_get_sup sv0 t in
      if SvSet.mem sv1 sv0sup then
        (* the constraint is already here *)
        t
      else (* really add the constraint *)
        let sv0inf = t_get_inf sv0 t
        and sv0sup = t_get_sup sv0 t
        and sv1inf = t_get_inf sv1 t
        and sv1sup = t_get_sup sv1 t in
        let t = { t with
                  t_inf = SvMap.add sv1 (SvSet.add sv0 sv1inf) t.t_inf;
                  t_sup = SvMap.add sv0 (SvSet.add sv1 sv0sup) t.t_sup } in
        let t = SvSet.fold (fun sv -> ctr_add_inf sv0 sv) sv1sup t in
        let t = SvSet.fold (fun sv -> ctr_add_inf sv sv1) sv0inf t in
        sanity_check "post,ctr_add_inf" t;
        t
    let ctr_check_not (sv0: sv) (sv1: sv) (t: t): t =
      let svinf = svsetmap_get sv1 t.t_inf in
      if SvSet.mem sv0 svinf then raise Bottom
      else t
    (** Variables managemement *)
    let sv_add (i: sv) (t: t): t =
      assert (not (SvMap.mem i t.t_inf || SvMap.mem i t.t_sup));
      { t with t_u = D.sv_add i t.t_u }
    let sv_rem  (i: sv) (t: t): t =
      let t = sv_drop_info i t in
      let t = { t with t_u = D.sv_rem i t.t_u } in
      sanity_check (F.asprintf "post,sv_rem[%a]" nsv_fpr i) t;
      t
    let vars_srename (nr: 'a node_mapping) (t: t): t =
      (* remove the nodes in nm_rem *)
      let t = SvSet.fold sv_drop_info nr.nm_rem t in
      (* rename the nodes in nm_map *)
      let do_sv (sv: sv): SvSet.t =
        try
          let sv0, svs0 = SvMap.find sv nr.nm_map in
          SvSet.add sv0 svs0
        with Not_found -> SvSet.singleton sv in
      let do_svset (svs: SvSet.t): SvSet.t =
        SvSet.fold (fun sv acc -> SvSet.union (do_sv sv) acc) svs SvSet.empty in
      let do_svmap (svm: SvSet.t SvMap.t): SvSet.t SvMap.t =
        SvMap.fold
          (fun sv svs acc ->
            let nsv = do_sv sv in
            let nsvs = do_svset svs in
            SvSet.fold (fun sv -> SvMap.add sv nsvs) nsv acc
          ) svm SvMap.empty in
      { t_u   = D.vars_srename nr t.t_u ;
        t_inf = do_svmap t.t_inf ;
        t_sup = do_svmap t.t_sup }
    let check_nodes (s: SvSet.t) (t: t): bool =
      try
        let check sv =
          if not (SvSet.mem sv s) then
            begin
              Log.warn "check fails: %a" nsv_fpr sv;
              raise False
            end in
        let f m =
          SvMap.iter
            (fun sv ->
              check sv;
              SvSet.iter check
            ) m in
        f t.t_inf;
        f t.t_sup;
        D.check_nodes s t.t_u
      with Not_found -> false
    let nodes_filter (nkeep: SvSet.t) (t: t): t =
      let f m =
        let m = SvMap.map (SvSet.inter nkeep) m in
        SvMap.fold
          (fun sv _ a ->
            if SvSet.mem sv nkeep then a
            else SvMap.remove sv a
          ) m m in
      let t = { t_u = D.nodes_filter nkeep t.t_u ;
                t_inf = f t.t_inf ;
                t_sup = f t.t_sup } in
      sanity_check "post,nodes_filter" t;
      t
    (** Comparison, Join and Widening operators *)
    let is_le (t0: t) (t1: t) (sat_diseq: sv -> sv -> bool): bool =
      try
        (* tries to prove that all constraints in ml are in mr *)
        let inc_ctrs (ml: SvSet.t SvMap.t) (mr: SvSet.t SvMap.t) =
          SvMap.iter
            (fun sv svsl ->
              let svsr = svsetmap_get sv mr in
              if not (SvSet.subset svsl svsr) then raise False
            ) ml in
        inc_ctrs t1.t_inf t0.t_inf;
        inc_ctrs t1.t_sup t0.t_sup;
        D.is_le t0.t_u t1.t_u sat_diseq
      with False -> false
    let join (t0: t) (t1: t): t =
      let t = { t_u   = D.join t0.t_u t1.t_u;
                t_inf = ctrs_meet t0.t_inf t1.t_inf;
                t_sup = ctrs_meet t0.t_sup t1.t_sup; } in
      sanity_check "post,join" t;
      t
    let widen (t0: t) (t1: t): t =
      let t = { t_u   = D.widen t0.t_u t1.t_u;
                t_inf = ctrs_meet t0.t_inf t1.t_inf;
                t_sup = ctrs_meet t0.t_sup t1.t_sup; } in
      sanity_check "post,widen" t;
      t
    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat (ncons: n_cons) (t: t): bool =
      let blocal =
        match ncons with
        | Nc_cons ((Tcons1.SUP | Tcons1.SUPEQ), Ne_var v0, Ne_var v1) ->
            (* try to prove v0 > v1 with our domain,
             * otherwise fall back to the underlying domain *)
            let v0sup = t_get_sup v0 t in
            SvSet.mem v1 v0sup || D.sat ncons t.t_u
          | _ -> false in
      if do_sanity_checks && blocal then
        begin
          if not (D.sat ncons t.t_u) then
            Log.fatal_exn "Error";
          true
        end
      else blocal || D.sat ncons t.t_u
    (** Transfer functions *)
    let assign (dst: sv) (expr: n_expr) (t: t): t =
      let t = sv_drop_info dst t in
      let t = { t with t_u = D.assign dst expr t.t_u } in
      sanity_check "post-assign" t;
      t
    let guard ?(no_apron=false) (b: bool) (ncons: n_cons) (t: t): t =
      let t = { t with t_u = D.guard ~no_apron b ncons t.t_u } in
      let t =
        match b, ncons with
        | true , Nc_cons (Tcons1.SUP  , Ne_var v0, Ne_var v1)
        | false, Nc_cons (Tcons1.SUPEQ, Ne_var v1, Ne_var v0) ->
            ctr_add_inf v1 v0 t (* register v1 < v0 *)
        | false, Nc_cons (Tcons1.SUP  , Ne_var v0, Ne_var v1)
        | true , Nc_cons (Tcons1.EQ   , Ne_var v0, Ne_var v1)
        | true , Nc_cons (Tcons1.SUPEQ, Ne_var v1, Ne_var v0) ->
            ctr_check_not v0 v1 t (* reduce to _|_ if v0 < v1 *)
        | _ -> t in
      sanity_check "post-guard" t;
      t
    (** Utilities for the abstract domain *)
    let simplify_n_expr (t: t): n_expr -> n_expr = D.simplify_n_expr t.t_u
    (** Summarizing dimensions related operations *)
    let expand (id: sv) (nid: sv) (t: t): t =
      { t with t_u = D.expand id nid t.t_u }
    let compact (lid: sv) (rid: sv) (t: t): t =
      { t with t_u = D.compact lid rid t.t_u }
    (** Conjunction *)
    let meet (t0: t) (t1: t): t =
      Log.todo_exn "meet"
    (** Drop information on an SV *)
    let sv_forget (sv: sv) (x: t): t =
      Log.todo_exn "sv_forget"
    (** Export of range information *)
    let bound_variable (dim: sv) (t: t): interval =
      D.bound_variable dim t.t_u
    (** Extract the set of all SVs *)
    let get_svs (t: t): SvSet.t = D.get_svs t.t_u
    (** Extract all SVs that are equal to a given SV *)
    let get_eq_class (i: sv) (t: t): SvSet.t = D.get_eq_class i t.t_u
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      SvSet.union
        (get_eq_class id x)
        (SvMap.find_opt id x.t_sup |> Option.get_default SvSet.empty)
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      SvSet.union
        (get_eq_class id x)
        (SvMap.find_opt id x.t_inf |> Option.get_default SvSet.empty)
  end: DOM_NUM_NB)



(** Constructor functors:
 **  Abstract domain, for both lax and strict inequalities *)
module Add_ineqsAll = functor (D: DOM_NUM_NB) ->
  (struct
    let module_name = "nd_add_ineqs"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()
    (** Internal flags *)
    let do_debug = false
    let do_sanity_checks = false
    (** Abstract elements type *)
    (* Trivial lax inequalities of the form sv <= sv are not recorded here *)
    type i =             (* inequalities for a variable x *)
        { i_lt: SvSet.t; (* forall y in S, y <  x *)
          i_le: SvSet.t; (* forall y in S, y <= x *)
          i_ge: SvSet.t; (* forall y in S, y >= x *)
          i_gt: SvSet.t  (* forall y in S, y >  x *) }
    (* Abstract states comprise an underlying abstract state
     * and add inequalities *)
    type t =
        { t_u:   D.t;       (* underlying *)
          t_i:   i SvMap.t; (* x => inequalities for variable x *) }
    (** Internal operations on type i *)
    let i_empty = { i_lt = SvSet.empty;
                    i_le = SvSet.empty;
                    i_ge = SvSet.empty;
                    i_gt = SvSet.empty }
    (** Bottom element (might be detected) *)
    let is_bot (t: t): bool = D.is_bot t.t_u
    (** Top element *)
    let top = { t_u = D.top;
                t_i = SvMap.empty }
    (** Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) (fmt: form) (t: t): unit =
      let printall = false in
      let hasineq =
        try
          let f m =
            SvMap.iter (fun _ i -> if i != i_empty then raise Stop) m in
          f t.t_i; false
        with Stop -> true in
      D.t_fpri sn ind fmt t.t_u;
      if printall || hasineq then
        let fpr sv c svs =
          if svs != SvSet.empty then
            F.fprintf fmt "%s  %a %s %a\n" ind nsv_fpr sv c nsvset_fpr svs in
        F.fprintf fmt "%sInequalities:\n" ind;
        SvMap.iter
          (fun sv i ->
            if printall then
              begin
                fpr sv "> " i.i_lt;
                fpr sv ">=" i.i_le;
                fpr sv "<=" i.i_ge;
                fpr sv "< " i.i_gt
              end
            else
              begin
                fpr sv "> " i.i_lt;
                fpr sv ">=" (SvSet.diff i.i_le i.i_lt)
              end
          ) t.t_i
    (** Low level utilities *)
    let find_default (sv: sv) (im: i SvMap.t): i =
      try SvMap.find sv im with Not_found -> i_empty
    (** Sanity check *)
    let sanity_check (msg: string) (t: t): unit =
      if do_sanity_checks then
        let error ste =
          Log.info "Sanity check:\n%a" (t_fpri SvMap.empty "  ") t;
          Log.fatal_exn "Sanity check issue <%s:%s>" msg ste in
        (* checking that the SVs that appear all exist *)
        let svs = D.get_svs t.t_u in
        SvMap.iter
          (fun sv i ->
            if not (SvSet.mem sv svs) then
              error (F.asprintf "sv:%a" nsv_fpr sv);
            let f_svset op s =
              if not (SvSet.subset s svs) then error (F.asprintf "svs:%s" op) in
            f_svset "<"  i.i_lt;
            f_svset "<=" i.i_le;
            f_svset ">=" i.i_ge;
            f_svset ">"  i.i_gt;
          ) t.t_i;
        (* checking that the two maps are dual *)
        SvMap.iter
          (fun svl li ->
            let f st ext s =
              SvSet.iter
                (fun svr ->
                  let i = find_default svr t.t_i in
                  let s1 = ext i in
                  if not (SvSet.mem svl s1) then error (F.asprintf "dual:%s" st)
                ) s in
            f "sup"   (fun i -> i.i_gt) li.i_lt;
            f "inf"   (fun i -> i.i_lt) li.i_gt;
            f "supeq" (fun i -> i.i_ge) li.i_le;
            f "infeq" (fun i -> i.i_le) li.i_ge;
          ) t.t_i
    (** Utility functions *)
    let sv_drop_info (sv: sv) (t: t): t =
      let f_i i = { i_lt = SvSet.remove sv i.i_lt;
                    i_le = SvSet.remove sv i.i_le;
                    i_ge = SvSet.remove sv i.i_ge;
                    i_gt = SvSet.remove sv i.i_gt } in
      { t with t_i = SvMap.map f_i (SvMap.remove sv t.t_i) }
    let im_join (im0: i SvMap.t) (im1: i SvMap.t): i SvMap.t =
      SvMap.fold
        (fun sv i0 acc ->
          try
            let i1 = SvMap.find sv im1 in
            let ij = { i_lt = SvSet.inter i0.i_lt i1.i_lt;
                       i_le = SvSet.inter i0.i_le i1.i_le;
                       i_ge = SvSet.inter i0.i_ge i1.i_ge;
                       i_gt = SvSet.inter i0.i_gt i1.i_gt } in
            SvMap.add sv ij acc
          with Not_found -> acc
        ) im0 SvMap.empty
    (* add constraint svl < svr *)
    let rec add_ctr_inf ~(strict: bool) ~(svl: sv) ~(svr: sv) (t: t): t =
      if do_debug then
        Log.debug "add_ctr_inf: %b, %a, %a\n%a" strict nsv_fpr svl nsv_fpr svr
          (t_fpri SvMap.empty "    ") t;
      sanity_check "pre,add_ctr_inf" t;
      let il = find_default svl t.t_i and ir = find_default svr t.t_i in
      let s = if strict then il.i_gt else il.i_ge in
      if SvSet.mem svr s then
        (* the constraint is already here *)
        t
      else if svl = svr then
        if strict then (* reduction to _|_ *)
          raise Bottom
        else (* do not add redundant sv <= sv *)
          t
      else
        let il = { il with i_ge = SvSet.add svr il.i_ge }
        and ir = { ir with i_le = SvSet.add svl ir.i_le } in
        if strict then
          let il = { il with i_gt = SvSet.add svr il.i_gt }
          and ir = { ir with i_lt = SvSet.add svl ir.i_lt } in
          let t = { t with t_i = SvMap.add svl il (SvMap.add svr ir t.t_i) } in
          (* add  sv < svr  when  sv <= svl < svr *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:true ~svl:sv ~svr) il.i_le t in
          (* add  svl < sv  when  svl < svr <= sv *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:true ~svl ~svr:sv) ir.i_ge t in
          sanity_check "post,add_ctr_inf,strict" t;
          t
        else
          let t = { t with t_i = SvMap.add svl il (SvMap.add svr ir t.t_i) } in
          (* add  sv <= svr  when  sv <= svl <= svr *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:false ~svl:sv ~svr) il.i_le t in
          (* add  sv < svr   when  sv < svl <= svr *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:true ~svl:sv ~svr) il.i_lt t in
          (* add  svl <= sv  when  svl <= svr <= sv *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:false ~svl ~svr:sv) ir.i_ge t in
          (* add  svl < sv   when  svl <= svr < sv *)
          let t =
            SvSet.fold
              (fun sv -> add_ctr_inf ~strict:true ~svl ~svr:sv) ir.i_gt t in
          sanity_check "post,add_ctr_inf,lax" t;
          t
    (** Variables managemement *)
    let sv_add (sv: sv) (t: t): t =
      assert (not (SvMap.mem sv t.t_i));
      let t = { t with t_u = D.sv_add sv t.t_u } in
      sanity_check (F.asprintf "post,sv_add[%a]" nsv_fpr sv) t;
      t
    let sv_rem (sv: sv) (t: t): t =
      let t = sv_drop_info sv t in
      let t = { t with t_u = D.sv_rem sv t.t_u } in
      sanity_check (F.asprintf "post,sv_rem[%a]" nsv_fpr sv) t;
      t
    let vars_srename (nr: 'a node_mapping) (t: t): t =
      (* remove the nodes in nm_rem *)
      let t = SvSet.fold sv_drop_info nr.nm_rem t in
      (* rename the nodes in nm_map *)
      let do_sv (sv: sv): SvSet.t =
        try
          let sv0, svs0 = SvMap.find sv nr.nm_map in
          SvSet.add sv0 svs0
        with Not_found -> SvSet.singleton sv in
      let do_svset (svs: SvSet.t): SvSet.t =
        SvSet.fold (fun sv acc -> SvSet.union (do_sv sv) acc) svs SvSet.empty in
      let do_i (i: i): i =
        { i_lt = do_svset i.i_lt;
          i_le = do_svset i.i_le;
          i_ge = do_svset i.i_ge;
          i_gt = do_svset i.i_gt } in
      let im =
        SvMap.fold
          (fun sv i acc ->
            let nsv = do_sv sv and i = do_i i in
            SvSet.fold (fun sv -> SvMap.add sv i) nsv acc
          ) t.t_i SvMap.empty in
      { t_u = D.vars_srename nr t.t_u ;
        t_i = im }
    let check_nodes (s: SvSet.t) (t: t): bool =
      try
        let check_sv sv =
          if not (SvSet.mem sv s) then
            begin
              Log.warn "check fails, sv: %a" nsv_fpr sv;
              raise False
            end in
        let check_svset svs =
          if not (SvSet.subset svs s) then
            begin
              Log.warn "check fails, svset: %a" nsvset_fpr svs;
              raise False
            end in
        SvMap.iter
          (fun sv i ->
            check_sv sv;
            check_svset i.i_lt;
            check_svset i.i_le;
            check_svset i.i_ge;
            check_svset i.i_gt;
          ) t.t_i;
        D.check_nodes s t.t_u
      with False -> false
    let nodes_filter (nkeep: SvSet.t) (t: t): t =
      let im =
        SvMap.fold
          (fun sv i acc ->
            if SvSet.mem sv nkeep then
              let i = { i_lt = SvSet.inter nkeep i.i_lt;
                        i_le = SvSet.inter nkeep i.i_le;
                        i_ge = SvSet.inter nkeep i.i_ge;
                        i_gt = SvSet.inter nkeep i.i_gt; } in
              SvMap.add sv i acc
            else acc
          ) t.t_i SvMap.empty in
      let t = { t_u = D.nodes_filter nkeep t.t_u ;
                t_i = im; } in
      sanity_check "post,nodes_filter" t;
      t
    (** Comparison, Join and Widening operators *)
    let is_le (t0: t) (t1: t) (sat_diseq: sv -> sv -> bool): bool =
      try
        (* tries to prove that all constraints in t1.t_i are in t0.t_i *)
        SvMap.iter
          (fun sv i1 ->
            let i0 = find_default sv t0.t_i in
            if not (SvSet.subset i1.i_lt i0.i_lt) then raise False;
            if not (SvSet.subset i1.i_le i0.i_le) then raise False;
            if not (SvSet.subset i1.i_ge i0.i_ge) then raise False;
            if not (SvSet.subset i1.i_gt i0.i_gt) then raise False;
          ) t1.t_i;
        D.is_le t0.t_u t1.t_u sat_diseq
      with False -> false
    let join (t0: t) (t1: t): t =
      let t = { t_u = D.join t0.t_u t1.t_u;
                t_i = im_join t0.t_i t1.t_i; } in
      sanity_check "post,join" t;
      t
    let widen (t0: t) (t1: t): t =
      let t = { t_u = D.widen t0.t_u t1.t_u;
                t_i = im_join t0.t_i t1.t_i; } in
      sanity_check "post,widen" t;
      t
    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let internal_sat (ncons: n_cons) (t: t): bool =
      match ncons with
      | Nc_cons (Tcons1.SUP, Ne_var v0, Ne_var v1) ->
          (* try to prove v0 > v1 locally,
           * otherwise fall back to the underlying domain *)
          let i0 = find_default v0 t.t_i in
          SvSet.mem v1 i0.i_lt
      | Nc_cons (Tcons1.SUPEQ, Ne_var v0, Ne_var v1) ->
          (* try to prove v0 >= v1 locally,
           * otherwise fall back to the underlying domain *)
          let i0 = find_default v0 t.t_i in
          (SvSet.mem v1 i0.i_lt || SvSet.mem v1 i0.i_le)
      | Nc_cons (Tcons1.EQ, Ne_var v0, Ne_var v1) ->
          (* try to prove v0 >= v1 and v0 <= v1 locally,
           * otherwise fall back to the underlying domain *)
          let i0 = find_default v0 t.t_i in
          (SvSet.mem v1 i0.i_le && SvSet.mem v1 i0.i_ge)
      | _ -> false
    let sat (ncons: n_cons) (t: t): bool =
      let blocal = internal_sat ncons t in
      if do_sanity_checks && blocal then
        begin
          if not (D.sat ncons t.t_u) then
            Log.fatal_exn "Error, incompatible results";
          true
        end
      else blocal || D.sat ncons t.t_u
    (** Transfer functions *)
    let assign (dst: sv) (expr: n_expr) (t: t): t =
      let t = sv_drop_info dst t in
      let t = { t with t_u = D.assign dst expr t.t_u } in
      sanity_check "post-assign" t;
      t
    let guard ?(no_apron=false) (b: bool) (ncons: n_cons) (t: t): t =
      if b && internal_sat ncons t then t else
      let t = { t with t_u = D.guard ~no_apron b ncons t.t_u } in
      if do_debug then
        Log.info "pre,guard(%a,%b):\n%a" n_cons_fpr ncons b
          (t_fpri SvMap.empty "  ") t;
      (* checks that we cannot derive svl R svr; otherwise, raise bottom *)
      let check_ctr_not_sup ~(strict: bool) ~(svl: sv) ~(svr: sv): unit =
        let op = if strict then Tcons1.SUP else Tcons1.SUPEQ in
        let nc = Nc_cons (op, Ne_var svl, Ne_var svr) in
        if do_debug then
          Log.info "pre,guard-check(%a):\n%a" n_cons_fpr nc
            (t_fpri SvMap.empty "  ") t;
        let b = internal_sat nc t in
        if do_debug then
          Log.info "post,guard-check(%a) => %b\n%a" n_cons_fpr nc b
            (t_fpri SvMap.empty "  ") t;
        if b then raise Bottom in
      let t =
        (* in each case, reduce to _|_ when the opposite inequality holds;
         * then add the constraint(s) if any *)
        match b, ncons with
        | true , Nc_cons (Tcons1.SUP  , Ne_var v0, Ne_var v1)
        | false, Nc_cons (Tcons1.SUPEQ, Ne_var v1, Ne_var v0) ->
            (* first, check that we do not have v1 >= v0 *)
            check_ctr_not_sup ~strict:false ~svl:v1 ~svr:v0;
            (* add constraint v1 < v0 *)
            add_ctr_inf ~strict:true ~svl:v1 ~svr: v0 t
        | true , Nc_cons (Tcons1.SUPEQ, Ne_var v0, Ne_var v1)
        | false, Nc_cons (Tcons1.SUP  , Ne_var v1, Ne_var v0) ->
            (* first, check that we do not have v0 > v1 *)
            check_ctr_not_sup ~strict:true ~svl:v1 ~svr:v0;
            (* add constraint v1 <= v0 *)
            add_ctr_inf ~strict:false ~svl:v1 ~svr: v0 t
        | true , Nc_cons (Tcons1.EQ   , Ne_var v0, Ne_var v1) ->
            (* first, check that we do not have v0 < v1 or v1 < v0 *)
            check_ctr_not_sup ~strict:true ~svl:v0 ~svr:v1;
            check_ctr_not_sup ~strict:true ~svl:v1 ~svr:v0;
            (* add constraints v0 <= v1 and v1 <= v0 *)
            add_ctr_inf ~strict:false ~svl:v0 ~svr:v1
              (add_ctr_inf ~strict:false ~svl:v1 ~svr:v0 t)
        | _ -> t in
      if do_debug then
        Log.info "post,guard(%a,%b):\n%a" n_cons_fpr ncons b
          (t_fpri SvMap.empty "  ") t;
      sanity_check "post-guard" t;
      t
    (** Utilities for the abstract domain *)
    let simplify_n_expr (t: t): n_expr -> n_expr = D.simplify_n_expr t.t_u
    (** Summarizing dimensions related operations *)
    let expand (id: sv) (nid: sv) (t: t): t =
      { t with t_u = D.expand id nid t.t_u }
    let compact (lid: sv) (rid: sv) (t: t): t =
      { t with t_u = D.compact lid rid t.t_u }
    (** Conjunction *)
    let meet (t0: t) (t1: t): t =
      Log.todo_exn "meet"
    (** Drop information on an SV *)
    let sv_forget (sv: sv) (x: t): t =
      Log.todo_exn "sv_forget"
    (** Export of range information *)
    let bound_variable (dim: sv) (t: t): interval =
      D.bound_variable dim t.t_u
    (** Extract the set of all SVs *)
    let get_svs (t: t): SvSet.t = D.get_svs t.t_u
    (** Extract all SVs that are equal to a given SV *)
    let get_eq_class (i: sv) (t: t): SvSet.t = D.get_eq_class i t.t_u
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      match SvMap.find_opt id x.t_i with
      | None -> SvSet.singleton id
      | Some { i_ge } -> i_ge |> SvSet.add id
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      match SvMap.find_opt id x.t_i with
      | None -> SvSet.singleton id
      | Some { i_le } -> i_le |> SvSet.add id
  end: DOM_NUM_NB)
