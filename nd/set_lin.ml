(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: set_lin.ml
 **       An experiment around the set domain
 ** Xavier Rival, Huisong Li, 2015/03/09 *)
open Data_structures
open Lib
open Sv_def

open Nd_sig
open Col_sig
open Set_sig
open Svenv_sig

open Nd_utils
open Set_utils
open Col_utils
open Sv_utils


(* A temporary flag to debug xlin *)
let tmp_debug_xlin = false
let tmp_debug_join_more_ctrs = false

(** Error handling *)
module Log =
  Logger.Make(struct let section = "s_lin___" and level = Log_level.DEBUG end)

(* Future improvements:
 *  - set up another version of this domain, with direct handling of multiple
 *    lin constraints per SETV (structure (set_lin * set_lin list) SvMap.t)
 *  - add more normalisation constraints for equalities
 *    => if S0 = S1 = S2 => decide to normalise to one of the SIs.
 *)

module Set_lin =
  (struct
    let module_name = "set_lin"
    let config_fpr fmt (): unit = F.fprintf fmt "%s" module_name
    (* Activation of local, internal sanity checks *)
    let sanity_log fmt = Log.sanity_log !Flags.flag_sanity_setlin fmt
    (* Activation of internal reduction of series of lin constraints *)
    let do_reduce_lin = true (* TODO: this seems potentially problematic *)
    (* Activation of extra handling for xlin constraints *)
    let do_preserve_xlin = true (* Seems fine *)

    (** Type of abstract values *)

    (* Linear constraints correspond to an equality of the form
     *     S = { x_0, x_1, ..., x_n } \uplus S_0 \uplus ... \uplus S_k
     * sl_elts is the set of SVs        x_0, x_1, ..., x_n
     * sl_sets is the set of SETVs      S_0, S_1, ..., S_k             *)
    type set_lin =
        { sl_elts: SvSet.t; (* elements *)
          sl_sets: SvSet.t  (* sets *) }

    (* Underlying type for constraints:
     *  u_lin maps S to the linear constraint mentioned above, if any
     *                  (at most one lin constraint per SETV)
     *  u_sub maps S to a set of sets S_0, ..., S_k known to be subsets of S
     *  u_mem maps S to a set of elements x_0, ..., x_n known to belong to S
     *  u_eqs maps S to a set of sets S_0, ..., S_k known to be equal to S
     *  u_xlin records additional linear constraints like u_lin, but that cannot
     *         be represented exactly (u_lin bears at most one constraint per
     *         symbol)
     *)
    type u =
        { u_lin:   set_lin SvMap.t; (* SETV = a linear constraint (if any) *)
          u_sub:   SvSet.t SvMap.t; (* SETV => set of subsets of SETVs *)
          u_mem:   SvSet.t SvMap.t; (* SETV => set of element SVs *)
          u_eqs:   SvSet.t SvMap.t; (* SETV => set of equal SETVs *)
          u_xlin:  (sv * set_lin) list (* additional lin constraints *) }

    (* The type of abstract elements (bottom or non bottom) *)
    type t =
        { t_t:     u option; (* None if _|_, Some u if non bot constraints u *)
          t_roots: SvSet.t;  (* set of root SETVs *) }


    (** Printing *)
    let svline_fpr ?(sn: string SvMap.t = SvMap.empty) (fmt: form) (sv: sv)
        : unit =
      try F.fprintf fmt "%s" (SvMap.find sv sn)
      with Not_found -> nsv_fpr fmt sv
    let set_sv_fpr ?(sn: string SvMap.t = SvMap.empty) fmt s =
      gen_svset_fpr ", " (svline_fpr ~sn) fmt s


    (** Utilities over set_lin *)
    (* Failure to preserve linearity *)
    exception Lin_error of string
    (* Basic values *)
    let sl_empty: set_lin =
      { sl_elts = SvSet.empty;
        sl_sets = SvSet.empty }
    let sl_one_set (i: sv) = { sl_empty with sl_sets = SvSet.singleton i }
    let sl_one_elt (i: sv) = { sl_empty with sl_elts = SvSet.singleton i }
    (* Empty *)
    let sl_is_empty (sl: set_lin): bool =
      sl.sl_elts = SvSet.empty && sl.sl_sets = SvSet.empty
    (** [sl_is_set sl] returns [true] iff. [sl] is only a set *)
    let sl_is_set (sl: set_lin): sv option =
      if SvSet.is_empty sl.sl_elts && SvSet.cardinal sl.sl_sets = 1 then
        Some (SvSet.choose sl.sl_sets)
      else None
    (* Display *)
    let sl_fpr ?(sn: string SvMap.t = SvMap.empty) (fmt: form) (sl: set_lin)
        : unit =
      let lin_setv_fpr =
        gen_svset_fpr " + " setv_fpr in
      if sl.sl_elts = SvSet.empty && sl.sl_sets = SvSet.empty then
        F.fprintf fmt " = empty"
      else
        F.fprintf fmt " = { %a } + %a" (set_sv_fpr ~sn) sl.sl_elts
          lin_setv_fpr sl.sl_sets
    (* Equality of set constraints *)
    let sl_eq c sl =
      SvSet.equal c.sl_elts sl.sl_elts && SvSet.equal c.sl_sets sl.sl_sets
    (* Disjoint union of two set_lin *)
    let sl_add (lin0: set_lin) (lin1: set_lin): set_lin =
      if SvSet.inter lin0.sl_elts lin1.sl_elts = SvSet.empty
          && SvSet.inter lin0.sl_sets lin1.sl_sets = SvSet.empty then
        { sl_elts = SvSet.union lin0.sl_elts lin1.sl_elts;
          sl_sets = SvSet.union lin0.sl_sets lin1.sl_sets }
      else raise (Lin_error "sl_add: non disjoint constraints")
    (* Inclusion of set_lin *)
    let sl_subset (sl0: set_lin) (sl1: set_lin): bool =
      SvSet.subset sl0.sl_elts sl1.sl_elts
        && SvSet.subset sl0.sl_sets sl1.sl_sets
    (* Subtraction of set_lin *)
    let sl_sub (sl0: set_lin) (sl1: set_lin): set_lin =
      if SvSet.subset sl1.sl_elts sl0.sl_elts
          && SvSet.subset sl1.sl_sets sl0.sl_sets then
        { sl_elts = SvSet.diff sl0.sl_elts sl1.sl_elts;
          sl_sets = SvSet.diff sl0.sl_sets sl1.sl_sets }
      else raise (Lin_error "sl_sub: non included constraints")
    let sl_diff (sl0: set_lin) (sl1: set_lin): set_lin * set_lin =
      let sl0_new =
        { sl_elts = SvSet.diff sl0.sl_elts sl1.sl_elts;
          sl_sets = SvSet.diff sl0.sl_sets sl1.sl_sets } in
      let sl1_new =
        { sl_elts = SvSet.diff sl1.sl_elts sl0.sl_elts;
          sl_sets = SvSet.diff sl1.sl_sets sl0.sl_sets } in
      sl0_new, sl1_new
    (* Linearization of an expression into a set_lin *)
    let linearize (ex: set_expr): set_lin option =
      let rec aux = function
        | S_empty -> sl_empty
        | S_node i -> sl_one_elt i
        | S_var i -> sl_one_set i
        | S_union (_, _) -> raise (Lin_error "union")
        | S_uplus (e0, e1) -> sl_add (aux e0) (aux e1) in
      try Some (aux ex) with Lin_error _ -> None
    (* Selection of the most general set_lin (to select which one to drop):
     *  (1) fewer elts;
     *  (2) fewer sets;
     *  (3) higher max SETV (this is a bit a random choice)
     *  (4) second argument if none of the above applies
     * => Returns the selected set_lin + optionally the other one if any *)
    let sl_more_gen (sl0: set_lin) (sl1: set_lin): set_lin * set_lin option =
      let default ( ) = Log.warn "sl_more_gen: default case"; sl1, Some sl0 in
      if sl0 = sl1 then
        sl0, None
      else
        let ce0 = SvSet.cardinal sl0.sl_elts
        and ce1 = SvSet.cardinal sl1.sl_elts in
        if ce0 < ce1 then sl0, Some sl1
        else if ce0 > ce1 then sl1, Some sl0
        else
          let cs0 = SvSet.cardinal sl0.sl_sets
          and cs1 = SvSet.cardinal sl1.sl_sets in
          if cs0 < cs1 then sl0, Some sl1
          else if cs0 > cs1 then sl1, Some sl0
          else if cs0 > 0 then
            let m0 = SvSet.max_elt sl0.sl_sets
            and m1 = SvSet.max_elt sl1.sl_sets in
            Log.warn "sl_more_gen: chooses based on index number...";
            if m0 > m1 then sl0, Some sl1
            else if m0 < m1 then sl1, Some sl0
            else default ( )
          else default ( )
    (* Insertion of an equality *)
    let insert_sl (setv: sv) (sl: set_lin) (m: set_lin SvMap.t)
        : set_lin SvMap.t =
      let f setv0 sl0 =
        if SvSet.mem setv0 sl.sl_sets then
          sl_add { sl with sl_sets = SvSet.remove setv0 sl.sl_sets } sl0
        else sl in
      (* normalize sl *)
      (* TODO: check this function; ignoring the accumulator is strange *)
      let sl = SvMap.fold (fun setv0 sl0 _ -> f setv0 sl0) m sl in
      (* normalize other equalities *)
      let m = SvMap.map (f setv) m in
      (* add the equality *)
      SvMap.add setv sl m


    (** Utilities over type u *)
    (* Pretty-printing, with indentation *)
    let u_fpri (sn: string SvMap.t) (ind: string) (fmt: form) (u: u): unit =
      let lin_setv_fpr =
        gen_svset_fpr " + " (fun fmt -> F.fprintf fmt "S[%a]" sv_fpr) in
      let do_lin ind fmt (i, c) =
        let has_elts = c.sl_elts != SvSet.empty in
        let has_sets = c.sl_sets != SvSet.empty in
        match has_elts, has_sets with
        | false, false ->
            F.fprintf fmt "%s%a = empty\n" ind setv_fpr i
        | false, true ->
            F.fprintf fmt "%s%a = %a\n" ind setv_fpr i lin_setv_fpr c.sl_sets
        | true, false ->
            F.fprintf fmt "%s%a = { %a }\n" ind setv_fpr i
              (set_sv_fpr ~sn) c.sl_elts
        | true, true ->
            F.fprintf fmt "%s%a = { %a } + %a\n" ind setv_fpr i
              (set_sv_fpr ~sn) c.sl_elts lin_setv_fpr c.sl_sets in
      SvMap.iter (fun i c -> do_lin ind fmt (i, c)) u.u_lin;
      SvMap.iter
        (fun i s ->
          let s = SvSet.remove i s in
          if s != SvSet.empty && i <= SvSet.min_elt s then
            let plur = if SvSet.cardinal s > 1 then "s" else "" in
            F.fprintf fmt "%s%a equal to set%s %a\n" ind setv_fpr i
              plur setvset_fpr s
        ) u.u_eqs;
      SvMap.iter
        (fun i s ->
          let plur = if SvSet.cardinal s > 1 then "s" else "" in
          F.fprintf fmt "%s%a contains set%s: %a\n" ind setv_fpr i
            plur setvset_fpr s
        ) u.u_sub;
      SvMap.iter
        (fun i s ->
          let plur = if SvSet.cardinal s > 1 then "s" else "" in
          F.fprintf fmt "%s%a contains element%s: %a\n" ind setv_fpr i
            plur (set_sv_fpr ~sn) s
        ) u.u_mem;
      List.iter
        (fun (i, c) ->
          F.fprintf fmt "%sExtra ctr (xlin): %a" ind (do_lin "") (i, c)
        ) u.u_xlin


    (** Internal sanity checking *)

    (* Internal sanity check:
     * 1. each var in a set_lin constraint should not be defined in
     *    another similar constraint (the u_lin field should allow to
     *    do a nomralisation of the constraints)
     * 2. set_lin constraints should imply membership constraints
     * 3. set_lin constraints should imply inclusion constraints
     *)
    (* TODO: possibly some other sanity checks to add
     * - others ?
     * TOCHECK:
     * - is there a relation satisfied by the equality constraints?
     *   => check invariants
     *)
    let u_sanity_check ?(oinfo: (form -> unit -> unit) option = None)
        (loc: string) (u: u): unit =
      if !Flags.flag_sanity_setlin != Logger.SC_ignore then
        let fail (msg: string) =
          sanity_log "Sanity check failed[%s]: %s\n%a"
            loc msg (u_fpri SvMap.empty "  ") u;
          match oinfo with
          | None -> ( )
          | Some info -> sanity_log "\n\nInfo was:\n%a" info () in
        SvMap.iter
          (fun i sl ->
            let subs = try SvMap.find i u.u_sub with Not_found -> SvSet.empty in
            let elts = try SvMap.find i u.u_mem with Not_found -> SvSet.empty in
            let fail (msg: string) =
              fail (F.asprintf "%s in [%a => %a]" msg setv_fpr i
                      (sl_fpr ~sn:SvMap.empty) sl) in
            (* check 1. *)
            SvSet.iter
              (fun setv ->
                if SvMap.mem setv u.u_lin then
                  fail (F.asprintf "constraint expansion,%a" setv_fpr setv);
              ) sl.sl_sets;
            (* check 2. *)
            if not (SvSet.subset sl.sl_sets subs) then
              fail ("missing inclusion constraint(s)");
            (* check 3. *)
            if not (SvSet.subset sl.sl_elts elts) then
              fail ("missing element constraint(s)");
          ) u.u_lin


    (** General utility functions *)

    (* map over the option type encoding bottom / non bottom *)
    let t_map (f: u -> u) (t: t): t =
      match t.t_t with
      | None -> t
      | Some u -> { t with t_t = Some (f u) }

    (* Empty element *)
    let empty (uo: u option): t = (* corresponds to top in fact *)
      { t_t     = uo;
        t_roots = SvSet.empty; }

    (* Is an abstract value top (empty of constraints);
     * this is useful to diagnose precision losses *)
    let u_is_top (u: u): bool =
      u.u_lin = SvMap.empty && u.u_sub = SvMap.empty
        && u.u_mem = SvMap.empty && u.u_eqs = SvMap.empty
    let is_top (t: t): bool =
      match t.t_t with
      | None -> false
      | Some u -> u_is_top u

    (* Fast access to constraints *)
    let u_get_sub (u: u) (i: sv): SvSet.t = (* subsets of i *)
      try SvMap.find i u.u_sub with Not_found -> SvSet.empty
    let u_get_mem (u: u) (i: sv): SvSet.t = (* elements of i *)
      try SvMap.find i u.u_mem with Not_found -> SvSet.empty
    let u_get_eqs (u: u) (i: sv): SvSet.t = (* sets equal to i *)
      try SvMap.find i u.u_eqs with Not_found -> SvSet.singleton i

    (* Adding some constraints *)
    (* Possible improvement: add some reduction here (none for now) *)
    let u_add_mem (u: u) (i: sv) (j: sv): u = (* i is an element of j *)
      let omem = u_get_mem u j in
      { u with u_mem = SvMap.add j (SvSet.add i omem) u.u_mem }
    let u_add_inclusion (u: u) (i: sv) (j: sv): u = (* i included in j *)
      (* Saturating membership constraints: we add all members of i to j.
       * If they are non empty *)
      let mem_i = u_get_mem u i in
      let u = if SvSet.is_empty mem_i then u else
        let mem_j = SvSet.union mem_i @@ u_get_mem u j in
        let u_mem = SvMap.add j mem_j u.u_mem in
        { u with u_mem } in
      (* saturating inclusion constraints. the subsets of j are :
       * + the original subsets of j
       * + i
       * + the subsets of i *)
      let osub = SvSet.union (u_get_sub u j) (u_get_sub u i) in
      { u with u_sub = SvMap.add j (SvSet.add i osub) u.u_sub }
    let u_add_eq (u: u) (i: sv) (j: sv): u = (* i = j *)
      let c = SvSet.union (u_get_eqs u i) (u_get_eqs u j) in
      (* Saturating membership constraints *)
      let mems = SvSet.fold
        (fun setv -> SvSet.union (u_get_mem u setv))
        c SvSet.empty in
      let is_empty_i =
        SvMap.find_opt i u.u_lin |> Option.fold ~none:false ~some:sl_is_empty in
      let is_empty_j =
        SvMap.find_opt j u.u_lin |> Option.fold ~none:false ~some:sl_is_empty in
      if (is_empty_i || is_empty_j) && not (SvSet.is_empty mems) then
        raise Bottom;
      let u = if SvSet.is_empty mems then u else
        let u_mem = SvSet.fold (fun setv -> SvMap.add setv mems) c u.u_mem in
        { u with u_mem } in
      (* Saturating inclusion constraints *)
      let subs = SvSet.fold
        (fun setv -> SvSet.union (u_get_sub u setv))
        c SvSet.empty in
      let u = if SvSet.is_empty subs then u else
        let u_sub = SvSet.fold (fun setv -> SvMap.add setv subs) c u.u_sub in
        { u with u_sub } in
      (* Saturating the equality constraints *)
      { u with u_eqs = SvSet.fold (fun i -> SvMap.add i c) c u.u_eqs }
    let u_add_lin (u: u) (i: sv) (sl: set_lin): u = (* i = sl *)
      u_sanity_check "add_lin,arg" u;
      let info fmt () =
        F.fprintf fmt "Adding cstr on %a in\n%a" setv_fpr i
          (u_fpri SvMap.empty "    ") u in
      (* refinement of the sl constraint *)
      let sl =
        if do_reduce_lin then
          SvSet.fold
            (fun setv acc ->
              try
                let sl0 = SvMap.find setv u.u_lin in
                sl_add { acc with sl_sets = SvSet.remove setv acc.sl_sets } sl0
              with Not_found -> acc
            ) sl.sl_sets sl
        else sl in
      (* a bit of reduction: search of all other equalities to sl *)
      let f_add_eqs j0 sl0 u =
        if sl_eq sl sl0 then u_add_eq u i j0
        else u in
      let u = SvMap.fold f_add_eqs u.u_lin u in
      let u =
        List.fold_left (fun u (j0, sl0) -> f_add_eqs j0 sl0 u) u u.u_xlin in
      (* adding membership constraints *)
      let u = SvSet.fold (fun sv u -> u_add_mem u sv i) sl.sl_elts u in
      (* adding inclusion constraints *)
      let u = SvSet.fold (fun sv u -> u_add_inclusion u sv i) sl.sl_sets u in
      (* add the new lin constraint, possibly as an extra constraint *)
      let u =
        if SvMap.mem i u.u_lin then
          begin
            if tmp_debug_xlin then
              Log.info "u_add_lin: add to xlin: %a => %a" setv_fpr i
                (sl_fpr ~sn:SvMap.empty) sl;
            { u with u_xlin = (i, sl) :: u.u_xlin }
          end
        else
          let u_lin = SvMap.add i sl u.u_lin in
          let u_lin =
            if do_reduce_lin then
              SvMap.map
                (fun sl0 ->
                  if SvSet.mem i sl0.sl_sets then
                    sl_add sl { sl0 with sl_sets = SvSet.remove i sl0.sl_sets }
                  else sl0
                ) u_lin
            else u_lin in
          { u with u_lin } in
      u_sanity_check ~oinfo:(Some info) "add_lin,out" u;
      u

    (** [from_lin_to_eqs u] check if there are any definition constraints
     *  [S := S'] in [u_lin] and add them to [u_eqs] if there are. *)
    let from_lin_to_eqs (u: u): u =
      SvMap.fold
        (fun setv sl u ->
          match sl_is_set sl with
          | None -> u
          | Some setv0 -> u_add_eq u setv setv0)
        u.u_lin u

    (* Remove all constraints over a group of SVs in one shot;
     * function f maps a symbolic variable to true if it should be dropped,
     * and to false otherwise. *)
    let drop_symb_svs (f: sv -> bool) (x: t): t =
      let find_eq set_lin c ele_sets i =
        let m, l =
          SvMap.fold
            (fun key lin (accm, accs) ->
              if key <> i && SvSet.subset lin.sl_elts c.sl_elts
                  && SvSet.subset lin.sl_sets c.sl_sets &&
                SvSet.inter lin.sl_elts ele_sets <> SvSet.empty then
                SvMap.add key lin accm,
                SvSet.union accs lin.sl_elts
              else
                accm, accs
            ) set_lin (SvMap.empty, SvSet.empty) in
        if SvSet.subset ele_sets l then Some m else None in
      let replace map_eq set_lin elts =
        let bindings = List.rev (SvMap.bindings map_eq) in
        let sl_sets, sl_elts, elts =
          List.fold_left
            (fun (accs, acce, acct) (key, lin) ->
              if SvSet.subset lin.sl_sets accs
                  && SvSet.subset lin.sl_elts acce then
                SvSet.add key (SvSet.diff accs lin.sl_sets),
                SvSet.diff acce lin.sl_elts,
                SvSet.diff acct lin.sl_elts
              else accs, acce, acct
            ) (set_lin.sl_sets, set_lin.sl_elts, elts) bindings in
        if elts = SvSet.empty then
          Some { sl_elts = sl_elts;
                 sl_sets = sl_sets }
        else None in
      match x.t_t with
      | None -> x
      | Some u ->
          (* The test above is too agressive (see test 8123j, 8123m, 8128j).
           * We simply remove all linear expressions involving any removed sv *)
          (* TO DISCUSS WITH XR *)
          let u_xlin =
            List.filter
              (fun (i, ({sl_elts; _ } as sl)) ->
                let b =
                  SvSet.for_all (fun sv -> not (f sv)) sl_elts in
                if not b then
                  Log.info "drop_symb_svs: xlin removed (%a => %a)"
                    setv_fpr i (sl_fpr ~sn:SvMap.empty) sl;
                b
              ) u.u_xlin in
          let lin =
            SvMap.fold
              (fun i c acc ->
                let b = SvSet.exists f c.sl_elts in
                if b then
                  let elts = SvSet.filter f c.sl_elts in
                  let m = find_eq u.u_lin c elts i in
                  match m with
                  | None -> SvMap.remove i acc
                  | Some map ->
                      match replace map c elts with
                      | Some c ->  SvMap.add i c acc
                      | None -> SvMap.remove i acc
                else acc
              ) u.u_lin u.u_lin in
          let mem =
            SvMap.fold
              (fun i c acc ->
                let nc = SvSet.filter (fun j -> not (f j)) c in
                if nc == c then acc else SvMap.add i nc acc
              ) u.u_mem u.u_mem in
          { x with t_t = Some { u with u_lin = lin; u_mem = mem; u_xlin } }

    (* Topification *)
    let topify (msg: string) (t: t): t =
      let cons =
        match t.t_t with
        | None -> None
        | Some u ->
            if !Flags.flag_dbg_dom_setlin then
              Log.warn "topify: %s" msg;
            Some { u_lin  = SvMap.empty;
                   u_sub  = SvMap.empty;
                   u_mem  = SvMap.empty;
                   u_eqs  = SvMap.empty;
                   u_xlin = [] } in
      { t with t_t = cons }

    (* Checking whether an abstract value entails some constraint *)
    exception Stop of bool
    (* a closure function: inputs Si, and returns all Sj found to be
     *  contained in Si, using equality and inclusion constraints *)
    let closure_subsets (u: u) (i: sv): SvSet.t =
      let add acc s = SvSet.diff s acc, SvSet.union s acc in
      let rec aux acc s =
        let nvs, acc = add acc s in
        if nvs = SvSet.empty then acc
        else
          let acc, next =
            SvSet.fold
              (fun j (old, n) ->
                SvSet.add j old,
                SvSet.union (u_get_eqs u j) (SvSet.union (u_get_sub u j) n)
              ) nvs (acc, SvSet.empty) in
          aux acc next in
      aux SvSet.empty (SvSet.singleton i)
    (* does u entail that i \in j ? *)
    let u_sat_mem (u: u) i j =
      let loc_debug = false in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "looking for membership(%a in %a)" nsv_fpr i setv_fpr j;
      try
        if SvSet.mem i (u_get_mem u j) then raise True;
        (* look at sets equal to j, and gather their known elements *)
        let smems = closure_subsets u j in
        if !Flags.flag_dbg_dom_setlin || loc_debug then
          Log.info "subsets: %a" setvset_fpr smems;
        SvSet.iter
          (fun j -> if SvSet.mem i (u_get_mem u j) then raise True)
          smems;
        if SvSet.mem i (SvMap.find j u.u_lin).sl_elts then raise True;
        false
      with
      | True -> true
      | Not_found -> false
    (* can we find a constraint that ensures i is empty ? *)
    let u_has_empty_ctr (u: u) (i: sv): bool =
      try
        let sl = SvMap.find i u.u_lin in
        sl.sl_elts = SvSet.empty && sl.sl_sets = SvSet.empty
      with Not_found -> false
    (* is i empty ? *)
    let u_sat_empty (u: u) i =
      SvSet.fold (fun i acc -> acc || u_has_empty_ctr u i)
        (u_get_eqs u i) false
    (* does u entail i=j ? *)
    let u_sat_eq (u: u) i j =
      SvSet.mem i (u_get_eqs u j) || SvSet.mem j (u_get_eqs u i)
    (* does constraint "i=sl" exist in u ? *)
    let u_sat_lin (u: u) i sl =
      let eqs = u_get_eqs u i in
      try
        (* We search amongst set variables equals to [i] if there is one
         * equal having a defintion equal to [sl]. *)
        SvSet.iter
          (fun i ->
            match SvMap.find_opt i u.u_lin with
            | None -> ()
            | Some slo -> if sl_eq sl slo then raise True
          ) eqs;
        (* Otherwise if [sl = { } + setv'], we check wether [i = setv']*)
        if SvSet.is_empty sl.sl_elts then
          let sl_sets =
            SvSet.fold
              (fun i acc ->
                if u_sat_empty u i then acc
                else SvSet.add i acc
              ) sl.sl_sets SvSet.empty in
          if SvSet.cardinal sl_sets = 1 then
            u_sat_eq u (SvSet.choose sl_sets) i
          else false
        else false
      with True -> true

    let reduce_empty lin =
      SvMap.fold
        (fun i sl acc ->
          let sl_sets =
            SvSet.filter
              (fun x ->
                (Option.fold
                   ~none:true
                   ~some:(fun x -> not (sl_is_empty x) ))
                  (SvMap.find_opt x lin))
              sl.sl_sets in
          SvMap.add i { sl with sl_sets } acc
        ) lin SvMap.empty

    (* does u entail i <= j ? (i subset of j) *)
    let u_sat_sub (u: u) i j =
      let i0 = u_get_eqs u i and j0 = u_get_eqs u j in
      let jsubsets =
        SvSet.fold (fun jj acc -> SvSet.union acc (u_get_sub u jj)) j0
          SvSet.empty in
      SvSet.inter i0 jsubsets != SvSet.empty

    (* does u entail that i in ex *)
    let u_sat_mem_ex (u: u) i ex =
      let rec aux_mem = function
        | S_empty -> false
        | S_node j -> i = j
        | S_var j -> u_sat_mem u i j
        | S_uplus (e0, e1) | S_union (e0, e1) -> aux_mem e0 || aux_mem e1 in
      if aux_mem ex then true
      else
        match linearize ex with
        | Some lin ->
            (* we look for sets that include this linearized expression,
             * and try to verify membership on them *)
            begin
              try
                SvMap.iter
                  (fun j sl ->
                    if sl_subset lin sl && u_sat_mem u i j then
                      raise True
                  ) u.u_lin;
                false
              with True -> true
            end
        | None -> false (* we give up *)


    (** Lattice elements *)

    (* Bottom element (empty context) *)
    let bot: t = empty None
    let is_bot (x: t): bool = x.t_t = None

    (* Top element (empty context) *)
    let top: t = empty (Some { u_lin   = SvMap.empty;
                               u_sub   = SvMap.empty;
                               u_mem   = SvMap.empty;
                               u_eqs   = SvMap.empty;
                               u_xlin  = [] })

    (* Pretty-printing, with indentation *)
    let t_fpri (sn: string SvMap.t) (ind: string) (fmt: form) (t: t): unit =
      match t.t_t with
      | None -> F.fprintf fmt "%sBOT\n" ind
      | Some u ->
          F.fprintf fmt "%sSETV-roots: { %a }\n%a" ind setvset_fpr t.t_roots
            (u_fpri sn ind) u

    (** Handling the removal of constraints *)
    (* Remove all constraints over a group of SETVs in one shot;
     * function fv maps a SETV to true if it should be dropped, and to false
     * otherwise *)
    (** [u_restore_xlin u] adds to [u_lin] all linear definitions in [u_xlin] of
        setv not having already a definition. *)
    let u_restore_xlin (u: u): u =
      let u_lin, u_xlin =
        List.fold_left
          (fun (lin, accrem) (i, c) ->
            if SvMap.mem i lin then
              begin
                if tmp_debug_xlin then
                  Log.info "restore_xlin: add xlin at %a" setv_fpr i;
                lin, (i, c) :: accrem
              end
            else SvMap.add i c lin, accrem
          ) (u.u_lin, []) u.u_xlin in
      let uu = { u with u_lin; u_xlin } in
      uu
    let drop_setvs (fv: sv -> bool) (x: t): t =
      let fset s = SvSet.exists fv s in
      let filter_set s = SvSet.filter (fun i -> not (fv i)) s in
      match x.t_t with
      | None -> x
      | Some u ->
          let loc_debug = false in
          if (loc_debug || tmp_debug_xlin) && u.u_xlin != [] then
            Log.info "drop_setvs: xlin found\n%a" (t_fpri SvMap.empty "  ") x;
          (* for the lin part, we try to recover some constraints *)
          let lin, rem =
            SvMap.fold
              (fun i c (acc, rem) ->
                if fv i || fset c.sl_sets then SvMap.remove i acc, (i,c) :: rem
                else acc, rem
              ) u.u_lin (u.u_lin, [ ]) in
          let xlin =
            List.fold_left
              (fun acc (i, c) ->
                Log.info "drop_setvs,xlin: %a => %b" setv_fpr i
                  (fv i || fset c.sl_sets);
                if fv i || fset c.sl_sets then
                  begin
                    Log.info "drop_setvs,xlin dropped: %a" setv_fpr i;
                    acc
                  end
                else (i, c) :: acc
              ) [] u.u_xlin in
          let lin =
            if List.length rem > 1 then
              let f (i,c) =
                (i,c), SvSet.cardinal c.sl_elts + SvSet.cardinal c.sl_sets in
              let rem = List.map f rem in
              let rem = List.sort (fun (_,i) (_,j) -> i - j) rem in
              let rem = List.map fst rem in
              (** [try_def sl1 (i0, sl0)] returns:
                  - [Some] [sl1]\[sl0] U {[i0]} if it contains no setv to drop
                  - [None] otherwise *)
              (** Maybe optimize by recursively folding definitions *)
              let try_def sl1 (i0, sl0): set_lin option =
                if sl_subset sl0 sl1 then
                  let sl = sl_add (sl_sub sl1 sl0) (sl_one_set i0) in
                  if fset sl.sl_sets then None
                  else Some sl
                else None in
              let rec save_constraints rem defs acc_lin =
                match rem with
                | [] -> acc_lin
                | (i1, sl1) :: q ->
                    let acc_lin =
                      match List.find_map (try_def sl1) defs with
                      | None -> acc_lin
                      | Some sl -> SvMap.add i1 sl acc_lin in
                    let defs =
                      if not (fv i1) then defs @ [(i1, sl1)]
                      else defs in
                    save_constraints q defs acc_lin in
              save_constraints rem [] lin
            else lin in
          let sub =
            SvMap.fold
              (fun i sub acc ->
                if fv i then acc
                else SvMap.add i (filter_set sub) acc
              ) u.u_sub SvMap.empty in
          let eqs =
            SvMap.fold
              (fun i s acc ->
                if fv i then SvMap.remove i acc
                else
                  let fs = filter_set s in
                  if SvSet.cardinal fs = 1 && SvSet.mem i fs then
                    SvMap.remove i acc
                  else SvMap.add i fs acc
              ) u.u_eqs u.u_eqs in
          let mem = SvMap.filter (fun i _ -> not (fv i)) u.u_mem in
          let uu = { u_lin  = lin;
                     u_sub  = sub;
                     u_mem  = mem;
                     u_eqs  = eqs;
                     u_xlin = xlin } in
          let uu = from_lin_to_eqs uu in
          let uu = u_restore_xlin uu in
          let r = { x with t_t = Some uu } in
          if u.u_xlin != [] then
            Log.warn "drop_setvs: xlin was present before call,result:\n%a"
              (t_fpri SvMap.empty "  ") r;
          r


    (** Management of symbolic variables *)

    (* For sanity check *)
    let check_nodes ~(svs: SvSet.t) ~(setvs: SvSet.t) (x: t): bool =
      (* Very systematic traversal, looking at all SVs/SETVs *)
      match x.t_t with
      | None -> true
      | Some u ->
          try
            let f_setv setv = if not (SvSet.mem setv setvs) then raise False in
            let f_svs s = if not (SvSet.subset s svs) then raise False in
            let f_setvs s = if not (SvSet.subset s setvs) then raise False in
            let f_sl sl = f_svs sl.sl_elts ; f_setvs sl.sl_sets in
            SvMap.iter (fun i sl -> f_setv i; f_sl sl) u.u_lin;
            SvMap.iter (fun i sets -> f_setv i; f_setvs sets) u.u_sub;
            SvMap.iter (fun i elts -> f_setv i; f_svs   elts) u.u_sub;
            SvMap.iter (fun i sets -> f_setv i; f_setvs sets) u.u_eqs;
            List.iter (fun (i,sl) -> f_setv i; f_sl sl) u.u_xlin;
            true
          with False -> false
    (* For internal use only *)
    let t_sanity_check ?(oinfo: (form -> unit -> unit) option = None)
        (loc: string) (x: t): unit =
      match x.t_t with
      | None -> ()
      | Some u -> u_sanity_check ~oinfo loc u


    (** A bit of reduction, for internal use only *)
    (* Notes:
     *  - we would need less reduction with an equality functor
     *  - we could share code with iterators
     *)
    (* Reduction of the linear part (replace a variable by lin expr if any) *)
    let t_reduce_lin (setv: sv) (t: t): t =
      t_sanity_check "t_reduce_lin,arg" t;
      let info fmt () =
        F.fprintf fmt "- setv: %a\n- arg:\n%a" setv_fpr setv
          (t_fpri SvMap.empty "  ") t in
      let u_aux (u: u): u =
        let u = {u with u_lin = reduce_empty u.u_lin} in
        if SvMap.mem setv u.u_lin then
          let lin0 = SvMap.find setv u.u_lin in
          let nlins =
            SvMap.fold
              (fun i lin acc ->
                if SvSet.mem setv lin.sl_sets then (* do the reduction *)
                  let lin =
                    sl_add lin0
                      { lin with sl_sets = SvSet.remove setv lin.sl_sets } in
                  SvMap.add i lin acc
                else (* nothing changes *) acc
              ) u.u_lin u.u_lin in
          { u with u_lin = nlins }
        else u in
      t_sanity_check ~oinfo:(Some info) "t_reduce_lin,out" t;
      t_map u_aux t
    (* Replacing a SETV setv by an equal SETV if there is one *)
    let t_reduce_eq (setv: sv) (t: t): t =
      let loc_debug = false in
      t_sanity_check "t_reduce_eq,arg" t;
      let info fmt () =
        F.fprintf fmt "- setv: %a\n- arg:\n%a" setv_fpr setv
          (t_fpri SvMap.empty "  ") t in
      if loc_debug then
        Log.info "t_reduce_eq,in: %a\n%a" setv_fpr setv
          (t_fpri SvMap.empty "  ") t;
      let u_aux (u: u): u =
        if SvMap.mem setv u.u_eqs then
          let others = SvSet.remove setv (SvMap.find setv u.u_eqs) in
          if SvSet.cardinal others > 0 then
            let setv0 = SvSet.min_elt others in
            (* replace setv by setv0 wherever it appears *)
            let f_setv i = if i = setv then setv0 else i in
            let f_set_lin sl =
              if SvSet.mem setv sl.sl_sets then
                if SvMap.mem setv0 u.u_lin then
                  let sl0 = SvMap.find setv0 u.u_lin in
                  let sl = { sl with sl_sets = SvSet.remove setv sl.sl_sets } in
                  sl_add sl sl0
                else
                  let s = SvSet.add setv0 (SvSet.remove setv sl.sl_sets) in
                  { sl with sl_sets = s }
              else sl in
            if loc_debug then
              Log.info "setv: %b setv0: %b" (SvMap.mem setv u.u_lin)
                (SvMap.mem setv0 u.u_lin);
            let nlins =
              if false then
                SvMap.fold
                  (fun setv sl0 acc ->
                    let sl0 = f_set_lin sl0 in
                    if setv = setv0 then
                      SvMap.add setv0 sl0 acc
                    else SvMap.add setv sl0 acc
                  ) u.u_lin SvMap.empty
              else SvMap.map f_set_lin u.u_lin in
            let nxlins =
              List.map (fun (i,sl) -> f_setv i, f_set_lin sl) u.u_xlin in
            let nsubs =
              SvMap.fold
                (fun i s acc ->
                  if i = setv then
                    let s = SvSet.union s (u_get_sub u setv0) in
                    SvMap.add setv s acc
                  else if SvSet.mem setv s then
                    let s = SvSet.add setv0 (SvSet.remove setv s) in
                    SvMap.add setv s acc
                  else acc
                ) u.u_sub u.u_sub in
            let nmems =
              if SvMap.mem setv u.u_mem then
                let s = SvMap.find setv u.u_mem in
                let s0 = u_get_mem u setv0 in
                SvMap.add setv0 (SvSet.union s s0) u.u_mem
              else u.u_mem in
            let nlins, nxlins =
              if SvMap.mem setv nlins then
                if SvMap.mem setv0 nlins then
                  let sl0 = SvMap.find setv0 nlins
                  and sl  = SvMap.find setv  nlins in
                  let slm, osl = sl_more_gen sl sl0 in
                  let nlins =
                    (* this instance of insert_sl solves nothing *)
                    SvMap.add(*insert_sl*) setv0 slm nlins in
                  let nxlins =
                    let f slo = (setv0, slo) :: nxlins in
                    Option.fold ~none:nxlins ~some:f osl in
                  nlins, nxlins
                else
                  (* insert_sl solves some problems but causes more issues *)
                  SvMap.add
                    (*insert_sl*) setv0 (SvMap.find setv nlins) nlins, nxlins
              else nlins, nxlins in
            { u with
              u_lin  = nlins;
              u_sub  = nsubs;
              u_mem  = nmems;
              u_xlin = nxlins; }
          else u
        else u in
      let t = t_map u_aux t in
      if loc_debug then
        Log.info "t_reduce_eq,out:\n%a" (t_fpri SvMap.empty "  ") t;
      t_sanity_check ~oinfo:(Some info) "t_reduce_eq,out" t;
      t


    (* Symbolic variables *)
    let sv_add (sv: sv) (t: t): sv * t = sv, t
    let sv_rem (sv: sv) (t: t): t =
      drop_symb_svs ((=) sv) t

    (* check if a set var root *)
    let setv_is_root (setv: sv) (t: t): bool =
      SvSet.mem setv t.t_roots

    (* collect root set variables *)
    let setv_col_root (t: t): SvSet.t = t.t_roots

    (* Set variables *)
    let setv_add ?(root: bool = false) ?(kind: set_par_type option = None)
        ?(name: string option = None) (setv: sv) (t: t): t =
      { t with
        t_roots = if root then SvSet.add setv t.t_roots else t.t_roots }
    let setv_rem (setv: sv) (t: t): t =
      let loc_debug = false in
      (* attempts to do some reduction, and removes *)
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "removing setv %a in\n%a" setv_fpr setv
          (t_fpri SvMap.empty "  ") t;
      let t = t_reduce_lin setv t in
      let t = t_reduce_eq setv t in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "reduction done:\n%a" (t_fpri SvMap.empty "  ") t;
      let t = drop_setvs ((=) setv) t in
      let t = { t with t_roots = SvSet.remove setv t.t_roots } in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "removed setv %a:\n%a" setv_fpr setv
          (t_fpri SvMap.empty "  ") t;
      t


    (** Comparison and join operators *)

    (* Comparison *)
    exception Abort
    (* are all constraints of u1 true in u0 ? *)
    let u_is_le (u0: u) (u1: u): bool =
      u_sanity_check "u_is_le,l" u0;
      u_sanity_check "u_is_le,r" u1;
      (* Possible improvements:
       *  - do some reduction before attempting is_le;
       *  - utilize the sat functions (they are a bit more clever) *)
      try
        SvMap.iter
          (fun i sl1 ->
            if not (sl_eq sl1 (SvMap.find i u0.u_lin)) then raise Abort
          ) u1.u_lin;
        SvMap.iter
          (fun i elts1 ->
            if not (SvSet.subset elts1 (u_get_mem u0 i)) then raise Abort
          ) u1.u_mem;
        SvMap.iter
          (fun i sub1 ->
            if not (SvSet.subset sub1 (u_get_sub u0 i)) then raise Abort
          ) u1.u_sub;
        SvMap.iter
          (fun i eqs1 ->
            if not (SvSet.subset eqs1 (u_get_eqs u0 i)) then raise Abort
          ) u1.u_eqs;
        List.iter
          (fun (setv, sl) ->
            if not (sl_eq sl (SvMap.find setv u0.u_lin)) then raise Abort
          ) u1.u_xlin;
        true
      with
      | Not_found -> false
      | Abort -> false

    let is_le (t0: t) (t1: t): bool =
      let loc_debug = false in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "is_le,call:\n%aand:\n%a"
          (t_fpri SvMap.empty "   ") t0 (t_fpri SvMap.empty "   ") t1;
      let res =
        match t0.t_t, t1.t_t with
        | None, _ -> true
        | Some _, None -> false
        | Some u0, Some u1 -> u_is_le u0 u1 in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "is_le,return: %b" res;
      res

    (* Lower upper bound *)
    let u_lub (u0: u) (u1: u): u =
      let loc_debug = false in
      (* A useful generalization:
       *   (S0 = S1 /\ S2 = empty) \/ (S0 = {x} + S1 /\ S2 = {x})
       *           = (S0 = S2 + S1)
       * To do it, we first compute singletons in both sides
       * Then, we compute lists of generalized constraints
       *
       * List of equalities that could be added *)
      u_sanity_check "u_lub,l" u0;
      u_sanity_check "u_lub,r" u1;
      let info fmt () =
        F.fprintf fmt "L:\n%a\nR:\n%a"
          (u_fpri SvMap.empty " ") u0 (u_fpri SvMap.empty " ") u1 in
      (* temporary eager closure; not sure it is a good idea here *)
      let do_temp_closure = false in
      let rec temp_closure (u: u): u =
        let u_lin, b =
          SvMap.fold
            (fun setv sl (acc, b) ->
              let sl, b =
                SvSet.fold
                  (fun setv0 (sl, b) ->
                    if SvMap.mem setv0 u.u_lin then
                      sl_add (SvMap.find setv0 u.u_lin) sl, true
                    else { sl with sl_sets = SvSet.add setv0 sl.sl_sets }, b
                  ) sl.sl_sets ({ sl with sl_sets = SvSet.empty }, b)in
              SvMap.add setv sl acc, b
            ) u.u_lin (u.u_lin, false) in
        let u = { u with u_lin } in
        if b then temp_closure u else u in
      let generalize_sngs (u: u): (sv * set_lin) list =
        (* singletons: set => { elt } *)
        let compute_singletons (u: u): (sv, sv) Bi_fun.t * set_lin SvMap.t =
          SvMap.fold
            (fun i sl (acc, rem) ->
              if sl.sl_sets = SvSet.empty
                  && SvSet.cardinal sl.sl_elts = 1 then
                Bi_fun.add i (SvSet.min_elt sl.sl_elts) acc,
                SvMap.remove i rem
              else acc, rem
            ) u.u_lin (Bi_fun.empty, u.u_lin) in
        let sngs, rem = compute_singletons u in
        let l =
          SvMap.fold
            (fun i sl acc ->
              if SvSet.cardinal sl.sl_elts = 1 then
                let x = SvSet.min_elt sl.sl_elts in
                match Bi_fun.inverse_opt x sngs with
                | None -> acc
                | Some j -> (i, { sl_elts = SvSet.remove x sl.sl_elts;
                                  sl_sets = SvSet.add j sl.sl_sets; } ) :: acc
              else acc
            ) rem [ ] in
        if !Flags.flag_dbg_dom_setlin || loc_debug then
          Log.debug "reduced constraints: %d" (List.length l);
        l in
      let compute_emptys (u: u) =
        SvMap.fold
          (fun i sl acc -> if sl_is_empty sl then SvSet.add i acc else acc)
          u.u_lin SvSet.empty in
      let gen0 = generalize_sngs u0 and gen1 = generalize_sngs u1 in
      let emp0 = compute_emptys u0 and emp1 = compute_emptys u1 in
      let add_eqs_lin u u_other: u =
         SvMap.fold
           (fun setv setlin acc ->
             let eqs = u_get_eqs u setv in
             (* let eqs_other = u_get_eqs u_other setv in
             let eqs_only = SvSet.diff eqs eqs_other in *)
             let u_lin =
               SvSet.fold
                 (fun setv' acc ->
                   if SvMap.mem setv' acc then acc
                   else SvMap.add setv' setlin acc
                 ) eqs u.u_lin in
             { u with u_lin })
           u.u_lin u in
       let u0 = add_eqs_lin u0 u1 in
       let u1 = add_eqs_lin u1 u0 in
      (* generalized equalities of set a, emptys of set b, element of side b *)
      let generalize (gena: (sv * set_lin) list) (empb: SvSet.t) (ub: u) =
        List.fold_left
          (fun acc (i, sl) ->
            if sl.sl_elts = SvSet.empty then
              let rec_sets = SvSet.diff sl.sl_sets empb in
              if SvSet.cardinal rec_sets = 1 then
                let x = SvSet.min_elt rec_sets in
                (* means we have equality x = i *)
                let b = u_sat_eq ub i x in
                if !Flags.flag_dbg_dom_setlin || loc_debug then
                  Log.debug "suggests equality %a = %a => %b"
                    setv_fpr i setv_fpr x b;
                (i, sl) :: acc
              else acc
            else acc
          ) [ ] gena in
      let lineqs_to_add = generalize gen0 emp1 u1 @ generalize gen1 emp0 u0 in
      let lin =
        SvMap.fold
          (fun i sl0 acc ->
            try
              let sl1 = SvMap.find i u1.u_lin in
              (* compute the difference or intersection of two set expression
               *  that equal to a same SETv, as example, let
               *  sl0 = E + X+ {\alpha} , sl1 = F + X + {\beta},
               *  then, lin0 = E + {\alpha}, lin1 = F + {\beta},
               *  lin_r = X *)
              let f_com f sl sr =
                { sl_elts = f sl.sl_elts sr.sl_elts;
                  sl_sets = f sl.sl_sets sr.sl_sets } in
              let lin0 = f_com SvSet.diff sl0 sl1 in
              let lin1 = f_com SvSet.diff sl1 sl0 in
              let lin_r = f_com SvSet.inter sl1 sl0 in
              if tmp_debug_join_more_ctrs then
                Log.info "Lin join:\n {0} %a => %a\n {1} %a => %a"
                  (sl_fpr ~sn:SvMap.empty) sl0 (sl_fpr ~sn:SvMap.empty) lin0
                  (sl_fpr ~sn:SvMap.empty) sl1 (sl_fpr ~sn:SvMap.empty) lin1;
              (* deal with the difference part: as example, lin0 = E+{\alpha},
               * lin1 = F + {\beta}, then, if u1 => E = {\beta}, we can reduce
               * lin0 = {\alpha}, lin1 = F, lin_r = X + E *)
              let compute_unify (u0, lin0) (u1, lin1) lin_r =
                (* Tries to fold defintion in l1.*)
                let reduce_diff l0 l1 u1 l_r =
                  SvSet.fold
                    (fun s (acc0, acc1, accr) ->
                      try
                        let ls = SvMap.find s u1.u_lin in
                        if SvSet.subset ls.sl_elts acc1.sl_elts &&
                          SvSet.subset ls.sl_sets acc1.sl_sets then
                          { acc0 with sl_sets = SvSet.remove s acc0.sl_sets },
                          f_com SvSet.diff acc1 ls,
                          { accr with sl_sets = SvSet.add s accr.sl_sets }
                        else acc0, acc1, accr
                      with Not_found -> acc0, acc1, accr
                    ) l0.sl_sets (l0, l1, l_r) in
                let lin0, lin1, lin_r = reduce_diff lin0 lin1 u1 lin_r in
                let lin1, lin0, lin_r = reduce_diff lin1 lin0 u0 lin_r in
                lin0, lin1, lin_r in
              if tmp_debug_join_more_ctrs then
                Log.info "Lin join,reduce:\n {0} ===> %a\n {1} ===> %a"
                  (sl_fpr ~sn:SvMap.empty) lin0
                  (sl_fpr ~sn:SvMap.empty) lin1;
              let nlin0, nlin1, lin_r =
                compute_unify (u0, lin0) (u1, lin1) lin_r in
              if tmp_debug_join_more_ctrs then
                Log.info
                  "Lin join,reduce:\n {0} ===> %a\n {1} ===> %a\n {r} ===> %a"
                  (sl_fpr ~sn:SvMap.empty) nlin0
                  (sl_fpr ~sn:SvMap.empty) nlin1
                  (sl_fpr ~sn:SvMap.empty) lin_r;
              if nlin0 = sl_empty && nlin1 = sl_empty then
                SvMap.add i lin_r acc
              else
                (* try to synthesize terms that are equivalent by the
                 * set equality constraints, and to preserve such constraints
                 *
                 * this comes from the fact that there seems to be no way to
                 * normalize the set_lin elements with respect to equalities *)
                let others u (lin: set_lin) =
                  if tmp_debug_join_more_ctrs then
                    SvMap.iter
                      (fun setv eqs ->
                        Log.info " %a => %a" setv_fpr setv setvset_fpr eqs
                      ) u.u_eqs;
                  SvSet.fold
                    (fun setv acc ->
                      if SvMap.mem setv u.u_eqs then
                        let eqs = SvMap.find setv u.u_eqs in
                        let eqs = SvSet.remove setv eqs in
                        if SvSet.cardinal eqs = 1 then
                          let osetv = SvSet.min_elt eqs in
                          let sl_sets =
                            SvSet.add osetv (SvSet.remove setv lin.sl_sets) in
                          { lin with sl_sets } :: acc
                        else acc
                      else acc
                    ) lin.sl_sets [lin] in
                let others0 = others u0 nlin0
                and others1 = others u1 nlin1 in
                if tmp_debug_join_more_ctrs then
                  Log.info "Lin join,others: %d, %d" (List.length others0)
                    (List.length others1);
                match others0, others1 with
                | lin0 :: _, lin1 :: _ ->
                    if tmp_debug_join_more_ctrs then
                      begin
                        Log.info
                          "Lin join,re-reduce:\n {0} ===> %a\n {1} ===> %a"
                          (sl_fpr ~sn:SvMap.empty) lin0
                          (sl_fpr ~sn:SvMap.empty) lin1;
                        Log.info "Lin join,keep: %b [%b]"
                          (lin0 = sl_empty && lin1 = sl_empty)
                          (SvSet.equal lin0.sl_sets lin1.sl_sets)
                      end;
                    if SvSet.equal lin0.sl_sets lin1.sl_sets
                        && SvSet.equal lin0.sl_elts lin1.sl_elts then
                      let lin_r =
                        { sl_sets = SvSet.union lin_r.sl_sets lin0.sl_sets ;
                          sl_elts = SvSet.union lin_r.sl_elts lin0.sl_elts } in
                      if tmp_debug_join_more_ctrs then
                        Log.info "Lin join,kept: %a => %a" setv_fpr i
                          (sl_fpr ~sn:SvMap.empty) lin_r;
                      SvMap.add i lin_r acc
                    else acc
                | _, _ -> acc
            with Not_found -> acc
          ) u0.u_lin SvMap.empty in
      let lin =
        List.fold_left
          (fun acc (i, sl) ->
            if SvMap.mem i acc then Log.fatal_exn "already here!"
            else SvMap.add i sl acc
          ) lin lineqs_to_add in
      let lin =
        SvMap.fold
          (fun i sl acc ->
            if not (SvMap.mem i lin) && u_sat_lin u0 i sl then
              SvMap.add i sl acc
            else acc
          ) u1.u_lin lin in
      (* remove empty *)
      let lin = reduce_empty lin in
      let eqs =
        SvMap.fold
          (fun i eqs0 acc ->
            let eqs = SvSet.inter eqs0 (u_get_eqs u1 i) in
            if SvSet.cardinal eqs <= 1 then acc (* only i ! *)
            else SvMap.add i eqs acc
          ) u0.u_eqs SvMap.empty in
      let mem =
        SvMap.fold
          (fun i mem0 acc ->
            let mem = SvSet.inter mem0 (u_get_mem u1 i) in
            if mem = SvSet.empty then acc
            else SvMap.add i mem acc
          ) u0.u_mem SvMap.empty in
      let sub =
        SvMap.fold
          (fun i sub0 acc ->
            let sub = SvSet.inter sub0 (u_get_sub u1 i) in
            if sub = SvSet.empty then acc
            else SvMap.add i sub acc
          ) u0.u_sub SvMap.empty in
      (* TODO: I am not sure the lin constraints are always reduced as much as
       *       possible; do some sanity check ? *)
      (* Temporary code: is there a chance to keep any xlin constraint *)
      if u0.u_xlin != [] || u1.u_xlin != [] then
        begin
          Log.warn "u_lub, u_xlin found [%d|%d]" (List.length u0.u_xlin)
            (List.length u1.u_xlin);
          let f xlin sls slo =
            List.iter
              (fun (setv, _) ->
                Log.info " %a => look[%b]: %b,%b" setv_fpr setv
                  (SvMap.mem setv lin)
                  (SvMap.mem setv sls) (SvMap.mem setv slo)
              ) xlin in
          f u0.u_xlin u0.u_lin u1.u_lin;
          f u1.u_xlin u1.u_lin u0.u_lin;
        end;
      let u =
        { u_lin  = lin;
          u_eqs  = eqs;
          u_mem  = mem;
          u_sub  = sub;
          u_xlin = [] } in
      let u = if do_temp_closure then temp_closure u else u in
      (* Still experimental code; trying to preserve xlin constraints *)
      let u =
        if do_preserve_xlin then
          let f_xlin xlin uo u =
            List.fold_left
              (fun acc (i, sl) ->
                if u_sat_lin uo i sl then
                  (* see if it can be added directly *)
                  { acc with u_xlin = (i, sl) :: acc.u_xlin }
                else acc
              ) u xlin in
          f_xlin u1.u_xlin u0 (f_xlin u0.u_xlin u1 u)
        else u in
      u_sanity_check ~oinfo:(Some info) "u_lub,out" u;
      u
    let t_lub (t0: t) (t1: t): t =
      t_sanity_check "t_lub,l" t0;
      t_sanity_check "t_lub,r" t1;
      let info fmt () =
        F.fprintf fmt "L:\n%a\nR:\n%a"
          (t_fpri SvMap.empty " ") t0 (t_fpri SvMap.empty " ") t1 in
      if not (SvSet.equal t0.t_roots t1.t_roots) then
        begin
          let fpr = t_fpri SvMap.empty "  " in
          Log.info "lub:\n L:\n%a R:\n%a" fpr t0 fpr t1;
          Log.fatal_exn "lub: inputs do not have the same roots\nL: %a\nR: %a\n"
            setvset_fpr t0.t_roots setvset_fpr t1.t_roots;
        end;
      let t =
        match t0.t_t, t1.t_t with
        | None, _ -> t1
        | _, None -> t0
        | Some u0, Some u1 -> { t0 with t_t = Some (u_lub u0 u1) } in
      t_sanity_check ~oinfo:(Some info) "t_lub,out" t;
      t

    (* Weak bound: serves as widening *)
    let weak_bnd (t0: t) (t1: t): t = t_lub t0 t1
    (* Upper bound: serves as join and widening *)
    let join (t0: t) (t1: t): t = t_lub t0 t1

    let rec simplify_set_expr e u =
      match e with
      | S_var sid ->
          let b =
            Option.fold ~none:false ~some: (fun x -> sl_is_empty x)
              (SvMap.find_opt sid u.u_lin) in
          if b then S_empty else e
      | S_uplus(e1, e2) ->
          S_uplus (simplify_set_expr e1 u, simplify_set_expr e2 u)
      | _ -> e
    let simplify_set_cons c u =
      match c with
      | S_eq(e1, e2) -> S_eq (simplify_set_expr e1 u, simplify_set_expr e2 u)
      | _ -> c

    (** Set satisfiability *)
    let set_sat (c: set_cons) (t: t): bool =
      t_sanity_check "set_sat,arg" t;
      let loc_debug = false in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "set_sat,call: %a\n%a" set_cons_fpr c
          (t_fpri SvMap.empty "    ") t;
      let fail ( ) =
        Log.info " Set state:\n%a" (t_fpri SvMap.empty "    ") t;
        Log.warn " Sat constraint dropped: %a" set_cons_fpr c;
        false in
      let r =
        match t.t_t with
        | None -> true (* any constraint valid under _|_ *)
        | Some u ->
            let u_l =
              let u_lin =
                SvMap.fold
                  (fun i sl acc ->
                    let sl_sets =
                      SvSet.filter
                        (fun x ->
                          (Option.fold ~none: true
                             ~some:(fun x -> not (sl_is_empty x)))
                            (SvMap.find_opt x u.u_lin))
                        sl.sl_sets in
                    let sl = { sl with sl_sets } in
                    SvMap.add i sl acc
                  ) u.u_lin SvMap.empty in
              from_lin_to_eqs { u with u_lin } in
            let sat_u u' =
              (* trying to support the same constraints as in guard *)
              let c = simplify_set_cons c u' in
              match c with
              | S_mem (i, ex) -> u_sat_mem_ex u' i ex
              | S_eq (S_var i, S_var j) -> u_sat_eq u' i j
              | S_subset (S_var i, S_var j) -> u_sat_sub u' i j
              | S_eq (S_var i, ex) | S_eq (ex, S_var i) ->
                  begin
                    match linearize ex with
                    | None -> fail ( )
                    | Some lin -> u_sat_lin u' i lin
                  end
              | S_eq (e1, e2) ->
                  begin
                    match linearize e1, linearize e2 with
                    | Some sl1, Some sl2 when sl_eq sl1 sl2 -> true
                    | Some sl1, Some sl2 ->
                        let sl1, sl2 = sl_diff sl1 sl2 in
                        begin
                          match sl_is_set sl1, sl_is_set sl2 with
                          | Some setv1, Some setv2 -> u_sat_eq u' setv1 setv2
                          | Some setv, None -> u_sat_lin u' setv sl2
                          | None, Some setv -> u_sat_lin u' setv sl1
                          | None, None -> fail ()
                        end
                    | _, _ -> fail ()
                  end
              | _ -> fail ( ) in
            sat_u u || sat_u u_l in
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "set_sat,return: %b\n" r;
      r


    (** Set condition test *)
    (** [case_disjunction nsat setv t] implements the following heuristics.
     *  If:
     *  + [y] in [setv]
     *  + [t] |= [setv = { x_i } uplus setv']
     *  + [nsat] |= ∀ i, [x_i] != [y]
     *  Then:
     *  [y] in [setv'] *)
    let case_disjunction (nsat: n_cons -> bool) (setv: sv) (u: u): u =
      u_sanity_check "case_disjunction,arg" u;
      let info fmt () =
        F.fprintf fmt "- setv: %a\n- arg:\n%a" setv_fpr setv
          (u_fpri SvMap.empty " ") u in
      let neq y x =
        let res = x <> y && nsat (Nc_cons (DISEQ, Ne_var x, Ne_var y)) in
        Log.debug "%a != %a: %b" nsv_fpr x nsv_fpr y res;
        res in
      if !Flags.flag_dbg_dom_setlin then
        Log.debug "setv = %a" setv_fpr setv;
      match SvMap.find_opt setv u.u_lin with
      | None -> u
      | Some ({ sl_sets ; sl_elts } as lin) ->
          Log.debug "lin%a" (sl_fpr ~sn:SvMap.empty) lin;
          if SvSet.cardinal sl_sets != 1 then u
          else
            let setv' = SvSet.min_elt sl_sets in
            let aux (y: sv) (u: u) : u =
              if SvSet.for_all (neq y) sl_elts then u_add_mem u y setv'
              else u in
            let u = SvSet.fold aux (u_get_mem u setv) u in
            u_sanity_check ~oinfo:(Some info) "case_disjunction,out" u;
            u
    let set_guard_aux nsat (c: set_cons) (t: t): t =
      match t.t_t with
      | None -> t
      | Some u ->
          let u =
            (* Basic constraints added using the utility functions:
             *  all constraints needed in the graph examples are supported,
             *  except the S0 = S1 \cup S2 (not \uplus), as I believe such
             *  constraints should just not arise here! *)
            match c with
            | S_mem (i, S_var j) ->
                u_add_mem u i j
            | S_eq (S_var i, S_empty) | S_eq (S_empty, S_var i) ->
                if SvMap.mem i u.u_lin then Log.warn "lin, already here";
                if not (SvSet.is_empty (u_get_mem u i)) then raise Bottom
                else
                  let cons = { sl_elts = SvSet.empty; sl_sets = SvSet.empty } in
                  if u.u_xlin != [] then Log.warn "set_guard_aux: xlin";
                  if SvMap.mem i u.u_lin then
                    Log.warn "set_guard_aux,lin,already here: %a => %a"
                      setv_fpr i (sl_fpr ~sn:SvMap.empty) cons;
                  let u_lin =
                    SvMap.map
                      (fun sl ->
                        { sl with sl_sets = SvSet.remove i sl.sl_sets }
                      ) u.u_lin in
                  let u_lin, u_eqs =
                    SvMap.fold
                      (fun i sl (accl, acce) ->
                        if sl.sl_elts = SvSet.empty
                            && SvSet.cardinal sl.sl_sets = 1 then
                          let elt = SvSet.choose sl.sl_sets in
                          let eqs =
                            try SvMap.find i acce
                            with Not_found -> SvSet.empty in
                          SvMap.remove i accl,
                          SvMap.add i (SvSet.add elt eqs) acce
                        else accl, acce
                      ) u_lin (u_lin, u.u_eqs) in
                  { u with
                    u_lin = SvMap.add i cons u_lin;
                    u_eqs = u_eqs }
            | S_eq (S_var i, S_var j) ->
                u_add_eq u i j
            | S_eq (S_var i, S_uplus (S_var j, S_var k))
            | S_eq (S_uplus (S_var j, S_var k), S_var i) ->
                u_add_lin u i { sl_sets = SvSet.add j (SvSet.singleton k);
                                sl_elts = SvSet.empty }
            | S_eq (S_var i, S_node j) | S_eq (S_node j, S_var i)
            | S_eq (S_var i, S_uplus (S_empty, S_node j))
            | S_eq (S_var i, S_uplus (S_node j, S_empty)) ->
                u_add_lin u i { sl_sets = SvSet.empty;
                                sl_elts = SvSet.singleton j }
            | S_eq (S_var i, S_uplus (S_node j, S_var k)) ->
                u_add_lin u i { sl_sets = SvSet.singleton k;
                                sl_elts = SvSet.singleton j }
                |> case_disjunction nsat i
            | S_eq (S_var i, e) ->
                begin
                  match linearize e with
                  | Some lin -> u_add_lin u i lin |> case_disjunction nsat i
                  | None ->
                      Log.warn "Set_guard: linearization failed; dropping";
                      u
                end
            | S_subset (S_var i, S_var j) ->
                if !Flags.flag_dbg_dom_setlin then
                  Log.info "inclusion: %a <= %a" nsv_fpr i setv_fpr j;
                u_add_inclusion u i j
            | _ ->
                (* otherwise, we just drop the constraint *)
                Log.warn "Set_guard ignored: %a" set_cons_fpr c;
                u in
          u_sanity_check "set_guard_aux,out" u;
          { t with t_t = Some u }
    let set_guard ?(nsat = None) (c: set_cons) (t: t): t =
      t_sanity_check "set_guard,arg" t;
      let info fmt () =
        F.fprintf fmt "- arg:\n%a" (t_fpri SvMap.empty " ") t in
      let nsat = Option.get_default (fun _ -> false) nsat in
      let gt = try set_guard_aux nsat c t with Bottom -> bot in
      if !Flags.flag_dbg_dom_setlin then
        Log.debug "Guard: %a\n%aResult:\n%a" set_cons_fpr c
          (t_fpri SvMap.empty "   ") t (t_fpri SvMap.empty "   ") gt;
      t_sanity_check ~oinfo:(Some info) "set_guard,out" t;
      gt


    (** Forget (if the meaning of the SV changes, e.g., for an assignment) *)
    let sv_forget (sv: sv) (t: t): t = (* will be used for assign *)
      let f i = i = sv in
      drop_symb_svs f t


    (** Renaming (e.g., post join) *)
    let symvars_srename
        (om: (Offs.t * sv) Offs.OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (svmo: colv_mapping option)
        (t: t)
        : t =
      let loc_debug = false in
      t_sanity_check "symvars_srename,arg" t;
      let info fmt () =
        F.fprintf fmt "- arg:\n%a" (t_fpri SvMap.empty " ") t in
      let omap fs x =
        match svmo with
        | None -> x
        | Some svm -> fs svm x in
      (* Initial status *)
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.info "symvars_srename,call:\n%a" (t_fpri SvMap.empty "  ") t;
      if om != Offs.OffMap.empty then
        Log.fatal_exn "symvars_srename: should get empty OffMap";
      (* Dropping SVs and SETVs that should be dropped *)
      let t = drop_symb_svs (fun i -> SvSet.mem i nm.nm_rem) t in
      let t = omap (fun svm -> drop_setvs (fun i -> SvSet.mem i svm.sm_rem)) t in
      (* Basic transformation functions *)
      let do_sv (i: sv): sv =
        try fst (SvMap.find i nm.nm_map)
        with Not_found -> Log.fatal_exn "symvars_srename,do_sv fails" in
      let do_sv_set (s: SvSet.t): SvSet.t =
        SvSet.fold (fun i acc -> SvSet.add (do_sv i) acc) s SvSet.empty in
      let do_setv =
        let fs svm (i: sv): sv =
          try fst (SvMap.find i svm.sm_map)
          with Not_found ->
            Log.warn "symvars_srename,do_setv fails: %a" setv_fpr i;
            i in
        omap fs in
      let do_setv_set =
        let fs _ (s: SvSet.t): SvSet.t =
          SvSet.fold (fun i a -> SvSet.add (do_setv i) a) s SvSet.empty in
        omap fs in
      let do_set_lin (sl: set_lin): set_lin =
        { sl_elts = do_sv_set sl.sl_elts;
          sl_sets = do_setv_set sl.sl_sets } in
      (* Transformation function for type "u" *)
      let do_u (u: u): u =
        let fmap f m = (* rename everything *)
          SvMap.fold (fun setv x -> SvMap.add (do_setv setv) (f x))
            m SvMap.empty in
        let f_xlin l =
          List.map (fun (setv, sl) -> do_setv setv, do_set_lin sl) l in
        let u = { u_lin  = fmap do_set_lin u.u_lin;
                  u_mem  = fmap do_sv_set u.u_mem;
                  u_sub  = omap (fun _ -> fmap do_setv_set) u.u_sub;
                  u_eqs  = omap (fun _ -> fmap do_setv_set) u.u_eqs;
                  u_xlin = f_xlin u.u_xlin } in
        omap
          (fun svm u ->
            SvMap.fold (* adding equalities from the mapping *)
              (fun _ (setv0, s) acc ->
                SvSet.fold (fun j acc -> u_add_eq acc setv0 j) s acc
              ) svm.sm_map u
          ) u in
      let u =
        match t.t_t with
        | None -> None
        | Some u ->
            if !Flags.flag_dbg_dom_setlin || loc_debug then
              begin
                Log.debug "symvars_srename,argument:\n%anode mapping:\n%a"
                  (t_fpri SvMap.empty "  ") t node_mapping_fpr nm;
                match svmo with
                | None -> ( )
                | Some svm -> Log.debug "setv-map:\n%a" colv_mapping_fpr svm
              end;
            Some (do_u u) in
      (* Roots should never be modified; fail if they do *)
      SvSet.iter
        (fun i ->
          if do_setv i != i then
            Log.fatal_exn "symvars_srename should not modify SETV root"
        ) t.t_roots;
      let tr = { t with t_t = u } in
      t_sanity_check ~oinfo:(Some info) "symvars_srename,out" tr;
      if !Flags.flag_dbg_dom_setlin || loc_debug then
        Log.debug "symvars_srename,return:\n%a" (t_fpri SvMap.empty "   ") tr;
      tr

    (* Synchronization of the SV environment*)
    let sve_sync_top_down (sve: svenv_mod) (t: t): t =
      (* do nothing for add; remove constraints over mod and rem *)
      let f i = PSet.mem i sve.svm_rem || PSet.mem i sve.svm_mod in
      drop_symb_svs f t

    (* Removes all symbolic vars that are not in a given set *)
    let symvars_filter (skeep: SvSet.t) (setvkeep: SvSet.t) (t: t): t =
      (* Removals are done by the generic removal functions *)
      drop_symb_svs (fun i -> not (SvSet.mem i skeep))
        (drop_setvs (fun i -> not (SvSet.mem i setvkeep)) t)
  end: DOM_SET)
