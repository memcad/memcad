(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_add_diseqs.ml
 **       addition of equalities to a numerical domain
 **         (without bottom interface)
 ** Xavier Rival, 2011/08/08 *)
open Apron
open Data_structures
open Lib
open Sv_def

open Nd_sig

open Nd_utils

module Log =
  Logger.Make(struct let section = "nd_+equl" and level = Log_level.DEBUG end)


(** Equalities are represented using union finds over atoms *)
let rem_atom (at: atom) (au: atom Union_find.t): atom Union_find.t =
  if Union_find.mem at au then
    let _,te = Union_find.rem at au in te
  else au

let union_atom (at: atom) (r: atom) (au: atom Union_find.t)
    : atom Union_find.t =
  let re, au =
    if Union_find.mem r au then
      Union_find.find r au
    else
      r, Union_find.add r au in
  let ae, au =
    if Union_find.mem at au then
      Union_find.find at au
    else
      at, Union_find.add at au in
  Union_find.union re ae au

let add_atom (at: atom) (r: atom) (au: atom Union_find.t): atom Union_find.t =
  let au = rem_atom at au in
  union_atom at r au


(** Module adding disequality constraints on top of another numerical domain *)
module Add_eqs = functor (M: DOM_NUM_NB) ->
  (struct
    let module_name = "nd_add_eqs"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name M.module_name M.config_fpr ()

    type t =
        { t_u: M.t ;              (* underlying: equalities, inequalities *)
          t_e: atom Union_find.t  (* equalities *) }

    (* Bottom element (might be detected) *)
    let is_bot (t: t): bool =
      (* Conflicting equalities and disequalities get reduced to _|_
       *  incrementally, via "raise Bottom" *)
      M.is_bot t.t_u

    (* Top element *)
    let top: t =
      { t_u = M.top ;
        t_e = Union_find.empty }

    (* Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) fmt (t: t): unit =
      let disp = ref true in
      let nind = ind^"  " in
      List.iter
        (fun cl ->
          if List.length cl > 1 then
            let fpp = gen_list_fpr "" (atom_fpr sn) " = " in
            if !disp then F.fprintf fmt "%sEquality classes:\n" ind;
            disp := false;
            F.fprintf fmt "%s%a\n" nind fpp cl
        ) (Union_find.get_classes t.t_e);
      M.t_fpri sn ind fmt t.t_u

    (* Variables managemement *)
    let sv_add (i: sv) (t: t): t =
      { t with t_u = M.sv_add i t.t_u }
    let sv_rem (i: sv) (t: t): t =
      { t_u = M.sv_rem i t.t_u;
        t_e = rem_atom (Anode i) t.t_e }

    (* Renaming *)
    let vars_srename (nr: 'a node_mapping) (t: t): t =
      let af_rem =
        SvSet.fold (fun key -> rem_atom (Anode key)) nr.nm_rem t.t_e in
      (* First we use the mapping to rename elements in the Union_find *)
      let f = function
        | Acst _ as c -> c
        | Anode sv ->
            match SvMap.find_opt sv nr.nm_map with
            | Some (sv', _) -> Anode sv'
            | None -> Anode sv in
      let af_map = Union_find.vars_rename f af_rem in
      (* Then we add the equalities induced by the mapping.
       * ie. the equalities between [el] and elements of [set].
       * We have to do it in a separate loop because there might be entries :
       *    N_i => N_j { N_k ; N_l }
       *    N_k => N_n *)
      let af_map =
        SvMap.fold
          (fun key (el, set) acc ->
            if SvSet.is_empty set then acc
            else
              let atom_el = Anode el in
              let rep, acc =
                if Union_find.mem atom_el acc then
                  Union_find.find atom_el acc
                else atom_el, Union_find.add atom_el ~rep:atom_el acc in
              let acc =
                SvSet.fold (fun sv acc -> Union_find.add (Anode sv) ~rep acc)
                  set acc in
              acc
          ) nr.nm_map af_map in
      { t_u = M.vars_srename nr t.t_u ;
        t_e = af_map }
    let check_nodes (s: SvSet.t) (t: t): bool =
      M.check_nodes s t.t_u
    let nodes_filter (nkeep: SvSet.t) (t: t): t =
      (* ljc: incomplete *)
      { t_u = M.nodes_filter nkeep t.t_u ;
        t_e = t.t_e }

    (** Comparison, Join and Widening operators *)
    let is_le (t0: t) (t1: t) (f: sv -> sv -> bool): bool =
      M.is_le t0.t_u t1.t_u f
    let join (t0: t) (t1: t): t =
      { t_u = M.join t0.t_u t1.t_u ;
        t_e = Union_find.meet t0.t_e t1.t_e }
    let widen (t0: t) (t1: t): t =
      { t_u = M.widen t0.t_u t1.t_u ;
        t_e = Union_find.meet t0.t_e t1.t_e }

    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat (cons: n_cons) (t: t): bool =
      match cons with
      | Nc_cons (Tcons1.EQ, Ne_var vi0, Ne_var vi1) ->
          (Union_find.is_same_class (Anode vi0) (Anode vi1) t.t_e
         || M.sat cons t.t_u)
      | Nc_cons (Tcons1.DISEQ, Ne_var vi, Ne_csti ci)
      | Nc_cons (Tcons1.DISEQ, Ne_csti ci, Ne_var vi) ->
          not (Union_find.is_same_class (Acst ci) (Anode vi) t.t_e)
            && M.sat cons t.t_u
      | Nc_cons (Tcons1.DISEQ, Ne_var v0, Ne_var v1) ->
          not (Union_find.is_same_class (Anode v0) (Anode v1) t.t_e)
            && M.sat cons t.t_u
      | _ -> (* send to underlying domain *)
          M.sat cons t.t_u

    (** Transfer functions *)
    let assign (dst: sv) (expr: n_expr) (t: t): t =
      let te =
        match expr with
        | Ne_var i ->
            add_atom (Anode dst) (Anode i) t.t_e
        | Ne_csti c ->
            add_atom (Anode dst) (Acst c) t.t_e
        |  _ -> rem_atom (Anode dst) t.t_e in
      { t_u = M.assign dst expr t.t_u ;
        t_e = te }
    let guard ?(no_apron=false) (b: bool) (cons: n_cons) (t: t): t =
      assert b;
      (* simplification on offset expressions *)
      let cons = Nd_utils.n_cons_simplify cons in
      match cons with
      | Nc_cons (Tcons1.DISEQ, Ne_var v0, Ne_var v1) ->
          (* Incremental reduction:
           *  -> if the equality is trivially satisfied or satisfied in num,
           *     then we reduce to _|_ immediately *)
          if v0 = v1 then raise Bottom
          else if sat (Nc_cons (Tcons1.EQ, Ne_var v0, Ne_var v1)) t then
            raise Bottom
          else
            { t with t_u = M.guard ~no_apron b cons t.t_u }
      | Nc_cons (Tcons1.DISEQ, Ne_var vi, Ne_csti ci)
      | Nc_cons (Tcons1.DISEQ, Ne_csti ci, Ne_var vi) ->
          (* Incremental reduction:
           *  -> if the equality is satisfied in the num domain, we
           *     reduce to _|_ immediately *)
          if sat (Nc_cons (Tcons1.EQ, Ne_var vi, Ne_csti ci)) t then
            raise Bottom
          else
            { t with t_u = M.guard ~no_apron b cons t.t_u }
      | Nc_cons (Tcons1.EQ, Ne_var v0, Ne_var v1) ->
          { t_u = M.guard ~no_apron b cons t.t_u;
            t_e = union_atom (Anode v0) (Anode v1) t.t_e }
      | Nc_cons (Tcons1.EQ, Ne_var v0, Ne_csti c0)
      | Nc_cons (Tcons1.EQ, Ne_csti c0, Ne_var v0) ->
          { t_u = M.guard ~no_apron b cons t.t_u;
            t_e = union_atom (Anode v0) (Acst c0) t.t_e }
            (* new case: reduction "e0 >= e1" to "e0 > e1" *)
      | _ ->
          { t with t_u = M.guard ~no_apron b cons t.t_u }

    (** Utilities for the abstract domain *)
    let simplify_n_expr (t: t): n_expr -> n_expr = M.simplify_n_expr t.t_u

    (** Summarizing dimensions related operations *)
    (* Expand the constraints on one dimension to another *)
    let expand (id: sv) (nid: sv) (x: t): t =
      { x with t_u = M.expand id nid x.t_u }
    (* Upper bound of the constraits of two dimensions *)
    let compact (lid: sv) (rid: sv) (x: t): t =
      { x with t_u = M.compact lid rid x.t_u }

    (** Conjunction *)
    let meet (lx: t) (rx: t): t =
      { lx with t_u = M.meet lx.t_u rx.t_u }

    (** Forget the information on a dimension *)
    let sv_forget (id: sv) (x: t): t =
      { t_e = rem_atom (Anode id) x.t_e;
        t_u = M.sv_forget id x.t_u }

    (** Export of range information *)
    (* bound of a variable *)
    let bound_variable (dim: sv) (x: t): interval =
      M.bound_variable dim x.t_u

    let get_svs (x: t): SvSet.t =
      M.get_svs x.t_u

    let get_eq_class (a: sv) (x: t): SvSet.t =
      if Union_find.mem (Anode a) x.t_e then
        List.fold_left
          (fun acc akey ->
            match akey with
            | Anode i -> SvSet.add i acc
            | _ -> acc
          ) SvSet.empty
          (Union_find.find_class (fst (Union_find.find (Anode a) x.t_e)) x.t_e)
      else
        SvSet.singleton a
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      SvSet.union
        (M.get_upper_svs id x.t_u)
        (get_eq_class id x)
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      SvSet.union
        (M.get_lower_svs id x.t_u)
        (get_eq_class id x)
  end: DOM_NUM_NB)
