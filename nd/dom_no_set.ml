(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_no_set.ml
 **       Lift functor from value domain to domain for value + sets
 **       => this module adds no set information, does almost nothing
 ** Xavier Rival, 2014/08/12 *)
open Data_structures
open Lib
open Offs
open Sv_def

open Ast_sig
open Nd_sig
open Svenv_sig
open Col_sig
open Seq_sig
open Set_sig
open Vd_sig

open Set_utils


(** Error handling *)
module Log =
  Logger.Make(struct let section = "d_nset__" and level = Log_level.DEBUG end)


module DBuild = functor
  ( M: DOM_VALUE ) ->
  (struct
    let module_name = "dom_no_set"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name M.module_name M.config_fpr ()

    (** Type of abstract values *)
    type t = M.t

    (** Domain initialization *)
    (* Domain initialization to a set of inductive definitions *)
    let init_inductives: SvGen.t -> StringSet.t -> SvGen.t =
      M.init_inductives

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t = M.bot
    let is_bot: t -> bool = M.is_bot
    (* Top element *)
    let top: t = M.top
    (* Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) (fmt: form) (x: t): unit =
      F.fprintf fmt "%sNumerical Part:\n%a" ind (M.t_fpri sn ("  "^ind)) x

    (** Management of symbolic variables *)
    (* For sanity check *)
    let check_nodes ~(svs: SvSet.t) ~(colvs: col_kinds): t -> bool =
      M.check_nodes svs
    (* symbolic variables *)
    let sv_add ?(mark: bool = true) (i: sv) (t: t): t =
      if mark then M.sv_add i t
      else t
    let sv_rem: sv -> t -> t = M.sv_rem

    (** Collection variables **)
    (* add collection variable with the given name *)
    let colv_add
        (ck: col_kind)
        ~(root: bool)
        ?(kind: set_par_type option = None)
        ?(name: string option = None) (_: sv) (_: colv_info option) (_: t): t =
      Log.fatal_exn "colv_add: no enabled domain offers support"
    let colv_rem (_: sv) (t: t): t = t
    let colv_is_root _ = raise Col_unsup
    let colv_get_roots (t: t): SvSet.t = raise Col_unsup

    (** General managment of symbolic variables *)
    (* The signatures of the functions below is likely to change with sets *)
    (* Renaming (e.g., post join) *)
    let symvars_srename
        ?(mark: bool = true)
        (o: (Offs.t * sv) OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (_: colv_map_complete option)
        (t: t): t =
      if mark then M.symvars_srename o nm t
      else t
    (* Synchronization of the SV environment *)
    let sve_sync_top_down: svenv_mod -> t -> t = M.sve_sync_top_down
    (* Check the symbolic vars correspond exactly to given set *)
    let symvars_check: SvSet.t -> t -> bool = M.symvars_check
    (* Removes all symbolic vars that are not in a given set *)
    let symvars_filter (s: SvSet.t) ?(col_vars: SvSet.t = SvSet.empty)
        (t: t): t = M.symvars_filter s t
    (* Merging into a new variable, arguments:
     *  . the stride of the structure being treated
     *  . node serving as a base address of a block;
     *  . node serving as a new block abstraction (as a sequence of bits)
     *  . old contents of the block, that is getting merged
     *  . offsets of external pointers into the block to build an
     *    environment *)
    let symvars_merge: int -> sv -> sv -> (Bounds.t * Offs.svo * Offs.size) list
      -> OffSet.t -> t -> t = M.symvars_merge

    (** Comparison and join operators *)
    (* Comparison *)
    let is_le: t -> t -> (sv -> sv -> bool) -> bool = M.is_le
    (* Upper bound: serves as join and widening *)
    let upper_bnd: ?hint: SvPrSet.t -> join_kind -> t -> t -> t = M.upper_bnd

    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat: t -> n_cons -> bool = M.sat

    (** Set satisfiability *)
    let set_sat (c: set_cons) (_: t): bool =
      Log.warn "set_sat (called without set support): %a" set_cons_fpr c;
      false

    (** Condition test *)
    let guard ?(no_apron=false): bool -> n_cons -> t -> t = M.guard ~no_apron
    (** Set condition test *)
    let set_guard (c: set_cons) (t: t): t =
      Log.warn "set_guard (called without set support): %a" set_cons_fpr c;
      t

    (** Seq satisfiability and condition test *)
    (* Either done at level above, or conservatively ignored *)
    let seq_sat (_: seq_cons) (_: t): bool = false
    let seq_guard (_: seq_cons) (t: t): t = t

    (** Assignment *)
    let assign: sv -> n_expr -> t -> t = M.assign
    (* Assignment inside a sub-memory *)
    let write_sub: sub_dest -> int -> n_rval -> t -> t = M.write_sub

    (** Utilities for the abstract domain *)
    let simplify_n_expr: t -> n_expr -> n_expr = M.simplify_n_expr

    (** Array domain specifc functions  *)
    (* Add an array content node  *)
    let sv_array_add: sv -> int -> int list -> t -> t = M.add_array_node
    (* Add an array address node  *)
    let sv_array_address_add: sv -> t -> t = M.add_array_address
    (* Checks wheter this node is the address of an array node *)
    let is_array_address: sv -> t -> bool = M.is_array_address
    (* Dereference an array cell in experision,
     * this function may cause disjunction *)
    let sv_array_deref: sv -> Offs.t -> t -> (t * sv) list =
      M.array_node_deref
    (* Dereference an array cell in l-value,
     * no disjunction is created since it merges groups *)
    let sv_array_materialize: sv -> Offs.t -> t -> t * sv =
      M.array_materialize

    (** Sub-memory specific functions *)
    (* Checks whether a node is of sub-memory type *)
    let is_submem_address: sv -> t -> bool = M.is_submem_address
    let is_submem_content: sv -> t -> bool = M.is_submem_content
    (* Read of a value inside a submemory block *)
    let submem_read: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo =
      M.submem_read
    let submem_deref: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo =
      M.submem_deref
    (* Localization of a node in a sub-memory *)
    let submem_localize: sv -> t -> sub_localize = M.submem_localize
    (* Binding of an offset in a sub-memory *)
    let submem_bind: sv -> sv -> Offs.t -> t -> t * Offs.svo = M.submem_bind
    (* Unfolding *)
    let unfold: sv -> sv -> unfold_dir -> t -> (sv SvMap.t * t) list =
      M.unfold
    (* Check predicates on array *)
    let assume (op: vassume_op): t -> t =
      M.assume op
    let check (op: vcheck_op): t -> bool =
      M.check op

    (** Summarzing dimensions related operations *)
    (* Expand the constraints on one dimension to another *)
    let expand: sv -> sv -> t -> t = M.expand
    (* Upper bound of the constraits of two dimensions *)
    let compact: sv -> sv -> t -> t = M.compact

    (** Conjunction *)
    let meet: t -> t -> t = M.meet

    (* Forget the information about an SV *)
    let sv_forget: sv -> t -> t = M.sv_forget
    (* Export of range information *)
    let sv_bound: sv -> t -> interval = M.bound_variable
    (* Extract the set of all SVs *)
    let get_svs: t -> SvSet.t = M.get_svs
    (* Extract all SVs that are equal to a given SV *)
    let get_eq_class: sv -> t -> SvSet.t = M.get_eq_class
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs: sv -> t -> SvSet.t = M.get_upper_svs
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs: sv -> t -> SvSet.t = M.get_lower_svs
  end: DOM_VALCOL)
