(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Thierry Martinez, Pascal Sotin, Antoine Toubhans, Pippijn
 **          Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sv_utils.mli
 **       opaque definition and operations over symbolic variables
 **       signatures of the symbolic variables description
 ** 2020/12/02 *)
open Data_structures
open Lib
open Sv_def

(** Comparison *)
val sv_compare: sv -> sv -> int

(** Conversions/Unsafe map *)
val sv_unsafe_of_int: int -> sv
val sv_to_int:        sv -> int
val sv_unsafe_map:    (int -> int) -> (sv -> sv)

(** Pretty printing *)
(* Node types *)
val ntyp_fpr: form -> ntyp -> unit
val ntyp_fprs: form -> ntyp -> unit
(* Over SVs *)
val sv_fpr: form -> sv -> unit
(* Over lists of SVs *)
val sv_list_fpr: form -> sv list -> unit
(* Over sets *)
val svprset_fpr: form -> SvPrSet.t -> unit
val svset_fpr: form -> SvSet.t -> unit

(** Transformations over modules *)
val svset_of_intset: Data_structures.IntSet.t -> SvSet.t
val svmap_of_intmap: 'a Data_structures.IntMap.t -> 'a SvMap.t
val svmap_of_list: 'a list -> 'a SvMap.t
