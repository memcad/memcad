(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: col_utils.ml
 **       utilities for collection domains
 ** Josselin Giet, 2021/10/15 *)
open Data_structures

open Sv_def

open Ast_sig
open Nd_sig
open Vd_sig
open Col_sig
open Set_sig
open Seq_sig

open Ast_utils
open Nd_utils
open Set_utils
open Seq_utils
open Sv_utils

module Log =
  Logger.Make(struct let section = "col_uts_" and level = Log_level.DEBUG end)

(** Utilities for col_kind SvMap.t *)
let col_kind_map_union (c0: col_kinds) (c1: col_kinds): col_kinds =
  SvMap.fold SvMap.add c0 c1
let col_par_type_to_col_kind = function
  | Ct_set _ -> CK_set
  | Ct_seq _ -> CK_seq
let col_par_type_map_to_col_kinds = SvMap.map col_par_type_to_col_kind

(** Mapping functions *)
(* Empty mapping *)
let gen_colv_mapping_empty (k: col_kind option): colv_mapping =
  { sm_kind   = k;
    sm_map    = SvMap.empty;
    sm_rem    = SvSet.empty; }
let colv_mapping_empty = gen_colv_mapping_empty None
let set_colv_mapping_empty = gen_colv_mapping_empty (Some CK_set)
let seq_colv_mapping_empty = gen_colv_mapping_empty (Some CK_seq)

(* Addition of a new node *)
let add_to_mapping (i: sv) (j: sv) (m: colv_mapping): colv_mapping =
  let map =
    try
      let k, s = SvMap.find i m.sm_map in
      SvMap.add i (k, SvSet.add j s) m.sm_map ;
    with
    | Not_found -> SvMap.add i (j, SvSet.empty) m.sm_map in
  { sm_kind = m.sm_kind ;
    sm_map = map ;
    sm_rem = SvSet.remove i m.sm_rem }

(* Pretty-printing *)
let col_par_type_fpr (fmt: form): col_par_type -> unit = function
  | Ct_set _ -> F.fprintf fmt "set"
  | Ct_seq _ -> F.fprintf fmt "seq"
let colv_mapping_fpr (fmt: form) (m: colv_mapping): unit =
  let v_fpr, set_fpr =
    match m.sm_kind with
    | None -> nsv_fpr, nsvset_fpr
    | Some CK_set -> setv_fpr, setvset_fpr
    | Some CK_seq -> seqv_fpr, seqvset_fpr in
  SvMap.iter
    (fun i (j, s) ->
      F.fprintf fmt "        %a -> %a { %a }\n" v_fpr i v_fpr j set_fpr s
    ) m.sm_map ;
  F.fprintf fmt "     Removed: %a\n" set_fpr m.sm_rem
let colv_map_complete_fpr (fmt: form) (c: colv_map_complete)
    : unit =
  F.fprintf fmt "Colvar Mapping:\n  Set part\n%a  Seq part\n%a"
    colv_mapping_fpr c.cmc_set colv_mapping_fpr c.cmc_seq
let colv_info_fpr (fmt: form) {max; min; size}: unit =
  let fpr fmt sv =
    let s = F.asprintf "%a" nsv_fpr sv in
    let n = Stdlib.max 1 (9 - String.length s) in
    (*Log.info "colv_info_fpr: %d" n;*)
    if true then assert (n > 0); (* otherwise, strange seg fault *)
    Format.fprintf fmt "%s%s" (String.make n ' ') s in
  Format.fprintf fmt "%a%a%a" fpr min fpr size fpr max

(* Extraction of mappings *)
let extract_mappings  (sr: (sv * sv) SvMap.t)
    : colv_mapping * colv_mapping * SvSet.t  =
  SvMap.fold
    (fun i (il, ir) (accl, accr, seti) ->
      add_to_mapping il i accl, add_to_mapping ir i accr,
      SvSet.add i seti
    ) sr (colv_mapping_empty, colv_mapping_empty, SvSet.empty)

let colv_mapping_join (map1: colv_mapping) (map2: colv_mapping): colv_mapping =
  assert (map1.sm_kind = map2.sm_kind);
  let sm_map = SvMap.merge
    (fun colv x1 x2 ->
      match x1, x2 with
      | Some x, None | None, Some x -> Some x
      | None, None -> None
      | Some _, Some _ ->
          Log.fatal_exn "mapping_join: %a is mapped twice" sv_fpr colv
    ) map1.sm_map map2.sm_map in
  let sm_rem = SvSet.union map1.sm_rem map2.sm_rem in
  { sm_kind = map1.sm_kind ;
    sm_map  = sm_map;
    sm_rem  = sm_rem }

(* Renaming *)
let colv_info_rename
    (nm: 'a node_mapping)
    (opt_colv_map: colv_map_complete option)
    (ck: col_kind)
    (infos: colv_info SvMap.t)
    : colv_info SvMap.t =
  let rename_sv sv =
    try fst @@ SvMap.find sv nm.nm_map
    with Not_found -> sv in
  let rename_info {min; max; size} =
    let min = rename_sv min in
    let max = rename_sv max in
    let size = rename_sv size in
    {min; max; size} in
  let rename_colv =
    match opt_colv_map, ck with
    | None, _ -> fun colv -> colv
    | Some { cmc_set; _ }, CK_set ->
        begin
          fun setv ->
            try SvMap.find setv cmc_set.sm_map |> fst
            with Not_found -> setv
        end
    | Some { cmc_seq; _ }, CK_seq ->
        begin
          fun seqv ->
            try SvMap.find seqv cmc_seq.sm_map |> fst
            with Not_found -> seqv
        end in
  SvMap.fold
    (fun colv info acc ->
      let colv = rename_colv colv in
      let info = rename_info info in
      SvMap.add colv info acc)
    infos SvMap.empty

(* Splitting colv sets *)
let colv_embedding_comp_project (ck: col_kind) (c: colv_embedding_comp)
    : SvSet.t SvMap.t =
  SvMap.fold
    (fun cv (s, k) acc ->
      if k = ck then SvMap.add cv s acc else acc
    ) c SvMap.empty
let colv_embedding_split (cc: colv_embedding_comp)
    : SvSet.t SvMap.t * SvSet.t SvMap.t =
  SvMap.fold
    (fun cv (s, k) (accs, accq) ->
      match k with
      | CK_set -> SvMap.add cv s accs, accq
      | CK_seq -> accs, SvMap.add cv s accq
    ) cc (SvMap.empty, SvMap.empty)

(** Functions on col var injections (for is_le) *)
module CEmb =
  struct
    let empty: colv_embedding =
      { n_img = SvMap.empty ;
        n_pre = SvMap.empty }
    (* To string, compact version *)
    let ne_fpr (fmt: form) (ni: colv_embedding): unit =
      let fpr fmt (s, ck) =
        F.fprintf fmt "%a:%a" col_kind_fpr ck (SvSet.t_fpr ",") s in
      SvMap.t_fpr "\n" fpr fmt ni.n_img
    (* To string, long version *)
    let ne_full_fpri (ind: string) (fmt: form) (inj: colv_embedding): unit =
      SvMap.iter
        (fun i (j, ck) ->
          match ck with
          | CK_set ->
              F.fprintf fmt "%s%a => %a\n" ind setv_fpr i setvset_fpr j
          | CK_seq ->
              F.fprintf fmt "%s%a => %a\n" ind seqv_fpr i seqvset_fpr j
        ) inj.n_img
    (* Tests membership *)
    let mem (i: sv) (ni: colv_embedding): bool =
      SvMap.mem i ni.n_img
    (* Find an element in the mapping *)
    let find (i: sv) (ni: colv_embedding): SvSet.t =
      SvMap.find i ni.n_img |> fst
    (* Add an element to the mapping *)
    let add (i: sv) (j: sv) (kind: col_kind)
        (ni: colv_embedding): colv_embedding =
      let j_set =
        try
          let set, skind = SvMap.find i ni.n_img in
          let _ = assert(kind = skind) in
          set
        with Not_found -> SvSet.empty in
      let i_set =
        try
          let set, skind = SvMap.find j ni.n_pre in
          let _ = assert(kind = skind) in
          set
        with Not_found -> SvSet.empty in
      { n_img = SvMap.add i (SvSet.add j j_set, kind) ni.n_img ;
        n_pre = SvMap.add j (SvSet.add i i_set, kind) ni.n_pre; }
    (* Initialization *)
    let init (m: colv_emb) : colv_embedding =
      Aa_maps.fold
        ( fun colv_l (colv_r, ck) acc -> add colv_l colv_r ck acc)
        m empty
    (* Extraction of siblings information *)
    (*  returns the left to right mapping with right element greater than 1 *)
    let siblings (ni: colv_embedding): (SvSet.t * col_kind) SvMap.t =
      SvMap.fold
        (fun j pre acc ->
          if (SvSet.cardinal @@ fst pre) > 1 then SvMap.add j pre acc
          else acc
        ) ni.n_pre SvMap.empty
    (* Forget, temporary code *)
    let direct_image (ni: colv_embedding): SvSet.t SvMap.t =
      SvMap.map fst ni.n_img
  end
