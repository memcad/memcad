(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: seq_utils.mli
 **       utilities for seq domain
 ** Josselin Giet, 2019/04/12 *)
open Data_structures
open Lib
open Sv_def

open Vd_sig
open Nd_sig
open Col_sig
open Seq_sig

(** Pretty-printing *)
val seqv_fpr: form -> sv -> unit
val seqvset_fpr: form -> SvSet.t -> unit
val seqvmap_fpr: string -> (form -> 'a -> unit) ->  form -> 'a SvMap.t -> unit
val seq_expr_fpr: form -> seq_expr -> unit
val seq_expr_mrk_fpr: form -> seq_expr_mrk -> unit
val seq_cons_fpr: form -> seq_cons -> unit
val seq_cons_mrk_fpr: form -> seq_cons_mrk -> unit
val seq_par_type_fpr: form -> seq_par_type -> unit
val cut_fpr: form -> cut -> unit
val cutted_expr_fpr: form -> cutted_expr -> unit

(** Basic utilities *)
val make_equality: sv -> sv -> seq_cons
val gen_eq: sv list -> sv list -> seq_expr SvMap.t -> seq_expr SvMap.t

(** Mapping functions *)
(* Map translation *)
val q_expr_map_aux: (sv -> seq_expr) -> (sv -> seq_expr) -> seq_expr -> seq_expr
val q_cons_map_aux: (sv -> seq_expr) -> (sv -> seq_expr) -> seq_cons -> seq_cons
val q_expr_map: (sv -> sv) -> (sv -> sv) -> seq_expr -> seq_expr
val q_cons_map: (sv -> sv) -> (sv -> sv) -> seq_cons -> seq_cons

(** Set of setv that appear *)
val seq_expr_seqvs: seq_expr -> SvSet.t
val seq_cons_seqvs: seq_cons -> SvSet.t
val seq_expr_seqvs_out_sort: seq_expr -> SvSet.t
val seq_expr_svs: seq_expr -> SvSet.t
val seq_cons_svs: seq_cons -> SvSet.t

(** [normalise expr] returns normalised expression equal to [expr]:
  - flatten possibly nested [Seq_Concat]
  - flatten possibly nested [Seq_Sort]
  - [Seq_Concat([])] -> [Seq_empty]
  - [Seq_Concat([x])] -> x (when [x] is [Seq_var], [Seq_val], [Seq_sort]) *)
val normalise: seq_expr -> seq_expr

val seq_cons_to_seq_cons_mrk: seq_cons -> seq_cons_mrk
val subst_seqcons_list_aux: seq_cons_mrk -> qid list -> seq_expr_mrk list -> seq_cons_mrk
val subst_seqcons_list: seq_cons -> qid list -> seq_expr_mrk list -> seq_cons_mrk
val split_list: seq_cons_mrk list -> seq_cons list

(** [remove_empty empty_seqvs expr] rewrite [expr] by removing all
    [seqv] \in [empty_seqvs] *)
val remove_empty: SvSet.t -> seq_expr -> seq_expr

(** [right_atom_val expr] returns [true] iff the rightmost atomic expression
 * is a value *)
val right_atom_val: seq_expr -> bool

exception SidemostAtomIsSort

(** [empty_leftmost expr] returns [atom, expr'] s.t. [expr = atom·expr'] *)
val empty_leftmost: seq_expr -> seq_expr * seq_expr
(** [empty_rightmost expr] returns [atom, expr'] s.t. [expr = expr'·atom] *)
val empty_rightmost: seq_expr -> seq_expr * seq_expr
(** [empty_sidemost_double b expr1 expr2] returns
    [atom1, expr1', atom2, expr2'] s.t:
    - if [b] is [true] [atom*] are the leftmost,
    - the rightmost, otherwise  *)
val empty_sidemost_double: bool -> seq_expr -> seq_expr ->
  seq_expr * seq_expr * seq_expr * seq_expr

(** [seq_expr_not_empty expr] returns [true] iff [expr] is a non-empty
    sequence. i.e. it contains at least on value node *)
val seq_expr_not_empty: seq_expr -> bool

(** [factorize seqv expr expr'] replaces all subsequences equal to [expr] in
    [expr'] by [seqv].
    The boolean returned value is [true]
    iff at least one rewriting has been performed. *)
val factorize: sv -> seq_expr -> seq_expr -> seq_expr * bool

(** Pruning some SEQVs from a list of seq constraints *)
(* This function should return an equivalent set of constraints, where
 * some SEQVs are removed (it is used for is_le) *)
val seq_cons_prune_seqvs: SvSet.t -> seq_cons list -> seq_cons list

(* replace instantiated seq variables & sv from a seq expression*)
val replace_expr: seq_expr -> seq_expr SvMap.t -> sv SvMap.t ->
  seq_expr
(* replace instantiated seq variables & sv from seq constraints *)
val replace_cons: seq_cons list -> seq_expr SvMap.t -> sv SvMap.t
  -> seq_cons list

val is_le_instantiate: seq_cons list -> seq_expr SvMap.t -> sv SvMap.t
  -> seq_expr SvMap.t * seq_cons list
val check_non_inst: seq_cons list -> seq_expr SvMap.t -> SvSet.t

val split_cons_fresh: seq_cons list -> SvSet.t -> seq_cons list * seq_cons list

(** Check properties of set parameters *)
val seq_par_type_is_const: seq_par_type -> bool
val seq_par_type_is_add: seq_par_type -> bool

(** Information utilities *)
val get_expr_len: colv_info SvMap.t -> seq_expr -> n_expr
val get_cons_len: colv_info SvMap.t -> seq_cons -> n_cons

val to_set_expr: seq_expr -> Set_sig.set_expr
val to_set_cons: seq_cons -> Set_sig.set_cons

val cut_expr: seq_expr list -> cutted_expr option

module MSet: sig
  type t

  val t_fpri: string -> form -> t -> unit

  val empty: t
  val is_empty_seqv: t -> bool
  val is_empty_sv: t -> bool
  val is_empty: t -> bool

  val mem_seqv: t -> sv -> int
  val mem_sv: t -> sv -> int

  val rm_seqv: sv -> t -> t

  val add_expr: seq_expr -> t -> t
  val of_expr: seq_expr -> t
  val sym_diff: t -> t -> t * t

end