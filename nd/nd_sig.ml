(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: nd_sig.ml
 **       numerical domain layers signatures
 ** Xavier Rival, 2011/07/03 *)
open Apron
open Data_structures
open Lib
open Ast_sig
open Sv_def

(** For incremental bottom reduction *)
(* When an abstract value representing _|_ is encountered,
 * and if Flags.do_raise_on_bottom is set,
 * the exception below may be raised *)
exception Bottom

(** Carrying out names through printers *)
type sv_namer = string SvMap.t (* maps certain SVs to a string *)

(** A format of expressions and constraints for the numeric domain only *)
(* It is rather close to Apron expressions, but there are differences:
 *  - no environment, no "string" vars
 *  - no notion of type (for now)
 *  - Ne_rand may evaluate to any value; it should appear only at the top-level
 *)
type n_expr =
  | Ne_rand (* random value *)
  | Ne_csti of int (* for now, only integer values *)
  | Ne_cstc of char
  | Ne_var of sv (* corresponds to a node in the graph *)
  | Ne_bin of Texpr1.binop * n_expr * n_expr
type n_cons =
  | Nc_rand
  | Nc_bool of bool
  | Nc_cons of Tcons1.typ * n_expr * n_expr

(** Support for arrays requires exporting range information *)
(* An interval: None means infinity *)
type interval =
    { intv_inf: int option;
      intv_sup: int option; }

(** For the interface with the shape domain *)
(* Mapping from one join argument into the result
 * - nm_map:
 *     map a nodeID into: a nodeID + a set of other nodesIDs
 *     (renaming performed in two steps: first rename; second add equalities);
 * - nm_rem:
 *     nodeIDs to remove
 * - nm_suboff:
 *     function to decide admissible sub-memory offsets,
 *     i.e., sub-memory environment offsets to preserve *)
type 'a node_mapping =
    { nm_map:    (sv * SvSet.t) SvMap.t ; (* mapping n -> elt+set *)
      nm_rem:    SvSet.t ; (* nodes to abstract away *)
      nm_suboff: ('a -> bool) ; (* admissible submem offsets *) }

(** Join kind *)
type join_kind =
  | Jjoin  (* real join, may not terminate *)
  | Jwiden (* widening, enforces termination *)
  | Jdweak (* directed weakening, over approximates only right side *)


(** Signature of the numerical domain, without a bottom element *)
module type DOM_NUM_NB =
  sig
    include INTROSPECT
    type t
    (** Bottom element (might be detected) *)
    val is_bot: t -> bool
    (** Top element *)
    val top: t
    (** Pretty-printing *)
    val t_fpri: sv_namer -> string -> form -> t -> unit
    (** Variables managemement *)
    val sv_add: sv -> t -> t
    val sv_rem: sv -> t -> t
    val vars_srename: 'a node_mapping -> t -> t
    val check_nodes: SvSet.t -> t -> bool
    val nodes_filter: SvSet.t -> t -> t
    (** Comparison, Join and Widening operators *)
    val is_le: t -> t -> (sv -> sv -> bool) -> bool
    val join: t -> t -> t
    val widen: t -> t -> t
    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    val sat: n_cons -> t -> bool
    (** Transfer functions *)
    val assign: sv -> n_expr -> t -> t
    val guard: ?no_apron:bool -> bool -> n_cons -> t -> t
    (** Utilities for the abstract domain *)
    val simplify_n_expr: t -> n_expr -> n_expr
    (** Summarizing dimensions related operations *)
    val expand: sv -> sv -> t -> t
    val compact: sv -> sv -> t -> t
    (** Conjunction *)
    (* Note: seems to be NEVER called in the non-bottom domains *)
    val meet: t -> t -> t
    (** Drop information on an SV *)
    (* Note: seems to be NEVER called in the non-bottom domains *)
    val sv_forget: sv -> t -> t
    (** Export of range information *)
    val bound_variable: sv -> t -> interval
    (** Extract the set of all SVs *)
    val get_svs: t -> SvSet.t
    (** Extract all SVs that are equal to a given SV *)
    val get_eq_class: sv -> t -> SvSet.t
    (* Get the set of all svs that majorate a given SV *)
    val get_upper_svs: sv -> t -> SvSet.t
    (* Get the set of all svs that minorate a given SV *)
    val get_lower_svs: sv -> t -> SvSet.t
  end


(** Signature of the numerical domain, with a bottom element *)
module type DOM_NUM =
  sig
    include DOM_NUM_NB
    (* Bottom element *)
    val bot: t
  end
