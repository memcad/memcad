(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: set_utils.ml
 **       utilities for set domain
 ** Huisong Li, 2014/09/25 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Nd_sig
open Col_sig
open Set_sig

open Ast_utils
open Sv_utils

module Log =
  Logger.Make(struct let section = "s_uts___" and level = Log_level.DEBUG end)


(** Pretty-printing *)
let setv_fpr (fmt: form) (s: sv): unit = F.fprintf fmt "S[%a]" sv_fpr s
let setvset_fpr : form -> SvSet.t -> unit = Nd_utils.gen_svset_fpr ", " setv_fpr
let setvmap_fpr (c: string) = Nd_utils.gen_svmap_fpr c setv_fpr
let rec set_expr_fpr (fmt: form) (e: set_expr): unit =
  match e with
  | S_var i -> F.fprintf fmt "S[%a]" sv_fpr i
  | S_node i -> F.fprintf fmt "N[%a]" sv_fpr i
  | S_uplus (i, j) -> F.fprintf fmt "%a [U+] %a" set_expr_fpr i set_expr_fpr j
  | S_union (i, j) -> F.fprintf fmt "%a [U] %a" set_expr_fpr i set_expr_fpr j
  | S_empty -> F.fprintf fmt "$emp"
let rec set_cons_fpr (fmt: form) (s: set_cons): unit =
  match s with
  | S_mem (i, j) -> F.fprintf fmt "N[%a] $in %a" sv_fpr i set_expr_fpr j
  | S_eq (i, j) -> F.fprintf fmt "%a = %a" set_expr_fpr i set_expr_fpr j
  | S_subset (i, j) -> F.fprintf fmt "%a <= %a" set_expr_fpr i set_expr_fpr j
let set_par_type_fpr (fmt: form) (t: set_par_type): unit =
  let f b x s = if b then StringSet.add x s else s in
  let s = f t.st_mono "Mono" (f t.st_const "Const" StringSet.empty) in
  let s = f t.st_add "Add" (f t.st_head "Head" s) in
  F.fprintf fmt "[%a]" (StringSet.t_fpr ";") s

(** Basic utility function *)

(** [make_equality sv sv'] returns the constraint [sv == sv']*)
let make_equality (setv1: sv) (setv2: sv) : set_cons =
  S_eq (S_var setv1, S_var setv2)

(** [gen_eq sl ol map] returns [map{ol_i |-> S_var sl_i}] *)
let gen_eq (sl: sv list) (ol: sv list) (m: set_expr SvMap.t) =
  assert (List.length sl = List.length ol);
  List.fold_left2
    (fun acc ii io ->
      SvMap.add io (S_var ii) acc
    ) m  sl ol

(* Map translation *)
let rec s_cons_map (f: sv -> sv) (g: sv -> sv) (s: set_cons) =
  match s with
  | S_mem (n, sr) -> S_mem (f n, s_expr_map f g sr)
  | S_eq (sl, sr) -> S_eq (s_expr_map f g sl, s_expr_map f g sr)
  | S_subset (sl, sr) -> S_subset (s_expr_map f g sl, s_expr_map f g sr)
and s_expr_map (f: sv -> sv) (g: sv -> sv) (e: set_expr) =
  match e with
  | S_var si -> S_var (g si)
  | S_node n -> S_node (f n)
  | S_uplus (sl, sr) -> S_uplus (s_expr_map f g sl, s_expr_map f g sr)
  | S_union (sl, sr) -> S_union (s_expr_map f g sl, s_expr_map f g sr)
  | S_empty -> S_empty


(** Transformers over set_expr and set_cons *)
(*  Take a function for setv to set_expr, and substitute everywhere *)
let gen_transformer f =
  let rec aux_expr e =
    match e with
    | S_var i -> f i
    | S_node _ | S_empty -> e
    | S_union (e0, e1) ->
        begin
          match aux_expr e0, aux_expr e1 with
          | S_empty, S_empty -> S_empty
          | S_empty, ee | ee, S_empty -> ee
          | ee0, ee1 -> S_union (ee0, ee1)
        end
    | S_uplus (e0, e1) ->
        begin
          match aux_expr e0, aux_expr e1 with
          | S_empty, S_empty -> S_empty
          | S_empty, ee | ee, S_empty -> ee
          | ee0, ee1 -> S_uplus (ee0, ee1)
        end in
  let aux_cons = function
    | S_mem (i, e0) -> S_mem (i, aux_expr e0)
    | S_eq (e0, e1) -> S_eq (aux_expr e0, aux_expr e1)
    | S_subset (e0, e1) -> S_subset (aux_expr e0, aux_expr e1) in
  aux_expr, aux_cons
let transform_set_expr (f: sv -> set_expr): set_expr -> set_expr =
  fst (gen_transformer f)
let transform_set_cons (f: sv -> set_expr): set_cons -> set_cons =
  snd (gen_transformer f)

(** Fold/iters over variables in expressions *)
let set_ast_folder ~(fsv:(sv -> 'a -> 'a)) ~(fsetv:(sv -> 'a -> 'a)) =
  let rec aux_se (e: set_expr) (a: 'a) =
    match e with
    | S_empty -> a
    | S_var i -> fsetv i a
    | S_node i -> fsv i a
    | S_uplus (e0, e1) | S_union (e0, e1) -> a |> aux_se e0 |> aux_se e1 in
  let rec aux_sc (sc: set_cons) (a: 'a) =
    match sc with
    | S_mem (i, e) -> fsv i a |> aux_se e
    | S_eq (e0, e1) | S_subset (e0, e1) -> a |> aux_se e0 |> aux_se e1 in
  aux_se, aux_sc
let set_expr_fold ?(fsv:(sv -> 'a -> 'a) = (fun _ a -> a))
    ?(fsetv:(sv -> 'a -> 'a) = fun _ a -> a): set_expr -> 'a -> 'a =
  fst (set_ast_folder ~fsv ~fsetv)
let set_cons_fold ?(fsv:(sv -> 'a -> 'a) = (fun _ a -> a))
    ?(fsetv:(sv -> 'a -> 'a) = fun _ a -> a): set_cons -> 'a -> 'a =
  snd (set_ast_folder ~fsv ~fsetv)
let set_expr_iter ?(fsv:(sv -> unit) = (fun _ -> ()))
    ?(fsetv:(sv -> unit) = fun _ -> ()) (e: set_expr): unit =
  set_expr_fold ~fsv:(fun s () -> fsv s) ~fsetv:(fun s () -> fsetv s) e ()
let set_cons_iter ?(fsv:(sv -> unit) = (fun _ -> ()))
    ?(fsetv:(sv -> unit) = fun _ -> ()) (c: set_cons): unit =
  set_cons_fold ~fsv:(fun s () -> fsv s) ~fsetv:(fun s () -> fsetv s) c ()

(** Set of setv that appear *)
let set_expr_setvs e = set_expr_fold ~fsetv:SvSet.add e SvSet.empty
let set_cons_setvs e = set_cons_fold ~fsetv:SvSet.add e SvSet.empty

(** Pruning some SETVs from a list of set constraints *)
(* This function should return an equivalent set of constraints, where
 * some SETVs are removed (it is used for is_le) *)
let set_cons_prune_setvs (torem: SvSet.t) (lc: set_cons list): set_cons list =
  let loc_debug = false in
  (* Phase 0. inspect all the variables used *)
  if loc_debug then
    List.iter (fun c -> Log.info "Prune,Ph-0: %a" set_cons_fpr c) lc;
  (* Phase 1. search for sets equal to empty *)
  let lc =
    let lc, emptys =
      List.fold_left
        (fun (acc_remain, acc_emp) c ->
          if loc_debug then Log.info "Prune,Ph-1: %a" set_cons_fpr c;
          match c with
          | S_eq (S_var i, S_empty) | S_eq (S_empty, S_var i) ->
              if SvSet.mem i torem then
                acc_remain, SvSet.add i acc_emp
              else c :: acc_remain, acc_emp
          | _ -> c :: acc_remain, acc_emp
        ) ([ ], SvSet.empty) lc in
    let aux_setv i =
      try if SvSet.mem i emptys then S_empty else S_var i
      with Not_found -> S_var i in
    List.map (transform_set_cons aux_setv) lc in
  (* Phase 2. make a table of equalities, among SETVs to remove and try to
   * rename each class into one element that is not to remove *)
  let lc =
    let uf_add i uf = if Union_find.mem i uf then uf else Union_find.add i uf in
    let uf = (* equalities turned into a union_find *)
      List.fold_left
        (fun uf c ->
          if loc_debug then Log.info "Prune,Ph-2: %a" set_cons_fpr c;
          match c with
          | S_eq (S_var i, S_var j) ->
              let uf = uf_add i (uf_add j uf) in
              let ii, uf = Union_find.find i uf in
              let jj, uf = Union_find.find j uf in
              Union_find.union ii jj uf
          | _ -> uf
        ) Union_find.empty lc in
    let renamer, _ =
      SvSet.fold
        (fun i ((binding, tobind) as acc) ->
          if SvSet.mem i tobind && Union_find.mem i uf then
            let i, _ = Union_find.find i uf in
            let cli = Union_find.find_class i uf in
            let cli_k = List.filter (fun i -> not (SvSet.mem i torem)) cli in
            match cli_k with
            | [ ] -> (* no representant... no renaming *)
                binding, SvSet.remove i tobind
            | rep :: _ -> (* renaming *)
                let cli_r = List.filter (fun i -> SvSet.mem i torem) cli in
                List.fold_left
                  (fun (binding, tobind) ii ->
                    SvMap.add ii rep binding, SvSet.remove ii tobind
                  ) acc cli_r
          else acc (* no equalities or already done, nothing to do *)
        ) torem (SvMap.empty, torem) in
    (* Resolving all equalities that can be resolved *)
    let aux_setv i =
      try S_var (SvMap.find i renamer) with Not_found -> S_var i in
    let lc = List.map (transform_set_cons aux_setv) lc in
    (* Filtering out constraints of the form Si = Si *)
    let filter = function
      | S_eq (S_var i, S_var j) -> i != j
      | _ -> true in
    List.filter filter lc in
  (* Phase 3. Resolving equalities of the form S_i = ... where S_i should
   * disappear. As of now, this will crash if there are several such
   * equalities for a given S_i. *)
  let lc =
    let lc, map =
      List.fold_left
        (fun (acc_lc, acc_map) c ->
          if loc_debug then Log.info "Prune,Ph-3: %a" set_cons_fpr c;
          match c with
          | S_eq (S_var i, ex) | S_eq (ex, S_var i) ->
              if SvSet.mem i torem then acc_lc, SvMap.add i ex acc_map
              else c :: acc_lc, acc_map
          | _ -> c :: acc_lc, acc_map
        ) ([ ], SvMap.empty) lc in
    let aux_setv i = try SvMap.find i map with Not_found -> S_var i in
    List.map (transform_set_cons aux_setv) lc in
  lc

(* replace instantiated set variables from a set expression *)
let rec replace_expr (sr: set_expr) (inst: set_expr SvMap.t)
    (nmap: sv SvMap.t): set_expr =
  match sr with
  | S_var sid ->
      begin
        try SvMap.find sid inst
        with Not_found ->
          Log.info "%a not instantiated in |%i|\n%a" setv_fpr sid
            (SvMap.cardinal inst) (SvMap.t_fpr "\n" set_expr_fpr) inst;
          raise Non_inst
      end
  | S_node nid ->
      begin
        try  S_node (SvMap.find nid nmap)
        with Not_found ->
          Log.info "N[%a] not instantiated" sv_fpr nid;
          raise Non_inst
      end
  | S_uplus (sl, sr) ->
      S_uplus (replace_expr sl inst nmap, replace_expr sr inst nmap)
  | S_union (sl, sr) ->
      S_union (replace_expr sl inst nmap, replace_expr sr inst nmap)
  | S_empty -> S_empty

(* replace instantiated set variables from set constraints *)
let replace_cons (setctr: set_cons list)
    (inst: set_expr SvMap.t) (nmap: sv SvMap.t)
    : set_cons list (* * set_cons list *) =
  try
    List.fold_left
      (fun acc_rep ele ->
        try
          let r_ele =
            match ele with
            | S_mem (nid, sr) ->
                S_mem (SvMap.find nid nmap, replace_expr sr inst nmap)
            | S_eq (sl, sr) ->
                S_eq (replace_expr sl inst nmap, replace_expr sr inst nmap)
            | S_subset (sl, sr) ->
                S_subset (replace_expr sl inst nmap, replace_expr sr inst nmap)
          in r_ele :: acc_rep
        with Non_inst ->
          Log.warn "NON INST: ...\n";
          acc_rep
      ) [] setctr
  with Not_found -> Log.fatal_exn "setvar/var not instantiated"

(* is_le_instantiate:
 * set variables from equality set constraints on the right side *)
let rec is_le_instantiate
    (setctr: set_cons list)
    (inst: set_expr SvMap.t)
    (nmap: sv SvMap.t)
    : set_expr SvMap.t
    * set_cons list =
  let a_inst i se inst ctr =
    if SvMap.mem i inst then inst, (S_eq (S_var i, se))::ctr
    else
      let setv = set_expr_setvs se in
      if SvSet.for_all (fun i -> SvMap.mem i inst) setv then
        SvMap.add i (replace_expr se inst nmap) inst, ctr
      else inst, (S_eq (S_var i, se)):: ctr in
  let inst, r_setctr =
    List.fold_left
      (fun (inst, ctr) cons ->
        match cons with
        | S_mem _ -> inst, cons :: ctr
        | S_subset _ -> inst, cons :: ctr
        | S_eq (S_var i, S_var j) ->
            if SvMap.mem i inst then a_inst j (S_var i) inst ctr
            else a_inst i (S_var j) inst ctr
        | S_eq (S_var i, sr) -> a_inst i sr inst ctr
        | S_eq (sl, S_var j) -> a_inst j sl inst ctr
        | S_eq (sl, sr) -> inst, cons :: ctr
      ) (inst, [ ]) setctr in
  if List.length r_setctr = List.length setctr then inst, r_setctr
  else
    is_le_instantiate r_setctr inst nmap


(* check if there is non instantiated set variables,
 * if there is non instantiated set variables, compute the minimal
 * set of non-instantiated set variables, that needs to be instantiated
 * to fresh set variables *)
let check_non_inst (setctr: set_cons list) (inst: set_expr SvMap.t)
    : SvSet.t =
  (** [f_add i setv map] returns [map{i ↦ setv ∪ map(i)}]*)
  let f_add i setv map =
    try  SvMap.add i (SvSet.union setv (SvMap.find i map)) map
    with Not_found -> SvMap.add i setv map in
  (* compute the dependence of non instantiated set variables *)
  let a_dep i inst se map =
    if SvMap.mem i inst then map
    else
      let setv = set_expr_setvs se in
      f_add i setv map in
  let dep =
    List.fold_left
      (fun acc con ->
        match con with
        | S_mem _ -> acc
        | S_subset _ -> acc
        | S_eq (S_var i, S_var j) ->
            if SvMap.mem j inst then a_dep i inst (S_var j) acc
            else a_dep j inst (S_var i) acc
        | S_eq (S_var i, sr) -> a_dep i inst sr acc
        | S_eq (sl, S_var j) -> a_dep j inst sl acc
        | S_eq (sl, sr) -> acc
      ) SvMap.empty setctr in
  let sc_inst, de_inst =
    SvMap.fold
      (fun k set (accl, accr) ->
         SvSet.add k accl, SvSet.union set accr)
      dep (SvSet.empty, SvSet.empty) in
  let de_inst = SvSet.diff de_inst sc_inst in
  let de_inst =
    SvSet.diff de_inst
      (SvMap.fold (fun k _ acc -> SvSet.add k acc) inst SvSet.empty) in
  if !Flags.flag_dbg_is_le_shape then
    Log.info "Non-Instantiated set variables: { %a }"
      (SvSet.t_fpr ";") de_inst;
  de_inst

(* split set constraints with fresh set variables *)
let split_cons_fresh (setctr: set_cons list) (setv: SvSet.t)
    : set_cons list * set_cons list =
  List.partition
    (fun ele -> SvSet.inter setv (set_cons_setvs ele) = SvSet.empty) setctr

(* linear uplus or union set variables from set expression
 * seems to gather a pair of sets respectively for SVs and SETVs  that denote
 * disjoint sets in a given expression *)
let rec linear_svars (se: set_expr) (is_disjoin: bool)
    : (SvSet.t * SvSet.t) option =
  let f sl sr =
    match linear_svars sl is_disjoin,
          linear_svars sr is_disjoin with
    | Some (ssl, snl), Some (ssr, snr) ->
        assert (SvSet.inter ssl ssr = SvSet.empty);
        assert (SvSet.inter snl snr = SvSet.empty);
        Some (SvSet.union ssl  ssr, SvSet.union snl snr)
    | _, _ -> None in
  match se with
  | S_var s -> Some (SvSet.singleton s, SvSet.empty)
  | S_node n -> Some (SvSet.empty, SvSet.singleton n)
  | S_uplus (sl, sr) -> if is_disjoin then f sl sr else None
  | S_union (sl, sr) -> if is_disjoin then None else f sl sr
  | S_empty -> Some (SvSet.empty, SvSet.empty)

(* make uplus or union set expression *)
let make_setexp ?(is_disjoin: bool = true) ~(setvs: SvSet.t) ~(svs: SvSet.t) ()
    : set_expr =
  let op =
    if is_disjoin then fun a b -> S_uplus (a, b)
    else fun a b -> S_union (a, b) in
  let f_setv setv se =
    SvSet.fold (fun ele acc -> op (S_var ele) acc) setv se in
  let f_sv sv se =
    SvSet.fold (fun ele acc -> op (S_node ele) acc) sv se in
  if not (SvSet.is_empty setvs) then
    let min = SvSet.min_elt setvs in
    f_sv svs (f_setv (SvSet.remove min setvs) (S_var min))
  else if not (SvSet.is_empty svs) then
    let min = SvSet.min_elt svs in
    f_sv (SvSet.remove min svs) (S_node min)
  else S_empty

(** Check properties of set parameters *)
let set_par_type_is_const (st: set_par_type): bool = st.st_const
let set_par_type_is_add (st: set_par_type): bool = st.st_add || st.st_head

