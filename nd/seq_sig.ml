(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: seq_sig.ml
 **       signatures of the sequence specifiques types/values
 ** Xavier Rival, started 2011/05/27 *)
open Data_structures
open Sv_def

type qid = sv

(** Definition of seq_values to define constraints *)
type seq_expr =
  | Seq_empty                         (** The empty seqence *)
  | Seq_var of qid                    (** Some sequence variable *)
  | Seq_val of sv                     (** Sequence containing only a value *)
  | Seq_Concat of seq_expr list       (** Concatenation of two sequences *)
  | Seq_sort of seq_expr              (** The sorted version of a sequence *)

type seq_expr_mrk =
  | Seq_mrk_empty                         (** The empty seqence *)
  | Seq_mrk_var of qid                    (** Some sequence variable *)
  | Seq_mrk_val of sv                     (** Sequence containing only a value *)
  | Seq_mrk_Concat of seq_expr_mrk list   (** Concatenation of two sequences *)
  | Seq_mrk_sort of seq_expr_mrk          (** The sorted version of a sequence *)
  | Seq_mrk_mrk of qid * qid
  | Seq_mrk_mrk_empty

(** Constraints bewteen sequences exprsssions *)
type seq_cons =
  | Seq_equal of seq_expr * seq_expr  (** The only predicate is equality *)

type seq_cons_mrk =
  | Seq_equal_mrk of seq_expr_mrk * seq_expr_mrk  (** The only predicate is equality *)

(** nature of arguments *)
type seq_par_type =
    { sq_const: bool;  (* "const":  Constant through the predicate *)
      sq_add:   bool;  (* "add": We can concatenate this arguments in seg *)
      sq_head:  bool } (* "head":   the sequence is exactly the data stored *)

(** A cut is a synthetic representation of a concatenation-only sequence
    expression.
    All concatenation of seqv are cut by atom (i.e. singlaton sv) *)
type cut =
  | Val of sv (** a singleton sequence *)
  | Seq_vars of sv list (** a uninterupted concatenation of seqvs *)
type cutted_expr = cut list
