(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: col_utils.mli
 **       utilities for collection domains
 ** Josselin Giet, 2021/10/15 *)
open Data_structures

open Sv_def

open Ast_sig
open Col_sig
open Nd_sig
open Vd_sig
open Set_sig
open Seq_sig
open Vd_sig

(** Utilities for col_kind SvMap.t *)
val col_kind_map_union: col_kinds -> col_kinds -> col_kinds
val col_par_type_to_col_kind: col_par_type -> col_kind
val col_par_type_map_to_col_kinds: col_par_type SvMap.t -> col_kinds

(** Mapping functions *)
(* Empty mapping *)
val set_colv_mapping_empty: colv_mapping
val seq_colv_mapping_empty: colv_mapping
(* Addition of a new node *)
val add_to_mapping: sv -> sv -> colv_mapping -> colv_mapping
(* Pretty-printing *)
val col_par_type_fpr: form -> col_par_type -> unit
val colv_mapping_fpr: form -> colv_mapping -> unit
val colv_map_complete_fpr: form -> colv_map_complete -> unit
val colv_info_fpr: form -> colv_info -> unit
(* Extraction of mappings and the set var on both sides *)
val extract_mappings: (sv * sv) SvMap.t
  -> colv_mapping * colv_mapping * SvSet.t
val colv_mapping_join: colv_mapping -> colv_mapping -> colv_mapping
(* Renaming *)
val colv_info_rename:
    'a node_mapping
  -> colv_map_complete option
    -> col_kind
      -> colv_info SvMap.t
        -> colv_info SvMap.t
(* Splitting colv sets *)
val colv_embedding_comp_project: col_kind -> colv_embedding_comp
  -> SvSet.t SvMap.t
val colv_embedding_split:
    colv_embedding_comp -> SvSet.t SvMap.t * SvSet.t SvMap.t

(** Functions on node injections (for is_le) *)
module CEmb:
    sig
      val empty: colv_embedding
          (* To string, compact version *)
      val ne_fpr: form -> colv_embedding -> unit
      (* To string, long version *)
      val ne_full_fpri: string -> form -> colv_embedding -> unit
      (* Tests membership *)
      val mem: sv -> colv_embedding -> bool
      (* Find an element in the mapping *)
      val find: sv -> colv_embedding -> SvSet.t
      (* Add an element to the mapping *)
      val add: sv -> sv -> col_kind -> colv_embedding -> colv_embedding
      (* Initialization *)
      val init: colv_emb -> colv_embedding
      (* Extraction of siblings information *)
      val siblings: colv_embedding -> (SvSet.t * col_kind) SvMap.t
      (* Forget, temporary code *)
      val direct_image: colv_embedding -> SvSet.t SvMap.t
    end
