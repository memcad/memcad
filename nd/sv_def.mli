(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Thierry Martinez, Pascal Sotin, Antoine Toubhans, Pippijn
 **          Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sv_def.mli
 **       opaque definition and operations over symbolic variables
 **       signatures of the symbolic variables description
 ** 2020/11/15 *)
open Data_structures

(** Symbolic variables *)

(* Type of an SV: what kind of concrete data it represents in the abstract *)
type ntyp =
  | Ntaddr  (* address *)
  | Ntint   (* integer value *)
  | Ntraw   (* raw series of bits, may be e.g., array space *)
  | Ntset   (* set parameter (maybe change type) *)
  | Ntseq   (* sequence parameter *)

(* Abstract type of symbolic variable used throughout the analyzer *)
type sv

(** Modules *)

(* Order, sets, maps, generator over SVs *)
module SvOrd: Data_structures.OrderedType with type t = sv
module SvSet: Data_structures.SET with type elt = sv
module SvMap: Data_structures.MAP with type key = sv
module SvGen: Data_structures.KeygenS
  with type key := sv and module Set := SvSet

(* Order, sets, maps, generator over pairs of SVs *)
module SvPrSet: Data_structures.LSET with type elt = sv * sv
module SvPrMap: Data_structures.MAP with type key = sv * sv
module IntSvPrOrd: Data_structures.OrderedType with type t = int * sv
module IntSvPrSet: Data_structures.LSET with type elt = int * sv

(* Order, sets, maps, generator over sets of SVs *)
module IntSvPrSetSet: Data_structures.PSET with type elt = IntSvPrSet.t
module IntSvPrSetMap: Data_structures.MAP with type key = IntSvPrSet.t

(* A few operations on sets and maps *)
val set_fpr: form -> SvSet.t -> unit
val sv_of_int_set: Data_structures.IntSet.t -> SvSet.t
val sv_of_int_map: 'a Data_structures.IntMap.t -> 'a SvMap.t

(** Refl definition for equality reasoning over types *)
type ('a, 'b) eq = Refl : ('a, 'a) eq
val eq: (sv, int) eq
