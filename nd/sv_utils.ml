(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Thierry Martinez, Pascal Sotin, Antoine Toubhans, Pippijn
 **          Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sv_utils.ml
 **       opaque definition and operations over symbolic variables
 **       signatures of the symbolic variables description
 ** 2020/11/15 *)
open Data_structures
open Lib
open Sv_def

(** Comparison *)
let sv_compare = SvOrd.compare

(** Conversions/Unsafe map *)
external sv_unsafe_of_int: int -> sv = "%identity"
external sv_to_int:        sv -> int = "%identity"
let sv_unsafe_map: (int -> int) -> (sv -> sv) =
  let Refl = Sv_def.eq in
  Fun.id

(** Pretty-printing *)
(* Node types *)
let ntyp_fpr (fmt: form) (nt: ntyp): unit =
  match nt with
  | Ntaddr -> F.fprintf fmt "addr"
  | Ntraw  -> F.fprintf fmt "raw"
  | Ntint  -> F.fprintf fmt "int"
  | Ntset  -> F.fprintf fmt "set"
  | Ntseq  -> F.fprintf fmt "seq"
let ntyp_fprs (fmt: form) (nt: ntyp): unit =
  match nt with
  | Ntaddr -> F.fprintf fmt "a"
  | Ntraw  -> F.fprintf fmt "r"
  | Ntint  -> F.fprintf fmt "i"
  | Ntset  -> F.fprintf fmt "s"
  | Ntseq  -> F.fprintf fmt "q"
(* Over SVs *)
let sv_fpr (fmt: form) (sv: sv) =
  let Refl = Sv_def.eq in
  F.fprintf fmt "%d" sv
(* Over lists of SVs *)
let sv_list_fpr (fmt: form) (l: sv list) =
  let Refl = Sv_def.eq in
  let pp_sep fmt () = F.pp_print_string fmt ";" in
  F.fprintf fmt "[%a]"
    (F.pp_print_list ~pp_sep F.pp_print_int) l
(* Over sets *)
let svprset_fpr (fmt: form) (s: SvPrSet.t): unit =
  F.fprintf fmt "{ %a }" (SvPrSet.t_fpr "; ") s
let svset_fpr = Sv_def.set_fpr

(** Transformations over modules *)
let svset_of_intset = Sv_def.sv_of_int_set
let svmap_of_intmap = Sv_def.sv_of_int_map
let svmap_of_list (l: 'a list): 'a SvMap.t =
  let make_map (l: 'a list): 'a IntMap.t =
    let rec aux i acc l =
      match l with
      | [ ] -> acc
      | x :: y -> aux (i+1) (IntMap.add i x acc) y in
    aux 0 IntMap.empty l in
  svmap_of_intmap (make_map l)
