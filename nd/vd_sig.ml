(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: vd_sig.ml
 **       value domain layers signatures
 ** Xavier Rival, 2021/07/22 *)
open Data_structures
open Sv_def
open Offs

open Ast_sig
open Nd_sig
open Col_sig
open Seq_sig
open Set_sig
open Svenv_sig


(** Localization of an SV in a sub-memory *)
(* Computed localization *)
type sub_localize =
  | Sl_unk     (* cannot be proved to be in the block *)
  | Sl_inblock of sv (* proved to be in the block, but not found *)
  | Sl_found of sv * Offs.t (* SV is at some known offset *)
(* Destination inside a sub-memory *)
type sub_dest =
  | Sd_env of sv * Offs.t (* (s,o): submem SV s and offset o from s *)
  | Sd_int of sv * Offs.t (* (i,o): global SV i and offset o from i *)


(** Right-value, used for write functions in the dom_value layer *)
(* - either an expression *)
type n_rval =
  | Rv_expr of n_expr
  | Rv_addr of sv * Offs.t (* to-update *)


(** Inductive calls for predicates *)
type 'a gen_ind_intp =
  | Ii_const of int
  | Ii_lval of 'a
type 'a gen_ind_pars =
    { ic_ptr:  'a list ;
      ic_int:  'a gen_ind_intp list ; (* integer parameters *)
      ic_set:  colvar list;           (* set parameters*)
      ic_seq:  colvar list;           (* seq parameters*) }
type 'a gen_ind_call =
    { ic_name: string; (* inductive that is called *)
      ic_pars: 'a gen_ind_pars option (* parameters, if supplied *) }


(** Value domain assume/check operations *)
(* Assume commands for the value domain: just array for now *)
type vassume_op =
  | VA_array
  | VA_aseg of sv * Offs.t * svo gen_ind_call
        * sv * Offs.t * svo gen_ind_call
  | VA_aind of sv * Offs.t * svo gen_ind_call
(* Check commands for the value domain *)
type vcheck_op =
  | VC_array
  | VC_ind of sv * Offs.t * string (* l = ind( ) *)
  | VC_aseg of sv * Offs.t * svo gen_ind_call
        * sv * Offs.t * svo gen_ind_call
  | VC_aind of sv * Offs.t * svo gen_ind_call


(** Commands to direct unfolding *)
(* Unfolding direction *)
type unfold_dir =
  | Udir_fwd
  | Udir_bwd


(** Support for the abstraction of collections *)
(* Implementation of collection SVs *)
type col_par_type =
  | Ct_set of set_par_type option
  | Ct_seq of seq_par_type option

type colv_info =
    { max:  sv;
      min:  sv;
      size: sv; (* cardinal for set, length for seq *) }

(** Shared sat functions *)
type vsat =
    { vs_num: (n_cons -> bool) ;
      vs_set: (set_cons -> bool) ;
      vs_seq: (seq_cons -> bool) }


(** Domain to represent values:
 **  An abstract value represents a set of functions from integer
 **  labeled symbolic variables into values (numeric integer or
 **  floating point values, boolean values, sub-memories)
 **  [all of which are not handled yet]
 **  [set symbolic variables should also appear here in the future]
 **)
module type DOM_VALUE =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (** Domain initialization *)
    (* Domain initialization to a set of inductive definitions *)
    val init_inductives: SvGen.t -> StringSet.t -> SvGen.t
    (** Lattice elements *)
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Top element *)
    val top: t
    (* Pretty-printing, takes a namer for SVs *)
    val t_fpri: sv_namer -> string -> form -> t -> unit
    (** Management of symbolic variables *)
    (* For sanity check *)
    val check_nodes: SvSet.t -> t -> bool
    (* SV addition and removal *)
    val sv_add: sv -> t -> t
    val sv_rem: sv -> t -> t
    (* Renaming (e.g., post join) *)
    val symvars_srename:
        (Offs.t * sv) OffMap.t -> (sv * Offs.t) node_mapping -> t -> t
    (* Synchronization of the SV environment *)
    val sve_sync_top_down: svenv_mod -> t -> t
    (* Check the symbolic vars correspond exactly to given set *)
    val symvars_check: SvSet.t -> t -> bool
    (* Removes all symbolic vars that are not in a given set *)
    val symvars_filter: SvSet.t -> t -> t
    (* Merging into a new variable, arguments:
     *  . the stride of the structure being treated
     *  . SV serving as a base address of a block;
     *  . SV serving as a new block abstraction (as a sequence of bits)
     *  . old contents of the block, that is getting merged
     *  . offsets of external pointers into the block to build an environment *)
    val symvars_merge: int -> sv -> sv -> (Bounds.t * Offs.svo * Offs.size) list
      -> Offs.OffSet.t -> t -> t
    (** Comparison and join operators *)
    (* Comparison *)
    val is_le: t -> t -> (sv -> sv -> bool) -> bool
    (* Upper bound: serves as join and widening *)
    val upper_bnd: ?hint:SvPrSet.t -> join_kind -> t -> t -> t
    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    val sat: t -> n_cons -> bool
    (** Condition test *)
    val guard: ?no_apron:bool -> bool -> n_cons -> t -> t
    (** Assignment *)
    val assign: sv -> n_expr -> t -> t
    (* Assignment inside a sub-memory *)
    val write_sub: sub_dest -> int -> n_rval -> t -> t
    (** Utilities for the abstract domain *)
    val simplify_n_expr: t -> n_expr -> n_expr
    (** Array domain specific functions *)
    (* Add an array content SV  *)
    val add_array_node: sv -> int -> int list -> t -> t
    (* Add an array address SV  *)
    val add_array_address: sv -> t -> t
    (* Checks wheter this SV is the address of an array SV *)
    val is_array_address: sv -> t -> bool
    (* Dereference an array cell in experision,
     * this function may cause disjunction *)
    val array_node_deref: sv -> Offs.t -> t -> (t * sv) list
    (* Dereference an array cell in l-value,
     * no disjunction is created since it merges groups *)
    val array_materialize: sv -> Offs.t -> t -> t * sv
    (* Summarzing dimensions related opeartions
     * expand the constraints on one dimension to another  *)
    val expand: sv -> sv -> t -> t
    (* Upper bound of the constraits of two dimensions *)
    val compact: sv -> sv -> t -> t
    (* Conjunction *)
    val meet: t -> t -> t
    (* Forget the information on a dimension *)
    val sv_forget: sv -> t -> t
    (** Export of range information *)
    val bound_variable: sv -> t -> interval
    (** Sub-memory specific functions *)
    (* Checks whether an SV is of sub-memory type *)
    val is_submem_address: sv -> t -> bool
    val is_submem_content: sv -> t -> bool
    (* Read of a value inside a submemory block *)
    val submem_read: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo
    val submem_deref: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo
    (* Localization of an SV in a sub-memory *)
    val submem_localize: sv -> t -> sub_localize
    (* Binding of an offset in a sub-memory *)
    val submem_bind: sv -> sv -> Offs.t -> t -> t * Offs.svo
    (* Regression testing support, inside sub-memories *)
    val check:  vcheck_op  -> t -> bool
    val assume: vassume_op -> t -> t
    (* Unfolding *)
    val unfold: sv -> sv -> unfold_dir -> t -> (sv SvMap.t * t) list
    (* Extract the set of all SVs *)
    val get_svs: t -> SvSet.t
    (* Get all variables that equal to a given SV *)
    val get_eq_class: sv -> t -> SvSet.t
    (* Get the set of all svs that majorate a given SV *)
    val get_upper_svs: sv -> t -> SvSet.t
    (* Get the set of all svs that minorate a given SV *)
    val get_lower_svs: sv -> t -> SvSet.t
  end


(** Signature of the values + collections (sets, sequences) domain:
 **  An abstract value represents a triplet of valuations from:
 **  - symbolic variables to values
 **  - set variables to values
 **  - seq variables to values
 **  The signature incorporates all the DOM_VALUE signature,
 **  and adds set properties as well.
 **  The elements that are specific to the set domain are marked "specific" *)
module type DOM_VALCOL =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (** Domain initialization *)
    (* Domain initialization to a set of inductive definitions *)
    val init_inductives: SvGen.t -> StringSet.t -> SvGen.t
    (** Lattice elements *)
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Top element *)
    val top: t
    (* Pretty-printing, with indentation *)
    val t_fpri: sv_namer -> string -> form -> t -> unit
    (** Management of symbolic variables *)
    (* For sanity check *)
    val check_nodes: svs:SvSet.t -> colvs:col_kinds -> t -> bool
    (** Symbolic variables **)
    (* add symbolic variables, mark used to choose num or set *)
    val sv_add: ?mark:bool -> sv -> t -> t
    val sv_rem: sv -> t -> t
    (** Collection variables **)
    (* add collection variable with the given name *)
    val colv_add: col_kind -> root: bool -> ?kind: set_par_type option
      -> ?name: string option -> sv -> (colv_info option) -> t -> t
    (* removal of a collection variable *)
    val colv_rem: sv -> t -> t
    (* check whether a collection variable is root *)
    val colv_is_root: sv -> t -> bool
    (* collect root set variables *)
    val colv_get_roots: t -> SvSet.t
    (** General managment of symbolic variables *)
    (* Renaming (e.g., post join), mark used to choose only rename seq domain *
     * or rename both, specifically to join *)
    val symvars_srename: (* may change a little bit *)
      ?mark: bool -> (Offs.t * sv) OffMap.t -> (sv * Offs.t) node_mapping
        -> colv_map_complete option -> t -> t
    (* Synchronization of the SV environment *)
    val sve_sync_top_down: svenv_mod -> t -> t (* may change a little bit *)
    (* Check the symbolic vars correspond exactly to given set *)
    val symvars_check: SvSet.t -> t -> bool (* may change a little bit *)
    (* Removes all symbolic vars that are not in a given set *)
    (* may change a little bit *)
    val symvars_filter: SvSet.t -> ?col_vars: SvSet.t ->  t -> t
    (* Merging into a new variable, arguments:
     *  . the stride of the structure being treated
     *  . SV serving as a base address of a block;
     *  . SV serving as a new block abstraction (as a sequence of bits)
     *  . old contents of the block, that is getting merged
     *  . offsets of external pointers into the block to build an environment *)
    val symvars_merge: int -> sv -> sv -> (Bounds.t * Offs.svo * Offs.size) list
      -> Offs.OffSet.t -> t -> t (* may change a little bit *)
    (** Comparison and join operators *)
    (* Comparison *)
    val is_le: t -> t -> (sv -> sv -> bool) -> bool
    (* Upper bound: serves as join and widening *)
    val upper_bnd: ?hint:SvPrSet.t -> join_kind -> t -> t -> t
    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    val sat: t -> n_cons -> bool
    (** Set satisfiability *)
    val set_sat: set_cons -> t -> bool (* specific *)
    (** Seq satisfiability *)
    val seq_sat: seq_cons -> t -> bool (* specific *)
    (** Condition test *)
    val guard: ?no_apron:bool -> bool -> n_cons -> t -> t
    (** Set condition test *)
    val set_guard: set_cons -> t -> t (* specific *)
    (** Seq condition test *)
    val seq_guard: seq_cons -> t -> t (* specific *)
    (** Assignment *)
    val assign: sv -> n_expr -> t -> t
    (* Assignment inside a sub-memory *)
    val write_sub: sub_dest -> int -> n_rval -> t -> t
    (** Utilities for the abstract domain *)
    val simplify_n_expr: t -> n_expr -> n_expr
    (** Array domain specific functions *)
    (* Add an array content SV *)
    val sv_array_add: sv -> int ->  int list -> t -> t
    (* Add an array address SV *)
    val sv_array_address_add: sv -> t -> t
    (* Dhecks wheter this SV is the address of an array SV *)
    val is_array_address: sv -> t -> bool
    (* Dereference an array cell in experision,
     * this function may cause disjunction *)
    val sv_array_deref: sv -> Offs.t -> t -> (t * sv) list
    (* Dereference an array cell in l-value,
     * no disjunction is created since it merges groups *)
    val sv_array_materialize: sv -> Offs.t -> t -> t * sv
    (** Sub-memory specific functions *)
    (* Checks whether an SV is of sub-memory type *)
    val is_submem_address: sv -> t -> bool
    val is_submem_content: sv -> t -> bool
    (* Read of a value inside a submemory block *)
    val submem_read: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo
    val submem_deref: (n_cons -> bool) -> sv -> Offs.t -> int -> t -> Offs.svo
    (* Localization of an SV in a sub-memory *)
    val submem_localize: sv -> t -> sub_localize
    (* Binding of an offset in a sub-memory *)
    val submem_bind: sv -> sv -> Offs.t -> t -> t * Offs.svo
    (* Regression testing support, inside sub-memories *)
    val check:  vcheck_op  -> t -> bool
    val assume: vassume_op -> t -> t
    (* Unfolding *)
    val unfold: sv -> sv -> unfold_dir -> t -> (sv SvMap.t * t) list
    (** Summarzing dimensions related operations *)
    (* Expand the constraints on one dimension to another *)
    val expand: sv -> sv -> t -> t
    (* Upper bound of the constraits of two dimensions *)
    val compact: sv -> sv -> t -> t
    (* Conjunction *)
    val meet: t -> t -> t
    (* Forget the information about an SV *)
    val sv_forget: sv -> t -> t
    (** Export of range information *)
    val sv_bound: sv -> t -> interval
    (** Extract the set of all SVs *)
    val get_svs: t -> SvSet.t
    (* Get all variables that equal to a given SV *)
    val get_eq_class: sv -> t -> SvSet.t
    (* Get the set of all svs that majorate a given SV *)
    val get_upper_svs: sv -> t -> SvSet.t
    (* Get the set of all svs that minorate a given SV *)
    val get_lower_svs: sv -> t -> SvSet.t
  end


(** Signature of maya domain.
 ** In this domain, each variable accounts for a may-empty set of values *)
module type DOM_MAYA =
  sig
    include INTROSPECT
    type t
    (** Pretty-printing *)
    val t_fpri: sv_namer -> string -> form -> t -> unit
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Assert that some variable is not empty *)
    val assert_non_empty: sv -> t -> t
    (** Management of symbolic variables *)
    val sv_rem: sv -> t -> t
    val sv_add: sv -> bool -> int -> int option -> t -> t
    (* Forget the informaiton of a varialbe except its existence *)
    val sv_forget: sv -> t -> t
    (* Merge the informaiton of two variables to one *)
    val compact: sv -> sv -> t -> t
    (* Narrow the type of all set variables from numeric information *)
    val narrow_set_vars: t -> t
    (* Comparision: variable-wise subset *)
    val is_incl: t -> t -> bool
    (* The bound of a variable *)
    val bound_variable: sv -> t -> interval
    (* Reset the size of a scalar variable *)
    val scalar_to_single: sv -> t -> t
    val scalar_to_exist: sv -> t -> t
    (* Reset the size of a variable by an expression *)
    val size_assign: sv -> n_expr -> t -> t
    (* Constrain the size of variables *)
    val size_guard: n_cons -> t -> t
    (* Comparison *)
    val is_le: t -> t -> bool
    (* Upper bound: serves as join and widening *)
    val upper_bnd: join_kind -> t -> t -> t
    (* Guard, strong version  *)
    val guard_s: bool -> n_cons -> t -> t
    (* Guard, weak version  *)
    val guard_w: bool -> n_cons -> t -> t
    (** Checks a constraint is satisfied (strong version vs weak version) *)
    val sat_w: t -> n_cons -> bool
    val sat_s: t -> n_cons -> bool
    (* Update, substitute the set with the value the expression *)
    val update_subs_set: sv -> n_expr -> t -> t
    (* Update, substitute one element in the set with the value of the
     * expression *)
    val update_subs_elt: sv -> n_expr -> t -> t
    (* Weak update, add one elt to a set  *)
    val update_add: sv -> n_expr -> t -> t
    (* Strong update, remove one elt to a set  *)
    val update_rem: sv -> n_expr -> t -> t
    (* Copy the information from a variable to another *)
    val expand: sv -> sv -> t -> t
    (* Rename a variable *)
    val rename_var: sv -> sv -> t -> t
    (* Filter variables *)
    val symvars_filter: SvSet.t -> t -> t
    (* Top element *)
    val top: t
    (* Get all variables that equal to a given SV *)
    val get_eq_class: sv -> t -> SvSet.t
    (* Get namer *)
    val get_namer: Nd_sig.sv_namer -> t -> unit
  end
