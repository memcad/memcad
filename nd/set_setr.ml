(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: set_sig.ml
 **       set domain interface
 ** Xavier Rival, 2017/02/28 *)
open Data_structures
open Lib
open Sv_def

open Nd_sig
open Col_sig
open Set_sig
open Svenv_sig

open Set_utils
open Sv_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "setr___" and level = Log_level.DEBUG end)
let debug_module = false

(** SETr shortcuts *)
module S = SETr
module L = S.SymSing.Logic

(** Helper functions *)
(* Key conversions *)
let setv_to_i (setv: sv): sv =
  let setv = sv_to_int setv in
  if setv < 0 then Log.fatal_exn "negative SETV";
  sv_unsafe_of_int (- 1 - setv)
let sv_to_i (sv: sv): sv =
  let i = sv_to_int sv in
  if i < 0 then Log.fatal_exn "negative SV";
  sv
let i_to_setv (i: sv) = sv_unsafe_map (fun i -> - 1 - i) i
let i_to_sv (i: sv) = i
let i_is_sv (i: sv): bool = sv_to_int i >= 0
(* Translations of expressions and constraints *)
let rec set_expr_2e: set_expr -> sv L.e = function
  | S_empty -> L.Empty
  | S_var setv -> L.Var (setv_to_i setv)
  | S_node sv -> L.Var (sv_to_i sv)
  | S_uplus (se0, se1) -> L.DisjUnion (set_expr_2e se0, set_expr_2e se1)
  | S_union (se0, se1) -> L.Union (set_expr_2e se0, set_expr_2e se1)
let set_cons_2t: set_cons -> sv L.t = function
  | S_eq (se0, se1) -> L.Eq (set_expr_2e se0, set_expr_2e se1)
  | S_mem (sv, se1) -> L.In (sv_to_i sv, set_expr_2e se1)
  | S_subset (se0, se1) -> L.SubEq (set_expr_2e se0, set_expr_2e se1)
(* Pretty-printing *)
let rec le_fpr (fmt: form) (e: sv L.e): unit =
  match e with
  | L.Empty -> F.fprintf fmt "empty"
  | L.Universe -> F.fprintf fmt "all"
  | L.DisjUnion (e0, e1) -> F.fprintf fmt "(%a + %a)" le_fpr e0 le_fpr e1
  | L.Union (e0, e1) -> F.fprintf fmt "(%a U %a)" le_fpr e0 le_fpr e1
  | L.Inter (e0, e1) -> F.fprintf fmt "(%a /\\ %a)" le_fpr e0 le_fpr e1
  | L.Diff (e0, e1) -> F.fprintf fmt "(%a \\ %a)" le_fpr e0 le_fpr e1
  | L.Comp e0 -> F.fprintf fmt "(all \\ %a)" le_fpr e0
  | L.Var i ->
      if sv_to_int i < 0 then F.fprintf fmt "S[%a]" sv_fpr (i_to_setv i)
      else F.fprintf fmt "N[%a]" sv_fpr (i_to_sv i)
  | L.Sing i ->
      if sv_to_int i < 0 then Log.fatal_exn "negative SV"
      else F.fprintf fmt "{ N[%a] }" sv_fpr (i_to_sv i)
let lt_fpri (ind: string) (fmt: form) (c: sv L.t): unit =
  let rec aux_1 fmt = function
    | L.True  -> F.fprintf fmt "true"
    | L.False -> F.fprintf fmt "false"
    | L.Eq (e0, e1) -> F.fprintf fmt "%a = %a" le_fpr e0 le_fpr e1
    | L.SubEq (e0, e1) -> F.fprintf fmt "%a <= %a" le_fpr e0 le_fpr e1
    | L.In (i, e1) ->
        F.fprintf fmt "N[%a] \\in %a" sv_fpr (i_to_sv i) le_fpr e1
    | c -> Log.fatal_exn "Basic term, should not come up" in
  let rec aux_l c =
    match c with
    | L.True | L.False | L.Eq (_,_) | L.SubEq (_,_) | L.In (_,_) -> [ c ]
    | L.And (c0,c1) -> aux_l c0 @ aux_l c1
    | L.Not _ -> Log.fatal_exn "Neg should not occur" in
  match aux_l c with
  | [ ] -> ()
  | [ c ] -> F.fprintf fmt "%s%a\n" ind aux_1 c
  | c :: l ->
      F.fprintf fmt "%s%a\n" ind aux_1 c;
      List.iter (F.fprintf fmt "%s/\\ %a\n" ind aux_1) l

module type PRE_DOMSET =
  sig
    val name: string
    module D:
        (S.Domain with
         type sym = int
         and type cnstr  = int L.t
         and type output = int L.t )
    val ctx: D.ctx
  end

module Make = functor (P: PRE_DOMSET) ->
  (struct
    (** Module configuration *)
    let module_name = Printf.sprintf "setr[%s]" P.name
    let config_fpr fmt (): unit = F.fprintf fmt "%s" module_name
    (** Quick access to SETr *)
    module D = P.D
    let ctx = P.ctx
    (** Type of abstract values *)
    type t = { t_u:     D.t;     (* Underlying SETr abstract value *)
               t_roots: SvSet.t (* Set of roots *) }
    (* Bottom element (empty context) *)
    let bot: t = { t_u     = D.bottom ctx;
                   t_roots = SvSet.empty; }
    let is_bot (x: t): bool = D.is_bottom ctx x.t_u
    (* Top element (empty context) *)
    let top: t = { t_u     = D.top ctx;
                   t_roots = SvSet.empty }
    (* Pretty-printing, with indentation *)
    let t_fpri (_: string SvMap.t) (ind: string) (fmt: form) (x: t): unit =
      let out = D.serialize ctx x.t_u in
      let Refl = Sv_def.eq in
      lt_fpri ind fmt out
    (** Management of symbolic variables *)
    (* Some internal functions *)
    let drop_svs (l: sv list) (x: t): t =
      let Refl = Sv_def.eq in
      { x with t_u = D.forget ctx (List.map sv_to_i l) x.t_u }
    let drop_setvs (l: sv list) (x: t): t =
      let Refl = Sv_def.eq in
      { x with t_u = D.forget ctx (List.map setv_to_i l) x.t_u }
    (* For sanity check *)
    let check_nodes ~(svs: SvSet.t) ~(setvs: SvSet.t) (_: t): bool =
      Log.todo "todo: check_nodes";
      true
    (* Symbolic variables *)
    let sv_add (sv: sv) (x: t): sv * t =
      sv, x
    let sv_rem (sv: sv) (x: t): t =
      drop_svs [ sv ] x
    (* check if a set var root *)
    let setv_is_root (setv: sv) (t: t): bool =
      SvSet.mem setv t.t_roots
    (* collect root set variables *)
    let setv_col_root (t: t): SvSet.t = t.t_roots
    (* Set variables *)
    let setv_add ?(root: bool = false) ?(kind: set_par_type option = None)
        ?(name: string option = None) (setv: sv) (x: t): t =
      { x with
        t_roots = if root then SvSet.add setv x.t_roots else x.t_roots }
    let setv_rem (setv: sv) (x: t): t =
      let x = drop_setvs [ setv ] x in
      { x with t_roots = SvSet.remove setv x.t_roots }
    (** Comparison and join operators *)
    (* Comparison *)
    let is_le (x0: t) (x1: t): bool = D.le ctx x0.t_u x1.t_u
    (* Weak bound: serves as widening *)
    let weak_bnd (x0: t) (x1: t): t =
      { x0 with t_u = D.widening ctx x0.t_u x1.t_u }
    (* Upper bound: serves as join and widening *)
    let join (x0: t) (x1: t): t =
      { x0 with t_u = D.join ctx x0.t_u x1.t_u }
    (** Set satisfiability *)
    let set_sat (c: set_cons) (x: t): bool =
      Log.debug "set_sat %a\n%a" set_cons_fpr c (t_fpri SvMap.empty "  ") x;
      let Refl = Sv_def.eq in
      D.sat ctx x.t_u (set_cons_2t c)
    (** Set condition test *)
    let set_guard ?(nsat = None) (c: set_cons) (x: t): t =
      let Refl = Sv_def.eq in
      { x with t_u = D.constrain ctx (set_cons_2t c) x.t_u }
    (** Forget (if the meaning of the sv changes) *)
    let sv_forget (sv: sv) (x: t): t =
      drop_svs [ sv ] x
    (** Renaming (e.g., post join) *)
    let symvars_srename
        (om: (Offs.t * sv) Offs.OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (svmo: colv_mapping option)
        (x: t)
        : t =
      let optmap fs x =
        match svmo with
        | None -> x
        | Some svm -> fs svm x in
      (* Initial status *)
      if debug_module then Log.debug "begin:\n%a" (t_fpri SvMap.empty "  ") x;
      if om != Offs.OffMap.empty then
        Log.fatal_exn "rename: should get empty OffMap";
      (* Roots should never be modified; fail if they do *)
      let do_setv setv =
        let f m i = try fst (SvMap.find i m.sm_map) with Not_found -> i in
        optmap f setv in
      SvSet.iter
        (fun setv ->
          if do_setv setv != setv then
            Log.fatal_exn "rename should not modify SETV root"
        ) x.t_roots;
      (* Remove SVs and SETVs that need to be *)
      let x =
        let x = drop_svs (SvSet.to_list nm.nm_rem) x in
        optmap (fun svm -> drop_setvs (SvSet.to_list svm.sm_rem)) x in
      (* Prepare map for renaming *)
      let ren =
        let make_l k_to_i =
          SvMap.fold (fun k0 (k1, _) acc -> (k_to_i k0, k_to_i k1) :: acc) in
        let ren = make_l sv_to_i nm.nm_map [ ] in
        let f svm ren = make_l setv_to_i svm.sm_map ren in
        let ren = optmap f ren in
        SETr_Rename.of_assoc_list ren in
      (* Renaming in SETr *)
      let Refl = Sv_def.eq in
      let u = D.rename_symbols ctx ren x.t_u in
      (* Addition of equalities *)
      let u =
        let fold k_to_i k_map acc =
          SvMap.fold
            (fun _ (k0, s) acc ->
              SvSet.fold
                (fun k1 ->
                  D.constrain ctx (L.Eq (L.Var (k_to_i k0), L.Var (k_to_i k1)))
              ) s acc
          ) k_map acc in
        let u = fold sv_to_i nm.nm_map u in
        let u = optmap (fun svm -> fold setv_to_i svm.sm_map) u in
        u in
      { x with t_u = u }
    (* Synchronization of the SV environment*)
    let sve_sync_top_down (sve: svenv_mod) (x: t): t =
      let l = PSet.fold (fun i acc -> i :: acc) sve.svm_mod [ ] in
      let l = PSet.fold (fun i acc -> i :: acc) sve.svm_rem l in
      drop_svs l x
    (* Removes all symbolic vars that are not in a given set *)
    (* may change a little bit *)
    let symvars_filter (skeep: SvSet.t) (setvkeep: SvSet.t) (x: t): t =
      let Refl = Sv_def.eq in
      let svs, setvs =
        List.fold_left
          (fun ((acc_sv, acc_setvs) as acc) i ->
            if i_is_sv i then
              let sv = i_to_sv i in
              if SvSet.mem sv skeep then acc
              else sv :: acc_sv, acc_setvs
            else
              let setv = i_to_setv i in
              if SvSet.mem setv setvkeep then acc
              else acc_sv, setv :: acc_setvs
          ) ([ ], [ ]) (D.symbols ctx x.t_u) in
      drop_svs svs (drop_setvs setvs x)
  end: DOM_SET)
