(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: set_utils.mli
 **       utilities for set domain
 ** Huisong Li, 2014/09/25 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Nd_sig
open Col_sig
open Set_sig

(** Pretty-printing *)
val setv_fpr: form -> sv -> unit
val setvset_fpr: form -> SvSet.t -> unit
val setvmap_fpr: string -> (form -> 'a -> unit) ->  form -> 'a SvMap.t -> unit
val set_expr_fpr: form -> set_expr -> unit
val set_cons_fpr: form -> set_cons -> unit
val set_par_type_fpr: form -> set_par_type -> unit

(** Basic utilities *)
val make_equality: sv -> sv -> set_cons
val gen_eq: sv list -> sv list -> set_expr SvMap.t -> set_expr SvMap.t

(* Map translation *)
val s_expr_map: (sv -> sv) -> (sv -> sv) -> set_expr -> set_expr
val s_cons_map: (sv -> sv) -> (sv -> sv) -> set_cons -> set_cons

(** Transformers over set_expr and set_cons *)
(*  Take a function for setv to set_expr, and substitute everywhere *)
val transform_set_expr: (sv -> set_expr) -> set_expr -> set_expr
val transform_set_cons: (sv -> set_expr) -> set_cons -> set_cons

(** Fold/iters over variables in expressions *)
val set_expr_fold: ?fsv:(sv -> 'a -> 'a) -> ?fsetv:(sv -> 'a -> 'a)
    -> set_expr -> 'a -> 'a
val set_cons_fold: ?fsv:(sv -> 'a -> 'a) -> ?fsetv:(sv -> 'a -> 'a)
    -> set_cons -> 'a -> 'a
val set_expr_iter: ?fsv:(sv -> unit) -> ?fsetv:(sv -> unit) -> set_expr -> unit
val set_cons_iter: ?fsv:(sv -> unit) -> ?fsetv:(sv -> unit) -> set_cons -> unit

(** Set of setv that appear *)
val set_expr_setvs: set_expr -> SvSet.t
val set_cons_setvs: set_cons -> SvSet.t

(** Pruning some SETVs from a list of set constraints *)
(* This function should return an equivalent set of constraints, where
 * some SETVs are removed (it is used for is_le) *)
val set_cons_prune_setvs: SvSet.t -> set_cons list -> set_cons list


(* replace instantiated set variables from a set expression*)
val replace_expr: set_expr -> set_expr SvMap.t -> sv SvMap.t -> set_expr
(* replace instantiated set variables from set constraints *)
val replace_cons: set_cons list -> set_expr SvMap.t -> sv SvMap.t
  -> set_cons list (* * set_cons list *)

(* instantiate set variables from equality set constraints *)
val is_le_instantiate: set_cons list -> set_expr SvMap.t -> sv SvMap.t
  -> set_expr SvMap.t * set_cons list

(* check if there is non instantiated set variables,
 * if there is non instantiated set variables, compute the minimal
 * set of non-instantiated set variables, that needs to instantiated
 * to fresh set variables *)
val check_non_inst: set_cons list -> set_expr SvMap.t -> SvSet.t

val split_cons_fresh: set_cons list -> SvSet.t -> set_cons list * set_cons list

(* liner uplus or union set variables from set expression *)
val linear_svars: set_expr -> bool -> (SvSet.t * SvSet.t) option

(* make uplus or union set expression *)
val make_setexp: ?is_disjoin:bool -> setvs:SvSet.t -> svs:SvSet.t -> unit
  -> set_expr

(** Check properties of set parameters *)
val set_par_type_is_const: set_par_type -> bool
val set_par_type_is_add: set_par_type -> bool

