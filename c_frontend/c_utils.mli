(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: c_utils.mli
 **       utilities for the micro C frontend
 ** Xavier Rival, 2011/07/16 *)
open Data_structures
open C_sig
open Ast_sig

(** Helpers for the parsing *)
(* Creation of variables *)
val make_c_var: string -> c_type -> c_var
(* Creation of type-less typed expressions and l-values *)
val mkve: c_exprk -> c_loc -> c_expr
val mkvl: c_lvalk -> c_lval
(* Transformation of one type into another *)
val unbox_clval_in_cexpr: c_expr -> c_lval
val unbox_cstat_block: c_stat -> c_block
val unbox_cstat_in_declaration: parsed_declaration -> c_stat
val unbox_c_var_in_declaration: parsed_declaration -> c_var
(* managing environments *)
val empty_unit: c_prog
val add_fundef: c_type * string * c_var list * c_stat list -> c_prog -> c_prog
val add_typedef: parsed_declaration -> c_prog -> c_prog

(** Pretty-printing of C abstract syntax trees *)
(* operators *)
val c_uniop_fpr: form -> c_uniop -> unit
val c_binop_fpr: form -> c_binop -> unit
(* types *)
val c_type_fpri:    ?decl: bool -> string -> form -> c_type -> unit
val c_type_fpr:     ?decl: bool -> form -> c_type -> unit
val c_typedef_fpri: string -> form -> string * c_type -> unit
(* variables *)
val c_var_fpr:   form -> c_var -> unit
(* expressions *)
val c_const_fpr: form -> c_const -> unit
val c_exprk_fpr: form -> c_exprk -> unit
val c_expr_fpr:  form -> c_expr -> unit
val c_lvalk_fpr: form -> c_lvalk -> unit
val c_lval_fpr:  form -> c_lval -> unit
(* specific commands (for memcad) *)
val c_memcad_fpr: form -> c_memcad_com -> unit
(* statements and programs *)
val c_stat_fpri:  string -> form -> c_stat -> unit
val c_block_fpri: string -> form -> c_block -> unit
val c_fun_fpri:   string -> form -> c_fun -> unit
val c_prog_fpri:  string -> form -> c_prog -> unit

(** MemCAD string utilities *)
val parse_memcad_comstring: c_memcad_com -> c_memcad_com

(** Other pp support stuffs *)
val c_stat_do_pp_status: c_stat -> bool

(** Types and storage *)
(* Functions to elaborate representation of types, with size and align info *)
val c_type_elaborate:
  (string * StringSet.t, c_type) Hashtbl.t -> c_type -> c_type
(* Functions to read the align and types *)
val c_type_alignof: c_type -> int
val c_type_sizeof: c_type -> int

(** Extraction of program elements *)
val get_function: c_prog -> string -> c_fun

(** Utility functions on expressions *)
(* Negation of conditions *)
val c_expr_neg: c_expr -> c_expr
(* Extraction of a function name out of a call (ptr calls non supported) *)
val c_call_name: c_call -> string

(** Basic utilities for types *)
(* Compatibility test, used for, e.g.:
 *  - verification of assignments, modulo pointer type casts
 *  - verification of index data types
 * (i.e., all pointers are considered a void* ) *)
val c_type_compat: c_type -> c_type -> bool
(* Read name *)
val c_type_read_named: c_type -> c_type
(* Binary typing *)
val c_type_binary: c_binop -> c_type -> c_type -> c_type
(* Unary typing *)
val c_type_unary: c_uniop -> c_type -> c_type
(* Read a struct type and returns a field *)
val c_type_read_struct_field: c_type -> string -> c_type
(* Read a pointer type, and returns underlying *)
val c_type_read_ptr: c_type -> c_type
(* Read a type for array cell read out *)
val c_type_read_array: c_type -> c_type -> c_type

(** Iteration function over types in the program *)
val c_prog_apply_type_op: (c_type -> c_type) -> c_prog -> c_prog

(** Translations between C and abstract domain syntax *)
val tr_c_type: c_type -> typ
val tr_c_var: c_var -> var
val tr_c_const: c_const -> const
val tr_c_field: c_type -> string -> field
val tr_c_exprk: c_exprk -> var expr
val tr_c_expr: c_expr -> var texpr
val tr_c_lvalk: c_lvalk -> var lval
val tr_c_lval: c_lval -> var tlval
val tr_c_exprk_winfo: c_exprk -> var_info expr
val tr_c_expr_winfo: c_expr -> var_info texpr
val tr_c_lvalk_winfo: c_lvalk -> var_info lval
val tr_c_lval_winfo: c_lval -> var_info tlval
val tr_c_colvar: c_memcad_colvar -> colvar
val tr_c_setexpr: c_memcad_setexpr -> var tlval setprop
val tr_c_seqexpr: c_memcad_seqexpr -> var tlval seqprop
