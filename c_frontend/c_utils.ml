(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: c_utils.ml
 **       utilities for the "small-C" frontend
 ** Xavier Rival, 2011/07/15 *)
open C_sig
open Flags
open Data_structures
open Lib
open Ast_sig


(** Error handling *)
module Log =
  Logger.Make(struct let section = "c_uts___" and level = Log_level.DEBUG end)

(** Helpers for the parsing *)
(* Creation of variables *)
let make_c_var (s: string) (typ: c_type): c_var =
  { cv_name     = s;
    cv_uid      = -1;
    cv_type     = typ;
    cv_volatile = false }
(* Creation of type-less typed expressions and l-values *)
let mkve (e: c_exprk) (p: c_loc): c_expr =
  { cek = e ;
    cet = Ctvoid (* at reation time, in parsers *) ;
    cep = p }
let mkvl (l: c_lvalk): c_lval =
  { clk = l ;
    clt = Ctvoid (* at reation time, in parsers *) }
(* Transformation of one type into another *)
let unbox_clval_in_cexpr (e: c_expr): c_lval =
  match e.cek with
  | Celval l -> l
  | _ -> Log.fatal_exn "unbox_clval_in_cexpr"
let unbox_cstat_block (s: c_stat): c_block =
  match s.csk with
  | Csblock b -> b
  | _ -> [ s ]
let unbox_cstat_in_declaration: parsed_declaration -> c_stat = function
  | Pcd_var (v, l) -> { csk = Csdecl v ; csl = l }
  | Pcd_type _ -> Log.fatal_exn "unbox_cstat_in_declaration"
let unbox_c_var_in_declaration: parsed_declaration -> c_var = function
  | Pcd_var (v, _) -> v
  | Pcd_type _ -> Log.fatal_exn "unbox_c_var_in_declaration"
(* managing environments *)
let empty_unit: c_prog =
  { cp_vars  = StringMap.empty ;
    cp_funs  = StringMap.empty ;
    cp_types = StringMap.empty ;
    cp_aggs  = StringMap.empty }
let add_fundef (typ, name, params, body) (u: c_prog): c_prog =
  assert (not (StringMap.mem name u.cp_vars)) ;
  let cf = { cf_type = typ ;
             cf_uid  = -1 ;
             cf_name = name ;
             cf_args = params ;
             cf_body = body } in
  { u with cp_funs = StringMap.add name cf u.cp_funs }
let add_typedef (pcd: parsed_declaration) (u: c_prog): c_prog =
  match pcd with
  | Pcd_type (s, t) -> { u with cp_types = StringMap.add s t u.cp_types }
  | Pcd_var (v, i) ->
      assert (not (StringMap.mem v.cv_name u.cp_vars));
      { u with cp_vars = StringMap.add v.cv_name v u.cp_vars }

(** Pretty-printing of C abstract syntax trees *)
(* operators *)
let c_uniop_fpr (fmt: form): c_uniop -> unit = function
  | Cuneg -> F.fprintf fmt "-"
  | Culnot -> F.fprintf fmt "!"
  | Cubnot -> F.fprintf fmt "~"
let c_binop_fpr (fmt: form): c_binop -> unit = function
  | Cbeq -> F.fprintf fmt "=="
  | Cbne -> F.fprintf fmt "!="
  | Cbge -> F.fprintf fmt ">="
  | Cble -> F.fprintf fmt "<="
  | Cblt -> F.fprintf fmt "<"
  | Cbgt -> F.fprintf fmt ">"
  | Cbadd -> F.fprintf fmt "+"
  | Cbmul -> F.fprintf fmt "*"
  | Cbsub -> F.fprintf fmt "-"
  | Cbmod -> F.fprintf fmt "%c" '%'
  | Cbdiv -> F.fprintf fmt "/"
  | Cbland -> F.fprintf fmt "&&"
  | Cblor  -> F.fprintf fmt "||"
  | Cbband -> F.fprintf fmt "&"
  | Cbbor  -> F.fprintf fmt "|"
  | Cbbslft -> F.fprintf fmt "<<"
  | Cbbsrgh -> F.fprintf fmt ">>"
(* types *)
let rec c_type_fpri ?(decl: bool = false) (ind: string) (fmt: form)
    (t: c_type): unit =
  let f_agg_field_list (fmt: form) (f: c_agg_field list): unit =
    let nind = "  "^ind in
    List.iter
      (fun fld ->
        F.fprintf fmt "%s%a %s;\n"
          nind (c_type_fpri ~decl:decl nind) fld.caf_typ fld.caf_name
      ) f in
  match t with
  | Ctchar -> F.fprintf fmt "char"
  | Ctint -> F.fprintf fmt "int"
  | Ctvoid -> F.fprintf fmt "void"
  | Ctnamed n -> F.fprintf fmt "%s" n.cnt_name
  | Ctstruct (Cad_named n) -> F.fprintf fmt "struct %s" n
  | Ctunion  (Cad_named n) -> F.fprintf fmt "union %s" n
  | Ctstruct (Cad_def s) ->
      let s0 =
        match s.cag_name with
        | None -> "struct"
        | Some n -> Printf.sprintf "struct %s" n in
      if decl then F.fprintf fmt "%s" s0
      else
        F.fprintf fmt "%s {\n%a%s}" s0 f_agg_field_list s.cag_fields ind
  | Ctunion (Cad_def u) ->
      if decl then F.fprintf fmt "union { ... }"
      else
        F.fprintf fmt "union {\n%a%s}" f_agg_field_list u.cag_fields ind
  | Ctptr None -> F.fprintf fmt "(?) *"
  | Ctptr (Some t) -> F.fprintf fmt "%a *" (c_type_fpri ~decl:decl ind) t
  | Ctarray (t, Some n) ->
      F.fprintf fmt "%a[%d]" (c_type_fpri ~decl:decl ind) t n
  | Ctarray (t, None)   -> F.fprintf fmt "%a[]"   (c_type_fpri ~decl:decl ind) t
  | Ctstring n -> F.fprintf fmt "char[%d]" n
let c_type_fpr ?(decl: bool = false): form -> c_type -> unit =
  c_type_fpri ~decl:decl ""
let c_typedef_fpri (ind: string) (fmt: form) ((s, td): string * c_type): unit =
  F.fprintf fmt "%stypedef %a %s;\n" ind (c_type_fpri ind) td s
(* variables *)
let c_var_fpr (fmt: form) (v: c_var): unit =
  F.fprintf fmt "%s<%d,%a,%s>" v.cv_name v.cv_uid
    (c_type_fpr ~decl:true) v.cv_type (string_of_bool v.cv_volatile)
let c_memcad_colvar_fpr (fmt: form) (s: c_memcad_colvar): unit =
  F.fprintf fmt "%s<%d>" s.mc_colvar_name s.mc_colvar_uid
(* expressions *)
let c_const_fpr (fmt: form) (c: c_const): unit =
  match c with
  | Ccint i -> F.fprintf fmt "%d" i
  | Ccchar c -> F.fprintf fmt "\'%c\'" c
  | Ccstring s -> F.fprintf fmt "\"%s\"" s
  | Ccnull -> F.fprintf fmt "null"
let rec c_exprk_fpr (fmt: form) (e: c_exprk): unit =
  let rec aux_basic fmt e =
    match e with
    | Ceconst c -> F.fprintf fmt "%a" c_const_fpr c
    | Celval l -> c_lval_fpr fmt l
    | Ceaddrof l0 -> F.fprintf fmt "&( %a )" c_lval_fpr l0
    | Ceuni (u, e0) -> F.fprintf fmt "%a (%a)" c_uniop_fpr u c_expr_fpr e0
    | _ -> F.fprintf fmt "(%a)" aux_log_or e
  and aux_cast fmt e = aux_basic fmt e
  and aux_mult fmt = function
    | Cebin (Cbmul, e0, e1) ->
        F.fprintf fmt "%a * %a" aux_mult e0.cek aux_cast e1.cek
    | Cebin (Cbmod, e0, e1) ->
        F.fprintf fmt "%a %% %a" aux_mult e0.cek aux_cast e1.cek
    | Cebin (Cbdiv, e0, e1) ->
        F.fprintf fmt "%a / %a" aux_mult e0.cek aux_cast e1.cek
    | e -> aux_cast fmt e
  and aux_add fmt = function
    | Cebin (Cbadd | Cbsub as b, e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_add e0.cek c_binop_fpr b aux_mult e1.cek
    | e -> aux_mult fmt e
  and aux_shift fmt = function
    | Cebin (((Cbbslft | Cbbsrgh) as b), e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_shift e0.cek c_binop_fpr b aux_add e1.cek
    | e -> aux_add fmt e
  and aux_relational fmt = function
    | Cebin (Cbge | Cbgt | Cble | Cblt as b, e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_relational e0.cek c_binop_fpr b aux_shift e1.cek
    | e -> aux_shift fmt e
  and aux_eq fmt = function
    | Cebin (Cbeq | Cbne as b, e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_eq e0.cek c_binop_fpr b aux_relational e1.cek
    | e -> aux_relational fmt e
  and aux_and fmt e = aux_eq fmt e
  and aux_exc_or fmt e = aux_and fmt e
  and aux_inc_or fmt e = aux_exc_or fmt e
  and aux_log_and fmt = function
    | Cebin (Cbland | Cbband as b, e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_log_and e0.cek c_binop_fpr b aux_inc_or e1.cek
    | e -> aux_inc_or fmt e
  and aux_log_or fmt = function
    | Cebin (Cblor | Cbbor as b, e0, e1) ->
        F.fprintf fmt "%a %a %a"
          aux_log_or e0.cek c_binop_fpr b aux_log_and e1.cek
    | e -> aux_log_and fmt e in
  aux_log_or fmt e
and c_expr_fpr (fmt: form) (e: c_expr): unit = c_exprk_fpr fmt e.cek
and c_lvalk_fpr (fmt: form) (l: c_lvalk): unit =
  match l with
  | Clvar v -> c_var_fpr fmt v
  | Clcolvarinfo (colv, ik) ->
      F.fprintf fmt "%a(%a)" Ast_utils.info_kind_fpr ik c_memcad_colvar_fpr colv
  | Clfield (l, f) -> F.fprintf fmt "%a.%s" c_lval_fpr l f
  | Clderef e -> F.fprintf fmt "*( %a )" c_expr_fpr e
  | Clindex (l, e) -> F.fprintf fmt "%a[%a]" c_lval_fpr l c_expr_fpr e
and c_lval_fpr (fmt: form) (l: c_lval): unit = c_lvalk_fpr fmt l.clk
(* specific commands (for memcad) *)
let c_num_right_fpr (fmt: form) (right: c_num_right): unit =
  match right with
  | Cnr_const i -> F.fprintf fmt "%d" i
  | Cnr_lval lv -> c_lval_fpr fmt lv
let c_memcad_iparam_fpr (fmt: form) (ip: c_memcad_iparam): unit =
  match ip with
  | Cmp_const i -> F.fprintf fmt "%d" i
  | Cmp_lval lv -> c_lval_fpr fmt lv
let c_memcad_colvars_fpr (fmt: form) (l: c_memcad_colvar list): unit =
  gen_list_fpr "" c_memcad_colvar_fpr ", " fmt l
let c_memcad_iparams_opt_fpr (fmt: form) (op: c_memcad_iparams option): unit =
  match op with
  | None -> ()
  | Some p ->
      let f = gen_list_fpr "" c_memcad_iparam_fpr ", " in
      F.fprintf fmt ", [%a | %a | %a | %a]" f p.mc_pptr f p.mc_pint
        c_memcad_colvars_fpr p.mc_pset c_memcad_colvars_fpr p.mc_pseq
let c_memcad_setexprs_fpr (fmt: form) (ses: c_memcad_setexpr list): unit =
  let c_memcad_setexpr_fpr (fmt: form) (se: c_memcad_setexpr): unit =
    match se with
    | Cmp_subset (i, j) ->
        F.fprintf fmt "%a $sub$ %a" c_memcad_colvar_fpr i c_memcad_colvar_fpr j
    | Cmp_mem (clv, j) ->
        F.fprintf fmt "%a $in %a" c_lval_fpr clv c_memcad_colvar_fpr j
    | Cmp_empty i ->
        F.fprintf fmt "%a = $empty" c_memcad_colvar_fpr i
    | Cmp_euplus (i, clv, j) ->
        F.fprintf fmt "%a = %a $uplus %a" c_memcad_colvar_fpr i
          c_lval_fpr clv c_memcad_colvar_fpr j in
  gen_list_fpr "" c_memcad_setexpr_fpr ", " fmt ses
let c_memcad_seqexprs_fpr (fmt: form) (ses: c_memcad_seqexpr list): unit =
  let rec c_memcad_seqexpr_basis_fpr fmt (se: c_memcad_seqexpr_basis): unit =
    match se with
    | Cmp_seq_empty -> F.fprintf fmt "[]"
    | Cmp_seq_val v -> F.fprintf fmt "[%a]" c_lval_fpr v
    | Cmp_seq_var v -> c_memcad_colvar_fpr fmt v
    | Cmp_seq_sort s -> F.fprintf fmt "%a" c_memcad_seqexpr_basis_fpr s
    | Cmp_seq_concat (s1, s2) ->
        F.fprintf fmt "%a.%a"
          c_memcad_seqexpr_basis_fpr s1 c_memcad_seqexpr_basis_fpr s2 in
  let c_memcad_seqexpr_fpr fmt (se: c_memcad_seqexpr): unit =
    match se with
    | Cmp_seq_equal (s1, s2) ->
        F.fprintf fmt "%a = %a"
          c_memcad_seqexpr_basis_fpr s1 c_memcad_seqexpr_basis_fpr s2 in
  gen_list_fpr "" c_memcad_seqexpr_fpr ", " fmt ses
let c_memcad_numexpr_fpr (fmt: form)
    ((op, lv, i): c_binop * c_lval * c_num_right): unit =
  F.fprintf fmt "%a %a %a" c_binop_fpr op c_lval_fpr lv c_num_right_fpr i
let c_memcad_numexprs_fpr (fmt: form) (exprs: c_num_expr list): unit =
  gen_list_fpr "" c_memcad_numexpr_fpr ", " fmt exprs
let c_memcad_fpr (fmt: form) (mc: c_memcad_com): unit =
  match mc with
  | Mc_add_inductive (lv, None, _) ->
      F.fprintf fmt "add_inductive( %a )" c_lval_fpr lv
  | Mc_add_inductive (lv, Some iname, op) ->
      F.fprintf fmt "add_inductive( %a, %s%a )" c_lval_fpr lv iname
        c_memcad_iparams_opt_fpr op
  | Mc_add_segment (lv, iname, op, lve, inamee, ope) ->
      F.fprintf fmt "add_segment( %a, %s%a *= %a, %s%a )"
        c_lval_fpr lv iname c_memcad_iparams_opt_fpr op
        c_lval_fpr lve inamee c_memcad_iparams_opt_fpr ope
  | Mc_assume num_exprs ->
      F.fprintf fmt "assume( %a )" c_memcad_numexprs_fpr num_exprs
  | Mc_assert num_exprs ->
      F.fprintf fmt "assert( %a )" c_memcad_numexprs_fpr num_exprs
  | Mc_check_segment (lv, iname, op, lve, inamee, ope) ->
      F.fprintf fmt "check_segment( %a, %s%a *= %a, %s%a )"
        c_lval_fpr lv iname c_memcad_iparams_opt_fpr op
        c_lval_fpr lve inamee c_memcad_iparams_opt_fpr ope
  | Mc_add_setexprs ses ->
      F.fprintf fmt "add_setexprs( %a )" c_memcad_setexprs_fpr ses
  | Mc_add_seqexprs ses ->
      F.fprintf fmt "add_seqexprs( %a )" c_memcad_seqexprs_fpr ses
  | Mc_decl_setvars ss ->
      F.fprintf fmt "decl_setvars( %a )" c_memcad_colvars_fpr ss
  | Mc_decl_seqvars ss ->
      F.fprintf fmt "decl_seqvars( %a )" c_memcad_colvars_fpr ss
  | Mc_check_setexprs ses ->
      F.fprintf fmt "check_setexprs( %a )" c_memcad_setexprs_fpr ses
  | Mc_check_seqexprs ses ->
      F.fprintf fmt "check_seqexprs( %a )" c_memcad_seqexprs_fpr ses
  | Mc_check_inductive (lv, None, _) ->
      F.fprintf fmt "check_inductive( %a )" c_lval_fpr lv
  | Mc_check_inductive (lv, Some iname, op) ->
      F.fprintf fmt "check_inductive( %a, %s%a )" c_lval_fpr lv iname
        c_memcad_iparams_opt_fpr op
  | Mc_check_bottomness b -> F.fprintf fmt "check_bottomness( %b )" b
  | Mc_unfold lv -> F.fprintf fmt "unfold( %a )" c_lval_fpr lv
  | Mc_unfold_bseg lv -> F.fprintf fmt "unfold_bseg( %a )" c_lval_fpr lv
  | Mc_unroll i -> F.fprintf fmt "unroll( %d )" i
  | Mc_merge -> F.fprintf fmt "merge"
  | Mc_sel_merge lv ->
      F.fprintf fmt "sel_merge( %a )" (gen_list_fpr "" c_var_fpr ", ") lv
  | Mc_output_ext (Out_dot (vars, opts)) ->
      let str_list_fpr = gen_list_fpr "" string_fpr ", " in
      F.fprintf fmt "output_dot( [ %a ], [ %a ] )"
        str_list_fpr vars str_list_fpr opts
  | Mc_output_stdout -> F.fprintf fmt "output_stdout( )"
  | Mc_force_live lv ->
      F.fprintf fmt "force_live( %a )" (gen_list_fpr "" c_var_fpr ", ") lv
  | Mc_kill_flow -> F.fprintf fmt "kill_flow"
  | Mc_array_check -> F.fprintf fmt "array_check"
  | Mc_array_assume -> F.fprintf fmt "array_assume"
  | Mc_reduce_localize lv -> F.fprintf fmt "reduce_localize( %a )" c_lval_fpr lv
  | Mc_reduce_eager -> F.fprintf fmt "reduce_eager"
  | Mc_mark_prio lv -> F.fprintf fmt "mark_prio( %a )" c_lval_fpr lv
  | Mc_comstring s -> F.fprintf fmt  "%s" s
(* statements and programs *)
let c_decl_fpri (ind: string) (fmt: form) (v: c_var): unit =
  F.fprintf fmt "%s%a %a" ind (c_type_fpri ~decl:true ind) v.cv_type c_var_fpr v
let c_call_fpr (fmt: form) (c: c_call): unit =
  F.fprintf fmt "%a( %a )" c_expr_fpr c.cc_fun
    (gen_list_fpr "" c_expr_fpr ", ") c.cc_args
let rec c_stat_fpri (ind: string) (fmt: form) (s: c_stat): unit =
  match s.csk with
  | Csdecl d ->
      F.fprintf fmt "%s%a;\n" ind (c_decl_fpri "") d
  | Csassign (lv, ex) ->
      F.fprintf fmt "%s%a = %a;\n" ind c_lval_fpr lv c_expr_fpr ex
  | Cspcall c ->
      F.fprintf fmt "%s%a;\n" ind c_call_fpr c
  | Csfcall (lv, c) ->
      F.fprintf fmt "%s%a = %a;\n" ind c_lval_fpr lv c_call_fpr c
  | Csif (c, b0, b1) ->
      let nind = "  " ^ ind in
      if b1 = [] then
        F.fprintf fmt "%sif( %a ){\n%a%s}\n"
          ind c_expr_fpr c (c_block_fpri nind) b0 ind
      else
        F.fprintf fmt "%sif( %a ){\n%a%s} else {\n%a%s}\n" ind c_expr_fpr c
          (c_block_fpri nind) b0 ind (c_block_fpri nind) b1 ind
  | Csblock b ->
      let nind = "  " ^ ind in
      F.fprintf fmt "%s{\n%a%s}\n" ind (c_block_fpri nind) b ind
  | Cswhile (e, b, None) ->
      let nind = "  " ^ ind in
      F.fprintf fmt "%swhile( %a ){\n%a%s}\n" ind c_expr_fpr e
        (c_block_fpri nind) b ind
  | Cswhile (e, b, Some u) ->
      let nind = "  " ^ ind in
      F.fprintf fmt "%s_memcad( \"unroll( %d )\" );\n%swhile( %a ){\n%a%s}\n"
        ind u ind c_expr_fpr e (c_block_fpri nind) b ind
  | Csreturn None ->
      F.fprintf fmt "%sreturn;\n" ind
  | Csreturn (Some e) ->
      F.fprintf fmt "%sreturn %a;\n" ind c_expr_fpr e
  | Csbreak ->
      F.fprintf fmt "%sbreak;\n" ind
  | Cscontinue ->
      F.fprintf fmt "%scontinue;\n" ind
  | Csexit ->
      F.fprintf fmt "exit(_);\n"
  | Cs_memcad c ->
      F.fprintf fmt "%s_memcad( \"%a\" );\n" ind c_memcad_fpr c
  | Csassert e ->
      F.fprintf fmt "%sassert( %a );\n" ind c_expr_fpr e
  | Csalloc (l, e) ->
      F.fprintf fmt "%s%a = malloc( %a );\n" ind c_lval_fpr l c_expr_fpr e
  | Csfree l ->
      F.fprintf fmt "%sfree( %a );\n" ind c_lval_fpr l
and c_block_fpri (ind: string) (fmt: form) (l: c_block): unit =
  match l with
  | [ ] -> ()
  | s0 :: l1 -> F.fprintf fmt "%a%a" (c_stat_fpri ind) s0 (c_block_fpri ind) l1
let c_fun_fpri (ind: string) (fmt: form) (cf: c_fun): unit =
  let funargs_fpr (fmt: form) (l: c_var list): unit =
    gen_list_fpr "" (c_decl_fpri "") ", " fmt l in
  let nind = F.asprintf "  %s" ind in
  F.fprintf fmt "%s%a %s( %a ){\n%a%s}\n"
    ind (c_type_fpri ind) cf.cf_type cf.cf_name funargs_fpr cf.cf_args
    (c_block_fpri nind) cf.cf_body ind
let c_prog_fpri (ind: string) (fmt: form) (cp: c_prog): unit =
  StringMap.iter (fun s td -> c_typedef_fpri ind fmt (s, td)) cp.cp_types;
  StringMap.iter (fun _ -> c_decl_fpri ind fmt) cp.cp_vars;
  StringMap.iter (fun _ -> c_fun_fpri ind fmt) cp.cp_funs

(** Other pp support stuffs *)
let c_stat_do_pp_status (s: c_stat): bool =
  match s.csk with
  | Csdecl _ -> !Flags.flag_status_decl
  | Csblock _ -> !Flags.flag_status_block
  | _ -> true

(** Extraction of program elements *)
let get_function (p: c_prog) (s: string): c_fun =
  try StringMap.find s p.cp_funs
  with Not_found -> Log.fatal_exn "function %s not found" s

(** Types and storage *)

type c_type_elaborate_cache = (string * StringSet.t, c_type) Hashtbl.t

let cache_aggregate (cache: c_type_elaborate_cache) (ag: c_aggregate)
  (w: StringSet.t) (compute: unit -> c_type) : c_type =
  match ag.cag_name with
  | None -> compute ()
  | Some name ->
      let cache_key = (name, w) in
      try
        Hashtbl.find cache cache_key
      with Not_found ->
        let result = compute () in
        Hashtbl.add cache cache_key result;
        result

(* Functions to elaborate representation of types, with size and align info *)
let rec c_type_elaborate (cache: c_type_elaborate_cache) (w: StringSet.t)
  (t: c_type): c_type =
  match t with
  | Ctvoid | Ctint | Ctchar | Ctstring _ -> t
  | Ctnamed cn ->
      if StringSet.mem cn.cnt_name w then t
      else
        Ctnamed { cn with cnt_type = c_type_elaborate cache w cn.cnt_type }
  | Ctstruct (Cad_named _)
  | Ctunion  (Cad_named _) -> t
  | Ctstruct (Cad_def st) ->
      cache_aggregate cache st w (fun () ->
        let l =
          List.map
            (fun fld -> { fld with
                          caf_typ = c_type_elaborate cache w fld.caf_typ }
            ) st.cag_fields in
        let s, a, l =
          List.fold_left
            (fun (s, a, l) fld ->
              let align = c_type_alignof fld.caf_typ
              and size  = c_type_sizeof fld.caf_typ in
              let pad =
                let delta = s mod align in
                if delta = 0 then 0
                else align - delta in
              let off = s + pad in
              off + size, max a align, { fld with
                                         caf_off  = off;
                                         caf_size = size; } :: l
            ) (0, -1, []) l in
        if false && (s <= 0 || a <= 0) then
          Log.fatal_exn
            "c_type_elaborate: struct %a (%i) w/ s(%i) or a(%i) <= 0"
            (Option.t_fpr Format.pp_print_string) st.cag_name (List.length l) s a
        else Ctstruct (Cad_def { cag_name   = st.cag_name;
                                 cag_align  = a;
                                 cag_size   = s;
                                 cag_fields = List.rev l }))
  | Ctunion (Cad_def u) ->
      cache_aggregate cache u w (fun () ->
        let l =
          List.map
            (fun fld ->
              let utyp = c_type_elaborate cache w fld.caf_typ in
              let size = c_type_sizeof utyp in
              { fld with
                caf_off  = 0;
                caf_size = size;
                caf_typ  = utyp }
            ) u.cag_fields in
        let s, a =
          List.fold_left
            (fun (accs, acca) fld ->
              let align = c_type_alignof fld.caf_typ in
              max accs fld.caf_size, max acca align
            ) (-1, -1) l in
        if s <= 0 || a <= 0 then Log.fatal_exn "c_type_elaborate: union"
        else Ctunion (Cad_def { cag_name   = u.cag_name ;
                                cag_align  = a ;
                                cag_size   = s ;
                                cag_fields = l }))
  | Ctptr tu -> Ctptr (Option.map (c_type_elaborate cache w) tu)
  | Ctarray (tu, n) -> Ctarray (c_type_elaborate cache w tu, n)
(* Functions to read the align and types *)
and c_type_alignof: c_type -> int = function
  | Ctvoid -> 1
  | Ctint -> 4
  | Ctchar -> 1
  | Ctstring _ -> 1
  | Ctnamed n -> c_type_alignof n.cnt_type
  | Ctstruct (Cad_named n)
  | Ctunion  (Cad_named n) ->
      Log.fatal_exn "align: aggregate %s w/o definition" n
  | Ctstruct (Cad_def cd)
  | Ctunion  (Cad_def cd)  -> let a = cd.cag_align in assert (a > 0); a
  | Ctptr _ -> Flags.abi_ptr_size
  | Ctarray (t, _) -> c_type_alignof t
and c_type_sizeof: c_type -> int = function
  | Ctvoid -> 1
  | Ctint -> 4
  | Ctchar -> 1
  | Ctstring n -> n * c_type_sizeof Ctchar
  | Ctnamed n -> c_type_sizeof n.cnt_type
  | Ctstruct (Cad_named n)
  | Ctunion  (Cad_named n) ->
      Log.fatal_exn "sizeof: aggregate %s w/o definition" n
  | Ctstruct (Cad_def cd)
  | Ctunion  (Cad_def cd) ->
      let a = cd.cag_size in assert (a > 0); a
  | Ctptr _ -> Flags.abi_ptr_size
  | Ctarray (t, Some n) -> n * (c_type_sizeof t)
  | Ctarray (t, None)   -> Log.fatal_exn "c_type_sizeof: incomplete array"
(* Elaboration function without extra argument *)
let c_type_elaborate (cache: c_type_elaborate_cache) (t: c_type): c_type =
  c_type_elaborate cache StringSet.empty t

(** Utility functions on expressions *)
(* Negation of conditions *)
let rec c_expr_neg (e: c_expr): c_expr =
  match e.cek with
  | Cebin (Cbge, e0, e1) -> { e with cek = Cebin (Cblt, e0, e1) }
  | Cebin (Cbgt, e0, e1) -> { e with cek = Cebin (Cble, e0, e1) }
  | Cebin (Cble, e0, e1) -> { e with cek = Cebin (Cbgt, e0, e1) }
  | Cebin (Cblt, e0, e1) -> { e with cek = Cebin (Cbge, e0, e1) }
  | Cebin (Cbeq, e0, e1) -> { e with cek = Cebin (Cbne, e0, e1) }
  | Cebin (Cbne, e0, e1) -> { e with cek = Cebin (Cbeq, e0, e1) }
  | Ceuni (Culnot, e0) -> { e with cek = e0.cek } (* double negation *)
  | Ceconst (Ccint i) ->
      if i = 0 then
        (* negation of false, we make this a one, represents true value *)
        { e with cek = Ceconst (Ccint 1) }
      else
        (* negation of true, zero represents false value *)
        { e with cek = Ceconst (Ccint 0) }
  | Cebin ((Cbband | Cbbor | Cblor) as op, e0, e1) ->
      let neg_op =
        match op with
        | Cbband -> Cbbor
        | Cbbor  -> Cbband
        | Cblor  -> Cbland
        | _ -> Log.fatal_exn "neg-op: %a" c_binop_fpr op in
      let ne0 = c_expr_neg e0
      and ne1 = c_expr_neg e1 in
      { e with cek = Cebin (neg_op, ne0, ne1) }
  (* according to C: not (e0 /\ e1)  is (not e0 \/ (e0 /\ not e1)) *)
  | Cebin (Cbland, e0, e1) ->
      let ne0 = c_expr_neg e0
      and ne1 = c_expr_neg e1 in
      let e0_ne1 = { e1 with cek = Cebin (Cbland, e0, ne1) } in
      { e with cek = Cebin (Cblor, ne0, e0_ne1 ) }
  | Celval _ -> (* condition l!=0 ; negation l==0 *)
      { e with cek = Cebin (Cbeq, e, { e with cek = Ceconst (Ccint 0) }) }
  | _ -> Log.todo_exn "negation of expression: %a" c_expr_fpr e
(* Extraction of a function name out of a call (ptr calls non supported) *)
let c_call_name (cc: c_call): string =
  match cc.cc_fun.cek with
  | Celval { clk = Clvar v ; clt = _ } -> v.cv_name
  | _ -> Log.fatal_exn "non trivial called function expression, unsupported"

(** Basic utilities for types *)
(* Compatibility test, used for, e.g.:
 *  - verification of assignments, modulo pointer type casts
 *  - verification of index data types
 * (i.e., all pointers are considered a void* ) *)
let rec c_type_compat (t0: c_type) (t1: c_type): bool =
  let non_equal_compat = function
    | Ctptr (Some t1), Ctarray (t2, _) -> c_type_compat t1 t2
    | Ctptr (Some Ctchar), Ctstring _ -> true
    | Ctptr _, Ctptr _ -> true (* quite permissive *)
    | Ctptr _, Ctint -> true (* problem if integer is negative *)
    | Ctint, Ctptr _ -> true (* the int may overflow *)
    | Ctint, Ctarray (_, _) -> true (* the int may overflow *)
    | Ctnamed n0, Ctnamed n1 ->
        (String.compare n0.cnt_name n1.cnt_name = 0
       || c_type_compat n0.cnt_type n1.cnt_type)
    | Ctnamed n0, _ -> c_type_compat n0.cnt_type t1
    | _, Ctnamed n1 -> c_type_compat t0 n1.cnt_type
    (* valid C: int i = 'a'; char a = 123; *)
    | (Ctint | Ctchar), (Ctint | Ctchar) -> true
    (* yet unmanaged cases *)
    | Ctint, _
    | Ctchar, _
    | Ctvoid, _
    | Ctptr _, _
    | Ctstruct _, _
    | Ctunion _, _
    | Ctarray _, _
    | Ctstring _, _ -> false in
  t0 = t1 || non_equal_compat (t0, t1)
(* Read name *)
let rec c_type_read_named (t: c_type): c_type =
  match t with
  | Ctnamed ct -> c_type_read_named ct.cnt_type
  | _ -> t
(* Binary typing *)
let c_type_binary (b: c_binop) (t0: c_type) (t1: c_type): c_type =
  match b, c_type_read_named t0, c_type_read_named t1 with
  | (Cbadd | Cbsub | Cbmul | Cbmod | Cbdiv | Cbband | Cbbor),
    (Ctint | Ctchar), Ctint -> Ctint
  | (Cbland | Cblor), Ctint, Ctint -> Ctint
  | (Cbbslft | Cbbsrgh), t, _ -> t
  | (Cbeq | Cbge | Cbgt | Cble | Cblt | Cbne),
      (Ctptr _ | Ctint), (Ctptr _ | Ctint) -> Ctint
  | (Cbeq | Cbge | Cbgt | Cble | Cblt | Cbne | Cbband | Cbbor),
      (Ctint | Ctchar), (Ctint | Ctchar) -> Ctint
  | (Cbadd | Cbsub | Cbmul), Ctptr t00, Ctint -> Ctptr t00
  | (Cbadd | Cbsub | Cbmul), Ctint, Ctptr t00 -> Ctptr t00
  | (Cbadd | Cbsub | Cbmul), Ctptr t00, Ctptr t01 when t00 = t01 -> Ctptr t00
  | Cbadd, (Ctarray (t00, Some _size)), Ctint -> Ctptr (Some t00)
  | _, nt0, nt1 ->
      Log.fatal_exn "binary: %a, %a, %a" c_binop_fpr b
        (c_type_fpr ~decl:false) nt0 (c_type_fpr ~decl:false) nt1
(* Unary typing *)
let c_type_unary (u: c_uniop) (t0: c_type): c_type =
  match u, c_type_read_named t0 with
  | (Cuneg | Culnot | Cubnot), Ctint -> Ctint
  | (Cuneg | Culnot | Cubnot), Ctchar -> Ctchar
  | (Culnot | Cubnot), Ctptr _ -> Ctint
  | _, _ ->
      Log.fatal_exn "unary: %a, %a" c_uniop_fpr u (c_type_fpr ~decl:false) t0
(* Read a struct type and returns a field *)
let rec c_type_read_struct_field (t: c_type) (f: string): c_type =
  (* auxilliary funcitons for lists of fields (unions, structures) *)
  let extract_field (ctx: string) (l: c_agg_field list): c_type =
    let candidates = (* candidates with that field name *)
      List.filter (fun fld -> String.compare fld.caf_name f = 0) l in
    match candidates with
    | [ ] -> Log.fatal_exn "%s field %S not found" ctx f
    | [ f ] -> f.caf_typ
    | _ -> Log.fatal_exn "several entries for %s field %S" ctx f in
  match t with
  | Ctstruct (Cad_def st) -> extract_field "struct" st.cag_fields
  | Ctunion  (Cad_def su) -> extract_field "union" su.cag_fields
  | Ctnamed nt -> c_type_read_struct_field nt.cnt_type f
  | _ -> Log.fatal_exn "c_type_read_struct_field: f: '%s' not a struct: %a"
           f (c_type_fpr ~decl:false) t
(* Read a pointer type, and returns underlying *)
let rec c_type_read_ptr (t: c_type): c_type =
  match t with
  | Ctptr (Some t) -> t
  | Ctarray (t, _size) -> t
  | Ctnamed nt -> c_type_read_ptr nt.cnt_type
  | _ -> Log.fatal_exn "not a pointer type, %a" (c_type_fpr ~decl:false) t
(* Read a type for array cell read out *)
let c_type_read_array (t_array: c_type) (t_index: c_type): c_type =
  match t_array with
  | Ctarray (t,_) ->
      if c_type_compat t_index Ctint then t
      else
        Log.fatal_exn "invalid type of index, %a"
          (c_type_fpr ~decl:false) t_index
  | Ctptr _ ->
      Log.fatal_exn "pointers as arrays are not handled, %a"
        (c_type_fpr ~decl:false) t_array
  | _ ->
      Log.fatal_exn "not an array type, %a" (c_type_fpr ~decl:false) t_array

(** MemCAD string utilities *)
let parse_memcad_comstring: c_memcad_com -> c_memcad_com = function
  | Mc_comstring s ->
      begin
        try
          read_from_string "MemCAD command string"
            Mc_parser.memcad_command Mc_lexer.token s
        with exn ->
          Log.fatal "Error while parsing command:\n%s" s;
          raise exn
      end
  | mc -> mc

(** Iteration function over types in the program *)
let c_prog_apply_type_op (f: c_type -> c_type) (p: c_prog): c_prog =
  let do_c_type = f in
  let do_c_agg_field (f: c_agg_field): c_agg_field =
    { f with caf_typ = do_c_type f.caf_typ } in
  let do_c_aggregate (a: c_aggregate): c_aggregate =
    { a with cag_fields = List.map do_c_agg_field a.cag_fields } in
  let do_c_var (v: c_var): c_var = { v with cv_type = do_c_type v.cv_type } in
  let rec do_c_exprk (e: c_exprk): c_exprk =
    match e with
    | Ceconst _ -> e
    | Celval l -> Celval (do_c_lval l)
    | Ceaddrof l -> Ceaddrof (do_c_lval l)
    | Ceuni (o0, e1) -> Ceuni (o0, do_c_expr e1)
    | Cebin (o0, e1, e2) -> Cebin (o0, do_c_expr e1, do_c_expr e2)
  and do_c_expr (e: c_expr): c_expr = { cek = do_c_exprk e.cek ;
                                        cep = e.cep ;
                                        cet = do_c_type e.cet }
  and do_c_lvalk (l: c_lvalk): c_lvalk =
    match l with
    | Clvar v -> Clvar (do_c_var v)
    | Clcolvarinfo (colv, ik) as l -> l
    | Clfield (l0, s) -> Clfield (do_c_lval l0, s)
    | Clindex (l0, e) -> Clindex (do_c_lval l0, do_c_expr e)
    | Clderef e -> Clderef (do_c_expr e)
  and do_c_lval (l: c_lval): c_lval = { clk = do_c_lvalk l.clk ;
                                        clt = do_c_type l.clt } in
  let do_c_num_right (nr: c_num_right): c_num_right =
    match (nr : c_num_right) with
    | Cnr_const _ as c -> c
    | Cnr_lval lv -> Cnr_lval (do_c_lval lv) in
  let do_c_memcad_iparam (ip: c_memcad_iparam): c_memcad_iparam =
    match ip with
    | Cmp_const _ -> ip
    | Cmp_lval l -> Cmp_lval (do_c_lval l) in
  let do_c_memcad_colvar (colv: c_memcad_colvar): c_memcad_colvar = colv in
  let do_c_memcad_iparams (ips: c_memcad_iparams): c_memcad_iparams =
    { mc_pptr = List.map do_c_memcad_iparam ips.mc_pptr;
      mc_pint = List.map do_c_memcad_iparam ips.mc_pint;
      mc_pset = ips.mc_pset;
      mc_pseq = ips.mc_pseq } in
  let do_c_memcad_setexpr (se: c_memcad_setexpr): c_memcad_setexpr =
    match se with
    | Cmp_subset (l, r) ->
        Cmp_subset (do_c_memcad_colvar l, do_c_memcad_colvar r)
    | Cmp_mem (m, r) ->
        Cmp_mem (do_c_lval m, do_c_memcad_colvar r)
    | Cmp_empty l ->
        Cmp_empty (do_c_memcad_colvar l)
    | Cmp_euplus (l, m, r) ->
        Cmp_euplus (do_c_memcad_colvar l, do_c_lval m, do_c_memcad_colvar r) in
  let rec do_c_memcad_seqexpr_basis (se: c_memcad_seqexpr_basis)
      : c_memcad_seqexpr_basis =
    match se with
    | Cmp_seq_empty -> Cmp_seq_empty
    | Cmp_seq_val m ->
        Cmp_seq_val (do_c_lval m)
    | Cmp_seq_var v ->
        Cmp_seq_var (do_c_memcad_colvar v)
    | Cmp_seq_concat (s1, s2) ->
        Cmp_seq_concat
          (do_c_memcad_seqexpr_basis s1,
           do_c_memcad_seqexpr_basis s2)
    | Cmp_seq_sort s ->
        Cmp_seq_sort (do_c_memcad_seqexpr_basis s) in
  let do_c_memcad_seqexpr (se: c_memcad_seqexpr) : c_memcad_seqexpr =
    match se with
    | Cmp_seq_equal (s1, s2) ->
        Cmp_seq_equal
          (do_c_memcad_seqexpr_basis s1,
           do_c_memcad_seqexpr_basis s2) in
  let do_c_memcad_com (com: c_memcad_com): c_memcad_com =
    let preprocess_num_exprs num_exprs =
      List.map
        (fun (op, l, a) -> (op, do_c_lval l, do_c_num_right a)) num_exprs in
    let preprocess_set_exprs set_exprs =
      List.map do_c_memcad_setexpr set_exprs in
    let preprocess_col_vars col_vars =
      List.map do_c_memcad_colvar col_vars in
    let preprocess_seq_exprs seq_exprs =
      List.map do_c_memcad_seqexpr seq_exprs in
    let preprocess_c_vars c_vars =
      List.map do_c_var c_vars in
    match com with
    | Mc_assume num_exprs ->
        Mc_assume (preprocess_num_exprs num_exprs)
    | Mc_assert num_exprs ->
        Mc_assert (preprocess_num_exprs num_exprs)
    | Mc_add_inductive (l, s, a) ->
        Mc_add_inductive (do_c_lval l, s, Option.map do_c_memcad_iparams a)
    | Mc_add_segment (l, s, a, le, se, ae) ->
        Mc_add_segment (do_c_lval l, s, Option.map do_c_memcad_iparams a,
                        do_c_lval le, se, Option.map do_c_memcad_iparams ae)
    | Mc_check_inductive (l, s, a) ->
        Mc_check_inductive (do_c_lval l, s, Option.map do_c_memcad_iparams a)
    | Mc_check_segment (l, s, a, le, se, ae) ->
        Mc_check_segment (do_c_lval l, s, Option.map do_c_memcad_iparams a,
                          do_c_lval le, se, Option.map do_c_memcad_iparams ae)
    | Mc_unfold l -> Mc_unfold (do_c_lval l)
    | Mc_unfold_bseg l -> Mc_unfold_bseg (do_c_lval l)
    | Mc_sel_merge l -> Mc_sel_merge (preprocess_c_vars l)
    | Mc_force_live l -> Mc_force_live (preprocess_c_vars l)
    | Mc_reduce_localize l -> Mc_reduce_localize (do_c_lval l)
    | Mc_mark_prio l -> Mc_mark_prio (do_c_lval l)
    | Mc_output_ext _
    | Mc_check_bottomness _
    | Mc_unroll _
    | Mc_merge
    | Mc_kill_flow
    | Mc_reduce_eager
    | Mc_output_stdout
    | Mc_array_check
    | Mc_array_assume
    | Mc_comstring _ -> com
    | Mc_add_setexprs l -> Mc_add_setexprs (preprocess_set_exprs l)
    | Mc_check_setexprs l -> Mc_check_setexprs (preprocess_set_exprs l)
    | Mc_decl_setvars l -> Mc_decl_setvars (preprocess_col_vars l)
    | Mc_add_seqexprs l -> Mc_add_seqexprs (preprocess_seq_exprs l)
    | Mc_check_seqexprs l -> Mc_check_seqexprs (preprocess_seq_exprs l)
    | Mc_decl_seqvars l -> Mc_decl_seqvars (preprocess_col_vars l) in
  let do_c_call (c: c_call): c_call =
    { cc_fun  = do_c_expr c.cc_fun;
      cc_args = List.map do_c_expr c.cc_args } in
  let rec do_c_statk (s: c_statk): c_statk =
    match s with
    | Csdecl v -> Csdecl (do_c_var v)
    | Cspcall c -> Cspcall (do_c_call c)
    | Csfcall (l, c) -> Csfcall (do_c_lval l, do_c_call c)
    | Csassign (l, e) -> Csassign (do_c_lval l, do_c_expr e)
    | Csblock b -> Csblock (do_c_block b)
    | Csif (e0, b1, b2) -> Csif (do_c_expr e0, do_c_block b1, do_c_block b2)
    | Cswhile (e0, b1, o) -> Cswhile (do_c_expr e0, do_c_block b1, o)
    | Csreturn (Some e) -> Csreturn (Some (do_c_expr e))
    | Csreturn None
    | Csbreak | Cscontinue | Csexit -> s
    | Cs_memcad mc -> Cs_memcad (do_c_memcad_com mc)
    | Csassert e -> Csassert (do_c_expr e)
    | Csalloc (l, e) -> Csalloc (do_c_lval l, do_c_expr e)
    | Csfree l -> Csfree (do_c_lval l)
  and do_c_stat (s: c_stat): c_stat = { s with csk = do_c_statk s.csk }
  and do_c_block (b: c_block): c_block = List.map do_c_stat b in
  let do_c_fun (f: c_fun): c_fun =
    { f with
      cf_type = do_c_type f.cf_type ;
      cf_args = List.map do_c_var f.cf_args ;
      cf_body = do_c_block f.cf_body } in
  { cp_vars  = StringMap.map do_c_var p.cp_vars ;
    cp_funs  = StringMap.map do_c_fun p.cp_funs ;
    cp_types = StringMap.map do_c_type p.cp_types ;
    cp_aggs  = StringMap.map do_c_aggregate p.cp_aggs }

(** Translations between C and abstract domain syntax *)
let tr_c_type: c_type -> typ =
  let rec aux_type (loop: bool) (seen: StringSet.t) (t: c_type): typ =
    match t with
    | Ctvoid -> Tvoid
    | Ctint -> Tint (4, Tsigned)
    | Ctchar -> Tint (1, Tunsigned)
    | Ctstruct (Cad_named _)
    | Ctunion  (Cad_named _) -> Tunk
    | Ctnamed n ->
        begin
          if StringSet.mem n.cnt_name seen then
            Tnamed n.cnt_name
          else
            try Ast_utils.tnamed_find n.cnt_name
            with Not_found -> Tunk (* circularity due to pointers *)
        end
    | Ctstruct (Cad_def s) ->
        let sfields =
          List.map
            (fun fld ->
              { tsf_name = fld.caf_name;
                tsf_off  = fld.caf_off;
                tsf_size = fld.caf_size;
                tsf_typ  = aux_type loop seen fld.caf_typ }
            ) s.cag_fields in
        Tstruct { ts_align  = s.cag_align;
                  ts_size   = s.cag_size;
                  ts_fields = sfields }
    | Ctptr (Some (Ctnamed n)) ->
        if loop then Tptr (Some (Tnamed n.cnt_name))
        else Tptr (Some (aux_type loop seen (Ctnamed n)))
    | Ctptr t -> Tptr (Option.map (aux_type loop seen) t)
    | Ctarray (t, Some n) -> Tarray (aux_type loop seen t, n)
    | Ctarray (t, None)   -> Tptr (Some (aux_type loop seen t)) (* ptr type *)
    | Ctstring n -> Tstring n
    | Ctunion (Cad_def u) ->
        let ufields =
          List.map
            (fun fld ->
              { tuf_name = fld.caf_name;
                tuf_size = fld.caf_size;
                tuf_typ  = aux_type loop seen fld.caf_typ }
            ) u.cag_fields in
        Tunion { tu_align  = u.cag_align;
                 tu_size   = u.cag_size;
                 tu_fields = ufields } in
  aux_type false StringSet.empty
let tr_c_var (v: c_var): var =
  { v_name = v.cv_name ;
    v_id   = v.cv_uid ;
    v_typ  = tr_c_type v.cv_type }
let tr_c_colvar (l: c_memcad_colvar): colvar =
  { colv_name = l.mc_colvar_name;
    colv_uid  = l.mc_colvar_uid;
    colv_root = true;
    colv_kind = l.mc_colvar_kind }
let tr_c_const (c: c_const): const =
  match c with
  | Ccint i -> Cint i
  | Ccchar c -> Cchar c
  | Ccstring s -> Cstring s
  | Ccnull -> Cint 0
let tr_c_field (t: c_type) (fld: string): field =
  let ooff =
    match tr_c_type t with
    | Tstruct t -> Some (Ast_utils.find_field t fld).tsf_off
    | Tunion tu -> Some 0
    | _ -> Log.fatal_exn "field in a non pack type" in
  { f_name = fld ;
    f_off  = ooff }
(* GADT used to translate c_* to two deifferent types with one function *)
type _ trad_res =
  | VAR     : var      trad_res
  | VARINFO : var_info trad_res
let rec tr_c_exprk_aux: type a. a trad_res -> c_exprk -> a expr =
  fun aux e ->
    match e with
    | Ceconst c -> Ecst (tr_c_const c)
    | Celval l ->
        begin
          match l.clk with
            (* In the case of a colv info we don't have to dereference the sv
             * corresponding to the info. *)
          | Clcolvarinfo _ -> EAddrOf (tr_c_lval_aux aux l)
          | Clvar v ->
              if v.cv_volatile then Erand
              else Elval (tr_c_lval_aux aux l)
          | _ -> Elval (tr_c_lval_aux aux l)
        end
    | Ceuni (Cuneg , e0)     -> Euni (Uneg , tr_c_expr_aux aux e0)
    | Ceuni (Culnot, e0)     -> Euni (Unot , tr_c_expr_aux aux e0)
    | Ceuni (Cubnot, e0)     -> Euni (Ubnot, tr_c_expr_aux aux e0)
    | Cebin (Cbadd , e0, e1) ->
        Ebin (Badd, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbmul , e0, e1) ->
        Ebin (Bmul, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbsub , e0, e1) ->
        Ebin (Bsub, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbmod , e0, e1) ->
        Ebin (Bmod, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbdiv , e0, e1) ->
        Ebin (Bdiv, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbge  , e0, e1) ->
        Ebin (Bge , tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cbgt  , e0, e1) ->
        Ebin (Bgt , tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cble  , e0, e1) ->
        Ebin (Bge , tr_c_expr_aux aux e1, tr_c_expr_aux aux e0)
    | Cebin (Cblt  , e0, e1) ->
        Ebin (Bgt , tr_c_expr_aux aux e1, tr_c_expr_aux aux e0)
    | Cebin (Cbeq  , e0, e1) ->
        Ebin (Beq , tr_c_expr_aux aux e1, tr_c_expr_aux aux e0)
    | Cebin (Cbne  , e0, e1) ->
        Ebin (Bne , tr_c_expr_aux aux e1, tr_c_expr_aux aux e0)
    | Cebin (Cbland, e0, e1) ->
        Ebin (Band, tr_c_expr_aux aux e0, tr_c_expr_aux aux e1)
    | Cebin (Cblor , e0, e1) ->
        (* accodring to c: e0 \/ e1 is : e0 \/ (not e0 /\ e1) *)
        let ne0 = c_expr_neg e0 in
        let ne0_e1 =  { e1 with cek = Cebin (Cbland, ne0, e1) } in
        Ebin (Bor , tr_c_expr_aux aux e0, tr_c_expr_aux aux ne0_e1)
    | Cebin (Cbband, e0, e1) -> Erand
    | Cebin (Cbbor , e0, e1) -> Erand
    | Cebin (Cbbslft, e0, e1) -> Erand
    | Cebin (Cbbsrgh, e0, e1) -> Erand
    | Ceaddrof l -> EAddrOf (tr_c_lval_aux aux l)
and tr_c_expr_aux: type a. a trad_res -> c_expr -> a texpr  =
  fun aux e -> tr_c_exprk_aux aux e.cek, tr_c_type e.cet
and tr_c_lvalk_aux: type a. a trad_res -> c_lvalk -> a lval =
  fun aux l ->
  match l, aux with
  | Clvar v, VAR -> Lvar (tr_c_var v)
  | Clvar v, VARINFO -> Lvar (Var (tr_c_var v))
  | Clcolvarinfo (colv, ik), VAR ->
    Log.fatal_exn "Unexecpected Clcolvarinfo (%a, %a)"
    c_memcad_colvar_fpr colv (assert false) ik
  | Clcolvarinfo (colv, ik), VARINFO -> Lvar (Info (tr_c_colvar colv, ik))
  | Clfield (l0, fld), _ -> Lfield (tr_c_lval_aux aux l0, tr_c_field l0.clt fld)
  | Clderef e0, _ -> Lderef (tr_c_expr_aux aux e0)
  | Clindex (l0, e0), _ -> Lindex (tr_c_lval_aux aux l0, tr_c_expr_aux aux e0)
and tr_c_lval_aux: type a. a trad_res -> c_lval -> a tlval =
  fun aux l -> tr_c_lvalk_aux aux l.clk, tr_c_type l.clt

let tr_c_expr = tr_c_expr_aux VAR
let tr_c_exprk = tr_c_exprk_aux VAR
let tr_c_lval = tr_c_lval_aux VAR
let tr_c_lvalk = tr_c_lvalk_aux VAR
let tr_c_expr_winfo = tr_c_expr_aux VARINFO
let tr_c_exprk_winfo = tr_c_exprk_aux VARINFO
let tr_c_lval_winfo = tr_c_lval_aux VARINFO
let tr_c_lvalk_winfo = tr_c_lvalk_aux VARINFO

(**  Translations between C set expression and abstract domain syntax *)
let tr_c_setexpr (l: c_memcad_setexpr): var tlval setprop =
  match l with
  | Cmp_subset (l, r) -> Sp_sub (tr_c_colvar l, tr_c_colvar r)
  | Cmp_mem (c, r) -> Sp_mem (tr_c_lval c, tr_c_colvar r)
  | Cmp_empty l -> Sp_emp (tr_c_colvar l)
  | Cmp_euplus (l, c, r) ->
      Sp_euplus (tr_c_colvar l, tr_c_lval c, tr_c_colvar r)

(**  Translations between C seq expression and abstract domain syntax *)
let rec tr_c_seqexpr_basis (l: c_memcad_seqexpr_basis): var tlval seqexpr =
  match l with
  | Cmp_seq_empty -> Qe_empty
  | Cmp_seq_var v -> Qe_var (tr_c_colvar v)
  | Cmp_seq_val x -> Qe_val (tr_c_lval x)
  | Cmp_seq_concat (s1, s2) ->
      let q1 = tr_c_seqexpr_basis s1 in
      let q2 = tr_c_seqexpr_basis s2 in
      Qe_concat (q1, q2)
  | Cmp_seq_sort s ->
      Qe_sort (tr_c_seqexpr_basis s)
let tr_c_seqexpr (l: c_memcad_seqexpr): var tlval seqprop =
  match l with
  | Cmp_seq_equal (e1, e2) ->
      Qp_equal (tr_c_seqexpr_basis e1, tr_c_seqexpr_basis e2)
