(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ast_utils.mli
 **       utilities on abstract syntax trees
 ** Xavier Rival, 2011/05/27 *)
open Data_structures
open Ast_sig

(** Some basic iterators on expressions, lvalues and set & seq properties *)
val map_expr: ('a -> 'b) -> 'a expr -> 'b expr
val map_texpr: ('a -> 'b) -> 'a texpr -> 'b texpr
val map_lval: ('a -> 'b) -> 'a lval -> 'b lval
val map_tlval: ('a -> 'b) -> 'a tlval -> 'b tlval
val map_setprop: ('a -> 'b) -> (colvar -> int) -> 'a tlval setprop
  -> 'b tlval setprop
val map_seqprop: ('a -> 'b) -> (colvar -> int) -> 'a tlval seqprop
  -> 'b tlval seqprop

(** Named types *)
val tnamed_find: string -> typ
val tnamed_add: string -> typ -> unit

(** Utilities on types *)
val sizeof_typ: typ -> int
val alignof_typ: typ -> int
val find_field: typ_struct -> string -> typ_sfield
val typ_is_ptr: typ -> bool

(** Pretty-printing of expressions, and lvalues *)
(* types *)
val typ_fpr:     form -> typ -> unit
(* unary operators *)
val uni_op_fpr:  form -> uni_op -> unit
(* binary operators *)
val bin_op_fpr:  form -> bin_op -> unit
(* constants *)
val const_fpr:   form -> const -> unit
(* expressions *)
val expr_fpr:     (form -> 'a -> unit) -> form -> 'a expr  -> unit
val texpr_fpr:    (form -> 'a -> unit) -> form -> 'a texpr -> unit
val lval_fpr:     (form -> 'a -> unit) -> form -> 'a lval  -> unit
val tlval_fpr:    (form -> 'a -> unit) -> form -> 'a tlval -> unit
val condtree_fpr: (form -> 'a -> unit) -> form -> 'a condtree -> unit

(** Printers for c-vars *)
(*val col_kind_fpr:  form -> col_kind -> string*)
val col_kind_fpr:  form -> col_kind -> unit
val var_fpr:       form -> var -> unit
val colvar_fpr:    form -> colvar -> unit
val info_kind_fpr: form -> info_kind -> unit
val var_info_fpr:  form -> var_info -> unit
val vexpr_fpr:     form -> var expr -> unit
val vtexpr_fpr:    form -> var texpr -> unit
val vlval_fpr:     form -> var lval -> unit
val vtlval_fpr:    form -> var tlval -> unit
val vcondtree_fpr: form -> var condtree -> unit

(** Printers for int *)
val iexpr_fpr:     form -> int expr -> unit
val itexpr_fpr:    form -> int texpr -> unit
val ilval_fpr:     form -> int lval -> unit
val itlval_fpr:    form -> int tlval -> unit
val icondtree_fpr: form -> int condtree -> unit

(** Typing *)
(* Computing well typed expressions and conditions *)
val type_texpr: (var -> typ) -> var texpr -> var texpr
val type_expr:  (var -> typ) -> var expr  -> var texpr
val type_tlval: (var -> typ) -> var tlval -> var tlval
val type_lval:  (var -> typ) -> var lval  -> var tlval

(** Binding *)
val bind_var:   (string -> int) -> var -> var
val bind_texpr: (string -> int) -> var texpr -> var texpr
val bind_tlval: (string -> int) -> var tlval -> var tlval

(** Extraction of condition trees *)
(* Simply pulls condition operators at the top into another ast.
 * This will make treatment of the guard operator easier *)
val extract_tree: 'a texpr -> 'a condtree
