%{
(** MemCAD analyzer
 **
 ** mc_parser.mly
 ** MemCAD command parser
 ** Xavier Rival, 2011/12/26 *)
open C_sig

(* Creation of type-less typed expressions and l-values *)
let mkve (e: c_exprk): c_expr =
  { cek = e ;
    cet = Ctvoid (* for now *);
    cep = 0; }
let mkvl (l: c_lvalk): c_lval =
  { clk = l ;
    clt = Ctvoid (* for now *) }
%}

%token T_comma T_pipe T_dot
%token T_lparen T_rparen
%token T_lbrack T_rbrack
%token T_arrow
%token T_subseteq T_setin T_equal T_setempty T_euplus
%token T_eq T_ne T_lt T_gt T_le T_ge
%token T_seg
%token T_min T_max T_size

%token <bool>      V_bool
%token <int>       V_int
%token <int>       V_size_arrow
%token <string>    V_string
%token <string>    V_astring

%token T_add_inductive
%token T_add_segment
%token T_assert
%token T_assume
%token T_decl_setvars
%token T_decl_seqvars
%token T_check_bottomness
%token T_check_inductive
%token T_check_segment
%token T_set_check
%token T_seq_check
%token T_force_live
%token T_kill_flow
%token T_merge
%token T_output_dot
%token T_output_stdout
%token T_reduce_eager
%token T_reduce_localize
%token T_mark_prio
%token T_sel_merge
%token T_seq_assume
%token T_set_assume
%token T_sort
%token T_unfold
%token T_unfold_bseg
%token T_unroll

%token T_array_check
%token T_array_assume
%token T_eof

%type <C_sig.c_memcad_com>             memcad_command
%type <string>                         indname
%type <string list>                    var_name_list
%type <C_sig.c_memcad_iparams option>  indparams
%type <C_sig.c_memcad_iparam list>     lparams
%type <C_sig.c_memcad_iparam>          param
%type <C_sig.c_var>                    c_var
%type <C_sig.c_lval>                   c_lval
%type <C_sig.c_var list>               c_var_list
%type <C_sig.c_memcad_setexpr>         set_expr
%type <C_sig.c_memcad_setexpr list>    set_exprs
%type <C_sig.c_num_expr list>          num_exprs
%type <C_sig.c_memcad_colvar list>     lparams_set
%type <C_sig.c_memcad_colvar list>     setvars

%start memcad_command
%%

/* The target below lists the commmands in the same order as in the
 * documentation, with the titles of the paragraphs
 * (this should make it easier to keep things consistent). */
memcad_command:
/* Doc: 3.2.1. Special output */
| T_output_stdout T_lparen T_rparen
    { Mc_output_stdout }
| T_output_dot T_lparen var_name_list T_pipe var_name_list T_rparen
    { Mc_output_ext (Lib.Out_dot ($3, $5)) }
/* Doc: 3.2.2. Structural assumptions and assertions */
| T_add_inductive T_lparen c_lval T_comma indname indparams T_rparen
    { Mc_add_inductive ($3, Some $5, $6) }
| T_add_segment T_lparen c_lval T_comma indname indparams T_seg
    c_lval T_comma indname indparams T_rparen
    { Mc_add_segment ($3, $5, $6, $8, $10, $11) }
| T_check_inductive T_lparen c_lval T_rparen
    { Mc_check_inductive ($3, None, None) }
| T_check_inductive T_lparen c_lval T_comma indname indparams T_rparen
    { Mc_check_inductive ($3, Some $5, $6) }
| T_check_segment T_lparen c_lval T_comma indname indparams T_seg
    c_lval T_comma indname indparams T_rparen
    { Mc_check_segment ($3, $5, $6, $8, $10, $11 ) }
/* Doc: 3.2.3. Value assumptions and assertions */
| T_assume T_lparen num_exprs T_rparen
    { Mc_assume $3 }
| T_assert T_lparen num_exprs T_rparen
    { Mc_assert $3 }
/* Doc: 3.2.4. Set assumptions and assertions */
| T_decl_setvars T_lparen setvars T_rparen
    { Mc_decl_setvars $3}
| T_set_assume T_lparen set_exprs T_rparen
    { Mc_add_setexprs $3 }
| T_set_check T_lparen set_exprs T_rparen
    { Mc_check_setexprs $3 }
/* Doc: 3.2.5. Sequence assumptions and assertions */
| T_decl_seqvars T_lparen seqvars T_rparen
    { Mc_decl_seqvars $3}
| T_seq_assume T_lparen seq_exprs T_rparen
    { Mc_add_seqexprs $3 }
| T_seq_check T_lparen seq_exprs T_rparen
    { Mc_check_seqexprs $3 }
/* Doc: 3.2.6. Control of unfolding */
| T_unfold T_lparen c_lval T_rparen
    { Mc_unfold $3 }
| T_unfold_bseg T_lparen c_lval T_rparen
    { Mc_unfold_bseg $3 }
/* Doc: 3.2.7. Checking and control of execution flows */
| T_check_bottomness T_lparen V_bool T_rparen
    { Mc_check_bottomness $3 }
| T_kill_flow
    { Mc_kill_flow }
/* Doc: 3.2.8. Control of the handling of disjunctions */
| T_unroll T_lparen V_int T_rparen
    { Mc_unroll $3 }
| T_merge T_lparen T_rparen
    { Mc_merge }
| T_sel_merge T_lparen c_var_list T_rparen
    { Mc_sel_merge (List.rev $3) }
| T_force_live T_lparen c_var_list T_rparen
    { Mc_force_live (List.rev $3) }
| T_mark_prio T_lparen c_lval T_rparen
    { Mc_mark_prio $3 }
/* Doc: 3.2.9. Reduction */
| T_reduce_eager
    { Mc_reduce_eager }
| T_reduce_localize T_lparen c_lval T_rparen
    { Mc_reduce_localize $3 }
/* Doc: 3.2.10. Array abstraction */
| T_array_check V_astring
    { Mc_array_check }
| T_array_assume V_astring
    { Mc_array_assume }

indname:
| V_string { $1 }

indparams:
| { None }
| T_comma T_lbrack lparams T_pipe lparams T_pipe lparams_set T_rbrack
      { Some { mc_pptr = $3 ;
               mc_pint = $5 ;
               mc_pset = $7 ;
               mc_pseq = [] } }
| T_comma T_lbrack lparams T_pipe lparams T_pipe lparams_set T_pipe
    lparams_seq T_rbrack
      { Some { mc_pptr = $3 ;
               mc_pint = $5 ;
               mc_pset = $7 ;
               mc_pseq = $9 } }

lparams:
| { [ ] }
| param { [ $1 ] }
| param T_comma lparams { $1 :: $3 }

param:
| V_int    { Cmp_const $1 }
| c_lval   { Cmp_lval  $1 }

setvars:
| {[ ]}
| setvar   { [ $1 ] }
| setvar T_comma setvars { $1::$3 }

setvar:
| V_string { { mc_colvar_name = $1;
               mc_colvar_uid  = -1;
               mc_colvar_kind = CK_set } }

seqvars:
| {[ ]}
| seqvar   { [ $1 ] }
| seqvar T_comma seqvars { $1::$3 }

seqvar:
| V_string { { mc_colvar_name = $1;
               mc_colvar_uid  = -1;
               mc_colvar_kind = CK_seq } }

lparams_set:
| {[ ]}
| setvar { [$1] }
| setvar T_comma lparams_set { $1::$3 }

lparams_seq:
| {[ ]}
| seqvar { [$1] }
| seqvar T_comma lparams_seq{ $1::$3 }

set_exprs:
| { [ ] }
| set_expr { [ $1 ] }
| set_expr T_comma set_exprs { $1 :: $3 }

set_expr:
| setvar T_subseteq setvar     { Cmp_subset (  $1, $3 ) }
| c_lval T_setin setvar        { Cmp_mem (   $1,  $3 ) }
| setvar T_equal T_setempty    { Cmp_empty $1 }
| setvar T_equal c_lval T_euplus setvar { Cmp_euplus ($1, $3, $5) }

seq_exprs:
| { [ ] }
| seq_expr { [ $1 ] }
| seq_expr T_comma seq_exprs { $1 :: $3 }

seq_expr:
| seq_expr_basis T_equal seq_expr_basis     { Cmp_seq_equal (  $1, $3 ) }

seq_expr_basis:
| seq_expr_basis_atom { $1 }
| seq_expr_basis_atom T_dot seq_expr_basis { Cmp_seq_concat ($1, $3) }

seq_expr_basis_atom:
| seqvar { Cmp_seq_var $1 }
| T_lbrack T_rbrack { Cmp_seq_empty }
| T_lbrack c_lval T_rbrack { Cmp_seq_val $2 }
| T_sort T_lparen seq_expr_basis T_rparen { Cmp_seq_sort $3 }


num_exprs:
| { [] }
| num_expr { [$1] }
| num_expr T_comma num_exprs { ($1 :: $3) }

num_expr:
| c_lval T_eq num_right { (Cbeq, $1, $3) }
| c_lval T_ne num_right { (Cbne, $1, $3) }
| c_lval T_lt num_right { (Cblt, $1, $3) }
| c_lval T_gt num_right { (Cbgt, $1, $3) }
| c_lval T_le num_right { (Cble, $1, $3) }
| c_lval T_ge num_right { (Cbge, $1, $3) }

c_var:
| V_string { { cv_name     = $1;
               cv_uid      = -1;
               cv_type     = Ctvoid;
               cv_volatile = false } }

info_kind:
| T_min  { Ast_sig.MIN  }
| T_max  { Ast_sig.MAX  }
| T_size { Ast_sig.SIZE }

num_right:
| V_int  { Cnr_const $1 }
| c_lval { Cnr_lval $1 }

c_lval:
| c_var { { clk = Clvar $1 ;
            clt = Ctvoid } }
| info_kind T_lparen seqvar T_rparen
    { { clk = Clcolvarinfo ($3, $1) ;
        clt = Ctvoid } }
| c_lval T_arrow V_string
    { { clk = Clfield (mkvl (Clderef (mkve (Celval $1))), $3);
        clt = Ctvoid } }
| c_lval T_lbrack c_lval T_rbrack
    { { clk = Clindex ($1,(mkve (Celval $3)));
        clt = Ctvoid } }
| c_lval T_lbrack c_lval T_rbrack T_dot V_string
    { { clk = Clfield (mkvl (Clindex ($1,(mkve (Celval $3)))),$6);
        clt = Ctvoid } }

c_var_list:
| { [ ] }
| c_var { [ $1 ] }
| c_var_list T_comma c_var { $3 :: $1 }

var_name_list:
| { [ ] }
| V_string { [ $1 ] }
| V_string T_comma var_name_list { $1 :: $3 }
