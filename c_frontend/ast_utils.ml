(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ast_utils.ml
 **       utilities on abstract syntax trees
 ** Xavier Rival, 2011/05/27 *)
open Data_structures
open Flags
open Lib
open Ast_sig

(** Error report *)
module Log =
  Logger.Make(struct let section = "ast_uts_" and level = Log_level.DEBUG end)

(** Some basic iterators on expressions, lvalues and set & seq properties *)
let rec map_expr (f: 'a -> 'b) (e: 'a expr): 'b expr =
  match e with
  | Erand -> Erand
  | Ecst c -> Ecst c
  | Euni (o, e0) -> Euni (o, map_texpr f e0)
  | Ebin (o, e0, e1) -> Ebin (o, map_texpr f e0, map_texpr f e1)
  | Elval lv -> Elval (map_tlval f lv)
  | EAddrOf lv -> EAddrOf (map_tlval f lv)
and map_texpr (f: 'a -> 'b) (e, t) = map_expr f e, t
and map_lval (f: 'a -> 'b) (l: 'a lval): 'b lval =
  match l with
  | Lvar v -> Lvar (f v)
  | Lderef e -> Lderef (map_texpr f e)
  | Lfield (l, off) -> Lfield (map_tlval f l, off)
  | Lindex (l, e) -> Lindex (map_tlval f l, map_texpr f e)
and map_tlval (f: 'a -> 'b) (l, t) = map_lval f l, t
let map_setprop (f: 'a -> 'b) (g: colvar -> int)  (s: 'a tlval setprop)
    : 'b tlval setprop =
  let do_setvar v = { v with colv_uid = g v } in
  match s with
  | Sp_sub (l, r) -> Sp_sub (do_setvar l, do_setvar r)
  | Sp_mem (l, r) -> Sp_mem (map_tlval f l, do_setvar r)
  | Sp_emp l -> Sp_emp (do_setvar l)
  | Sp_euplus (l, i, r) -> Sp_euplus (do_setvar l, map_tlval f i, do_setvar r)
let rec map_seqexpr (f: 'a -> 'b) (g: colvar -> int)  (s: 'a tlval seqexpr)
    : 'b tlval seqexpr =
  match s with
  | Qe_empty -> Qe_empty
  | Qe_var v -> Qe_var { v with colv_uid = g v }
  | Qe_val x -> Qe_val (map_tlval f x)
  | Qe_sort s -> Qe_sort (map_seqexpr f g s)
  | Qe_concat (s1, s2) -> Qe_concat (map_seqexpr f g s1, map_seqexpr f g s2)
let map_seqprop (f: 'a -> 'b) (g: colvar -> int)  (s: 'a tlval seqprop)
    : 'b tlval seqprop =
  match s with
  | Qp_equal (s1, s2) -> Qp_equal (map_seqexpr f g s1, map_seqexpr f g s2)

(** Named types *)
let tnamed_types: typ StringMap.t ref = ref StringMap.empty
let tnamed_find (n: string): typ =
  StringMap.find n !tnamed_types
let tnamed_add (n: string) (t: typ): unit =
  tnamed_types := StringMap.add n t !tnamed_types

(** Utilities on types *)
let rec alignof_typ (t: typ): int =
  match t with
  | Tunk -> Log.fatal_exn "align of [unknown-type]"
  | Tint (sz,_) -> sz
  | Tfloat _ -> Flags.abi_ptr_size
  | Tptr _ -> Flags.abi_ptr_size
  | Tbool -> Flags.abi_bool_size
  | Tchar -> Flags.abi_char_size
  | Tstring _ -> Flags.abi_char_size
  | Tstruct s -> s.ts_align
  | Tunion tu -> tu.tu_align
  | Tarray (tu, _) -> alignof_typ tu
  | Tnamed n -> alignof_typ (tnamed_find n)
  | Tvoid -> 1
let rec sizeof_typ (t: typ): int =
  match t with
  | Tunk -> Log.fatal_exn "size of [unknown-type]"
  | Tint (sz,_) -> sz
  | Tfloat sz -> sz
  | Tptr _ -> Flags.abi_ptr_size
  | Tbool -> Flags.abi_bool_size
  | Tchar -> Flags.abi_char_size
  | Tstring n -> Flags.abi_char_size * n
  | Tstruct s -> s.ts_size
  | Tunion tu -> tu.tu_size
  | Tarray (t0,nb) -> nb * sizeof_typ t0
  | Tnamed n -> sizeof_typ (tnamed_find n)
  | Tvoid -> 1
let find_field (s: typ_struct) (fld: string): typ_sfield =
  let l =
    List.filter (fun f -> String.compare f.tsf_name fld = 0) s.ts_fields in
  match l with
  | [ ]   -> Log.fatal_exn "find_field_offset: none found"
  | [ f ] -> f
  | _     -> Log.fatal_exn "find_field_offset: too many found"
let typ_is_ptr: typ -> bool = function
  | Tptr _ -> true
  | _ -> false


(** Pretty-printing of expressions, and lvalues *)
let tsign_fpr (fmt: form): tsign -> unit = function
  | Tsigned   -> F.fprintf fmt "signed"
  | Tunsigned -> F.fprintf fmt "unsigned"
let tisize_fpr (fmt: form): tisize -> unit = function
  | 1 -> F.fprintf fmt "char"
  | 2 -> F.fprintf fmt "short"
  | 4 -> F.fprintf fmt "int"
  | _ -> Log.fatal_exn "unbound size of int type"
let field_names_of_struct s =
  List.map (fun f -> f.tsf_name) s.ts_fields
let field_names_of_union u =
  List.map (fun f -> f.tuf_name) u.tu_fields
let struct_fpr fmt s =
  gen_list_fpr "" F.pp_print_string "; " fmt (field_names_of_struct s)
let union_fpr fmt u =
  gen_list_fpr "" F.pp_print_string "; " fmt (field_names_of_union u)
(* types *)
let rec typ_fpr (fmt: form): typ -> unit = function
  | Tunk -> F.fprintf fmt "[unknown-type]"
  | Tint (i, s) -> F.fprintf fmt "%a %a" tsign_fpr s tisize_fpr i
  | Tfloat 4 -> F.fprintf fmt "float"
  | Tfloat 8 -> F.fprintf fmt "double"
  | Tfloat _ -> Log.fatal_exn "unbound size of float type"
  | Tptr None -> F.fprintf fmt "(?)*"
  | Tptr (Some t) -> F.fprintf fmt "(%a)*" typ_fpr t
  | Tbool -> F.fprintf fmt "bool"
  | Tchar -> F.fprintf fmt "char"
  | Tstring n -> F.fprintf fmt "char[%d]" n
  | Tstruct s -> F.fprintf fmt "struct: {%a}" struct_fpr s
  | Tunion  u -> F.fprintf fmt "union: {%a}" union_fpr  u
  | Tarray (t0,nb) -> F.fprintf fmt "%a[%d]" typ_fpr t0 nb
  | Tnamed n -> F.fprintf fmt "type[%s]" n
  | Tvoid -> F.fprintf fmt "void"
(* unary operators *)
let uni_op_fpr (fmt: form): uni_op -> unit = function
  | Uneg -> F.fprintf fmt "-"
  | Unot -> F.fprintf fmt "!"
  | Ubnot -> F.fprintf fmt "~"
(* binary operators *)
let bin_op_fpr (fmt: form): bin_op -> unit = function
  | Badd -> F.fprintf fmt "+"
  | Bsub -> F.fprintf fmt "-"
  | Bmul -> F.fprintf fmt "*"
  | Bdiv -> F.fprintf fmt "/"
  | Bmod -> F.fprintf fmt "%c" '%'
  | Beq  -> F.fprintf fmt "=="
  | Bgt  -> F.fprintf fmt ">"
  | Bge  -> F.fprintf fmt ">="
  | Bne  -> F.fprintf fmt "!="
  | Band -> F.fprintf fmt "&&"
  | Bor  -> F.fprintf fmt "||"
(* constants *)
let const_fpr (fmt: form): const -> unit = function
  | Cint i -> F.fprintf fmt "%d" i
  | Cint64 ii -> F.fprintf fmt "%Ld" ii
  | Cchar c -> F.fprintf fmt "\'%c\'" c
  | Cstring s -> F.fprintf fmt "\"%s\"" s
(* expressions and components *)
let arith_fpr (afpr: form -> 'a -> unit) =
  let rec aux_expr0 (fmt: form): 'a expr -> unit = function
    | Ebin ((Band | Bor) as o, e0, e1) ->
        F.fprintf fmt "%a %a %a" aux_texpr1 e0 bin_op_fpr o aux_texpr1 e1
    | e -> aux_expr1 fmt e
  and aux_expr1 (fmt: form): 'a expr -> unit = function
    | Ebin ((Beq | Bgt | Bge | Bne) as o, e0, e1) ->
        F.fprintf fmt "%a %a %a" aux_texpr1 e0 bin_op_fpr o aux_texpr2 e1
    | e -> aux_expr2 fmt e
  and aux_expr2 (fmt: form): 'a expr -> unit = function
    | Ebin ((Badd | Bsub) as o, e0, e1) ->
        F.fprintf fmt "%a %a %a" aux_texpr2 e0 bin_op_fpr o aux_texpr3 e1
    | e -> aux_expr3 fmt e
  and aux_expr3 (fmt: form): 'a expr -> unit = function
    | Ebin ((Bmul | Bdiv | Bmod) as o, e0, e1) ->
        F.fprintf fmt "%a %a %a" aux_texpr3 e0 bin_op_fpr o aux_texpr4 e1
    | e -> aux_expr4 fmt e
  and aux_expr4 (fmt: form): 'a expr -> unit = function
    | Euni (o, e0) -> F.fprintf fmt "%a %a" uni_op_fpr o aux_texpr5 e0
    | e -> aux_expr5 fmt e
  and aux_expr5 (fmt: form): 'a expr -> unit = function
    | Erand -> F.fprintf fmt "rand"
    | Ecst c -> const_fpr fmt c
    | Elval lv -> aux_tlval fmt lv
    | EAddrOf lv -> F.fprintf fmt "& %a" aux_tlval lv
    | e -> F.fprintf fmt "(%a)" aux_expr0 e
  and aux_texpr0 fmt (e, t) = aux_expr0 fmt e
  and aux_texpr1 fmt (e, t) = aux_expr1 fmt e
  and aux_texpr2 fmt (e, t) = aux_expr2 fmt e
  and aux_texpr3 fmt (e, t) = aux_expr3 fmt e
  and aux_texpr4 fmt (e, t) = aux_expr4 fmt e
  and aux_texpr5 fmt (e, t) = aux_expr5 fmt e
  and aux_lval (fmt: form): 'a lval -> unit = function
    | Lvar x -> afpr fmt x
    | Lderef e -> F.fprintf fmt "(* %a)" aux_texpr3 e
    | Lfield (l, f) -> F.fprintf fmt "%a.%s" aux_tlval l f.f_name
    | Lindex (l, e) -> F.fprintf fmt "%a[%a]" aux_tlval l aux_texpr0 e
  and aux_tlval (fmt: form) (l, t) = aux_lval fmt l in
  aux_expr0, aux_texpr0, aux_lval, aux_tlval
let expr_fpr  afpr = let f, _, _, _ = arith_fpr afpr in f
let texpr_fpr afpr = let _, f, _, _ = arith_fpr afpr in f
let lval_fpr  afpr = let _, _, f, _ = arith_fpr afpr in f
let tlval_fpr afpr = let _, _, _, f = arith_fpr afpr in f
let condtree_fpr afpr fmt =
  let rec aux fmt = function
    | Ctrand -> F.fprintf fmt "rand"
    | Ctleaf ex -> texpr_fpr afpr fmt ex
    | Ctland (c0, c1) -> F.fprintf fmt "(%a) /\\ (%a)" aux c0 aux c1
    | Ctlor (c0, c1) -> F.fprintf fmt "(%a) \\/ (%a)" aux c0 aux c1 in
  aux fmt

(** Printers for c-vars based expressions *)
let col_kind_fpr (fmt: form): col_kind -> unit = function
  | CK_set -> F.fprintf fmt "set"
  | CK_seq -> F.fprintf fmt "seq"
let var_fpr (fmt: form) (v: var): unit =
  if Flags.flag_debug_uids then F.fprintf fmt "%s<%d>" v.v_name v.v_id
  else F.fprintf fmt "%s" v.v_name
let colvar_fpr (fmt: form) (colv: colvar): unit =
  if Flags.flag_debug_uids then
    F.fprintf fmt "%s<%d>" colv.colv_name colv.colv_uid
  else F.fprintf fmt "%s" colv.colv_name
let info_kind_fpr (fmt: form) (ik: info_kind): unit =
  F.fprintf fmt "%s" (match ik with MAX -> "max" | MIN -> "min" | SIZE -> "size")
let var_info_fpr (fmt: form) (vi: var_info): unit =
  match vi with
  | Var v -> var_fpr fmt v
  | Info (colv, ik) -> F.fprintf fmt "%a(%a)" info_kind_fpr ik colvar_fpr colv
let vexpr_fpr:     form -> var  expr -> unit =  expr_fpr var_fpr
let vtexpr_fpr:    form -> var texpr -> unit = texpr_fpr var_fpr
let vlval_fpr:     form -> var  lval -> unit =  lval_fpr var_fpr
let vtlval_fpr:    form -> var tlval -> unit = tlval_fpr var_fpr
let vcondtree_fpr: form -> var condtree -> unit = condtree_fpr var_fpr

(** Printers for int *)
let int_fpr (fmt: form) (v: int): unit = F.fprintf fmt "%d" v
let iexpr_fpr:     form -> int  expr -> unit =  expr_fpr int_fpr
let itexpr_fpr:    form -> int texpr -> unit = texpr_fpr int_fpr
let ilval_fpr:     form -> int  lval -> unit =  lval_fpr int_fpr
let itlval_fpr:    form -> int tlval -> unit = tlval_fpr int_fpr
let icondtree_fpr: form -> int condtree -> unit = condtree_fpr int_fpr

(** Typing *)
(* Basic functions to check correctness of types, combine them, etc *)
let is_typ_int: typ -> bool = function
  | Tint (_, _) -> true
  | _ -> false
let cst_typ: const -> typ = function
  | Cint _ -> Tint (4, Tsigned)
  | Cint64 _ -> Tint (8, Tsigned)
  | Cchar _ -> Tchar
  | Cstring s -> Tstring (String.length s)
let int_promotion (sz0, sg0) (sz1, sg1) =
  if sz0 = sz1 && sg0 = sg1 then (sz0, sg1)
  else Log.todo_exn "non trivial integer promotion"
let bin_typ (b: bin_op) (t0: typ) (t1: typ) =
  match b, t0, t1 with
  | Bgt, Tint _, Tint _
  | Bge, Tint _, Tint _
  | Beq, Tint _, Tint _ -> Tbool
  | Badd, Tint (sz0, sg0), Tint (sz1, sg1) ->
      let sz, sg = int_promotion (sz0, sg0) (sz1, sg1) in
      Tint (sz, sg)
  | _, _, _ ->
      Log.fatal_exn "bin_typ: %a, %a, %a" bin_op_fpr b typ_fpr t0 typ_fpr t1
(* Computing well typed expressions and conditions *)
let rec type_texpr (m: var -> typ) (e: var texpr): var texpr =
  type_expr m (fst e)
and type_expr (m: var -> typ) (e: var expr): var texpr =
  match e with
  | Erand -> Log.todo_exn "typ rand"
  | Ecst c -> e, cst_typ c
  | Euni _ -> Log.todo_exn "typ uni"
  | Ebin (b, te0, te1) ->
      let ne0 = type_texpr m te0 and ne1 = type_texpr m te1 in
      let nt = bin_typ b (snd ne0) (snd ne1) in
      Ebin (b, ne0, ne1), nt
  | Elval lv -> let nlv = type_tlval m lv in Elval nlv, snd nlv
  | EAddrOf lv ->
      let nlv = type_tlval m lv in
      EAddrOf nlv, Tptr (Some (snd nlv))
and type_tlval (m: var -> typ) (l: var tlval): var tlval =
  type_lval m (fst l)
and type_lval (m: var -> typ) (l: var lval): var tlval =
  match l with
  | Lvar x ->
      let t =
        try m x
        with Not_found ->
          Log.warn "type not found for var %a\n" var_fpr x;
          Tunk in
      l, t
  | Lderef e ->
      let ne = type_texpr m e in
      begin
        match snd ne with
        | Tptr (Some t) -> Lderef ne, t
        | _ -> Log.fatal_exn "not a valid pointer type"
      end
  | Lfield (lv, f) ->
      let nlv = type_tlval m lv in
      let tfield =
        let tstruct =
          match snd nlv with
          | Tstruct p -> p
          | _ -> Log.fatal_exn "not a valid struct type" in
        find_field tstruct f.f_name in
      Lfield (nlv, f), tfield.tsf_typ
  | Lindex (lv, e) ->
      let nlv = type_tlval m lv and ne = type_texpr m e in
      begin
        match snd nlv, snd ne with
        | Tptr (Some t), Tint _ -> Lindex (nlv, ne), t
        | _, _ ->  Log.fatal_exn "type Log.fatal_exn on array dereference"
      end


(** Binding *)
let bind_var (f: string -> int) (v: var): var =
  try { v with v_id = f v.v_name }
  with Not_found -> Log.fatal_exn "variable %a does not exist" var_fpr v
let bind_texpr (f: string -> int) = map_texpr (bind_var f)
let bind_tlval (f: string -> int) = map_tlval (bind_var f)


(** Extraction of condition trees *)
(* Simply pulls condition operators at the top into another ast.
 * This will make treatment of the guard operator easier *)
let rec extract_tree (e: 'a texpr): 'a condtree =
  match fst e with
  | Ebin ((Beq | Bne), (Erand, _), _) | Ebin ((Beq | Bne), _, (Erand, _))
  | Erand -> Ctrand
  | Ebin (Band, e0, e1) -> Ctland (extract_tree e0, extract_tree e1)
  | Ebin (Bor, e0, e1)  -> Ctlor  (extract_tree e0, extract_tree e1)
  | _ -> Ctleaf e
