typedef struct elt {
  struct elt* next;
  int data;
} elt;

typedef elt* list;

void f( list l ) {
  int i = 0;
  while(l != 0){
    l->data = i++;
    l = l->next;
  }
}
