%--------------------------------------------------------------------
% Brief general description of the structure of the analyzer
%--------------------------------------------------------------------
\chapter{Introduction to the \memcad analyzer and basic usage}

\section{Informal presentation}
\label{s:2:1:informal}
\memcad stands for {\bf Mem}ory {\bf C}omposite {\bf A}bstract {\bf D}omain.
It is an experimental tool for program verification, that focuses mainly
on memory properties.

\memcad is a static analyzer for C programs, specialized in the inference
and verification of memory properties.
It performs a forward analysis by abstract interpretation.
More precisely, it starts from abstract pre-conditions and computes abstract
post-conditions.
Unless specified otherwise, the pre-condition assumes an empty memory state
(no variable is defined)
The analysis is designed to be sound, which means that the invariants it
computes should over-approximate the concrete behaviors of the analyzed
program
(though, note that \memcad is offered with no guarantee, even though the
design intent is to ensure its soundness ---\eg, the analyzer may not be
free of implementation bugs).
For instance, when a new local variable is defined and before it is
uninitialized, it is assumed to hold any possible value.
To allow for the verification of a code fragment under some assumption
(\eg, that a function is called in a state where some structure are
already built and stored in memory), the analyzer features specific
assumption commands.

During the analysis, \memcad attempts to prove the absence of memory
errors and assertion violations.
In particular, it attempts to prove:
\begin{itemize}
\item the absence of memory errors, such as the dereference of null or
  invalid pointers;
\item the absence of accesses (read or write) outside the bounds of
  memory blocks (that includes array out of bound errors);
\item that regular assertions are satisfied;
\item that structural assertions (that are described in a specific
  language) are also satisfied, which means that certain structural
  invariants hold.
\end{itemize}
The main use of \memcad consists in verifying the preservation of
structural assertions such as the preservation of well-formed dynamic
structures such as lists, trees, possibly nested into arrays.

The analysis is conservative and may fail to prove properties that hold
in all concrete executions.
Messages reporting the analyzer cannot prove the correctness of some
operations do not necessarily mean that there exists a concrete execution
where they do not hold, or even that the analyzer cannot prove them under
different settings.
In many cases, they stem for imprecision in the analysis, that could either
be solved using other settings, or require some deeper refinements of the
abstract interpreter implemenation (addition of an abstract domain,
refinement of abstract transfer functions and analysis algorithms).
In the current version of the \memcad implementation, shape properties
(well formedness of structures with respect to purely pointer properties)
and basic numerical properties (linear constraints) can be specified and
are handled rather precisely.
Also, set abstraction allow to verify the preservation of shared
data structures.
The analysis handles basic relations between pointer properties, numerical
properties and sharing properties, but cannot handle precisely all such
properties (the current version is not able to generalize numerical
invariants on red-black trees, for instance).

The abstract domain of \memcad is highly parametric and modular.
The analyzer relies on a library of abstract domains that can often
be enabled or disabled independently.
Some domains require some parameterization, which can be performed
either automatically or manually.

The \memcad analyzer inputs C programs, and optional user-supplied
description of data-structures of interest.
The purpose of the data-structure descriptions is to parameterize the
abstract domain of the analyzer.
The data-structure descriptions can be inferred automatically when the
structures involve no sharing or pointer relations (as in doubly-linked
lists or trees with parent pointers).
In the other hand, basic structures such as singly-linked trees or
binary trees can be inferred from their type definitions.
Not all C features are supported and the programs to analyze must be
compilable by the clang compiler.
Moreover, \memcad does not aim at handling all features of the C
programming language.
The following C features are not currently supported by \memcad,
although support of several of these could be added easily (the
main purpose of \memcad is to design, implement and
assess memory abstractions, not numerical abstractions):
\begin{itemize}
\item floating point variables;
\item inlined assembly code;
\item recursive functions;
\item function pointers;
\item pointers used as arrays (though core of the analyzer could
  handle this feature well, the frontend and typing phase currently
  do not).
\end{itemize}

In the following sections, we illustrate basic use of the \memcad
static analyzer.

\section{Use of \memcad to verify basic properties~/~programs}
\label{s:2:use-example}
In this section, we consider a very simple program and show how
the \memcad analyzer can be invoked.
We consider a basic singly-linked list data-type and a function that
traverses an instance of that type so as to perform an in-place
modification to the data stored in each element of its argument, all
stored in a C file \sfile{list.c}:
\begin{center}
  \lstinputlisting[style=c]{code-list-raw.c}
\end{center}
Assuming the argument of function \lstinline{f} is a valid pointer to a
well-formed list, the execution of \lstinline{f} will cause no runtime
error and the list structure will be preserved.
We study several ways to invoke \memcad so as to verify these properties.

\subsection{Automatic inductive predicate generation and harness
  annotation}
\label{s:2:2:1:aind}
The first way is based on:
\begin{itemize}
\item an inductive predicates synthesized by \memcad in order to describe
  the list data-type;
\item the addition of meta-commands to specify the intended pre- and
  post-conditions.
\end{itemize}
To use this approach, we first need to update the source code with the
meta commands, in the fille ``\sfile{list-v.c}'' (note that we use a
slightly different name to emphasize the fact that this file is meant
only to be passed to the analyzer) below:
\begin{center}
  \lstinputlisting[style=c]{code-list-mccom.c}
\end{center}
First, let us note that the meta-command \lstinline{_memcad} is used to
control the analysis, and state assumptions and proof goals using a
meta-syntax specific to the analyzer.
Two instances of this command are used here:
\begin{itemize}
\item the first one consists of the meta-command \lstinline{add_inductive}
  and requires the analyzer to assume at the point where it appears the
  value stored in variable \lstinline{l} satisfies some inductive predicate
  in separation logic named \lstinline{elt} (this predicate describes a
  structure that can either be inferred from type definitions or set
  manually; more details on this predicate will be provided below);
\item the second one consists of the meta-command \lstinline{check_inductive}
  and requires the analysis to prove that the value stored in variable
  \lstinline{l} still satisfies the inductive predicate named \lstinline{elt}.
\end{itemize}
Essentially, those two commands are used here so as to specify an abstract
pre-condition (to assume) and an abstract post-condition (to prove).
The decoration of the source code with these two meta-command is called
a \emph{harness} since it adds a logical environment \emph{around} the
fragment of code under consideration.
\memcad provides a wide range of meta-commands to achieve a number of
tasks such as specifying assumptions or analysis goals, and to provide
the analysis with hints or to require for it to log specific pieces
of information.
The definition of meta-commands is described in \sref{3:2:commands}.

Then, the analysis is launched by the command below:
\begin{verbatim}
./analyze -a -auto-ind list-v.c
\end{verbatim}
The \soption{-a} option instructs the executable to perform the analysis.
The \soption{-auto-ind} option activates automatic inference of the set
of inductive definitions to be used for the analysis from the type
definitions in file \sfile{list-v.c}.
This generates one inductive definition \lstinline{elt} to be used as a
parameter of the abstract domain before actually launching analysis.
Thus, the execution is split into two phases:
\begin{enumerate}
\item collection of inductive definitions computed from the type
  definitions (under the assumption that no sharing is used), and
  instantiation of the abstract domain;
  essentially, this phase computes an inductive definition in separation
  logic of the form below:
  \[
  \begin{array}{lccl}
    \icallpz{\alpha}{\indelt}
    & ::= & & (\emp, \alpha = 0) \\
    & & \logor &
    (\alpha \cdot \fldnext \relpt{} \beta_0
    \lsep \alpha \cdot \flddata \relpt{} \beta_1 \\
    & & & \lsep \icallpz{\beta_0}{\indelt}, \alpha \not= 0) \\
  \end{array}
  \]
  (internally, the analysis takes the memory layout into account
  and utilizes numeric offsets rather than field names)
\item forward abstract interpretation of the program using this
  inductive definition as a parameter of the abstract domain, and
  starting from the entry point of the program with an empty memory
  state (no variable, and no heap space);
  this phase over-approximates soundly each variable definition and
  program statement;
  while doing so, the \lstinline{_memcad} meta-commands respectively
  lets the analysis make a memory assumption (which would be satisfied
  if replaced by any code fragment that onstructs a valid structure,
  that can be described by \lstinline{elt}), and requires it to verify
  a memory property.
\end{enumerate}
Note that an automatically generated inductive definition is named
after the name of the struct it is derived from.
In our example, the struct is named \lstinline{elt}, so
\lstinline{elt} must be used as the inductive name in both the
\lstinline{add_inductive} and in the \lstinline{check_inductive}
\memcad commands.

The analyzer output will end like this:
\begin{verbatim}
[...]
Final status report (0.1901 sec spent)
- assertions proved:     1 (context)
- assertions not proved: 0 (context)
\end{verbatim}
If the count on the line 'assertions not proved' is non zero, it means
the program could not be verified (\ie, at least one memory assertion
could not be proved).

The automatically generated inductive definition can be found in the file
named ``\sfile{auto.ind}''.
The syntax of inductive definition files is described in details
in section \sref{3:3:ind}.

\subsection{Manually provided inductive predicate and harness annotation}
\label{s:2:2:2:mind}
While the previous method is more automatic, relying on the analyzer to
synthesize inductive predicate has several drawbacks.
First, some C data-types are compatible with several distinct inductive
predicates; in such cases, the automatically generated inductive predicate
may not be the one the user would intend to use.
While the analysis will always yield sound results, it could be source of
improper understanding of the analysis output.
Second, this technique is of more limited scope than the capability of the
tool in terms of expressiveness, and some more advanced inductive predicates
will not be inferred at all.

With this in mind, we propose a second technique where the inductive
predicate is provided manually.
The first step consists in expressing the inductive definition provided
below in separation logic inside a language provided by \memcad:
\[
\begin{array}{lccl}
  \icallpz{\alpha}{\indelt}
  & ::= & & (\emp, \alpha = 0) \\
  & & \logor &
  (\alpha \cdot \fldnext \relpt{} \beta_0
  \lsep \alpha \cdot \flddata \relpt{} \beta_1 \\
  & & & \lsep \icallpz{\beta_0}{\indelt}, \alpha \not= 0) \\
\end{array}
\]
In the following, we assume the following description is put in a
text file ``\sfile{elt.ind}'':
\begin{verbatim}
elt<0,0> :=
| [0] -   emp
      -   this = 0
| [2 addr int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.elt( )
      -   this != 0
        & alloc( this, 8 )
.
\end{verbatim}
This file expresses that a pointer to a memory region that can be
summarized by the abstract predicate \lstinline{elt} is either a null
pointer together with an empty region, or a non-null pointer to a
structure with two fields at numerical offsets 0 and 4, the first
of which points to another memory region that can also be described
by the same predicate.
The syntax of inductive definition files is described in details
in section \sref{3:3:ind}.

Then, the analysis is launched by the command below:
\begin{verbatim}
./analyze -a -use-ind elt.ind list-v.c
\end{verbatim}
The C file annotation does not change, but this time the analyzer
does not attempt to come up with a definition for the structure
that is pointed to by variable \lstinline{elt}.
The rest of the analysis is also entirely similar to the analysis
described in \sref{2:2:1:aind}.

\subsection{Invocation of \memcad using an external specification file}
\label{s:2:2:3:runspec}
The two previous sections require some annotation in the source code
and a slight modification of the initial program, so as to add a
so-called \emph{harness function}.
The use of such a harness is not always desirable as it requires to
modify the source code.
It is also harder to use when considering more complex properties.
Moreover, it is common to try to verify a large number of functions
with very similar context and proof objectives and harnesses may not
be most convenient in such cases.

The alternate approach is to provide the analysis with a separate
description of analysis goals written in a domain specific language
provided by \memcad and that extends the syntax of inductive
predicates.
We now show the application of this approach to the code shown at
the beginning of this section.

We assume the file ``\sfile{list.spec}'' below:
\begin{verbatim}
elt<0,0> :=
| [0] -   emp
      -   this = 0
| [2 addr int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.elt( )
      -   this != 0
        & alloc( this, 8 )
.
$form H :=
    l :: a
  * a |-> p
  * p.elt()
.
$goal funcf {
  $form( H )
} f( l ) {
  $form( H )
}
.
\end{verbatim}
First, we remark that the file includes the inductive predicate
that was considered in \sref{2:2:2:mind}.
This means that we will not pass to the analyzer an inductive
predicate definition file anymore.
The second entry in the file is a definition for a separation logic
formula called \lstinline{H}, that corresponds to a single variable
\lstinline{l} stored at an address \lstinline{a} and the content of
which is described by symbolic variable \lstinline{p}, together with
the assertion that \lstinline{p} is the address of a valid structure
of type \lstinline{elt}.
In math notation, this amounts the logical statement below:
\[
H ::= \exists a, p, \; a \mapsto p \lsep p.\texttt{elt} \logand
\texttt{\&l} = a
\]
Finally, the third entry specifies a goal called \lstinline{funcf},
that consists of a Hoare style triple with pre-condition \lstinline{H},
body \lstinline{f( l )} (a C function call to \lstinline{f} with
argument \lstinline{l}), and post-condition \lstinline{H}.
Note that, in that case, we do not need to make any modification to
the source file so the analyzer will be invoked on the initial
\sfile{list.c}, and not on the modified version.

Then, the analysis is launched by the command below:
\begin{verbatim}
./analyze -a -spec list.spec list.c
\end{verbatim}

The results of the analysis are entirely similar to the previous sections.
While this approach does not seem any lighterweight at first, it pays off
when considering larger examples.
Let us consider the case of a library made of, say, 20 functions that all
operate on the same structure and need to be verified one by one.
The syntax of the specification files allows to factorize large part of
the pre- and post-conditions.

Still, at this point, it requires more expertise, since the specification
domain specific language is based on a syntax close to that of separation
logic.

The syntax of inductive definition files is described in details
in section \sref{3:4:spec}.


\section{How to}
We now show a couple of additional examples of analyses, and show how
it can be carried out either with very general or with very specific
pre-conditions.
In the following of this section, we consider a piece of code taken from
an AVL tree library, that consists of an insertion function and a call
to it:
\begin{center}
  \lstinputlisting[style=c]{code-avl-raw.c}
\end{center}
In the following, we discuss how to verify some properties on this
program, related to the absence of memory errors and to the preservation
of structural invariants.
We assume that the definition of the structural properties of interest
for AVL trees is provided in an inductive predicate \lstinline{avltree}
that we do not discuss here.

\subsection{Analyze a program in the abstract}
The main use for the \memcad analyzer is to verify that all executions
of the insertion function satisfy the property of interest.
To achieve that, we build a fake \lstinline{main} function (usually called
\emph{harness}) that assumes any instance of a correct AVL tree is
available, calls the insertion function on it and asserts that the
result should also be a correct AVL tree.
The code below shows this pattern.
The following C code triggers shape analysis of an AVL tree library.
The code starts from a valid AVL tree, inserts one random integer
into it then proves that the AVL tree is still valid after
the previous insertion (of any possible integer value).
While such code compiles, it is not meant to be executed.
Such code is used by \memcad to certify that
the considered AVL tree insert function implementation is correct
(only sortedness is not verified).

Example test program to analyze; with an abstract
execution flow (all possible execution flows will be considered):
\begin{center}
  \lstinputlisting[style=c]{code-avl-abs.c}
\end{center}

Alternatively, it is possible to start with the raw C file (without
any annotation) and to use a \emph{specification file} instead, that
contains the definition of the pre-condition to assume and of the
post-condition to verify.

\subsection{Analyze a program in the concrete}
The following code tests a unique execution path using an AVL tree library.
The integer one is inserted into an empty tree, then it is found back,
then it is removed and cannot be found back anymore.
Such code compiles, can be executed and dynamically tested even
with valgrind.
Such code ensures that we can write simple example programs using the
data structure library we want to analyze (and that we are not experts at).
Keep in mind that the assert statements are proof goals that \memcad
will try to prove.

Example test program to analyze, with a single concrete
execution flow:
%\input{code-avl-conc.tex}
\begin{center}
  \lstinputlisting[style=c]{code-avl-conc.c}
\end{center}
