#!/bin/bash

#set -x # debug

# clean all previous logs
find minix/ -name *.log -exec rm -f {} \;

# run on minix
\rm -f minix_logs minix_errors # cleanup
for f in `find minix/ -name '*.c'` ; do
    (./analyze -header for_minix.h -very-silent -nd-box -a -no-latex $f \
        2>&1) >> minix_logs
done

# classify errors
grep Fatal minix_logs | sed 's/at line .*$//g' | sort | uniq -c | sort -r -n -k1 > minix_errors
