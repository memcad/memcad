(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: spec_utils.ml
 **       utilities for analysis goal specifications.
 ** Xavier Rival, 2023/06/04 *)
open Data_structures
open Sv_def

open Ast_sig
open Dom_sig
open Ind_form_sig
open Spec_sig
open Vd_sig


(** Printers *)
(* Spec functions *)
val s_sval_fpr: form -> s_sval -> unit
val s_pexpr_fpr: form -> s_pexpr -> unit
val s_comp_fpr: form -> s_comp -> unit
val s_pterm_fpr:   form -> s_pterm -> unit
val s_pform_fpr:   form -> s_pform -> unit
val s_henv_fpr:    form -> s_henv -> unit
val s_hterm_fpr:   form -> s_hterm -> unit
val s_hform_fpr:   form -> s_hform -> unit
val s_formula_fpr: form -> s_formula -> unit
val s_col_params_fpr: form -> s_col_params -> unit
val proc_call_fpr: form -> proc_call -> unit
val s_goal_fpr:    form -> s_goal -> unit
val p_iline_fpr: form -> p_iline -> unit
module Parsed_spec_fpr:
    sig
      val s_pexpr_fpr: form -> Parsed_spec.s_pexpr -> unit
      val s_pterm_fpr: form -> Parsed_spec.s_pterm -> unit
      val s_pform_fpr: form -> Parsed_spec.s_pform -> unit
      val s_hterm_fpr: form -> Parsed_spec.s_hterm -> unit
      val s_hform_fpr: form -> Parsed_spec.s_hform -> unit
      val s_formula_fpr: form -> Parsed_spec.s_formula -> unit
    end


(** Auxiliary function to construct abstract states *)
(* Construction of constraints *)
val build_meml_cons:
    sv StringMap.t -> (sv * col_kind * colv_info option) StringMap.t
      -> s_pterm -> meml_cons


(** Elaboration *)
val elaborate_offset: Parsed_spec.c_type_spec -> Parsed_spec.offset
  -> Offs.t
val elaborate_cell: Parsed_spec.c_type_spec -> Parsed_spec.cell
  -> Ind_sig.cell
val elaborate_heapatom: Parsed_spec.c_type_spec -> Parsed_spec.heapatom
  -> Ind_sig.heapatom
val elaborate_hform: Parsed_spec.c_type_spec -> Parsed_spec.hform
  -> Ind_sig.hform
val elaborate_pathstr: Parsed_spec.c_type_spec -> Parsed_spec.pathstr
  -> Ind_sig.pathstr
val elaborate_pformatom: Parsed_spec.c_type_spec -> Parsed_spec.pformatom
  -> Ind_sig.pformatom
