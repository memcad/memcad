(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_utils.ml
 **       basic operations on inductive definitions
 ** Xavier Rival, 2011/06/30 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open Set_sig
open Seq_sig
open Spec_sig

open Ast_utils
open Ind_form_utils
open Spec_utils
open Sv_utils

open Ind_inst
open FInd


(** Error report *)
module Log =
  Logger.Make(struct let section = "i_uts___" and level = Log_level.DEBUG end)


(** Exceptions used in the analyses, to quickly return results *)
exception A_Success
exception A_Failure


(** Pretty-printing *)

(* Heap formulas *)
let cell_fpr      = FInd.cell_fpr
let heapatom_fpr  = FInd.heapatom_fpr
let hform_fpr     = FInd.hform_fpr
let apath_fpr     = FInd.apath_fpr
let pformatom_fpr = FInd.pformatom_fpr
let pform_fpr     = FInd.pform_fpr

(* Rules *)
let ir_kind_fpr (fmt: form): ir_kind -> unit = function
  | Ik_unk -> F.fprintf fmt "unknown"
  | Ik_empz -> F.fprintf fmt "{emp,0}"
  | Ik_range (r0, r1) -> F.fprintf fmt "[%d...%d[" r0 r1
let irule_fpri (ind: string) (fmt: form) (r: irule): unit =
  let typs =
    SvMap.fold
      (fun i typ acc ->
        F.asprintf "%s %a" acc ntyp_fpr typ
      ) r.ir_typ "" in
  let nind = ind^"        " in
  F.fprintf fmt "%s| [%d%s]\n%s- %a\n%s- %a\n" ind
    r.ir_num typs nind FInd.hform_fpr r.ir_heap nind FInd.pform_fpr r.ir_pure

(* Parameter types *)
let int_par_rec_fpr (fmt: form) (p: int_par_rec): unit =
  let str =
    match p.ipt_const, p.ipt_add, p.ipt_incr, p.ipt_decr with
    | true , _    , _    , _     -> "constant"
    | false, true , _    , _     -> "additive"
    | false, false, true , _     -> "increasing"
    | false, false, false, true  -> "decreasing"
    | false, false, false, false -> "?????" in
  F.fprintf fmt "%s" str
let pars_rec_fpri (ind: string) (fmt: form) (p: pars_rec): unit =
  IntMap.iter
    (fun i b ->
      F.fprintf fmt "%s - ptr @%d: %s\n" ind i (if b then "constant" else "_")
    ) p.pr_ptr_const;
  IntMap.iter
    (fun i r ->
      F.fprintf fmt "%s - int @%d: %a\n" ind i int_par_rec_fpr r
    ) p.pr_int;
  IntMap.iter
    (fun i r ->
      F.fprintf fmt "%s - set @%d: %a\n" ind i Set_utils.set_par_type_fpr r
    ) p.pr_set;
  IntMap.iter
    (fun i r ->
      F.fprintf fmt "%s - seq @%d: %a\n" ind i Seq_utils.seq_par_type_fpr r
    ) p.pr_seq

(* Inductive definitions *)
let ind_properties_fpr (ind: string) (fmt: form) (i: ind): unit =
  let nind = "  "^ind in
  (* Printing the analysis results *)
  F.fprintf fmt "%sRules:" ind;
  List.iteri
    (fun i ir ->
      F.fprintf fmt "\n%srule %d, ptr: {%a}, int: {%a}, set: {%a} => %b" nind i
        (IntSet.t_fpr ", ") ir.ir_uptr (IntSet.t_fpr ", ") ir.ir_uint
        (IntSet.t_fpr ", ") ir.ir_uset ir.ir_unone
    ) i.i_rules;
  F.fprintf fmt "%s%a\n" ind (gen_list_fpr "" FInd.apath_fpr ";") i.i_ppath;
  if i.i_rules_cons != [] then
    begin
      F.fprintf fmt "%sI_rules_cons:\n" ind;
      gen_list_fpr ""
        (fun fmt (a, b, c, d) ->
          F.fprintf fmt "%s%a, => (%a, %a, %a)" nind aform_fpr a
            (gen_list_fpr "" aform_fpr ";") b
            (gen_list_fpr "" sform_fpr ";") c
            (gen_list_fpr "" qform_fpr ";") d
        ) "\n" fmt i.i_rules_cons;
      F.fprintf fmt "\n"
    end;
  (* - parameter types *)
  if i.i_ppars > 0 || i.i_ipars > 0 || i.i_spars > 0 || i.i_qpars > 0 then
    begin
      let f_int mpr =
        for k = 0 to i.i_ipars - 1 do
          let pr =
            try F.asprintf "%a" int_par_rec_fpr (IntMap.find k mpr)
            with Not_found -> "???" in
          F.fprintf fmt "%s - int-par %d: %s\n" nind k pr
        done in
      F.fprintf fmt "%sParameter types for all rules:\n" ind;
      f_int i.i_pkind.pr_int;
      F.fprintf fmt "%sParameter types for emp rules:\n" ind;
      f_int i.i_pkind_emp.pr_int;
    end;
  F.fprintf fmt "%sBlock size info: %s\n" ind
    (Option.fold ~none:"<none>" ~some:(fun i -> F.asprintf "<%d bytes>" i)
       i.i_blksize);
  F.fprintf fmt "%sParameters that may trigger non local unfolding: [%a]" ind
    (gen_list_fpr ""
       (fun fmt (i,ck) -> F.fprintf fmt "%a@%d" col_kind_fpr ck i) ",")
    i.i_nonloc_unf_pars
let ind_fpri ~(properties: bool) (ind: string) (fmt: form) (i: ind): unit =
  F.fprintf fmt "%s%s<ptr:%d,int:%d,set:%d,seq:%d> :=\n%a\n"
    ind i.i_name i.i_ppars i.i_ipars i.i_spars i.i_qpars
    (gen_list_fpr "" (irule_fpri ("    "^ind)) "") i.i_rules;
  if properties then ind_properties_fpr ("  "^ind) fmt i

(* Save rules to a file *)
let rules_fpr (fmt: form) (i: ind): unit =
  let name =
    if i.i_name <> "" && String.get i.i_name 0 = '%' then
      (* names introduced by the C frontend (they start with '%')
       * are not valid ind. def. names *)
      String.pop i.i_name
    else i.i_name in
  F.fprintf fmt "%s<%d,%d> :=\n%a" name i.i_ppars i.i_ipars
    (gen_list_fpr "" (irule_fpri "  ") "") i.i_rules
let rules_to_file (out_file: string) (il: ind list): unit =
  with_out_file out_file
    (fun out ->
      let fmt = F.formatter_of_out_channel out in
      List.iter
        (fun i ->
          F.fprintf fmt "%a.\n" rules_fpr i
        ) il;
    )

(** Modules for the management of symbols *)

module MS =
  struct
    (* Management of symbols *)
    module Ps = Parsed_spec
    (** A macro may be parametrised by collection variables.
     *  The list corresponds to arguments (with type and name) *)
    type 'a macro = (col_kind * string) list * 'a
    type 'a par_macro = (indpar_type * string) list * 'a
    (** A dictionnary of macros *)
    type 'a mdic = 'a macro StringMap.t
    type 'a par_mdic = 'a par_macro StringMap.t
    (** A type t represents an environment  *)
    type fresh =
      | Disallow
      | Keep
      | Rename of string StringMap.t

    type t = { t_syms:  Ps.s_pexpr mdic ;
               (** dictionnary for constants macros *)
               t_margs: StringSet.t;
               (** arguments of the currently processed macro *)
               t_fresh: fresh;
               (** fresh variables *) }

    (** Printing *)
    let macro_fpr (type a) (a_fpr: form -> a -> unit) (fmt: form)
        (args, a: a macro) : unit =
      F.fprintf fmt "%a [ %a ]"
        a_fpr a
        (gen_list_fpr " "
          (fun fmt (ck, name) -> F.fprintf fmt "%a %s" col_kind_fpr ck name)
          ", ")
        args
    let mdic_fpr (type a) (a_fpr: form -> a -> unit) (fmt: form)
        (dic: a macro StringMap.t) : unit =
      if StringMap.is_empty dic then
        Format.fprintf fmt " _ "
      else
        Format.fprintf fmt "\n  %a"
          (StringMap.t_fpr "\n  " (macro_fpr a_fpr)) dic
    let fresh_fpr (fmt: form): fresh -> unit = function
      | Disallow -> F.fprintf fmt "Disallow"
      | Keep -> F.fprintf fmt "Keep"
      | Rename map ->
          (StringMap.t_fpr "\n  " string_fpr) fmt map
    let t_fpr (fmt: form) (t: t) : unit =
      Format.fprintf fmt
        "t_syms:%a\nt_margs: { %a }\nt_fresh:\n  %a"
          (mdic_fpr Parsed_spec_fpr.s_pexpr_fpr) t.t_syms
          (StringSet.t_fpr "; ") t.t_margs
          fresh_fpr t.t_fresh

    (** Empty environment *)
    let emp = { t_syms = StringMap.empty ;
                t_margs = StringSet.empty ;
                t_fresh = Disallow ; }

    let fresh_count = ref 0

    (** [get_symbs (c, l) t] returns:
     * - [None] if [c] is in [t.t_margs], [c] is an argument
     * - [Some macro] if [c -> macro] is in [t.t_syms] *)
    let get_symbs (c, l) (t: t)
        : ((col_kind * string) list * Ps.s_pexpr) option =
      if StringSet.mem c t.t_margs then
        begin
          (* this is a variable inside a symbol definition *)
          if l != [] then
            Log.fatal_exn "higher order arguments not allowed";
          None
        end
      else
        try Some (StringMap.find c t.t_syms)
        with Not_found ->
          let c' =
            match
              match t.t_fresh with
              | Disallow -> None
              | Keep -> Some c
              | Rename fresh_map -> StringMap.find_opt c fresh_map
            with
            | Some result -> result
            | None -> Log.fatal_exn "argument non defined %s" c in
          Some ([], Ps.Spe_sval c')

    (** [set_symb s e t] returns [t with t_syms{s-> e}] *)
    let set_symb s e t =
      { t with t_syms = StringMap.add s e t.t_syms }

    let prepare_params
        (l: (indpar_type * string) list)
        (d_cst: Ps.s_pexpr mdic): t =
      List.fold_left
        (fun t (_,x) ->
          { t_syms = StringMap.remove x t.t_syms ;
            t_margs = StringSet.add x t.t_margs ;
            t_fresh = t.t_fresh ; }
        ) { emp with t_syms = d_cst } l

    (** [prepare_symbs l d_cst] creates a new [t] with:
     *  - names in [l] in [t_margs]
     *  - bindings of [d_cst] in [t_syms]  *)
    let prepare_symbs
        (l: (col_kind * string) list)
        (d_cst: Ps.s_pexpr mdic): t =
      List.fold_left
        (fun t (_,x) ->
          { t_syms = StringMap.remove x t.t_syms ;
            t_margs = StringSet.add x t.t_margs ;
            t_fresh = t.t_fresh ; }
        ) { emp with t_syms = d_cst } l

    let prepare_par_apply
        (formal: (indpar_type * string) list)
        (actual: Ps.s_pexpr list): t =
      List.fold_left2
        (fun t (_,s) e ->
          { t with t_syms = StringMap.add s ([],e) t.t_syms }
        ) emp formal actual

    (** [prepare_apply formal actual] returns an environment whose bindings are
     *  [ formal_i -> actual_i ] in t_syms *)
    let prepare_apply
        (formal: (col_kind * string) list)
        (actual: Ps.s_pexpr list): t =
      List.fold_left2
        (fun t (_,s) e ->
          { t with t_syms = StringMap.add s ([],e) t.t_syms }
        ) emp formal actual
  end

let elaborate_offset = Spec_utils.elaborate_offset

let elaborate_s_off (c_type_spec: Parsed_spec.c_type_spec)
      (off: Parsed_spec.s_off): FInd.s_off =
  match off with
  | FSpec.So_int off ->
      FInd.So_int (elaborate_offset c_type_spec off)
  | FSpec.So_sval (k, sv, off) ->
      FInd.So_sval (k, sv, elaborate_offset c_type_spec off)

let elaborate_s_range (c_type_spec: Parsed_spec.c_type_spec)
      ((a, b): Parsed_spec.s_range): FInd.s_range =
  let a = elaborate_s_off c_type_spec a in
  let b = elaborate_s_off c_type_spec b in
  (a, b)


let map_s_iargs f (args : 'a s_iargs) : 'a s_iargs =
  { num = f args.num;
    ptr = f args.ptr;
    set = f args.set;
    seq = f args.seq;
  }

module SvalSet = StringSet
module SvalMap = StringMap

let collect_old_sval (s_pexpr: Parsed_spec.s_pexpr): SvalSet.t =
  match s_pexpr with
  | Spe_old_sval sval -> SvalSet.singleton sval
  | Spe_empty
  | Spe_int _
  | Spe_sval _
  | Spe_info _ -> SvalSet.empty
  | Spe_symbol _ -> assert false (* Spe_symbol unexpected in flatten_s_pexpr *)
  | Spe_set _
  | Spe_seq _
  | Spe_sort _
  | Spe_bop _ -> assert false (* Handled in collect_s_pexpr *)

let rec collect_fresh_var (e: Parsed_spec.s_pexpr): SvalSet.t =
  match e with
  | Spe_symbol (c, aargs) ->
      if aargs = [] then
        SvalSet.singleton c
      else
        SvalSet.empty
  | Spe_sval s -> SvalSet.singleton s
  | Spe_empty
  | Spe_int _
  | Spe_info _ -> SvalSet.empty
  | Spe_old_sval _
  | Spe_set _
  | Spe_seq _
  | Spe_sort _
  | Spe_bop _ -> assert false (* Handled in collect_s_pexpr *)

let rec collect_s_pexpr (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (pexpr: Parsed_spec.s_pexpr): SvalSet.t =
  match pexpr with
  | Spe_old_sval _
  | Spe_empty
  | Spe_int _
  | Spe_sval _
  | Spe_info _ -> collect_atomic pexpr
  | Spe_symbol (_c, aargs) ->
      List.fold_left SvalSet.union
        (collect_atomic pexpr)
        (List.map (collect_s_pexpr collect_atomic) aargs)
  | Spe_set set
  | Spe_seq set ->
      List.fold_left SvalSet.union SvalSet.empty
        (List.map (collect_s_pexpr collect_atomic) set)
  | Spe_sort e -> collect_s_pexpr collect_atomic e
  | Spe_bop (_, e0, e1) ->
      SvalSet.union
        (collect_s_pexpr collect_atomic e0)
        (collect_s_pexpr collect_atomic e1)

let collect_s_pterm (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (pterm: Parsed_spec.s_pterm): SvalSet.t =
  match pterm with
  | Spt_cmp (_, e0, e1) ->
      SvalSet.union
        (collect_s_pexpr collect_atomic e0)
        (collect_s_pexpr collect_atomic e1)
  | Spt_symbol _ -> assert false

let collect_s_pform (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (pform : Parsed_spec.s_pform): SvalSet.t =
  List.fold_left SvalSet.union SvalSet.empty
    (List.map (collect_s_pterm collect_atomic) pform)

let collect_s_sval (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (sv : s_sval): SvalSet.t =
  collect_atomic (Spe_sval sv)

let collect_s_off (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (off : Parsed_spec.s_off): SvalSet.t =
  match off with
  | So_int _ -> SvalSet.empty
  | So_sval (_k, sv, _o) -> collect_s_sval collect_atomic sv

let collect_s_range (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (s_range : Parsed_spec.s_range): SvalSet.t =
  let (base, size) = s_range in
  SvalSet.union
    (collect_s_off collect_atomic base)
    (collect_s_off collect_atomic size)

let collect_s_iargs (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (s_iargs : s_sval s_iargs): SvalSet.t =
  List.fold_left SvalSet.union SvalSet.empty
    (List.map (List.fold_left SvalSet.union SvalSet.empty)
       (List.map (List.map (collect_s_sval collect_atomic))
          [s_iargs.num; s_iargs.ptr; s_iargs.set; s_iargs.seq]))

let collect_s_icall (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (s_icall : s_sval * s_sval s_iargs): SvalSet.t =
  let (sv, a) = s_icall in
  SvalSet.union (collect_s_sval collect_atomic sv)
    (collect_s_iargs collect_atomic a)

let rec collect_s_hterm (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (s_hterm : Parsed_spec.s_hterm): SvalSet.t =
  match s_hterm with
  | Sht_fsymbol (_symb, l)
  | Sht_hsymbol (_symb, l) ->
      List.fold_left SvalSet.union SvalSet.empty
        (List.map (collect_s_pexpr collect_atomic) l)
  | Sht_cell (sv0, r0, sv1, r1) ->
      List.fold_left SvalSet.union SvalSet.empty
        [ collect_atomic (Spe_sval sv0);
          collect_s_range collect_atomic r0;
          collect_atomic (Spe_sval sv1);
          collect_s_range collect_atomic r1 ]
  | Sht_ind (_iname, icall) ->
      collect_s_icall collect_atomic icall
  | Sht_seg (_iname, s, t) ->
      SvalSet.union (collect_s_icall collect_atomic s)
        (collect_s_icall collect_atomic t)

let collect_s_hform (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (s_pform : Parsed_spec.s_hform): SvalSet.t =
  List.fold_left SvalSet.union SvalSet.empty
    (List.map (collect_s_hterm collect_atomic) s_pform)

let string_map_keys (map: 'a StringMap.t): StringSet.t =
  StringMap.fold (fun key _value acc -> StringSet.add key acc)
    map StringSet.empty

let string_map_values (map: 'a StringMap.t): StringSet.t =
  StringMap.fold (fun _key value acc -> StringSet.add value acc)
    map StringSet.empty

let collect_s_form (collect_atomic: Parsed_spec.s_pexpr -> SvalSet.t)
    (form: s_henv * Parsed_spec.s_hform * Parsed_spec.s_pform): SvalSet.t =
  let (e, h, p) = form in
  StringSet.fold
    (fun s acc -> SvalSet.union (collect_s_sval collect_atomic s) acc)
    (string_map_values e)
    (SvalSet.union
       (collect_s_hform collect_atomic h) (collect_s_pform collect_atomic p))

let fresh_var_counter = ref 0

let make_fresh_var_index () =
  let index = !fresh_var_counter in
  fresh_var_counter := succ index;
  index

(** Translation of parsed specifications into the symbol free version *)
let p_ilines_translate (c_type_spec: Parsed_spec.c_type_spec)
    (l: p_iline list): s_goal StringMap.t =
  let module Ps     = Parsed_spec in
  let module Ps_fpr = Parsed_spec_fpr in
  let rec flatten_s_pexpr flatten_old_sval = function
    | Ps.Spe_empty -> Spe_empty
    | Ps.Spe_int i -> Spe_int i
    | Ps.Spe_sval sv -> Spe_sval sv
    | Ps.Spe_old_sval sv -> Spe_sval (flatten_old_sval sv)
    | Ps.Spe_set l -> Spe_set (List.map (flatten_s_pexpr flatten_old_sval) l)
    | Ps.Spe_seq l -> Spe_seq (List.map (flatten_s_pexpr flatten_old_sval) l)
    | Ps.Spe_bop (op, e1, e2) ->
        let e1 = flatten_s_pexpr flatten_old_sval e1 in
        let e2 = flatten_s_pexpr flatten_old_sval e2 in
        Spe_bop(op, e1, e2)
    | Ps.Spe_sort e ->
        let e = flatten_s_pexpr flatten_old_sval e in
        Spe_sort e
    | Ps.Spe_info (info, sv) -> Spe_info (info, sv)
    | Ps.Spe_symbol _ as e ->
        Log.fatal_exn "flatten_s_pexpr: %a" Ps_fpr.s_pexpr_fpr e in
  let flatten_s_pterm flatten_old_sval = function
    | Ps.Spt_cmp (c, e0, e1) ->
        let e0 = flatten_s_pexpr flatten_old_sval e0 in
        let e1 = flatten_s_pexpr flatten_old_sval e1 in
        Spt_cmp (c, e0, e1)
    | t -> Log.fatal_exn "flatten_s_pterm: %a" Ps_fpr.s_pterm_fpr t in
  let flatten_s_pform flatten_old_sval =
    List.map (flatten_s_pterm flatten_old_sval) in
  let flatten_s_arglist = List.map (fun sv -> A_sval sv) in
  let flatten_s_iargs {num; ptr; set; seq} =
    let num = flatten_s_arglist num in
    let ptr = flatten_s_arglist ptr in
    let set = flatten_s_arglist set in
    let seq = flatten_s_arglist seq in
    {num; ptr; set; seq} in
  let flatten_s_hterm = function
    | Ps.Sht_cell (s, rs, t, rt) ->
        let rs = elaborate_s_range c_type_spec rs in
        let rt = elaborate_s_range c_type_spec rt in
        Sht_cell (s, rs, t, rt)
    | Ps.Sht_ind (i, (s, sa)) -> Sht_ind (i, (s, flatten_s_iargs sa))
    | Ps.Sht_seg (i, (s, sa), (t, ta)) ->
        Sht_seg (i, (s, flatten_s_iargs sa), (t, flatten_s_iargs ta))
    | t -> Log.fatal_exn "flatten_s_hterm: %a" Ps_fpr.s_hterm_fpr t in
  let flatten_s_hform = List.map flatten_s_hterm in
  (* get names of all inductive definitions *)
  let ind_names =
    List.fold_left
      (fun acc -> function
        | Piind ind -> StringSet.add ind.name acc
        | _ -> acc
      ) StringSet.empty l in
  (* for each formula:
   * - check that the names of the inductives are known
   * - replace all symbols with their definitions (names should only depend
   *   on symbols introduced before their location *)
  let d_cst, d_env, d_heap, d_pure, d_form, goals =
    let map_get msg x m =
      try StringMap.find x m
      with
      | Not_found ->
          Log.fatal_exn "Spec formula translation, not found: %s" msg in
    (* Global iteration *)
    List.fold_left
      (fun (((d_cst:  Ps.s_pexpr MS.mdic),
             (d_env:  s_henv StringMap.t),
             (d_heap: Ps.s_hform MS.mdic),
             (d_pure: Ps.s_pform MS.mdic),
             (d_form: (s_henv * Ps.s_hform * Ps.s_pform) MS.par_mdic),
             goals) as acc) il ->
        (* checks correct definition of inductive definition *)
        let check_ind (iname: string): unit =
          if not (StringSet.mem iname ind_names) then
            Log.fatal_exn "Inductive <%s> undefined" iname in
        (* translation of an expression *)
        let rec do_s_pexpr (d_symbs: MS.t) (e: Ps.s_pexpr): Ps.s_pexpr =
          match e with
          | Ps.Spe_empty | Ps.Spe_int _ -> e
          | Ps.Spe_symbol (c, aargs) ->
              begin
                match MS.get_symbs (c, aargs) d_symbs with
                | None -> e
                | Some (_, _) -> Log.todo_exn "do binding"
              end
          | Ps.Spe_set l -> Ps.Spe_set (List.map (do_s_pexpr d_symbs) l)
          | Ps.Spe_seq l -> Ps.Spe_seq (List.map (do_s_pexpr d_symbs) l)
          | Ps.Spe_sort e ->
              let e = do_s_pexpr d_symbs e in
              Ps.Spe_sort e
          | Ps.Spe_sval sv ->
              begin
                match MS.get_symbs (sv, []) d_symbs with
                | None -> e
                | Some (_, e) -> e
              end
          | Ps.Spe_old_sval sv ->
              begin
                match MS.get_symbs (sv, []) d_symbs with
                | None -> e
                | Some (_, e) ->
                    match e with
                    | Ps.Spe_sval sv -> Ps.Spe_old_sval sv
                    | _ -> Log.fatal_exn "<%s> is not bound to a variable" sv
              end
          | Ps.Spe_info (info, sv) ->
              begin
                match MS.get_symbs (sv, []) d_symbs with
                | None -> e
                | Some (_, Spe_sval sv) -> Ps.Spe_info (info, sv)
                | Some (_, _) ->
                    Log.fatal_exn
                      "Unexpected non-sv expression in info translation"
              end
          | Ps.Spe_bop (o, e0, e1) ->
              Ps.Spe_bop (o, do_s_pexpr d_symbs e0, do_s_pexpr d_symbs e1)
        (* passing arguments for macros *)
        and do_s_par_args symb fargs (aargs: Ps.s_pexpr list)
            (d_symbs: MS.t): MS.t =
          let nf = List.length fargs and na = List.length aargs in
          if nf != na then
            Log.fatal_exn "argument length mismatch %s (%d-%d)" symb nf na;
          let aargs = List.map (do_s_pexpr d_symbs) aargs in
          MS.prepare_par_apply fargs aargs
        and do_s_args symb fargs (aargs: Ps.s_pexpr list)
            (d_symbs: MS.t): MS.t =
          let nf = List.length fargs and na = List.length aargs in
          if nf != na then
            Log.fatal_exn "argument length mismatch %s (%d-%d)" symb nf na;
          let aargs = List.map (do_s_pexpr d_symbs) aargs in
          MS.prepare_apply fargs aargs in
        (* translation of a pure formula term *)
        let rec do_s_pterm d_symbs (t: Ps.s_pterm): Ps.s_pform =
          match t with
          | Ps.Spt_symbol (symb, l) ->
              let args, term = map_get "p_term" symb d_pure in
              let d_symbs = do_s_args symb args l d_symbs in
              List.concat_map (do_s_pterm d_symbs) term
          | Ps.Spt_cmp (c, e0, e1) ->
              [ Ps.Spt_cmp (c,do_s_pexpr d_symbs e0,do_s_pexpr d_symbs e1) ] in
        (* translation of a pure formula *)
        let do_s_pform d_symbs : Ps.s_pform -> Ps.s_pform =
          List.concat_map (do_s_pterm d_symbs) in
        let do_s_sval d_symbs (sv: s_sval): s_sval =
          match MS.get_symbs (sv, []) d_symbs with
          | None ->
             sv
          | Some (l, e) ->
              assert (l = []);
              match e with
              | Ps.Spe_sval sv -> sv
              | _ -> Log.fatal_exn "<%s> is not bound to a variable" sv in
        (* translation of an environment into a map (fully translated) *)
        let instantiate_s_henv d_symbs (m: s_henv): s_henv =
          StringMap.fold
            (fun k v acc ->
              let v = do_s_sval d_symbs v in
              StringMap.add k v acc)
            m StringMap.empty in
        let do_s_henv: Ps.s_henv -> s_henv = function
          | Ps.Se_emp -> StringMap.empty
          | Ps.Se_symbols symbs ->
              List.fold_left
                (fun acc symb ->
                  let m = map_get "s_henv" symb d_env in
                  StringMap.fold StringMap.add m acc
                ) StringMap.empty symbs
          | Ps.Se_map m -> m in
        (* does the conjunction of two formulas *)
        let merge_formulas (ae, ah, ap) (ne, nh, np) =
          let e = StringMap.fold StringMap.add ne ae in
          let h = ah @ nh in
          let p = ap @ np in
          e, h, p in
        (* the following three functions produce a triple with
         * a fully translated environment and partly translated
         * lists of heap and pure predicates *)
        let rec do_s_hterm d_symbs
            : Ps.s_hterm -> s_henv * Ps.s_hform * Ps.s_pform = function
          | Ps.Sht_fsymbol (symb, l) ->
              let args, (e, h, p) = map_get "s_fsymbol" symb d_form in
              let d_symbs = do_s_par_args symb args l d_symbs in
              let fresh_var_set =
                SvalSet.diff
                  (collect_s_form collect_fresh_var (e, h, p))
                  (SvalSet.union
                     d_symbs.t_margs
                     (string_map_keys d_symbs.t_syms)) in
              let fresh_rename_map =
                SvalSet.fold
                  (fun var accu ->
                     let index = make_fresh_var_index () in
                     let fresh_var = Format.sprintf "%s.%d" var index in
                     StringMap.add var fresh_var accu)
                  fresh_var_set StringMap.empty in
              let d_symbs = { d_symbs with t_fresh = Rename fresh_rename_map } in
              let e = instantiate_s_henv d_symbs e in
              let f = do_s_hform d_symbs h in
              merge_formulas (e, [ ], do_s_pform d_symbs p) f
          | Ps.Sht_hsymbol (symb, l) ->
              let args, h = map_get "s_hterm" symb d_heap in
              let d_symbs = do_s_args symb args l d_symbs in
              do_s_hform d_symbs h
          | Ps.Sht_cell (sv0,r0,sv1,r1) ->
              let sv0 = do_s_sval d_symbs sv0 in
              let r0 = do_s_range d_symbs r0 in
              let sv1 = do_s_sval d_symbs sv1 in
              let r1 = do_s_range d_symbs r1 in
              StringMap.empty, [ Ps.Sht_cell (sv0,r0,sv1,r1) ], [ ]
          | Ps.Sht_ind (iname, (sv, a)) ->
              check_ind iname;
              let sv = do_s_sval d_symbs sv in
              let a = map_s_iargs (List.map (do_s_sval d_symbs)) a in
              StringMap.empty, [ Ps.Sht_ind (iname, (sv, a)) ], [ ]
          | Ps.Sht_seg (iname,(svs,sa),(svt,ta)) ->
              check_ind iname;
              let svs = do_s_sval d_symbs svs in
              let svt = do_s_sval d_symbs svt in
              let sa = map_s_iargs (List.map (do_s_sval d_symbs)) sa
              and ta = map_s_iargs (List.map (do_s_sval d_symbs)) ta in
              StringMap.empty, [ Ps.Sht_seg (iname,(svs,sa),(svt,ta)) ], [ ]
        and do_s_range d_symbs ((b, s): Ps.s_range): Ps.s_range =
          (do_s_off d_symbs b, do_s_off d_symbs s)
        and do_s_off d_symbs (off: Ps.s_off): Ps.s_off =
          match off with
          | So_int _ -> off
          | So_sval (k, sv, o) -> So_sval (k, do_s_sval d_symbs sv, o)
        and do_s_hform d_symbs (hf: Ps.s_hform)
            : s_henv * Ps.s_hform * Ps.s_pform =
          List.fold_left
            (fun acc ht -> merge_formulas acc (do_s_hterm d_symbs ht))
            (StringMap.empty, [ ], [ ]) hf in
        let do_s_formula d_symbs
            : Ps.s_formula -> s_henv * Ps.s_hform * Ps.s_pform = function
          | Ps.Sf_extended (se, sh, sp) ->
              merge_formulas (do_s_hform d_symbs sh)
                (do_s_henv se, [ ], do_s_pform d_symbs sp) in
        match il with
        | Piind _ | Pibind (_, _) | Piprec _ -> acc
        | Picst (n, l, e) ->
            let e = do_s_pexpr (MS.prepare_symbs l d_cst) e in
            (StringMap.add n (l, e) d_cst, d_env, d_heap, d_pure, d_form, goals)
        | Pienv (n, e) ->
            let e = do_s_henv e in
            (d_cst, StringMap.add n e d_env, d_heap, d_pure, d_form, goals)
        | Piheap (n, l, h) ->
            let e, h, p = do_s_hform (MS.prepare_symbs l d_cst) h in
            if e = StringMap.empty && p = [ ] then
              (d_cst, d_env, StringMap.add n (l, h) d_heap,
               d_pure, d_form, goals)
            else Log.fatal_exn "invalid heap predicate definition"
        | Pipure (n, l, p) ->
            let p = do_s_pform (MS.prepare_symbs l d_cst) p in
            (d_cst, d_env, d_heap, StringMap.add n (l, p) d_pure, d_form, goals)
        | Piform (n, l, f) ->
            let f = do_s_formula { (MS.prepare_params l d_cst) with
                                   t_fresh = Keep } f in
            (d_cst, d_env, d_heap, d_pure, StringMap.add n (l, f) d_form, goals)
        | Pigoal (name, (params, fpre, f, fpost)) ->
            let d_symbs = MS.prepare_symbs params d_cst in
            let d_symbs = { d_symbs with t_fresh = Keep } in
            let f_form flatten_old_sval form =
              let e, h, p = do_s_formula d_symbs form in
              e, flatten_s_hform h, flatten_s_pform flatten_old_sval p in
            let forbid_old_sval sv =
              Log.fatal_exn "%s@old is only available in post-condition" sv in
            let (henv_pre, hform_pre, pform_pre) =
              f_form forbid_old_sval fpre in
            let old_svals_set =
              let _e, _h, p = do_s_formula d_symbs fpost in
              collect_s_pform collect_old_sval p in
            let add_old_sval sv map =
              let sv_old = sv ^ "@old" in
              SvalMap.add sv (sv_old, sv_old ^ "_a") map in
            let old_svals_map =
              SvalSet.fold add_old_sval old_svals_set SvalMap.empty in
            let flatten_old_sval sv =
              fst (SvalMap.find sv old_svals_map) in
            let (henv_post, hform_post, pform_post) =
              f_form flatten_old_sval fpost in
            let add_old_sval (pre: bool) sv (sv_old, sv_old_a) (henv, hform) =
              let henv = StringMap.add sv_old sv_old_a henv in
              let word = (FInd.So_int (Offs.of_int 0),
                          FInd.So_int (Offs.of_int 4)) in
              let hterm =
                if pre then
                  Sht_cell (sv_old_a, word, sv, word)
                else
                  Sht_cell (sv_old_a, word, sv_old, word) in
              (henv, hterm :: hform) in
            let henv_pre, hform_pre =
              SvalMap.fold (add_old_sval true) old_svals_map
                (henv_pre, hform_pre) in
            let add_old_sval_pform sv (sv_old, _sv_old_a) pform =
              (* Spt_cmp (S_ceq, Spe_sval sv, Spe_sval sv_old) :: pform in *)
              pform in
            let pform_pre =
              SvalMap.fold add_old_sval_pform old_svals_map pform_pre in
            let fpre = (henv_pre, hform_pre, pform_pre) in
            let henv_post, hform_post =
              SvalMap.fold (add_old_sval false) old_svals_map
                (henv_post, hform_post) in
            let henv_post =
              match f.result_binding with
              | None -> henv_post
              | Some ret ->
                  match StringMap.find_opt ret henv_post with
                  | None ->
                      Log.fatal_exn
                        "Return variable %s does not occur in post-condition"
                        ret
                  | Some addr ->
                      StringMap.add "#return" addr
                        (StringMap.remove ret henv_post) in
            let fpost = (henv_post, hform_post, pform_post) in
            let old_svals =
              SvalMap.fold (fun _sv (sv_old, _sv_old_a) acc -> sv_old :: acc)
                old_svals_map [] in
            let g = params, fpre, f, fpost, old_svals in
            (d_cst, d_env, d_heap, d_pure, d_form, StringMap.add name g goals)
      ) (StringMap.empty, StringMap.empty, StringMap.empty,
         StringMap.empty, StringMap.empty, StringMap.empty) l in
  let par_ffpr f fmt (l, x) =
    let g fmt (c, v) = F.fprintf fmt "%a %s" indpar_type_fpr c v in
    F.fprintf fmt "[%a]\n%a" (gen_list_fpr "" g ";") l f x in
  let ffpr f fmt (l, x) =
    let g fmt (c, v) = F.fprintf fmt "%a %s" col_kind_fpr c v in
    F.fprintf fmt "[%a]\n%a" (gen_list_fpr "" g ";") l f x in
  let form_fpr fmt (e, h, p) =
    F.fprintf fmt "%a * %a & %a" s_henv_fpr e Ps_fpr.s_hform_fpr h
      Ps_fpr.s_pform_fpr p in
  Log.info "Csts:\n%aEnvs:\n%aHeap:\n%aPure:\n%aForm:\n%a"
    (StringMap.t_fpr "\n" (ffpr Ps_fpr.s_pexpr_fpr)) d_cst
    (StringMap.t_fpr "\n" s_henv_fpr) d_env
    (StringMap.t_fpr "\n" (ffpr Ps_fpr.s_hform_fpr)) d_heap
    (StringMap.t_fpr "\n" (ffpr Ps_fpr.s_pform_fpr)) d_pure
    (StringMap.t_fpr "\n" (par_ffpr form_fpr)) d_form;
  StringMap.iter
    (fun name (params, fpre, proc_call, fpost, _old_svals) ->
      Log.info "Goal: %s [%a] {\n%a\n} %a {\n%a }\n"
        name
        s_col_params_fpr params
        s_formula_fpr fpre
        proc_call_fpr proc_call
        s_formula_fpr fpost
    ) goals;
  goals


(** Helper functions *)
(* Check that an inductive call has no argument *)
let assert_indcall_no_par msg (ic: indcall): unit =
  if ic.ii_argp != [] || ic.ii_argi != [] || ic.ii_args != [] then
    Log.fatal_exn "%s: indcall has parameters" msg
let assert_ind_no_par msg (ind: ind): unit =
  if ind.i_ppars > 0 || ind.i_ipars > 0 || ind.i_spars > 0 then
    Log.fatal_exn "%s: ind %s has parameters (unsupported)" msg ind.i_name


(** Visitor *)
let visitor (fp: 'a -> int -> 'a) (fi: 'a -> int -> 'a) (fs: 'a -> int -> 'a)
    (fq: 'a -> int -> 'a) : ('a -> hform -> 'a) * ('a -> pform -> 'a) =
  let do_formal_arg conv a fa = match (conv fa : formal_arg) with
    | Fa_this | Fa_var_new _ -> a
    | Fa_par_ptr i -> fp a i
    | Fa_par_int i -> fi a i
    | Fa_par_set i -> fs a i
    | Fa_par_seq i -> fq a i in
  let do_indcall a ic =
    let a = do_formal_arg formal_arg_of_main a ic.ii_maina in
    let a = List.fold_left (do_formal_arg formal_arg_of_ptr) a ic.ii_argp in
    let a = List.fold_left (do_formal_arg formal_arg_of_int) a ic.ii_argi in
    List.fold_left (do_formal_arg formal_arg_of_set) a ic.ii_args in
  let do_heapatom a = function
    | Hacell c -> do_formal_arg formal_arg_of_arith a c.ic_dest
    | Haind ic -> do_indcall a ic
    | Haseg (ic0, ic1) -> do_indcall (do_indcall a ic0) ic1 in
  let do_hform (a: 'a) (h: hform): 'a = List.fold_left do_heapatom a h in
  let do_sexpr a = function
    | Se_var v -> do_formal_arg formal_arg_of_set a v
    | Se_uplus (v0, v1) ->
        do_formal_arg formal_arg_of_arith
          (List.fold_left (do_formal_arg formal_arg_of_set) a v0) v1
    | Se_union (v0, v1) ->
        do_formal_arg formal_arg_of_arith
          (List.fold_left (do_formal_arg formal_arg_of_set) a v0) v1 in
  let rec do_aexpr a = function
    | Ae_cst _ -> a
    | Ae_var v -> do_formal_arg formal_arg_of_arith a v
    | Ae_colvinfo (ik, x) -> do_formal_arg formal_arg_of_seq a x
    | Ae_plus (ae0, ae1) -> do_aexpr (do_aexpr a ae0) ae1 in
  let do_sform a = function
    | Sf_mem (v0, v1) ->
        do_formal_arg formal_arg_of_set
          (do_formal_arg formal_arg_of_arith a v0) v1
    | Sf_equal (v0, as1) -> do_sexpr (do_formal_arg formal_arg_of_set a v0) as1
    | Sf_empty v -> do_formal_arg formal_arg_of_set a v in
  let rec do_qexpr a = function
    | Qe_sort s -> do_qexpr a s
    | Qe_concat s -> List.fold_left do_qexpr a s
    | Qe_var x -> do_formal_arg formal_arg_of_seq a x
    | Qe_val v -> do_formal_arg formal_arg_of_arith a v
    | Qe_empty -> a in
  let do_qform a = function
    | Qf_equal (s1, s2) -> do_qexpr (do_formal_arg formal_arg_of_seq a s1) s2 in
  let do_aform a = function
    | Af_equal (ae0, ae1)
    | Af_noteq (ae0, ae1)
    | Af_supeq (ae0, ae1)
    | Af_sup (ae0, ae1) -> do_aexpr (do_aexpr a ae0) ae1 in
  let do_pathform a (sc, _, dt) =
    do_formal_arg formal_arg_of_ptr
      (do_formal_arg formal_arg_of_ptr a sc) dt in
  let do_pformatom a = function
    | Pf_alloc _ -> a
    | Pf_set   f -> do_sform a f
    | Pf_seq   f -> do_qform a f
    | Pf_arith f -> do_aform a f
    | Pf_path p  -> do_pathform a p in
  let do_pform (a: 'a) (p: pform): 'a = List.fold_left do_pformatom a p in
  do_hform, do_pform
let visitor_hform fp fi fs fq = fst (visitor fp fi fs fq)
let visitor_pform fp fi fs fq = snd (visitor fp fi fs fq)


(** Equality test *)
let compare (i0: ind) (i1: ind): int =
  if i0 == i1 then 0
  else
    (* if it failed, it would mean an inductive is improperly copied all over
     * the place, which we normally avoid (a given inductive is represented
     * by the same OCaml object all the time *)
    let c = String.compare i0.i_name i1.i_name in
    assert (c != 0);
    c


(** Operations on tags *)
let ntyp_merge (nt0: ntyp) (nt1: ntyp): ntyp =
  if nt0 = nt1 then nt0
  else
    begin
      Log.info "Merging distict SV types: %a %a" ntyp_fpr nt0 ntyp_fpr nt1;
      Ntraw
    end


(** Utilities for inductive definitions analyses *)
let iter_heap (f: 'a -> heapatom -> 'a) (i: ind) (x: 'a): 'a =
  List.fold_left
    (fun acc r ->
      List.fold_left f acc r.ir_heap
    ) x i.i_rules



(** Inductive parameters use analysis:
 *  For each rule, we compute the set of parameters that are NOT used,
 *  so as to allow for parameters weakening later in join *)
let indpar_use_analysis (ind: ind): ind =
  (* compute the set of parameters of all sorts *)
  let rec f i acc = if i < 0 then acc else f (i - 1) (IntSet.add i acc) in
  let g i = f (i-1) IntSet.empty in
  let pars = g ind.i_ppars, g ind.i_ipars, g ind.i_spars, g ind.i_qpars in
  (* compute the information for each rule *)
  let f_ptr (sp, si, ss, sq) i = IntSet.remove i sp, si, ss, sq
  and f_int (sp, si, ss, sq) i = sp, IntSet.remove i si, ss, sq
  and f_set (sp, si, ss, sq) i = sp, si, IntSet.remove i ss, sq
  and f_seq (sp, si, ss, sq) i = sp, si, ss, IntSet.remove i sq in
  let do_hform, do_pform = visitor f_ptr f_int f_set f_seq in
  let rules =
    List.map
      (fun ir ->
        let sp, si, ss, sq = do_hform (do_pform pars ir.ir_pure) ir.ir_heap in
        let bn = IntSet.cardinal sp = ind.i_ppars
            && IntSet.cardinal si = ind.i_ipars
            && IntSet.cardinal ss = ind.i_spars in
        { ir with
          ir_uptr  = sp;
          ir_uint  = si;
          ir_uset  = ss;
          ir_useq  = sq;
          ir_unone = bn }
      ) ind.i_rules in
  { ind with i_rules = rules }
(* Functions to query the result of this analysis *)
let par_may_use_rules_gen (fkind: ir_kind -> bool)
    (par: formal_aux_arg) (ind: ind): bool =
  try
    List.iter
      (fun ir ->
        if fkind ir.ir_kind then
          match par with
          | Fa_par_ptr i -> if IntSet.mem i ir.ir_uptr then raise True
          | Fa_par_int i -> if IntSet.mem i ir.ir_uint then raise True
          | Fa_par_set i -> if IntSet.mem i ir.ir_uset then raise True
      ) ind.i_rules;
    false
  with True -> true
let kind_all_empty = function
  | Ik_empz | Ik_unk -> true
  | Ik_range (_,_) -> false
let kind_all_non_empty = function
  | Ik_unk | Ik_range (_,_) -> true
  | Ik_empz -> false
let par_may_use_rules_emp: formal_aux_arg -> ind -> bool =
  par_may_use_rules_gen kind_all_empty
let par_may_use_rules_notemp: formal_aux_arg -> ind -> bool =
  par_may_use_rules_gen kind_all_non_empty
let no_par_use_rules_gen (fkind: ir_kind -> bool) (ind: ind): bool =
  try
    List.iter
      (fun ir ->
        if fkind ir.ir_kind && not ir.ir_unone then raise False
      ) ind.i_rules;
    true
  with False -> false
let no_par_use_rules_emp: ind -> bool =
  no_par_use_rules_gen kind_all_empty
let no_par_use_rules_notemp: ind -> bool =
  no_par_use_rules_gen kind_all_non_empty



(** Nodes analysis: populates the map of types
 *  - either checks the types are coherent if the map is already full;
 *  - or populates map ir_typ with "Ntraw" record (no type assumption) *)
let indnodes_analysis (i: ind): ind =
  let nrules =
    List.map
      (fun ir ->
        if ir.ir_num = 0 then
          begin
            assert (ir.ir_typ = SvMap.empty);
            ir
          end
        else if ir.ir_typ = SvMap.empty then
          (* we make up a map of type "raw" *)
          let rec aux i acc =
            if i < 0 then acc
            else aux (i-1) (SvMap.add (sv_unsafe_of_int i) Ntraw acc) in
          { ir with ir_typ = aux ir.ir_num SvMap.empty }
        else
          (* we need to check that the indexes in the map correspond *)
          begin
            assert (SvMap.cardinal ir.ir_typ = ir.ir_num);
            SvMap.iter
              (fun ix _ ->
                let ix = sv_to_int ix in
                assert (0 <= ix && ix < ir.ir_num)
              ) ir.ir_typ;
            ir
          end
      ) i.i_rules in
  { i with i_rules = nrules }


(** Inductive "direction", that is paths traversed from one ind call to
 *  the next one:
 *  This analysis is very incomplete
 *  it considers only paths of the form this.f->$i * $i.ind(...) *)
let inddir_analysis (ind: ind): ind =
  let dirs, self_dirs =
    List.fold_left
      (fun ((dirs, self_dirs) as acc) r ->
        let m =
          List.fold_left
            (fun acc -> function
              | Haind _ -> acc
              | Haseg (_, _) -> acc
              | Hacell c ->
                  match c.ic_dest with
                  | Fa_var_new i -> IntMap.add i c.ic_off acc
                  | _ -> acc
            ) IntMap.empty r.ir_heap in
        List.fold_left
          (fun acc -> function
            | Hacell _ -> acc
            | Haseg (_, _) -> acc
            | Haind ic ->
                match ic.ii_maina with
                | Fa_this -> acc
                | Fa_var_new i ->
                    try
                      let o = IntMap.find i m in
                      let dirs' = Offs.OffSet.add o (fst acc) in
                      (* self_dirs are more restrictive than dirs *)
                      let self_dirs' =
                        if ic.ii_ind = ind.i_name then
                          Offs.OffSet.add o (snd acc)
                        else snd acc in
                      dirs', self_dirs'
                    with Not_found -> acc
          ) acc r.ir_heap
      ) (Offs.OffSet.empty, Offs.OffSet.empty) ind.i_rules in
  if !Flags.flag_debug_ind_analysis then
    begin
      Log.force "Inductive %s ind directions: { %a }"
        ind.i_name (Offs.OffSet.t_fpr ", ") dirs;
      Log.force "Inductive %s self ind directions: { %a }"
        ind.i_name (Offs.OffSet.t_fpr ", ") self_dirs
    end;
  { ind with i_dirs = dirs; i_may_self_dirs = self_dirs }


(** Computation of weakening types for integer/set parameters *)

(* Constants *)
let int_par_rec_top =
  { ipt_const = false ;
    ipt_incr  = false ;
    ipt_decr  = false ;
    ipt_add   = false }
let pars_rec_top =
  { pr_ptr_const  = IntMap.empty;
    pr_int        = IntMap.empty;
    pr_set        = IntMap.empty;
    pr_seq        = IntMap.empty }

(* Module containing the whole integer parameters analysis *)
module AnalysisIntParTypes =
  struct
    (* Types for integer parameters as used during the analysis *)
    type int_wk_typ =
      | WtEq  (* a_l = a_r *)
      | WtNon (* a_l has no relation with a_r *)
      | WtLeq (* a_l \leq a_r *)
      | WtGeq (* a_l \geq a_r *)
      | WtAdd (* a_l = a_r for inductive edge, for segments,
               * \alpha(a_l) *= \beta(a_l') and \alpha(a_r) *= \beta(a_r'),
               * a_l = a_l' + x and a_r = a_r' + x *)
    type int_pars_wk_typ = int_wk_typ IntMap.t

    (* Pretty-printing *)
    let int_wk_typ_fpr (fmt: form) (t: int_wk_typ): unit =
      match t with
      | WtEq  -> F.fprintf fmt "="
      | WtNon -> F.fprintf fmt "non"
      | WtLeq -> F.fprintf fmt "<="
      | WtGeq -> F.fprintf fmt ">="
      | WtAdd -> F.fprintf fmt "+"
    let pars_wk_typ_fpri (int: string) (fmt: form) (m: int_wk_typ IntMap.t)
        : unit =
      F.fprintf fmt "%sint_pars:%a" int (IntMap.t_fpr " | " int_wk_typ_fpr) m

    (* Collects all the occurrences of the i-th int of set parameters
     * as "new local names" (fails otherwise), from all the sub inductive
     * calls *)
    let linear_ic (fa: formal_arg) (hf: hform) (name: string): IntSet.t =
      let f_i_par (fa: formal_arg) (ic: indcall): int =
        match fa with
        | Fa_par_int i ->
            begin
              match List.nth ic.ii_argi i with
              | Fa_var_new n -> n
              | _ ->  raise A_Failure
            end
        | Fa_par_set i ->
            begin
              match  List.nth ic.ii_args i with
              | Fa_var_new n -> n
              | _ ->  raise A_Failure
            end
        | _ -> Log.fatal_exn "linear: improper arg" in
      let do_call ic acc =
        if ic.ii_ind = name then
          let n = f_i_par fa ic in
          if IntSet.mem n acc then raise A_Failure
          else IntSet.add n acc
        else acc in
      List.fold_left
        (fun acc h ->
          match h with
          | Haind ic -> do_call ic acc
          | Hacell c -> acc
          | Haseg (ic0, ic1) -> do_call ic0 (do_call ic1 acc)
        ) IntSet.empty hf

    (* Collect equality constraints over a set or integer parameters *)
    let linear_cons (fa: formal_arg) (pure: pform): pformatom =
      let f (fa: formal_arg) (pf: pformatom): pformatom option =
        match fa, pf with
        | Fa_par_int i, Pf_arith (Af_equal (al, ar)) ->
            if al = Ae_var (Fa_par_int i) then
              Some (Pf_arith (Af_equal (al, ar)))
            else if ar = Ae_var (Fa_par_int i) then
              Some (Pf_arith (Af_equal (ar, al)))
            else None
        | Fa_par_set i, Pf_set (Sf_equal (al, ar)) ->
            if al = Fa_par_set i then
              Some (Pf_set (Sf_equal (al, ar)))
            else if ar = Se_var (Fa_par_set i) then
              Some (Pf_set (Sf_equal (Fa_par_set i,  Se_var al)))
            else None
        | Fa_par_set i, Pf_set (Sf_empty fa) ->
            if fa = Fa_par_set i then Some (Pf_set (Sf_empty fa))
            else None
        | _ -> None in
      let fil_pure =
        List.fold_left
          (fun acc ele ->
            match f fa ele with
            | None -> acc
            | Some pf -> pf :: acc
          ) [] pure in
      if List.length fil_pure <> 1 then raise A_Failure
      else List.hd fil_pure

    (* Check whether the i-th int parameter is additive *)
    let wk_add_pform (i: int) (hf: hform) (pure: pform) (name: string): bool =
      try
        let all_args = linear_ic (Fa_par_int i) hf name in
        let pf = linear_cons (Fa_par_int i) pure in
        let rec f_aexpr ae acc =
          match ae with
          | Ae_var (Fa_var_new i) -> IntSet.add i acc
          | Ae_cst _ -> acc
          | Ae_plus (al, ar) -> f_aexpr ar (f_aexpr al acc)
          | _ -> raise A_Failure in
        let f_aform = function
          | Pf_arith (Af_equal (fs, se)) -> f_aexpr se IntSet.empty
          | _ -> IntSet.empty in
        IntSet.subset all_args (f_aform pf)
      with
      | A_Failure | Not_found -> false

    (* check whether the i-th pointer parameter is eq or none
     * or check whether the i-th int parameter is eq, leq, geq or none
     * or check whether the i-th set parameter is eq or none
     * (none means no such constraints for weakening) *)
    let wk_pform (i: formal_arg) (tp: int_wk_typ) (pure: pform) (heap: hform)
        (indname: string): bool =
      let do_formal_arg (i: formal_arg) (fa: formal_arg): bool =
        fa <> i in
      let do_formal_arith_arg (i: formal_arg) (fa: formal_arith_arg): bool =
        do_formal_arg i (formal_arg_of_arith fa) in
      let do_formal_set_arg (i: formal_arg) (fa: formal_set_arg): bool =
        do_formal_arg i (formal_arg_of_set fa) in
      let rec do_aexpr (i: formal_arg) (ae: aexpr): bool =
        match ae with
        | Ae_cst _ -> true
        | Ae_var v -> do_formal_arith_arg i v
        | Ae_colvinfo (ik, x) -> false (*sound answer, to discuss with XR*)
        | Ae_plus (ae0, ae1) -> do_aexpr i ae0 && do_aexpr i ae1 in
      let do_sexpr (i: formal_arg) (se: sexpr): bool=
        match se with
        | Se_var sa -> do_formal_set_arg i sa
        | Se_uplus (ls, ar) | Se_union (ls, ar) ->
            do_formal_arith_arg i ar && List.for_all (do_formal_set_arg i) ls in
      let do_aform (i: formal_arg) (af: aform): bool =
        match af with
        | Af_equal (al, ar)
        | Af_noteq (al, ar) -> do_aexpr i al && do_aexpr i ar
        | Af_sup (al, ar) | Af_supeq (al, ar) ->
            begin
              match al, ar, tp with
              | Ae_var ag, _, WtLeq ->
                  (formal_arg_of_arith ag = i || do_aexpr i al && do_aexpr i ar)
              | _, Ae_var ag, WtGeq ->
                  (formal_arg_of_arith ag = i || do_aexpr i al && do_aexpr i ar)
              | _, _, _ -> do_aexpr i al && do_aexpr i ar
            end in
      let do_sform (i: formal_arg) (sf: sform): bool =
        match sf with
        | Sf_mem (ar, sr) ->
            do_formal_arith_arg i ar && do_formal_set_arg i sr
        | Sf_equal (sa, se) -> do_sexpr i se && do_formal_set_arg i sa
        | Sf_empty sa -> do_formal_set_arg i sa in
      let do_pformatom (i: formal_arg) (pf: pformatom): bool =
        match pf with
        | Pf_set sf -> do_sform i sf
        | Pf_arith af -> do_aform i af
        | _ -> true in
      let do_ic (fa: formal_arg) (hf: hform) (name: string): bool =
        let f_i_par (fa: formal_arg) (ic: indcall): bool =
          match fa with
          | Fa_par_int i -> formal_arg_of_int (List.nth ic.ii_argi i) <> fa
          | Fa_par_set i -> formal_arg_of_set (List.nth ic.ii_args i) <> fa
          | Fa_par_ptr i -> formal_arg_of_ptr (List.nth ic.ii_argp i) <> fa
          | _ ->  Log.fatal_exn "do_ic: improper arg" in
        List.for_all
          (function
            | Haind ic -> if ic.ii_ind = name then f_i_par i ic else true
            | Hacell c -> true
            | Haseg (_, _) ->
                Log.warn "inductive parameters analysis, seg, drop" ;
                false
          ) hf in
      List.for_all
        (fun p ->
          match tp with
          | WtEq -> true
          | _ -> do_pformatom i p && do_ic i heap indname
        ) pure

    (* Compute weakening types for all the parameters of an inductive def *)
    let ind_rules_pars_wktyp (ind: ind): int_pars_wk_typ * int_pars_wk_typ =
      let lint_typs = [ WtEq; WtLeq; WtGeq; WtNon ] in
      let emp_rules = List.filter (fun r -> r.ir_kind = Ik_empz) ind.i_rules in
      (* Compute weakening types (eq, non) for all the pointer parameters
       * Compute weakening types (eq, leq, geq, non) for all the int parameters
       * Compute weakening types (eq, non) for all the set parameters *)
      let rec f_wktyp (i: int) (f: int -> formal_arg)
          (ls: int_wk_typ list) (m: int_pars_wk_typ)
          (rs: irule list): int_pars_wk_typ =
        (* Limitation:
         *    - this function keeps only one type per argument
         *    - the selection is not done in an appropriate manner.
         * Maybe refactor it at some point. *)
        if i < 0 then m
        else
          let fi = f i in
          let res =
            List.fold_left
              (fun acc t ->
                if IntMap.mem i acc then Log.warn "f_wktyp: overwriting type";
                let fpred r = wk_pform fi t r.ir_pure r.ir_heap ind.i_name in
                if List.for_all fpred rs then
                  IntMap.add i t acc
                else acc
              ) m ls in
          f_wktyp (i-1) f ls res rs in
      (* Compute weakening types add for all the int parameters *)
      let f_wktyp_int_add (is: IntSet.t) (m: int_wk_typ IntMap.t)
          (rs: irule list): int_wk_typ IntMap.t =
        IntSet.fold
          (fun i acc ->
            let fpred r = wk_add_pform i r.ir_heap r.ir_pure ind.i_name in
            if List.for_all fpred rs then
              IntMap.add i WtAdd m
            else m
          ) is m in
      let init_wktyp (ind: ind) (rs: irule list) =
        let f_int = fun i -> (Fa_par_int i : formal_arg) in
        f_wktyp (ind.i_ipars-1) f_int lint_typs IntMap.empty rs in
      let pars_wktyp = init_wktyp ind ind.i_rules in
      let eq_index =
        IntMap.fold (fun i m acc -> if m = WtEq then IntSet.add i acc else acc)
          pars_wktyp IntSet.empty in
      (* only do additive type computing for "eq" type int and set parameters *)
      let pars_wktyp =
        f_wktyp_int_add eq_index pars_wktyp ind.i_rules in
      (* if the inductive definition has emp rule, then, compute parameters type
       * only for empty rule *)
      let emp_pars_wktyp =
        if emp_rules = [ ] then pars_wktyp
        else init_wktyp ind emp_rules in
      pars_wktyp, emp_pars_wktyp

    (* Main function to produce the type for integer parameters *)
    let compute_int_par_rec (ind: ind)
        : int_par_rec IntMap.t * int_par_rec IntMap.t =
      (* run the existing analysis *)
      let pars_wktyp, emp_pars_wktyp = ind_rules_pars_wktyp ind in
      (* transform the results in a pointwise way *)
      let f pars_int_wktyp =
        IntMap.map
          (function
              (* TODO: rules for Geq and Leq seem opposite *)
            | WtEq  -> { int_par_rec_top with ipt_const = true }
            | WtAdd -> { int_par_rec_top with ipt_add = true }
            | WtGeq -> { int_par_rec_top with ipt_incr = true }
            | WtLeq -> { int_par_rec_top with ipt_decr = true }
            | WtNon -> int_par_rec_top
          ) pars_int_wktyp in
      f pars_wktyp, f emp_pars_wktyp
  end


(** Inductive definition parameters analysis
 *  This analysis aims at inferring whether parameters of inductive
 *  definitions may be constant or additive, which helps algorithms
 *  (non local unfolding, join...) *)

(* Parameters analysis *)
let indpars_analysis_ptr_set_seq (ind: ind): pars_rec =
  let flag_debug = !Flags.flag_debug_ind_analysis in
  if flag_debug then
    Log.force "Parameters analysis of inductive definition %s" ind.i_name;
  let make_map x i =
    let rec aux acc i =
      if i < 0 then acc
      else aux (IntMap.add i x acc) (i-1) in
    aux IntMap.empty (i-1) in
  let t_empty =
    { st_const = true ;
      st_add   = true ;
      st_head  = true ;
      st_mono  = true ; } in
  let q_empty =
    { sq_const = false ;
      sq_add   = true  ;
      sq_head  = false ; } in
  let res =
    { pr_ptr_const = make_map true ind.i_ppars ;
      pr_int       = make_map int_par_rec_top ind.i_ipars ;
      pr_set       = make_map t_empty ind.i_spars ;
      pr_seq       = make_map q_empty ind.i_qpars } in
  let topify r =
    let t_top  = { st_const = false ;
                   st_add   = false ;
                   st_head  = false ;
                   st_mono  = false } in
    let q_top  = { sq_const = false ;
                   sq_add   = false ;
                   sq_head  = false } in
    { pr_ptr_const = IntMap.map (fun _ -> false) r.pr_ptr_const ;
      pr_int       = IntMap.map (fun _ -> int_par_rec_top) r.pr_int ;
      pr_set       = IntMap.map (fun _ -> t_top) r.pr_set ;
      pr_seq       = IntMap.map (fun _ -> q_top) r.pr_seq } in
  (* Algo for pr_set:
   *  - start from most precise assignment
   *  - for each rule, make the list of calls to the same inductive
   *  - build abstract constraints for each rule
   *    => equalities
   *    => linear sums with this (linearization of equalities; fails if not lin)
   *  - intersection
   *)
  let do_rule (acc: pars_rec) (r: irule): pars_rec =
    Log.info "do_rule:\n%a" (irule_fpri "  ") r;
    let rec_calls =
      List.fold_left
        (fun acc -> function
          | Hacell _ -> acc
          | Haind c ->
              if String.compare c.ii_ind ind.i_name = 0 then c.ii_args :: acc
              else acc
          | Haseg (ic0, ic1) ->
              if ic0.ii_args = [] && ic1.ii_args = [] then acc
              else Log.todo_exn "indpars_analysis: segment parameters"
        ) [ ] r.ir_heap in
    (* linearization of set expressions, will help to extend this analysis *)
    let rec linearize (f: sform)
        : formal_set_arg * formal_set_arg list * formal_arith_arg list =
      match f with
      | Sf_empty sl -> sl, [ ], [ ]
      | Sf_equal (sl, Se_var sr) -> sl, [ sr ], [ ]
      | Sf_equal (sl, Se_uplus (sr, u)) -> sl, sr, [ u ]
      | Sf_equal (sl, Se_union (sr, u)) -> sl, sr, [ u ]
      | Sf_mem _ -> Log.fatal_exn "non linearizable" in
    (* set variables known to be empty, and known set relations *)
    let emp, rel, vpars, vnews =
      List.fold_left
        (fun (emp, rel, vpars, vnews) -> function
          | Pf_alloc _ | Pf_arith _ | Pf_set (Sf_mem _) | Pf_path _ ->
              emp, rel, vpars, vnews
          | Pf_seq _ ->
              (* for predicates that we use, sequences parameters can only
               * be "concatenable" at this point;
               * TODO: should we include some check to verify that the
               * inductive definitions are used in a proper way *)
              emp, rel, vpars, vnews
          | Pf_set f ->
              let dst, ls, ln = linearize f in
              let vpars, vnews =
                let f (vp, vn) = function
                  | Fa_par_set i -> IntSet.add i vp, vn
                  | Fa_var_new i -> vp, IntSet.add i vn in
                List.fold_left f (f (vpars, vnews) dst) ls in
              match dst, ls, ln with
              | Fa_par_set i, [ ], [ ] ->
                  IntSet.add i emp, rel, IntSet.add i vpars, vnews
              | Fa_par_set i, ls, ln ->
                  assert (not (IntMap.mem i rel)); (* otherwise, overwrite *)
                  emp, IntMap.add i (ls, ln) rel, vpars, vnews
              | _, _, _ -> emp, rel, vpars, vnews
        ) (IntSet.empty, IntMap.empty, IntSet.empty, IntSet.empty) r.ir_pure in
    let uf =
      IntMap.fold
        (fun i x uf ->
          match x with
          | [ s ], [ ] ->
              let uf = Union_find.add_if_absent (Fa_par_set i) uf in
              let uf = Union_find.add_if_absent s uf in
              let c0, uf = Union_find.find (Fa_par_set i) uf in
              let c1, uf = Union_find.find s uf in
              Union_find.union c0 c1 uf
          | _, _ -> uf
        ) rel Union_find.empty in
    (* flatten list to set; aborts if Fa_par_set present *)
    let flatten l =
      List.fold_left
        (fun acc -> function
          | Fa_par_set j -> raise Stop
          | Fa_var_new j -> IntSet.add j acc
        ) IntSet.empty l in
    (* treat the arguments of the sub-calls *)
    let rec aux i ((accc, acch, acca, accm) as acc) subs totspars =
      if i = totspars then acc
      else
        let sub0  = List.map List.hd subs
        and subso = List.map List.tl subs in
        let pre_is_mono = (* never used anywhere else than mem *)
          try
            List.iter
              (function
                | Fa_par_set i -> if IntSet.mem i vpars then raise Stop
                | Fa_var_new i -> if IntSet.mem i vnews then raise Stop
              ) sub0;
            true
          with Stop -> false in
        let acc =
          if sub0 = [ ] then (* no recursive call *)
            let accm = if pre_is_mono then IntSet.add i accm else accm in
            if IntSet.mem i emp then (* const, head, add *)
              IntSet.add i accc, IntSet.add i acch, IntSet.add i acca, accm
            else (* const, add *)
              IntSet.add i accc, acch, IntSet.add i acca, accm
          else
            let is_const = (* if sub call params always provably equal *)
              try
                List.iter
                  (fun x ->
                    if x = Fa_par_set i then ( )
                    else
                      let c, uf = Union_find.find (Fa_par_set i) uf in
                      if c != fst (Union_find.find x uf) then raise Stop
                  ) sub0;
                true
              with Stop | Not_found -> false in
            let is_mono = (* if const, and never used anywhere else than mem *)
              is_const && pre_is_mono in
            Log.info "is_mono: %b" is_mono;
            let is_head, is_add =
              try
                let ls, ln = IntMap.find i rel in
                (* check if add *)
                let is_add =
                  try
                    let a = flatten sub0 in
                    let b = flatten ls in
                    IntSet.equal a b
                  with Stop -> false in
                (* head if add + ln = [ This ] *)
                let is_head = is_add && ln = [ Fa_this ] in
                is_head, is_add
              with Not_found -> false, false in
            let f b acc = if b then IntSet.add i acc else acc in
            f is_const accc, f is_head acch, f is_add acca, f is_mono accm in
        aux (i+1) acc subso totspars in
    let argsc, argsh, argsa, argsm =
      aux 0 (IntSet.empty,IntSet.empty,IntSet.empty,IntSet.empty)
        rec_calls ind.i_spars in
    (* compute the intersection *)
    let nset =
      IntMap.mapi
        (fun i t ->
          Log.info "is_mono: %b|%b" t.st_mono (IntSet.mem i argsm);
          { st_const = t.st_const && IntSet.mem i argsc;
            st_head  = t.st_head  && IntSet.mem i argsh;
            st_add   = t.st_add   && IntSet.mem i argsa;
            st_mono  = t.st_mono  && IntSet.mem i argsm; }
        ) acc.pr_set in
    { acc with pr_set = nset } in
  let res = List.fold_left do_rule res ind.i_rules in
  (* functions to treat pointer, integer and set parameters *)
  let do_ptr_pars (acc: pars_rec) (lp: formal_ptr_args): pars_rec =
    let discard_info i acc =
      assert (IntMap.mem i acc.pr_ptr_const);
      { acc with pr_ptr_const = IntMap.add i false acc.pr_ptr_const } in
    let _, acc =
      List.fold_left
        (fun (i, acc) (arg: formal_ptr_arg) ->
          match arg with
          | Fa_this ->
              i + 1, discard_info i acc
          | Fa_var_new _ -> Log.todo_exn "ipa: ptr, new"
          | Fa_par_ptr k ->
              if k = i then (* info about the ith parameter is preserved *)
                i + 1, acc
              else (* parameter maybe not constant, info must be removed *)
                i + 1, discard_info i acc
        ) (0, acc) lp in
    acc in
  let pr_discard (i: int) (m: int_par_rec IntMap.t): int_par_rec IntMap.t =
    assert (IntMap.mem i m);
    IntMap.add i int_par_rec_top m in
  let pr_discard_cst (i: int) (m: int_par_rec IntMap.t): int_par_rec IntMap.t =
    assert (IntMap.mem i m);
    let old_info = IntMap.find i m in
    let new_info = { old_info with
                     ipt_const = false } in
    IntMap.add i new_info m in
  let do_int_pars (linrels: int IntMap.t) (acc: pars_rec)
      (li: formal_int_args): pars_rec =
    let discard i acc =
      { acc with pr_int = pr_discard i acc.pr_int } in
    let discard_cst i acc =
      { acc with pr_int = pr_discard_cst i acc.pr_int } in
    let _, acc =
      List.fold_left
        (fun (i, acc) (arg: formal_int_arg) ->
          match arg with
          | Fa_par_int k ->
              if k = i then (* info about the ith parameter is preserved *)
                i + 1, acc
              else (* parameter maybe not constant, info must be removed *)
                i + 1, discard i acc
          | Fa_var_new n ->
              try
                let k = IntMap.find n linrels in
                if k = i then (* parameter may be additive *)
                  i + 1, discard_cst i acc
                else (* parameter maybe not constant, info must be removed *)
                  i + 1, discard i acc
              with
              | Not_found -> i + 1, discard i acc
        ) (0, acc) li in
    acc in
  (* iteration over the set of all rules *)
  let _res =
    List.fold_left
      (fun acc ir ->
        (* computation of the set of local variables that are in a linear
         * relation together with an integer parameter *)
        let m_linrel =
          List.fold_left
            (fun rels -> function
              | Pf_alloc _ | Pf_set _ | Pf_arith (Af_noteq _)
              | Pf_seq _ (* Since nothing is done for set, I didn't had much *)
              | Pf_path _ | Pf_arith (Af_sup _)
              | Pf_arith (Af_supeq _) -> rels
              | Pf_arith (Af_equal (ae0, ae1)) ->
                  match ae0, ae1 with
                  | Ae_var (Fa_var_new n),
                    Ae_plus (Ae_var (Fa_par_int m), Ae_cst _)
                  | Ae_var (Fa_par_int m),
                    Ae_plus (Ae_var (Fa_var_new n), Ae_cst _)
                  | Ae_plus (Ae_var (Fa_var_new n), Ae_cst _),
                    Ae_var (Fa_par_int m)
                  | Ae_plus (Ae_var (Fa_par_int m), Ae_cst _),
                    Ae_var (Fa_var_new n) ->
                      IntMap.add n m rels
                  | _, _ -> rels
            ) IntMap.empty ir.ir_pure in
        (* computation of what is preserved under that rule, based on the
         * recursive calls that can be found *)
        List.fold_left
          (fun acc -> function
            | Hacell _ -> acc
            | Haind ic ->
                if String.compare ic.ii_ind ind.i_name = 0 then
                  begin
                    assert (List.length ic.ii_argi = ind.i_ipars);
                    assert (List.length ic.ii_argp = ind.i_ppars);
                    assert (List.length ic.ii_args = ind.i_spars);
                    assert (List.length ic.ii_argq = ind.i_qpars);
                    let acc = do_ptr_pars acc ic.ii_argp in
                    let acc = do_int_pars m_linrel acc ic.ii_argi in
                    acc
                  end
                else acc (* other inductive called, ignore *)
            | Haseg (ic0, ic1) ->
                Log.warn "indpars analysis, seg, losing precision";
                topify acc
          ) acc ir.ir_heap
      ) res ind.i_rules in
  if flag_debug then
    Log.info "Indpars_analysis result:\n%a" (pars_rec_fpri "  ") res;
  res
let indpars_analysis (ind: ind): ind =
  (* NOTE: for now, only integer parameters require better types for
   *       empty rules, so we handle only these *)
  (* ptrs, sets, seqs *)
  let pkind = indpars_analysis_ptr_set_seq ind in
  (* ints, from extended analysis *)
  let pr_int, pr_int_emp = AnalysisIntParTypes.compute_int_par_rec ind in
  { ind with
    i_pkind     = { pkind with pr_int = pr_int } ;
    i_pkind_emp = { pkind with pr_int = pr_int_emp } }


(** Computation of the inductives that have ONE rule corresponding to:
 *  - an empty region;
 *  - and a null pointer.
 *  This information can be used in order to speed up materialization. *)
let empty_heap_rule_analysis (i: ind): ind =
  (* checks whether a rule has the above property *)
  let check_rule (r: irule): bool =
    try
      if r.ir_heap = [ ] then
        begin
          List.iter
            (function
              | Pf_arith (Af_equal (Ae_var Fa_this, Ae_cst 0))
              | Pf_arith (Af_equal (Ae_cst 0, Ae_var Fa_this)) ->
                  raise A_Success
              | _ -> raise A_Failure
            ) r.ir_pure;
          false
        end
      else false
    with
    | A_Success -> true
    | A_Failure -> false in
  try
    let b =
      List.fold_left
        (fun acc r ->
          if check_rule r then
            if acc then raise A_Failure (* found a second empty rule *)
            else true (* found one empty rule *)
          else acc
        ) false i.i_rules in
    { i with i_mt_rule = b }
  with
  | A_Failure -> { i with i_mt_rule = false } (* multiple empty rules found *)


(** Computation of the inductives that have ONE rule corresponding to:
 *  - an empty region;
 *  - and an integer parameter.
 *  This information can be used in order to speed up materialization. *)
exception Found_iparam of int
let empty_heap_iparam_rule_analysis (i: ind): ind =
  (* checks whether a rule has the above property *)
  let check_rule (r: irule): int option =
    try
      if r.ir_heap = [ ] then
        begin
          List.iter
            (function
              | Pf_arith (Af_equal (Ae_var Fa_this, Ae_var (Fa_par_ptr x)))
              | Pf_arith (Af_equal (Ae_var (Fa_par_ptr x), Ae_var Fa_this)) ->
                  raise (Found_iparam x)
              | _ -> raise A_Failure
            ) r.ir_pure;
          None
        end
      else None
    with
    | Found_iparam ip -> Some ip
    | A_Failure -> None in
  let b =
    List.fold_left
      (fun acc r ->
         match check_rule r with
         | Some ip ->
             begin
               match acc with
               | Some _ -> raise A_Failure (* found a second empty rule *)
               | None -> Some ip (* found one empty rule *)
             end
         | None -> acc
      ) None i.i_rules in
  match b with
  | Some ip -> { i with i_emp_ipar = ip }
  | None -> i (* no such or multiple empty rules found *)


(** Computation of parameters which may denote prev pointers.
 *  This information is important for backward unfolding. *)
let bwdpar_analysis (i: ind): ind =
  (* search for parameters which are candidate prev pointers *)
  let allpars =
    let rec aux k =
      if k < 0 then IntSet.empty else IntSet.add k (aux (k-1)) in
    aux (i.i_ppars - 1) in
  let prev_pars =
    let f_doit (acc: IntSet.t): heapatom -> IntSet.t = function
      | Hacell _ -> acc
      | Haind ic ->
          let _, m =
            List.fold_left
              (fun (i, acc) : (formal_ptr_arg -> _) -> function
                | Fa_this -> i+1, acc
                | _ -> i+1, IntSet.remove i acc
              ) (0, acc) ic.ii_argp in
          m
      | Haseg (ic0, ic1) ->
          if ic0.ii_argp != [] || ic1.ii_argp != [] then
            begin
              Log.warn "bwdpar_analysis: segment rule, assumes no bwd par";
              IntSet.empty
            end
          else acc in
    iter_heap f_doit i allpars in
  if !Flags.flag_debug_ind_analysis then
    Log.force "Prev pointers: %a" intset_fpr prev_pars;
  (* collect all possibly pointer fields *)
  let allfields =
    let f_doit (acc: Offs.OffSet.t): heapatom -> Offs.OffSet.t = function
      | Haind _ -> acc
      | Hacell c ->
          if c.ic_size = Flags.abi_ptr_size then Offs.OffSet.add c.ic_off acc
          else acc
      | Haseg (_, _) -> acc in
    iter_heap f_doit i Offs.OffSet.empty in
  (* check which fields are prev fields *)
  let prev_fields =
    let f_doit (acc: Offs.OffSet.t): heapatom -> Offs.OffSet.t = function
      | Haind _ -> acc
      | Hacell c ->
          if Offs.OffSet.mem c.ic_off acc then
            match c.ic_dest with
            | Fa_par_ptr i ->
                if IntSet.mem i prev_pars then acc
                else Offs.OffSet.remove c.ic_off acc
            | _ -> Offs.OffSet.remove c.ic_off acc
          else acc
      | Haseg (_, _) -> acc in
    iter_heap f_doit i allfields in
  if !Flags.flag_debug_ind_analysis then
    Log.force "Prev fields: %a" (Offs.OffSet.t_fpr ",") prev_fields;
  (* search fields which are always equal to a prev pointer parameter *)
  { i with
    i_pr_pars = prev_pars ;
    i_pr_offs = prev_fields }


(** Computation of rules that are useful for segments:
 *  only rules that have at least one recursive call need be considered
 *  for segments *)
let ind_calc_segrules (i: ind): ind =
  let l =
    List.filter
      (fun r ->
        List.fold_left
          (fun acc -> function
            | Hacell _ -> acc
            | Haind _ -> true
            | Haseg (_, _) -> true
          ) false r.ir_heap
      ) i.i_rules in
  (* checks whether the root of a non empty segment may not be the null value;
   * if that is the case, the reduction below becomes available:
   *    [ a.i()====>b.i'() /\ a=0 ]   =>   [ a=b; empty segment ]   *)
  let rnull =
    (* compute whether a rule satisfies (P) the reduction above is ok *)
    let f_rule r =
      (* if there is a (non empty) field from this, the rule satisfies P *)
      let b0 =
        List.fold_left
          (fun acc -> function
            | Hacell c -> acc || c.ic_size > 0
            | Haseg (_, _) | Haind _ -> acc
          ) false r.ir_heap in
      (* if there is a predicate this not be null, the rule satisfies P *)
      let b1 =
        List.fold_left
          (fun acc -> function
            | Pf_arith (Af_noteq (Ae_var Fa_this, Ae_cst 0))
            | Pf_arith (Af_noteq (Ae_cst 0, Ae_var Fa_this)) -> true
            | _ -> acc
          ) false r.ir_pure in
      b0 || b1 in
    List.fold_left (fun acc r -> acc && f_rule r) true l in
  (* debug and result *)
  if !Flags.flag_debug_ind_analysis then
    Log.force "Found %d segment rules in %s; reduction property: %b"
      (List.length l) i.i_name rnull;
  { i with
    i_srules = l;
    i_reds0  = rnull }


(** Computation of fields that are equal to parameters *)
let ind_field_pars (ind: ind): ind =
  (* types: ptrs -> offset (the least one) *)
  let join x0 x1 =
    match x0, x1 with
    | None, x | x, None -> x
    | Some y0, Some y1 ->
        let m =
          IntMap.fold
            (fun i j0 acc ->
              try
                if IntMap.find i y1 = j0 then IntMap.add i j0 acc
                else acc
              with Not_found -> acc
            ) y0 IntMap.empty in
        Some m in
  let f_rule (r: irule): int IntMap.t =
    List.fold_left
      (fun acc -> function
        | Haind _ -> acc
        | Haseg (_, _) -> acc
        | Hacell c ->
            (* if offset is integer, and points to ptr, add to map *)
            match Offs.to_int_opt c.ic_off, c.ic_dest with
            | Some i, Fa_par_ptr j -> IntMap.add j i acc
            | _, _ -> acc
      ) IntMap.empty r.ir_heap in
  let result =
    List.fold_left
      (fun acc r ->
        if r.ir_heap = [ ] then acc
        else join acc (Some (f_rule r))
      ) None ind.i_rules in
  let f_pars =
    match result with
    | None -> IntMap.empty
    | Some m -> m in
  if !Flags.flag_debug_ind_analysis && f_pars != IntMap.empty then
    Log.force "Field parameters %s: {{ %a }}" ind.i_name
      (IntMap.t_fpr ", " (fun fmt -> F.fprintf fmt "%d")) f_pars;
  { ind with i_fl_pars = f_pars }


(** Computation of ir_kind fields
 *  i.e., when a rule is either necessary null (emp) or non null (non emp) *)
let ind_rules_kind (ind: ind): ind =
  let f_rule (r: irule): irule =
    let kind =
      try
        (* check whether the pointer is null *)
        let this_is_null =
          try
            List.iter
              (function
                | Pf_arith (Af_equal (Ae_var Fa_this, Ae_cst 0))
                | Pf_arith (Af_equal (Ae_cst 0, Ae_var Fa_this)) ->
                    raise A_Success (* it is null *)
                | _ -> raise A_Failure
              ) r.ir_pure;
            false
          with
          | A_Success -> true
          | A_Failure -> false in
        (* check whether the heap region is empty *)
        let region_is_emp = r.ir_heap = [ ] in
        (* compute the range *)
        let range =
          List.fold_left
            (fun acc -> function
              | Haind ic -> acc
              | Haseg (_, _) -> acc
              | Hacell c ->
                  match Offs.to_int_opt c.ic_off with
                  | None -> raise A_Failure
                  | Some o ->
                      let rec aux k acc =
                        if o > k then acc else aux (k-1) (IntSet.add k acc) in
                      aux (o+c.ic_size-1) acc
            ) IntSet.empty r.ir_heap in
        (* gather all the info, and produce a valid kind *)
        match this_is_null, region_is_emp, range with
        | true, true, _ -> Ik_empz
        | false, true, _ -> Log.warn "empty region, non null"; Ik_empz
        | false, false, s ->
            if s = IntSet.empty then
              Log.fatal_exn "rule kind: non null rull, empty range"
            else Ik_range (IntSet.min_elt s, 1 + IntSet.max_elt s)
        | _, _, _ -> Log.fatal_exn "undetermined rule kind"
      with A_Failure -> Ik_unk in
    if !Flags.flag_debug_ind_analysis then
      Log.force "Rule kind: %a" ir_kind_fpr kind;
    { r with ir_kind = kind } in
  { ind with i_rules = List.map f_rule ind.i_rules }


(** Computation of rules_cons, a constraint of a rule
 *  that differ from all the other rules
 *  i.e., when this = 0 and this != 0 *)
let ind_rules_cons (ind: ind): ind =
  let weak_arith_arg = function
    | Fa_this | Fa_par_int _ | Fa_par_ptr _ -> true
    | Fa_var_new i -> false in
  let weak_set_arg = function
    | Fa_par_set _ -> true
    | Fa_var_new i -> false in
  let weak_seq_arg = function
    | Fa_par_seq _ -> true
    | Fa_var_new i -> false in
  let rec weak_aexpr = function
    | Ae_cst i -> true
    | Ae_var fa -> weak_arith_arg fa
    | Ae_colvinfo (ik, x) -> false
    | Ae_plus (la, ra) -> weak_aexpr la && weak_aexpr ra in
  let weak_sexpr = function
    | Se_var sa -> if weak_set_arg sa then Some (Se_var sa, true) else None
    | Se_uplus (lsa, fa) ->
        if weak_arith_arg fa then
          Some (Se_uplus (lsa, fa), (List.for_all  weak_set_arg lsa))
        else None
    | Se_union (lsa, fa) ->
        if weak_arith_arg fa then
          Some (Se_union (lsa, fa), (List.for_all  weak_set_arg lsa))
        else None in
  let rec weak_qexpr = function
    | Qe_var qa -> if weak_seq_arg qa then Some (Qe_var qa, true) else None
    | Qe_val fa -> if weak_arith_arg fa then Some (Qe_val fa, true) else None
    | Qe_empty -> Some (Qe_empty, true)
    | Qe_concat qel ->
        let l = List.map weak_qexpr qel in
        if List.exists Option.is_none l then None else
        let qel, bl = List.map Option.get l |> List.split in
        Some (Qe_concat qel, List.for_all ( (=) true) bl)
    | Qe_sort qe ->
        qe
        |> weak_qexpr
        |> Option.map (fun (qe, b) -> (Qe_sort qe, b)) in
  let weak_aform = function
    | Af_equal (le, re)
    | Af_noteq (le, re)
    | Af_sup (le, re)
    | Af_supeq (le, re) -> weak_aexpr le && weak_aexpr re in
  let weak_sform sf =
    match sf with
    | Sf_mem (fa, sa) ->
        if weak_arith_arg fa && weak_set_arg sa then Some sf else None
    | Sf_empty sa ->
        if weak_set_arg sa then Some sf else None
    | Sf_equal (sa, se) ->
        if weak_set_arg sa then
          match weak_sexpr se with
          | None -> None
          | Some (se, true) -> Some (Sf_equal (sa, se))
          | Some (se, false) ->
              match se with
              | Se_union (_, fa) -> Some (Sf_mem (fa, sa))
              | Se_uplus (_, fa) -> Some (Sf_mem (fa, sa))
              | _ -> None
      else None in
  let weak_qform qf =
    match qf with
    | Qf_equal (qa, qexpr) ->
        match weak_seq_arg qa, weak_qexpr qexpr with
        | true, Some (qexpr, true) -> Some (Qf_equal (qa, qexpr))
        | _, _  -> None in
  let f_rule (r: irule) =
    try
      let cons1 =
        List.find
          (function
            | Pf_arith (Af_equal (Ae_var Fa_this, _))
            | Pf_arith (Af_equal (_, Ae_var Fa_this))
            | Pf_arith (Af_noteq (_, Ae_var Fa_this))
            | Pf_arith (Af_noteq (Ae_var Fa_this, _)) -> true
            | _ -> false
          ) r.ir_pure in
      let cons =
        match cons1 with
        | Pf_arith (Af_equal (Ae_var Fa_this, le)) ->
            Af_equal (Ae_var Fa_this, le)
        | Pf_arith (Af_equal (re, Ae_var Fa_this)) ->
            Af_equal (Ae_var Fa_this, re)
        | Pf_arith (Af_noteq (re, Ae_var Fa_this)) ->
            Af_noteq (Ae_var Fa_this, re)
        | Pf_arith (Af_noteq (Ae_var Fa_this, le)) ->
            Af_noteq (Ae_var Fa_this, le)
        | _ -> Log.fatal_exn "ind_rules_cons: cons is not arith" in
      let cons_o =
        List.fold_left
          (fun (acca, accs, accq) p ->
            match p with
            | Pf_set sf ->
                begin
                  match weak_sform sf with
                  | None -> acca, accs, accq
                  | Some f -> acca, f :: accs, accq
                end
            | Pf_seq qf ->
                begin
                  match weak_qform qf with
                  | None -> acca, accs, accq
                  | Some f -> acca, accs, f::accq
                end
            | Pf_arith pf ->
                if weak_aform pf && p <> cons1 then
                  pf :: acca, accs, accq else acca, accs, accq
            | _ -> acca, accs, accq
          ) ([], [], []) r.ir_pure in
      Some (cons, cons_o)
    with Not_found -> None in
  let is_conflict cons_l cons_r =
    match cons_l, cons_r with
    | Af_equal (Ae_var Fa_this, Ae_cst i),
      Af_equal (Ae_var Fa_this, Ae_cst j) ->
        i<>j
    | Af_equal (Ae_var Fa_this, le), Af_noteq (Ae_var Fa_this, re) ->
        le = re
    | Af_noteq (Ae_var Fa_this, le), Af_equal (Ae_var Fa_this, re) ->
        le = re
    | Af_noteq (Ae_var Fa_this, le), Af_noteq (Ae_var Fa_this, re) ->
        false
    | _, _ -> false in
  let rec check_conflict rules_cons =
    match rules_cons with
    | [ ] -> true
    | (cons_h, _, _, _) :: tl ->
        List.for_all (fun (cons_t, _, _, _) -> is_conflict cons_h cons_t) tl
          && (check_conflict tl) in
  let ores =
    List.fold_left
      (fun acco r ->
        match acco, f_rule r with
        | None, _ | _, None -> None
        | Some (acc, accr), Some (af, (afl, sfl, qfl)) ->
            Some ((af, afl, sfl, qfl) :: acc,
                  { r with ir_cons = Some af } :: accr)
      ) (Some ([],[])) ind.i_rules in
  match ores with
  | Some (i_rules_cons, i_rules) ->
      if check_conflict i_rules_cons then
        { ind with i_rules ; i_rules_cons }
      else ind
  | None -> ind

(** Computation of the guard condition of the empty rule,
 *  e.g.  this = null
 *  or    this = e *)
let emp_rule_cons (ind: ind): aform option =
  try
    let emp_rule = List.filter (fun r -> r.ir_kind = Ik_empz) ind.i_rules in
    if List.length emp_rule <> 1 then None
    else
      let emp_rule = List.hd emp_rule in
      emp_rule.ir_cons
  with Not_found -> None


(** Checks whether an inductive definition resembles a list inductive def
 ** (this is used in order to feed data into the list domain) *)
let ind_list_check (ind: ind): ind =
  try
    (* there should be exactly two rules, and one should be empty *)
    if not ind.i_mt_rule then raise A_Failure;
    if List.length ind.i_rules != 2 then raise A_Failure;
    (* the non empty rule should define non ambiguous next offset and size *)
    let onext, osize =
      List.fold_left
        (fun ((on, os) as acc) r ->
          match r.ir_kind with
          | Ik_empz -> acc
          | Ik_range (omin, omax) ->
              (* - check the size of the block *)
              let nsize =
                match os with
                | None -> Some (omax - omin)
                | Some i -> if omax - omin = i then os else raise A_Failure in
              (* - compute the list of inductive calls; should be one
               *   and the map from new symbolic variables into offsets from
               *   this *)
              let indcalls, i_2off =
                List.fold_left
                  (fun (accl, accm) -> function
                    | Haind ic -> ic :: accl, accm
                    | Haseg (_, _) -> Log.todo_exn "??002"
                    | Hacell ic ->
                        match Offs.to_int_opt ic.ic_off, ic.ic_dest with
                        | Some o, Fa_var_new v -> accl, IntMap.add o v accm
                        | _, _ -> accl, accm
                  ) ([ ], IntMap.empty) r.ir_heap in
              (* - localization of a *single* next field *)
              begin
                match indcalls with
                | [ ] | _ :: _ :: _ -> raise A_Failure
                | [ icall ] ->
                    if String.compare icall.ii_ind ind.i_name = 0
                        && icall.ii_argp = [] && icall.ii_argi = []
                        && icall.ii_args = [] then
                      match icall.ii_maina with
                      | Fa_var_new v ->
                          let nxt =
                            try IntMap.find v i_2off
                            with Not_found -> raise A_Failure in
                          if on = None || on = Some nxt then Some nxt, nsize
                          else raise A_Failure
                      | _ -> raise A_Failure
                    else raise A_Failure
              end
          | Ik_unk -> raise A_Failure
        ) (None, None) ind.i_rules in
    match onext, osize with
    | Some nxt, Some sz ->
        Log.info "Inductive %s identified as list(%d,%d)"
          ind.i_name nxt sz;
        { ind with i_list = Some (nxt, sz) }
    | None, _ | _, None -> raise A_Failure
  with A_Failure -> { ind with i_list = None }


(** Searches for parameters that could trigger non-local unfolding *)
let ind_nonloc_unfold_pars (ind: ind): ind =
  let loc_debug = false in
  let all_sets =
    let rec aux i = if i = 0 then IntSet.empty else IntSet.add i (aux (i-1)) in
    aux ind.i_spars in
  (* condition to check for all rules:
   * -- either rule empty and then S = empty
   * -- or among pure predicates set equality S = (this) U+ S' *)
  let nonloc_unfold_spars =
    List.fold_left
      (fun acc r ->
        let l_pars, empty, has_block, has_seg =
          List.fold_left
            (fun (accl, empty, has_block, has_seg) -> function
              | Hacell _ -> accl, false, true, has_seg
              | Haind ic ->
                  if ic.ii_ind = ind.i_name then
                    let accl =
                      match accl with
                      | [ ] -> List.map (fun x -> [ x ]) ic.ii_args
                      | l -> List.map2 (fun a b -> a :: b) ic.ii_args l in
                    accl, false, has_block, has_seg
                  else accl, false, has_block, has_seg
              | Haseg _ -> accl, empty, has_block, true
            ) ([ ], true, false, false) r.ir_heap in
        (* list s_pars maps each index i to
         *  - None   if any of the i-th position argument is not Fa_var_new ...
         *  - Some s if all the i-th position arguments are Fa_var_new j and
         *           s is the set of the such j indexes *)
        let s_pars =
          List.map
            (fun l ->
              List.fold_left
                (function
                  | None -> fun _ -> None
                  | Some s -> function
                      | Fa_par_set _ -> None
                      | Fa_var_new i -> Some (IntSet.add i s)
                ) (Some IntSet.empty) l
            ) l_pars in
        if loc_debug then
          List.iteri
            (fun i -> function
              | None -> Log.info "par %d => none" i
              | Some s -> Log.info "par %d => %a" i (IntSet.t_fpr ",") s
            ) s_pars;
        if has_seg then
          IntSet.empty
        else if empty then (* empty case, collect empty sets *)
          List.fold_left
            (fun acc -> function
              | Pf_set (Sf_empty (Fa_par_set i)) -> IntSet.add i acc
              | _ -> acc)
            IntSet.empty r.ir_pure
        else if has_block then
          let check_par_def i expr =
            match expr with
            | Se_uplus (l, Fa_this) ->
                let ok, s =
                  List.fold_left
                    (fun (ok,acc) -> function
                      | Fa_par_set _ -> false, acc
                      | Fa_var_new j -> ok, IntSet.add j acc
                    ) (true, IntSet.empty) l in
                if ok then
                  match List.nth_opt s_pars i with
                  | None | Some None -> false
                  | Some (Some rs) ->
                      let b = IntSet.equal rs s in
                      if loc_debug then
                        Log.info "check result:  %d\n %a\n => Analysis:%b" i
                          (IntSet.t_fpr ",") s b;
                      b
                else false
            | Se_var _ | Se_union _ | Se_uplus (_, _) -> false in
          let rule_unfold_spars =
            List.fold_left
              (fun acc -> function
                | Pf_set (Sf_equal (Fa_par_set i, expr)) ->
                    if check_par_def i expr then IntSet.add i acc
                    else acc
                | _ -> acc)
              IntSet.empty r.ir_pure in
          IntSet.inter acc rule_unfold_spars
        else IntSet.empty
      ) all_sets ind.i_rules in
  let l_nonloc_unfold_spars =
    IntSet.fold (fun i acc -> (i, CK_set) :: acc) nonloc_unfold_spars [ ] in
  { ind with i_nonloc_unf_pars = l_nonloc_unfold_spars }


(** Searches for size information across all rules *)
(* If all instructions are either empty rules (null pointer) or a block
 * of size s (the same for all the rules), try to compute s *)
let ind_size_blocks (ind: ind): ind =
  let oosize =
    List.fold_left
      (fun oosize r ->
        match r.ir_kind, oosize with
        | _, None
        | Ik_unk, _ -> None
        | Ik_empz, _ -> oosize
        | Ik_range (rlow, rhigh), Some None ->
            if rlow = 0 then Some (Some rhigh)
            else None
        | Ik_range (rlow, rhigh), Some (Some size) ->
            if rlow = 0 && rhigh = size then Some (Some size)
            else None
      ) (Some None) ind.i_rules in
  match oosize with
  | None -> ind
  | Some osize -> { ind with i_blksize = osize }


(** Computation of access paths between pointer parameters *)
let ptr_path_computation (ind: ind): ind =
  try
    (* there should be exactly two rules, and one should be empty *)
    if not ind.i_mt_rule then raise A_Failure;
    if List.length ind.i_rules != 2 then raise A_Failure;
    (* empty rule and next rule *)
    let erule, nrule =
      List.find (fun r -> r.ir_kind = Ik_empz) ind.i_rules,
      List.find (fun r -> r.ir_kind <> Ik_empz) ind.i_rules in
    let rc_ptr =
      List.fold_left
        (fun acc ele ->
          match ele with
          | Haind ic ->
              if ic.ii_ind = ind.i_name && acc = None then
                Some ic.ii_argp
              else raise A_Failure;
          | _ -> acc
        ) None nrule.ir_heap in
    (* rename path predicate in empty rule *)
    let init_paths =
      List.fold_left
        (fun acc ele ->
          match ele with
          | Pf_path (Fa_par_ptr i, Pe_epsilon, Fa_par_ptr j) ->
              (List.nth (Option.get rc_ptr) i,
               List.nth (Option.get rc_ptr) j) :: acc
          | _ -> acc
        ) [] erule.ir_pure in
    (* replace new var by the euqual ptr from empty rule *)
    let replace_new (sc : formal_ptr_arg) =
      match sc with
      | Fa_var_new i ->
          let fst, snd =
            List.find (fun (fst, snd) -> fst = sc || snd = sc) init_paths in
          if fst = sc then snd else fst
      | _ -> sc in
    let infer_paths =
      List.fold_left
        (fun acc ele ->
          match ele with
          | Pf_path (Fa_var_new i , Pe_single ofs, dt) ->
              (replace_new (Fa_var_new i), Pe_seg ofs, dt) :: acc
          | _ -> acc
        ) [] nrule.ir_pure in
    { ind with i_ppath = infer_paths }
  with
  | A_Failure -> ind
  | Not_found -> ind
  | Invalid_argument _ -> ind

(* Collect Set COLVs in rules *)
let ind_setvar_compute (ind: ind): ind =
  let do_ic ic (accs, accn, accq) =
    List.fold_left
      (fun acc a ->
        match (a : formal_set_arg) with
        | Fa_var_new i -> IntSet.add i acc
        | Fa_par_set _ -> acc
      ) accs ic.ii_args,
    List.fold_left
      (fun acc a ->
        match (a : formal_int_arg) with
        | Fa_var_new i -> IntSet.add i acc
        | Fa_par_int _ -> acc
      ) accn ic.ii_argi,
    List.fold_left
      (fun acc a ->
        match (a : formal_seq_arg) with
        | Fa_var_new i -> IntSet.add i acc
        | Fa_par_seq _ -> acc
      ) accq ic.ii_argq in
  let do_rule r =
    List.fold_left
      (fun (acc: IntSet.t * IntSet.t * IntSet.t) h ->
        match h with
        | Hacell _ -> acc
        | Haind ic -> do_ic ic acc
        | Haseg (ic0, ic1) -> do_ic ic0 (do_ic ic1 acc)
      ) (IntSet.empty, IntSet.empty, IntSet.empty) r.ir_heap in
  let irules =
    List.map
      (fun ele -> let svars, nvars, qvars = do_rule ele in
        { ele with ir_snum = svset_of_intset svars;
                   ir_qnum = svset_of_intset qvars;
                   ir_nnum = svset_of_intset nvars })
      ind.i_rules in
  let isrules =
    List.map
      (fun ele -> let svars, nvars, qvars = do_rule ele in
        { ele with ir_snum = svset_of_intset svars;
                   ir_qnum = svset_of_intset qvars;
                   ir_nnum = svset_of_intset nvars })
      ind.i_srules in
  { ind with
    i_rules  = irules;
    i_srules = isrules; }


(** Elaboration *)

(* Elaboration functions *)
let elaborate_irule (c_type_spec: Parsed_spec.c_type_spec)
    (irule : Parsed_spec.irule): irule =
  { ir_num = irule.num;
    ir_nnum  = SvSet.empty;
    ir_snum  = SvSet.empty;
    ir_qnum  = SvSet.empty;
    ir_typ   = svmap_of_list irule.typ;
    ir_heap  = List.map (Spec_utils.elaborate_heapatom c_type_spec) irule.heap;
    ir_pure  = List.map (Spec_utils.elaborate_pformatom c_type_spec) irule.pure;
    ir_kind  = Ik_unk;
    ir_uptr  = IntSet.empty;
    ir_uint  = IntSet.empty;
    ir_uset  = IntSet.empty;
    ir_useq  = IntSet.empty;
    ir_unone = false;
    ir_cons = None }
let elaborate_ind (c_type_spec: Parsed_spec.c_type_spec)
    (ind: Parsed_spec.ind): ind =
  let rules = List.map (elaborate_irule c_type_spec) ind.rules in
  { i_name            = ind.name;
    i_ppars           = ind.ppars;
    i_ipars           = ind.ipars;
    i_spars           = ind.spars;
    i_qpars           = ind.qpars;
    i_rules           = rules;
    i_rules_cons      = [];
    i_srules          = rules;
    i_mt_rule         = false;
    i_emp_ipar        = -1;
    i_reds0           = false;
    i_blksize         = None;
    i_dirs            = Offs.OffSet.empty;
    i_may_self_dirs   = Offs.OffSet.empty;
    i_pr_pars         = IntSet.empty;
    i_fl_pars         = IntMap.empty;
    i_pr_offs         = Offs.OffSet.empty;
    i_list            = None;
    i_pkind           = pars_rec_top;
    i_pkind_emp       = pars_rec_top;
    i_ppath           = [];
    i_nonloc_unf_pars = [ ] }


(** Set of inductive definitions *)
(* Global references *)
let ind_defs: ind StringMap.t ref = ref StringMap.empty
let ind_bind: string StringMap.t ref = ref StringMap.empty
let ind_prec: string list ref = ref [ ]

(* Initialization from parsing *)
let ind_init (context: Parsed_spec.type_context) (l: p_iline list): unit =
  (* Reinitialisation of inductive definition tables *)
  ind_defs := StringMap.empty;
  ind_bind := StringMap.empty;
  ind_prec := [ ];
  (* From list of parsed elements *)
  List.iter
    (function
      | Piind ind ->
          let name = ind.name in
          Log.info "Ind_init: %s [%d]" ind.name (StringMap.cardinal !ind_defs);
          assert (not (StringMap.mem name !ind_defs));
          (* verification of the rules (temporary, to fix other issues) *)
          List.iter
            (fun (r : Parsed_spec.irule) ->
              if List.length r.typ != r.num then
                Log.warn "incorrect rule in %s" name;
            ) ind.rules;
          let c_type_spec_opt =
            match context ind.c_type_ref_opt with
            | Error `Not_found -> None
            | Ok c_type_spec -> Some c_type_spec in
          begin
            match c_type_spec_opt with
            | None -> ()
            | Some c_type_spec ->
                let ind = elaborate_ind c_type_spec ind in
                (* compute the parameters used by each rule *)
                let ind = indpar_use_analysis ind in
                (* make the proper node associations, hint generations *)
                let ind = inddir_analysis (indnodes_analysis ind) in
                (* flag whether there is an "empty heap + null ptr" rule *)
                let ind = empty_heap_rule_analysis ind in
                (* flag whether there is an "empty heap + int param" rule *)
                let ind = empty_heap_iparam_rule_analysis ind in
                (* flag parameters that may denote prev pointers *)
                let ind = bwdpar_analysis ind in
                (* FOR XAR: flag parameters MUST denote prev pointers *)
                let ind =
                  let n = Offs.OffSet.union ind.i_may_self_dirs ind.i_pr_offs in
                  { ind with i_may_self_dirs = n } in
                (* flag some rules as "full inductive edges only"
                 * (should not be used at all for segments) *)
                let ind = ind_calc_segrules ind in
                (* analyze fields corresponding to pointer parameters *)
                let ind = ind_field_pars ind in
                (* computing rules kinds *)
                let ind = ind_rules_kind ind in
                (* check whether it resembles a list inductive definition *)
                let ind = ind_list_check ind in
                (* computing new set variables in each rule *)
                let ind = ind_setvar_compute ind in
                (* comoute i_rules_cons *)
                let ind = ind_rules_cons ind in
                (* analyze behavior of parameters *)
                let ind =
                  if !Flags.flag_indpars_analysis then indpars_analysis ind
                  else ind in
                let ind =  ptr_path_computation ind in
                (* search for nonlocal unfolding parameters *)
                let ind = ind_nonloc_unfold_pars ind in
                (* search for an (opitional) block size information *)
                let ind = ind_size_blocks ind in
                (* overwrite previous record *)
                ind_defs := StringMap.add name ind !ind_defs;
                Log.info "Inductive %s; block configuration result: %b\n%a"
                  ind.i_name ind.i_mt_rule (ind_fpri ~properties:true "  ") ind;
          end;
          Log.info ""
      | Pibind (tname, iname) ->
          assert (not (StringMap.mem tname !ind_bind));
          ind_bind := StringMap.add tname iname !ind_bind
      | Piprec l ->
          assert (!ind_prec = [ ]);
          ind_prec := l
      | Picst _ | Pienv _ | Piheap _ | Pipure _ | Piform _ | Pigoal _ -> ( )
    ) l


(* Extraction of an inductive *)
let ind_find (indname: string): ind =
  try StringMap.find indname !ind_defs
  with Not_found ->
    Log.fatal_exn "inductive %s not_found; map size: %d"
      indname (StringMap.cardinal !ind_defs)

(* Retrieve an inductive name from type *)
let indname_find (tname: string): string =
  try StringMap.find tname !ind_bind
  with Not_found -> Log.fatal_exn "type %s: no inductive found" tname
