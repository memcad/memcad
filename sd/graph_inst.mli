(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2022 INRIA
 **
 ** File: graph_inst.mli
 **       Instantiation of set parameters for the graph based shape domain join
 ** Xavier Rival, 2022/04/23 *)
open Data_structures
open Sv_def

open Graph_sig
open Ind_sig
open Inst_sig
open Set_sig

(** Functions specific to SV instantiation (integers) *)
(* type fresh sv in instantiation according to out graph *)
val int_par_type_sv_wk: graph -> node_relation -> sv_inst -> sv_inst
  -> int_par_rec SvMap.t * int_par_rec SvMap.t
val g_jinst_sv_calls:
    (sv SvMap.t * sv list)
  -> (sv SvMap.t * sv list * SvSet.t SvMap.t)
    -> (sv * sv -> sv * sv)
      -> Graph_sig.node_relation
        -> sv list

(** Constraints added to instantiation by introduction rule *)
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
val g_jinst_set_addcstr_segemp: seg_edge -> set_colv_inst -> set_colv_inst

(** Integrating unresolved set constraints from is_le into inst *)
(* For sets based on the general function *)
val g_jinst_set_addctrs_post_isle:
    msg: string                          (* location of the call, for logging *)
  -> findkind:(sv -> set_par_type option) (* Extraction of Set COLV types *)
    -> sarg_template:sv list              (* arguments of the is_le template *)
      -> sarg_out:sv list                 (* arguments of the output *)
	-> svmap:sv SvMap.t           (* Is_le mapping for symbolic variables *)
	  -> setvmap: SvSet.t SvMap.t     (* Is_le mapping for set parameters *)
	    -> sc: set_cons list (* Constraints: unrenamed, undischarged, right*)
	      -> inst: set_colv_inst      (* Previous instantiation *)
		-> set_colv_inst

(** Generic functions for instantiation in join *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
val jinst_set_addcstr_icall_eqs: ind_args -> ind_args
  -> set_colv_inst -> set_colv_inst
val jinst_seq_addcstr_icall_eqs: ind_args -> ind_args
  -> seq_colv_inst -> seq_colv_inst
val jinst_seq_addcstr_scall_eqs: ind_args_seg -> ind_args_seg
  -> seq_colv_inst -> seq_colv_inst


(** List specific instantiation heuristics, for sets *)
(* Try to guess some instantiation constraints for const set parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
val jinst_set_guesscstr_segemp: sv -> ind_edge(* -> ind_args*) -> graph
  -> set_colv_inst -> set_colv_inst


(** Functions below all need to be checked/maybe merged with oters *)

(* graph_join: instantiate fresh set variables according to
 * guessed equal relationship *)
val instantiate_eq:
    set_colv_inst       (* existing instantiation instantiation *)
  -> sv SvMap.t         (* gussesed equality relation on out set parameters *)
    -> set_expr SvMap.t (* gussed weaken from out set parameters to
                         * input set expressions *)
      -> set_colv_inst  (* new instantiation *)

(* deal with set parameters for introducing empty segment *)
val inst_void_seg: seg_edge -> join_inst -> join_inst

(* Generate equality from set parameters of output inductive edge to
 * set parameters of input inductive edge *)
val l_call_inst:
    ?skipset: bool
  -> ind_args (* set pars from the join input *)
  -> graph    (* join input graph *)
    -> ind_args (* set pars from the join output being constructed *)
      -> join_inst (* instantiation being constructed *)
        -> pr:pars_rec
          -> graph * join_inst * sv list

(* Generate equality from set parameters of output segment edge to
 * set parameters of input segment edge *)
val l_seg_inst:
    ?skipset: bool
  -> seg_edge (* set pars from the join input *)
    -> graph (* join input graph *)
      -> seg_edge (* set pars from the join output being constructed *)
        -> join_inst (* instantiation being constructed *)
          -> pr:pars_rec
            -> graph * join_inst
                * (sv list * sv list)  (* SV parameters lists *)

(* [rename_is_le_inst is_le_inst jinst m] adds in [jinst] all the binding in
 * [is_le_inst] with key [m(key)] *)
val rename_is_le_inst:
    Ast_sig.col_kind
  -> set_expr SvMap.t    (* is_le_inst: instantiation mapping elements to map *)
  -> set_expr SvMap.t   (* jinst:      initial instantiation mapping *)
    -> sv SvMap.t       (* m:          renaming over keys *)
      -> set_expr SvMap.t           (* new instantiation mapping *)

(* Deal with instantaition from is_le_ind *)
val is_le_call_inst:
    ind_args (* ind pars from the is_le output *)
  -> ind_args (* ind pars from the join output being constructed *)
    -> join_inst (* instantiation being constructed *)
      -> sv_inst   (* sv instantiation from is_le *)
        -> is_le_setv_inst option (* set instantiation from is_le *)
          -> is_le_seqv_inst   (* seq instantiation from is_le *)
            -> sv SvMap.t   (* sv mapping inferred by the inclusion *)
              -> join_inst * sv list

(* Deal with instantaition from is_le_seg *)
val is_le_seg_inst:
    seg_edge (* set pars from the is_le output *)
  -> seg_edge (* set pars from the join output being constructed *)
    -> join_inst (* instantiation being constructed *)
      -> sv_inst (* sv instantiation from is_le *)
        -> is_le_setv_inst option (* set instantiation from is_le *)
          -> is_le_seqv_inst   (* seq instantiation from is_le *)
            -> sv SvMap.t  (* sv mapping inferred by the inclusion *)
              -> join_inst * sv list * sv list

(* generate equality map from set parameters of output
 * summarized edge to set parameters of summarized edge
 * generated in inclusion checking *)
val gen_map: sv list -> sv list -> sv SvMap.t -> sv SvMap.t


(* Guess constraints among set parameters and numerical parameters
 * from output graph *)
val guess_cons:
    graph -> (* output graph *)
      graph -> vsat_l:Vd_sig.vsat -> sv_inst ->
        graph -> vsat_r:Vd_sig.vsat -> sv_inst ->
          node_relation ->
            sv SvMap.t           (* ? information about segment set pars *)
              * set_expr SvMap.t (*  *)
              * set_expr SvMap.t (*  *)
              * sv_inst          (* enriched left SV instantiation *)
              * sv_inst          (* enriched right SV instantiation *)
