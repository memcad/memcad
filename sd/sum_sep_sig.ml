(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sum_sep_sig.ml
 **       Data types for abstraction based on separating conjunction
 **       summaries, with predicates defined by extension over sets
 ** Vincent Rebiscoul and Xavier Rival, 2019/03/04 *)
open Data_structures
open Lib
open Sv_def

open Graph_sig
open Nd_sig
open Svenv_sig


(* Kinds of heap fragments for a node (very similar to graph) *)
type sheap_frag =
  | Shemp                        (* empty (no memory) *)
  | Shpt of pt_edge Block_frag.t (* block *)
  | Shsum of int                 (* summary *)
(* Note: Shsum should carry at least some information about set arguments! *)

type snode =
    { ln_i:     sv;           (* node id *)
      ln_e:     sheap_frag;   (* memory block at this address *)
      ln_typ:   ntyp;         (* type *) }

(* SV invariants *)
type smem =
    { (* Memory state *)
      lm_mem:       snode SvMap.t;
      (* Managemeng of SVs *)
      lm_nkey:      SvGen.t; (* key allocator for the symbolic variables *)
      lm_roots:     SvSet.t;
      lm_svemod:    svenv_mod;
      (* Management of Set COLVs *)
      lm_setvkey:   SvGen.t; (* key allocator for the set variables *)
      lm_setvroots: SvSet.t; (* setv which are root *) }
