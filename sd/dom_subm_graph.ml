(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_subm_graph.ml
 **       implementation of submemory based on graphs
 ** Xavier Rival, 2016/07/16 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def

open Dom_sig
open Dom_subm_sig
open Graph_sig
open Inst_sig
open Ind_sig
open Nd_sig
open Svenv_sig
open Vd_sig

open Dom_utils
open Graph_utils
open Sv_utils

open Apron

(** Error report *)
module Log =
  Logger.Make(struct let section = "d_submg_" and level = Log_level.DEBUG end)

(* whether or not to print full debug information *)
let full_debug = true

(* TODO:
 * - field t_stride seems unused *)


(** Submemory layer *)
(* A sub-memory encloses
 *  - a graph
 *  - a local name space;
 *  - a local environment *)
module Submem =
  (struct
    let module_name = "dom_subm_graph"
    let config_fpr fmt (): unit = () (* leaf module *)
    (* A sub-memory encloses:
     *  - a graph to describe the sub-memory contents
     *  - a bi-directional index binding global keys and local ones
     *  - an environment mapping offsets in the sub-memory space into
     *    nodes representing their addresses in the sub-memory
     *  - a map of offsets known to be in the submem, with the length of
     *    blocks known to start from them
     *  - information about the placement in the memory:
     *    SVs standing for address and value, begin offset, end offset:
     *    t_addr+[t_min,t_max[ => t_val
     *  - refcount information, on dependencies on global SVs
     *)
    type t =
        { (* graph *)
          t_graph:  graph; (* sub-memory graph *)
          (* indexes *)
          t_index:  (sv, sv) Bi_fun.t; (* bi-directional index *)
          t_env:    (Offs.t, sv) Bi_fun.t; (* environment *)
          t_off_ds: int OffMap.t; (* deltas between env offsets and stride *)
          (* placement information *)
          t_stride: int; (* value on which offsets are aligned *)
          t_addr:   sv; (* main mem SV for base address       *)
          t_val:    sv; (* main mem SV for content            *)
          t_min:    Bounds.t; (* min offset (included) *)
          t_max:    Bounds.t; (* max offset (not included) *)
          (* refcount information *)
          t_rc:     sv Refcount.t; (* global SVs refcount *) }

    (** Dom ID *)
    let dom_id: mod_id ref = ref (sv_unsafe_of_int (-1), "subm")

    (** Utilities *)
    (* index conversions *)
    let glo_to_loc (x: t) (iglo: sv): sv = Bi_fun.image iglo x.t_index
    let loc_to_glo (x: t) (iloc: sv): sv = Bi_fun.inverse_inj iloc x.t_index
    (* index conversion across a constraint *)
    let up_n_expr (x: t) (e: n_expr): n_expr =
      Nd_utils.n_expr_map (loc_to_glo x) e
    let up_n_cons (x: t) (c: n_cons): n_cons =
      Nd_utils.n_cons_map (loc_to_glo x) c
    (* map lift to global indexes *)
    let up_renaming (x: t) (r: sv SvMap.t): sv SvMap.t =
      let f = loc_to_glo x in
      SvMap.fold (fun i j acc -> SvMap.add (f i) (f j) acc) r SvMap.empty

    (** Temporary utilities: specific to current abstraction of sub-memories *)
    (* Exporting of keys of local graph *)
    let get_keys (x: t): SvSet.t =
      Aa_sets.fold SvSet.add (Bi_fun.whole_image x.t_index) SvSet.empty

    (** Sanity checks *)
    (* Sanity property:
     *  - t_index should be well-formed
     *  - nodes in the sub graph should correspond to indexes
     *)
    let sanity_check (ctx: string) (x: t): unit =
      let _, svem = Graph_utils.sve_sync_bot_up x.t_graph in
      if not (Dom_utils.svenv_is_empty svem) then
        Log.fatal_exn
          "sanity_check<%s> fails (submem): non empty svenv mod" ctx;
      Bi_fun.sanity_check ctx x.t_index;
      let indexes = get_keys x in
      let snodes = Graph_utils.get_all_nodes x.t_graph in
      if not (SvSet.equal indexes snodes) then
        begin
          Log.error "indexes: %a\nsnodes: %a" svset_fpr indexes
            svset_fpr snodes;
          Log.fatal_exn
            "sanity_check<%s> fails (submem): indexes and snodes matching" ctx;
        end

    (** Valid inductive definitions *)
    let valid_inductives: StringSet.t ref = ref StringSet.empty
    let init_inductives (g: SvGen.t) (s: StringSet.t): SvGen.t =
      let g, k = SvGen.gen_key g in (* this domain generates keys *)
      let inds =
        if !submem_inds = StringSet.empty then s else !submem_inds in
      valid_inductives := inds;
      if sv_upg then dom_id := k, snd !dom_id;
      g

    (** Construction of an (empty) abstract value *)
    (* A general value for reconstruction of elements *)
    let recompute_refcount (x: t): t =
      let f b =
        SvSet.fold Refcount.incr (Bounds.t_sym_var_ids_add SvSet.empty b) in
      let rc = Refcount.incr x.t_addr (Refcount.incr x.t_val Refcount.empty) in
      let rc = f x.t_min (f x.t_max rc) in
      { x with t_rc = rc }
    (* Creation of an empty sub-memory (cells are not there yet
     *  args give address SV, content SV, stride, low and high bounds *)
    let empty (loc: sv) (cont: sv)
        (stride: int) (blo: Bounds.t) (bhi: Bounds.t): t =
      recompute_refcount { t_graph  = graph_empty !valid_inductives;
                           t_index  = Bi_fun.empty_inj;
                           t_env    = Bi_fun.empty;
                           t_off_ds = OffMap.empty;
                           t_stride = stride;
                           t_addr   = loc;
                           t_val    = cont;
                           t_min    = blo;
                           t_max    = bhi;
                           t_rc     = Refcount.empty }
    (* Computation of a lower bound on the size of a block in sub-memory *)
    let env_get_block_size_option (i: sv) (x: t): int option =
      try
        match (node_find i x.t_graph).n_e with
        | Hemp | Hind _ | Hseg _ -> None
        | Hpt pte ->
            Some (Block_frag.fold_base
                    (fun _ pe m ->
                      match Offs.size_to_int_opt pe.pe_size with
                      | Some i -> i + m
                      | _ -> m
                    ) pte 0)
      with Not_found -> None
    let strengthen_off_delta (oknown: Offs.t) (onew: Offs.t) (i: sv) (x: t)
        (off_ds: int OffMap.t): int OffMap.t =
      try OffMap.add onew (OffMap.find oknown x.t_off_ds) off_ds
      with Not_found ->
        Log.info "offset was not found!";
        match env_get_block_size_option i x with
        | None -> off_ds
        | Some d -> OffMap.add onew d off_ds
    (* Post-build: reduces / recomputes the delta-offsets *)
    let post_build (x: t): t =
      let off_ds =
        Bi_fun.fold_dom
          (fun o i acc ->
            match env_get_block_size_option i x with
            | None -> acc
            | Some d -> OffMap.add o d acc
          ) x.t_env x.t_off_ds in
      { x with t_off_ds = off_ds }

    (** Filling fields *)
    let update_max (b: Bounds.t) (stride: int) (x: t): t =
      Log.info "updating the max to: %a" Bounds.t_fpr b;
      { x with t_max = (Bounds.add_int b stride) }
    let get_addr (x: t): sv      = x.t_addr
    let get_cont (x: t): sv      = x.t_val
    let get_omin (x: t): Bounds.t = x.t_min
    let get_omax (x: t): Bounds.t = x.t_max
    let get_env (x: t): (Offs.t, sv) Bi_fun.t = x.t_env
    let get_off_ds (x: t): int OffMap.t = x.t_off_ds

    (** Check if it may be impacted by a glo SV numeric write/removal *)
    let may_impact_sv_mod (i: sv) (x: t): bool =
      Refcount.hasref i x.t_rc

    (** Temporary utilities: specific to current abstraction of sub-memories *)
    (* Discarding of the add and rem fields of the local graph *)
    let discard_addrem (x: t): t =
      let g, _ = Graph_utils.sve_sync_bot_up x.t_graph in
      { x with t_graph = g }
    (* Synchronization of add_rem fields:
     *  - exports locally removed nodes (g)
     *  - exports locally added nodes (l) *)
    let sync_addrem (x: t): t * SvSet.t * SvSet.t =
      let g, svm = Graph_utils.sve_sync_bot_up x.t_graph in
      let add = PMap.fold (fun i _ -> SvSet.add i) svm.svm_add SvSet.empty in
      (* local sanity check, regarding to the local indexes *)
      PSet.iter
        (fun i -> assert (not (Bi_fun.mem_dir i x.t_index))) svm.svm_rem;
      (* fixing the env index *)
      let env =
        Bi_fun.fold_dom
          (fun off i acc ->
            if PSet.mem i svm.svm_rem then Bi_fun.rem_dir off acc
            else acc
          ) x.t_env x.t_env in
      (* (g) removed nodes *)
      let rem, index =
        PSet.fold
          (fun iloc (accr, accb) ->
            let iglo = loc_to_glo x iloc in
            SvSet.add iglo accr, Bi_fun.rem_dir iglo accb
          ) svm.svm_rem (SvSet.empty, x.t_index) in
      { t_graph  = g;
        t_index  = index;
        t_env    = env;
        t_off_ds = x.t_off_ds;
        t_stride = x.t_stride;
        t_addr   = x.t_addr;
        t_val    = x.t_val;
        t_min    = x.t_min;
        t_max    = x.t_max;
        t_rc     = x.t_rc }, add, rem

    (** Pretty-printing *)
    let t_fpri (ind: string) (fmt: form) (x: t): unit =
      let nind = "  "^ind in
      F.fprintf fmt "%s%a [%a - %a[ => %a <(%d)>\n%s{{\n%a"
        ind sv_fpr x.t_addr Bounds.t_fpr x.t_min Bounds.t_fpr x.t_max
        sv_fpr x.t_val x.t_stride ind (Refcount.t_fpri nind sv_fpr) x.t_rc;
      F.fprintf fmt "%sIndex:\n" ind;
      if full_debug then Bi_fun.t_fpr nind sv_fpr sv_fpr fmt x.t_index;
      F.fprintf fmt "%sEnv:\n%sSub-env:\n" ind nind;
      if not (Bi_fun.is_empty x.t_env) then
        begin
          let f_off fmt o =
            try
              let x = OffMap.find o x.t_off_ds in
              F.fprintf fmt "%a (++%d)" Offs.t_fpr o x
            with Not_found ->
              F.fprintf fmt "%a (++?)" Offs.t_fpr o in
          Bi_fun.t_fpr nind f_off sv_fpr fmt x.t_env
        end;
      F.fprintf fmt "%a%s}}\n" (graph_fpri nind) x.t_graph ind

    (** Environment *)
    (* Computation of a lower bound on the size of a block in sub-memory *)
    let env_get_block_size_lower_bound (i: sv) (x: t): int =
      match env_get_block_size_option i x with None -> 0 | Some n -> n
    (* Check if an offset is in the env, add it, find it*)
    let env_mem (o: Offs.t) (x: t): bool = Bi_fun.mem_dir o x.t_env
    let env_add (o: Offs.t) (i: sv) (x: t): t =
      Log.info "adding-to-env: %a < %a [%b]" Offs.t_fpr o
        Bounds.t_fpr x.t_max (Offs.is_on_stride x.t_stride o);
      let rc =
        if env_mem o x then x.t_rc
        else
          SvSet.fold Refcount.incr (Offs.t_sym_var_ids_add SvSet.empty o)
            x.t_rc in
      let off_ds =
        match env_get_block_size_option i x with
        | None -> x.t_off_ds
        | Some n -> OffMap.add o n x.t_off_ds in
      { x with
        t_env    = Bi_fun.add o i x.t_env;
        t_rc     = rc;
        t_off_ds = off_ds }
    let env_find (o: Offs.t) (x: t): sv =
      try Bi_fun.image o x.t_env
      with
      | Not_found ->
          Log.info "Not found in env_find: %a\n%a" Offs.t_fpr o (t_fpri "  ") x;
          Log.fatal_exn "not_found in env_find"
    (* looks for a decomposition of an offset as off(in env)+k where
     * k is smaller than the stride (i.e., search of a block field) *)
    let env_localize (osimplifier: Offs.t -> Offs.t) (o: Offs.t) (x: t)
        : (sv * Offs.t) option =
      Bi_fun.fold_dom
        (fun off i acc ->
          match acc with
          | Some _ -> acc
          | None ->
              let delta = osimplifier (Offs.sub_t o off) in
              match Offs.to_int_opt delta with
              | None -> None
              | Some idelta ->
                  if idelta >= 0 then Some (i, delta)
                  else None
        ) x.t_env None

    (** Temporary creation implementation *)
    let sv_add_gen (a: nalloc) (gl_i: sv) (x: t): sv * t =
      let i, g = sv_add_fresh Ntaddr a x.t_graph in
      let svm = { g.g_svemod with
                  svm_add = PMap.remove i g.g_svemod.svm_add } in
      let g = { g with g_svemod = svm } in
      i, { x with
           t_graph = g;
           t_index = Bi_fun.add gl_i i x.t_index }
    let sv_add_address:  sv -> t -> sv * t = sv_add_gen Nheap
    let sv_add_contents: sv -> t -> sv * t = sv_add_gen Nnone
    let add_cell (addr: sv) (bnd: Bounds.t) (size: Offs.size)
        (cont: sv) (x: t): t =
      if !flag_debug_submem then
        Log.force "add cell:\n%a" (graph_fpri "   ") x.t_graph;
      let pe = { pe_size = size;
                 pe_dest = cont, Offs.zero } in
      { x with
        t_graph = pt_edge_block_append (addr, bnd) pe x.t_graph }
    let register_node (iloc: sv) (iglo: sv) (x: t): t =
      { x with t_index = Bi_fun.add iglo iloc x.t_index }
    let node_assume_placed (iloc: sv) (x: t): t =
      { x with t_graph = node_assume_placed iloc x.t_graph }

    (** Constraints satisfaction *)
    let make_sat (main_sat: n_cons -> bool) (x: t) (nc: n_cons): bool =
      if !flag_debug_submem then
        Log.force "submem_sat: %a" Nd_utils.n_cons_fpr nc;
      let aux (): bool =
        main_sat (up_n_cons x nc) in
      (* First, attempts to solve on the graph *)
      let b_graph =
        match nc with
        | Nc_cons (Tcons1.DISEQ, Ne_csti 0, Ne_var i)
        | Nc_cons (Tcons1.DISEQ, Ne_var i, Ne_csti 0) ->
            pt_edge_mem (i, Offs.zero) x.t_graph
        | _ -> false in
      (* Second, attempts to verify on the numeric value *)
      b_graph || aux ()

    (** Reading functions *)
    let underlying_sat (c: n_cons): bool =
      Log.warn "calling unimplemented underlying sat function";
      false
    let read_sub_base_off
        (osimplifier: Offs.t -> Offs.t)
        (o: Offs.t) (sz: int) (x: t): Offs.svo =
      (* - localize in the environment an offset such that the delta is
       *   a positive integer
       * - walk from that point to search
       * - dereference edge in the graph
       *)
      Log.info "call   read_sub_base_off %a" Offs.t_fpr o;
      let o = osimplifier o in
      match env_localize osimplifier o x with
      | None -> Log.todo_exn "offset not found in environment"
      | Some on ->
          Log.info "\tFound sub-cell: %a" onode_fpr on;
          let g, pe = pt_edge_extract underlying_sat on sz x.t_graph in
          (* for now, we do not account for graph update *)
          assert (g == x.t_graph);
          loc_to_glo x (fst pe.pe_dest), snd pe.pe_dest
    let read_sub_internal
        (isrc: sv) (o: Offs.t) (sz: int) (x: t): Offs.svo =
      let g, pe = pt_edge_extract underlying_sat (isrc, o) sz x.t_graph in
      (* for now, we do not account for graph update *)
      assert (g == x.t_graph);
      loc_to_glo x (fst pe.pe_dest), snd pe.pe_dest

    (** Localization of a node as an address *)
    (* Finds an offset mapping to a node *)
    let localize_offset_of_node (i: sv) (x: t): Offs.t option =
      let s = Bi_fun.inverse i x.t_env in
      if Aa_sets.is_empty s then None
      else Some (Aa_sets.min_elt s)
    (* Decides whether a node is *definetely* allocated in the sub-memory *)
    let localize_node_in_block (i: sv) (x: t): bool =
      match (node_find (glo_to_loc x i) x.t_graph).n_e with
      | Hemp -> false
      | Hpt _ | Hind _ | Hseg _ -> true

    (** Delegated unfolding *)
    let unfold
        (main_sat: n_cons -> bool)
        (n: sv) (udir: unfold_dir) (x: t)
        : (t
             * sv SvMap.t (* renaming *)
             * n_cons list) list =
      let l = Graph_materialize.unfold ~submem:true n udir x.t_graph in
      List.map
        (fun ur ->
          (* unfolded sub-memory *)
          let x = { x with
                    t_graph = ur.ur_g; } in
          (* reduction of equalities inferred in the unfolding *)
          let x, renaming =
            if ur.ur_eqs = SvPrSet.empty then x, SvMap.empty
            else
              let ng, renaming =
                let sat = make_sat main_sat x in
                graph_merge_eq_nodes sat ur.ur_eqs x.t_graph in
              let nenv =
                Bi_fun.fold_dom
                  (fun off i acc ->
                    let j = try SvMap.find i renaming with Not_found -> i in
                    Bi_fun.add off j acc
                  ) x.t_env Bi_fun.empty in
              { x with
                t_graph = ng;
                t_env   = nenv }, up_renaming x renaming in
          (* experimental (trying to solve equality propagation issue) *)
          (* - check whether a capture occurs in the env *)
          let add_cons =
            (* propagate equalities on nodes that should be merged
             * (it often indirectly causes _|_ reduction later on) *)
            Bi_fun.fold_inverse
              (fun i s acc ->
                let rec aux acc l =
                  match l with
                  | [ ] | [ _ ] -> acc
                  | o0 :: o1 :: l2 ->
                      let e0 = Offs.to_n_expr o0 and e1 = Offs.to_n_expr o1 in
                      aux (Nc_cons (Tcons1.EQ, e0, e1) :: acc) (o1 :: l2) in
                aux acc (Aa_sets.fold (fun a b -> a :: b) s [ ])
              ) x.t_env [ ] in
          (* Equality constraints *)
          let constraints =
            SvPrSet.fold
              (fun (i, j) acc ->
                Nc_cons (Tcons1.EQ, Ne_var i, Ne_var j) :: acc
              ) ur.ur_eqs ur.ur_cons in
          (* All constraints *)
          let cons = add_cons @ List.map (up_n_cons x) constraints in
          if !flag_debug_submem then
            begin
              Log.info "UPconstraints: %d" (List.length cons);
              List.iter (Log.info "- %a" Nd_utils.n_cons_fpr) cons
            end;
          x, renaming, cons
        ) l

    (** Renaming using a node mapping *)
    let symvars_srename (om: (Offs.t * sv) OffMap.t) (nm: Offs.t node_mapping)
        (x: t): (t * n_cons list) option =
      try
        let loc_debug = false && !flag_debug_submem in
        if !flag_debug_submem then
          Log.force "SUB.symvars_srename, BEFORE:\n%a" (t_fpri "  ") x;
        let renamer: sv SvMap.t list =
          SvMap.fold
            (fun old (n0, nl1) acc ->
              List.fold_left
                (fun acc map ->
                  SvSet.fold (fun n acc -> SvMap.add old n map :: acc)
                    nl1 (SvMap.add old n0 map :: acc)
                ) [ ] acc
            ) nm.nm_map [ SvMap.empty ] in
        (* compute all possible ways to rename a given offset *)
        let f_off (o: Offs.t): Offs.OffSet.t =
          List.fold_left
            (fun acc r ->
              match Offs.t_rename_opt r o with
              | None -> acc
              | Some off -> Offs.OffSet.add off acc
            ) Offs.OffSet.empty renamer in
        let f_bound (b: Bounds.t): Bounds.t =
          if loc_debug then Log.info "Trying to rename: %a" Bounds.t_fpr b;
          let ol = Bounds.to_off_list b in
          let os =
            List.fold_left
              (fun acc o -> Offs.OffSet.union acc (f_off o))
              Offs.OffSet.empty ol in
          let bopt =
            Offs.OffSet.fold
              (fun o acc ->
                match acc with
                | None -> Some (Bounds.of_offs o)
              | Some bacc -> Some (Bounds.merge bacc (Bounds.of_offs o))
              ) os None in
          if loc_debug then Log.info "Renamed %a into" Bounds.t_fpr b;
          match bopt with
          | None -> Log.warn "no conversion"; raise Stop
          | Some b -> b in
        if !flag_debug_submem then
          begin
            Log.force "renamers: %d" (List.length renamer);
            List.iter (Log.force "%a" (SvMap.t_fpr ";" sv_fpr)) renamer;
          end;
        (* renaming in the environment
         * ->>> filter acceptable submem offsets *)
        let env, off_ds =
          Bi_fun.fold_dom
            (fun osub i ->
              Offs.OffSet.fold
                (fun ogr (acce, accd) ->
                  let keep = nm.nm_suboff ogr in
                  if !flag_debug_submem then
                    Log.force "sub-off %a [%a] kept: %b" Offs.t_fpr ogr
                      Offs.t_fpr osub keep;
                  let e = if keep then Bi_fun.add ogr i acce else acce in
                  let d = strengthen_off_delta osub ogr i x accd in
                  e, d
                ) (f_off osub)
            ) x.t_env (Bi_fun.empty, OffMap.empty) in
        (* augmenting environment with additional offsets if needed after
         * a join where new offsets were synthesized (unification failure) *)
        let env, off_ds, eqs =
          Bi_fun.fold_dom
            (fun o_osub i (acc_env, acc_od, acc_eqs) ->
              try
                let n_osub, _ = OffMap.find o_osub om in
                let eo = Offs.to_n_expr o_osub and en = Offs.to_n_expr n_osub in
                let eq = Nc_cons (Tcons1.EQ, eo, en) in
                let n_od = strengthen_off_delta o_osub n_osub i x acc_od in
                Bi_fun.add n_osub i acc_env, n_od, eq :: acc_eqs
              with Not_found -> acc_env, acc_od, acc_eqs
            ) x.t_env (env, off_ds, [ ]) in
        let x = { x with
                  t_env    = env;
                  t_off_ds = off_ds;
                  t_min    = f_bound x.t_min;
                  t_max    = f_bound x.t_max; } in
        if !flag_debug_submem then
          Log.force "SUB.symvars_srename, AFTER:\n%a" (t_fpri "  ") x;
        Some (recompute_refcount x, eqs)
      with Stop -> None

    (** Upper bounds *)

    (* Implementation of upper bounds *)
    let upper_bnd
        (acci: int) (* negative int allocator, main mem *)
        (accrel: node_relation) (* relation accumulator, main mem *)
        (accsn: (sv * sv) SvMap.t) (* localization accumulator, main mem *)
        ((iaddr, icont): sv * sv) (* base and content SVs *)
        (nsat0: n_cons -> bool) (x0: t)
        (nsat1: n_cons -> bool) (x1: t)
        : t * node_relation * (sv * sv) SvMap.t * int =
      if !flag_debug_submem then
        Log.force "Submem join:\nLL:\n%a\nRR:\n%a"
          (t_fpri "     ") x0 (t_fpri "    ") x1;
      (* Computation of roots... *)
      let nrel, _, nenv, nds =
        Bi_fun.fold_dom
          (fun off i0 (accr, acci, acce, accd) ->
            try
              let acci' = sv_unsafe_of_int acci in
              let ds =
                try
                  let d0 = OffMap.find off x0.t_off_ds
                  and d1 = OffMap.find off x1.t_off_ds in
                  if d0 = d1 then OffMap.add off d0 accd else accd
                with Not_found -> accd in
              let i1 = Bi_fun.image off x1.t_env in
              let r  = Nrel.add accr (i0, i1) acci' in
              r, acci + 1, Bi_fun.add off acci' acce, ds
            with
            | Not_found -> (* we discard that element from the environment *)
                if !flag_debug_submem then
                  Log.force "Dropping offset %a" Offs.t_fpr off;
                accr, acci, acce, accd
          ) x0.t_env (Nrel.empty, 0, Bi_fun.empty, OffMap.empty) in
      let g_init, (_, _), _ =
        init_graph_from_roots ~submem:true nrel.n_pi SvMap.empty SvMap.empty
          x0.t_graph x1.t_graph in
      let sat0, sat1 = make_sat nsat0 x0, make_sat nsat1 x1 in
      (* Trigger the join in the underlying *)
      let g, rel, (j_inst0, remsetin0), (j_inst1, remsetin1) =
        Graph_join.join ~submem:true
          (tr_join_arg sat0 (x0.t_graph, ext_graph None None))
          ~vsat_l:{ Vd_utils.vsat_empty with vs_num = sat0 }
          (tr_join_arg sat1 (x1.t_graph, ext_graph None None))
          ~vsat_r:{ Vd_utils.vsat_empty with vs_num = sat1 }
          None None nrel Nrel.empty Nrel.empty true g_init in
      assert (remsetin0 = [] && remsetin1 = []);
      let { set_inst = sinst0; sv_inst = ninst0; subm_mod = subm0 } =
        j_inst0 in
      let { set_inst = sinst1; sv_inst = ninst1; subm_mod = subm1 } =
        j_inst1 in
      assert (sinst0 = Inst_utils.set_colv_inst_empty);
      assert (sinst1 = Inst_utils.set_colv_inst_empty);
      assert (ninst0 = Inst_utils.sv_inst_empty);
      assert (ninst1 = Inst_utils.sv_inst_empty);
      (* Compute a new index and a new relation *)
      let index, accrel, accsn, acci = (* also add max int *)
        Nrel.fold
          (fun ires (il, ir) (accx, accrel, accsn, acci) ->
            let acci' = sv_unsafe_of_int acci in
            if !flag_debug_submem then
              Log.force "adding %a = (%a,%a) => %a" sv_fpr ires
                sv_fpr il sv_fpr ir sv_fpr acci';
            let pre_l = loc_to_glo x0 il in
            let pre_r = loc_to_glo x1 ir in
            if !flag_debug_submem then
              Log.force "       %a = (%a,%a)"
                sv_fpr acci' sv_fpr pre_l sv_fpr pre_r;
            ( Bi_fun.add acci' ires accx,
              Nrel.add accrel (pre_l, pre_r) acci',
              SvMap.add acci' (icont, ires) accsn,
              acci - 1 )
          ) rel (Bi_fun.empty_inj, accrel, accsn, acci) in
      (* Produce a result, assuming no instantiation to do *)
      let f_check subm =
        assert (subm.sub_fold = SvMap.empty) in
      f_check subm0; f_check subm1;
      let x =
        let stride =
          if x0.t_stride = x1.t_stride then x0.t_stride
          else Log.fatal_exn "joining two submem on different strides" in
        { t_graph  = g;
          t_index  = index;
          t_env    = nenv;
          t_off_ds = nds;
          t_stride = stride;
          t_addr   = iaddr;
          t_val    = icont;
          t_min    = Bounds.inter x0.t_min x1.t_min;
          t_max    = Bounds.inter x0.t_max x1.t_max;
          t_rc     = Refcount.empty } in
      recompute_refcount (discard_addrem x), accrel, accsn, acci

    (** Inclusion check *)
    let is_le (main_sat0: n_cons -> bool) (x0: t) (x1: t): bool =
      if Bi_fun.size x0.t_env != Bi_fun.size x1.t_env then
        Log.fatal_exn "incompatible envs (2)";
      (* Environment *)
      let env =
        Bi_fun.fold_dom
          (fun o i0 acc ->
            try Aa_maps.add (Bi_fun.image o x1.t_env) i0 acc
            with Not_found ->
              (* crashing here is probably avoidable
               * => we should simply drop singular entries of only one side *)
              Log.fatal_exn "incompatible envs (1)"
          ) x0.t_env Aa_maps.empty in
      let vsat_l = { vs_num = (make_sat main_sat0 x0) ;
                     vs_set = (fun _ -> false) ;
                     vs_seq = (fun _ -> false) } in
      let le_res =
        Graph_is_le.is_le ~submem: true x0.t_graph
          None ~vsat_l x1.t_graph env Aa_maps.empty in
      match le_res with
      | None -> false
      | Some _ -> true (* should we forward any information ??? *)

    (** Regression testing *)
    let ind_check
        (usat: n_cons -> bool) (* underlying domain sat function *)
        (off: Offs.t) (iname: string) (x: t): bool =
      (* construction of the inductive edge, assuming no parameter (for now) *)
      let ind = Ind_utils.ind_find iname in
      assert (ind.i_ppars = 0 && ind.i_ipars = 0 && ind.i_spars = 0);
      let ie = { ie_ind  = ind;
                 ie_args = { ia_ptr = [];
                             ia_int = [];
                             ia_set = [];
                             ia_seq = [] } } in
      (* retrieving offset, and building injection and graph to compare *)
      let src =
        try Bi_fun.image off x.t_env
        with Not_found -> Log.fatal_exn "ind_check, offset not found" in
      let inj = Aa_maps.singleton src src in
      let g =
        let g = graph_empty x.t_graph.g_inds in
        let g = sv_add src Ntaddr Nnone g in
        ind_edge_add src ie g in
      (* constraint satisfaction check *)
      let sat (c: n_cons): bool =
        make_sat usat x c in
      (* comparison and extraction of the result *)
      let r =
        Graph_is_le.is_le_partial ~fs:None ~submem:true false x.t_graph None
          SvSet.empty ~vsat_l:{ Vd_utils.vsat_empty with vs_num = sat }
          g inj Aa_maps.empty in
      match r with
      | Ilr_not_le | Ilr_le_seg _ | Ilr_le_ind _ -> false
      | Ilr_le_rem ilrem ->
          let (_, _, emb, _, sv_inst, set_inst,_,seq_inst,sc) =
            let xyz0,zyx1,xyz2,zyx3,xyz4,zyx5,xyz6,zyx7,xyz8 =
              ilrem.ilr_graph_l,
              ilrem.ilr_svrem_l,
              ilrem.ilr_svrtol,
              ilrem.ilr_colvrtol,
              ilrem.ilr_svinst,
              ilrem.ilr_setinst,
              ilrem.ilr_setctr_r,
              ilrem.ilr_seqinst,
              ilrem.ilr_seqctr_r in
            xyz0,zyx1,xyz2,zyx3,xyz4,zyx5,xyz6,zyx7,xyz8 in
          assert (set_inst.cvi_def = SvMap.empty);
          assert (set_inst.cvi_cons = []);
          assert (sc = [ ]);
          assert (set_inst.cvi_new = SvSet.empty);
          assert (seq_inst.cvi_def = SvMap.empty);
          assert (seq_inst.cvi_cons = []);
          assert (seq_inst.cvi_new = SvSet.empty);
          assert (sv_inst = Inst_utils.sv_inst_empty);
          true

    (** Write inside the sub-memory *)
    let write (sat: n_cons -> bool) (dst: sv * Offs.t) (ex: n_expr) (x: t): t =
      match ex with
      | Ne_var iglo -> (* this is a mutation *)
          let iloc = glo_to_loc x iglo in
          Log.info "Mutation inside sub-mem (%a => %a)"
            sv_fpr iloc sv_fpr iglo;
          Log.info "sub-mutation: %a ::= %a" onode_fpr dst sv_fpr iloc;
          let e = { pe_size = Offs.size_of_int 4; (* temporary, to fix *)
                    pe_dest = iloc, Offs.zero } in
          { x with
            t_graph = pt_edge_replace sat dst e x.t_graph }
      | _ -> Log.todo_exn "submem_write, complex expression"

    (** Guard inside the sub-memory *)
    let guard (c: n_cons) (x: t): t =
      match graph_guard true c x.t_graph with
      | Gr_bot -> Log.todo_exn "bottom found in submem-guard"
      | Gr_no_info -> x
      | _ -> (* might be possible to do something here to improve precision *)
          Log.info "submem-guard: other result"; x

  end: SUBMEM_SIG)
