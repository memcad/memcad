(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_is_le.ml
 **       inclusion algorithm on graphs
 ** Xavier Rival, 2011/09/21 *)
open Data_structures
open Flags
open Lib
open Sv_def

open Ast_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Inst_sig
open Col_sig
open Set_sig
open Seq_sig
open Vd_sig

open Gen_dom
open Gen_is_le

open Ast_utils
open Col_utils
open Graph_utils
open Ind_utils
open Inst_utils
open Nd_utils
open Set_utils
open Seq_utils
open Sv_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "g_isle__" and level = Log_level.DEBUG end)
let debug_module = false


(** Inclusion check auxilliary elements *)
(* A notion of state used in the inclusion check algorithm *)
type le_state =
    { (* Arguments configurations *)
      ls_cur_l:    graph ;
      ls_cur_r:    graph ;
      ls_cur_svi:  node_embedding ; (* cur. SV right to left mapping *)
      ls_cur_cvi:  colv_embedding;  (* cur. COLV right to left mapping *)
      (** Iteration strategy *)
      (* Pending rules (given as pairs of node names) *)
      ls_rules:    rules ; (* instances of rules pending application *)
      (* Nodes that were removed in the left argument (helps join) *)
      ls_rem_l:    SvSet.t ;
      (** Underlying domain constraints *)
      (* Satisfiability *)
      ls_vsat_l:   vsat ;
      (* new constraints on fresh symbolic variables of left *)
      ls_ctr_l:    n_cons list ;     (* constraints on fresh SVs *)
      ls_fsvs_l:   SvSet.t;          (* fresh SVs *)
      ls_inst_l:   n_expr SvMap.t;
      (* Accumulation of constraints *)
      ls_ctr_r:    n_cons list ;
      ls_setctr_r: set_cons list ;
      ls_seqctr_r: seq_cons list ;
      (* used for future guess_cons instantiation *)
      ls_iseg_r:   seg_edge list ;
      (* collect the fresh node and their arguments introduced
       * on the right side in rules seg-seg, seg-ind *)
      ls_fresh_seg_r: ind_edge SvMap.t ;
      (** Termination of the inclusion checking *)
      (* Whether we only need to empty both graphs or only the left graph *)
      ls_emp_both: bool ;
      (* Whether a success configuration has been reached *)
      ls_success:  bool ;
      (** Instantiation *)
      (* Nodes from the right input that need instantiation *)
      ls_fresh:    freshset ;
      (** Hints *)
      (* Hint on the left argument: nodes not to split *)
      ls_hint_l:   sv Aa_sets.t option ;
      (* Right remainder, excluded due to hint => left *)
      ls_excl_l:   graph ;
      (* Optional "end of segment node", to inhibit rules from that point *)
      ls_end_seg:  SvSet.t ;
      (** Parameters about the is_le semantics *)
      (* Whether we consider a sub-memory: no alloc check for sub-memories *)
      ls_submem:   bool;
      (* derive some disequality from left graph *)
      ls_gl_diseq: (sv -> sv -> bool);
    }
(* Pretty-printing of a configuration *)
let le_state_fpr (fmt: form) (ls: le_state): unit =
  let config_sep: string = "------------------------------------------\n" in
  let fl s f fmt l =
    let ff f fmt x = F.fprintf fmt "  %a" f x in
    if l = [ ] then F.fprintf fmt "%s: <none>\n" s
    else F.fprintf fmt "%s:\n%a\n" s (gen_list_fpr "" (ff f) "\n") l in
  F.fprintf fmt "%sLeft:\n%aRight:\n%aInjection (R->L):\n%a"
    config_sep (graph_fpri " ") ls.ls_cur_l (graph_fpri " ") ls.ls_cur_r
    (Nemb.ne_full_fpri "  ") ls.ls_cur_svi;
  F.fprintf fmt "Col Inject (R->L):\n%a"
    (Col_utils.CEmb.ne_full_fpri "  ") ls.ls_cur_cvi;
  F.fprintf fmt "%a%a%a%a"
    (fl "Num_cons_r" n_cons_fpr) ls.ls_ctr_r
    (fl "Set_cons_r" Set_utils.set_cons_fpr) ls.ls_setctr_r
    (fl "Seq_cons_r" Seq_utils.seq_cons_fpr) ls.ls_seqctr_r
    (fl "Num_cons_l" n_cons_fpr) ls.ls_ctr_l;
  F.fprintf fmt "N_fresh_svs_l: %a\n" nsvset_fpr ls.ls_fsvs_l;
  F.fprintf fmt "N_inst_l:\n";
  SvMap.iter
    (fun sv e -> F.fprintf fmt "  %a -> %a\n" nsv_fpr sv n_expr_fpr e)
    ls.ls_inst_l;
  F.fprintf fmt "%s" config_sep

(** Management of the set of applicable rules *)
(* Collecting applicable rules at a graph node *)
let collect_rules_node_gen =
  let sv_seg_end i g =
    match (node_find i g).n_e with
    | Hseg se -> Some se.se_dnode
    | Hemp | Hpt _ | Hind _ -> None in
  collect_rules_sv_gen sv_kind sv_seg_end
let collect_rules_node = collect_rules_node_gen false None
let collect_rules_node_st (il: sv) (ir: sv) (ls: le_state): le_state =
  let nr =
    collect_rules_node_gen false ls.ls_hint_l ls.ls_end_seg ls.ls_cur_svi
      ls.ls_cur_l ls.ls_cur_r il ir ls.ls_rules in
  { ls with ls_rules = nr }
(* Initialization: makes prioretary points-to rules *)
let rules_init
    (prio: bool) (* whether available pt-edges should be treated in priority *)
    (es: SvSet.t) (* end of segment(s), if any *)
    (ni: node_embedding)
    (gl: graph) (gr: graph) (r: node_emb): rules =
  if !Flags.flag_dbg_is_le_shape then
    Log.info "isle init,l:\n%aisle init,-r:\n%a"
      (graph_fpri "  ") gl (graph_fpri "  ") gr;
  let r =
    Aa_maps.fold
      (fun ir il acc ->
        if !Flags.flag_dbg_is_le_shape then
          Log.info "collecting at %a,%a" sv_fpr il sv_fpr ir;
        collect_rules_node_gen prio None es ni gl gr il ir acc
      ) r empty_rules in
  r



(** Utility functions for the is_le rules *)

(* Augment the current mapping to take into account a matching of
 * a pair of lists of inductive arguments *)
let fix_map_sv (msg: string) (ls: le_state) (il: sv) (ir: sv): le_state =
  try
    (* checks whether ir is mapped (otherwise, Not_found);
     * if mapped to oil it should be either be provably equal to il,
     *                               or a fresh SV that may be mapped later *)
    let oil = Nemb.find ir ls.ls_cur_svi in
    if oil = il then ls
    else
      (* in the case of equal *)
      let cs = Nc_cons (Apron.Tcons1.EQ, Ne_var il, Ne_var oil) in
      if SvSet.mem il ls.ls_fsvs_l then
        begin
          Log.info "tmp,ctr_l(fix_map_sv): %a" n_cons_fpr cs;
          { ls with ls_ctr_l = cs :: ls.ls_ctr_l }
        end
      else if ls.ls_vsat_l.vs_num cs then
        ls
      else
        raise (Le_false (F.asprintf "fix_map_sv[%s] (%a,%a->%a)"
                           msg sv_fpr ir sv_fpr il sv_fpr oil))
  with
  | Not_found ->
      collect_rules_node_st il ir
        { ls with ls_cur_svi = Nemb.add ir il ls.ls_cur_svi }
(* Similar functions for lists of arguments, pointer arguments, int arguments *)
let fix_map_args (msg: string)
    (ls: le_state) (al: sv list) (ar: sv list): le_state =
  if List.length al != List.length ar then
    Log.fatal_exn "fix_map_args[%s], lengths differ" msg;
  let smsg = Printf.sprintf "%s,l" msg in
  List.fold_left2 (fix_map_sv smsg) ls al ar
let fix_map_ptrargs (msg: string)
    (ls: le_state) (al: ind_args) (ar: ind_args): le_state =
  fix_map_args (Printf.sprintf "%s,ptr" msg) ls al.ia_ptr ar.ia_ptr
let fix_map_intargs (msg: string)
    (ls: le_state) (al: ind_args) (ar: ind_args): le_state =
  fix_map_args (Printf.sprintf "%s,int" msg) ls al.ia_int ar.ia_int

(** [fix_map_colargs kind msg ls al ar] add the bindings for [kind] parameters
 * from [al] & [ar] in [ls_cur_cvi]  *)
let fix_map_colargs  (kind: col_kind) (msg: string)
    (ls: le_state) (al: ind_args) (ar: ind_args): le_state =
  let largs, rargs =
    match kind with
    | CK_set -> al.ia_set, ar.ia_set
    | CK_seq -> al.ia_seq, ar.ia_seq in
  if List.length largs != List.length rargs then
    Log.fatal_exn "fix_map_args[%s], lengths differ for %a" msg
      col_kind_fpr kind;
  try
    List.fold_left2
      (fun ls l r ->
        { ls with
          ls_cur_cvi = Col_utils.CEmb.add r l kind ls.ls_cur_cvi; }
      ) ls largs rargs
  with Invalid_argument _ ->
    Log.fatal_exn "fix_map_colargs: arguments not match"
let fix_map_setargs = fix_map_colargs CK_set
let fix_map_seqargs = fix_map_colargs CK_seq

let fix_map_seqargs_seg (msg: string)
    (ls: le_state) (al: ind_args_seg) (ar: ind_args_seg): le_state =
  let largs, rargs = al.ias_seq, ar.ias_seq in
  if List.length largs != List.length rargs then
    Log.fatal_exn "fix_map_args[%s], lengths differ for %a" msg
      col_kind_fpr CK_seq;
  try
    List.fold_left2
      (fun ls (l1, l2) (r1, r2) ->
        let ls_cur_cvi =
          ls.ls_cur_cvi |> Col_utils.CEmb.add r1 l1 CK_seq
            |> Col_utils.CEmb.add r2 l2 CK_seq in
        { ls with ls_cur_cvi }
      ) ls largs rargs
  with Invalid_argument _ ->
    Log.fatal_exn "mapdir_ind_colargs: arguments not match"

(** Series of functions only called in the ind-ind rule *)
(* Searches the mapping mapping for a right SV; if not found, add a fresh
 * one and continue; based on the type of weakening that is allowed, add
 * some relevant constraints to ls_ctr_l *)
let fix_map_ord (msg: string) (ls: le_state)
    (il: sv) (ir: sv): (unit -> le_state) * (unit -> le_state) =
  let loc_debug = false in
  let f ir ls =
    try
      Nemb.find ir ls.ls_cur_svi, ls
    with
      Not_found ->
        let ill, g = sv_add_fresh Ntint Nnone ls.ls_cur_l in
        ill, { ls with
               ls_cur_l   = g;
               ls_fsvs_l  = SvSet.add ill ls.ls_fsvs_l;
               ls_cur_svi = Nemb.add ir ill ls.ls_cur_svi } in
  let fleq () =
    let ill, ls = f ir ls in
    let ctr = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var ill, Ne_var il) in
    if loc_debug then
      Log.info "tmp,ctr_l(map_ind_arg): %a" n_cons_fpr ctr;
    { ls with ls_ctr_l = ctr :: ls.ls_ctr_l }
  and fgeq () =
    let ill, ls = f ir ls in
    let ctr = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var il, Ne_var ill) in
    if loc_debug then
      Log.info "tmp,ctr_l(map_ind_arg): %a" n_cons_fpr ctr;
    { ls with ls_ctr_l = ctr :: ls.ls_ctr_l } in
  fleq, fgeq
let fix_map_leq (msg: string) (ls: le_state)
    (il: sv) (ir: sv): le_state =
  fst (fix_map_ord msg ls il ir) ()
let fix_map_geq (msg: string) (ls: le_state)
    (il: sv) (ir: sv): le_state =
  snd (fix_map_ord msg ls il ir) ()


let wkind_map_int_arg (msg: string) (ls: le_state)
    (ipt: int_par_rec) (il: sv) (ir: sv): le_state =
  if ipt.ipt_const then     fix_map_sv msg ls il ir
  else if ipt.ipt_add then  fix_map_sv msg ls il ir
  else if ipt.ipt_incr then fix_map_geq msg ls il ir
  else if ipt.ipt_decr then fix_map_leq msg ls il ir
  else if Nemb.mem ir ls.ls_cur_svi then ls
  else                      fix_map_sv msg ls il ir
(* idem, on list of arguments *)
let wkind_map_int_args (msg: string) (ls: le_state) (pk: pars_rec)
    (ils: sv list) (irs: sv list): le_state =
  let _, ls =
    List.fold_left2
      (fun (index, ls) il ir ->
        let ipt = IntMap.find index pk.pr_int in
        let ls = wkind_map_int_arg msg ls ipt il ir in
        index+1, ls
      ) (0, ls) ils irs in
  ls
let wkind_map_set_arg (msg: string) (ls: le_state) (pt: Set_sig.set_par_type)
    (il: sv) (ir: sv): le_state =
  if pt.st_const || pt.st_head || pt.st_add then
    { ls with ls_cur_cvi = Col_utils.CEmb.add ir il CK_set ls.ls_cur_cvi }
  else
    let ill, g = colv_add_fresh (Ct_set None) ls.ls_cur_l in
    { ls with
      ls_cur_l   = g ;
      ls_cur_cvi = Col_utils.CEmb.add ir ill CK_set ls.ls_cur_cvi }
let wkind_map_set_args (msg: string) (ls: le_state)
    (pk: pars_rec) (ils: sv list) (irs: sv list)
  : le_state =
  let _, ls =
    List.fold_left2
      (fun (index, ls) il ir ->
        let pt = IntMap.find index pk.pr_set in
        let ls = wkind_map_set_arg msg ls pt il ir in
        index+1, ls
      ) (0, ls) ils irs in
  ls
let wkind_map_is_args (msg: string) (ls: le_state)
    (pk: pars_rec) (il: ind_args) (ir: ind_args)
    : le_state =
  let ls = wkind_map_int_args msg ls pk il.ia_int ir.ia_int in
  let ls = wkind_map_set_args msg ls pk il.ia_set ir.ia_set in
  ls


(** Generation of fresh SV or COLV, to be mapped to an existing one *)
(* Generate a fresh node, to be mapped with some given node *)
let fresh_map_sv (nt: ntyp) (il: sv) (ls: le_state): sv * le_state =
  let ir, g = sv_add_fresh nt Nnone ls.ls_cur_r in
  ir, { ls with
        ls_cur_r   = g ;
        ls_cur_svi = Nemb.add ir il ls.ls_cur_svi }
let fresh_map_args (nt: ntyp) (ill: sv list)
    (ls: le_state): sv list * le_state =
  let l, ls =
    List.fold_left
      (fun (l, ls) sv ->
        let sv, ls = fresh_map_sv nt sv ls in
        sv :: l, ls
      ) ([ ], ls) ill in
  List.rev l, ls
(* seems redundant with the above, but if we refactor it, test 543 crashes *)
let fresh_map_iargs (ill: sv list) (ls: le_state)
  : sv list * le_state =
  let lipars, rg2 =
    List.fold_left
      (fun (acclr, accg) il ->
        let nir, ngr = sv_add_fresh Ntint Nnone accg in
        nir :: acclr, ngr
      ) ([ ], ls.ls_cur_r) ill in
  let ls =  { ls with ls_cur_r = rg2} in
  let irr =  List.rev lipars in
  irr, ls

(* handling of set parameters *)
(* TODO: the code below has been duplicated;
 *       there is another function called do_set which is very similar *)
let rec do_set ind (argl, argr, i) ls =
  match argl, argr with
  | [], [] -> [], ls
  | pl :: argl, pr :: argr ->
      let acc_pars, ls = do_set ind (argl, argr, i+1) ls in
      let pk = IntMap.find i ind.i_pkind.pr_set in
      let col = Ct_set (Some pk) in
      let colvr, ls_cur_r = colv_add_fresh col ls.ls_cur_r in
      let colve, ls_cur_r = colv_add_fresh col ls_cur_r in
      Log.info "mapsplit,do_set: (%a, %a) => %a : %a\n%a" setv_fpr pl
        setv_fpr pr setv_fpr colvr setv_fpr colve
        (Col_utils.CEmb.ne_full_fpri "  ") ls.ls_cur_cvi;
      let ls_cur_cvi = Col_utils.CEmb.add colvr pl CK_set ls.ls_cur_cvi in
      let ls = { ls with ls_cur_r; ls_cur_cvi } in
      if pk.st_const then
        let scs = [ S_eq (S_var pr, S_var colvr) ;
                    S_eq (S_var pr, S_var colve) ] in
        (*Log.todo_exn "mapslit,set,const"*)
        colve :: acc_pars, { ls with ls_setctr_r = scs @ ls.ls_setctr_r }
      else if pk.st_add then (* additive, split in a linear manner *)
        let sc = S_eq (S_var pr, S_uplus (S_var colvr, S_var colve)) in
        colve :: acc_pars, { ls with ls_setctr_r = sc :: ls.ls_setctr_r }
      else Log.todo_exn "mapsplit,set,other"
  | _, _ ->
      Log.fatal_exn "ind_mapsplit_pars, arg size: %i (l), %i (r)"
        (List.length argl) (List.length argr)


(** [ind_mapsplit_pars argsl argdl argsr ls] returns [ls, argse] s.t.
 *  [argsl *= argdl ⊑ argsr==> ↦ EMPTY ⊑ argse==>].
 *  Works for {!apply_seg_seg} and {!apply_seg_ind} *)
let ind_mapsplit_pars
    (ind: ind)                                              (* inductive *)
    ((argsl, argel, argdl): ind_args * ind_args_seg * ind_args) (* source *)
    (argsr: ind_args)                                       (* destination *)
    (ls: le_state)
    : ind_args * le_state  =
  (* handling of seq parameters; TODO maybe revise and share code *)
  let rec do_seq (acc_pars, ls) argsl argsr: (sv list * le_state) =
    let col, kind = Ct_seq None, CK_seq in
    match argsl, argsr with
    | [], [] -> acc_pars, ls
    | (pl, pl') :: argsl', pr :: argsr' ->
        begin
          (* if argsl != [] || argsr != [] then
             Log.fatal_exn "mapsplit,aux function, list of length > 1"; *)
          let colvr, ls_cur_r = colv_add_fresh col ls.ls_cur_r in
          let colvr', ls_cur_r = colv_add_fresh col ls_cur_r in
          let colve, ls_cur_r = colv_add_fresh col ls_cur_r in
          let ls_cur_cvi = ls.ls_cur_cvi |> Col_utils.CEmb.add colvr pl kind
            |> Col_utils.CEmb.add colvr' pl' kind in
          let ls = { ls with ls_cur_r; ls_cur_cvi } in
          (* We add the constraints *)
          let seq_cons =
            Seq_equal
              (Seq_var pr,
               Seq_Concat [Seq_var colvr; Seq_var colve; Seq_var colvr']) in
          let ls_seqctr_r = seq_cons :: ls.ls_seqctr_r in
          do_seq (colve :: acc_pars, { ls with ls_seqctr_r }) argsl' argsr'
        end
    | _ , _ ->
        Log.fatal_exn "ind_mapsplit_pars %a args of different size %i, %i"
          col_par_type_fpr col (List.length argsl) (List.length argsr) in
  let ptr_args, ls = fresh_map_args Ntaddr argdl.ia_ptr ls in
  let int_args, ls = fresh_map_iargs argdl.ia_int ls in
  let set_args, ls = do_set ind (argel.ias_set, argsr.ia_set, 0) ls in
  let seq_args, ls = do_seq ([], ls) argel.ias_seq argsr.ia_seq in
  let ind_args =
    { ia_ptr = ptr_args ;
      ia_int = int_args ;
      ia_set = set_args ;
      ia_seq = List.rev seq_args } in
  ind_args, ls


(** [ind_mapsplit_pars argsl argdl argsr ls] returns [ls, argse] s.t.
 *  [argsl *= argdl ⊑ argsr==> ↦ EMPTY ⊑ argse==>].
 *  Works for {!apply_seg_seg} and {!apply_seg_ind} *)
let ind_mapsplit_pars_seg
    (ind: ind)                                              (* inductive *)
    ((argsl, argel, argdl): ind_args * ind_args_seg * ind_args) (* source *)
    (argsr: ind_args_seg)                                       (* destination *)
    (ls: le_state)
    : ind_args_seg * le_state  =
  (* handling of seq parameters; TODO maybe revise and share code *)
  let rec do_seq (acc_pars, ls) argsl argsr: ((sv * sv) list * le_state) =
    let col, kind = Ct_seq None, CK_seq in
    match argsl, argsr with
    | [], [] -> acc_pars, ls
    | (pl, pl') :: argsl', (pr, pr') :: argsr' ->
        begin
          (* if argsl' != [] || argsr' != [] then
            Log.fatal_exn "mapsplit,aux function, list of length > 1"; *)
          let colvr, ls_cur_r = colv_add_fresh col ls.ls_cur_r in
          let colve, ls_cur_r = colv_add_fresh col ls_cur_r in
          let colvr', ls_cur_r = colv_add_fresh col ls_cur_r in
          let colve', ls_cur_r = colv_add_fresh col ls_cur_r in
          let ls =
            { ls with ls_cur_r;
              ls_cur_cvi = ls.ls_cur_cvi
              |> Col_utils.CEmb.add colvr pl kind
              |> Col_utils.CEmb.add colvr' pl' kind } in
          (* We add the constraints *)
          let seq_cons1 =
            Seq_equal (Seq_var pr, Seq_Concat [Seq_var colvr; Seq_var colve]) in
          let seq_cons2 =
            Seq_equal (Seq_var pr', Seq_Concat [Seq_var colve'; Seq_var colvr'])
          in
          let ls_seqctr_r = seq_cons1 :: seq_cons2 :: ls.ls_seqctr_r in
          do_seq
            ((colve, colve') :: acc_pars, { ls with ls_seqctr_r })
            argsl' argsr'
        end
    | _ , _ ->
        Log.fatal_exn "ind_mapsplit_pars %a args of different size %i, %i"
          col_par_type_fpr col (List.length argsl) (List.length argsr) in
  let ptr_args, ls = fresh_map_args Ntaddr argdl.ia_ptr ls in
  let int_args, ls = fresh_map_iargs argdl.ia_int ls in
  let set_args, ls = do_set ind (argel.ias_set, argsr.ias_set, 0) ls in
  let seq_args, ls = do_seq ([], ls) argel.ias_seq argsr.ias_seq in
  let ind_args_seg =
    { ias_ptr = ptr_args ;
      ias_int = int_args ;
      ias_set = set_args ;
      ias_seq = List.rev seq_args } in
      ind_args_seg, ls

(* Integer arguments, for both inductive and segment edges *)
let fix_map_sv_iargs_gen (msg: string) (pr: pars_rec)
    (l_sargs: sv list) (l_dargs: sv list) (r_sargs: sv list) (r_dargs: sv list)
    (ls: le_state): le_state =
  let do_arg (lsa: sv) (lda: sv) (rsa: sv) (rda: sv)
      (ipr: int_par_rec) (ls: le_state): le_state =
    if ipr.ipt_const then
      fix_map_sv msg (fix_map_sv msg ls lsa rsa) lda rda
    else if ipr.ipt_add then
      let isa, g = sv_add_fresh Ntint Nnone ls.ls_cur_l in
      let ida, g = sv_add_fresh Ntint Nnone g in
      let ctr = Nc_cons (Apron.Tcons1.EQ, Ne_var isa,
                         Ne_bin (Apron.Texpr1.Add, Ne_var ida,
                                 Ne_bin (Apron.Texpr1.Sub,
                                         Ne_var lsa, Ne_var lda))) in
      let ls =  { ls with
                  ls_cur_l  = g;
                  ls_fsvs_l = SvSet.add isa (SvSet.add ida ls.ls_fsvs_l);
                  ls_ctr_l  = ctr :: ls.ls_ctr_l } in
      fix_map_sv msg (fix_map_sv msg ls isa rsa) ida rda
    else if ipr.ipt_incr then
      fix_map_leq msg (fix_map_geq msg ls lsa rsa) lda rda
    else if ipr.ipt_decr then
      fix_map_geq msg (fix_map_leq msg ls lsa rsa) lda rda
    else ls in
  let rec do_args (index: int)
      (l_sargs: sv list) (l_dargs: sv list)
      (r_sargs: sv list) (r_dargs: sv list)
      (ls: le_state): le_state =
    match l_sargs, l_dargs, r_sargs, r_dargs with
    | [], [], [], [] -> ls
    | lsa :: tl_s, lda :: tl_d, rsa :: tr_s, rda :: tr_d ->
        let ipt = IntMap.find index pr.pr_int in
        let ls = do_arg lsa lda rsa rda ipt ls in
        do_args (index+1) tl_s tl_d tr_s tr_d ls
    | _ ->  Log.fatal_exn "fix_map_args[%s], lengths differ" msg; in
  do_args 0 l_sargs l_dargs r_sargs r_dargs ls
(* used for seg-seg and seg-ind *)
let fix_map_sv_iargs_ind (msg: string) (l_sargs: ind_args) (l_dargs: ind_args)
    (r_sargs: ind_args) (r_dargs: ind_args)
    (ind: ind) (ls: le_state): le_state =
  fix_map_sv_iargs_gen msg ind.i_pkind
    l_sargs.ia_int l_dargs.ia_int r_sargs.ia_int r_dargs.ia_int ls
(* used for seg-seg *)
let fix_map_sv_iargs_seg (msg: string) (l_sargs: ind_args) (l_dargs: ind_args)
    (r_sargs: ind_args) (r_dargs: ind_args_seg)
    (ind: ind) (ls: le_state): le_state =
  fix_map_sv_iargs_gen msg ind.i_pkind
    l_sargs.ia_int l_dargs.ia_int r_sargs.ia_int r_dargs.ias_int ls


(* Enriching an algorithm state with the result of a unification *)
let le_state_enrich (l: (sv * sv * sv) list) (ls: le_state): le_state =
  List.fold_left
    (fun (ls: le_state) (il, ir, _) ->
      fix_map_sv "make_blocks_compatible" ls il ir
    ) ls l

(* Making blocks compatible, through a possibly enriched unification *)
let make_blocks_compatible
    (mcl: pt_edge Block_frag.t) (mcr: pt_edge Block_frag.t)
    (ls: le_state)
    : le_state * bool =
  let bf_fpr = Block_frag.block_frag_fpr (fun fmt _ -> F.fprintf fmt ".") in
  if !Flags.flag_dbg_graph_blocks then
    Log.info "Enforcing compatibility:\n - %a\n - %a" bf_fpr mcl bf_fpr mcr;
  (* Enrich the mapping with extra mappings *)
  (* Attempt at performing the unification *)
  try
    Block_frag.fold_list2_bound1
      (fun lbeg rbeg ls ->
        match Bounds.unify_all lbeg rbeg with
        | None -> raise Stop
        | Some (uni, ubeg) -> le_state_enrich uni ls
      ) mcl mcr ls, true
  with
  | Stop -> ls, false



(** Individual rules *)
(* Unfolding rules that do not appear here and are part of unfold:
 *    emp - ind
 *    pt - ind
 *    pt - seg *)

(* Below is the implementation of all the non unfolding rules *)
(* pt - pt [par ptr OK, par int OK] *)
let apply_pt_pt (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = node_find isl ls.ls_cur_l in
  let nr = node_find isr ls.ls_cur_r in
  match nl.n_e, nr.n_e with
  | Hpt mcl, Hpt mcr ->
      if not ls.ls_submem then
        begin (* allocation consistency check, but only if not submem *)
          let check_alloc =
            match nl.n_alloc, nr.n_alloc with
            | Nstack, Nstack
            | Nnone, Nnone -> true
            | Nheap, Nheap -> true
            | _, _ -> false in
          if not !Flags.flag_ignore_n_alloc && not check_alloc then
            Log.fatal_exn "alloc constraint fails: %a-%a"
              nalloc_fpr nl.n_alloc nalloc_fpr nr.n_alloc
        end;
      (* Experimental code for arrays *)
      let sz_l = Block_frag.cardinal mcl and sz_r = Block_frag.cardinal mcr in
      if sz_l != sz_r then raise (Le_false "sizes do not match");
      let ls, compat =
        let r = make_blocks_compatible mcl mcr ls in
        if !Flags.flag_dbg_graph_blocks then
          Log.info "Arrayness: %b" (not (fst r == ls));
        r in
      if not compat then
        raise (Le_false "blocks not compatible");
      (* Code that works only in the non array case *)
      let ls =
        Block_frag.fold_base
          (fun os pl ls ->
            if Block_frag.mem os mcr then
              let pr = Block_frag.find_addr os mcr in
              (* Sizes do not normally need be matched
               * (the matching of bounds kind of supersedes it) *)
              if not (pl.pe_size = pr.pe_size) then
                Log.warn "is_le, pt-pt, sizes";
              let ls =
                let odl = snd pl.pe_dest and odr = snd pr.pe_dest in
                if !Flags.flag_dbg_graph_blocks then
                  Log.info "unifying %a-%a" Offs.t_fpr odl Offs.t_fpr odr;
                if Offs.t_is_const odl && Offs.t_is_const odr
                    && Offs.compare odl odr = 0 then ls
                else
                  match Offs.t_unify odl odr with
                  | None -> raise (Le_false "incompatible destination offsets")
                  | Some (uni, _) -> le_state_enrich uni ls in
              let idl = fst pl.pe_dest and idr = fst pr.pe_dest in
              (* check we do not overwrite anything in the mapping *)
              (* => alternative solution: add equality constraint to prove *)
              (* check treatment of this problem here *)
              let ls_cur_svi =
                if Nemb.mem idr ls.ls_cur_svi then
                  let midl = Nemb.find idr ls.ls_cur_svi in
                  if midl = idl then ls.ls_cur_svi
                  else (* prove equality *)
                    if ls.ls_vsat_l.vs_num
                        (Nc_cons (Apron.Tcons1.EQ, Ne_var idl,
                                  Ne_var midl)) then
                      ls.ls_cur_svi
                    else
                      raise (Le_false "pt-pt creates incompatible mapping")
                else
                  Nemb.add idr idl ls.ls_cur_svi in
              collect_rules_node_st idl idr
                { ls with
                  ls_cur_svi = ls_cur_svi;
                  ls_rem_l   = SvSet.add isl ls.ls_rem_l; }
            else ls
          ) mcl ls in
      let vrules = invalidate_rules isl isr Kpt Kpt ls.ls_rules in
      { ls with
        ls_cur_l = pt_edge_block_destroy isl ls.ls_cur_l;
        ls_cur_r = pt_edge_block_destroy isr ls.ls_cur_r;
        ls_rules = vrules; }
  | _, _ -> Log.fatal_exn "pt-pt; improper config"

(* ind - ind [par ptr OK, par int OK] *)
let apply_ind_ind (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = node_find isl ls.ls_cur_l in
  let nr = node_find isr ls.ls_cur_r in
  match nl.n_e, nr.n_e with
  | Hind icl, Hind icr ->
      if Ind_utils.compare icl.ie_ind icr.ie_ind = 0 then
        (* deal with integer and set parameters *)
        let ipr = ind_seg_edge_int_par_typ isl icl ls.ls_vsat_l.vs_num in
        let ls = wkind_map_is_args "ind-ind" ls ipr icl.ie_args icr.ie_args in
        (* HSL: deal with pointer parameters with hacks *)
        let ls =
          (* fast ind-ind rule: if left ptr is null, tries to discharge
           * obligation, without matching parameters *)
          if !Flags.do_quick_ind_ind_mt
              && icl.ie_ind.i_mt_rule
              && ls.ls_vsat_l.vs_num (Nc_cons (Apron.Tcons1.EQ,
                                               Ne_var isl, Ne_csti 0))
              && Ind_utils.no_par_use_rules_emp icl.ie_ind
              (* when left ptr is null, parameters may *
               * need match, therefore, some heuristic to guess when *
               * matching is not necessary *)
              && List.exists (fun ele ->
                  (node_find ele ls.ls_cur_l).n_attr <> Attr_none ||
                  (Nemb.mem ele ls.ls_cur_svi)
                ) icl.ie_args.ia_ptr
                (* JG: TODO check if we need to check something for col
                  parameters *)
          then
            (* heap region in the left side is empty and all the empty rules
             * assert no information on all parameters => no matching ! *)
            ls
          else fix_map_ptrargs "ind-ind" ls icl.ie_args icr.ie_args in
        let ls = fix_map_setargs "ind-ind" ls icl.ie_args icr.ie_args in
        let ls = fix_map_seqargs "ind-ind" ls icl.ie_args icr.ie_args in
        { ls with
          ls_cur_l = ind_edge_rem isl ls.ls_cur_l;
          ls_cur_r = ind_edge_rem isr ls.ls_cur_r;
          ls_rem_l = SvSet.add isl ls.ls_rem_l;
          ls_rules = invalidate_rules isl isr Kind Kind ls.ls_rules }
      else Log.fatal_exn "inductives do not match"
  | Hemp, Hemp ->
      (* both edges were consumed by another rule somehow;
       * we can discard the application of that rule *)
      ls
  | _, _ -> Log.fatal_exn "ind-ind; improper config"

(* seg - seg [par ptr OK, par int KO] *)
let apply_seg_seg (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = node_find isl ls.ls_cur_l in
  let nr = node_find isr ls.ls_cur_r in
  match nl.n_e, nr.n_e with
  | Hseg s0, Hseg s1 ->
      hseg_sanity_check ~isok:3 "is_le,seg-seg,l" s0;
      hseg_sanity_check ~isok:3 "is_le,seg-seg,r" s1;
      if Ind_utils.compare s0.se_ind s1.se_ind = 0 then
        let ls = fix_map_ptrargs "seg_seg,src" ls s0.se_sargs s1.se_sargs in
        (* default case: a*=b * G < a*=d * H   ==>  G < b*=d *  H   *)
        let default_seg_seg ( ) =
          (* segment gets consumed in the left argument;
           * another (shorter) segment gets added in the right argument *)
          (* remove the former segment in the right side *)
          let ls = { ls with ls_cur_r = seg_edge_rem isr ls.ls_cur_r } in
          (* add a fresh (middle point) node in the right side graph *)
          let insrc, ls = fresh_map_sv Ntaddr s0.se_dnode ls in
          (* add fresh nodes for the mapping of middle arguments *)
          let se_args, ls =
            Log.info "HSEG,temp: problematic call to mapsplit,seg-seg";
            (* TODO: maybe make this conjoining of arguments a separate op *)
            let s1_sargs = { ias_ptr = s1.se_sargs.ia_ptr ;
                             ias_int = s1.se_sargs.ia_int ;
                             ias_set = s1.se_eargs.ias_set ;
                             ias_seq = s1.se_eargs.ias_seq } in
            ind_mapsplit_pars_seg s0.se_ind
              (s0.se_sargs, s0.se_eargs, s0.se_dargs) s1_sargs ls in
          let ls =
            fix_map_sv_iargs_seg "seg_seg,int" s0.se_sargs s0.se_dargs
              s1.se_sargs se_args s0.se_ind ls in
          (* build the fresh segment edge and add it to the right side graph *)
          let se = { se_ind   = s0.se_ind ;
                     se_eargs = { ind_args_s_empty with
                                  ias_set = se_args.ias_set;
                                  ias_seq = se_args.ias_seq } ;
                     se_sargs = { ind_args_empty with
                                  ia_ptr = se_args.ias_ptr;
                                  ia_int = se_args.ias_int } ;
                     se_dargs = s1.se_dargs ;
                     se_dnode = s1.se_dnode } in
          hseg_sanity_check ~isok:3 "is_le,seg-seg,out" se;
          let fresh_seg_r =
            SvMap.add insrc { ie_ind  = s0.se_ind;
                              ie_args = ind_args_seg_to_ind_args se_args }
                              ls.ls_fresh_seg_r in
          collect_rules_node_st s0.se_dnode insrc
            { ls with
              ls_cur_l = seg_edge_rem isl ls.ls_cur_l;
              ls_cur_r = seg_edge_add insrc se ls.ls_cur_r;
              ls_rem_l = SvSet.add isl ls.ls_rem_l;
              ls_fresh_seg_r = fresh_seg_r;
              ls_rules = invalidate_rules isl isr Kseg Kseg ls.ls_rules } in
        (* s1: right side, s0: left side *)
        if Nemb.mem s1.se_dnode ls.ls_cur_svi then
          (* case:  a*=b * G < a*=b * H   ==>   G < H *)
          (* attempts to match both segments and remove them completely *)
          let idl = Nemb.find s1.se_dnode ls.ls_cur_svi in
          if idl = s0.se_dnode then
            let ls = fix_map_ptrargs "seg_seg,dst" ls s0.se_dargs s1.se_dargs in
            let ls =
              fix_map_setargs "seg_seg,edg" ls
                (ind_args_seg_to_ind_args s0.se_eargs)
                (ind_args_seg_to_ind_args s1.se_eargs) in
            let ls =
              fix_map_seqargs_seg "seg_seg,edg" ls s0.se_eargs s1.se_eargs in
            let ls =
              fix_map_sv_iargs_ind "seg_seg,int" s0.se_sargs s0.se_dargs
                s1.se_sargs s1.se_dargs s0.se_ind ls in
            (* we can consume both segments in the same time *)
            let ls_iseg_r =
              if s1.se_dargs.ia_int = [] then ls.ls_iseg_r
              else s1::ls.ls_iseg_r in
            { ls with
              ls_cur_l = seg_edge_rem isl ls.ls_cur_l;
              ls_cur_r = seg_edge_rem isr ls.ls_cur_r;
              ls_iseg_r = ls_iseg_r;
              ls_rem_l = SvSet.add isl ls.ls_rem_l;
              ls_rules = invalidate_rules isl isr Kseg Kseg ls.ls_rules }
          else default_seg_seg ( )
        else default_seg_seg ( )
      else Log.fatal_exn "rule seg-seg, applied to distinct inductives"
  | _, _ ->
      let msg =
        F.asprintf "todo in graph_is_le: rule seg-seg not applied to seg-seg" in
      raise (FailMsg msg)

(* seg - ind [par ptr OK, par int KO] *)
let apply_seg_ind (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = node_find isl ls.ls_cur_l in
  let nr = node_find isr ls.ls_cur_r in
  match nl.n_e, nr.n_e with
  | Hseg segl, Hind indr ->
      hseg_sanity_check ~isok:5 "is_le,seg-ind" segl;
      if Ind_utils.compare indr.ie_ind segl.se_ind = 0 then
        (* case: a*=b * G < a() * H   ==>   G < b() * H *)
        (* segment gets consumed in the left argument;
         * inductive gets split into a segment and an inductive in the right *)
        let ls = fix_map_ptrargs "seg_ind,src" ls segl.se_sargs indr.ie_args in
        (* was breaking graph-03-k
        let ls = fix_map_setargs "seg_ind,src" ls
          (ind_args_seg_to_ind_args segl.se_eargs) indr.ie_args in
        in
         *)
        (* add a fresh (middle point) node in the right side graph *)
        let insrc, ls = fresh_map_sv Ntaddr segl.se_dnode ls in
        (* add fresh nodes for the mapping of pointer destination arguments *)
        let se_args, ls =
          Log.info "HSEG,temp: problematic call to mapsplit";
          ind_mapsplit_pars indr.ie_ind
            (segl.se_sargs, segl.se_eargs, segl.se_dargs) indr.ie_args ls in
        let ls =
          fix_map_sv_iargs_ind "seg_ind,int" segl.se_sargs segl.se_dargs
            indr.ie_args se_args segl.se_ind ls in
        (* build the fresh inductive edge and add it to the right side graph *)
        let ie =
          { ie_ind  = indr.ie_ind ;
            ie_args = se_args ; } in
        collect_rules_node_st segl.se_dnode insrc
          { ls with
            ls_cur_l = seg_edge_rem isl ls.ls_cur_l;
            ls_cur_r = ind_edge_add insrc ie (ind_edge_rem isr ls.ls_cur_r);
            ls_fresh_seg_r = SvMap.add insrc ie ls.ls_fresh_seg_r;
            ls_rem_l = SvSet.add isl ls.ls_rem_l;
            ls_rules = invalidate_rules isl isr Kseg Kind ls.ls_rules }
      else Log.todo_exn "unhandled seg-ind case"
  | _, _ -> Log.fatal_exn "seg-ind; improper config"

(* void - seg [par ptr OK, par int KO] *)
let apply_void_seg (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nr = node_find isr ls.ls_cur_r in
  match nr.n_e with
  | Hseg s1 ->
      hseg_sanity_check ~isok:5 "is_le,void-seg,l" s1;
      let idr = s1.se_dnode in
      let ext_l =
        try Nemb.find idr ls.ls_cur_svi
        with Not_found ->
          let msg = F.asprintf "todo in graph_is_le: emp-seg: ext not mapped" in
          raise (FailMsg msg) in
      if ext_l = isl then (* segment successfully mapped to empty region *)
        (* pointer arguments are mapped to each other *)
        let ls =
          List.fold_left2
            (fun acc iars iard ->
              let ofind i =
                try Some (Nemb.find i acc.ls_cur_svi)
                with Not_found -> None in
              match ofind iars, ofind iard with
              | None, None -> raise (Le_false "emp-seg, no ptr par info")
              | Some ial, None ->
                  { acc with ls_cur_svi = Nemb.add iard ial acc.ls_cur_svi }
              | None, Some ial ->
                  { acc with ls_cur_svi = Nemb.add iars ial acc.ls_cur_svi }
              | Some ias, Some iad ->
                  if ias = iad then acc
                  else raise (Le_false "emp-seg, conflicting ptr par info")
            ) ls s1.se_sargs.ia_ptr s1.se_dargs.ia_ptr in
        let setctr, _ =
	  let ind = s1.se_ind in
          List.fold_left
            (fun (acc, i) iars ->
	      let pkind = IntMap.find i ind.i_pkind.pr_set in
              (* TODO fix now: this should be done only when the parameter
               * is head or add;
               * otherwise, no constraint from the empty segment case *)
              Log.info "tmp,debug empty constraint: S%a %a" sv_fpr iars
		set_par_type_fpr pkind;
	      let acc =
		if Set_utils.set_par_type_is_add pkind then
		  S_eq (S_var iars, S_empty) :: acc
		else acc in
	      acc, i + 1
            ) (ls.ls_setctr_r, 0) s1.se_eargs.ias_set in
        let seqctr =
          s1.se_eargs.ias_seq
          |> List.map_flatten (fun (x, y) -> [x; y])
          |> List.fold_left
              (fun acc iars ->
                Seq_equal (Seq_var iars, Seq_empty) :: acc
              ) ls.ls_seqctr_r in
        let intctr =
          List.fold_left2
            (fun acc iars iard ->
              let cons = Nc_cons (Apron.Tcons1.EQ, Ne_var iars, Ne_var iard) in
              Log.info "tmp,ctr_r(voidseg): %a" n_cons_fpr cons;
              cons :: acc
            ) ls.ls_ctr_r
            s1.se_sargs.ia_int s1.se_dargs.ia_int in
        { ls with
          ls_setctr_r = setctr;
          ls_seqctr_r = seqctr;
          ls_ctr_r    = intctr;
          ls_cur_r    = seg_edge_rem isr ls.ls_cur_r ;
          ls_rules    = invalidate_rules isl isr Kemp Kseg ls.ls_rules }
      else (* segment not mapped into an empty region *)
        let msg =
          F.asprintf "todo in graph_is_le: segment to non empty: %s=%s> %a"
            s1.se_ind.i_name s1.se_ind.i_name sv_fpr s1.se_dnode in
        raise (FailMsg msg)
  | _ -> Log.fatal_exn "void-seg; improper config"

(* stop rule [par ptr OK, par int KO]
 *   this rule is specific to inductive edge search
 *   when a stop node is encountered (to limit weakening depth) *)
let apply_stop_node_ind (isl: sv) (isr: sv) (ls: le_state): le_state=
  (* We may discard the node left in the right graph, and
   * propagate it as a remainder in the right graph!
   * then, if all remainder is of the form  x.i()  it means
   * we may synthesize a (strong) implication edge.
   * To achieve that, we move right inductive edge into a
   * placeholder (ls_excl_l), to be checked at the end. *)
  if debug_module then
    Log.info "IsLe: reached a stop node, about to stop";
  let nr = node_find isr ls.ls_cur_r in
  match nr.n_e with
  | Hemp | Hpt _ ->
      (* do nothing in this case for now; not sure what to do *)
      ls
  | Hind icr ->
      assert_no_int_arg "stop(ind)" icr.ie_args ;
      let nexcl =
        let fsv (l: sv list): sv list =
          List.map
            (fun i ->
              if debug_module then
                Log.info "apply_stop_node_ind,trying to map %a" sv_fpr i;
              try Nemb.find i ls.ls_cur_svi
              with Not_found -> Log.fatal_exn "stop-node: ptr par not mapped"
            ) l in
        (* mapping arguments: pointers, integers, and collection *)
        let pargs = fsv icr.ie_args.ia_ptr in
        let iargs = fsv icr.ie_args.ia_int in
        let sargs =
          List.map
            (fun i ->
              if debug_module then
                Log.info "apply_stop_node_ind,trying to map %a" sv_fpr i;
              try SvSet.choose (Col_utils.CEmb.find i ls.ls_cur_cvi)
              with Not_found -> Log.fatal_exn "stop-node: ptr par not mapped"
            ) icr.ie_args.ia_set in
        let args = { ia_ptr = pargs ;
                     ia_int = iargs ;
                     ia_set = sargs ;
                     ia_seq = [] } in (* JG:TODO *)
        let g0 = sv_add isl nr.n_t Nnone ls.ls_excl_l in
        let g1 =
          List.fold_left
            (fun accg i -> sv_add i Ntaddr Nnone g0) g0 pargs in
        if debug_module then
          Log.info "apply_stop_node_ind,excl:\n%a" (graph_fpri "  ") g1;
        let ie = { ie_ind  = icr.ie_ind ;
                   ie_args = args } in
        ind_edge_add isl ie g1 in
      { ls with
        ls_cur_r  = ind_edge_rem isr ls.ls_cur_r ;
        ls_excl_l = nexcl }
  | Hseg _ ->
      Log.todo_exn "apply_stop_node_ind: segment"


(** Post inclusion check routine *)
let exp_count_nonemp_lft (ls: le_state): int =
  SvMap.fold
    (fun sv n acc ->
      match n.n_e with
      | Hemp -> acc
      | Hind ie ->
          let nc = Nc_cons (Apron.Tcons1.EQ, Ne_var sv, Ne_csti 0) in
          let bc = ls.ls_vsat_l.vs_num nc in
          if debug_module then
            Log.info "exp_count_nonemp_lfpt,ind_edge: %a; [%b,%b]" nsv_fpr sv
              ie.ie_ind.i_mt_rule bc;
          if bc then acc else acc + 1
      | _ -> acc + 1
    ) ls.ls_cur_l.g_g 0
(* Checks whether a configuration is a success configuration *)
let is_success (ls: le_state): le_state =
  (* TODO: look into the remaining inductive or segment edges in ls_cur_l;
   *  if provably empty, remove *)
  let nnum_l = exp_count_nonemp_lft ls in
  let num_l = num_edges ls.ls_cur_l in
  let num_r = num_edges ls.ls_cur_r in
  if !Flags.flag_dbg_is_le_shape then
    Log.info "%aReturn from is_le: %d(%d) | %d edges" le_state_fpr ls
      num_l nnum_l num_r;
  let num_l = nnum_l in
  if (not ls.ls_emp_both || num_l = 0) && num_r = 0 then
    (* Inclusion established in the graph domain;
     * we now need to look at side predicates *)
    (* first: collect unmapped sv from constraints on right side *)
    let f_unmap (i: sv) (acc: SvSet.t) =
      if Nemb.mem i ls.ls_cur_svi then acc else SvSet.add i acc in
    let non_mapped_svs_r =
      List.fold_left
        (fun acc ctr ->
          Nd_utils.n_cons_fold f_unmap ctr acc
        ) SvSet.empty ls.ls_ctr_r in
    (* for all the non mapped svs in right side, map them to generated fresh
     * svs in left side *)
    let ls =
      SvSet.fold
        (fun sv ls ->
          let typ = (SvMap.find sv ls.ls_cur_r.g_g).n_t in
          let il, g = sv_add_fresh typ Nnone ls.ls_cur_l in
          { ls with
            ls_cur_l   = g;
            ls_fsvs_l  = SvSet.add il ls.ls_fsvs_l;
            ls_cur_svi = Nemb.add sv il ls.ls_cur_svi }
        ) non_mapped_svs_r ls in
    (* now, rename all the constraints on right side (ls_ctr_r) to constraints
     * on left side (ls_ctr_l) *)
    let ls =
      List.fold_left
        (fun ls ctr ->
          try
            let ctr =
              Nd_utils.n_cons_map (fun i -> Nemb.find i ls.ls_cur_svi) ctr in
            { ls with ls_ctr_l = ctr :: ls.ls_ctr_l }
          with
            Not_found -> Log.fatal_exn "nd_instantiation: non_complete sv map"
        ) ls (List.rev ls.ls_ctr_r) in
    let ls = { ls with ls_ctr_r = [] } in
    if !Flags.flag_dbg_is_le_shape then
      begin
        let r1 = ls.ls_seqctr_r != [] in
        let r0 = r1 || ls.ls_setctr_r != [] in
        (* are we interested in ls_ctr_l or ls_ctr_r ??? *)
        if ls.ls_ctr_l != [] then
          Log.info "Left predicates: %d\n%a" (List.length ls.ls_ctr_l)
            (gen_list_items_fpr ~iind:2 n_cons_fpr) ls.ls_ctr_l;
        Log.info "Predicates to look at: %d,%d,%d\n%a%a%a"
          (List.length ls.ls_ctr_r) (List.length ls.ls_setctr_r)
          (List.length ls.ls_seqctr_r)
          (gen_list_items_fpr ~iind:2 ~lret:r0 n_cons_fpr) ls.ls_ctr_r
          (gen_list_items_fpr ~iind:2 ~lret:r1 set_cons_fpr) ls.ls_setctr_r
          (gen_list_items_fpr ~iind:2 seq_cons_fpr) ls.ls_seqctr_r;
        Log.info "Instantiable SVs: { %a }" nsvset_fpr ls.ls_fsvs_l;
        Log.info "Instantiated SVs:";
        SvMap.iter
          (fun sv -> Log.info "    %a => %a" nsv_fpr sv Nd_utils.n_expr_fpr)
          ls.ls_inst_l;
      end;
    (* seperate constraints on left side into two parts: a part without
     * fresh svs, which should be proved satisfied and a part with fresh
     * svs, which can be should for instantiate fresh svs *)
    let ctr_to_prove, ctr_to_inst =
      List.fold_left
        (fun (accp, acci) ctr ->
          let no_freshsvs =
            Nd_utils.n_cons_fold
              (fun i acc -> if SvSet.mem i ls.ls_fsvs_l then false else acc)
              ctr true in
          if no_freshsvs then ctr :: accp, acci
          else accp, ctr :: acci
        ) ([], []) ls.ls_ctr_l in
    let ls = { ls with ls_ctr_l = ctr_to_inst } in
    (* instantiation fresh svs according to constraints on left side *)
    let inst, ctr_to_prove1, ctr_to_inst =
      sv_inst_equalities ls.ls_ctr_l ls.ls_fsvs_l ls.ls_inst_l in
    let ctr_to_prove = ctr_to_prove @ ctr_to_prove1 in
    let ls = { ls with
               ls_inst_l = inst;
               ls_ctr_l  = ctr_to_inst } in
    (* update node mapping and instantiation *)
    let f_l2r (l: sv) (j: sv) (nm: node_embedding): node_embedding =
      let r =
        SvMap.fold (fun k e acc -> if e = l then SvMap.add k j acc else acc)
          nm.n_img nm.n_img in
      { nm with n_img = r } in
    let e_l2r (l: sv) (nm: node_embedding): sv option =
      let r = SvMap.filter (fun k e -> e = l) nm.n_img in
      try Some (fst (SvMap.choose r))
      with Not_found -> None in
    let ls_cur_svi =
      SvMap.fold
        (fun i e cur_i ->
          match e with
          | Ne_var j -> f_l2r i j cur_i
          | _ ->
              begin
                match e_l2r i cur_i with
                | None ->  cur_i
                | Some j -> cur_i
              end
        ) inst ls.ls_cur_svi in
    let ls = { ls with ls_cur_svi = ls_cur_svi } in
    (* Discharging of proof obligations *)
    let l_rem =
      List.fold_left
        (fun acc lctr ->
          let bres = ls.ls_vsat_l.vs_num lctr in
          if !Flags.flag_dbg_is_le_shape then
            Log.info "Verifying num cstr on left: %a => %b"
              Nd_utils.n_cons_fpr lctr bres;
          if bres then acc
          else lctr :: acc
        ) [ ] ctr_to_prove in
    let l_rem =
      List.fold_left
        (fun acc lctr ->
          let bres =
            match lctr with
            | Nc_cons (Apron.Tcons1.DISEQ, Ne_var i, Ne_var j) ->
                ls.ls_gl_diseq i j
            | _ -> false in
          if !Flags.flag_dbg_is_le_shape then
            Log.info "Verifying num constr on left: %a => %b"
              Nd_utils.n_cons_fpr lctr bres;
          if bres then acc
          else lctr :: acc
        ) [ ] l_rem in
    if ls.ls_setctr_r != [] then
      Log.todo "set_ctr_r,len: %d\n%a" (List.length ls.ls_setctr_r)
        (gen_list_items_fpr ~iind:4 Set_utils.set_cons_fpr) ls.ls_setctr_r;
    let set_rem =
      let sv_trans (sv: sv): sv =
        try Graph_utils.Nemb.find sv ls.ls_cur_svi
        with
        | Not_found ->
            Log.info "SV renaming failed (is_le): %a" nsv_fpr sv;
            raise (Unmapped "SV") in
      let setv_trans (setv: sv): sv =
        try
          let s = Col_utils.CEmb.find setv ls.ls_cur_cvi in
          let c = SvSet.cardinal s in
          if c = 0 then raise (Unmapped "SETV-none")
          else if c = 1 then SvSet.max_elt s
          else raise (Unmapped "SETV-multi")
        with
        | Not_found ->
            Log.info "SETV renaming failed (is_le): %a" setv_fpr setv;
            raise (Unmapped (F.asprintf "SETV-not-found: %a" setv_fpr setv)) in
      List.fold_left
        (fun acc ctr ->
          try
            (* Maybe add some renaming here: check! *)
            let tctr = Set_utils.s_cons_map sv_trans setv_trans ctr in
            let bres = ls.ls_vsat_l.vs_set tctr in
            if !Flags.flag_dbg_is_le_shape then
              Log.info "Set-Cstr,Verifying set cstr on left: %a => %b"
                Set_utils.set_cons_fpr tctr bres;
            if bres then acc
            else ctr :: acc
          with
          | exc ->
              Log.info "Set-Cstr,FAIL:\n Verifying set cstr on left: %a\n => %s"
                Set_utils.set_cons_fpr ctr (Printexc.to_string exc);
              ctr :: acc
        ) [ ] ls.ls_setctr_r in
    if ls.ls_seqctr_r != [] then
      Log.todo "seq_ctr_r: %d |=> %d\n"
        (List.length ls.ls_seqctr_r) (List.length set_rem);
    { ls with
      ls_ctr_r    = [ ] ; (* accumulator becomes empty *)
      ls_setctr_r = set_rem ;
      ls_success  = (List.length l_rem = 0) ; }
  else (* Inclusion could not be established in the graph domain *)
    { ls with ls_success  = false }

let reset, output_isle_state =
  let bc, sc = ref 0, ref 0 in
  (fun () -> incr bc; sc := 0),
  fun (ls: le_state) (rule: string) ->
  let filename = Printf.sprintf "%s-isle-%i-%i" (Flags.out_prefix ()) !bc !sc in
  let title = Printf.sprintf "%s-%s" filename rule in
  incr sc;
  let namer sv = raise Not_found in
  List.iter
    (fun (graph, label) ->
      let labels =
        match label with
        | "-inl" ->
            F.asprintf "NODE embedding\n%aCOLV inst\n%aNODE cons:\n%a"
              (Nemb.ne_full_fpri "  ") ls.ls_cur_svi
              (Col_utils.CEmb.ne_full_fpri "  ") ls.ls_cur_cvi
              (gen_list_fpr "" Nd_utils.n_cons_fpr ";") ls.ls_ctr_l
        | "-inr" ->
            F.asprintf
              "NODE cons:\n  %a\nSET cons:\n  %a\nSEQ cons:\n  %a"
              (gen_list_fpr "" Nd_utils.n_cons_fpr "\n  ") ls.ls_ctr_r
              (gen_list_fpr "" Set_utils.set_cons_fpr "\n  ") ls.ls_setctr_r
              (gen_list_fpr "" Seq_utils.seq_cons_fpr "\n  ") ls.ls_seqctr_r
        | _ -> assert false (* impossible case *) in
      let labels = labels
        |> String.split_on_char '\n'
        |> String.concat "\\l" in
      let chan = open_out @@ filename^label^".dot" in
      Graph_visu.pp_graph ~labels title [] [Successors] graph namer chan;
      close_out_noerr chan;
      let cmd = Printf.sprintf "dot -Tpdf %s%s.dot -o %s%s.pdf"
          filename label filename label in
      cmd |> run_command |> ignore
    )
    [ls.ls_cur_l, "-inl"; ls.ls_cur_r, "-inr"]



(** The new inclusion algorithm, with refactored strategy application *)
(* This function is based on a recursive algorithm
 * implementing a worklist on applicable rules (not
 * nodes or edges!) *)
let rec s_is_le_rec (ls: le_state): le_state =
  (* Find out the next rule to apply *)
  match rules_next ls.ls_rules with
  | None ->
      (* indicates there are no remaing rules to apply (we are finished) *)
      (* or maybe we should look for a stop node *)
      if !Flags.flag_dbg_is_le_shape then
        Log.info "IsLe-NoRule:\n%a" le_state_fpr ls;
      ls
  | Some (k, (il, ir), rem_rules) ->
      (* ir is a real node unless k = Rstop *)
      assert (k = Rstop || sv_to_int ir >= 0);
      if !Flags.flag_dbg_is_le_shape then
        begin
          Log.info "%aIsLe-Treating (%a,%a): %a"
            le_state_fpr ls sv_fpr il sv_fpr ir rkind_fpr k;
          if !Flags.flag_dbg_is_le_strategy then
            Log.info "isle-rules ready:\n%a" rules_fpr ls.ls_rules
        end;
      if !Flags.flag_enable_ext_export_all then
        begin
          let rule = F.asprintf "%a-%a-%a" rkind_fpr k sv_fpr il sv_fpr ir in
          output_isle_state ls rule
        end;
      let ls0 = { ls with ls_rules = rem_rules } in
      let ls1 =
        match k with
        (* Stop *)
        | Rstop -> apply_stop_node_ind il ir ls0
        (* Rules that should be reduced *)
        | Rpp -> apply_pt_pt   il ir ls0
        | Rii -> apply_ind_ind il ir ls0
        | Rss -> apply_seg_seg il ir ls0
        | Rsi -> apply_seg_ind il ir ls0
        | Rvs -> apply_void_seg il ir ls0
        (* Unfold rules *)
        | Rei -> s_is_le_unfold true il ir ls0
        | Rps
        | Rpi ->
            try
              s_is_le_unfold false il ir ls0
            with
            | Le_false "unfold: no successful branch" ->
                s_is_le_unfold true il ir ls0 in
      s_is_le_rec ls1
and s_is_le_unfold
    (hint_empty: bool) (* whether to consider empty rules first or last*)
    (il: sv) (ir: sv) (ls: le_state): le_state =
  if !Flags.flag_dbg_is_le_shape then
    Log.info "IsLe triggerring unfolding<emp-hint:%b>" hint_empty;
  let l_mat =
    Graph_materialize.materialize_ind ~submem:ls.ls_submem
      (Some hint_empty) false false ir ls.ls_cur_r in
  if !Flags.flag_dbg_is_le_shape then
    Log.info "IsLe performed unfolding: %d" (List.length l_mat);
  let els =
    List.fold_left
      (fun acc ur ->
        (* only the empty rule of the segment materialization will yield
         * equalities to reduce immediately, and we should not get any here *)
        assert (ur.ur_eqs = SvPrSet.empty);
        match acc with
        | Some _ ->
            (* inclusion already found, no other check *)
            acc
        | None ->
            (* inclusion not found yet; we try current disjunct *)
            if !Flags.flag_dbg_is_le_shape then
              List.iter
                (fun ctr ->
                  Log.info "Predicate to prove on right nodes: %a"
                    Nd_utils.n_cons_fpr ctr
                ) ur.ur_cons;
            try
              let ls0 =
                if !Flags.flag_dbg_is_le_shape then
                  begin
                    Log.info "tmp: s_is_le_unfold, %d, %d"
                      (List.length ur.ur_setcons) (List.length ls.ls_setctr_r);
                    List.iter (fun c -> Log.info "tmp,ctr_r(u): %a" n_cons_fpr c)
                      ur.ur_cons
                  end;
                collect_rules_node_st il ir
                  { ls with
                    ls_cur_r = ur.ur_g ;
                    ls_ctr_r = ur.ur_cons @ ls.ls_ctr_r;
                    ls_setctr_r = ur.ur_setcons @ ls.ls_setctr_r;
                    ls_seqctr_r = ur.ur_seqcons @ ls.ls_seqctr_r;
                  } in
              let ols = s_is_le_rec ls0 in
              let lsuccess = is_success ols in
              if lsuccess.ls_success then
                Some lsuccess
              else
                None
            with
            | Le_false msg ->
                if !Flags.flag_dbg_is_le_shape then
                  Log.info "is_success returned and failed: %s" msg;
                (* underlying test may fail, while next succeeds *)
                (* hence, we catch Le_false and return None here *)
                None
      ) None l_mat in
  match els with
  | None -> raise (Le_false "unfold: no successful branch")
  | Some ls ->
      assert ls.ls_success ;
      ls


(** The main inclusion testing functions *)
(* Basically, trigger the functions above *)
let rec is_le_start (ls: le_state): le_state option =
  (* Iteration *)
  let ols = s_is_le_rec ls in
  if !Flags.flag_enable_ext_export_all then
    begin
      let ls = { ols with
                 ls_cur_l = ls.ls_cur_l ;
                 ls_cur_r = ls.ls_cur_r } in
      output_isle_state ols "end";
      output_isle_state ls "summary"
    end;
  (* Computation of the inclusion check result *)
  let lls = is_success ols in
  if lls.ls_success then
    (* inclusion holds, relation gets forwarded *)
    Some lls
  else
    (* inclusion does not hold, no relation to forward *)
    None

(* The main function for inclusion testing
 *  - inst:
 *    the first argument allows for parameters be marked as instantiable
 *    in the right hand side of an inclusion check performed to enable a
 *    weakening (e.g. in join)
 *  - stop:
 *    allows to use the liveness analysis results in order to guide the
 *    weakening process
 *)
let gen_is_le
    ~(fs: freshset option) (* nodes that may be instantiated *)
    ~(submem: bool)      (* whether sub-memory is_le (no alloc check) *)
    (emp_both: bool)     (* whether both arguments should be fully emptied *)
    (ho: hint_ug option) (* hint, the left argument ("stop" nodes) *)
    (es: SvSet.t)        (* segment end(s), if any *)
    (xl: graph)          (* left input *)
    ~(vsat_l: vsat)      (* satisfiability test function, left arg *)
    (xr: graph)          (* right input *)
    (r: node_emb)        (* injection from right into left *)
    (* injection from right set variables into left set variables *)
    (s_r: colv_emb)
    : le_state option (* extended relation if inclusion proved *) =
  try
    reset ();
    (* Initialization *)
    let lh = Option.map (fun x -> x.hug_live) ho in
    let ni = Nemb.init r in
    let ci = Col_utils.CEmb.init s_r in
    let fs = Option.fold ~none:freshset_empty ~some:(fun x -> x) fs in
    let ils = { ls_cur_l    = xl ;
                ls_cur_r    = xr ;
                ls_cur_svi  = ni ;
                ls_cur_cvi  = ci;
                ls_rules    = rules_init emp_both es ni xl xr r ;
                ls_rem_l    = SvSet.empty ;
                ls_vsat_l   = vsat_l ;
                ls_ctr_l    = [ ];
                ls_fsvs_l   = SvSet.empty;
                ls_inst_l   = SvMap.empty;
                ls_iseg_r   = [ ];
                ls_fresh_seg_r = SvMap.empty;
                ls_ctr_r    = [ ] ;
                ls_setctr_r = [ ] ;
                ls_seqctr_r = [ ] ;
                ls_success  = false ;
                ls_fresh    = fs;
                ls_emp_both = emp_both;
                ls_hint_l   = lh;
                ls_excl_l   = (graph_empty xl.g_inds);
                ls_end_seg  = es;
                ls_submem   = submem;
                ls_gl_diseq = sat_graph_diseq xl; } in
    (* Temporary *)
    if !Flags.flag_dbg_is_le_shape then
      begin
        match ho with
        | None -> Log.info "IsLe: no hint"
        | Some h ->
            Log.info "IsLe: { %a }" (Aa_sets.t_fpr "; " sv_fpr) h.hug_live
      end;
    (* Launch *)
    if not !very_silent_mode then
      Log.info "start is_le";
    let ob = is_le_start ils in
    if not !very_silent_mode then
      Log.info "return is_le %b" (ob != None);
    ob
  with
  | Le_false msg ->
      if not !very_silent_mode then
        Log.info "is_le fails on exception: %s" msg;
      None


(* instantiation of set variables in is_le *)
let col_instantiation (ls: le_state) (g: graph)
    : le_state
    * is_le_setv_inst
    * set_cons list
    * is_le_seqv_inst
    * seq_cons list =
  let loc_debug = false in
  (* initialize instantiation of set variables from set mapping and
   * generate equal set constraints that need to be proved *)
  let inst_set, inst_seq = gen_eq_colctr ls.ls_cur_cvi in
  (* Set specific part of instantiation *)
  let ls, inst_set, set_cons_l, set_cons_prove, n_set =
    let open Set_utils in
    if loc_debug then Log.info "tmp,col_instantiation,ph 0";
    (* first instantiation pass, which only produce new instantiation according
     * to initialize instantiation, this pass will not generate fresh set
     * variables on the left side *)
    let def_set, setcons =
      is_le_instantiate ls.ls_setctr_r inst_set.cvi_def ls.ls_cur_svi.n_img in
    if loc_debug then
      begin
        Log.info "tmp,col_instantiation,ph 1:\n - map:";
        SvMap.iter
          (fun setv se ->
            Log.info "    %a => %a" setv_fpr setv set_expr_fpr se
          ) def_set;
        Log.info " - ctrs: %a\n - fresh: %a"
          (gen_list_fpr "" set_cons_fpr ", ") setcons
          freshset_fpr ls.ls_fresh
      end;
    (* compute a minimal set of non instantiated set variables,
     * that instantiating them could lead to a complete instantiation *)
    let non_inst = check_non_inst setcons def_set in
    let ctr_elt =
      List.fold_left
        (fun acc -> function
          | S_mem (i, S_var j) ->
              let o = try SvMap.find j acc with Not_found -> SvSet.empty in
              SvMap.add j (SvSet.add i o) acc
          | _ -> acc
        ) SvMap.empty setcons in
    let rem =
      SvMap.fold
        (fun sv k acc ->
          if k = CK_set then
            if SvMap.mem sv def_set then acc
            else
              begin
                SvSet.add sv acc
              end
          else acc
        ) ls.ls_fresh.f_col SvSet.empty in
    if loc_debug then
      begin
        Log.info "tmp debug col_instantiation, phase 2:\n - non inst: { %a }"
          (SvSet.t_fpr ",") rem;
        SvSet.iter
          (fun i ->
            Log.info "elt S[%a]: %d elts" sv_fpr i
              (try SvSet.cardinal (SvMap.find i ctr_elt) with Not_found -> 0)
          ) rem
      end;
    if not (SvSet.cardinal rem = 0) then Log.warn "remaining uninst!!!";
    (* map minimal non instantiated set variables to fresh set variables in
     * the left side *)
    let def_map, ls_cur_l, nset =
      gen_fresh_inst non_inst def_set CK_set ls.ls_cur_l in
    let def_set =
      SvMap.fold
        (fun colv_r colv_l acc ->
          assert (not (SvMap.mem colv_r acc));
          SvMap.add colv_r (S_var colv_l) acc)
        def_map def_set in
    let ls = { ls with ls_cur_l = ls_cur_l } in
    if loc_debug then Log.info "tmp,col_instantiation, ph 3";
    (* second instantiation pass, produce a complete instantiation and
     * set constraints that need to be proved *)
    let inst, setcons =
      is_le_instantiate setcons def_set ls.ls_cur_svi.n_img in
    if loc_debug then Log.info "tmp,col_instantiation, ph 4";
    (* rename set constraints to the left side according to instantiation *)
    let setcons_l1 =
      replace_cons setcons inst ls.ls_cur_svi.n_img in
    if loc_debug then Log.info "tmp,col_instantiation, ph 5";
    (* remove set variables from unfolding during inclusion checking *)
    let inst = SvMap.filter (fun k _ -> SvGen.key_is_used g.g_colvkey k) inst in
    if loc_debug then
      begin
        Log.info "tmp,col_instantiation, ph 6: set, (clen: %d, fplen: %d)"
          (List.length inst_set.cvi_cons) (List.length setcons_l1);
        List.iter (fun c -> Log.info "tmp: %a" set_cons_fpr c) setcons_l1
      end;
    let set_cons_l = inst_set.cvi_cons @ setcons_l1 in
    (* filter set constraints with fresh set variables as they cannot be
     * proved, need further instantiation in joining *)
    let set_cons_l, further_prove = split_cons_fresh set_cons_l nset in
    if loc_debug then
      Log.info "tmp,col_instantiation, ph 7: set, (clen: %d, fplen: %d)"
        (List.length set_cons_l) (List.length further_prove);
    ls, inst, set_cons_l, further_prove, nset in
  (* Seq specific part of instantiation *)
  let ls, inst_seq, seq_cons_l, seq_cons_prove, n_seq =
    let open Seq_utils in
    if loc_debug then
      Log.debug "Step 0:\n  seq_ctr_r:\n    %a\nseq_def:\n  %a"
        (gen_list_fpr "_" seq_cons_fpr "\n    ") ls.ls_seqctr_r
        (SvMap.t_fpr "\n    " seq_expr_fpr) inst_seq.cvi_def;
    (* first instantiation pass, which only produce new instantiation according
     * to initialize instantiation, this pass will not generate fresh seq
     * variables on the left side *)
    let def_seq, seqcons =
      is_le_instantiate ls.ls_seqctr_r inst_seq.cvi_def ls.ls_cur_svi.n_img in
    (* compute a minimal set of non instantiated set variables,
     * that instantiating them could lead to a complete instantiation*)
    let non_inst = check_non_inst seqcons def_seq in
    if loc_debug then
      Log.debug
        "Step 1 %s:\n  seqcons:\n    %a\n  def_seq:\n    %a\n  non_inst: %a"
        "After first round of instanciation; Some seqv to instantiate"
        (gen_list_fpr "_" seq_cons_fpr "\n    ") seqcons
        (SvMap.t_fpr "\n    " seq_expr_fpr) def_seq
        seqvset_fpr non_inst;
    (* map minimal  non instantiated set variables to fresh set variables in
     * the left side *)
    let def_map, ls_cur_l, nset =
      gen_fresh_inst non_inst def_seq CK_seq ls.ls_cur_l in
    if loc_debug then
      Log.debug "Step 2 %s:\n  def_seq:\n  %a\n  nset: %a"
        "Afetr adding fresh variable"
        (SvMap.t_fpr "\n    " seqv_fpr) def_map
        seqvset_fpr non_inst;
    let def_seq =
      SvMap.fold
        (fun colv_r colv_l acc ->
          assert (not (SvMap.mem colv_r acc));
          SvMap.add colv_r (Seq_var colv_l) acc)
        def_map def_seq in
    let ls = { ls with ls_cur_l = ls_cur_l } in
    (* second instantiation pass, produce a complete instantiation and
     * set constraints that need to be proved *)
    let inst, seqcons =
      is_le_instantiate seqcons def_seq ls.ls_cur_svi.n_img in
    if loc_debug then
      Log.debug "Step 3 %s:\n  seqcons:\n    %a\n  def_seq:\n    %a"
        "After second round of instanciation; ALL seqv msut be ok"
        (gen_list_fpr "_" seq_cons_fpr "\n    ") seqcons
        (SvMap.t_fpr "\n    " seq_expr_fpr) def_seq;
    (* rename set constraints to the left side according to instantiation *)
    let seqcons_l1 = replace_cons seqcons inst ls.ls_cur_svi.n_img in
    if loc_debug then
      Log.debug "Step 4 %s:\n  seqcons:\n    %a\n  n_img:\n    %a"
        "After translating ALL seqcons"
        (gen_list_fpr "_" seq_cons_fpr "\n    ") seqcons_l1
        (SvMap.t_fpr "\n    " sv_fpr) ls.ls_cur_svi.n_img;
    (* remove set variables from unfolding during inclusion checking*)
    let inst = SvMap.filter (fun k _ -> SvGen.key_is_used g.g_colvkey k) inst in
    let seq_cons_l = inst_seq.cvi_cons @ seqcons_l1 in
    (* filter set constraints with fresh set variables as they cannot be
     * proved, need further instantiation in joining *)
    let seq_cons_l, further_prove = split_cons_fresh seq_cons_l nset in
    if loc_debug then
      Log.debug "Step 5 %s:\n  seqcons_:\n    %a\n  further_prove:\n    %a"
        "After spliting seqcons"
        (gen_list_fpr "_" seq_cons_fpr "\n    ") seq_cons_l
        (gen_list_fpr "_" seq_cons_fpr "\n    ") further_prove;
    ls, inst, seq_cons_l, further_prove, nset in
  ls,
  { cvi_def  = inst_set;
    cvi_cons = set_cons_prove;
    cvi_new  = n_set }, set_cons_l,
  { cvi_def  = inst_seq;
    cvi_cons = seq_cons_prove;
    cvi_new  = n_seq }, seq_cons_l

(* guess constraints among set parameters and numerical parameters
 * input graph
 * Note: this function does not handle collection variables; TODO: check *)
let guess_cons (xl: graph) (ls: le_state): le_state =
  if !Flags.flag_dbg_is_le_shape then
    Log.info "Guess_cons,call";
  let ls_iind_l =
    List.map
      (fun se ->
        if se.se_dargs.ia_set != [] || se.se_sargs.ia_set != [] then
          Log.error "guess_cons: sets";
        if se.se_dargs.ia_seq != [] || se.se_sargs.ia_seq != [] then
          Log.error "guess_cons: seqs";
        let id = Nemb.find se.se_dnode ls.ls_cur_svi in
        let ie =
          let ptrs =
            List.map (fun i -> Nemb.find i ls.ls_cur_svi) se.se_dargs.ia_ptr in
          let ints =
            List.map (fun i -> Nemb.find i ls.ls_cur_svi) se.se_dargs.ia_int in
          { ie_ind  = se.se_ind;
            ie_args = { ia_ptr = ptrs;
                        ia_int = ints;
                        ia_set = [];
                        ia_seq = [] } } in
        (id, ie)
      ) ls.ls_iseg_r in
  let merge_g (gi: graph) (go: graph): graph =
    let g =
      SvMap.fold
        (fun id node acc ->
          if SvMap.mem id acc then acc
          else SvMap.add id node acc
        ) go.g_g gi.g_g in
    { gi with
      g_nkey   = go.g_nkey;
      g_g      = g;
      g_svemod = go.g_svemod } in
  let creat_g (id: sv) (ie: ind_edge): graph =
    let g0 = sv_add id Ntaddr Nnone (graph_empty xl.g_inds) in
    let f_buildpars = List.fold_left (fun acc i -> SvSet.add i acc) in
    let ia_nodes = f_buildpars SvSet.empty ie.ie_args.ia_int in
    let pa_nodes = f_buildpars SvSet.empty ie.ie_args.ia_ptr in
    let sa_nodes = f_buildpars SvSet.empty ie.ie_args.ia_set in
    let g0 = SvSet.fold (fun i -> sv_add i Ntint Nnone) ia_nodes g0 in
    let g0 = SvSet.fold (fun i -> sv_add i Ntaddr Nnone) pa_nodes g0 in
    let g0 = SvSet.fold (fun i -> colv_add i CK_set) sa_nodes g0 in
    ind_edge_add id ie g0 in
  let init_inj (id: sv) (ie: ind_edge): node_embedding * colv_embedding =
    let nm = Nemb.add id id Nemb.empty in
    let nm =
      List.fold_left (fun acc i -> Nemb.add i i acc) nm ie.ie_args.ia_ptr in
    let nm =
      List.fold_left (fun acc i -> Nemb.add i i acc) nm ie.ie_args.ia_int in
    let sm =
      List.fold_left
        (fun acc i -> CEmb.add i i CK_set acc) CEmb.empty ie.ie_args.ia_set in
    nm, sm in
  let r =
    List.fold_left
      (fun ls (id, ie) ->
        if ie.ie_args.ia_int = [] then ls
        else
          let ls_cur_svi, ls_cur_cvi = init_inj id ie in
          let ls_cur_l = merge_g xl ls.ls_cur_l in
          let ls_cur_r = creat_g id ie in
          let ls_a =
            { ls with
              ls_cur_l       = ls_cur_l;
              ls_cur_r       = ls_cur_r;
              ls_cur_svi     = ls_cur_svi;
              ls_cur_cvi     = ls_cur_cvi;
              ls_rules       = empty_rules;
              ls_ctr_r       = [];
              ls_setctr_r    = [];
              ls_iseg_r      = [];
              ls_fresh_seg_r = SvMap.empty;
              ls_success     = false;
              ls_emp_both    = false; } in
          let ls_a = collect_rules_node_st id id ls_a in
          match is_le_start ls_a with
          | Some lls -> lls
          | None -> ls
          | exception _ -> ls
      ) ls ls_iind_l in
  if !Flags.flag_dbg_is_le_shape then
    Log.info "Guess_cons,return";
  r

(* TODO: check whether it is possible to refactor this part of the code a bit *)
let do_int_wktyp (xr: graph) (ls: le_state): int_par_rec SvMap.t =
  (* type integer parameters on right graph *)
  let type_iargs_r =
    int_par_type_iargs_g xr (fun ir -> Nemb.find ir ls.ls_cur_svi) in
  let type_iargs_l =
    SvMap.fold
      (fun ir wk acc ->
        let il = Nemb.find ir ls.ls_cur_svi in
        if SvSet.mem il ls.ls_fsvs_l then
          try SvMap.add il (merge_int_par_type (SvMap.find il acc) wk) acc
          with Not_found -> SvMap.add il wk acc
        else acc
      ) type_iargs_r SvMap.empty in
  SvMap.fold
    (fun id ie acc ->
      int_par_type_iargs_e ie.ie_args ie.ie_ind.i_pkind.pr_int acc
    ) ls.ls_fresh_seg_r type_iargs_l


(* The main function for inclusion testing:
 * used for checking stabilization of abstract iterates
 *
 *  - stop:
 *    allows to use the liveness analysis results in order to guide the
 *    weakening process
 *)
let is_le
    ~(submem: bool)      (* whether sub-memory is_le (no alloc check) *)
    (xl: graph)          (* left input *)
    (ho: hint_ug option) (* hint, the left argument ("stop" nodes) *)
    ~(vsat_l: vsat)           (* satisfiability test function, left arg *)
    (xr: graph)          (* right input *)
    (r: node_emb)        (* injection from right into left *)
    (s_r: colv_emb)        (* injection from right set vars into left *)
    : (sv SvMap.t       (* extended relation if inclusion proved *)
         * set_expr SvMap.t (* instantiated constraints of Set COLVs *)
         * seq_expr SvMap.t (* instantiated constraints of Seq COLVs *)
         * sv_inst           (* sv instantiation  *)
      ) option  =
  match gen_is_le ~fs:None ~submem true ho SvSet.empty xl ~vsat_l xr r s_r with
  | None -> None
  | Some ls ->
      if num_edges ls.ls_excl_l = 0 then
        let ls_r = guess_cons xl ls in
        let ls = { ls with
                   ls_ctr_l  = ls_r.ls_ctr_l;
                   ls_fsvs_l = ls_r.ls_fsvs_l;
                   ls_inst_l = ls_r.ls_inst_l } in
        (* sv instantiation according to eq numerical constraints *)
        let ie =
          SvMap.fold (fun k _ acc -> SvSet.add k acc)
            ls.ls_inst_l SvSet.empty in
        let sv_inst =
          { sv_inst_empty with
            sv_fresh = ls.ls_fsvs_l;
            sv_ie    = ie;
            sv_eqs   = ls.ls_inst_l;
            sv_cons  = ls.ls_ctr_l; } in
        let sv_inst = sv_instantiation sv_inst ls.ls_vsat_l.vs_num in
        let sv_inst = do_sv_inst_left_ctrs sv_inst ls.ls_vsat_l.vs_num in
        let typed_fresh_svl = do_int_wktyp xr ls in
        let sv_inst =
          typed_sv_instantiation sv_inst typed_fresh_svl ls.ls_vsat_l.vs_num in
        assert (sv_inst.sv_cons = []);
        if !Flags.flag_dbg_is_le_shape then
          Log.info "is_le,instantiation of symbolic variables:\n%a"
            (sv_inst_fpri "  ") sv_inst;
        let ls = { ls with
                   ls_inst_l = sv_inst.sv_eqs;
                   ls_ctr_l = sv_inst.sv_cons } in
        let ls, set_inst, set_cons_l, seq_inst, seq_cons_l =
          col_instantiation ls xr in
        if !Flags.flag_dbg_is_le_shape then
          begin
            Log.info "is_le,instantiation of set variables:";
            SvMap.iter
              (fun i sr ->
                Log.info "  S[%a]->%a" sv_fpr i Set_utils.set_expr_fpr sr
              ) set_inst.cvi_def;
            List.iter (fun sc -> Log.info "? %a" set_cons_fpr sc) set_cons_l;
            Log.info "is_le,instantiation of seq variables:";
            SvMap.iter
              (fun i sr ->
                Log.info "  Q[%a]->%a" sv_fpr i Seq_utils.seq_expr_fpr sr
              ) seq_inst.cvi_def;
            List.iter (fun sc -> Log.info "? %a" seq_cons_fpr sc) seq_cons_l;
          end;
        if List.for_all ls.ls_vsat_l.vs_set set_cons_l
            && set_inst.cvi_cons = []
            && List.for_all ls.ls_vsat_l.vs_seq seq_cons_l
            && seq_inst.cvi_cons = []
            && set_inst.cvi_new = SvSet.empty
            && seq_inst.cvi_new = SvSet.empty then
          begin
            if !Flags.flag_dbg_is_le_shape then
              Log.info "is_le,conditions cleared, returning positive result";
            Some (ls.ls_cur_svi.n_img, set_inst.cvi_def,
                  seq_inst.cvi_def, sv_inst)
          end
        else
          begin
            if !Flags.flag_dbg_is_le_shape then
              Log.info "is_le,conditions failed";
            None
          end
      else Log.fatal_exn "is_le did not completely consume right argument"


(* Partial inclusion test:
 * used for weakening graphs (join, directed_weakening, graph_abs)
 * used for verifying assertions
 *
 * used for weakening graphs (join, directed_weakening, graph_abs)
 *  - inst:
 *    nodes that can be instantiated are the integer parameters in the right
 *    hand side (they are used for weakening)
 *  - stop:
 *    allows to use the liveness analysis results in order to guide the
 *    weakening process
 *)
let is_le_partial
    ~(fs: freshset option) (* nodes that may be instantiated *)
    ~(submem: bool)      (* whether sub-memory is_le (no alloc check) *)
    (search_ind: bool)   (* whether to search for an inductive / a segment *)
    (xl: graph)          (* left input *)
    (ho: hint_ug option) (* hint, the left argument ("stop" nodes) *)
    (es: SvSet.t)       (* segment end(s), if any *)
    ~(vsat_l: vsat)           (* satisfiability test function, left arg *)
    (xr: graph)          (* right input *)
    (r: node_emb)        (* injection from right into left *)
    (s_r: colv_emb)        (* injection from right set vars into left *)
    : is_le_res (* result, left remainder, and possibly right segment *) =
  match gen_is_le ~fs ~submem false ho es xl ~vsat_l xr r s_r with
  | None -> Ilr_not_le
  | Some ls ->
      if search_ind then
        (* Tries to extract *one* *inductive* edge from graph *)
        match ind_edge_extract_single ls.ls_excl_l with
        | many, None ->
            (* all the right argument was consumed
             * likely means an inductive edge will be synthesized *)
            if !Flags.flag_dbg_is_le_shape then
              Log.info "IsLe partial: inductive found";
            if many then
              Log.fatal_exn
                "IsLe partial for local abstraction; a lot of stuff left"
            else (* left graph is empty, weaken to ind succeeded *)
              Ilr_le_ind ls.ls_cur_l
        | b, Some (ir, ie) ->
            if b then
              begin
                (* there was more than a single inductive edge left
                 * or other edges remained; this is a failure case *)
                if !Flags.flag_dbg_is_le_shape then
                  Log.info "IsLe partial: several inductives ?";
                Ilr_not_le
              end
            else
              (* there was exactly one inductive edge left to match
               * likely means a segment will be synthesized *)
              begin
                if !Flags.flag_dbg_is_le_shape then
                  Log.info "IsLe partial: segment found";
                Ilr_le_seg (ls.ls_cur_l, ir, ie, ls.ls_cur_svi.n_img)
              end
      else
        begin
          (* sv instantiation according to eq numerical constraints *)
          let ie =
            SvMap.fold (fun k _ acc -> SvSet.add k acc) ls.ls_inst_l
              SvSet.empty in
          let sv_inst =
            { sv_inst_empty with
              sv_fresh = ls.ls_fsvs_l;
              sv_ie    = ie;
              sv_eqs   = ls.ls_inst_l;
              sv_cons  = ls.ls_ctr_l; } in
          let sv_inst  = sv_instantiation sv_inst ls.ls_vsat_l.vs_num in
          if !Flags.flag_dbg_is_le_shape then
            Log.info "is_le_partial,instantiation of symbolic variables:\n%a"
              (sv_inst_fpri "  ") sv_inst;
          let ls = { ls with
                     ls_inst_l = sv_inst.sv_eqs;
                     ls_ctr_l  = sv_inst.sv_cons } in
          let ls, set_inst, set_cons_l, seq_inst, seq_cons_l =
            col_instantiation ls xr in
          (* Logging the COLV instatiation part *)
          if !Flags.flag_dbg_is_le_shape then
            begin (* TODO share with similar printers *)
              Log.info "is_le_partial,instantiation of set variables:";
              SvMap.iter
                (fun i sr ->
                  Log.info "  S[%a]->%a" sv_fpr i Set_utils.set_expr_fpr sr
                ) set_inst.cvi_def;
              Log.info "Set predicates to prove: %a"
                (gen_list_fpr "" Set_utils.set_cons_fpr ";") set_cons_l;
              Log.info "Set predicates for further prove:";
              List.iter (fun sc -> Log.info "  %a" Set_utils.set_cons_fpr sc)
                set_inst.cvi_cons;
              Log.info "is_le_partial,instantiation of seq variables:";
              SvMap.iter
                (fun i sr ->
                  Log.info "Q[%a]->%a\n" sv_fpr i Seq_utils.seq_expr_fpr sr
                ) seq_inst.cvi_def;
              Log.info "Seq predicates to prove:";
              List.iter (fun sc -> Log.info "  %a" Seq_utils.seq_cons_fpr sc)
                seq_cons_l;
              Log.info "Seq predicates for further prove:";
              List.iter (fun sc -> Log.info "  %a" Seq_utils.seq_cons_fpr sc)
                seq_inst.cvi_cons
            end;
          (* prove set predicates *)
          (* We compute the list of col constraints that CANNOT be proved *)
          (* TODO: fix, this code seems never used;
           *  except when doing a seg-seg++ rule;
           *  instead, just return the set of not proved constraints *)
          let nonproved_set_l =
            List.filter
              (fun cons -> not (ls.ls_vsat_l.vs_set cons)) set_cons_l in
          if !Flags.flag_dbg_is_le_shape then
            if nonproved_set_l = [] then
              Log.info "All set constraints are proved"
            else
              Log.info "Some set constraints are not proved\n  %a"
                (gen_list_fpr "" Set_utils.set_cons_fpr "\n  ") nonproved_set_l;
          (* prove seq predicates *)
          let nonproved_seq_l =
            List.filter
              (fun cons -> not (ls.ls_vsat_l.vs_seq cons)) seq_cons_l in
          if !Flags.flag_dbg_is_le_shape then
            if nonproved_seq_l = [] then
              Log.info "All seq constraints are proved"
            else
              Log.info "Some seq constraints are not proved\n  %a"
                (gen_list_fpr "" Seq_utils.seq_cons_fpr "\n  ") nonproved_seq_l;
          (* returning the result *)
          if (*nonproved_set_l = [] &&*) nonproved_seq_l = [] then
            Ilr_le_rem { ilr_graph_l  = ls.ls_cur_l;
                         ilr_svrem_l  = ls.ls_rem_l;
                         ilr_svrtol   = ls.ls_cur_svi.n_img;
                         ilr_colvrtol = CEmb.direct_image ls.ls_cur_cvi;
                         ilr_svinst   = sv_inst;
                         ilr_setinst  = set_inst;
                         ilr_seqinst  = seq_inst;
                         (* TODO why to return *all* constraints
                          * not just non proved ones ??? *)
                         ilr_setctr_r = ls.ls_setctr_r;
                         ilr_seqctr_r = nonproved_seq_l }
          else
            Ilr_not_le
        end
