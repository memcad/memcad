(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: gen_join.ml
 **       Generic elements used in the join algorithm
 ** Xavier Rival, 2014/03/04 *)
open Data_structures
open Lib
open Sv_def

open Gen_dom

open Graph_sig
open Nd_sig

open Graph_utils
open Nd_utils
open Sv_utils

(** Sided algorithms support *)

(* Display *)
let side_to_char: side -> char = function
  | Lft -> 'l'
  | Rgh -> 'r'
let side_fpr (fmt: form): side -> unit = function
  | Lft -> F.fprintf fmt "left"
  | Rgh -> F.fprintf fmt "right"
(* Swap side *)
let other_side: side -> side = function
  | Lft -> Rgh
  | Rgh -> Lft
(* Side element extraction *)
let sel_side (s: side) (x, y) =
  match s with
  | Lft -> x
  | Rgh -> y
let get_sides (s: side) (x, y) =
  match s with
  | Lft -> y, x
  | Rgh -> x, y

(** Error report *)
module Log =
  Logger.Make(struct let section = "gen_join" and level = Log_level.DEBUG end)

(** Kinds of rules in the join algorithm *)
type rkind =
  | Rpp | Rii         (* matching of pairs of pt/inductive edges *)
  | Rsegintro of side (* introduction of a segment *)
  | Rsegext           (* extension of a segment *)
  | Riweak            (* inductive in the left; weaken in the right graph *)
  | Rweaki            (* inductive in the right; weaken in the left graph *)
  | Rindintro         (* introduction of an inductive *)
  | Rsplitind of side (* seperate an inductive edge by introducing a segment *)
  | Rsegext_ext       (* extension of segments on both sides *)

(** Status, with set of applicable rules *)
type instances = SvPrSet.t
type rules =
    { (** Classical rules of separation logic with inductives *)
      r_pt_prio:    instances; (* prioretary points-to edges *)
      r_pt_pt:      instances; (* matching a pair of points-to edges *)
      r_ind_ind:    instances; (* matching a pair of inductive edges *)
      r_weak_ind:   instances; (* weaken into an inductive (left) *)
      r_ind_weak:   instances; (* weaken into an inductive (right) *)
      r_segintro_l: instances; (* introduction of a segment (left is empty) *)
      r_segintro_r: instances; (* introduction of a segment (right is empty) *)
      r_segext:     instances; (* extension of a segment *)
      r_indintro:   instances; (* introduction of an inductive *)
      (** Rules based on more global rewrittings of formulas *)
      (* split an inductive edge to a segment and inductive edge
       * (right is empty) *)
      r_splitind_l: instances;
      (* split an inductive edge to a segment and inductive edge
       * (left is empty) *)
      r_splitind_r: instances;
      (* extension of segments on both sides *)
      r_segext_ext: instances }

(** Utilities and Pretty-printing *)
(* Emmpty set of rules *)
let empty_rules = { r_pt_prio    = SvPrSet.empty ;
                    r_pt_pt      = SvPrSet.empty ;
                    r_ind_ind    = SvPrSet.empty ;
                    r_weak_ind   = SvPrSet.empty ;
                    r_ind_weak   = SvPrSet.empty ;
                    r_segintro_l = SvPrSet.empty ;
                    r_segintro_r = SvPrSet.empty ;
                    r_segext     = SvPrSet.empty ;
                    r_indintro   = SvPrSet.empty ;
                    r_splitind_l = SvPrSet.empty ;
                    r_splitind_r = SvPrSet.empty ;
                    r_segext_ext = SvPrSet.empty ; }
(* Pretty-printing *)
let rkind_fpr (fmt: form): rkind -> unit = function
  | Rpp         -> F.fprintf fmt "pt-pt"
  | Rii         -> F.fprintf fmt "ind-ind"
  | Rsegintro s -> F.fprintf fmt "seg-intro-%c" (side_to_char s)
  | Rsegext     -> F.fprintf fmt "seg-ext"
  | Riweak      -> F.fprintf fmt "ind-weak"
  | Rweaki      -> F.fprintf fmt "weak-ind"
  | Rindintro   -> F.fprintf fmt "ind-intro"
  | Rsplitind s -> F.fprintf fmt "split-ind-%c" (side_to_char s)
  | Rsegext_ext -> F.fprintf fmt "seg_ext_ext"
let rules_fpr (fmt: form) (r: rules): unit =
  let instances_fpr (fmt: form) ((name, s): string * SvPrSet.t): unit =
    if s != SvPrSet.empty then
      F.fprintf fmt "%s %a\n" name svprset_fpr s in
  F.fprintf fmt "%a%a%a%a%a%a%a%a%a%a%a%a"
    instances_fpr ("pt-prio:     ", r.r_pt_prio)
    instances_fpr ("pt-pt:       ", r.r_pt_pt)
    instances_fpr ("ind-ind:     ", r.r_ind_ind)
    instances_fpr ("weak-ind:    ", r.r_weak_ind)
    instances_fpr ("ind-weak:    ", r.r_ind_weak)
    instances_fpr ("seg-intro-l: ", r.r_segintro_l)
    instances_fpr ("seg-intro-r: ", r.r_segintro_r)
    instances_fpr ("seg-ext:     ", r.r_segext)
    instances_fpr ("ind-intro:   ", r.r_indintro)
    instances_fpr ("split-ind-l: ", r.r_splitind_l)
    instances_fpr ("split-ind-r: ", r.r_splitind_r)
    instances_fpr ("seg_ext_ext: ", r.r_segext_ext)

(** Strategy function, returning the next applicable rule *)
(* current strategy:
 *  1. pt-prio
 *  2. segext_ext
 *  3. ind-ind
 *  4. seg-intro-l
 *     seg-intro-r
 *  5. seg-ext
 *  6. weak-ind
 *     ind-weak
 *  7. pt-pt
 *  8. ind-intro
 *  9. splitind_l
 *     splitind_r *)
let rules_next (r: rules): (rkind * (sv * sv) * rules) option =
  if r.r_pt_prio != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_pt_prio in
    Some (Rpp, p,
          { r with r_pt_prio = SvPrSet.remove p r.r_pt_prio })
  else if r.r_segext_ext != SvPrSet.empty then
    (* used to be after ind-ind and seg-intro *)
    let p = SvPrSet.min_elt r.r_segext_ext in
    Some (Rsegext_ext, p,
          { r with r_segext_ext = SvPrSet.remove p r.r_segext_ext })
  else if r.r_ind_ind != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_ind_ind in
    Some (Rii, p,
          { r with r_ind_ind = SvPrSet.remove p r.r_ind_ind })
  else if r.r_segintro_l != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_segintro_l in
    Some (Rsegintro Lft, p,
          { r with r_segintro_l = SvPrSet.remove p r.r_segintro_l })
  else if r.r_segintro_r != SvPrSet.empty then
    (* introduction of a segment from empty region in the right graph:
     * this may not be the right place for this rule *)
    let p = SvPrSet.min_elt r.r_segintro_r in
    Some (Rsegintro Rgh, p,
          { r with r_segintro_r = SvPrSet.remove p r.r_segintro_r })
  else if r.r_segext != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_segext in
    Some (Rsegext, p,
          { r with r_segext = SvPrSet.remove p r.r_segext })
  else if r.r_ind_weak != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_ind_weak in
    Some (Riweak, p,
          { r with r_ind_weak = SvPrSet.remove p r.r_ind_weak })
  else if r.r_weak_ind != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_weak_ind in
    Some (Rweaki, p,
          { r with r_weak_ind = SvPrSet.remove p r.r_weak_ind })
  else if r.r_pt_pt != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_pt_pt in
    Some (Rpp, p,
          { r with r_pt_pt = SvPrSet.remove p r.r_pt_pt })
  else if r.r_indintro != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_indintro in
    Some (Rindintro, p,
          { r with r_indintro = SvPrSet.remove p r.r_indintro })
  else if r.r_splitind_l != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_splitind_l in
    Some (Rsplitind Lft, p,
          { r with r_splitind_l = SvPrSet.remove p r.r_splitind_l })
  else if r.r_splitind_r != SvPrSet.empty then
    let p = SvPrSet.min_elt r.r_splitind_r in
    Some (Rsplitind Rgh, p,
          { r with r_splitind_r = SvPrSet.remove p r.r_splitind_r })
  else None

(** Collecting applicable rules *)
let collect_rules_sv_gen
    (is_prio: sv * sv -> bool) (* whether pt rules are prioritary (init) *)
    (is_seg: bool)  (* whether candidate for extension as segments *)
    (is_seg_intro: bool)         (* whether seg intro *)
    (jrel: node_relation)        (* node relation *)
    (il: sv) (kl: region_kind)  (* left node  *)
    (ir: sv) (kr: region_kind)  (* right node *)
    (jr: rules): rules =
  if !Flags.flag_dbg_join_strategy then
    Log.force "searching for rules at %a,%a" sv_fpr il sv_fpr ir;
  (* Searching for siblings (only in the right graph) *)
  let detect_siblings (sd: side) (i: sv) (jr: rules): rules =
    let sib = Nrel.siblings sd i jrel in
    if !Flags.flag_dbg_join_shape then
      Log.force "Siblings %a at %a (%d)[%b]: %a"
        side_fpr sd sv_fpr i (SvSet.cardinal sib)
        (SvSet.cardinal sib > 1) (SvSet.t_fpr "; ") sib;
    if SvSet.cardinal sib > 1 then
      match sd with
      | Lft ->
          let l =
            SvSet.fold (fun j -> SvPrSet.add (i, j)) sib jr.r_segintro_l in
          { jr with r_segintro_l = l }
      | Rgh ->
          let l =
            SvSet.fold (fun j -> SvPrSet.add (j, i)) sib jr.r_segintro_r in
          { jr with r_segintro_r = l }
    else jr in
  if is_seg then
    { jr with r_segext_ext = SvPrSet.add (il, ir) jr.r_segext_ext }
  else
    let jr = detect_siblings Lft il jr in
    let jr = detect_siblings Rgh ir jr in
    (* Regular rules *)
    let nrules =
      match kl, kr with
      (* empty nodes, no rule to add *)
      | Kemp, Kemp -> jr
      (* usual atomic rules *)
      | Kpt , Kpt  ->
          if is_prio (il, ir) && not is_seg_intro then
            { jr with r_pt_prio = SvPrSet.add (il, ir) jr.r_pt_prio }
          else
            { jr with r_pt_pt = SvPrSet.add (il, ir) jr.r_pt_pt }
      | Kind, Kind ->
          { jr with r_ind_ind = SvPrSet.add (il, ir) jr.r_ind_ind }
      | Kemp, Kind
      | Kpt , Kind ->
          { jr with r_weak_ind = SvPrSet.add (il, ir) jr.r_weak_ind }
      | Kind, Kemp
      | Kind, Kpt  ->
          { jr with r_ind_weak = SvPrSet.add (il, ir) jr.r_ind_weak }
      | Kseg, Kseg
      | Kseg, Kpt
      | Kpt , Kseg ->
          { jr with r_segext = SvPrSet.add (il, ir) jr.r_segext }
      (* cases with no rule to add, no relevant weakening;
       * those edges may get discared due to siblings (other nodes in
       * relation with the same nodes) *)
      | Kemp, Kpt
      | Kpt, Kemp ->
          { jr with r_indintro = SvPrSet.add (il, ir) jr.r_indintro }
      | Kemp, Kseg
      | Kseg, Kemp ->
          if !Flags.flag_dbg_join_strategy then
            Log.force "cancelling seg-emp";
          jr
      | Kind, Kseg ->
          { jr with r_ind_weak = SvPrSet.add (il, ir) jr.r_ind_weak }
      | Kseg, Kind ->
          { jr with r_weak_ind = SvPrSet.add (il, ir) jr.r_weak_ind } in
    nrules

(** Invalidation of rules that were performed or disabled, by the application
 ** of another rule *)
(* Kinds of elements to invalidate *)
type invalid =
  | Ipt                (* a points to region *)
  | Iind               (* an inductive predicate *)
  | Inone              (* nothing *)
  | Iblob of SvSet.t  (* a memory blob *)
  | Isiblings          (* siblings of a node *)
(* Case of a local rule *)
let invalidate_rules
    (isl: sv) (isr: sv) (invl: invalid) (invr: invalid) (r: rules): rules =
  (* Map over the rules type *)
  let map_rules (f: string -> SvPrSet.t -> SvPrSet.t) (r: rules): rules =
    { r_pt_prio    = f "pt-prio"   r.r_pt_prio;
      r_pt_pt      = f "pt-pt"     r.r_pt_pt;
      r_ind_ind    = f "ind-ind"   r.r_ind_ind;
      r_weak_ind   = f "wk-ind"    r.r_weak_ind;
      r_ind_weak   = f "ind-wk"    r.r_ind_weak;
      r_segintro_l = r.r_segintro_l; (* no invalidate on seg-intro *)
      r_segintro_r = r.r_segintro_r; (* no invalidate on seg-intro *)
      r_segext     = f "seg-ext"   r.r_segext;
      r_indintro   = f "ind-intro" r.r_indintro;
      r_splitind_l = r.r_splitind_l; (* no invalidate in sep-ind *)
      r_splitind_r = r.r_splitind_r; (* no invalidate in sep-ind *)
      r_segext_ext = f "segext-ext" r.r_segext_ext } in
  (* Invalidate rules over a set *)
  let invalidate (il, ir) (msg: string) (s: SvPrSet.t): SvPrSet.t =
    if !Flags.flag_dbg_join_strategy then
      Log.force "invalidating %s join rule at (%a,%a)" msg sv_fpr il sv_fpr ir;
    SvPrSet.remove (il, ir) s in
  let invalidate_map
      (f: (sv * sv) -> bool) (msg: string) (s: SvPrSet.t): SvPrSet.t =
    SvPrSet.fold
      (fun p acc ->
        if f p then invalidate p msg acc
        else acc
      ) s s in
  (* Rules consuming *one* node in each side *)
  let invalidate_l  = invalidate_map (fun (x, _) -> x = isl)
  and invalidate_r  = invalidate_map (fun (_, y) -> y = isr)
  and invalidate_lr = invalidate_map (fun (x, y) -> x = isl || y = isr) in
  (* Rules consuming a blob in one side *)
  let invalidate_nb (s: SvSet.t) =
    map_rules (invalidate_map (fun (_, y) -> SvSet.mem y s)) in
  let invalidate_bn (s: SvSet.t) =
    map_rules (invalidate_map (fun (x, _) -> SvSet.mem x s)) in
  let invalidate_bb (sl: SvSet.t) (sr: SvSet.t) =
    map_rules
      (invalidate_map (fun (x, y) -> SvSet.mem x sl || SvSet.mem y sr)) in
  match invl, invr with
  | Ipt, Ipt ->
      { r with
        r_pt_prio = invalidate_lr "pt-prio" r.r_pt_prio;
        r_pt_pt   = invalidate_lr "pt-pt" r.r_pt_pt }
  | Iind, Iind ->
      { r with
        r_ind_ind  = invalidate_lr "ind-ind" r.r_ind_ind;
        r_weak_ind = invalidate_r  "weak-ind" r.r_weak_ind;
        r_ind_weak = invalidate_l  "ind-weak" r.r_ind_weak }
  | Inone   , Iblob br -> invalidate_nb br r
  | Iblob bl, Iblob br -> invalidate_bb bl br r
  | Iblob bl, Inone    -> invalidate_bn bl r
  | Isiblings, Inone -> r (* no invalidate on seg-intro *)
  | Inone, Isiblings -> r (* no invalidate on seg-intro *)
  | _, _ -> Log.todo_exn "invalidate"

(** Extraction of mappings *)
let extract_mappings
    (svl: SvSet.t) (svr: SvSet.t)
    (nr: Graph_sig.node_relation): unit node_mapping * unit node_mapping =
  if !Flags.flag_dbg_join_gen then
    Log.info "Node_relation(extract_mappings):\n%a" Graph_utils.Nrel.nr_fprc nr;
  let init (sv: SvSet.t): unit node_mapping =
    { nm_map    = SvMap.empty ;
      nm_rem    = sv ;
      nm_suboff = fun _ -> true ; } in
  SvMap.fold
    (fun i (il, ir) (accl, accr) ->
      Nd_utils.add_to_mapping il i accl, Nd_utils.add_to_mapping ir i accr
    ) nr.n_pi (init svl, init svr)

(** Addition of rules *)
let rules_add_weakening (p: sv * sv) (r: rules): rules =
  { r with r_indintro = SvPrSet.add p r.r_indintro }
let rules_add_splitind_l (p: sv * sv) (r: rules): rules =
  { r with r_splitind_l = SvPrSet.add p r.r_splitind_l }
let rules_add_splitind_r (p: sv * sv) (r: rules): rules =
  { r with r_splitind_r = SvPrSet.add p r.r_splitind_r }

(** Auxiliary function used both in Graph and in List *)
(* Computation of siblings for the inductive edge split rule *)
let calc_split_ind_dest_o (allsibl: SvSet.t SvMap.t) (iso: sv) (iss: sv): sv =
  (* extract other siblings *)
  let siblings =
    let siblings =
      SvMap.find_err "siblings inconsistency in sep_ind" iss allsibl in
    assert (SvSet.mem iso siblings);
    SvSet.remove iso siblings in
  (* extraction of the destination in the siblings set *)
  let card = SvSet.cardinal siblings in
  if card = 0 then Log.fatal_exn "no sibling"
  else
    let n = SvSet.min_elt siblings in
    if card > 1 then
      Log.info "sep-ind found too many siblings: %a ; picking %a"
        nsvset_fpr siblings nsv_fpr n;
    n
