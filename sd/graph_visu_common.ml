(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_visu_common.ml
 **       functions common to graphs and encoded graphs output
 ** Francois Berenger and Huisong Li, started 2015/02/24 *)
open Data_structures
open Sv_def

open Graph_sig
open Graph_utils
open Graph_visu_atom

let get_name (namer: namer) (id: sv): string =
  try fst (namer id)
  with Not_found -> "_"

let successors_only
    (root_vars: StringSet.t)
    (g: graph)
    (namer: namer): SvSet.t =
  (* keep only nodes from 'g' which are successors (direct or not)
   * of nodes in 'vars' *)
  let descendants (g: graph) (vars: SvSet.t): SvSet.t =
    let rec loop to_visit visited =
      if SvSet.is_empty to_visit then
        visited
      else
        let curr, remaining = SvSet.pop_min to_visit in
        let nexts =
          try Node.successors (get_node curr g)
          with Not_found -> SvSet.empty in
        let to_visit' = SvSet.diff (SvSet.union remaining nexts) visited in
        let visited' = SvSet.add curr visited in
        loop to_visit' visited' in
    loop vars SvSet.empty in
  let root_nodes =
    SvMap.fold
      (fun k _v acc ->
         try
           let name = get_name namer k in
           if StringSet.mem name root_vars then SvSet.add k acc
           else acc
         with Not_found -> acc (* anonymous node *)
      ) g.g_g SvSet.empty in
  descendants g root_nodes

let filter_nodes (interesting_nodes: SvSet.t) (g: graph): graph =
  let new_map =
    SvMap.fold
      (fun k v acc ->
        if SvSet.mem k interesting_nodes then
          (* update node predecessors (the filtered graph has less nodes) *)
          let v' =
            { v with n_preds = SvSet.inter v.n_preds interesting_nodes } in
          SvMap.add k v' acc
        else acc
      ) g.g_g SvMap.empty in
  { g with g_g = new_map }
