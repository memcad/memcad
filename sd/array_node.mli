(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: array_node.mli
 **       lifting of a numerical domain into an array abstraction
 ** Jiangchao Liu 2014/09/23 *)
open Data_structures
open Sv_def

open Ast_sig
open Dom_sig
open Vd_sig

(** Materialization environment *)
val mat_left: bool ref
val mater_id: (SvSet.t * SvSet.t) ref
val mat_var:  ((VarSet.t * VarSet.t) IntMap.t) ref
val varm: (sv * typ) VarMap.t ref

(** Temp names for dimensions on array cells,
 ** used in transfer functions on summarizing dimensions  *)
val tmp_dim_reset: unit -> unit

(** A functor that describes the properties of an array **)
module Array_Node (Op: DOM_MAYA) : (ARRAY_NODE with type op = Op.t)
