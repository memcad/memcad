(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_mem_low_shape.ml
 **       Low level memory abstraction (memory cells level)
 ** Xavier Rival, 2011/05/27 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def
open Timer

open Ast_sig
open C_sig
open Col_sig
open Dom_sig
open Graph_sig
open Graph_encode
open Ind_sig
open Nd_sig
open Seq_sig
open Set_sig
open Spec_sig
open Svenv_sig
open Vd_sig

open Ast_utils
open Dom_utils
open Graph_utils
open Ind_utils
open Inst_utils
open Inst_sig
open Nd_utils
open Seq_utils
open Set_utils
open Sv_utils
open Vd_utils

open Ind_inst
open Apron

module GU = Graph_utils

(** Improvements to consider:
 ** - share code for the initialization of join and weakening
 **   (this will be better done when the structures are made similar)
 **)

(** Error report *)
module Log =
  Logger.Make(struct let section = "dm_ls___" and level = Log_level.DEBUG end)

(** Localization of cells that may be in a sub-memory space *)
(* Resolves how an address should be localizedd *)
type cell_loc =
  | Cl_main       (* address in the main memory *)
  | Cl_sub_intern (* internal address in a sub-memory *)
  | Cl_sub_base   (* base address of a sub-memory *)
let cell_loc_fpr (fmt: form): cell_loc -> unit = function
  | Cl_main       -> F.fprintf fmt "main"
  | Cl_sub_intern -> F.fprintf fmt "sub[intern]"
  | Cl_sub_base   -> F.fprintf fmt "sub[base]"


(** Utilities *)
(* Pretty-printing *)
let colv_emb_fpri (ind: string) (fmt: form) (ce: colv_emb): unit =
  Aa_maps.t_fpr "" "\n"
    (fun fmt (sv0, (sv1, ck)) ->
      F.fprintf fmt "%s%a => %a"  ind (colv_fpr ck) sv0 (colv_fpr ck) sv1
    ) fmt ce

(** The shape domain *)
module DBuild = functor (Dv: DOM_VALCOL) ->
  (struct
    (** Module name *)
    let module_name = "[shape]"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name Dv.module_name Dv.config_fpr ()
    (** Dom ID *)
    let dom_id: mod_id ref = ref (sv_unsafe_of_int (-1),"shape")

    (** Type of symbolic indexes *)
    type sym_index = sv (* int for the graph implementation *)
    type off_sym_index = sym_index * Offs.t

    (** Type of abstract values *)
    type t =
        { t_shape:   graph;      (* shape component *)
          t_num:     Dv.t;       (* numeric component *)
          t_envmod:  svenv_mod;  (* local symvar env modifications *) }

    (** Domain initialization to a set of inductive definitions *)
    let inductives: StringSet.t ref = ref StringSet.empty
    let init_inductives (g: SvGen.t) (s: StringSet.t): SvGen.t =
      let g = Dv.init_inductives g s in
      inductives := s;
      let g, k = SvGen.gen_key g in (* this domain generates keys *)
      if sv_upg then dom_id := k, snd !dom_id;
      g
    let inductive_is_allowed (s: string): bool = StringSet.mem s !inductives

    (** Fixing sets of keys *)
    let sve_sync_bot_up (x: t): t * svenv_mod =
      { x with t_envmod = svenv_empty }, x.t_envmod
    let sanity_sv (_: SvSet.t) (x: t): bool =
      Log.todo "sanity_sv: unimp";
      true

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t = { t_shape  = Graph_utils.graph_empty !inductives;
                   t_num    = Dv.bot;
                   t_envmod = svenv_empty; }
    let is_bot (x: t): bool = Dv.is_bot x.t_num
    (* Top element, with empty set of roots *)
    let top (): t =
      { t_shape  = Graph_utils.graph_empty !inductives;
        t_num    = Dv.top;
        t_envmod = svenv_empty; }
    (* Pretty-printing *)
    let t_fpri (ind: string) (fmt: form) (x: t): unit =
      F.fprintf fmt "%a%a" (Graph_utils.graph_fpri ind) x.t_shape
        (Dv.t_fpri SvMap.empty ind) x.t_num;
      if !Flags.flag_dbg_symvars then
        F.fprintf fmt "%sSVE-Mod-shape:\n%a" ind
          (svenv_fpri (ind^"  ")) x.t_envmod
    (* External output *)
    let ext_output (o: output_format) (base: string) (namer: namer) (x: t)
        : unit =
      match o with
      | Out_dot (vars, opts) ->
          (* don't slow down memcad during benchmarks *)
          if !Flags.very_silent_mode then
            Log.warn "no pdf output in very_silent_mode"
          else
            let dot_fn = Printf.sprintf "%s.dot" base in
            let enc02_dot_fn = Printf.sprintf "%s.enc02.dot" base in
            let enc02_pdf_fn = Printf.sprintf "%s.enc02.pdf" base in
            let enc_dot_fn = Printf.sprintf "%s.enc.dot" base in
            let enc_pdf_fn = Printf.sprintf "%s.enc.pdf" base in
            let pdf_fn = Printf.sprintf "%s.pdf" base in
            let opts = List.map visu_opt_of_string opts in
            if List.mem Connex_component opts && List.mem Successors opts then
              failwith "ext_output: use either CC either SUCC";
            let labels =
              Format.asprintf "%a\n%a"
                (fun fmt->
                  SvMap.iter
                    (fun colv ck ->
                      F.fprintf fmt "%a; "
                        (match ck with
                        | Ct_seq _ -> seqv_fpr
                        | Ct_set _ -> setv_fpr)
                        colv)
                ) x.t_shape.g_colvkind
                (Dv.t_fpri SvMap.empty "") x.t_num
                |> String.split_on_char '\n'
                |> String.concat "\\l" in
            with_out_file dot_fn
              (Graph_visu.pp_graph pdf_fn ~labels vars opts x.t_shape namer);
            if not !Flags.flag_enable_ext_export_all then
              begin
                let enc_txt_fn = Printf.sprintf "%s.enc.txt" base in
                let enc02_txt_fn = Printf.sprintf "%s.enc02.txt" base in
                let disj_num = sv_unsafe_of_int (-1) in (* we don't care about it *)
                with_out_file enc_txt_fn
                  (Graph_encode.pp_encoded_graph
                    disj_num vars x.t_shape namer enc_dot_fn);
                with_out_file enc02_txt_fn
                  (Graph_encode.pp_precisely_encoded_graph
                     disj_num vars x.t_shape namer enc02_dot_fn);
              end;
            let shape_graph_to_pdf =
              Printf.sprintf "dot -Tpdf %s -o %s" dot_fn pdf_fn in
            ignore (run_command shape_graph_to_pdf);
            let enc_shape_graph_to_pdf =
              Printf.sprintf "dot -Tpdf %s -o %s" enc_dot_fn enc_pdf_fn in
            ignore (run_command enc_shape_graph_to_pdf);
            let simple_shape_graph_to_pdf =
              Printf.sprintf "dot -Tpdf %s -o %s" enc02_dot_fn enc02_pdf_fn in
            ignore (run_command simple_shape_graph_to_pdf)

    (** Graph encoding *)
    let encode (disj: sv) (l: namer) (x: t)
        : renamed_path list * IntSvPrSetSet.t * sv =
      Graph_encode.encode disj l x.t_shape

    (** Management of symbolic variables *)

    (* Ensuring consistency of graph nodes wrt numeric value environment *)
    let sve_fix (x: t): t =
      if !Flags.flag_sanity_graph then
        Graph_utils.graph_sanity_check "sve_fix,before" x.t_shape;
      let g0, svm = Graph_utils.sve_sync_bot_up x.t_shape in
      if !Flags.flag_dbg_symvars then
        Log.info "sve_fix:\n%a" (Svenv_utils.svenv_mod_fpri "  ") svm;
      { t_shape  = g0;
        t_num    = Dv.sve_sync_top_down svm x.t_num;
        t_envmod = svenv_join x.t_envmod svm; }
    (* Addition of a fresh node with known id *)
    let sv_add (i: sym_index) (nt: ntyp) (na: nalloc) (x: t): t =
      if !Flags.flag_sanity_graph then
        Graph_utils.graph_sanity_check "sv_add,before" x.t_shape;
      sve_fix { x with t_shape = GU.sv_add i nt na x.t_shape }
    (* Will that symb. var. creation be allowed by the domain? *)
    let sv_is_allowed ?(attr: node_attribute = Attr_none) (nt: ntyp)
        (na: nalloc) (x: t): bool = true
    (* Add a node, with a newly generated id *)
    let sv_add_fresh ?(attr: node_attribute = Attr_none) ?(root: bool = false)
        (nt: ntyp) (na: nalloc) (x: t): sym_index * t =
      if !Flags.flag_sanity_graph then
        Graph_utils.graph_sanity_check "sv_add_fresh,before" x.t_shape;
      let i, x0 =
        Graph_utils.sv_add_fresh ~attr:attr ~root:root nt na x.t_shape in
      i, sve_fix { x with
                   t_shape = x0 }
    let sv_get_all (x: t): SvSet.t =
      Graph_utils.get_all_nodes x.t_shape
    (* Recover information about a symbolic variable *)
    let sv_get_info (i: sv) (x: t): nalloc option * ntyp option =
      let ni = Graph_utils.node_find i x.t_shape in
      Some ni.n_alloc, Some ni.n_t

    (** Set/Seq domain *)
    let colv_add ?(root: bool = false) (cv: sv) (ck: col_kind) (t: t): t =
      let t_shape = Graph_utils.colv_add ~root  cv ck t.t_shape in
      let info = SvMap.find_opt cv t_shape.g_colvinfo in
      let t_num = Dv.colv_add ck ~root ~name:None cv info t.t_num in
      sve_fix { t with t_shape; t_num }
    let colv_add_fresh (root: bool) (s: string) (k: col_kind) (t: t)
        : sv * t * colv_info option =
      let par_type =
        match k with
        | CK_set -> Ct_set None
        | CK_seq -> Ct_seq None in
      let i, t_shape = colv_add_fresh ~root par_type t.t_shape in
      let info = find_info i t_shape in
      let t_num = Dv.colv_add k ~root:root ~name:(Some s) i info t.t_num in
      i, sve_fix { t with t_num; t_shape }, info
    let colv_delete (colv: sv) (t: t) =
      let t_num = Dv.colv_rem colv t.t_num in
      let t_shape = colv_rem colv t.t_shape in
      sve_fix { t with t_num; t_shape}
    let colv_delete_non_root (i: sv) (x: t): t =
      if Dv.colv_is_root i x.t_num then x
      else colv_delete i x

    (** Sanity checks *)
    (* A checking function that numeric domain nodes be consistent *)
    let check_nodes (ctxt: string) (x: t): unit =
      if !Flags.flag_sanity_env then
        (* collect nodes in the graph *)
        let svs = sv_get_all x in
        if Flags.flag_sanity_env_pp then
          Log.info "sanity_check, memory nodes: { %a }" (SvSet.t_fpr "; ") svs;
        (* check that the numerical domain speaks about exactly those keys *)
        let is_valid = Dv.symvars_check svs x.t_num in
        if Flags.flag_sanity_env_pp then
          Log.info "Dom-shape check_nodes(%s): %b" ctxt is_valid;
        if not is_valid then
          begin
            Log.info "check_nodes(%s): inconsistent SVs (symvars)" ctxt;
            Log.fatal_exn "check_nodes failed"
          end;
        (* check SVs and COLVs in the value domain layer *)
        let b =
          let colvs = Graph_utils.colv_get_all x.t_shape in
          Dv.check_nodes ~svs ~colvs x.t_num in
        if not b then
          Log.info "check_nodes(%s): inconsistent SVs/COLVs" ctxt;
        (* optionally, check that consistence was enforced *)
        let _, svm = Graph_utils.sve_sync_bot_up x.t_shape in
        if not (Dom_utils.svenv_is_empty svm) then
          begin
            Log.info "check_nodes(%s): inconsistent abstract" ctxt;
            Log.info "%a" (svenv_fpri "  ") svm;
            Log.fatal_exn "check_nodes failed"
          end
    (* Gathering all sanity checks in a same function *)
    let sanity_check (ctxt: string)
        (do_check_nodes: bool) (* whether to check consistency of node names *)
        (do_graph_sanity: bool) (* whether to perform graph sanity check *)
        (x: t): unit =
      if !flag_sanity_bshape then
        begin
          Log.info "DO SANITY: %s,%b\n" ctxt do_check_nodes;
          if do_check_nodes then
            check_nodes ctxt x;
          if do_graph_sanity && !Flags.flag_sanity_graph then
            Graph_utils.graph_sanity_check ctxt x.t_shape
        end

    (** Utility functions for the manipulation of abstract values *)
    (* Satifying function in the value domain *)
    let sat_val (t: t) (c: n_cons): bool =
      if !Flags.flag_debug_block_sat then
        Log.info "sat_val: %a\n%a" n_cons_fpr c
          (Dv.t_fpri SvMap.empty "      ") t.t_num;
      Dv.sat t.t_num c

    (* Garbage collection *)
    let gc (roots: sym_index uni_table) (x: t): t =
      check_nodes "gc-in" x;
      let t_shape, colv_to_rem = Graph_utils.gc roots x.t_shape in
      if !Flags.flag_sanity_graph then
        Graph_utils.graph_sanity_check "gc,ongoing" t_shape;
      let t_num = SvSet.fold Dv.colv_rem colv_to_rem x.t_num in
      let o = sve_fix { x with t_shape; t_num } in
      check_nodes "gc-out" o;
      o
    (* Reduction of nodes that denote the same concrete values
     *   returns both the value after reduction, and the renaming *)
    let red_equal_cells (eqreds: SvPrSet.t) (t: t): t * sv SvMap.t =
      let loc_debug = false in
      let sat = sat_val t in
      (* 1. reduce equalities in the graph *)
      let (n_shape, renaming), isbot =
        try Graph_utils.graph_merge_eq_nodes sat eqreds t.t_shape, false
        with
        | Bottom -> (t.t_shape, SvMap.empty), true in
      (* 2. get the set of equalities to apply in the numerical domain *)
      let n_num =
        if isbot then Dv.bot
        else
          SvMap.fold
            (fun irem ikeep acc ->
              (* reduction: if ikeep was null in the numeric, and if
               *  irem has a points-to edge, reduce to bottom! *)
              let num =
                if Dv.sat acc (Nc_cons (Tcons1.EQ, Ne_var irem, Ne_csti 0))
                    && Graph_utils.node_is_pt ikeep t.t_shape then
                  Dv.bot
                else acc in
              (* apply the equality constraint in the numerical domain *)
              let cons = Nc_cons (Tcons1.EQ, Ne_var ikeep, Ne_var irem) in
              Dv.guard true cons num
            ) renaming t.t_num in
      if loc_debug then
        Log.debug "%a\n%a\n%a" (t_fpri "  PRE   ") t (SvPrSet.t_fpr "; ") eqreds
          (t_fpri "  POST  ") {t with t_shape = n_shape; t_num = n_num };
      sve_fix { t with
                t_shape = n_shape;
                t_num   = n_num }, renaming
    (* Reduction of a state with a segment known to be empty *)
    let red_empty_segment (id: sv) (t: t): t * sv SvMap.t =
      (* removal of the segment, and the equalities due to it being empty *)
      let n_shape, eqreds, setcons, seqv_empty =
        Graph_utils.red_empty_segment id t.t_shape in
      (* reduction of all equalities obtained above *)
      let t_num =
        SvSet.fold
          (fun seqv acc ->
            let cons = Seq_equal (Seq_empty, Seq_var seqv) in
            Dv.seq_guard cons acc
          ) seqv_empty t.t_num in
      let t_num =
        List.fold_left (fun acc cons -> Dv.set_guard cons acc) t_num setcons in
      red_equal_cells eqreds { t with t_shape = n_shape; t_num }

    (** Comparison and Join operators *)
    (* Function to check constraint satisfaction on a t *)
    let make_sat (x: t) (nn: n_cons): bool =
      if !Flags.flag_sanity_graph then
        Graph_utils.graph_sanity_check "make_sat,before" x.t_shape;
      if false then
        Log.info "Sat, abstract value:\n%a"
          (Dv.t_fpri SvMap.empty "  ") x.t_num;
      match nn with
      | Nc_cons (Tcons1.DISEQ, Ne_csti 0, Ne_var i)
      | Nc_cons (Tcons1.DISEQ, Ne_var i, Ne_csti 0) ->
          if Graph_utils.pt_edge_mem (i, Offs.zero) x.t_shape then true
          else Dv.sat x.t_num nn
      | _ ->
          Dv.sat x.t_num nn
    let make_setsat (x: t) (sc: set_cons): bool =
      Dv.set_sat sc x.t_num
    let make_seqsat (x: t) (sc: seq_cons): bool =
      Dv.seq_sat sc x.t_num
    let make_vsat (x: t): vsat =
      { vs_num = (make_sat x) ;
        vs_set = (make_setsat x) ;
        vs_seq = (make_seqsat x) }

    let inst_seq ~(doskip:bool) (inst: seq_colv_inst) (x: t): colv_mapping * t =
      let map, x =
        SvMap.fold
          (fun seqv def (accm, acct) ->
            match def with
            | Seq_var seqv' -> SvMap.add seqv seqv' accm, acct
            | _ ->
                let nseqv, t_shape =
                  Graph_utils.colv_add_fresh (Ct_seq None) acct.t_shape in
                let info = find_info nseqv t_shape in
                let t_num =
                  if doskip then acct.t_num
                  else
                    acct.t_num
                      |> Dv.colv_add CK_seq ~root:false nseqv info
                      |> Dv.seq_guard (Seq_equal (Seq_var nseqv, def)) in
                SvMap.add seqv nseqv accm, { acct with t_shape; t_num}
          ) inst.colvi_eqs (SvMap.empty, x) in
      let mapping =
        SvMap.fold (fun k e -> Col_utils.add_to_mapping e k)
          map Col_utils.seq_colv_mapping_empty in
      if doskip then
        mapping, x
      else
        (* When there is a mapping seqv_in |-> seqv_out { seqv_out' }
         * - We allocate a fresh variable seqv_in' in the input,
         * - We add the constraint [seqv_in = seqv_in'],
         * - We modifty the mapping to obtain:
         *      seqv_in  |-> seqv_out
         *      seqv_in' |-> seqv_out'
         * TODO: remove the set part of the mapping.
         * (could still be useful as a intermediary type)
         *)
        if doskip then
          mapping, x
        else
          let sm_map, x =
            SvMap.fold
              (fun seqv (seqv_out, set) (acc, x) ->
                let acc = SvMap.add seqv (seqv_out, SvSet.empty) acc in
                Log.debug "%a => %a { %a }"
                  sv_fpr seqv sv_fpr seqv_out (SvSet.t_fpr "; ") set;
                SvSet.fold
                  (fun seqv_out' (acc, x) ->
                    let nseqv, t_shape =
                      Graph_utils.colv_add_fresh (Ct_seq None) x.t_shape in
                    let acc = SvMap.add nseqv (seqv_out', SvSet.empty) acc in
                    let info = find_info nseqv t_shape in
                    Log.debug "Adding binding %a => %a"
                      sv_fpr nseqv sv_fpr seqv_out';
                    let t_num =
                      x.t_num
                      |> Dv.colv_add CK_seq ~root:false nseqv info
                      |> Dv.seq_guard
                          (Seq_equal (Seq_var nseqv, Seq_var seqv)) in
                    acc, {x with t_num; t_shape})
                  set (acc, x)
              ) mapping.sm_map (SvMap.empty, x) in
          { mapping with sm_map }, x

    (* This function does some preparation for set abstraction renaming.
     * As an example, \sete --> {\alpha}+{\beta} in inst, in order to rename
     *  1. we generate a fresh set variable \sete',
     *  2. then, let \sete --> \sete' and add \sete' = {\alpha} + {\beta}
     *     into the input set abstraction *)
    let inst_set ~(doskip:bool) (inst: (set_expr, set_cons) colv_inst) (x: t)
        : colv_mapping * t =
      let map, x =
        SvMap.fold
          (fun i ele (accm, acct) ->
            match ele with
            | S_var sid -> SvMap.add i sid accm, acct
            | _ ->
                let key, mem =
                  Graph_utils.colv_add_fresh (Ct_set None) acct.t_shape in
                let info = find_info key mem in
                let num =
                  if doskip then acct.t_num
                  else
                    acct.t_num
                     |> Dv.colv_add CK_set ~root:false key info
                     |> Dv.set_guard (S_eq (S_var key, ele)) in
                let acct = { acct with
                             t_num   = num;
                             t_shape = mem } in
                SvMap.add i key accm, acct
          ) inst.colvi_eqs (SvMap.empty, x) in
      let mapping =
        SvMap.fold (fun k e -> Col_utils.add_to_mapping e k)
          map Col_utils.set_colv_mapping_empty in
      if doskip then
        mapping, x
      else
        (* TODO: seems copy-pase from above... *)
        let sm_map, x =
          SvMap.fold
            (fun setv_in (setv_out, set) (acc, x) ->
              let acc = SvMap.add setv_in (setv_out, SvSet.empty) acc in
              Log.debug "%a => %a { %a }"
                sv_fpr setv_in sv_fpr setv_out (SvSet.t_fpr "; ") set;
              SvSet.fold
                (fun setv_out' (acc, x) ->
                  let nsetv, t_shape =
                    Graph_utils.colv_add_fresh (Ct_seq None) x.t_shape in
                  let acc = SvMap.add nsetv (setv_out', SvSet.empty) acc in
                  let info = find_info nsetv t_shape in
                  Log.debug "Adding binding %a => %a"
                      sv_fpr nsetv sv_fpr setv_out';
                  let t_num =
                    x.t_num
                    |> Dv.colv_add CK_set ~root:false nsetv info
                    |> Dv.set_guard (S_eq (S_var setv_in, S_var nsetv )) in
                  acc, {x with t_num; t_shape})
                set (acc, x)
            ) mapping.sm_map (SvMap.empty, x) in
        { mapping with sm_map } , x

    (* deal with the sv instantiation from join *)
    let inst_sv (inst: sv_inst) (x: t) (map: 'a node_mapping)
        : t * 'a node_mapping =
      (* Fresh SVs that are also colv information variables are already
       * added when we add the corresponding colv: we ignore them. *)
      let sv_fresh =
        SvSet.filter (fun sv -> not @@ is_info sv x.t_shape) inst.sv_fresh in
      let t_num = SvSet.fold Dv.sv_add sv_fresh x.t_num in
      let map =
        let rem =
          SvSet.union map.nm_rem
            (SvSet.filter (fun sv -> not (SvMap.mem sv map.nm_map))
               inst.sv_fresh) in
        { map with nm_rem = rem } in
      let t_num =
        SvMap.fold
          (fun i ex acc ->
            Dv.guard true (Nc_cons (Lincons1.EQ, Ne_var i, ex)) acc
          ) inst.sv_eqs t_num in
      let t_num =
        SvMap.fold
          (fun i exs acc ->
            List.fold_left
              (fun acc ex ->
                Log.debug "cons: %a > %a" nsv_fpr i n_expr_fpr ex;
                Dv.guard true (Nc_cons (Lincons1.SUP, Ne_var i, ex)) acc
              ) acc exs
          ) inst.sv_low t_num in
      let t_num =
        SvMap.fold
          (fun i exs acc ->
            List.fold_left
              (fun acc ex ->
                Dv.guard true (Nc_cons (Lincons1.SUP, ex, Ne_var i)) acc
              ) acc exs
          ) inst.sv_up t_num in
      let t_num =
        SvMap.fold
          (fun i exs acc ->
             List.fold_left
              (fun acc ex ->
                Dv.guard true (Nc_cons (Lincons1.SUPEQ, Ne_var i, ex)) acc
              ) acc exs
          ) inst.sv_eqlow t_num in
      let t_num =
        SvMap.fold
          (fun i exs acc ->
             List.fold_left
              (fun acc ex ->
                Dv.guard true (Nc_cons (Lincons1.SUPEQ, ex, Ne_var i)) acc
              ) acc exs
          ) inst.sv_equp t_num in
      { x with t_num = t_num }, map

    let add_colv_info (x_in: graph) (x_out: graph)
        (c_map: colv_map_complete) (map: 'a node_mapping): 'a node_mapping =
      let aux cmap map =
        SvMap.fold (fun colv_in (colv_out, set) acc ->
          if SvSet.mem colv_in x_in.g_colvroots then acc
          else
            let _ = assert (SvSet.is_empty set) in
            let {max = max_in; min = min_in; size = size_in} =
              SvMap.find colv_in x_in.g_colvinfo in
            let map_seqv_out colv_out acc =
              let {max = max_out; min = min_out; size = size_out} =
                SvMap.find colv_out x_out.g_colvinfo in
              acc
              |> add_to_mapping max_in max_out
              |> add_to_mapping min_in min_out
              |> add_to_mapping size_in size_out in
            SvSet.fold map_seqv_out (SvSet.add colv_out set) acc)
          cmap.sm_map map in
      if !Flags.flag_colv_info then
        map |> aux c_map.cmc_seq |> aux c_map.cmc_set
      else
        map

    (** [map_info g_in g_out inst map] adds to [map] all colv_info from colv in
        [inst.colv_inst.colvi_add]. *)
    let map_info (g_in: graph) (g_out: graph) (col_torem: col_kinds)
        ({set_inst; seq_inst; _}: join_inst) (map: 'a node_mapping)
        : 'a node_mapping =
      if !Flags.flag_dbg_join_shape then
        Log.debug "%a\n%a"
          (graph_fpri " in   ") g_in
          (graph_fpri " out  ") g_out;
      let map_from_colv colv acc =
        Log.debug "Treating %a" setv_fpr colv;
        let {max = max_in; min = min_in; size = size_in} =
          SvMap.find colv g_in.g_colvinfo in
        let {max = max_out; min = min_out; size = size_out} =
          SvMap.find colv g_out.g_colvinfo in
        acc
        |> add_to_mapping max_in max_out
        |> add_to_mapping min_in min_out
        |> add_to_mapping size_in size_out in
      if !Flags.flag_colv_info then
        map
        |> SvSet.fold map_from_colv
            (SvSet.diff set_inst.colvi_add set_inst.colvi_rem)
        |> SvSet.fold map_from_colv
            (SvSet.diff seq_inst.colvi_add seq_inst.colvi_rem)
      else
        map

    (* Function for converting hints (temporary glue) *)
    let convert_bin_hint (h: sym_index hint_bs): hint_bg =
      { hbg_live = h.hbs_live }
    let convert_uni_hint (h: sym_index hint_us): hint_ug =
      { hug_live = h.hus_live }
    let convert_bin_lint (h: Offs.svo lint_bs): lint_bg =
      { lbg_dead = h.lbs_dead }
    (* Checks if the left argument is included in the right one *)
    let is_le (ninj: sym_index bin_table) (sinj: colv_emb)
        (xl: t) (xr: t): svenv_upd option =
      let loc_debug = false in
      sanity_check "is_le,l,before" true true xl;
      sanity_check "is_le,r,before" true true xr;
      if loc_debug then
        Log.info "Is_le:\n- L:\n%a- R:\n%a- SV:\n%a\n- COLV:\n%a"
          (t_fpri "  ") xl (t_fpri "  ") xr
          (bin_table_fpri sv_fpr "  ") ninj
          (colv_emb_fpri "  ") sinj;
      (* launching the graph algorithm and processing its output *)
      let oinj =
        Graph_is_le.is_le ~submem:false xl.t_shape None ~vsat_l:(make_vsat xl)
          xr.t_shape ninj sinj in
      match oinj with
      | None ->
          if loc_debug || !flag_dbg_is_le_gen then (* temptemp *)
            Log.info "ISLE Comparison: shape did not conclude le!";
          None
      | Some (inj, set_inst, seq_inst, sv_inst) ->
          if loc_debug || !flag_dbg_is_le_gen then (* temptemp *)
            begin
              Log.info "ISLE Comparison: is_le holds in the shape!";
              Log.info "ISLE,Numerics:\n%a Mapping:"
                (Dv.t_fpri SvMap.empty "  ") xr.t_num;
              SvMap.iter
                (fun sv0 sv1 ->
                  Log.info "  %a => %a" nsv_fpr sv0 nsv_fpr sv1) inj;
              Log.info " SV-inst:\n%a" (sv_inst_fpri "  ") sv_inst;
            end;
          let inj_rel: (sv * Offs.t) node_mapping =
            let nodes = sv_get_all xl in
            SvMap.fold (fun i j -> add_to_mapping j i) inj
              { nm_map    = SvMap.empty ;
                nm_rem    = nodes;
                nm_suboff = fun _ -> true } in
          let xl, inj_rel = inst_sv sv_inst xl inj_rel in
          let set_map, xl =
            inst_set ~doskip:false
              { set_colv_inst_empty with colvi_eqs = set_inst } xl in
          let seq_map, xl =
            inst_seq ~doskip:false
              { seq_colv_inst_empty with colvi_eqs = seq_inst } xl in
          let col_map = { cmc_set = set_map; cmc_seq = seq_map } in
          let inj_rel = add_colv_info xl.t_shape xr.t_shape col_map inj_rel in
          if !flag_dbg_is_le_gen then
            Log.info "Effective mapping:\n%a" node_mapping_fpr inj_rel;
          let n_l =
            xl.t_num
            |> Dv.symvars_srename OffMap.empty inj_rel (Some col_map) in
          let sat_diseq = Graph_utils.sat_graph_diseq xr.t_shape in
          let r =
            if Dv.is_le n_l xr.t_num sat_diseq then
              let svu =
                svenv_upd_embedding
                  { inj_rel with nm_suboff = fun _ -> true } in
              Some svu
            else None in
          if loc_debug || !flag_dbg_is_le_gen then (* temptemp *)
            Log.info "ISLE Numeric comparison: %s\nleft:\n%aright:\n%a"
              (if r = None then "false" else "true")
              (Dv.t_fpri SvMap.empty "   ") n_l
              (Dv.t_fpri SvMap.empty "   ") xr.t_num;
          r


    (* Generic join *)
    let join (j: join_kind) (hso: sym_index hint_bs option)
        (lso: (Offs.svo lint_bs) option)
        (roots_rel: sym_index tri_table)
        (colroots_rel: (sv, col_kind) tri_map)
        ((xl, jl): t * join_ele) ((xr, jr): t * join_ele)
        : t * svenv_upd * svenv_upd =
      let loc_debug = false in
      sanity_check "join-l,before" true true xl;
      sanity_check "join-r,before" true true xr;
      (* Printing functions used thoughout *)
      let pp_status_inst msg set_instl seq_instl set_instr seq_instr =
        Log.info "Instantiations, %s:\n- left:\n%a%a- right:\n%a%a" msg
          (set_colv_inst_fpri "   ") set_instl (seq_colv_inst_fpri "   ")
          seq_instl (set_colv_inst_fpri "   ") set_instr
          (seq_colv_inst_fpri "   ") seq_instr in
      let pp_status_num msg num_l num_r =
        Log.info "Num status,%s:\n- left:\n%a- right:\n%a" msg
          (Dv.t_fpri SvMap.empty "      ") num_l
          (Dv.t_fpri SvMap.empty "      ") num_r in
      if false then pp_status_num "j-init" xl.t_num xr.t_num;
      (* Hint conversion *)
      let hgo = Option.map convert_bin_hint hso in
      let lgo = Option.map convert_bin_lint lso in
      (* Computation of the node_relation *)
      let nrel =
        Aa_sets.fold
          (fun (il, ir, io) acc -> Nrel.add acc (il, ir) io)
          roots_rel Nrel.empty in
      (* Computation of setvar_relation *)
      let srel, qrel =
        Aa_maps.fold
          (fun (il, ir, io) kind (sacc, qacc) ->
            match kind with
            | CK_set ->
                let sacc = Graph_utils.Nrel.add sacc (il, ir) io in
                sacc, qacc
            | CK_seq ->
                let qacc = Graph_utils.Nrel.add qacc (il, ir) io in
                sacc, qacc)
          colroots_rel Graph_utils.Nrel.(empty, empty) in
      if !flag_dbg_join_shape then
        Log.info "Node-Relation found:\n%a" (Nrel.nr_fpri "  ") nrel;
      if !flag_dbg_join_shape then
        Log.info "Setvar-Relation found:\n%a" (Nrel.nr_fpri "  ") srel;
      (* Creation of the initial graph *)
      let g_init, (col_toaddl, col_toaddr), col_torem =
        init_graph_from_roots ~tmpexp:!do_exp_jinst ~submem:false
          nrel.n_pi srel.n_pi qrel.n_pi xl.t_shape xr.t_shape in
      if !flag_dbg_join_shape then
        Log.info "col_toaddl: %a\ncol_toaddr: %a"
          col_kinds_fpr col_toaddl col_kinds_fpr col_toaddr;
      let xl, xr =
        if !do_exp_jinst then
          let xl = SvMap.fold colv_add col_toaddl xl
          and xr = SvMap.fold colv_add col_toaddr xr in
          let xl = sve_fix xl in
          let xr = sve_fix xr in
          xl, xr
        else xl, xr in
      let srel, qrel =
        if not !do_exp_jinst then srel, qrel
        else
          SvMap.fold
            (fun i kind (sacc, qacc) ->
              match kind with
              | CK_set ->
                  let sacc = Graph_utils.Nrel.add sacc (i, i) i in
                  sacc, qacc
              | CK_seq ->
                  let qacc = Graph_utils.Nrel.add qacc (i, i) i in
                  sacc, qacc)
            col_torem (srel, qrel) in
      if loc_debug then pp_status_num "join-in" xl.t_num xr.t_num;
      (* Sat functions *)
      let vsat_l = make_vsat xl and vsat_r = make_vsat xr in
      let nrel =
        nrel
        |> add_colvinfo_nrel xl.t_shape xr.t_shape g_init srel
        |> add_colvinfo_nrel xl.t_shape xr.t_shape g_init qrel in
      (* Computation of graph join, and of new graph relation *)
      let g_out, onrel, (j_inst_l, remsetin_l), (j_inst_r, remsetin_r) =
        Graph_join.join ~submem:false
          (tr_join_arg vsat_l.vs_num (xl.t_shape, jl)) ~vsat_l
          (tr_join_arg vsat_r.vs_num (xr.t_shape, jr)) ~vsat_r
          hgo lgo nrel srel qrel false g_init in
      (* Preparation of the mapping for used to nodes rename *)
      let svmap_l, svmap_r =
        Gen_join.extract_mappings (sv_get_all xl) (sv_get_all xr) onrel in
      (* Complete instantiation of COLVs following guesses (if any) *)
      if !flag_dbg_join_num || loc_debug then
        begin
          pp_status_inst "join-out" j_inst_l.set_inst j_inst_l.seq_inst
            j_inst_r.set_inst j_inst_r.seq_inst;
          pp_status_num "join-out" xl.t_num xr.t_num;
        end;
      (* experimental begin *)
      let xl, xr =
        let set_roots = setv_roots xl.t_shape in
        let set_instl =
          jinst_set_post_join_refine set_roots vsat_l.vs_set
            j_inst_l.set_inst in
        let set_instr =
          jinst_set_post_join_refine set_roots vsat_r.vs_set
            j_inst_r.set_inst in
        let seq_roots = seqv_roots xl.t_shape in
        let seq_instl =
          jinst_seq_post_join_refine seq_roots vsat_l.vs_seq
            j_inst_l.seq_inst in
        let seq_instr =
          jinst_seq_post_join_refine seq_roots vsat_r.vs_seq
            j_inst_r.seq_inst in
        (* When either instantiation has non bound variables,
         * inherit from the other *)
        let set_instl = colv_inst_strengthen set_instr set_instl in
        let set_instr = colv_inst_strengthen set_instl set_instr in
        let seq_instl = colv_inst_strengthen seq_instr seq_instl in
        let seq_instr = colv_inst_strengthen seq_instl seq_instr in
        (* Instantiation of the set.seq variables *)
        if !flag_dbg_join_num || loc_debug then
          pp_status_inst "refined" set_instl seq_instl set_instr seq_instr;
        if !flag_dbg_join_num || loc_debug then
          pp_status_num "Before set inst" xl.t_num xr.t_num;
        (* Since we need new added colv info to be allocated,
         * colv instantiation must be ate the level of the whole abstract vale
         * and not the numerical part. *)
        if !do_exp_jinst then
          let f_add_setcons (inst: (set_expr, set_cons) colv_inst) (x: t): t =
            Inst_utils.gen_colv_inst_fold_ctr
              ~expr_fpr:set_expr_fpr ~cons_fpr:set_cons_fpr
              ~colv_add:(fun colv -> colv_add colv CK_set ~root:false)
              ~colv_rem:colv_delete
              ~guard:(fun cons x -> { x with t_num = Dv.set_guard cons x.t_num })
              ~eqcons:(fun i ex -> (S_eq (S_var i, ex)))
              ~sat:(fun cons x -> Dv.set_sat cons x.t_num)
              inst x in
          let xl = f_add_setcons set_instl xl in
          let xr = f_add_setcons set_instr xr in
          if !flag_dbg_join_num || loc_debug then
            pp_status_num "After set inst" xl.t_num xr.t_num;
          let f_add_seqcons (inst: seq_colv_inst) (x: t): t =
            Inst_utils.gen_colv_inst_fold_ctr
              ~expr_fpr:seq_expr_fpr ~cons_fpr:seq_cons_fpr
              ~colv_add:(fun colv -> colv_add colv CK_seq ~root:false)
              ~colv_rem:colv_delete
              ~guard:(fun cons x -> { x with t_num = Dv.seq_guard cons x.t_num })
              ~eqcons:(fun i ex->(Seq_equal(Seq_var i,ex)))
              ~sat:(fun cons x -> Dv.seq_sat cons x.t_num)
              inst x in
          let xl = f_add_seqcons seq_instl xl in
          let xr = f_add_seqcons seq_instr xr in
          if !flag_dbg_join_num || loc_debug then
            begin
              pp_status_num "After seq inst" xl.t_num xr.t_num;
              Log.info "COLVs to remove: %a\n" col_kinds_fpr col_torem;
            end;
          let xl = SvMap.fold (fun c _ -> colv_delete c) col_torem xl in
          let xr = SvMap.fold (fun c _ -> colv_delete c) col_torem xr in
          if !flag_dbg_join_num || loc_debug then
            pp_status_num "Removal done" xl.t_num xr.t_num;
          xl, xr
        else xl, xr in
      (* let xl = { xl with t_num = num_l }
       * and xr = { xr with t_num = num_r } in *)
      (* experimental end *)
      (* Set instantiaion part *)
      if loc_debug then Log.warn "join-pre-inst-set";
      let smap_l, xl = inst_set ~doskip:!do_exp_jinst j_inst_l.set_inst xl in
      let smap_r, xr = inst_set ~doskip:!do_exp_jinst j_inst_r.set_inst xr in
      if remsetin_l != [] then Log.warn "remsetin_l!=[]";
      if remsetin_r != [] then Log.warn "remsetin_r!=[]";
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "After set inst(2)" xl.t_num xr.t_num;
      (* Seq instantiaion part *)
      if loc_debug then Log.warn "join-pre-inst-seq";
      let qmap_l, xl = inst_seq ~doskip:!do_exp_jinst j_inst_l.seq_inst xl in
      let qmap_r, xr = inst_seq ~doskip:!do_exp_jinst j_inst_r.seq_inst xr in
      let colvmap_l = { cmc_set = smap_l; cmc_seq = qmap_l } in
      let colvmap_r = { cmc_set = smap_r; cmc_seq = qmap_r } in
      (* Computation of numerical domain join *)
      if loc_debug then Log.warn "join-pre-inst-sv";
      let xl, svmap_l = inst_sv j_inst_l.sv_inst xl svmap_l in
      let xr, svmap_r = inst_sv j_inst_r.sv_inst xr svmap_r in
      let svmap_l, svmap_r =
        if !do_exp_jinst then
          let svmap_l = map_info xl.t_shape g_out col_torem j_inst_l svmap_l in
          let svmap_r = map_info xr.t_shape g_out col_torem j_inst_r svmap_r in
          svmap_l, svmap_r
        else
          let svmap_l = add_colv_info xl.t_shape g_out colvmap_l svmap_l in
          let svmap_r = add_colv_info xr.t_shape g_out colvmap_r svmap_r in
          svmap_l, svmap_r in
      if !flag_dbg_join_num then
        begin
          Log.info "Renaming (left):\n%a%a%a"
            (Dv.t_fpri SvMap.empty "  ") xl.t_num node_mapping_fpr svmap_l
            Col_utils.colv_map_complete_fpr colvmap_l;
          Log.info "Renaming (right):\n%a%a%a"
            (Dv.t_fpri SvMap.empty "  ") xr.t_num node_mapping_fpr svmap_r
            Col_utils.colv_map_complete_fpr colvmap_r;
        end;
      (* Sub-memory instantiation *)
      (* - saturate instantiation so as to reflect pointers into sub-mems *)
      let saturate (g: graph) (subm: sv_submem_mod): sv_submem_mod =
        let f (svcol: sv_collapsing): sv_collapsing =
          (* for each predecessor, gather offsets it reaches in the sub-mem *)
          let preds = get_predecessors_of_node svcol.svcol_base g in
          let offs =
            SvSet.fold (fun i -> collect_offsets i g) preds Offs.OffSet.empty in
          { svcol with svcol_extptr = offs } in
        { subm with sub_fold = SvMap.map f subm.sub_fold } in
      let subm_l = saturate xl.t_shape j_inst_l.subm_mod
      and subm_r = saturate xr.t_shape j_inst_r.subm_mod in
      if !flag_dbg_join_shape then
        Log.debug
          "SV submem modifs (after Saturate):\n  Left\n%a\n  Right\n%a"
          (sv_submem_mod_fpr "  ") subm_l (sv_submem_mod_fpr "  ") subm_r;
      (* - instantiation and renaming function *)
      let instantiate_rename
          (subm: sv_submem_mod)         (* node duplication, submems *)
          (map:  unit node_mapping)     (* for sv renaming *)
          (colvmap: colv_map_complete)  (* for colv renaming *)
          (n: Dv.t): Dv.t =
        if !flag_dbg_join_num || !flag_dbg_join_shape then
          Log.info "log instantiate_rename:\n%a" (Dv.t_fpri SvMap.empty "  ") n;
        (* - foldings of sub-memory regions (if any needs folding) *)
        let n =
          SvMap.fold
            (fun nn svcol ->
              Dv.symvars_merge svcol.svcol_stride svcol.svcol_base nn
                svcol.svcol_block svcol.svcol_extptr
            ) subm.sub_fold n in
        (* - renaming *)
        let n =
          (* filtering sub-memory roots in srename, nm_suboff field setting:
           *    we only keep the roots that correspond to actual references
           *    in the output graph *)
          let map = { map with nm_suboff = node_offset_is_referred g_out } in
          let om =
            (* by changing types in the join state record, we may avoid this
             * conversion step *)
            List.fold_left
              (fun acc (o_old, i_new, o_new) ->
                assert (not (OffMap.mem o_old acc));
                OffMap.add o_old (o_new, i_new) acc
              ) OffMap.empty subm.sub_new_off in
          let ocolvmap = if !do_exp_jinst then None else Some colvmap in
          Dv.symvars_srename om map ocolvmap n in
        (* - return *)
        if !flag_debug_array_blocks then
          Log.info "[sbase] Instantiated:\n%a" (Dv.t_fpri SvMap.empty "  ") n;
        n in
      if loc_debug || !flag_dbg_join_num then (* temptemp *)
        Log.info "J,before rename %a:\nL:\n%aR:\n%a" join_kind_fpr j
          (Dv.t_fpri SvMap.empty "  ") xl.t_num
          (Dv.t_fpri SvMap.empty "  ") xr.t_num;
      let n_l = instantiate_rename subm_l svmap_l colvmap_l xl.t_num in
      let n_r = instantiate_rename subm_r svmap_r colvmap_r xr.t_num in
      if loc_debug || !flag_dbg_join_num then (* temptemp *)
        Log.force "J,efore ValCol_dom %a:\nL:\n%aR:\n%a" join_kind_fpr j
          (Dv.t_fpri SvMap.empty "  ") n_l (Dv.t_fpri SvMap.empty "  ") n_r;
      (* - the actual numeric join *)
      let hint = Graph_utils.compute_hint_simpl g_out in
      let n_out =
        match j with
        | Jjoin | Jwiden -> Dv.upper_bnd ~hint j n_l n_r
        | Jdweak ->
            Log.fatal_exn "gen_join does not implement directed weakening" in
      if loc_debug || !flag_dbg_join_num then (* temptemp *)
        Log.force "J,ValCol_dom %a result: [%b] \n%a" (* temptemp *)
          join_kind_fpr j (Dv.is_le n_out n_l (fun _ _ -> false))
          (Dv.t_fpri SvMap.empty "  ") n_out;
      (* Graph SV/COLV fixes *)
      let g_out =
        if !do_exp_jinst then
          SvMap.fold (fun c _ -> Graph_utils.colv_rem c) col_torem g_out
        else g_out in
      let g_out = colv_inst_fold_rem colv_rem j_inst_l.set_inst g_out in
      let g_out = colv_inst_fold_rem colv_rem j_inst_l.seq_inst g_out in
      let g_out, _ = Graph_utils.sve_sync_bot_up g_out in
      (* Result *)
      let t_out = { t_shape  = g_out;
                    t_num    = n_out;
                    t_envmod = svenv_empty; } in
      let t_out = sve_fix t_out in
      check_nodes "join-out" t_out;
      t_out, svenv_upd_embedding svmap_l, svenv_upd_embedding svmap_r

    (* Directed weakening; over-approximates only the right element *)
    let directed_weakening (hso: sym_index hint_bs option)
        (roots_rel: sym_index tri_table)
        (colroots_rel: (sv, col_kind) tri_map)
        (xl: t) (xr: t): t * svenv_upd =
      sanity_check "dweak-l,before" true true xl;
      sanity_check "dweak-r,before" true true xr;
      (* Hint conversion *)
      let hgo = Option.map convert_bin_hint hso in
      (* Computation of the node_relation *)
      let wi =
        Aa_sets.fold
          (fun (il, ir, io) acc -> Nwkinj.add acc (il, ir) io)
          roots_rel Nwkinj.empty in
      (* Computation of setvar_relation *)
      let srel, qrel =
        Aa_maps.fold
          (fun (il, ir, io) ck (accs, accq) ->
            match ck with
            | CK_set ->
                let accs = Nwkinj.add accs (il, ir) io in
                accs, accq
            | CK_seq ->
                let accq = Nwkinj.add accq (il, ir) io in
                accs, accq)
          colroots_rel Nwkinj.(empty, empty) in
      (* Creation of the initial graph *)
      let g_init, (_, _), _ =
        init_graph_from_roots ~submem:false wi.wi_img
          srel.wi_img qrel.wi_img xl.t_shape xr.t_shape in
      (* Computation of the directed weakening in the graph *)
      let g_out, wi =
        Graph_dir_weak.directed_weakening
          xl.t_shape xr.t_shape ~vsat_r:(make_vsat xr) hgo wi g_init in
      (* Computation of the numerical weakened abstract value *)
      let map_r = Graph_dir_weak.extract_mapping xr.t_shape wi in
      if !flag_dbg_dweak_num then
        Log.info "DW,Renaming:\n%a%a" (Dv.t_fpri SvMap.empty "  ") xr.t_num
          node_mapping_fpr map_r;
      let map_r = { map_r with nm_suboff = fun _ -> Log.info "mapr"; true } in
      let n_r = Dv.symvars_srename OffMap.empty map_r None xr.t_num in
      if !flag_dbg_dweak_num then
        Log.info "DW,Renaming result:\n%a" (Dv.t_fpri SvMap.empty "  ") n_r;
      (* Result *)
      let g_out, _ = Graph_utils.sve_sync_bot_up g_out in
      let t_out = sve_fix { t_shape  = g_out;
                            t_num    = n_r;
                            t_envmod = svenv_empty; } in
      sanity_check "dweak-out" true true t_out;
      t_out, svenv_upd_embedding map_r

    (* Unary abstraction, sort of incomplete canonicalization operator *)
    let local_abstract (ho: sym_index hint_us option) (x: t): t =
      if !Flags.flag_sanity_graph then
        graph_sanity_check "local_abstract,before" x.t_shape;
      (* computation of the hint for underlying *)
      (*  i.e., nodes corresponding to values of hint variables *)
      let hu =
        let t = x.t_shape in
        let f h =
          let h = convert_uni_hint h in
          let svals =
            Aa_sets.fold
              (fun root acc ->
                let nt, pte =
                  pt_edge_extract (sat_val x)
                    (root, Offs.zero) Flags.abi_ptr_size t in
                assert (nt == t);
                assert (Offs.is_zero (snd pte.pe_dest));
                Aa_sets.add (fst pte.pe_dest) acc
              ) h.hug_live h.hug_live in
          { hug_live = svals } in
        Option.map f ho in
      (* weak local abstraction on graphs *)
      let xshape = Graph_abs.graph_abs hu ~vsat_l:(make_vsat x) x.t_shape in
      (* numeric domain variables need be filtered *)
      let xnodes = Graph_utils.get_all_nodes xshape in
      let xnum   = Dv.symvars_filter xnodes x.t_num in
      let o = { t_shape  = xshape;
                t_num    = xnum;
                t_envmod = svenv_empty; } in
      check_nodes "local-abs" o;
      o

    (** Unfolding support *)
    (* Returns a disjunction of cases *)
    let unfold_gen (uloc: unfold_loc) (udir: unfold_dir) (t: t)
        : (sv SvMap.t (* renaming *) * t (* new abstract *)) list =
      let l =
        match uloc with
        | Uloc_main n ->
            check_nodes "unfold-arg" t;
            if !Flags.flag_sanity_graph then
              graph_sanity_check "unfold,before" t.t_shape;
            (* Remove the inductive edge, and extract inductive
             * Perform materialization for each rule *)
            let lrems =
              Graph_materialize.unfold ~submem:false n udir t.t_shape in
            (* Add predicates *)
            List.map
              (fun ur ->
                if !Flags.flag_dbg_materialize then
                  Log.info "tmp,unfold result:\n%a" (unfold_res_fpri "  ") ur;
                let t = sve_fix { t with t_shape  = ur.ur_g; } in
                let nnum1 =
                  SvMap.fold
                    (fun ele (col_kind, info) acc ->
                       Dv.colv_add col_kind ~root:false ele info acc)
                    ur.ur_newcolvs t.t_num in
                let nnum1 =
                  List.fold_left (fun nacc p -> Dv.set_guard p nacc)
                    nnum1 ur.ur_setcons in
                let nnum1 =
                  List.fold_left (fun nacc p -> Dv.seq_guard p nacc)
                    nnum1 ur.ur_seqcons in
                let nnum1 =
                  List.fold_left
                    (fun nacc p -> Dv.guard true p nacc) nnum1 ur.ur_cons in
                let t =
                  SvMap.fold
                    (fun e k t ->
                      if Dv.colv_is_root e t.t_num then t
                      else
                        { t with
                          t_num   = Dv.colv_rem e t.t_num ;
                          t_shape = Graph_utils.colv_rem e t.t_shape }
                    ) ur.ur_remcolvs { t with t_num = nnum1 } in
                let t = sve_fix t in
                (* reduction of equal cells, determined in unfolding
                 * (in the empty segment case) *)
                let t, renaming = red_equal_cells ur.ur_eqs t in
                check_nodes "unfold-mid" t;
                if !Flags.flag_dbg_materialize then
                  Log.info "Numeric value:\n%a"
                    (Dv.t_fpri SvMap.empty "  ") nnum1;
                check_nodes "unfold-res" t;
                renaming, t
              ) lrems
        | Uloc_sub (i, n) ->
            let lnum = Dv.unfold i n udir t.t_num in
            List.map (fun (r, num) -> r, { t with t_num = num }) lnum in
      (* Filter out bottom values *)
      let aux msg l =
        Log.info "LLen(%s) %d" msg (List.length l);
        let i = ref 0 in
        List.iter
          (fun (_, x) ->
            Log.info "Unfold %d\n%a" !i (t_fpri "   ") x;
            incr i
          ) l in
      if false then aux "pre-filter" l;
      let l = List.filter (fun (_, x) -> not (is_bot x)) l in
      if false then aux "post-filter" l;
      l

    let unfold (n: sv) (t: t): t list =
      List.map snd (unfold_gen (Uloc_main n) Udir_fwd t)


    (** Cell operations *)

    (* Addition of a cell *)
    let cell_create ?(attr:node_attribute = Attr_none)
        ((si, so): Offs.svo) (sz: Offs.size) (nt: ntyp) (x: t): t =
      if !Flags.flag_sanity_graph then
        graph_sanity_check "cell_create,before" x.t_shape;
      (* creates destination node *)
      let dst, x = sv_add_fresh nt Nnone x in
      let pe = { pe_size = sz ;
                 pe_dest = dst, Offs.zero } in
      let x =
        let bsrc = si, Bounds.of_offs so in
        { x with
          t_shape = pt_edge_block_append bsrc pe x.t_shape } in
      if !Flags.flag_sanity_graph then
        graph_sanity_check "cell_create,after" x.t_shape;
      x

    (* Deletion of a cell *)
    let cell_delete ?(free:bool = false) ?(root:bool = false)
        (i: sv) (x: t): t =
      let g = if root then Graph_utils.sv_unroot i x.t_shape else x.t_shape in
      let g0 = Graph_utils.pt_edge_block_destroy i g in
      let g1 = if free then Graph_utils.mark_free i g0 else g0 in
      sve_fix { x with
                t_shape = g1 }

    (* Read and Resolve cells *)
    let cell_read_one ((isrc, osrc) as src: Offs.svo) (sz: int) (x: t)
        : t * Offs.svo =
      (* determine whether the edge is in a sub-memory region,
       * i.e., whether it source is in a sub-memory range:
       *  (1) or isrc is a negative index [keep going in the same submemory]
       *  (2) either isrc,osrc is in a sub-memory range [go down] *)
      let is_in_submem =
        if sv_to_int isrc < 0 then (* case (1) *)
          Cl_sub_intern
        else if Dv.is_submem_address isrc x.t_num then
          match pt_edge_localize_in_graph (sat_val x) src sz x.t_shape with
          | None ->
              (* base address is sub-memory address, but offset undetermined *)
              Log.warn "LOCALIZATION ISSUE (may fail later; check this out!)";
              Cl_sub_base
          | Some pe -> (* case (2) *)
              if Dv.is_submem_content (fst pe.pe_dest) x.t_num then
                Cl_sub_base
              else
                begin
                  Log.warn "CHECK whether there may be OVERLAPPING";
                  Cl_main
                end
        else Cl_main in
      match is_in_submem with
      | Cl_sub_base ->
          Log.info "\tabout to look in sub-memory %a %a"
            sv_fpr isrc Offs.t_fpr osrc;
          x, Dv.submem_read (sat_val x) isrc osrc sz x.t_num
      | Cl_sub_intern ->
          Log.info "\tabout to dereference a sub-memory node";
          x, Dv.submem_deref (sat_val x) isrc osrc sz x.t_num
      | Cl_main ->
          let nt, pte = pt_edge_extract (sat_val x) src sz x.t_shape in
          if Offs.size_to_int_opt pte.pe_size = Some sz then
            sve_fix { x with t_shape = nt }, pte.pe_dest
          else
            Log.fatal_exn "cell_read_one: not the right size (%d,%a)"
              sz Offs.size_fpr pte.pe_size

    (* Reading of a cell and (non)local unfolding *)
    let rec cell_read
        (is_loc_equal: bool)
        (src: Offs.svo)      (* address of the cell *)
        (sz: int)            (* size of the cell *)
        (x: t)               (* input abstract memory *)
        : (t                 (* output, may have changed e.g., if unfold *)
           * Offs.svo        (* address of the cell IN THE OUTPUT abs. mem *)
           * Offs.svo option (* Some content if successful, None otherwise *)
          ) list =           (* list of disjuncts *)
      cell_read_exc is_loc_equal Flags.max_unfold src sz x
    and cell_read_exc
        (is_loceq: bool) (count: int) (src: Offs.svo) (sz: int) (x: t)
          : (t * Offs.svo * Offs.svo option) list =
      let loc_debug = false in
      if !Flags.flag_sanity_graph then
        graph_sanity_check "cell_read_exc, before" x.t_shape;
      try
        let x, cont = cell_read_one src sz x in
        (* if Unfold_request is not raised, src is unchanged *)
        [ x, src, Some cont ]
      with
      | Graph_sig.No_edge msg ->
          if loc_debug then (* add a flag for this print *)
            begin
              Log.info "cell_read: No_edge exn [%s] caught in\n%a"
                msg (t_fpri "   ") x;
              Log.info "  flags: red:%b loceq:%b"
                !Flags.flag_reduce_quick_eval is_loceq
            end;
          (* TODO: this is not super clean:
           * we may now do a reduction in all cases (not just is_loceq)
           * and change the state, causing later issues coming from
           * dom_mem_shape_exprs (if the rest of the state changes) *)
          if !Flags.flag_reduce_quick_eval || is_loceq then
            let eq_set =
              pt_edge_localize_in_equal (sat_val x) src x.t_shape in
            let x, renaming = red_equal_cells eq_set x in
            let is, os = src in
            cell_read_exc is_loceq count (SvMap.find is renaming, os) sz x
          else
            try_nonlocal_unfold src sz x
      | Graph_sig.Unfold_request (i, dir) ->
          if count > 0 then
            let ncount = count - 1 in
            let l = unfold_gen i dir x in
            if !flag_dbg_unfold || loc_debug then
              List.iteri
                (fun i (_, u) -> Log.info "Unfold<%d>:\n%a" i (t_fpri "  ") u)
                l;
            let l = (* apply renaming to src *)
              List.map
                (fun (r0, u) ->
                  let isrc, osrc = src in
                  let isrc_new =
                    try SvMap.find isrc r0 with Not_found -> isrc in
                  cell_read_exc is_loceq ncount (isrc_new, osrc) sz u
                ) l in
            let l = List.flatten l in
            List.iter
              (fun (x, _, _) ->
                check_nodes "post-cell_read_exc unfold(u)" x;
                if !Flags.flag_sanity_graph then
                  graph_sanity_check "cell_read_exc, unfold, after" x.t_shape
              ) l;
            l
          else Log.fatal_exn "reached the maximal number of unfolds!"
    and try_nonlocal_unfold (src: Offs.svo) (sz: int) (x: t)
        : (t * svo * svo option) list =
      let loc_debug = false in
      (* The algorithm is very close to that in dom_mem_low_list for now
       * but we should generalize this code a lot *)
      (* (snd src) may be null, which is ok; just unfold at base (fst src) *)
      let isrc = fst src and osrc = snd src in
      if loc_debug then
        Log.info "tmp,Nlu,try: %a (%a) in\n%atmp,nlu,searching" nsv_fpr isrc
          Offs.t_fpr (snd src) (t_fpri "     ") x;
      (* Collecting inductive definitions with one unfold parameter *)
      let svs_inds, indi_opt =
        SvMap.fold
          (fun sv n (acc, opt) ->
            let do_ind (ind: ind) args =
              let def = acc, opt in
              if Option.fold ~none:true ~some:(fun (x,_) -> x = ind) opt then
                match ind.i_nonloc_unf_pars with
                | [] -> def
                | [ i, ck ] ->
                    if loc_debug then
                      Log.info "Nlu, unfold_par: %d" i;
                    if ck = CK_set then (* one unfold parameter found *)
                      let arg = List.nth args.ia_set i in
                      SvMap.add sv (arg, n.n_e) acc, Some (ind, i)
                    else def
                | _ :: _ :: _ -> def
              else acc, opt in
            match n.n_e with
            | Hemp | Hpt _ -> (acc, opt)
            | Hind ie -> do_ind ie.ie_ind ie.ie_args
            | Hseg se ->
                do_ind se.se_ind (ind_args_seg_to_ind_args se.se_eargs)
          ) x.t_shape.g_g (SvMap.empty, None) in
      (* If one inductive of the above kind was found, search for blocks
       * with the size matching that of the inductive definition *)
      let svs_pts =
        match indi_opt with
        | None -> SvSet.empty
        | Some (ind, _) ->
            SvMap.fold
              (fun sv n acc ->
                match n.n_e with
                | Hpt pt ->
                    if Some (Block_frag.byte_size pt) = ind.i_blksize then
                      SvSet.add sv acc
                    else acc
                | Hemp | Hind _ | Hseg _ -> acc
              ) x.t_shape.g_g SvSet.empty in
      if loc_debug then
        begin
          Log.info "tmp,Nlu,ind-opt: %a\n SVs   => %a\n SETVs:"
            (option_fpr "<?>" (fun fmt (i,_) -> F.fprintf fmt "%s" i.i_name))
            indi_opt svset_fpr svs_pts;
          SvMap.iter
            (fun sv (arg, _) ->
              Log.info "   %a => %a" nsv_fpr sv setv_fpr arg
            ) svs_inds
        end;
      (* Expression that needs to be satisfied to allow nonlocal unfolding *)
      let set_cons =
        let setvs =
          SvMap.fold (fun _ (e,_) -> SvSet.add e) svs_inds SvSet.empty in
        let set_expr = make_setexp ~is_disjoin:true ~setvs ~svs:svs_pts () in
        S_mem (isrc, set_expr) in
      let is_sat = Dv.set_sat set_cons x.t_num in
      if loc_debug then
        Log.info "tmp,Nlu,sat,set:\n   %a\n=> %b" set_cons_fpr set_cons is_sat;
      if is_sat then
        (* Compute the disjunction of cases *)
        let all_colvs =
          SvMap.fold (fun cv _ -> SvSet.add cv)
            (Graph_utils.colv_get_all x.t_shape) SvSet.empty in
        let ipar =
          match indi_opt with
          | None -> Log.fatal_exn "nlu, fails"
          | Some (_, i) -> i in
        let split_inds: t list =
          SvMap.fold
            (fun sv (arg, heap_frag) acc ->
              if loc_debug then
                Log.info "Nlu,at node: %a" nsv_fpr sv;
              let do_end colvs_add colvs_rem setcons seqcons xmem =
                (* TODO: attempt to remove COLVs that are already added *)
                (* check there is no risk to capture SV with wrong kind ? *)
                let colvs_add =
                  SvSet.fold
                    (fun cv acc -> SvMap.remove cv acc) all_colvs colvs_add in
                if loc_debug then
                  Log.info "Nlu,COLVs:\n + %a\n - %a" col_kinds_fpr colvs_add
                    col_kinds_fpr colvs_rem;
                (* Add new COLVs to the num *)
                let num =
                  SvMap.fold
                    (fun setv ck -> Dv.colv_add ck ~root:false setv None)
                    colvs_add x.t_num in
                (* Add the constraints to the numerics *)
                let num =
                  List.fold_left
                    (fun acc c -> Dv.set_guard c acc) num setcons in
                let num =
                  List.fold_left
                    (fun acc c -> Dv.seq_guard c acc) num seqcons in
                let num = Dv.set_guard (S_mem (isrc, S_var arg)) num in
                (* Construct states and remove redundant COLVs *)
                let xx = (* remove all non root COLVs to remove *)
                  SvMap.fold (fun cv _ -> colv_delete_non_root cv) colvs_rem
                    { x with t_shape = xmem; t_num = num } in
                xx :: acc in
              (* TODO: revise ind and seg cases to share code *)
              let module GM = Graph_materialize in
              match heap_frag with
              | Hind ie ->
                  if loc_debug then Log.info "Nlu,ind edge";
                  let xmem,sega,inda,sctrs,qctrs,colvs_add,colvs_rem =
                    GM.nlu_split_ind_pars x.t_shape isrc ipar ie in
                  (* Construct the new memory by removing/adding the edges *)
                  let xmem = ind_edge_rem sv xmem in
                  let xmem =
                    let se = { se_ind   = ie.ie_ind ;
                               se_sargs = ind_args_empty (* for now *) ;
                               se_eargs = sega ;
                               se_dargs = ind_args_empty (* for now *) ;
                               se_dnode = isrc } in
                    seg_edge_add sv se xmem in
                  let xmem =
                    ind_edge_add isrc { ie with ie_args = inda } xmem in
                  (* Finish with the common section *)
                  do_end colvs_add colvs_rem sctrs qctrs xmem
              | Hseg se ->
                  if loc_debug then Log.info "Nlu,seg edge";
                  sanity_check "nlu,seg,before split" true true x;
                  let xmem, sega, rsega, sctrs, qctrs, colvs_add, colvs_rem =
                    GM.nlu_split_seg_pars x.t_shape isrc ipar se in
                  (* Construct the new memory by removing/adding the edges *)
                  let xmem = seg_edge_rem sv xmem in
                  let xmem =
                    let se = { se_ind   = se.se_ind ;
                               se_sargs = se.se_sargs ;
                               se_eargs = sega ;
                               se_dargs = ind_args_empty (* for now *) ;
                               se_dnode = isrc } in
                    seg_edge_add sv se xmem in
                  let xmem =
                    let se = { se_ind   = se.se_ind ;
                               se_sargs = ind_args_empty (* for now *) ;
                               se_eargs = rsega ;
                               se_dargs = se.se_sargs ;
                               se_dnode = se.se_dnode } in
                    seg_edge_add isrc se xmem in
                  (* Finish with the common section *)
                  do_end colvs_add colvs_rem sctrs qctrs xmem
              | Hemp | Hpt _ -> Log.fatal_exn "nonlocal_unfold emp/Lhpt"
            ) svs_inds [] in
        if loc_debug then
          List.iteri (fun i x -> Log.info "Nlu abs %d\n%a" i (t_fpri "  ") x)
            split_inds;
        let ret =
          SvSet.fold
            (fun elt acc ->
              let num =
                let c = Nc_cons (Apron.Tcons1.EQ, Ne_var isrc, Ne_var elt) in
                Dv.guard true c x.t_num in
              let x = { x with t_num = num } in
              let rl =
                cell_read_exc false Flags.max_unfold (elt, osrc) sz x in
              List.iter
                (fun (rx,_,_) -> sanity_check "cell_read,after" true true rx)
                rl;
              List.rev_append rl acc
            ) svs_pts [ ] in
        let ret =
          List.fold_left
            (fun acc elt ->
              let rl = cell_read_exc false Flags.max_unfold src sz elt in
              List.iter
                (fun (rx,_,_) -> sanity_check "cell_read,after" true true rx)
                rl;
              List.rev_append rl acc
            ) ret split_inds in
        ret
      else Log.fatal_exn "nlu,fails"
    (* Cell_resolve *)
    let cell_resolve (src: Offs.svo) (size: int) (x: t)
        : (t * Offs.svo * bool) list =
      let f (x, src, op_cont) = x, src, op_cont != None in
      List.map f (cell_read true src size x)

    (* Write into a cell *)
    let cell_write (ntyp: ntyp)
        (dst: Offs.svo) (size: int) (ne: n_expr) (x: t): t =
      (* a logger that should eventually be removed *)
      let f_log = true in
      let log i s = if f_log then Log.info "LOGwrite[%d]: %s" i s in
      if !flag_dbg_assign then
        Log.info "cell_write:\n kind: %a\n dest: %a\n size: %d\n expr: %a"
          ntyp_fpr ntyp onode_fpr dst size n_expr_fpr ne;
      (* get the old points-to edge *)
      let get_old_pe (): pt_edge option =
        pt_edge_find_interval (sat_val x) (fst dst) (snd dst) size x.t_shape in
      log 0 "localizing";
      (* characterization of the place where the write should be done
       * (main-memory, or a sub-memory) *)
      let is_cell_sub, has_sub_add =
        if sv_to_int (fst dst) < 0 then (* node is internal to sub-memory *)
          begin
            Log.info "SUBMEM: internal node";
            Cl_sub_intern, None
          end
        else if Dv.is_submem_address (fst dst) x.t_num then
          (* check if the edge can be localized as part of the content *)
          match get_old_pe () with
          | None -> (* not found, we cannot conclude *)
              (* this case is very unsatisfactory *)
              (* switch flag to try to debug this... *)
              Log.warn "Failed write cell localization\n%a" (t_fpri "  ") x;
              if false then
                Log.fatal_exn "localization of cell to cell write fails"
              else Cl_sub_base, Some (fst dst)
          | Some pe ->
              if Dv.is_submem_content (fst pe.pe_dest) x.t_num then
                Cl_sub_base, Some (fst dst)
              else Cl_main, Some (fst dst)
        else (* we are sure it is not part of a sub-memory *)
          Cl_main, None in
      log 1 "found if edge is sub";
      if !flag_dbg_assign then
        Log.info "write in submem: %a" cell_loc_fpr is_cell_sub;
      (* check that a submemory contents node has only one predecessor
       * (to allow in place write);
       * - for now, we do the check, and abort when it fails
       * - eventually copy the sub-memory contents in the abstract *)
      let is_submem_updatable (i: sv) (x: t): unit =
        let n = SvSet.cardinal (node_find i x.t_shape).n_preds in
        if n != 1 then
          Log.fatal_exn "submem is not updateable without a copy" in
      let is_submem_old_edge_updatable (x: t): unit =
        let ope = get_old_pe () in
        match ope with
        | Some pe ->
            is_submem_updatable (fst pe.pe_dest) x
        | None ->
            match has_sub_add with
            | None -> Log.fatal_exn "submem_updateable: unsure"
            | Some i ->
                match get_pt_successors_of_node i x.t_shape with
                | None -> Log.fatal_exn "submem_updateable: unsure"
                | Some s ->
                    SvSet.iter
                      (fun i ->
                        if Dv.is_submem_content i x.t_num then
                          is_submem_updatable i x
                      ) s in
      (* Finishing a write operation into the main memory *)
      let write_in_main (x: t) (nval: Offs.svo): t =
        let pte = { pe_size = Offs.size_of_int size;
                    pe_dest = nval } in
        let nt = pt_edge_replace (sat_val x) dst pte x.t_shape in
        sve_fix { x with t_shape = nt } in
      (* Numeric cell write, in main memory *)
      let main_num () =
        (* if the old node is not shared, do not replace it *)
        let is_one_cell =
          match get_old_pe () with
          | Some pe ->
              let ndest = node_find (fst pe.pe_dest) x.t_shape in
              log 5
                (F.asprintf "zdest: %a, %b, %b" sv_fpr (fst pe.pe_dest)
                   (Dv.is_submem_address (fst pe.pe_dest) x.t_num)
                   (Dv.is_submem_content (fst pe.pe_dest) x.t_num));
              let c = SvSet.cardinal ndest.n_preds in
              let is_size_eq =
                match Offs.size_to_int_opt pe.pe_size with
                | None -> false
                | Some i -> i = size in
              (* report "one_cell" only if sizes matche exactly *)
              if is_size_eq && c = 1
                  && not (Dv.is_submem_address (fst pe.pe_dest) x.t_num) then
                Some (fst pe.pe_dest)
              else None
          | None -> None in
        match is_one_cell with
        | Some one_cell ->
            log 6 "assign one cell";
            { x with t_num = Dv.assign one_cell ne x.t_num }
        | None ->
            log 6 "assign not one cell";
            let nn, x = sv_add_fresh ntyp Nnone x in
            write_in_main { x with t_num = Dv.assign nn ne x.t_num }
              (nn, Offs.zero) in
      (* Assignment main := sub, numeric case *)
      let main_sub_var_num (iright: sv) =
        let nn, x = sv_add_fresh ntyp Nnone x in
        let ex = Ne_var iright in
        let x = { x with t_num = Dv.assign nn ex x.t_num } in
        write_in_main x (nn, Offs.zero) in
      (* Assignment main := sub, pointer case *)
      let main_sub_ptr (iright: sv) =
        match Dv.submem_localize iright x.t_num with
        | Sl_unk -> main_sub_var_num iright
        | Sl_found (ig, off) -> write_in_main x (ig, off)
        | Sl_inblock isub ->
            (* add a node, bind it to an offset, and mutation in main *)
            let nn, x = sv_add_fresh Ntint Nnone x in
            let off = Offs.of_n_expr (Ne_var nn) in
            let nnum, on = Dv.submem_bind isub iright off x.t_num in
            write_in_main { x with t_num = nnum } on in
      let ddst = Sd_env (fst dst, snd dst) in
      log 2 "about to discriminate cases";
      (* Case analysis *)
      match ntyp, ne, is_cell_sub with
      | _, Ne_var iright, Cl_main -> (* main := ... *)
          log 3 "main := ... (mutate)";
          if sv_to_int iright >= 0 then (* main := main *)
            write_in_main x (iright, Offs.zero)
          else if ntyp = Ntaddr then (* main := sub, ptr *)
            main_sub_ptr iright
          else (* main := sub, num *)
            main_sub_var_num iright
      | Ntaddr, _, Cl_main -> (* main := ... *)
          log 3 "main := ... (addr)";
          begin
            match decomp_lin_opt ne with
            | None ->
                log 4 "main_num";
                main_num ()
            | Some (iright,eoff) ->
                log 4 "found decomp";
                if sv_to_int iright >= 0 then (* main := main *)
                  write_in_main x (iright, Offs.of_n_expr eoff)
                else Log.todo_exn "cell write, ptr, main:=sub"
          end
      | _, _, Cl_main ->
          log 3 "main := ... (num)";
          main_num () (* numeric, main memory write *)
      | Ntaddr, _, Cl_sub_base -> (* sub := ... *)
          log 3 "sub := ... (addr)";
          (* - check that the submem can be updated in place *)
          is_submem_old_edge_updatable x;
          (* - perform a write in the sub-memory
           *   . after creating a node in the case of sub := main
           *   . without a node creation in the case of sub := sub
           * - schedule no main memory write *)
          begin
            match decomp_lin_opt ne with
            | None -> Log.todo_exn "cell_write, ptr, sub_base, none"
            | Some (iright,eoff) -> (* sub := ... *)
                let rhs =
                  if sv_to_int iright >= 0 then (* sub := main *)
                    Rv_addr (iright, Offs.of_n_expr eoff)
                  else (* sub := sub *)
                    Rv_expr ne in
                { x with t_num = Dv.write_sub ddst size rhs x.t_num }
          end
      | Ntaddr, _, Cl_sub_intern ->
          (* should be very similar to the case below, save for the
           * kind of expression that need be passed to write_sub *)
          log 3 "x_in_sub := ... (ptr)";
          Log.todo_exn "cell_write, in submem, pointer"
      | _, _, Cl_sub_intern ->
          log 3 "x_in_sub := ... (num)";
          Log.info "dest: %a" onode_fpr dst;
          let sv_sub =
            match Dv.submem_localize (fst dst) x.t_num with
            | Sl_unk   -> Log.fatal_exn "sub = ... (num), loc unk"
            | Sl_found (isub, off) ->
                Log.fatal_exn "sub = ... (num), loc-f, %a" sv_fpr isub
            | Sl_inblock isub -> isub in
          is_submem_updatable sv_sub x;
          let ddst = Sd_int (fst dst, snd dst) in
          { x with t_num = Dv.write_sub ddst size (Rv_expr ne) x.t_num }
      | _, _, Cl_sub_base ->
          log 3 "sub := ... (num)";
          (* - check that the submem can be updated in place *)
          is_submem_old_edge_updatable x;
          (* - perform a write in the sub-memory *)
          { x with t_num = Dv.write_sub ddst size (Rv_expr ne) x.t_num }

    (* Reduction function to be used in guard *)
    let guard_reduction (c: n_cons) (x: t): t =
      (* this function checks that, if 'c' is a guard condition of
       * an inductive edge *)
      let node_is_ind (n: sv) (g: graph): bool =
        match (node_find n g).n_e with
        | Hemp | Hpt _ | Hseg _ -> false
        | Hind _ -> true in
      let res =
        match c with
        | Nc_cons (Tcons1.EQ, Ne_var v, Ne_csti i)
        | Nc_cons (Tcons1.EQ, Ne_csti i, Ne_var v) ->
            if node_is_ind v x.t_shape then
              Some (v, Nc_cons (Tcons1.EQ, Ne_var v, Ne_csti i))
            else None
        | Nc_cons (Tcons1.DISEQ, Ne_var v, Ne_csti i)
        | Nc_cons (Tcons1.DISEQ, Ne_csti i, Ne_var v)->
            if node_is_ind v x.t_shape then
              Some (v, Nc_cons (Tcons1.DISEQ, Ne_var v, Ne_csti i))
            else None
        | Nc_cons (Tcons1.EQ, Ne_var v1, Ne_var v2)->
            if node_is_ind v1 x.t_shape then
              Some (v1, Nc_cons (Tcons1.EQ, Ne_var v1, Ne_var v2))
            else if node_is_ind v2 x.t_shape then
              Some (v2, Nc_cons (Tcons1.EQ, Ne_var v2, Ne_var v1))
            else None
        | Nc_cons (Tcons1.DISEQ, Ne_var v1, Ne_var v2) ->
            if node_is_ind v1 x.t_shape then
              Some (v1, Nc_cons (Tcons1.DISEQ, Ne_var v1, Ne_var v2))
            else if node_is_ind v2 x.t_shape then
              Some (v2, Nc_cons (Tcons1.DISEQ, Ne_var v2, Ne_var v1))
            else None
        | _ -> None in
      match res with
      | None -> x
      | Some (v, c) ->
          let cons, set_cons, seq_cons =
            Graph_materialize.unfold_num v c x.t_shape in
          let num =
            List.fold_left (fun nacc p -> Dv.guard true p nacc) x.t_num cons in
          let num =
            List.fold_left (fun nacc p -> Dv.set_guard p nacc) num set_cons in
          let num =
            List.fold_left (fun nacc p -> Dv.seq_guard p nacc) num seq_cons in
          { x with t_num = num }


    (** Transfer functions *)

    (* Condition test *)
    let rec guard (c: n_cons) (x: t): t =
      if !Flags.flag_sanity_graph then
        graph_sanity_check "guard,before" x.t_shape;
      if !flag_dbg_guard then
        Log.info "guard: %a\n%a" n_cons_fpr c
          (Dv.t_fpri SvMap.empty "  ") x.t_num;
      let r =
        (* first, apply the guard in the graph if the expression
         * is not a sub-memory expression *)
        let is_main = n_cons_fold (fun i b -> b && sv_to_int i >= 0) c true in
        let gtest =
          if is_main then graph_guard true c x.t_shape
          else Gr_no_info in
        (* then, apply  *)
        match gtest with
        | Gr_bot -> (* test in the graph level yields _|_ *)
            { x with t_num = Dv.bot }
        | Gr_no_info -> (* conventional guard function *)
            let x = if is_main then guard_reduction c x else x in
            { x with t_num = Dv.guard true c x.t_num }
        | Gr_equality (irem, ikeep) when
            (is_info irem x.t_shape || is_info ikeep x.t_shape) ->
            { x with t_num = Dv.guard true c x.t_num}
        | Gr_equality (irem, ikeep) -> (* equality reduction *)
            fst (red_equal_cells (SvPrSet.singleton (irem, ikeep)) x)
        | Gr_emp_seg i ->
            let x, renaming = red_empty_segment i x in
            let c =
              if SvMap.is_empty renaming then c
              else
                n_cons_map
                  (fun i ->
                    try SvMap.find i renaming with Not_found -> i
                  ) c in
            guard c x
        | Gr_emp_ind i ->
            (* this would allow removing inductives corresponding to
             * a empty region, and prevent join imprecisions
             * though, in many cases, it also loses important
             * information about actual inductive arguments, so it
             * cannot be activated yet *)
            let x = if is_main then guard_reduction c x else x in
            { x with
              (*t_shape = ind_edge_rem i tt.t_shape;*)
              t_num   = Dv.guard true c x.t_num } in
      if !flag_dbg_guard then
        Log.info "Domshape guard [%a]\n . Before:\n%a\n . After:\n%a"
          n_cons_fpr c (Dv.t_fpri SvMap.empty "   ") x.t_num
          (Dv.t_fpri SvMap.empty "   ") r.t_num;
      let x = sve_fix r in
      if !Flags.flag_sanity_graph then
        graph_sanity_check "guard,after" x.t_shape;
      if is_bot x then x
      else if true then
        { x with t_shape = red_empty_points_to (make_sat x) x.t_shape }
      else x

    (* Checking that a constraint is satisfied *)
    let sat (c: n_cons) (x: t): bool =
      if !Flags.flag_sanity_graph then
        graph_sanity_check "sat, before" x.t_shape;
      Dv.sat x.t_num c


    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    let mark_prio (lv: svo) (x: t): t =
      let sv, off = lv in
      assert (Offs.is_zero off);
      let node = SvMap.find sv x.t_shape.g_g in
      let node = { node with n_ptprio = true } in
      let graph = SvMap.add sv node x.t_shape.g_g in
      let x = { x with t_shape = { x.t_shape with g_g = graph } } in
      x


    (** Assuming and checking inductive edges *)

    (* Unfold *)
    let ind_unfold (u: unfold_dir) ((n,o): Offs.svo) (t: t): t list =
      assert (Offs.is_zero o);
      match u with
      | Udir_fwd ->
          if Graph_utils.node_is_ind n t.t_shape then
            (* there is an inductive edge to unfold *)
            unfold n t
          else
            (* there is no edge to unfold, we keep the abstract state as is *)
            [ t ]
      | Udir_bwd ->
          (* unfolding should affect a segment edge TO node n
           *  => thuse we should first localize such a segment
           * nb: fails if there is zero or several choices *)
          let s = seg_edge_find_to n t.t_shape in
          let card = SvSet.cardinal s in
          if card = 1 then
            let svmin = SvSet.min_elt s in
            Log.info "SUCCESS: %a" sv_fpr svmin;
            List.map snd (unfold_gen (Uloc_main svmin) Udir_bwd t)
          else (* failure case *)
            begin
              Log.info "FAILURE";
              [ t ]
            end

    (* Prepare parameters for ind_edge  *)
    let prepare_pars
        (tref: t) (* for evaluation of pointer fields *)
        (mayext: bool) (* whether graph may need extension *)
        (seg_dest: bool) (* whether ind call is segment edge destination *)
        (ic: Offs.svo gen_ind_call)
        (t: t) (* for evaluation of pointer fields *)
        : t (* graph produced *)
        * (sv * n_cons) list
        * sv list * sv list * sv list =
      assert (mayext != (tref == t));
      let ind = Ind_utils.ind_find ic.ic_name in
      (* Allocation of the node arguments, for integer parameters *)
      let comp_t, comp_ipars =
        match ic.ic_pars with
        | None -> t, [ ]
        | Some iii ->
            assert (List.length iii.ic_int = ind.i_ipars);
            List.fold_left
              (fun (accg, accl) -> function
                | Ii_const i ->
                    (* add a fresh node, and generate a constraint
                     * that it be equal to i *)
                    let n, ng = sv_add_fresh Ntint Nnone accg in
                    Log.info "LS:int parameter: |%a| => %d\n%a" nsv_fpr n i
                      (t_fpri "  ") accg;
                    ng, (n, Nc_cons (Tcons1.EQ, Ne_var n, Ne_csti i)) :: accl
                | Ii_lval (n,off) ->
                    assert (Offs.is_zero off);
                    let nacct =
                      if mayext && not (node_mem n accg.t_shape) then
                        let nn = node_find n tref.t_shape in
                        let nt = GU.sv_add n nn.n_t nn.n_alloc accg.t_shape in
                        { accg with t_shape = nt }
                      else accg in
                    nacct, (n, Nc_bool true) :: accl
              ) (t, [ ]) (List.rev iii.ic_int) in
      let back_t = comp_t in
      (* Reading the location of ptr parameters *)
      let comp_t, comp_ppars =
        match ic.ic_pars with
        | None -> comp_t, [ ]
        | Some iii ->
            assert (List.length iii.ic_ptr = ind.i_ppars);
            List.fold_left
              (fun ((acct: t), accl) ((n,off): Offs.svo) ->
                let nacct =
                  if mayext && not (node_mem n acct.t_shape) then
                    let nn = node_find n tref.t_shape in
                    let nt = GU.sv_add n nn.n_t nn.n_alloc acct.t_shape in
                    { acct with t_shape = nt }
                  else acct in
                assert (Offs.is_zero off); (* should evaluate to zero off *)
                nacct, n :: accl
              ) (comp_t, [ ]) (List.rev iii.ic_ptr) in
      (* set parameters *)
      let comp_t, comp_spars =
        match ic.ic_pars with
        (* In case of segment destination ind call there are no set parameters *)
        | _ when seg_dest -> comp_t, []
        | None -> comp_t, [ ]
        | Some iii ->
            assert (List.length iii.ic_set = ind.i_spars);
            List.fold_left
              (fun (acct, accl) i ->
                let s_uid = sv_unsafe_of_int i.colv_uid in
                if SvGen.key_is_used acct.t_shape.g_colvkey s_uid then
                  acct, s_uid :: accl
                else
                  let t_shape =
                    Graph_utils.colv_add ~root:true s_uid CK_set acct.t_shape in
                  { acct with t_shape }, s_uid :: accl
              ) (comp_t, [ ]) (List.rev iii.ic_set) in
      (* seq parameters *)
      let comp_t, comp_qpars =
        (* TODO: code dupplication with the above *)
        match ic.ic_pars with
        | None -> comp_t, [ ]
        | Some iii ->
            assert (List.length iii.ic_seq = ind.i_qpars);
            List.fold_left
              (fun (acct, accl) i ->
                let q_uid = sv_unsafe_of_int i.colv_uid in
                if SvGen.key_is_used acct.t_shape.g_colvkey q_uid then
                  acct, q_uid :: accl
                else
                  (* True: beacause it's an argument of ind predicates *)
                  (* JG:TODO to check... *)
                  let t_shape =
                    Graph_utils.colv_add ~root:true q_uid CK_seq acct.t_shape in
                  { acct with t_shape }, q_uid :: accl
              ) (comp_t, [ ]) (List.rev iii.ic_seq) in
      if not mayext then assert (comp_t == back_t);
      comp_t, comp_ipars, comp_ppars, comp_spars, comp_qpars

    (* Utility function for the assumption and the checking of segments *)
    let make_seg_edge
        (tref: t) (* for evaluation of pointer fields *)
        (mayext: bool) (* whether graph may need extension *)
        (iname: string) (* inductive name *)
        ((src, ic): sv * Offs.svo gen_ind_call)
        ((dst, ic_e): sv * Offs.svo gen_ind_call)
        (t: t)
        : t (* graph produced *)
          * (sv * n_cons) list
          * node_emb
          * colv_emb (* initial mapping, for inclusion *) =
      (* Modification only if different graph for evaluation *)
      assert (mayext != (tref == t));
      assert (ic.ic_name = iname && ic_e.ic_name = iname);
      let ind = Ind_utils.ind_find ic.ic_name in
      is_ind_collection_sane "dom_mem_low_shape,make_seg_edge" ind;
      (* Allocation of the node arguments, for integer parameters *)
      (* TODO: set/seq segment parameters; these calls are problematic *)
      let comp_t1, comp_ipars, comp_ppars, comp_spars, comp_qpars =
        prepare_pars tref mayext false ic t in
      let comp_t2, comp_ipars_e, comp_ppars_e, comp_spars_e, comp_qpars_e =
        prepare_pars tref mayext true ic_e comp_t1 in
      assert (comp_spars_e = [ ]) (*&& comp_qpars_e = [ ])*);
      (* Construction of the inductive edge *)
      let se = { se_ind  = ind ;
                 se_eargs = { ind_args_s_empty with
                              ias_set = comp_spars;
                              ias_seq =
                                try List.combine comp_qpars comp_qpars_e
                                with Invalid_argument _ ->
                                  Log.fatal_exn "invalid segment argument"} ;
                 se_sargs = { ind_args_empty with
                              ia_ptr = comp_ppars ;
                              ia_int = List.map fst comp_ipars };
                 se_dargs = { ia_ptr = comp_ppars_e ;
                              ia_int = List.map fst comp_ipars_e;
                              ia_set = [] ;
                              ia_seq = [] };
                 se_dnode = dst; } in
      hseg_sanity_check ~isok:1 "dom_mem_low_shape_make_seg_edge" se;
      (* Construction of the adequate injection *)
      let inj =
        let f = List.fold_left (fun acc i -> Aa_maps.add i i acc) in
        f (f (Aa_maps.add dst dst (Aa_maps.singleton src src)) comp_ppars)
          comp_ppars_e in
      let colv_inj =
        Aa_maps.empty
        |> List.fold_right (fun i -> Aa_maps.add i (i, CK_set)) comp_spars
        |> List.fold_right (fun i -> Aa_maps.add i (i, CK_set)) comp_spars_e
        |> List.fold_right (fun i -> Aa_maps.add i (i, CK_seq)) comp_qpars
        |> List.fold_right (fun i -> Aa_maps.add i (i, CK_seq)) comp_qpars_e in
      let tn = { comp_t2 with t_shape = seg_edge_add src se comp_t2.t_shape } in
      tn, List.rev_append comp_ipars comp_ipars_e, inj, colv_inj

    (* Utility function for the assumption and the checking of ind edges *)
    let make_ind_edge
        (tref: t) (* for evaluation of pointer fields *)
        (mayext: bool) (* whether graph may need extension *)
        (i: sv) (ic: Offs.svo gen_ind_call) (t: t)
        : t (* graph produced *)
        * (sv * n_cons) list
        * node_emb * colv_emb (* initial mapping, for inclusion *) =
      let ind = Ind_utils.ind_find ic.ic_name in
      let comp_t1, comp_ipars, comp_ppars, comp_spars, comp_qpars =
        prepare_pars tref mayext false ic t in
      (* Construction of the inductive edge *)
      let ie = { ie_ind  = ind ;
                 ie_args = { ia_ptr = comp_ppars ;
                             ia_int = List.map fst comp_ipars;
                             ia_set = comp_spars ;
                             ia_seq = comp_qpars } } in
      (* Construction of the adequate injection *)
      let inj, colv_inj =
        let f = List.fold_left (fun acc i -> Aa_maps.add i i acc) in
        (*HS: the next line is a bit weird *)
        let inj = (f (f (Aa_maps.singleton i i) comp_ppars) comp_ppars) in
        let colv_inj =
          Aa_maps.empty
          |> List.fold_right (fun i -> Aa_maps.add i (i, CK_set)) comp_spars
          |> List.fold_right (fun i -> Aa_maps.add i (i, CK_seq)) comp_qpars in
        inj, colv_inj in
      let tn = { comp_t1 with t_shape = ind_edge_add i ie comp_t1.t_shape } in
      tn, comp_ipars, inj, colv_inj

    (** Transfer functions *)
    let tr_setcon (x: svo setprop): set_cons =
      let s_var i =
        S_var (sv_unsafe_of_int i) in
      let tr_int i =
        match i with
        | None -> 0
        | Some i -> i in
      match x with
      | Sp_sub (l, r) -> S_subset (s_var l.colv_uid, s_var r.colv_uid)
      | Sp_mem ((i, o), r) -> (* check the off be zero for now*)
          let o =tr_int (Offs.to_int_opt o) in
          if o = 0 then
            S_mem (i, s_var r.colv_uid)
          else Log.fatal_exn "tr_setcon: offset is not eauqal to zero"
      | Sp_emp l -> S_eq (S_empty, s_var l.colv_uid)
      | Sp_euplus (l, (i, o), r) ->
          let o = tr_int (Offs.to_int_opt o) in
          if o = 0 then
            S_eq (s_var l.colv_uid, S_uplus (S_node i, s_var r.colv_uid))
          else Log.fatal_exn "tr_setcon: offset is not eauqal to zero"

    let tr_seqcon (x: svo seqprop): seq_cons =
      let tr_int i =
        match i with
        | None -> 0
        | Some i -> i in
      let rec tr_seqexp (x: svo seqexpr): seq_expr =
        match x with
        | Qe_empty -> Seq_empty
        | Qe_var x -> Seq_var (sv_unsafe_of_int x.colv_uid)
        | Qe_val (i, o) ->
            let o = tr_int (Offs.to_int_opt o) in
            if o <> 0 then
              Log.fatal_exn "tr_setcon: offset is not eauqal to zero";
            Seq_val i
        | Qe_sort s -> Seq_sort (tr_seqexp s)
        | Qe_concat (s1, s2) ->
            Seq_Concat [tr_seqexp s1; tr_seqexp s2] |> normalise in
      match x with
      | Qp_equal (s1, s2) ->
          Seq_equal (tr_seqexp s1, tr_seqexp s2)

    (* Assume construction: assume an inductive predicate *)
    let assume (op: meml_log_form) (x: t): t =
      match op with
      | SL_set sp ->
          let sp = tr_setcon sp in
          { x with t_num = Dv.set_guard sp x.t_num }
      | SL_seq sp ->
          let sp = tr_seqcon sp in
          { x with t_num = Dv.seq_guard sp x.t_num }
      | SL_ind (ic, (i, off)) ->
          (* Assume construction: assume an inductive predicate *)
          if inductive_is_allowed ic.ic_name then
            begin
              if !Flags.flag_sanity_graph then
                graph_sanity_check "assume,before" x.t_shape;
              assert (Offs.is_zero off);
              (* Generate the arguments (temp: only constants) *)
              let comp_t, comp_ipars, _ , _ = make_ind_edge x false i ic x in
              (* Taking into account all relations *)
              let comp_n =
                List.fold_left
                  (fun acc_n (_, i_ctr) ->
                    Dv.guard true i_ctr acc_n
                  ) comp_t.t_num comp_ipars in
              { t_shape  = comp_t.t_shape;
                t_num    = comp_n;
                t_envmod = svenv_empty; }
            end
          else x
      | SL_seg (ic, (i, off), ic_e, (i_e, off_e)) ->
          (* Assume construction: assume a segment predicate *)
          assert (ic.ic_name = ic_e.ic_name);
          if inductive_is_allowed ic.ic_name then
            begin
              if !Flags.flag_sanity_graph then
                graph_sanity_check "assume,before" x.t_shape;
              assert (Offs.is_zero off);
              assert (Offs.is_zero off_e);
              (* Generate the arguments (temp: only constants) *)
              let comp_t, comp_ipars, _, _ =
                make_seg_edge x false ic.ic_name (i, ic) (i_e, ic_e) x in
              (* Taking into account all relations *)
              let comp_n =
                List.fold_left
                  (fun acc_n (_, i_ctr) ->
                    Dv.guard true i_ctr acc_n
                  ) comp_t.t_num comp_ipars in
              (* Taking into account all relations *)
              { t_shape  = comp_t.t_shape;
                t_num    = comp_n;
                t_envmod = svenv_empty; }
            end
          else x
      | SL_array ->
          { x with t_num = Dv.assume VA_array x.t_num }

    let f_trans (inj: sv SvMap.t) (j: sv): sv =
      try SvMap.find j inj
      with Not_found -> Log.fatal_exn "renaming failed (dom_shape) %a" sv_fpr j

    (* part common to ind_check and seg_check *)
    let final_check (tref: t) (comp_ipars: (sv * Nd_sig.n_cons) list)
        (inj: (sv, sv) Aa_maps.t)
        (sinj: colv_emb) (x: t): bool =
      let r =
        Graph_is_le.is_le_partial ~fs:None ~submem:false false x.t_shape None
          SvSet.empty ~vsat_l:(make_vsat x) tref.t_shape inj sinj in
      match r with
      | Ilr_not_le | Ilr_le_seg _ -> false
      | Ilr_le_ind _ -> Log.fatal_exn "is_le returned an inductive"
      | Ilr_le_rem ilrem ->
          (* JG: todo*)
          assert (ilrem.ilr_setinst.cvi_new = SvSet.empty);
          assert (ilrem.ilr_seqinst.cvi_new = SvSet.empty);
          assert (ilrem.ilr_setinst.cvi_cons = []);
          assert (ilrem.ilr_seqinst.cvi_cons = []);
          (*assert (ilrem.ilr_setctr_r = []);*) (* causes fails ?? *)
          assert (ilrem.ilr_seqctr_r = []);
          (* Renaming and verification of the numeric relations *)
          List.for_all
            (fun (i, cons) ->
              let rcons = n_cons_map (f_trans ilrem.ilr_svrtol) cons in
              if !Flags.flag_debug_check_ind then
                Log.info "Check constraint %a => %a\nval num:\n%a" sv_fpr i
                  n_cons_fpr rcons (Dv.t_fpri SvMap.empty "  ") x.t_num;
              Dv.sat x.t_num rcons
            ) comp_ipars

    let check (op: meml_log_form) (x: t): bool =
      sanity_check "check,before" true true x;
      match op with
      | SL_set sp ->
          let sp = tr_setcon sp in
          Dv.set_sat sp x.t_num
      | SL_seq qp ->
          let qp = tr_seqcon qp in
          Dv.seq_sat qp x.t_num
      | SL_ind (ic, (i, off)) ->
          (* Check construction, that an inductive be there *)
          if !Flags.flag_sanity_graph then
            graph_sanity_check "ind_check,before" x.t_shape;
          if Dv.is_submem_address i x.t_num then
            (* inductive edge checking should be delegated to the sub-memory;
             *  - for now parameters are not handled in inductive to check *)
            if ic.ic_pars != None then
              Log.todo_exn "ind_check, sub-memory, with parameters"
            else Dv.check (VC_ind (i, off, ic.ic_name)) x.t_num
          else if not (Offs.is_zero off) then
            Log.fatal_exn "ind_check, main memory, but non zero offset"
          else (* "normal" case, inductive leaving in the main memory *)
            (* Generate the arguments (temp: only constants) *)
            let tref, comp_ipars, inj, sinj =
              let t0 = sv_add i Ntaddr Nnone (top ()) in
              make_ind_edge x true i ic t0 in
            final_check tref comp_ipars inj sinj x
      | SL_seg (ic, (i, off), ic_e, (i_e, off_e)) ->
          if !Flags.flag_sanity_graph then
            graph_sanity_check "seg_check,before" x.t_shape;
          Log.debug "source: %a,%a" sv_fpr i Offs.t_fpr off;
          assert(Offs.is_zero off);
          assert(Offs.is_zero off_e);
          (* we don't deal with submem at the moment *)
          (* Generate the arguments (temp: only constants) *)
          if ic = ic_e && (i, off) = (i_e, off_e) then
            true
          else
            let tref, comp_ipars, inj, sinj =
              let t0 = sv_add i Ntaddr Nnone (top ()) in
              let t0 = if i_e = i then t0 else sv_add i_e Ntaddr Nnone t0 in
              make_seg_edge x true ic.ic_name (i, ic) (i_e, ic_e) t0 in
            final_check tref comp_ipars inj sinj x
      | SL_array -> Dv.check VC_array x.t_num

    (** Construction from formulas and checking of formulas *)
    (* Auxiliary function to construct the abstract shape *)
    (* TODO: move some of the functions below to a separate file spec_utils *)
    let build_t
        (opt_sym_to_sv: sv StringMap.t option) (* names already fixed *)
        ((henv, heap, pure): s_formula)
        (col_params: colvar StringMap.t)
        : t * sv StringMap.t * sv StringMap.t
        * (sv * col_kind * colv_info option) StringMap.t =
      let crash msg t =
        Log.todo_exn "build_t: %s\n%a\n%a" msg (t_fpri "  ") t
          Spec_utils.s_formula_fpr (henv, heap, pure) in
      let t = top () in
      (* Creation of the nodes that are required for the environment *)
      let t, env, dict =
        match opt_sym_to_sv with
        | None ->
            StringMap.fold
              (fun var svar (t, env, dict) ->
                let sv, t = sv_add_fresh Ntaddr Nstack t in
                t, StringMap.add var sv env, StringMap.add svar sv dict
              ) henv (t, StringMap.empty, StringMap.empty)
        | Some sym_to_sv ->
            let t, env =
              StringMap.fold
                (fun var svar (t, env) ->
                  let sv = StringMap.find svar sym_to_sv in
                  let t_shape =
                    Graph_utils.sv_add ~attr:Attr_none ~root:false sv
                      Ntaddr Nstack t.t_shape in
                  let t = sve_fix { t with t_shape } in
                  t, StringMap.add var sv env
                ) henv (t, StringMap.empty) in
            t, env, sym_to_sv in
      let t, colv_env, colv_dict =
        StringMap.fold
          (fun vname {colv_kind; colv_uid} (t, colv_env, colv_dict) ->
            let colv, t, info = colv_add_fresh true vname colv_kind t in
            let colv_env  =
              StringMap.add vname (colv, colv_kind, info) colv_env in
            let colv_dict =
              StringMap.add vname (colv, colv_kind, info) colv_dict in
            t, colv_env, colv_dict)
          col_params
          (t, StringMap.empty, StringMap.empty) in
      (* Creation of the nodes that are required for the heap *)
      let t, dict =
        let do_sval svsym (t, dict) =
          if StringMap.mem svsym dict then t, dict else
          let sv, t = sv_add_fresh Ntaddr Nheap t in
          t, StringMap.add svsym sv dict in
        let do_arg a = function
          | A_sval sv -> do_sval sv a
          | A_svar _ -> Log.todo_exn "do_arg:svar,ko" in
        let do_colval (vname: string): unit =
          if StringMap.mem vname colv_dict then () else
          Log.fatal_exn "from_s_formula: unknown colvar: %s" vname in
        let do_colvarg = function
          | A_sval colv -> do_colval colv
          | A_svar _ -> Log.todo_exn "do_colvarg: svar:ko" in
        let do_arg_list l a =
          List.fold_left do_arg a l in
        let do_iargs {num; ptr; set; seq} acc =
          let acc = do_arg_list num acc in
          let acc = do_arg_list ptr acc in
          (* For collection variables we don't have to translate them.
           * We only check that they are defined. *)
          List.iter do_colvarg set;
          List.iter do_colvarg seq;
          acc in
        let do_term (t, dict) = function
          | Sht_cell (sv0, _, sv1, _) ->
              (t, dict)
              |> do_sval sv0
              |> do_sval sv1
          | Sht_ind (_, (sv, svl)) ->
              (t, dict)
              |> do_sval sv
              |> do_iargs svl
          | Sht_seg (_, (sv0, svl0), (sv1, svl1)) ->
              (t, dict)
              |> do_sval sv0
              |> do_iargs svl0
              |> do_sval sv1
              |> do_iargs svl1 in
        List.fold_left do_term (t, dict) heap in
      (* Function to retrieve sv *)
      let get svsym =
        try StringMap.find svsym dict
        with Not_found -> crash "unbound svsym" t in
      let get_sarg: s_arg -> svo = function
        | A_svar _ -> assert false
        | A_sval sv -> get sv, Offs.zero in
      let get_colvarg: s_arg -> colvar = function
        | A_svar _ -> assert false
        | A_sval colv ->
          try
            let colvar = StringMap.find colv col_params in
            let colv_sv,_, _ = StringMap.find colv colv_env in
            let Refl = Sv_def.eq in
            { colvar with colv_uid = colv_sv }
        with Not_found -> Log.fatal_exn "colv %s not declared" colv in
      let get_args (args: s_arg s_iargs) : svo gen_ind_pars =
        { ic_ptr = List.map get_sarg args.ptr;
          ic_int = List.map (fun a -> Ii_lval(get_sarg a)) args.num;
          ic_set = List.map get_colvarg args.set ;
          ic_seq = List.map get_colvarg args.seq } in
      (* Addition of the edges in the graph *)
      let t =
        let mpt, lind =
          List.fold_left
            (fun (mpt, lind) -> function
              | Sht_cell (sv0, r0, sv1, r1) ->
                  let o = try StringMap.find sv0 mpt with Not_found -> [ ] in
                  let n = (r0, sv1, r1) :: o in
                  StringMap.add sv0 n mpt, lind
              | Sht_ind (_, (_, _)) | Sht_seg (_, (_, _), (_, _)) as t ->
                  mpt, t :: lind
            ) (StringMap.empty, [ ]) heap in
        (* inductive edges *)
        let t =
          List.fold_left
            (fun (t: t) -> function
              | Sht_ind (ind, (sv, a)) as h ->
                  let sv = get sv in
                  let pars = get_args a in
                  let ic = {
                    ic_name = ind;
                    ic_pars = Some pars } in
                  if !Flags.flag_debug_spec then
                    Log.debug "%a\n  => %a"
                      Spec_utils.s_hterm_fpr h
                      (Vd_utils.gen_ind_call_fpr onode_fpr) ic;
                  let t, _, _, _ =
                    make_ind_edge t false sv ic t in
                  t
              | Sht_seg (ind, (svs, argss), (svd, argsd)) as h ->
                  let svs = get svs in
                  let svd = get svd in
                  let pars = get_args argss in
                  let pard = get_args argsd in
                  let ics = {
                    ic_name = ind ;
                    ic_pars = Some pars } in
                  let icd = {
                    ic_name = ind;
                    ic_pars = Some pard } in
                  if !Flags.flag_debug_spec then
                    Log.debug "%a\n  => %a*=%a"
                      Spec_utils.s_hterm_fpr h
                      (Vd_utils.gen_ind_call_fpr onode_fpr) ics
                      (Vd_utils.gen_ind_call_fpr onode_fpr) icd;
                  let t, _, _, _ =
                    make_seg_edge t false ind (svs, ics) (svd, icd) t in
                  t
              | Sht_cell (_, _, _, _) -> Log.fatal_exn "unexpected Sht_cell"
            ) t lind in
        (* points-to edges *)
        let t =
          let do_offset = function
            | FInd.So_int o -> o
            | _ -> crash "unhandled offset" t in
          let do_size so = Offs.to_size (do_offset so) in
          StringMap.fold
            (fun src l t ->
              (* This line is important !
               * Before you comment it, make sure you tested it on tests having
               * a cell with at least two offsets entries. *)
              let l = List.rev l in
              let src = get src, Bounds.zero in
              let t, _ =
                List.fold_left
                  (fun (t, base) (r, dest, r_dest) ->
                    let size = do_size (snd r) in
                    let src = fst src, Bounds.of_offs base in
                    let next_base = Offs.add_size (do_offset (fst r)) size in
                    let pe = { pe_size = size ;
                               pe_dest = get dest, do_offset (fst r_dest) } in
                    let t =
                      { t with
                        t_shape = pt_edge_block_append src pe t.t_shape } in
                    t, next_base
                  ) (t, Offs.zero) l in
              t
            ) mpt t in
        t in
      if !Flags.flag_debug_spec then
        begin
          let colv_ck_info_fpr fmt (colv, ck, info) =
            match ck with
            | CK_set ->
                Format.fprintf fmt "%a %a" setv_fpr colv
                  (Option.t_fpr Col_utils.colv_info_fpr) info
            | CK_seq ->
                Format.fprintf fmt "%a %a" seqv_fpr colv
                  (Option.t_fpr Col_utils.colv_info_fpr) info in
          Log.debug "env:\n  %a\ncolv_env\n  %a\ndict:\n  %a"
            (StringMap.t_fpr "\n  " nsv_fpr) env
            (StringMap.t_fpr "\n  " colv_ck_info_fpr) colv_env
            (StringMap.t_fpr "\n  " nsv_fpr) dict
        end;
      t, env, dict, colv_env
    (* Pre-condition construction, from spec *)
    let from_s_formula
        (col_params: colvar StringMap.t)
        ((henv, heap, pure) as sform: s_formula)
        : t * s_pre_env =
      let loc_debug = false in
      (* Construction of the shape abstraction *)
      let t, env, dict, colv_env = build_t None sform col_params in
      (* Guard with the pure predicates *)
      let guard_pterm t pt =
        match Spec_utils.build_meml_cons dict colv_env pt with
        | Mc_num c -> guard c t
        | Mc_set c -> { t with t_num = Dv.set_guard c t.t_num }
        | Mc_seq c -> { t with t_num = Dv.seq_guard c t.t_num } in
      let t = List.fold_left guard_pterm t pure in
      if loc_debug then Log.info "from_s_formula returns:\n%a" (t_fpri "  ") t;
      t, (env, colv_env)
    (* Post-condition check, from spec *)
    let sat_s_formula (sym_to_sv: sv StringMap.t)
        (cvs: (sv * col_kind * colvar) StringMap.t)
        (f: s_formula) (t: t): bool =
      let loc_debug = false in
      let tgoal, _, dict, colv_env =
        let cvs = StringMap.map (fun (_, _, a) -> a) cvs in
        build_t (Some sym_to_sv) f cvs in
      if !Flags.flag_enable_ext_export_all then
        begin
          let out_name = (Flags.out_prefix ())^"-3-N0-D0" in
          let namer =
            let dict_inv =
              StringMap.fold
                (fun key sv -> SvMap.add sv (key, sv))
                dict SvMap.empty in
            fun sv ->
              SvMap.find_opt sv dict_inv |> Option.get_default ("", sv) in
          ext_output (Out_dot ([], [])) out_name namer tgoal
        end;
      let svinj =
        StringMap.fold
          (fun _ sv acc ->
            Aa_maps.add sv sv acc
          ) sym_to_sv Aa_maps.empty in
      let cvinj =
        StringMap.fold (fun _ (a, b, _) acc -> Aa_maps.add a (a, b) acc)
          cvs Aa_maps.empty in
      (* Usually the addition of colv info variable is done in dom_env for
       * inclusion test. We do it now since dom_env has no
       * colv |-> {min; max; size} mapping yet. *)
      let svinj =
        Aa_maps.fold
          (fun colv_l (colv_r, ck) acc ->
            match
              SvMap.find_opt colv_l t.t_shape.g_colvinfo,
              SvMap.find_opt colv_r tgoal.t_shape.g_colvinfo
            with
            | Some {min = min_l; max = max_l; size = size_l},
              Some {min = min_r; max = max_r; size = size_r} ->
                acc
                |> Aa_maps.add min_l min_r
                |> Aa_maps.add max_l max_r
                |> Aa_maps.add size_l size_r
            | None, None -> acc
            | Some _, None | None, Some _ ->
                Log.fatal_exn "unexpected non-matching colvs information")
          cvinj svinj in
      if loc_debug then
        Log.todo "sat_s_formula:\nState:\n%aGoal:\n%a%a%a"
          (t_fpri "  ") t (t_fpri "  ") tgoal
          (bin_table_fpri sv_fpr " ") svinj (colv_emb_fpri "  ") cvinj;
      match is_le svinj cvinj t tgoal with
      | None ->
          if loc_debug then Log.info "sat_s_formula: is_le failed";
          false
      | Some tr -> (* tr: svenv_upd = sv -> sv_upd *)
          if loc_debug then
            Log.info "sat_s_formula: is_le checked; looking at pure formulas";
          let right_to_left =
            SvMap.fold
              (fun sv_l _ acc ->
                match tr sv_l with
                | Svu_mod (sv_r, svs_r) ->
                    if loc_debug then
                      Log.info "  %a { %a } => %a" nsv_fpr sv_r nsvset_fpr svs_r
                        nsv_fpr sv_l;
                    SvSet.fold (fun sv_r -> SvMap.add sv_r sv_l) svs_r
                      (SvMap.add sv_r sv_l acc)
                | Svu_rem ->
                    if loc_debug then Log.info "%a was removed" sv_fpr sv_l;
                    acc
                | exception Not_found ->
                    Log.fatal_exn "%a was not found" sv_fpr sv_l
              ) t.t_shape.g_g SvMap.empty in
          let r =
            try
              let dict =
                StringMap.fold
                  (fun name sv acc ->
                    match SvMap.find_opt sv right_to_left with
                    | Some sv_l -> StringMap.add name sv_l acc
                    | None ->
                        Log.info "Node %a is not in r_2_l mapping"
                          nsv_fpr sv;
                        SvMap.iter
                          (fun sv0 sv1 ->
                            Log.info "  %a => %a" nsv_fpr sv0 nsv_fpr sv1
                          ) right_to_left;
                        (* TEMP: Trying to continue, to see result *)
                        acc
                  ) dict StringMap.empty in
              let _, _, pf = f in
              List.iter
                (fun pt ->
                  let b, s =
                    try
                      let b =
                        match Spec_utils.build_meml_cons dict colv_env pt with
                        | Mc_num c -> sat c t
                        | Mc_set c -> Dv.set_sat c t.t_num
                        | Mc_seq c -> Dv.seq_sat c t.t_num in
                      b, if b then "ok" else "ko"
                    with exn -> false, "fail:"^Printexc.to_string exn in
                  Log.info "sat_s_formula, to prove: %a => %s"
                    Spec_utils.s_pterm_fpr pt s;
                  if not b then raise False
                ) pf;
              true
            with False -> false in
          Log.info "sat_s_formula: result %b" r;
          r
  end : DOM_MEM_LOW)
