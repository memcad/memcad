(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_sig.ml
 **       signatures of a simplified inductive domain based on lists
 **       (no parameters, no arbitrary inductives)
 ** Xavier Rival, 2014/02/23 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_sig
open Array_ind_sig
open Nd_sig
open Set_sig
open Seq_sig
open Svenv_sig
open Vd_sig

(** The purpose of this domain is to implement a simplified version of
 ** the general inductive definition based abstract domain, and to serve
 ** for experimentation. *)

(** Ptr predicates *)

type ptr_var =
  | Pv_this
  | Pv_actual of int  (* i-th actual parameter *)
  | Pv_field of int   (* contents of a field of offset i *)

type ptri =
  { (* list of parameters in recursive calls *)
    p_params: ptr_var list ;
    (* Map offs |-> ptr parameter for unfolding *)
    (* JG: add offset for destination *)
    p_off: (int * int) IntMap.t}

(** Set predicates *)

(* Variables in constraints, that denote set elements *)
type set_elt =
  | Se_this         (* address of the current object *)
  | Se_field of int (* contents of a field of offset i *)

(* Variables in constraints, that denote sets, in the context of an inductive
 * definition; those variables refer to parameters:
 *  - actual parameters (of the current call)
 *  - next parameter (of the call corresponding to the tail of the list)
 *  - sub-parameter (of a call corresponding to a nested list) *)
type set_var =
  | Sv_actual of int  (* i-th actual parameter ---E_i *)
  | Sv_nextpar of int (* i-th parameter of next call ---E'_i *)
  | Sv_subpar of int * int (* i-th parameter of j-th sub-call ---E^j_i *)

(* Expressions defining a set, in the context of the inductive definition *)
type set_def =
  | Sd_var of set_var              (* X *)
  | Sd_uplus of set_elt * set_var  (* {y} \uplus X *)
  | Sd_eunion of set_elt * set_var (* {y} \union X *)
  | Sd_union of set_var * set_var  (* X \union Y *)

(* Constraints over sets, in the context of the inductive definition *)
type set_equa =
  | Se_mem of set_elt (* i0 *) * set_def (* s1 *)    (* i0 \in s1 *)
  | Se_eq  of set_var (* s0 *) * set_def (* s1 *)    (* s0 = s1 *)

(* Set part to a "list" inductive definition *)
type seti =
    { (* set parameter kinds *)
      s_params:  set_par_type list; (* const/head/add *)
      (* parameter used for non-local unfolding an inductive to a segment
       * and an inductive edge *)
      s_uf_seg:  int option;
      (* set constraints associated to the definition *)
      s_equas:   set_equa list }

(** Seq predicates *)

(* Variables in constraints, that denote seq elements *)
type seq_elt =
  | Qe_this         (* address of the current object *)
  | Qe_field of int (* contents of a field of offset i *)

(* Variables in constraints, that denote seqs, in the context of an inductive
 * definition; those variables refer to parameters:
 *  - actual parameters (of the current call)
 *  - next parameter (of the call corresponding to the tail of the list)
 *  - sub-parameter (of a call corresponding to a nested list) *)
type seq_var =
  | Qv_actual of int  (* i-th actual parameter ---E_i *)
  | Qv_nextpar of int (* i-th parameter of next call ---E'_i *)

(* Expressions defining a set, in the context of the inductive definition *)
type seq_def =
  | Qd_empty                       (* [] *)
  | Qd_var of seq_var              (* X *)
  | Qd_val of seq_elt              (* [a] *)
  | Qd_concat of seq_def list      (* S_1.__.S_n *)
  | Qd_sort of seq_def             (* sort( S ) *)

(* Constraints over sets, in the context of the inductive definition *)
type seq_equa =
  | Qe_eq  of seq_var (* s0 *) * seq_def (* s1 *)    (* s0 = s1 *)

(* Seq part to a "list" inductive definition *)
type seqi =
    { (* set parameter kinds *)
      q_params:  seq_par_type list; (* const/head *)
      (* parameter used for unfolding an inductive to a segment and an
       * inductive edge (i.e. non-local unfolding).
       * It is an head parameter  *)
      q_uf_seg:  int option;
      (* seq constraints associated to the definition *)
      q_equas:   seq_equa list }

(** Abstract elements *)

(* "List" inductive definition body *)
type l_def =
    { ld_name:    string option; (* name of the ind. def. it derives from *)
      ld_nextoff: int;           (* offset of the next field (list tail ptr) *)
      ld_nextdoff: int;          (* offset of the destination *)
      ld_prevpar: int option;    (* prev field (for dll) parameter number *)
      ld_size:    int;           (* size of the block describing an element *)
      ld_onexts:  (int * l_def * int) list; (* nested lists (if any) *)
      ld_submem:  submem_ind option; (* list type: whether in array *)
      ld_m_offs:  int list;      (* offset for maya variables*)
      ld_emp_csti: int;          (* endpoint of the list*)
      ld_emp_mcons: mform list;  (* maya constraints "emp" case *)
      ld_next_mcons: mform list; (* maya constraints for "next" case *)
      ld_ptr:     ptri option;   (* ptr constraints (if any) *)
      ld_set:     seti option;   (* set constraints (if any) *)
      ld_seq:     seqi option;   (* seq constraints (if any) *) }

(* "List" inductive predicate, with parameters *)
type l_call =
    { lc_def:   l_def;   (* definition (induction scheme) *)
      lc_ptrargs:  sv list (* arguments; list of ptr   *) ;
      lc_setargs:  sv list (* arguments; list of Set COLVs *) ;
      lc_seqargs:  sv list (* arguments; list of Seq COLVs *) }

(* "List" inductive predicate, with parameters *)
type lseg_call =
    { lc_def:   l_def;   (* definition (induction scheme) *)
      lc_ptrargs:  sv list (* arguments; list of ptr   *) }

(* Kinds of heap fragments for a node (very similar to graph) *)
type lheap_frag =
  | Lhemp                             (* empty (no memory) *)
  | Lhpt of pt_edge Block_frag.t      (* block *)
  | Lhlist of l_call                  (* complete list *)
  | Lhlseg of l_call * sv * lseg_call (* segment of a list *)

type lnode =
    { ln_i:     sv;           (* node id *)
      ln_e:     lheap_frag;   (* memory block at this address *)
      ln_typ:   ntyp;         (* type *)
      ln_prio:  bool;         (* For pr-prio join/isle directive *)
      ln_odesc: l_def option; (* desc that node may have had in the past *) }

(* SV invariants:
 *  - svemod holds already performed local changes
 *  - prerem SVs are not reachable and ready to be disposed of
 *  - nkey holds SVs that are allocated (could be in prerem)
 *)
type lmem =
    { (* Memory state *)
      lm_mem:       lnode SvMap.t;
      (* Managemeng of SVs *)
      lm_nkey:      SvGen.t; (* key allocator for the symbolic variables *)
      lm_roots:     SvSet.t;
      lm_svemod:    svenv_mod;
      (* Dangling SVs: neither root nor referenced somewhere else *)
      lm_dangle:    SvSet.t;
      (* SVs for collections (sequence or set predicates): COLVs *)
      lm_colvkey:   SvGen.t;              (* key allocator *)
      lm_colvroots: SvSet.t;              (* roots *)
      lm_colvkind:  col_par_type SvMap.t; (* kind: set(kind) | seq(kind) *)
      (* GC management by ref counting and prev pointers;
       *  - lm_refcnt stores a map of the form
       *               node => prev => number of occurences
       *                               of "node" as a successor of prev *)
      lm_refcnt:    int SvMap.t SvMap.t; }

(** Unfolding utilities *)

(* These types seem to be used during unfolding, as a step between the
 * definition of the inductive edge and the production of the result lmem *)
type l_content =
  | Data of int         (* Raw data field.
                         *  - the int arguement is its size *)
  | Call of l_def * int (* Ptr + inductive or nested call to list predicate;
                         *  - the l_def argument is the inductive
                         *  - the int agument is the offset in the call
                         * Field of the form  this.o->a * a.ind()  *)
  | Ptr of (sv * int)   (* Pointer somewhere:
                         *  - target sv (?)
                         *  - the int argument is the destination offset *)

(** a [list_cell] maps offset to the content at this offset *)
type l_cell = l_content IntMap.t

(** Unfolding algorithm result *)

type unfold_result =
    { ur_lmem:     lmem;
      (* Constraints *)
      ur_cons:     n_cons list;           (* num constraints, to integrate *)
      ur_mcons:    mform list;            (* maya constraints *)
      ur_setcons:  set_cons list;         (* set constraints, to integrate *)
      ur_seqcons:  seq_cons list;         (* seq constraints, to integrate *)
      (* Added keys *)
      ur_newcolvs: col_kinds;             (* set of new COLVs *)
      (* Removed keys *)
      ur_remcolvs: col_kinds;             (* set of removed COLVs *) }


(** Guard result (to guide the reduction afterwards) *)

type lguard_res =
  | Gr_no_info           (* no information, do nothing *)
  | Gr_bot               (* incompatible constraints, need to reduce to _|_ *)
  | Gr_sveq of sv * sv   (* two SVs are found equal; reduce by removing one *)


(** Partial inclusion checking algorithm output *)
(* Shared record type *)
type l_ilrem =
    { ilr_lmem_l:    lmem;            (* remaining part of the left hand side *)
      ilr_svrem_l:   SvSet.t;         (* SV removed in the left hand memory *)
      ilr_sv_rtol:   sv SvMap.t;      (* Img of SV embedding *)
      ilr_colv_rtol: colv_embedding_comp; (* Img of COLV embedding *)
      ilr_setctr_r:  set_cons list; (* un-discharged, un-renamed R-SET ctrs *)
      ilr_seqctr_r:  seq_cons list; (* un-discharged, un-renamed R-SEQ ctrs *) }

(* Ilr_not_le:  failed to establish inclusion
 * Ilr_le:      success, with node mapping right to left;
 *              when boolean argument true, constraints are checked
 *       and set variable mapping from right to left, set domain
 *       in right side, and some unfold set info to help implication prove
 *)
type is_le_res = (* yes or maybe comparison resimt *)
  [ `Ilr_le of (l_ilrem * bool) (* boolean: whether is_success passed *)
  | `Ilr_not_le ]
type is_le_weaken_guess =
  [ `Ilr_le_list of l_ilrem
  | `Ilr_le_lseg of l_ilrem * sv (* Segment destination identified *)
  | `Ilr_not_le ]


(** Join algorithm output *)

type join_output =
   { (* output shape domain*)
     out:       lmem;
     (* relation between outputs and inputs *)
     rel:       node_relation;
     (* set constraints to use in the instantiation *)
     instset_l: Inst_sig.set_colv_inst;
     instset_r: Inst_sig.set_colv_inst;
     (* seq constraints to use in the instantiation *)
     instseq_l: Inst_sig.seq_colv_inst;
     instseq_r: Inst_sig.seq_colv_inst; }
