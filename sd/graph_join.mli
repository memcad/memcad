(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_join.mli
 **       Graph join
 ** Xavier Rival, 2011/07/26 *)
open Data_structures
open Offs
open Sv_def

open Graph_sig
open Nd_sig
open Inst_sig
open Set_sig
open Seq_sig
open Vd_sig

(** The main graph join function *)
(* note: we miss something to try to discharge proof obligations! *)
val join:
    submem:bool (* whether to compute is_le for a sub-mem (no alloc check) *)
  -> graph * join_arg (* left graph *)
    -> vsat_l:vsat (* left function to prove constraints satisfied *)
      -> graph * join_arg (* right input *)
        -> vsat_r:vsat (* right function to prove constraints satisfied *)
          -> hint_bg option (* optional hint *)
            -> lint_bg option (* optional nullable node address *)
            -> node_relation (* relation between both inputs *)
            -> node_relation  (* initial set var relation *)
            -> node_relation  (* initial seq var relation *)
                -> bool (* whether to NOT make roots prioretary *)
                  -> graph (* pre-computed, initial version of output *)
                    -> graph * node_relation
                        * (join_inst * (sv * sv) list)
                        * (join_inst * (sv * sv) list)
