(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_utils.mli
 **       utilities for the abstract domain
 ** Xavier Rival, 2011/10/17 *)
open Data_structures
open Sv_def

open Ast_sig
open Dom_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Svenv_sig
open Vd_sig

(** Basic pretty-printing *)
val join_kind_fpr: form -> join_kind -> unit
(* Domain tables *)
val bin_table_fpri: (form -> 'a -> unit)
  -> string -> form -> 'a bin_table -> unit

(** Map functions over gen_ind_call *)
val map_gen_ind_call: ('a -> 'b) -> ?g:(colvar -> int) -> ?h:(colvar -> int)
  -> 'a gen_ind_call -> 'b gen_ind_call

(** Emptiness test on gen_ind_call non list domain parameters *)
(* Check that parameters make a definition ok for the list domain *)
val gen_ind_call_list_dom_support: 'a gen_ind_call -> bool

(** Hints for domain operators *)
val extract_hint: hint_be option -> hint_ue option

(** Renaming operations *)
(* Pretty printing *)
val renaming_fpri: string -> form -> sv SvMap.t -> unit
(* Composition of renamings:
 *   - a renaming r is represented by a map M_r such that
 *     r(i) = M_r(i) if i \in M_r
 *     r(i) = i      otherwise
 *   - composition is defined as usual: (r0 o r1)(i) = r0(r1(i))
 *)
val renaming_compose: sv SvMap.t -> sv SvMap.t -> sv SvMap.t

(** Envmod structure *)
val svenv_empty: svenv_mod
val svenv_is_empty: svenv_mod -> bool
val svenv_fpri: string -> form -> svenv_mod -> unit
(* removal of an SV *)
val svenv_rem: sv -> svenv_mod -> svenv_mod
val svenv_add: sv -> ntyp -> svenv_mod -> svenv_mod
val svenv_join: svenv_mod -> svenv_mod -> svenv_mod
(* Mapping function used in dom_mem_low_prod/sep *)
val svenv_map: (sv -> sv) -> svenv_mod -> svenv_mod

(** Symbolic variable environment updater *)
val sv_upd_2set: sv_upd -> SvSet.t
val sv_upd_fpr: form -> sv_upd -> unit
val svenv_upd_empty: svenv_upd
val svenv_upd_identity: svenv_upd
val svenv_upd_embedding: unit Nd_sig.node_mapping -> svenv_upd

(*extending the joining graph with its encoding *)
val ext_graph: abs_graph option ->  abs_graph option -> join_ele
