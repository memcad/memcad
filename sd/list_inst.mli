(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_inst.mli
 **       Instantiation of set parameters for the list domain join
 ** Xavier Rival, 2015/07/16 *)
open Data_structures
open Sv_def

open Inst_sig
open List_sig
open Set_sig
open Seq_sig

(** Management of internal dimensions in the instantiation *)
(* Addition/removal of COLVs coming from inductive/segments
 *  (no constraints are added; these functions only add/remove the parameters *)
val jinst_set_addpars_icall: l_call -> set_colv_inst -> set_colv_inst
val jinst_set_rempars_icall: l_call -> set_colv_inst -> set_colv_inst
val jinst_seq_addpars_icall: l_call -> seq_colv_inst -> seq_colv_inst
val jinst_seq_rempars_icall: l_call -> seq_colv_inst -> seq_colv_inst

(** Constraints added to instantiation by introduction rule *)
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
val l_jinst_set_addcstr_segemp: l_call -> set_colv_inst -> set_colv_inst
val l_jinst_seq_addcstr_segemp: l_call -> seq_colv_inst -> seq_colv_inst

(** Integrating unresolved set constraints from is_le into inst *)
(* For sets based on the general function *)
val l_jinst_set_addctrs_post_isle:
    msg: string                  (* Location of the call *)
  -> (sv -> set_par_type option) (* Extraction of Set COLV types *)
    -> l_call                    (* Call used for the comparison *)
      -> l_call                  (* Call produced in the output *)
        -> sv SvMap.t            (* Is_le mapping for symbolic variables *)
          -> SvSet.t SvMap.t     (* Is_le mapping for set parameters *)
            -> set_cons list     (* Constraints to process *)
              -> set_colv_inst   (* Previous instantiation *)
                -> set_colv_inst (* Finished instantiation *)
(* For seqs *)
val l_jinst_seq_addctrs_post_isle:
    msg: string                  (* Location of the call *)
  -> (sv -> seq_par_type option) (* Extraction of Seq COLV types *)
    -> l_call                    (* Call used for the comparison *)
      -> l_call                  (* Call produced in the output *)
        -> sv SvMap.t            (* Is_le mapping for symbolic variables *)
          -> SvSet.t SvMap.t     (* Is_le mapping for seq parameters *)
            -> seq_cons list     (* Constraints to process *)
              -> seq_colv_inst   (* Previous instantiation *)
                -> seq_colv_inst (* Finished instantiation *)

(** Instantiation obtained from a pair of calls
 *   (maintains the set parameters associated to the call) *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
val jinst_set_addcstr_icall_eqs: l_call -> l_call
  -> set_colv_inst -> set_colv_inst
val jinst_seq_addcstr_icall_eqs: l_call -> l_call
  -> seq_colv_inst -> seq_colv_inst

(** List specific instantiation heuristics, for sets *)
(* Try to guess some instantiation constraints for const set parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
(* [XR]: check this instantiation scheme later *)
val jinst_set_guesscstr_segemp: sv -> l_call -> lmem -> set_colv_inst
  -> set_colv_inst
(* Apply l_const_inst exhaustively to compute as many instantiation constraints,
 * as post-analysis step *)
val jinst_set_guesscstr_segs_postjoin: lmem -> set_colv_inst -> set_colv_inst

(** List specific instantiation heuristics, for seqs *)
(* Try to guess some instantiation constraints for const seq parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
(* JG: This function should not be useful for my work....
 * It's left here for the sake of ... *)
val jinst_seq_guesscstr_segemp: sv -> l_call -> lmem -> seq_colv_inst
  -> seq_colv_inst
