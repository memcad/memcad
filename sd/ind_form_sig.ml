(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_form_sig.ml
 **       basic data-types for the inductive definitions.
 ** Xavier Rival, 2023/06/08 *)
open Ast_sig


(** Parameter types *)

type indpar_type =
  | It_ptr (* pointer: node not bound by the inductive *)
  | It_num (* numeric: node bound by the inductive; info in the under domain *)
  | It_set (* set:     special range of variable; bound in the under domain *)
  | It_seq (* seqnces: ordered sequence; nound in the under domain *)


(** Parameters in inductive calls *)

(* Occurrence of a variable in an inductive
 * we use various types, depending on the position of the occurence *)
type formal_arg =
  | Fa_this           (* main parameter *)
  | Fa_var_new of int (* new variable *)
  | Fa_par_int of int (* integer type parameter *)
  | Fa_par_ptr of int (* pointer type parameter *)
  | Fa_par_set of int (* set type parameter *)
  | Fa_par_seq of int (* sequence type parameter *)
type formal_main_arg =
  | Fa_this           (* main parameter *)
  | Fa_var_new of int (* new variable *)
type formal_aux_arg =
  | Fa_par_int of int (* integer type parameter *)
  | Fa_par_ptr of int (* pointer type parameter *)
  | Fa_par_set of int (* set type parameter *)
type formal_ptr_arg =
  | Fa_this           (* main parameter *)
  | Fa_var_new of int (* new variable *)
  | Fa_par_ptr of int (* pointer type parameter *)
type formal_int_arg =
  | Fa_var_new of int (* new variable *)
  | Fa_par_int of int (* integer type parameter *)
type formal_set_arg =
  | Fa_var_new of int (* new set variable *)
  | Fa_par_set of int (* set type parameter *)
type formal_seq_arg =
  | Fa_var_new of int (* new set variable *)
  | Fa_par_seq of int (* set type parameter *)
type formal_arith_arg = (* pointer or integer *)
  | Fa_this           (* main parameter *)
  | Fa_var_new of int (* new variable *)
  | Fa_par_int of int (* integer type parameter *)
  | Fa_par_ptr of int (* pointer type parameter *)

(* Arguments of a call *)
type formal_ptr_args = formal_ptr_arg list
type formal_int_args = formal_int_arg list
type formal_set_args = formal_set_arg list
type formal_seq_args = formal_seq_arg list

(* Inductive call *)
type indcall =
    { ii_maina: formal_main_arg;  (* main parameter *)
      ii_ind:   string;           (* name of called inductive *)
      ii_argp:  formal_ptr_args;  (* list of pointer arguments *)
      ii_argi:  formal_int_args;  (* list of integer arguments *)
      ii_args:  formal_set_args;  (* list of set arguments *)
      ii_argq:  formal_seq_args;  (* list of sequences arguments *) }


(** Pure formulas, expressions, etc *)

(* Set expression (very limited for now; to extend later) *)
type sexpr =
  | Se_var of formal_set_arg                (* a set repr. by a variable *)
  | Se_uplus of formal_set_arg list * formal_arith_arg (* X \uplus { y } *)
  | Se_union of formal_set_arg list * formal_arith_arg (* X \cup Y *)

(* Sequence expressions (should be improved in the future, for exemple
 * flatten the definition!) *)
type qexpr =
  | Qe_empty
  | Qe_var of formal_seq_arg
  | Qe_val of formal_arith_arg
  | Qe_concat of qexpr list
  | Qe_sort of qexpr

(* Arith expression *)
type aexpr =
  | Ae_cst of int
  | Ae_colvinfo of info_kind * formal_seq_arg
  | Ae_var of formal_arith_arg
  | Ae_plus of aexpr * aexpr

(* Set formula *)
type sform =
  | Sf_mem of formal_arith_arg * formal_set_arg  (* membership *)
  | Sf_equal of formal_set_arg * sexpr           (* equality *)
  | Sf_empty of formal_set_arg                   (* emptiness *)

(* Seq formula *)
type qform = (* Some usual predicates such as mem could be added as well *)
  | Qf_equal of formal_seq_arg * qexpr (* S = ... equality *)

(* Arith formula *)
type aform =
  | Af_equal of aexpr * aexpr
  | Af_noteq of aexpr * aexpr
  | Af_sup of aexpr * aexpr
  | Af_supeq of aexpr * aexpr


(** Elements of AST of inductive definitions and specifications *)

(* Operators *)
type s_bop =
  | Bconcat     (* sequence concatenation *)
  | Buplus      (* set union *)
  | Badd | Bsub (* integer arithmetics *)
