(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: inst_sig.ml
 **       data-type for the instantiation of set parameters
 ** Xavier Rival, Huisong Li, 2015/04/05 *)
open Data_structures
open Sv_def

open Ast_sig
open Col_sig
open Seq_sig
open Set_sig
open Nd_sig

(** SV parameters instantiation (after join) *)
type sv_inst =
    { (* Bookkeeping of SVs *)
      sv_fresh:   SvSet.t; (* fresh sv *)
      sv_ie:      SvSet.t; (* sv instantiated by equal expr *)
      (* Definition of SVs to be instantiated *)
      sv_eqs:   n_expr SvMap.t;        (* new sv => expr *)
      sv_low:   (n_expr list) SvMap.t; (* new sv => strict lower bound exprs *)
      sv_up:    (n_expr list) SvMap.t; (* new sv => strict upper bound exprs *)
      sv_eqlow: (n_expr list) SvMap.t; (* new sv => lower bound exprs *)
      sv_equp:  (n_expr list) SvMap.t; (* new sv => upper bound exprs *)
      (* constraints to prove *)
      sv_cons:  n_cons list; }

(** COLV parameters instantiation (after is_le) *)
(* type for intermediate result in post inclusion test instantiation *)
type ('a, 'b) is_le_colv_inst =
    (* in practice, cvi_def seems to only be used to map a COLV to an
     * expression that denotes just a variable *)
    { cvi_def:  'a SvMap.t; (* from right colv to left col_expr *)
      cvi_cons: 'b list;    (* constraint in the right part *)
      cvi_new:  SvSet.t (* New colv_added *) }
type is_le_setv_inst = (set_expr, set_cons) is_le_colv_inst
type is_le_seqv_inst = (seq_expr, seq_cons) is_le_colv_inst

(** COLV parameters instantiation (after join) *)
(* Synthesized colvs need to be resolved to something of the original graphs;
 * Fields managing COLVs that were added:
 *  - colvi_add:   added so far
 *  - colvi_rem:   were added and then removed
 * Fields managing the information about COLVs:
 *  - colvi_eqs:   exact resolution using an equality
 *  - colvi_guess: no exact resolution, we just know some members of the set
 *                 and will try to look among the roots if one satisfies this
 *                 (this is a bit arbitrary and could be improved)
 *  - colvi_none:  no constraint, and can be considered a free variable
 *                 so it could be instantiated to anything *)
type ('a, 'b) colv_inst =
    { (* Kind of collection variable being instantiated *)
      colvi_kind:  col_kind;
      (* Management of colvs to be instantiated *)
      colvi_add:   SvSet.t;
      colvi_rem:   SvSet.t;
      (* Definition of colvs to be instantiated *)
      colvi_eqs:   'a SvMap.t;       (* new colv => renamed expr *)
      colvi_guess: sv SvMap.t;       (* new colv => guessed element *)
      colvi_none:  SvSet.t;          (* new colv => could be anything *)
      (* Other definitions *)
      colvi_props: 'b list;     (* def. constraints that can be assumed *)
      (* Validation of the instantiation *)
      colvi_prove: 'b list;     (* constraints to prove *)
      (* fresh set variables introduced in the input set domain *)
      colvi_fresh: SvSet.t;
    }
type set_colv_inst = (set_expr,set_cons) colv_inst
type seq_colv_inst = (seq_expr,seq_cons) colv_inst
