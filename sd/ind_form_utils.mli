(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_form_utils.mli
 **       utilities for basic data-types for the inductive definitions.
 ** Xavier Rival, 2023/06/09 *)
open Data_structures

open Ind_form_sig


(** Conversions *)
val formal_arg_of_main: formal_main_arg -> formal_arg
val formal_arg_of_ptr: formal_ptr_arg -> formal_arg
val formal_arg_of_int: formal_int_arg -> formal_arg
val formal_arg_of_set: formal_set_arg -> formal_arg
val formal_arg_of_seq: formal_seq_arg -> formal_arg
val formal_arg_of_arith: formal_arith_arg -> formal_arg

(** Pretty-printing *)
(* Parameter types *)
val indpar_type_fpr: form -> indpar_type -> unit
(* Parameters *)
val formal_arg_fpr: form -> formal_arg -> unit
val formal_main_arg_fpr: form -> formal_main_arg -> unit
val formal_ptr_arg_fpr: form -> formal_ptr_arg -> unit
val formal_seq_arg_fpr: form -> formal_seq_arg -> unit
val formal_set_arg_fpr: form -> formal_set_arg -> unit
val formal_arith_arg_fpr: form -> formal_arith_arg -> unit
val indcall_fpr: form -> indcall -> unit
(* Pure formulas *)
val sexpr_fpr: form -> sexpr -> unit
val qexpr_fpr: form -> qexpr -> unit
val aexpr_fpr: form -> aexpr -> unit
val sform_fpr: form -> sform -> unit
val qform_fpr: form -> qform -> unit
val aform_fpr: form -> aform -> unit
(* Operators *)
val s_bop_fpr: form -> s_bop -> unit
