(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_algos.ml
 **       elements used for weakening algorithms on graphs
 ** Xavier Rival, 2012/04/10 *)
open Data_structures
open Flags
open Lib
open Sv_def

open Graph_sig
open Nd_sig
open Ind_sig
open Set_sig
open Seq_sig
open Vd_sig

open Col_utils
open Graph_utils
open Inst_utils

(** Partial inclusion tests into inductive and segment edges *)

(* Construction of a graph with only one inductive edge
 * followed by inclusion checking of the side into that segment:
 *  - pointer parameters instantiable to other nodes
 *  - integer parameters instantiable to expressions *)
let is_le_ind
    ~(submem: bool) (* whether sub-memory is_le (no alloc check) *)
    (ind: ind) (* inductive definition to use for weakening *)
    (grw: graph) (* graph to weaken *)
    (iso: sv) (* index of the node from which to weaken *)
    ~(vsat: vsat) (* function to prove constraints are satisfied *)
    : is_le_res * ind_edge (* * freshset *) =
  (* grw: graph to weaken *)
  let (ieo, grw, fs), nppars =
    let ppars, grw = ind_args_1_make Ntaddr ind.i_ppars grw in
    ind_edge_make ind.i_name ~srcptr:ppars grw, ppars in
  (* freshset: nodes added, that need assignment/instantiation *)
  let fs =
    let p = List.fold_left (fun a sv -> SvSet.add sv a) SvSet.empty nppars in
    { fs with f_ptr = p } in
  (* grt: graph-template, with just one inductive edge *)
  let grt =
    let grt = graph_empty grw.g_inds in
    let grt = sv_add iso Ntaddr Nnone grt in
    let grt = ind_edge_add iso ieo (freshset_add fs grt) in
    let grt = { grt with
                g_colvkey   = grw.g_colvkey;
                g_colvroots = grw.g_colvroots;
              } in
    grt in
  (* calling the weakening function *)
  let le_res =
    let inj = Aa_maps.singleton iso iso in
    Graph_is_le.is_le_partial ~fs:(Some fs) ~submem:submem false
      grw None SvSet.empty ~vsat_l:vsat grt inj Aa_maps.empty in
  le_res, ieo

(* Construction of a graph with only one segment edge
 * followed by inclusion checking of the right side into that segment *)
let is_le_seg
    ~(submem: bool) (* whether sub-memory is_le (no alloc check) *)
    (ind: ind) (* inductive definition to use for weakening *)
    (grw: graph) (* graph to weaken *)
    (isr: sv) (idr: sv) (* source and destination in the right side *)
    ~(vsat: vsat) (* function to prove constraints are satisfied *)
    : is_le_res * seg_edge * freshset =
  (* grw: graph to weaken *)
  let (seg, grw, fs), nppars =
    let lsrc, grw = ind_args_1_make Ntaddr ind.i_ppars grw in
    let ldst, grw = ind_args_1_make Ntaddr ind.i_ppars grw in
    seg_edge_make ind.i_name ~srcptr:lsrc ~dstptr:ldst idr grw, lsrc @ ldst in
  (* freshset: nodes added, that need assignment/instantiation *)
  let fs =
    let p = List.fold_left (fun a sv -> SvSet.add sv a) SvSet.empty nppars in
    { fs with f_ptr = p } in
  (* grt: graph-template, with just one segment edge *)
  let grt =
    let grt = sv_add isr Ntaddr Nnone (graph_empty grw.g_inds) in
    let grt = if isr = idr then grt else sv_add idr Ntaddr Nnone grt in
    seg_edge_add isr seg (freshset_add fs grt) in
  (* calling the weakening function *)
  let le_res =
    if isr = idr then
      Ilr_le_rem { ilr_graph_l  = grw;
                   ilr_svrem_l  = SvSet.empty;
                   ilr_svrtol   = SvMap.singleton isr isr;
                   ilr_colvrtol = SvMap.empty;
                   ilr_svinst   = sv_inst_empty;
                   ilr_setinst  = empty_inst;
                   ilr_setctr_r = [];
                   ilr_seqinst  = empty_inst;
                   ilr_seqctr_r = [ ] }
    else
      let inj = Aa_maps.add idr idr (Aa_maps.singleton isr isr) in
      Graph_is_le.is_le_partial ~fs:(Some fs) ~submem:submem false
        grw None (SvSet.singleton idr) ~vsat_l:vsat grt inj
        Aa_maps.empty in
    le_res, seg, fs
