(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: spec_sig.ml
 **       data-types for the analysis goal specifications.
 ** Xavier Rival, 2023/06/03 *)
open Data_structures
open Sv_def

open Ast_sig
open Ind_form_sig

open Ind_gen_functor
open Ind_inst


(** Specifications, after elaboration *)

(* Symbolic values used in formulas *)
type s_sval = string
(* Pure expressions *)
type s_pexpr =
  | Spe_empty (* empty collection: sequence or set *)
  | Spe_int of int
  | Spe_sval of s_sval
  | Spe_set of s_pexpr list (* finite set given by list of elements *)
  | Spe_seq of s_pexpr list (* finite seq given by list of elements *)
  | Spe_info of info_kind * s_sval
  | Spe_sort of s_pexpr
  | Spe_bop of s_bop * s_pexpr * s_pexpr
type s_comp =
  | S_ceq | S_cnoteq | S_cleq | S_clt (* Num specific*)
  | S_setmem | S_subseteq             (* Set specific *)
  | S_colequal                        (* Set+Seq specific *)
(* Pure formulas *)
type s_pterm =
  | Spt_cmp of s_comp * s_pexpr * s_pexpr
type s_pform = s_pterm list
(* Heap formulas *)
type s_henv = string StringMap.t
type s_arg = A_sval of s_sval | A_svar of string
type 'a s_iargs =
    { num: 'a list ;
      ptr: 'a list ;
      set: 'a list ;
      seq: 'a list }
type s_icall = s_sval * s_arg s_iargs
type s_off = FInd.s_off
type s_range = FInd.s_range
type s_hterm =
  | Sht_cell of s_sval * s_range * s_sval * s_range
  | Sht_ind of string * s_icall
  | Sht_seg of string * s_icall * s_icall
type s_hform = s_hterm list
type s_formula = s_henv * s_hform * s_pform
type s_params = (indpar_type * string) list
type s_col_params = (col_kind * string) list
(* procedure call in a goal:
 * either  ret = function_name( arguments )
 *          where result_binding ::= Some ret
 * or      function_name( arguments )
 *          where result_bindint ::= None *)
type proc_call =
    { result_binding: string option; (* optional return destination var *)
      function_name: string;
      arguments: string list; }
type old_svals = string list
type s_goal =
    s_col_params      (* universally quantified col parameters *)
      * s_formula     (* pre-condition *)
      * proc_call     (* call, defined above *)
      * s_formula     (* post-condition *)
      * old_svals     (* list of old-vars *)


(** Intermediate format, for parsing only *)

(* Instantiation of symbolic formulas for parsed files *)
module FPSpec =
  struct
    (* Offsets either numeric or symbolic; not sure in which context *)
    type symbolic_offset_item =
      | Field_name of string   (* symbolic offset *)
      | Array_subscript of int (* numeric array subscript; always const ??? *)
    (* ? *)
    type c_type_ref = string
    (* ? *)
    type symbolic_offset =
        { pos:            Lexing.position;
          c_type_ref_opt: c_type_ref option;
          items:          symbolic_offset_item list; }
    (* Main offset type *)
    type offset =
      | Numeric of Offs.t
      | Symbolic of symbolic_offset
    (** Utilities *)
    let offset_to_int_opt = function
      | Numeric o -> Offs.to_int_opt o
      | Symbolic s -> None
    (** Printers *)
    let symbolic_offset_item_fpr (fmt: form) (item: symbolic_offset_item): unit =
      match item with
      | Field_name name -> Format.fprintf fmt ".%s" name
      | Array_subscript i -> Format.fprintf fmt "[%d]" i
    let symbolic_offset_fpr (fmt: form) (off: symbolic_offset): unit =
      begin
        match off.c_type_ref_opt with
        | None -> ()
        | Some c_type_ref -> F.fprintf fmt "#%s" c_type_ref
      end;
      F.pp_print_list symbolic_offset_item_fpr fmt off.items
    let offset_fpr (fmt: form) (off: offset): unit =
      match off with
      | Numeric off -> Offs.t_fpr fmt off
      | Symbolic sym -> symbolic_offset_fpr fmt sym
    let offset_dest_fpr (fmt: form) (off: offset): unit =
      match off with
      | Numeric off ->
          if not (Offs.is_zero off) then
            F.fprintf fmt "->%a" Offs.t_fpr off
      | Symbolic sym -> F.fprintf fmt "->%a" symbolic_offset_fpr sym
  end
module FSpec = MakeSymForms( FPSpec )

(* Module of definitions used for the parsing of spec formulas, and for
 * representing their non-macro expanded forms:
 *  => All constructors of the form "symbol" are removed afterwards *)
module Parsed_spec =
  struct
    (* Instances of parameterized types *)
    type offset = FPSpec.offset

    (* Pure expressions *)
    type s_pexpr =
      | Spe_empty (* empty collection: sequence or set *)
      | Spe_int of int      (* integer value *)
      | Spe_sval of s_sval  (* symbolic value *)
      | Spe_old_sval of s_sval (* symbolic value *)
      | Spe_set of s_pexpr list (* finite set given by list of elements *)
      | Spe_seq of s_pexpr list (* finite seq given by list of elements *)
      | Spe_sort of s_pexpr
      | Spe_info of info_kind * s_sval
      | Spe_symbol of string * s_pexpr list  (* symbol with parameters *)
      | Spe_bop of s_bop * s_pexpr * s_pexpr (* binary *)
    (* Pure formulas *)
    type s_pterm =
      | Spt_symbol of string * s_pexpr list   (* symbol *)
      | Spt_cmp of s_comp * s_pexpr * s_pexpr
    type s_pform = s_pterm list
    (* Heap formulas: environment, heap terms, pure terms, etc *)
    type s_henv =
      | Se_emp
      | Se_symbols of string list
      | Se_map of string StringMap.t
    type s_off = FSpec.s_off
    type s_range = FSpec.s_range
    type s_hterm =
      | Sht_fsymbol of string * s_pexpr list (* symbol: env+heap+pure *)
      | Sht_hsymbol of string * s_pexpr list (* symbol: heap *)
      | Sht_cell of s_sval * s_range * s_sval * s_range
      | Sht_ind of string * (s_sval * s_sval s_iargs)
      | Sht_seg of string * (s_sval * s_sval s_iargs) * (s_sval * s_sval s_iargs)
    type s_hform = s_hterm list
    type s_formula =
      | Sf_extended of s_henv * s_hform * s_pform
    (* Goals *)
    type s_goal = s_col_params * s_formula * proc_call * s_formula

    (* Components of inductive data-types
     *  => the versions below are only used in the parser and then compiled
     *     to the regular form *)
    type cell      = FSpec.cell
    type heapatom  = FSpec.heapatom
    type hform     = FSpec.hform
    type pathstr   = FSpec.pathstr
    type apath     = FSpec.apath
    type pformatom = FSpec.pformatom
    type pform     = FSpec.pform

    (* inductive rule *)
    type irule =
        { num:  int;
          typ:  ntyp list;
          heap: hform;
          pure: pform; }
    (* inductive definitions *)
    type ind =
        { name:           string;
          ppars:          int;
          ipars:          int;
          spars:          int;
          qpars:          int;
          c_type_ref_opt: FPSpec.c_type_ref option;
          rules:          irule list; }

    type c_type_spec = FPSpec.symbolic_offset -> Offs.t
    type type_context =
        FPSpec.c_type_ref option -> (c_type_spec, [`Not_found]) result
  end

(* A line in a file may be either an inductive, a binding to a C type,
 * or some precedence information among inductive definitions *)
type p_iline =
  | Piind of Parsed_spec.ind                 (* inductive definition *)
  | Pibind of string * string    (* definition associated to a type *)
  | Piprec of string list        (* precedence information *)
  (* constant environment *)
  | Pienv  of string * Parsed_spec.s_henv
  (* (parameterized) basic (arith,collection) expression *)
  | Picst  of string * s_col_params * Parsed_spec.s_pexpr
  (* (parameterized) heap formula *)
  | Piheap of string * s_col_params * Parsed_spec.s_hform
  (* (parameterized) pure formula *)
  | Pipure of string * s_col_params * Parsed_spec.s_pform
  (* (parameterized) formula to be used in defs *)
  | Piform of string * s_params * Parsed_spec.s_formula
  (* proof goal, without any free variable *)
  | Pigoal of string * Parsed_spec.s_goal
