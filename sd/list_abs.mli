(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_abs.mli
 **       experimental partial unary abstraction algorithm
 ** Xavier Rival, 2014/04/03 *)
open Data_structures
open Sv_def

open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

(** Main weakening function; tries to apply weakening at each point *)
val local_abstract:
    stop: sv Aa_sets.t option (* SVs to stop at *)
  -> vsat: vsat               (* satisfiability test function *)
    -> lmem
      -> lmem
          * (sv SvMap.t)
          * Col_sig.colv_embedding_comp
          * (set_cons list)
          * (seq_cons list)
