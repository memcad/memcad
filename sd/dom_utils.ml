(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_utils.ml
 **       utilities for the abstract domain
 ** Xavier Rival, 2011/10/17 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Dom_sig
open Ind_sig
open Nd_sig
open Svenv_sig
open Graph_sig
open Vd_sig

open Sv_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "d_uts___" and level = Log_level.DEBUG end)


(** Basic pretty-printing *)
let join_kind_fpr (fmt: form): join_kind -> unit = function
  | Jjoin  -> F.fprintf fmt "join"
  | Jwiden -> F.fprintf fmt "widen"
  | Jdweak -> F.fprintf fmt "dweak"
(* Domain tables *)
let bin_table_fpri (fpr: form -> 'a -> unit) (ind: string) (fmt: form)
    (tbl: 'a bin_table): unit =
  Aa_maps.t_fpr "" "\n"
    (fun fmt (sv0, sv1) ->
      F.fprintf fmt "%s%a => %a" ind fpr sv0 fpr sv1
    ) fmt tbl


(** Map functions over gen_ind_call *)
let map_gen_ind_indp (f: 'a -> 'b) (ic: 'a gen_ind_intp): 'b gen_ind_intp =
  match ic with
  | Ii_const i -> Ii_const i
  | Ii_lval l -> Ii_lval (f l)
let map_gen_ind_pars (f: 'a -> 'b) (g: colvar -> int) (h: colvar -> int)
    (ic: 'a gen_ind_pars): 'b gen_ind_pars =
  { ic_ptr  = List.map f ic.ic_ptr ;
    ic_int  = List.map (map_gen_ind_indp f) ic.ic_int;
    ic_set  = List.map (fun x -> { x with colv_uid = g x }) ic.ic_set ;
    ic_seq  = List.map (fun x -> { x with colv_uid = h x }) ic.ic_seq }
let map_gen_ind_call (f: 'a -> 'b) ?(g: colvar -> int = fun x -> -1)
    ?(h: colvar -> int = fun x -> -1)
    (ic: 'a gen_ind_call): 'b gen_ind_call =
  { ic_name = ic.ic_name ;
    ic_pars = Option.map (map_gen_ind_pars f g h) ic.ic_pars }


(** Emptiness test on gen_ind_call non list domain parameters *)
(* Check that parameters make a definition ok for the list domain *)
let gen_ind_call_list_dom_support (ic: 'a gen_ind_call): bool =
  match ic.ic_pars with
  | None -> true
  | Some p -> p.ic_int = [ ]


(** Hints for domain operators *)
let extract_hint (ho: hint_be option): hint_ue option =
  Option.map (fun h -> { hue_live = h.hbe_live }) ho


(** Renaming operations *)
(* Pretty printing *)
let renaming_fpri (ind: string) (fmt: form): sv SvMap.t -> unit =
  SvMap.t_fpr ("\n"^ind) sv_fpr fmt
(* Composition of renamings:
 *   - a renaming r is represented by a map M_r such that
 *     r(i) = M_r(i) if i \in M_r
 *     r(i) = i      otherwise
 *   - composition is defined as usual: (r0 o r1)(i) = r0(r1(i))
 *)
let renaming_compose (r0: sv SvMap.t) (r1: sv SvMap.t): sv SvMap.t =
  let add i k r2 =
    if SvMap.mem i r2 then
      begin
        Log.info "combine_renaming: bad renaming:\n r0:\n%a r1:\n%a"
          (renaming_fpri "  ") r0 (renaming_fpri "  ") r1;
        Log.fatal_exn "combine_renaming: bad renaming"
      end
    else SvMap.add i k r2 in
  let r2 =
    SvMap.fold
      (fun i j r2 ->
        let k = try SvMap.find j r0 with Not_found -> j in
        SvMap.add i k r2
      ) r1 SvMap.empty in
  SvMap.fold add r0 r2


(** Envmod structure *)
let svenv_empty: svenv_mod =
  { svm_add = PMap.empty;
    svm_rem = PSet.empty;
    svm_mod = PSet.empty }
let svenv_is_empty (svm: svenv_mod): bool =
  svm.svm_add = PMap.empty
    && svm.svm_rem = PSet.empty
    && svm.svm_mod = PSet.empty
let svenv_fpri (ind: string) (fmt: form) (sv: svenv_mod): unit =
  let fmap = PMap.t_fpr "" ", " (fun fmt (s,_) -> sv_fpr fmt s) in
  let fset = PSet.t_fpr ", " sv_fpr in
  F.fprintf fmt "%s{ Add: %a\n%s  Rem: %a\n%s  Mod: %a }\n"
    ind fmap sv.svm_add ind fset sv.svm_rem ind fset sv.svm_mod
(* add and removal of an SV *)
let svenv_add (i: sv) (nt: ntyp) (sv: svenv_mod): svenv_mod =
  assert (not (PSet.mem i sv.svm_rem));
  assert (not (PMap.mem i sv.svm_add));
  assert (not (PSet.mem i sv.svm_mod));
  { sv with svm_add = PMap.add i nt sv.svm_add }
let svenv_rem (i: sv) (sv: svenv_mod): svenv_mod =
  assert (not (PMap.mem i sv.svm_add));
  assert (not (PSet.mem i sv.svm_mod));
  { sv with svm_rem = PSet.add i sv.svm_rem }
let svenv_join (sv0: svenv_mod) (sv1: svenv_mod): svenv_mod =
  let sanity_checks = false in (* turn on for debugging *)
  let f_check svc svo =
    PMap.iter
      (fun i _ ->
        if PSet.mem i svo.svm_rem || PSet.mem i svo.svm_mod then
          begin
            Log.info "Incompatible svenvs:\n1:\n%a\n2:\n%a\n"
              (svenv_fpri "  ") sv0 (svenv_fpri "  ") sv1;
            Log.fatal_exn "svenv_join"
          end;
      ) svc.svm_add in
  (* removed / modified in sv1 should not have been added in sv0 *)
  if sanity_checks then f_check sv1 sv0;
  (* added in sv1 should not have been removed / modified in sv1 *)
  if sanity_checks then f_check sv0 sv1;
  { svm_add = PMap.fold PMap.add sv0.svm_add sv1.svm_add;
    svm_rem = PSet.fold PSet.add sv0.svm_rem sv1.svm_rem;
    svm_mod = PSet.fold PSet.add sv0.svm_mod sv1.svm_mod; }
let svenv_map (f: sv -> sv) (sv: svenv_mod): svenv_mod =
  { svm_add = PMap.fold (fun i -> PMap.add (f i)) sv.svm_add PMap.empty;
    svm_rem = PSet.fold (fun i -> PSet.add (f i)) sv.svm_rem PSet.empty;
    svm_mod = PSet.fold (fun i -> PSet.add (f i)) sv.svm_mod PSet.empty; }

(* Symbolic variable environment updater *)
let sv_upd_2set: sv_upd -> SvSet.t = function
  | Svu_mod (i, s) -> SvSet.add i s
  | Svu_rem         -> SvSet.empty
let sv_upd_fpr (fmt: form) = function
  | Svu_mod (i, s) ->
      F.fprintf fmt "M: [ %a, { %a } ]" sv_fpr i (SvSet.t_fpr ",") s
  | Svu_rem        -> F.fprintf fmt "R"
let svenv_upd_empty: svenv_upd = fun _ -> raise Not_found
let svenv_upd_identity: svenv_upd = fun i -> Svu_mod (i, SvSet.empty)
let svenv_upd_embedding (nm: unit Nd_sig.node_mapping): svenv_upd =
  fun i ->
    try
      let j, s = SvMap.find i nm.Nd_sig.nm_map in
      Svu_mod (j, s)
    with Not_found ->
      if SvSet.mem i nm.Nd_sig.nm_rem then Svu_rem
      else raise Not_found

(*extending the joining graph with its encoding *)
let ext_graph (ei: abs_graph option) (eo: abs_graph option): join_ele =
  { abs_gi = ei;
    abs_go = eo; }
