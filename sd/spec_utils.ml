(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: spec_utils.ml
 **       utilities for analysis goal specifications.
 ** Xavier Rival, 2023/06/04 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Dom_sig
open Ind_form_sig
open Ind_sig
open Nd_sig
open Seq_sig
open Set_sig
open Spec_sig
open Vd_sig

open Apron
open Ind_inst

open Ast_utils
open Ind_form_utils
open Nd_utils
open Seq_utils
open Set_utils
open Sv_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "sp_ut___" and level = Log_level.DEBUG end)


(** Printers *)

let s_sval_fpr (fmt: form) (sv: s_sval): unit =
  F.fprintf fmt "%s" sv
let rec s_pexpr_fpr (fmt: form): s_pexpr -> unit = function
  | Spe_empty -> F.fprintf fmt "{}"
  | Spe_int i -> F.fprintf fmt "%d" i
  | Spe_sval sv -> s_sval_fpr fmt sv
  | Spe_set l -> F.fprintf fmt "{%a}" (gen_list_fpr "" s_pexpr_fpr ",") l
  | Spe_seq l -> F.fprintf fmt "[%a]" (gen_list_fpr "" s_pexpr_fpr ",") l
  | Spe_sort e -> F.fprintf fmt "sort(%a)" s_pexpr_fpr e
  | Spe_info (info, sv) -> F.fprintf fmt "%a( %s )" info_kind_fpr info sv
  | Spe_bop (o, e0, e1) ->
      F.fprintf fmt "(%a) %a (%a)" s_pexpr_fpr e0 s_bop_fpr o s_pexpr_fpr e1
let s_comp_fpr (fmt: form): s_comp -> unit = function
  | S_ceq -> F.fprintf fmt "="
  | S_cnoteq -> F.fprintf fmt "!="
  | S_cleq -> F.fprintf fmt "<="
  | S_clt -> F.fprintf fmt "<"
  | S_setmem -> F.fprintf fmt "#"
  | S_subseteq -> F.fprintf fmt "#="
  | S_colequal -> F.fprintf fmt "=="
let s_pterm_fpr (fmt: form): s_pterm -> unit = function
  | Spt_cmp (c, e0, e1) ->
      F.fprintf fmt "%a %a %a" s_pexpr_fpr e0 s_comp_fpr c s_pexpr_fpr e1
let s_pform_fpr (fmt: form): s_pform -> unit = function
  | [] -> ()
  | t :: f ->
      s_pterm_fpr fmt t;
      List.iter (F.fprintf fmt "\n/\\ %a" s_pterm_fpr) f
let s_range_fpr = FInd.s_range_fpr
let s_henv_fpr (fmt: form) (env: s_henv): unit =
  StringMap.t_fpr " * " (fun fmt -> F.fprintf fmt "%s") fmt env
let s_arg_fpr (fmt: form): s_arg -> unit = function
  | A_sval sv -> s_sval_fpr fmt sv
  | A_svar s -> string_fpr fmt s
let s_args_fpr (fmt: form) (args: s_arg list): unit =
  gen_list_fpr "" s_arg_fpr ", " fmt args
let s_iargs_fpr (type a) (f: form -> a -> unit) (fmt: form) (args: a s_iargs)
    : unit =
  let f: form -> (a list) -> unit = gen_list_fpr "" f ", " in
  F.fprintf fmt "%a|%a|%a|%a" f args.ptr f args.num f args.set f args.seq
let s_svals_fpr (fmt: form) (args: s_sval list): unit =
  gen_list_fpr "" s_sval_fpr ", " fmt args
let s_hterm_fpr (fmt: form): s_hterm -> unit = function
  | Sht_cell (sv0, r0, sv1, r1) ->
      F.fprintf fmt "%a%a |-> %a%a" s_sval_fpr sv0 s_range_fpr r0
        s_sval_fpr sv1 s_range_fpr r1
  | Sht_ind (i, (sv0, svl)) ->
      F.fprintf fmt "%a.%s( %a )"
        s_sval_fpr sv0 i (s_iargs_fpr s_arg_fpr) svl
  | Sht_seg (i, (sv0, svl0), (sv1, svl1)) ->
      F.fprintf fmt "%a.%s( %a ) *= %a.%s( %a )"
        s_sval_fpr sv0 i (s_iargs_fpr s_arg_fpr) svl0
        s_sval_fpr sv1 i (s_iargs_fpr s_arg_fpr) svl1
let s_hform_fpr (fmt: form): s_hform -> unit = function
  | [] -> ()
  | t :: f ->
      s_hterm_fpr fmt t;
      List.iter (F.fprintf fmt "\n * %a" s_hterm_fpr) f
let s_henv_is_emp (m: s_henv) = m = StringMap.empty
let s_formula_fpr (fmt: form) (henv, hf, pf) =
  match s_henv_is_emp henv, hf, pf with
  | true, [ ], [ ] -> ()
  | true, [ ], _ -> s_pform_fpr fmt pf
  | true, _, [ ] -> s_hform_fpr fmt hf
  | true, _, _ -> F.fprintf fmt "%a\n /\\ %a" s_hform_fpr hf s_pform_fpr pf
  | false, [ ], [ ] -> s_henv_fpr fmt henv
  | false, [ ], _ -> F.fprintf fmt "%a\n * %a" s_henv_fpr henv s_pform_fpr pf
  | false, _, [ ] -> F.fprintf fmt "%a\n * %a" s_henv_fpr henv s_hform_fpr hf
  | false, _, _ ->
      F.fprintf fmt "%a\n* %a\n/\\ %a" s_henv_fpr henv s_hform_fpr hf
        s_pform_fpr pf
let s_params_fpr (fmt: form) (l: s_params): unit =
  let fpr fmt (ck, v) = F.fprintf fmt "%a %s" indpar_type_fpr ck v in
  gen_list_fpr "" fpr " " fmt l
let s_col_params_fpr (fmt: form) (l: s_col_params): unit =
  let fpr fmt (ck, v) = F.fprintf fmt "%a %s" col_kind_fpr ck v in
  gen_list_fpr "" fpr " " fmt l
let proc_call_fpr (fmt: form) (pc: proc_call): unit =
  let result_binding_fpr fmt (result_binding: string option) =
    match result_binding with
    | None -> ()
    | Some x -> F.fprintf fmt "%s = " x in
  F.fprintf fmt "%a%s( %a )" result_binding_fpr pc.result_binding
    pc.function_name (gen_list_fpr "" string_fpr ",") pc.arguments
let s_goal_fpr (fmt: form) ((params, pre, proc_call, post, _old_svals): s_goal) =
  F.fprintf fmt  "[%a] { %a } %a { %a }" s_col_params_fpr params
    s_formula_fpr pre proc_call_fpr proc_call s_formula_fpr post

module Parsed_spec_fpr =
  struct
    open Parsed_spec
    let rec s_pexpr_fpr (fmt: form): s_pexpr -> unit = function
      | Spe_empty -> F.fprintf fmt "{}"
      | Spe_int i -> F.fprintf fmt "%d" i
      | Spe_sval sv -> s_sval_fpr fmt sv
      | Spe_old_sval sv -> F.fprintf fmt "%a@old" s_sval_fpr sv
      | Spe_set l -> F.fprintf fmt "{%a}" (gen_list_fpr "" s_pexpr_fpr ",") l
      | Spe_seq l -> F.fprintf fmt "[%a]" (gen_list_fpr "" s_pexpr_fpr ",") l
      | Spe_sort e -> F.fprintf fmt "sort(%a)" s_pexpr_fpr e
      | Spe_info (info, sv) -> F.fprintf fmt "%a( %s )" info_kind_fpr info sv
      | Spe_symbol (s, args) -> F.fprintf fmt "%s%a" s s_aargs_fpr args
      | Spe_bop (o, e0, e1) ->
          F.fprintf fmt "(%a) %a (%a)" s_pexpr_fpr e0 s_bop_fpr o s_pexpr_fpr e1
    and s_aargs_fpr (fmt: form) l =
      if l != [] then
        F.fprintf fmt "[%a]" (gen_list_fpr "" s_pexpr_fpr ", ") l
    let s_pterm_fpr (fmt: form): s_pterm -> unit = function
      | Spt_symbol (symb, l) ->
          F.fprintf fmt "$pure( %s )%a" symb s_aargs_fpr l
      | Spt_cmp (c, e0, e1) ->
          F.fprintf fmt "%a %a %a" s_pexpr_fpr e0 s_comp_fpr c s_pexpr_fpr e1
    let s_pform_fpr (fmt: form): s_pform -> unit = function
      | [] -> ()
      | t :: f ->
          s_pterm_fpr fmt t;
          List.iter (F.fprintf fmt "\n /\\ %a" s_pterm_fpr) f
    let s_henv_fpr (fmt: form): s_henv -> unit = function
      | Se_emp -> ()
      | Se_map env ->
          StringMap.t_fpr "\n* " (fun fmt -> F.fprintf fmt "%s") fmt env
      | Se_symbols l ->
         F.fprintf fmt "$env(%a)" (gen_list_fpr "" string_fpr ",") l
    let s_hterm_fpr (fmt: form): s_hterm -> unit = function
      | Sht_fsymbol (symb, l) ->
          F.fprintf fmt "$form( %s )%a" symb s_aargs_fpr l
      | Sht_hsymbol (symb, l) ->
          F.fprintf fmt "$heap( %s )%a" symb s_aargs_fpr l
      | Sht_cell (sv0, r0, sv1, r1) ->
          F.fprintf fmt "%a%a |-> %a%a" s_sval_fpr sv0 FSpec.s_range_fpr r0
            s_sval_fpr sv1 FSpec.s_range_fpr r1
      | Sht_ind (i, (sv0, svl)) ->
          F.fprintf fmt "%a.%s( %a )"
            s_sval_fpr sv0 i (s_iargs_fpr s_sval_fpr) svl
      | Sht_seg (i, (sv0, svl0), (sv1, svl1)) ->
          F.fprintf fmt "%a.%s( %a ) *= %a.%s( %a )"
            s_sval_fpr sv0 i (s_iargs_fpr s_sval_fpr) svl0
            s_sval_fpr sv1 i (s_iargs_fpr s_sval_fpr) svl1
    let s_hform_fpr (fmt: form): s_hform -> unit = function
      | [] -> ()
      | t :: f ->
          s_hterm_fpr fmt t;
          List.iter (F.fprintf fmt "\n * %a" s_hterm_fpr) f
    let s_henv_is_emp = function
      | Se_emp -> true
      | Se_map m -> m = StringMap.empty
      | Se_symbols l -> l = [ ]
    let s_formula_fpr (fmt: form) = function
      | Sf_extended (henv, hf, pf) ->
          match s_henv_is_emp henv, hf, pf with
          | true, [ ], [ ] ->
              ()
          | true, [ ], _ ->
              s_pform_fpr fmt pf
          | true, _, [ ] ->
              s_hform_fpr fmt hf
          | true, _, _ ->
              F.fprintf fmt "%a\n /\\ %a" s_hform_fpr hf s_pform_fpr pf
          | false, [ ], [ ] ->
              s_henv_fpr fmt henv
          | false, [ ], _ ->
              F.fprintf fmt "%a\n * %a" s_henv_fpr henv s_pform_fpr pf
          | false, _, [ ] ->
              F.fprintf fmt "%a\n * %a" s_henv_fpr henv s_hform_fpr hf
          | false, _, _ ->
              F.fprintf fmt "%a\n * %a\n /\\ %a" s_henv_fpr henv s_hform_fpr hf
                s_pform_fpr pf
    let hform_fpr = FSpec.hform_fpr
    let pform_fpr = FSpec.pform_fpr
    let irule_fpr (fmt: form) (r: irule): unit =
      let pp_typs =
        F.pp_print_list ~pp_sep:F.pp_print_space ntyp_fpr in
      F.fprintf fmt "| [%d%a]\n\t- %a\n\t- %a\n"
        r.num pp_typs r.typ hform_fpr r.heap pform_fpr r.pure
    let ind_fpr (fmt: form) (i: ind): unit =
      F.fprintf fmt "%s<%d,%d,%d,%d> :=\n%a\n"
        i.name i.ppars i.ipars i.spars i.qpars
        (gen_list_fpr "" irule_fpr "") i.rules
  end
let p_iline_fpr (fmt: form): p_iline -> unit = function
  | Piind ind -> Parsed_spec_fpr.ind_fpr fmt ind
  | Pibind (d, t) -> F.fprintf fmt "%s: %s.\n" d t
  | Piprec l ->
      F.fprintf fmt "prec: %a\n" (gen_list_fpr "" string_fpr ", ") l
  | Picst (s, l, e) ->
      F.fprintf fmt "$cst %s[%a] := %a\n"
        s s_col_params_fpr l Parsed_spec_fpr.s_pexpr_fpr e
  | Pienv (s, e) ->
      F.fprintf fmt "$env %s := \n  %a\n" s Parsed_spec_fpr.s_henv_fpr e
  | Piheap (s, l, e) ->
      F.fprintf fmt "$heap %s[%a] := \n  %a\n"
        s s_col_params_fpr l Parsed_spec_fpr.s_hform_fpr e
  | Pipure (s, l, e) ->
      F.fprintf fmt "$pure %s[%a] := \n  %a\n"
        s s_col_params_fpr l Parsed_spec_fpr.s_pform_fpr e
  | Piform (s, l, f) ->
      F.fprintf fmt "$form %s[%a] := \n  %a\n"
        s s_params_fpr l Parsed_spec_fpr.s_formula_fpr f
  | Pigoal (name, (params, pre, proc_call, post)) ->
      F.fprintf fmt "$goal %s [%a] {\n%a\n} %a {\n%a }"
        name
        s_col_params_fpr params
        Parsed_spec_fpr.s_formula_fpr pre
        proc_call_fpr proc_call
        Parsed_spec_fpr.s_formula_fpr post


(** Auxiliary function to construct abstract states *)

(* Construction of constraints *)
let build_meml_cons (dict: sv StringMap.t)
    (colv_env: (sv * col_kind * colv_info option) StringMap.t)
    (pt: s_pterm): meml_cons =
  (* Much of this code needs to be shared with above functions *)
  let get_sv (vname: s_sval): sv =
    try StringMap.find vname dict
    with Not_found -> Log.fatal_exn "get_sv: undeclared sym_variable %s"
        vname in
  let get_colv (ck: col_kind) (colv: s_sval): sv =
    try
      let colv, ck0, _ = StringMap.find colv colv_env in
      let () = if ck != ck0 then raise Not_found in
      colv
    with Not_found -> Log.fatal_exn "get_colv: undeclared colv variable %s"
        colv in
  let rec s_pexpr_to_num_expr: s_pexpr -> n_expr = function
    | Spe_int i -> Ne_csti i
    | Spe_sval sv -> Ne_var (get_sv sv)
    | Spe_bop (op, e1, e2) ->
        let e1 = s_pexpr_to_num_expr e1 in
        let e2 = s_pexpr_to_num_expr e2 in
        let op =
          match op with
          | Badd -> Texpr1.Add
          | Bsub -> Texpr1.Sub
          | _ ->
              Log.fatal_exn "s_pexpr_to_num_expr, unexpected operator %a"
                s_bop_fpr op in
        Ne_bin (op, e1, e2)
    | Spe_info (info, sv) ->
        begin
          match StringMap.find sv colv_env, info with
          | (_, _, None), _ ->
              Log.fatal_exn
                "s_pexpr_to_num_expr: %s has no information variable" sv
          | (_, _, Some { min; _ }), MIN -> Ne_var min
          | (_, _, Some { max; _ }), MAX -> Ne_var max
          | (_, _, Some { size; _ }), SIZE -> Ne_var size
        end
    | Spe_sort _ | Spe_set _ | Spe_seq _ | Spe_empty as e ->
        Log.fatal_exn "p_expr_to_num_expr unexpected expr: %a"
          s_pexpr_fpr e in
  let s_pexpr_to_node: s_pexpr -> set_expr = function
    | Spe_sval sv -> S_node (get_sv sv)
    | _ as e ->
        Log.fatal_exn "p_expr_to_node unexpected expr: %a"
          s_pexpr_fpr e in
  let rec s_pexpr_to_set_expr: s_pexpr -> set_expr = function
    | Spe_empty -> S_empty
    | Spe_sval colv -> S_var (get_colv CK_set colv)
    | Spe_bop (Buplus, e1, e2) ->
        let e1 = s_pexpr_to_set_expr e1 in
        let e2 = s_pexpr_to_set_expr e2 in
        S_uplus (e1, e2)
    | Spe_set (v :: values) ->
        List.fold_left (fun acc v -> S_uplus (acc, s_pexpr_to_node v))
          (s_pexpr_to_node v) values
    | _ -> Log.todo_exn "s_pexpr_to_set_expr %s" __LOC__ in
  let s_pexpr_to_atom: s_pexpr -> seq_expr = function
    | Spe_sval sv -> Seq_val (get_sv sv)
    | _ as e ->
        Log.fatal_exn "s_pexpr_to_seq_expr: unexpected atom %a" s_pexpr_fpr e in
  let rec s_pexpr_to_seq_expr: s_pexpr -> seq_expr = function
    | Spe_empty -> Seq_empty
    | Spe_sval colv -> Seq_var (get_colv CK_seq colv)
    | Spe_seq [] -> Seq_empty
    | Spe_seq l -> Seq_Concat (List.map s_pexpr_to_atom l)
    | Spe_bop (Bconcat, e1, e2) ->
        let e1 = s_pexpr_to_seq_expr e1 in
        let e2 = s_pexpr_to_seq_expr e2 in
        Seq_Concat [e1; e2]
    | Spe_sort (e) ->
        let e = s_pexpr_to_seq_expr e in
        Seq_sort e
    | Spe_bop (_, _, _) | Spe_set _ | Spe_int _ | Spe_info (_, _) as e ->
        Log.fatal_exn "s_pexpr_to_seq_expr: unexpected expr %a" s_pexpr_fpr e in
  let is_setvar (colvname: string): bool =
    match StringMap.find_opt colvname colv_env with
    | Some (_, CK_set, _) -> true
    | None | Some (_, CK_seq, _) -> false in
  let is_seqvar (colvname: string): bool =
    match StringMap.find_opt colvname colv_env with
    | Some (_, CK_seq, _) -> true
    | None | Some (_, CK_set, _) -> false in
  match pt with
  | Spt_cmp ((S_cnoteq| S_cleq | S_clt | S_ceq) as op, e1, e2) as f ->
      let e1 = s_pexpr_to_num_expr e1 in
      let e2 = s_pexpr_to_num_expr e2 in
      let cons =
        match op with
        | S_cnoteq -> Nc_cons (Tcons1.DISEQ, e1, e2)
        | S_cleq -> Nc_cons (Tcons1.SUPEQ, e2, e1)
        | S_clt -> Nc_cons (Tcons1.SUP, e2, e1)
        | S_ceq -> Nc_cons (Tcons1.EQ, e1, e2)
        | _ -> assert false (* filtered above *) in
      if !Flags.flag_debug_spec then
        Log.debug "guard_pterm: %a\n  => %a" s_pterm_fpr f n_cons_fpr cons;
      Mc_num cons
  | Spt_cmp (S_setmem, Spe_sval sv, e2) as f ->
      let sv = get_sv sv in
      let e2 = s_pexpr_to_set_expr e2 in
      let cons = S_mem (sv, e2) in
      if !Flags.flag_debug_spec then
        Log.debug "guard_pterm %a\n  => %a" s_pterm_fpr f set_cons_fpr cons;
      Mc_set cons
  | Spt_cmp (S_colequal, Spe_sval colv, e) as f when is_seqvar colv ->
      (* Pattern to detect sequence constraints.
       * Other constraints using == comparison are set constraints *)
      let colv = get_colv CK_seq colv in
      let e = s_pexpr_to_seq_expr e in
      let cons = Seq_equal (Seq_var colv, e) in
      if !Flags.flag_debug_spec then
        Log.debug "guard_pterm %a\n  => %a" s_pterm_fpr f seq_cons_fpr cons;
      Mc_seq cons
  | Spt_cmp (S_colequal, Spe_sval colv, e) as f when is_setvar colv ->
      let colv = get_colv CK_set colv in
      let e = s_pexpr_to_set_expr e in
      let cons = S_eq (S_var colv, e) in
      if !Flags.flag_debug_spec then
        Log.debug "guard_pterm %a\n  => %a" s_pterm_fpr f set_cons_fpr cons;
      Mc_set cons
  | pt ->
      Log.todo_exn "sat_p_formula: other case (non num) %a" s_pterm_fpr pt


(** Elaboration *)
(* Functions to translate parsed fragments of AST into regular inductive
 * definitions *)

let elaborate_offset (c_type_spec: Parsed_spec.c_type_spec)
    (offset: Parsed_spec.offset): Offs.t =
  match offset with
  | FPSpec.Numeric off -> off
  | FPSpec.Symbolic symbolic -> c_type_spec symbolic
let elaborate_cell (c_type_spec: Parsed_spec.c_type_spec)
    (cell: Parsed_spec.cell): Ind_sig.cell =
  { ic_off  = elaborate_offset c_type_spec cell.ic_off;
    ic_size = cell.ic_size;
    ic_dest = cell.ic_dest;
    ic_doff = elaborate_offset c_type_spec cell.ic_doff; }
let elaborate_heapatom (c_type_spec: Parsed_spec.c_type_spec)
    (heapatom: Parsed_spec.heapatom): Ind_sig.heapatom =
  match heapatom with
  | Hacell cell -> Hacell (elaborate_cell c_type_spec cell)
  | Haind indcall -> Haind indcall
  | Haseg (a, b) -> Haseg (a, b)
let elaborate_hform (c_type_spec: Parsed_spec.c_type_spec)
    (hform: Parsed_spec.hform): Ind_sig.hform =
  List.map (elaborate_heapatom c_type_spec) hform
let elaborate_pathstr (c_type_spec: Parsed_spec.c_type_spec)
    (pathstr: Parsed_spec.pathstr): Ind_sig.pathstr =
  match pathstr with
  | Pe_epsilon -> Pe_epsilon
  | Pe_seg l -> Pe_seg (List.map (elaborate_offset c_type_spec) l)
  | Pe_single l -> Pe_single (List.map (elaborate_offset c_type_spec) l)
let elaborate_apath (c_type_spec: Parsed_spec.c_type_spec)
    (path: Parsed_spec.apath): Ind_sig.apath =
  let (a, p, b) = path in
  (a, elaborate_pathstr c_type_spec p, b)
let elaborate_pformatom (c_type_spec: Parsed_spec.c_type_spec)
    (atom: Parsed_spec.pformatom): Ind_sig.pformatom =
  match atom with
  | Pf_alloc i -> Pf_alloc i
  | Pf_set s -> Pf_set s
  | Pf_seq q -> Pf_seq q
  | Pf_arith a -> Pf_arith a
  | Pf_path p -> Pf_path (elaborate_apath c_type_spec p)
