(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: array_ppred_utils.mli
 **       signatures of a simplified inductive domain based on lists
 **       (no parameters, no arbitrary inductives)
 ** Jiangchao Liu, 2016/03/01 *)
open Data_structures

open Array_ppred_sig

(** Pretty printing *)
val prtype_fpr: form -> prule_type -> unit
val prule_fpr: form -> prule -> unit
val pdef_fpri: string -> form -> p_def -> unit
val pmem_fpri: string -> form -> pmem -> unit

(** Unfolding *)
val unfold: pmem -> pm_unfold_result list

(** Find pure predicates defs from users' specifications *)
val search_ppred: unit -> unit
