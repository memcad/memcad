(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2022 INRIA
 **
 ** File: graph_inst.ml
 **       Instantiation of set parameters for the graph based shape domain join
 ** Xavier Rival, 2022/04/23 *)
open Data_structures
open Lib
open Sv_def

open Graph_sig
open Ind_sig
open Inst_sig
open Nd_sig
open Seq_sig
open Set_sig
open Vd_sig

open Graph_utils
open Inst_utils
open Nd_utils
open Seq_utils
open Set_utils
open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "g_ins___" and level = Log_level.DEBUG end)

(** Utilities *)
(* Extracting a list of set parameter types out of an inductive definition *)
(* => might be moved to ind_utils at some point *)
let extract_set_arg_kinds (ind: ind): set_par_type list =
  let l, _ =
    IntMap.fold (fun i t (a, ri) -> assert (i = ri); t :: a, ri + 1)
      ind.i_pkind.pr_set ([], 0) in
  List.rev l

(** Functions specific to SV instantiation (integers) *)
(* type fresh sv in instantiation according to out graph *)
let int_par_type_sv_wk (g: graph) (rel: node_relation) (sv_instl: sv_inst)
    (sv_instr: sv_inst)
    : int_par_rec SvMap.t * int_par_rec SvMap.t =
  let fst_rel id = fst (Nrel.find_p rel id) in
  let snd_rel id = snd (Nrel.find_p rel id) in
  let g_pars_wktyp_l = int_par_type_iargs_g g fst_rel in
  let g_pars_wktyp_r = int_par_type_iargs_g g snd_rel in
  let to_out_wkty (iwk: int_par_rec SvMap.t) (f_rel: sv -> sv) (sv_inst: sv_inst)
      : int_par_rec SvMap.t =
    SvMap.fold
      (fun id wk acc ->
        let al = f_rel id in
        if SvSet.mem al sv_inst.sv_fresh then
          try SvMap.add al (merge_int_par_type (SvMap.find al acc) wk) acc
          with Not_found -> SvMap.add al wk acc
        else acc
      ) iwk SvMap.empty in
  to_out_wkty g_pars_wktyp_l fst_rel sv_instl,
  to_out_wkty g_pars_wktyp_r snd_rel sv_instr
let g_jinst_sv_calls
    (rels, ies_ia_ptr)
    (relo, ieo_ia_ptr, nro) f_get_side jrel =
  (* computation of pointer parameters:
   *    - assumes mappings found in the right hand side
   *    - no mapping in the left hand side (no edge)
   * => find a candidate node mapped to the right parameter *)
  let lrev =
    List.fold_left2
      (fun accp pl0 pr0 ->
        if !Flags.flag_dbg_join_shape then
          Log.force "parameters: %a - %a" sv_fpr pl0 sv_fpr pr0;
        let pr1 =
          try SvMap.find pr0 relo
          with Not_found -> Log.fatal_exn "ind-intro ppars, no R-map" in
        let pl1 =
          try SvMap.find pl0 rels
          with Not_found ->
            let set0 =
              try SvMap.find pr1 nro
              with Not_found -> Log.fatal_exn "ind-intro ppars, no L-set" in
            if !Flags.flag_dbg_join_shape then
              Log.force "only right mapping %a: %d elts"
                sv_fpr pr1 (SvSet.cardinal set0);
            if SvSet.cardinal set0 = 0 then
              Log.fatal_exn"ind-intro ppars, empty L-set"
            else if SvSet.cardinal set0 > 1 then
              Log.fatal_exn"ind-intro ppars, too large L-set"
            else SvSet.min_elt set0  in
        let p =
          try Nrel.find jrel (f_get_side (pr1, pl1))
          with Not_found -> Log.fatal_exn "ind-intro, ppar not found" in
        p :: accp
      ) [ ] ies_ia_ptr ieo_ia_ptr in
  List.rev lrev

(** Constraints added to instantiation by introduction rule *)
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
let g_jinst_set_addcstr_segemp (seg: seg_edge) (inst: set_colv_inst) =
  let argst = extract_set_arg_kinds seg.se_ind in
  gen_jinst_set_addcstr_segemp seg.se_eargs.ias_set argst inst

(** Integrating unresolved set constraints from is_le into inst *)
(* For sets based on the general function *)
let g_jinst_set_addctrs_post_isle
    ~(msg: string)              (* location of the call, for logging *)
    ~(findkind: sv -> set_par_type option) (* Extraction of Set COLV types *)
    ~(sarg_template: sv list)   (* arguments of the is_le template *)
    ~(sarg_out: sv list)        (* arguments of the output *)
    ~(svmap:   sv SvMap.t)      (* Is_le mapping for symbolic variables *)
    ~(setvmap: SvSet.t SvMap.t) (* Is_le mapping for set parameters *)
    ~(sc: set_cons list)       (* Constraints: unrenamed, undischarged, right *)
    ~(inst: set_colv_inst)     (* Previous instantiation *)
    : set_colv_inst =
  let loc_debug = false in
  if loc_debug then
    Log.debug "tmp,g_jinst_set_addctrs_post_isle(%s): args:%d\n%aCtrs:\n  %a"
      msg (List.length sarg_out) (set_colv_inst_fpri "    ") inst
      (gen_list_fpr "" set_cons_fpr ", ") sc;
  (* Declare new collection variables into inst *)
  let inst =
    List.fold_left (fun acc i -> colv_inst_add i acc)
      inst sarg_out in
  (* Map set args from isle template to join output *)
  let call_vars: sv SvMap.t =
    List.fold_left2 (fun acc iw io -> SvMap.add iw io acc)
      SvMap.empty sarg_template sarg_out in
  (* Launch inst function and compare to previous version *)
  let nsinsto =
    Inst_utils.gen_jinst_set_addctrs_post_isle findkind call_vars
      svmap setvmap sc inst in
  let e, r = set_colv_inst_compare nsinsto inst in
  if loc_debug then
    Log.debug "tmp,g_jinst_set_addctrs_post_isle(%s): %b,%b (%s)" msg e
      (nsinsto = inst) r;
  nsinsto


(** Generic functions for instantiation in join *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
let jinst_set_addcstr_icall_eqs (lc_i: ind_args) (lc_o: ind_args)
    (inst: set_colv_inst): set_colv_inst =
  Inst_utils.gen_jinst_set_addcstr_icall_eqs lc_i.ia_set lc_o.ia_set inst
let jinst_seq_addcstr_icall_eqs (lc_i: ind_args) (lc_o: ind_args)
    (inst: seq_colv_inst): seq_colv_inst =
  Inst_utils.gen_jinst_seq_addcstr_icall_eqs lc_i.ia_seq lc_o.ia_seq inst
let jinst_seq_addcstr_scall_eqs (lc_i: ind_args_seg) (lc_o: ind_args_seg)
    (inst: seq_colv_inst): seq_colv_inst =
  let lc_i_1 = List.map fst lc_i.ias_seq in
  let lc_o_1 = List.map fst lc_o.ias_seq in
  let lc_i_2 = List.map snd lc_i.ias_seq in
  let lc_o_2 = List.map snd lc_o.ias_seq in
  inst
  |> Inst_utils.gen_jinst_seq_addcstr_icall_eqs lc_i_1 lc_o_1
  |> Inst_utils.gen_jinst_seq_addcstr_icall_eqs lc_i_2 lc_o_2


(** List specific instantiation heuristics, for sets *)
(* Try to guess some instantiation constraints for const set parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
let jinst_set_guesscstr_segemp (is: sv) (*(ind: ind) (ia: ind_args)*)
    (ie: ind_edge) (g: graph)
    (inst: set_colv_inst): set_colv_inst =
  let loc_debug_tmp = false in
  let argst = extract_set_arg_kinds ie.ie_ind in
  try
    let extaset =
      (* - extract the node at location id
       * - check if parameters can be "guessed" from that node *)
      let nd = node_find is g in
      match nd.n_e with
      | Hemp ->
          if loc_debug_tmp then
            Log.debug "tmp,jinst_set_guesscstr_segemp,extaset,emp";
          raise Not_found
      | Hpt _ ->
          if loc_debug_tmp then
            Log.debug "tmp,jinst_set_guesscstr_segemp,extaset,pt";
          raise Not_found
      | Hind _ ->
          Log.todo_exn "jinst_set_guesscstr_segemp,extaset,ind"
      | Hseg _ ->
          Log.todo_exn "jinst_set_guesscstr_segemp,extaset,seg" in
    Inst_utils.gen_jinst_set_guesscstr_segemp ie.ie_args.ia_set
      extaset argst inst
  with Not_found -> inst




(** Functions below all need to be checked/maybe merged with oters *)

(* ===> Internal function used only in this file! *)
(* Instantiated fresh set variables from set constraints *)
let rec instantiate_cons
    (inst: set_expr SvMap.t) (* candidate instantiation of colvs *)
    (setctr: set_cons list)  (* a series of already accumulated constraints *)
    (fresh: SvSet.t)         (* colvs that are still not done *)
    : set_expr SvMap.t       (* augmented instantiation of colvs *)
    * set_cons list          (* remaining fresh set constraints *)
    * SvSet.t                (* remaining fresh COLVs *) =
  (* instantiate a set variable accoring to equal set expressions *)
  let inst_var (sl: set_expr) (sr: set_expr) (setvs: SvSet.t)
      : (sv * set_expr) option =
    let sl_uplus = linear_svars sl true in
    let sr_uplus = linear_svars sr true in
    let sl_union = linear_svars sl false in
    let sr_union = linear_svars sr false in
    let f_inst b ssl ssr nsl nsr =
      let ssl_f = SvSet.inter ssl setvs in
      let ssr_f = SvSet.inter ssr setvs in
      let f_inst setvl setvr svl svr v =
        let v_s, v_n = SvSet.diff setvl setvr, SvSet.diff svl svr in
        Some (v, make_setexp ~is_disjoin:b ~setvs:v_s ~svs:v_n ()) in
      if ssl_f = SvSet.empty && SvSet.cardinal ssr_f = 1 then
        let v = SvSet.min_elt ssr_f in
        f_inst ssl ssr nsl nsr v
      else if ssr_f = SvSet.empty && SvSet.cardinal ssl_f = 1 then
        let v = SvSet.min_elt ssl_f in
        f_inst ssr ssl nsr nsl v
      else None in
    match sl_uplus, sr_uplus, sl_union, sr_union with
    | Some (ssl, nsl),  Some (ssr, nsr), _, _ ->
        f_inst true ssl ssr nsl nsr
    | _, _, Some (ssl, nsl),  Some (ssr, nsr) ->
        f_inst false ssl ssr nsl nsr
    | _ -> None in
  (* walk through the set constraints, and try to resolve them
   *  - inst grows with additional mappings
   *  - r_setctr shrinks as constraints get processed
   *  - r_fresh shrinks as variables get instantiated *)
  let inst, r_setctr, r_fresh =
    List.fold_left
      (fun (inst, ctrs, s) constr ->
        match constr with
        | S_mem _ -> inst, constr :: ctrs, s
        | S_subset _ -> inst, constr :: ctrs, s
        | S_eq (sl, sr) ->
            begin
              let ins = inst_var sl sr fresh in
              match ins with
              | Some (i, e) -> SvMap.add i e inst, ctrs, SvSet.remove i s
              | None -> inst, constr :: ctrs, s
            end
      ) (inst, [ ], fresh) setctr in
  (* do some kind of saturation until some kind of fixpoint is reached,
   * by removal of constraints that are turned into association mappings *)
  if List.length r_setctr == List.length setctr then
    inst, setctr, fresh
  else instantiate_cons inst r_setctr r_fresh

(* ===> at the very end of the join computation, in graph_join.ml *)
(* instantiate fresh set variables according to guessed equal relationship *)
let instantiate_eq
    (inst: set_colv_inst) (* existing instantiation information *)
    (eq: sv SvMap.t)      (* gussesed equality relation on out set parameters *)
    (smap: set_expr SvMap.t) (* gussed weaken from out set parameters to
                              * input set expressions *)
    : set_colv_inst       (* new instantiation *) =
  let loc_debug_tmp = false in
  if loc_debug_tmp then
    begin
      Log.debug "tmp,inst_eq,eq:\n%a" (setvmap_fpr ",  " setv_fpr) eq;
      Log.debug "tmp,inst_eq,s:\n%a" (setvmap_fpr ",  " set_expr_fpr) smap;
      Log.debug "tmp,inst_eq,inst:\n%a" (set_colv_inst_fpri "  ") inst;
    end;
  let fresh = inst.colvi_fresh in
  if loc_debug_tmp then Log.debug "tmp,inst_eq,fresh: %a" setvset_fpr fresh;
  (* partition instantiation equalities Si => expr into:
   *  - ninst: still need instantiation as expr contains non-instantiated svs
   *  - ainst: already fully instantiated *)
  let ninst, ainst =
    SvMap.fold
      (fun key se (accn, acca) ->
        if loc_debug_tmp then Log.debug "tmp,inst_eq,fold_eqs: %a" setv_fpr key;
        let svars = Set_utils.set_expr_setvs se in
        let fresh_svars = SvSet.inter svars fresh in
        if SvSet.is_empty fresh_svars then
          accn, SvMap.add key se acca
        else
          SvMap.add key se accn, acca
      ) inst.colvi_eqs (SvMap.empty, SvMap.empty) in
  let ffpr = setvmap_fpr ",  " set_expr_fpr in
  if loc_debug_tmp then Log.debug "tmp,inst_eq,ainst: %a" ffpr ainst;
  if loc_debug_tmp then Log.debug "tmp,inst_eq,ninst: %a" ffpr ninst;
  (* creating a list of constraints based on known equalities
   *  i.e., when either member was instantiated before and the other not *)
  let setctr =
    SvMap.fold
      (fun key i acc ->
        if loc_debug_tmp then
          Log.debug "tmp,inst_eq,fold_setctr: %a:%a"
            setv_fpr key setv_fpr i;
        try
          match SvMap.mem key ainst, SvMap.mem i ainst with
          | true, true  -> acc
          | true, false ->
              if loc_debug_tmp then
                Log.debug "tmp,inst_eq,set: k:%a, i:%a" setv_fpr key setv_fpr i;
              (S_eq (SvMap.find key ainst, SvMap.find i ninst)) :: acc
          | false, true ->
              (S_eq (SvMap.find key ninst, SvMap.find i ainst)) :: acc
          | false, false ->
              (S_eq (SvMap.find key ninst, SvMap.find i ninst)) :: acc
        with exn ->
          if loc_debug_tmp then
            Log.debug "tmp,inst_eq,exception: %s" (Printexc.to_string exn);
          acc
      ) eq [] in
  (* strengthen set constraints with additional set equalities *)
  let setctr =
    SvMap.fold
      (fun k se acc ->
        (S_eq (SvMap.find k inst.colvi_eqs, se)) :: acc
      ) smap setctr in
  (* tryes to realize a series of mappings based on constraints in setctr *)
  let f_inst, setctr, f_fresh = instantiate_cons SvMap.empty setctr fresh in
  if loc_debug_tmp then
    Log.debug "tmp,inst_eq,added fresh,0: %d elts {%a}" (SvSet.cardinal f_fresh)
      setvset_fpr f_fresh;
  let eqs =
    let do_sid sid = try SvMap.find sid f_inst with Not_found -> S_var sid in
    let rename_expr = Set_utils.transform_set_expr do_sid in
    SvMap.map rename_expr inst.colvi_eqs in
  let inst = { inst with
               colvi_fresh = f_fresh;
               colvi_eqs   = eqs; } in
  if loc_debug_tmp then
    Log.debug "tmp,inst_eq,output:\n%a\n" (set_colv_inst_fpri "  ") inst;
  inst

(* ===> used right before the regular function to add constraints on empty
 *      segments
 * ===> maybe merge the underlying into this one
 * ===> maybe the work on set domain is redundant
 *)
(* deal with set parameters for introducing empty segment *)
let inst_void_seg (seg: seg_edge) (inst: join_inst) : join_inst =
  (* TODO: seems like we should apply gen_jinst_set_addcstr_segemp *)
  (* TODO: this code is not using existing primitives; see why... *)
  hseg_sanity_check ~isok:2 "graph_inst,inst_void_seg,l" seg;
  let set_inst, _ =
    List.fold_left
      (fun (inst, i) setv ->
        let pk = IntMap.find i seg.se_ind.i_pkind.pr_set in
        (* parameters setv is marked fresh *)
        (*Log.debug "FRESH: %a" Set_utils.setv_fpr setv;
        let colvi_fresh = SvSet.add setv inst.colvi_fresh in
        let inst = { inst with colvi_fresh } in*)
        if pk.st_add then (* additive parameter, set to zero *)
	  (*colv_inst_over_bind setv S_empty inst, i + 1*)
	  let colvi_eqs = SvMap.add setv S_empty inst.colvi_eqs in
          { inst with colvi_eqs }, i + 1
        else (* non-additive parameter, no further setting to do *)
          inst, i + 1
      ) (inst.set_inst, 0) seg.se_eargs.ias_set in
  let seq_inst =
    let f = List.map_flatten (fun (x,y) ->  [x; y]) in
    List.fold_left
      (fun inst seqv ->
        (* Marking the new seqv as "added".
         * If we move to expinst permanantly use function such as
         * gen_jinst_set_addcstr_segemp *)
        let colvi_add = SvSet.add seqv inst.colvi_add in
        let colvi_fresh = SvSet.add seqv inst.colvi_fresh in
        let colvi_eqs = SvMap.add seqv Seq_empty inst.colvi_eqs in
        { inst with colvi_add; colvi_fresh; colvi_eqs }
      ) inst.seq_inst (f seg.se_eargs.ias_seq) in (* to check  *)
  { inst with set_inst; seq_inst}

(* ===> used in graph_join:
 *      -- in both sides for ind_ind
 *      -- in one side for gen_ind_weak
 *)
(* generate equality from set parameters of output inductive edge to
 * set parameters of input inductive edge *)
let l_call_inst
    ?(skipset: bool = false)
    (ls_i: ind_args) (* pars from the join input *)
    (g: graph)    (* join input graph *)
    (ls_o: ind_args) (* pars from the join output being constructed *)
    (inst: join_inst) (* instantiation being constructed *)
    ~(pr: pars_rec)
    : graph * join_inst * sv list =
  let f accg accs =
    let ia, accg = sv_add_fresh Ntint Nnone accg in
    accg, ia, { accs with sv_fresh = SvSet.add ia accs.sv_fresh } in
  let _, g, rev_args, sv_inst =
    List.fold_left
      (fun (i, accg, acca, accs) ele ->
        let ipt = IntMap.find i pr.pr_int in
        (* TODO: the following cases need to be checked *)
        if ipt.ipt_const || ipt.ipt_add then
          i+1, accg, ele :: acca, accs
        else if ipt.ipt_incr then
          let accg, ia, accs =  f accg accs in
          let cons = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var ele, Ne_var ia) in
          i+1, accg, ia :: acca, { accs with sv_cons = cons :: accs.sv_cons }
        else if ipt.ipt_decr then
          let accg, ia, accs =  f accg accs in
          let cons = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var ia, Ne_var ele) in
	  let accs = { accs with sv_cons = cons :: accs.sv_cons } in
          i+1, accg, ia :: acca, accs
        else
          (* JG: If I understand correctly what this is doing:
           * It creates a fresh sv in input to be mapped to int arg in ouput.
           * We do not share ant constraint: this is a big loss of
           * information ! *)
          i+1, accg, ele :: acca, accs
      )
      (0, g, [], inst.sv_inst) ls_i.ia_int in
  let i_args = List.rev rev_args in
  let set_inst =
    if skipset then inst.set_inst
    else
      { inst.set_inst with
        colvi_eqs =
          Set_utils.gen_eq ls_i.ia_set ls_o.ia_set inst.set_inst.colvi_eqs } in
  let seq_inst =
    (* in new instanciation this function is replaced by
     * jinst_seq_addcstr_icall_eqs *)
    if skipset then inst.seq_inst else
    { inst.seq_inst with
      colvi_eqs =
        Seq_utils.gen_eq ls_i.ia_seq ls_o.ia_seq inst.seq_inst.colvi_eqs } in
  g, { inst with set_inst; seq_inst; sv_inst }, i_args

(* ===> used in graph_join, only for the seg_ext function
 *      between the two other generic instantiation functions
 * ===> check if the work done here is not redundant
 *)
(* Generate equality from set parameters of output segment edge
 * to set parameters of input segment edge *)
(* used in rule seg-ext *)
let l_seg_inst
    ?(skipset:bool = false)
    (ls_i: seg_edge) (* set pars from the join input *)
    (g: graph)    (* join input graph *)
    (ls_o: seg_edge) (* set pars from the join output being constructed *)
    (inst: join_inst) (* instantiation being constructed *)
    ~(pr: pars_rec)
    : graph * join_inst
    * (sv list * sv list) =
  hseg_sanity_check ~isok:1 "graph_inst,l_seg_inst,i" ls_i;
  hseg_sanity_check ~isok:1 "graph_inst,l_seg_inst,o" ls_o;
  (* collection parameters instantiation on edge itself *)
  let set_inst =
    if skipset then inst.set_inst
    else
      let eqs =
        inst.set_inst.colvi_eqs
        |> Set_utils.gen_eq ls_i.se_eargs.ias_set ls_o.se_eargs.ias_set in
      { inst.set_inst with colvi_eqs = eqs } in
  let seq_inst =
    let temp = List.map_flatten (fun (x,y) -> [x; y]) in
    let eqs =
      inst.seq_inst.colvi_eqs
      |> Seq_utils.gen_eq (temp ls_i.se_eargs.ias_seq)
          (temp ls_o.se_eargs.ias_seq) in
    { inst.seq_inst with colvi_eqs = eqs } in
  let f accg accs =
    let ia, accg = sv_add_fresh Ntint Nnone accg in accg, ia,
    { accs with sv_fresh = SvSet.add ia accs.sv_fresh } in
  (* integer parameters instantiated on the source/destination *)
  let _, g, rev_sargs, rev_dargs, sv_inst =
    List.fold_left2
      (fun (i, g, args_s, args_d, accs) id_s id_d ->
        let ipt = IntMap.find i pr.pr_int in
        if ipt.ipt_const then
          i+1, g, id_s :: args_s, id_d :: args_d, accs
        else if ipt.ipt_add then
          let g, is, accs = f g accs in
          let g, id, accs = f g accs in
          let cons =
            Nc_cons (Apron.Tcons1.EQ, Ne_var is,
                     Ne_bin (Apron.Texpr1.Add, Ne_var id,
                             Ne_bin (Apron.Texpr1.Sub, Ne_var id_s,
                                     Ne_var id_d))) in
          let cons = cons :: accs.sv_cons in
          i+1, g, is :: args_s, id :: args_d, { accs with sv_cons = cons }
        else if ipt.ipt_incr then
          let g, is, accs = f g accs in
          let g, id, accs = f g accs in
          let cons0 = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var id_s, Ne_var is) in
          let cons1 = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var id, Ne_var id_d) in
          let cons = cons0 :: cons1 :: accs.sv_cons in
          i+1, g, is::args_s, id :: args_d, { accs with sv_cons = cons }
        else if ipt.ipt_decr then
          let g, is, accs = f g accs in
          let g, id, accs = f g accs in
          let cons0 = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var is, Ne_var id_s) in
          let cons1 = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var id_d, Ne_var id) in
          let cons = cons0 :: cons1 :: accs.sv_cons in
          i+1, g, is :: args_s, id :: args_d, { accs with sv_cons = cons }
        else
          let g, is, accs = f g accs in
          let g, id, accs = f g accs in
          i+1, g, is :: args_s, id :: args_d, accs
      )
      (0, g, [], [], inst.sv_inst) ls_i.se_sargs.ia_int ls_i.se_dargs.ia_int in
  let inst = { inst with set_inst; seq_inst; sv_inst } in
  g, inst, (List.rev rev_sargs, List.rev rev_dargs)

(* Generate equality map from set parameters of output
 * summarized edge to set parameters of summarized edge
 * generated in inclusion checking *)
let gen_map (is: sv list) (io: sv list) (m: sv SvMap.t) =
  let len_is = List.length is and len_io = List.length io in
  if len_is != len_io then
    Log.fatal_exn "gen_map,length: %d --- %d" len_is len_io;
  List.fold_left2 (fun acc is o -> SvMap.add is o acc) m is io

(* ===> used in graph_join, in function guess_cons
 * ===> first see whether guess_cons needs to be refactored
 *)
(* [rename_is_le_inst is_le_inst jinst m] adds in [jinst] all the binding in
 * [is_le_inst] with key [m(key)] *)
let rename_is_le_inst
    (ck: Ast_sig.col_kind)
    (is_le_inst: 'a SvMap.t) (* is_le_inst: instantiation mapping elts to map *)
    (jinst: 'a SvMap.t)      (* jinst:      initial instantiation mapping *)
    (m: sv SvMap.t)          (* m:          renaming over keys *)
    : 'a SvMap.t           (* new instantiation mapping *) =
  let colv_fpr = Vd_utils.colv_fpr ck in
  SvMap.fold
    (fun i ex acc ->
       let j = SvMap.find i m in
       if SvMap.mem j acc then
         (* double instantiation attempt, fail *)
         Log.fatal_exn "attempt at double instantiation (%a)" colv_fpr j
       else SvMap.add j ex acc
    ) is_le_inst jinst

(* ===> used in graph_join: ind_weak and ind_intro
 * ===> see what is done in the list domain at that level *)
(* Deal with instantiation from is_le_ind *)
let is_le_call_inst
    (ls_i: ind_args) (* set pars from the is_le output *)
    (ls_o: ind_args) (* set pars from the join output being constructed *)
    (inst: join_inst) (* instantiation being constructed *)
    (sv_inst: sv_inst) (* sv instantiation from is_le *)
    (oset_inst: is_le_setv_inst option)  (* set instantiation from is_le *)
    (seq_inst: is_le_seqv_inst)  (* seq instantiation from is_le *)
    (rel:  sv SvMap.t)   (* sv mapping inferred by the inclusion *)
    : join_inst * sv list =
  (* set parameters:
   * - generate maps of set parameters from is_le ind to out ind
   * - accumulate equalities with the equalities collected in is_le;
   * - accumulate novel fresh symbols and constraints to prove *)
  let set_inst =
    match oset_inst with
    | None -> inst.set_inst
    | Some set_inst ->
        let setpar_maps = gen_map ls_i.ia_set ls_o.ia_set SvMap.empty in
	let eqs =
          rename_is_le_inst CK_set set_inst.cvi_def inst.set_inst.colvi_eqs
            setpar_maps in
        { inst.set_inst with
          colvi_eqs   = eqs;
          colvi_fresh = SvSet.union set_inst.cvi_new inst.set_inst.colvi_fresh;
          colvi_prove = set_inst.cvi_cons @ inst.set_inst.colvi_prove } in
  (* do similar operations over seq parameters *)
  let seqpar_maps = gen_map ls_i.ia_seq ls_o.ia_seq SvMap.empty in
  let seq_inst =
    let eqs =
      rename_is_le_inst CK_seq seq_inst.cvi_def inst.seq_inst.colvi_eqs
        seqpar_maps in
    let add = ls_o.ia_seq |> SvSet.of_list |> SvSet.union seq_inst.cvi_new in
    { inst.seq_inst with
      colvi_add   = SvSet.union add inst.seq_inst.colvi_add;
      colvi_eqs   = eqs;
      colvi_fresh = SvSet.union seq_inst.cvi_new inst.seq_inst.colvi_fresh;
      colvi_prove = seq_inst.cvi_cons @ inst.seq_inst.colvi_prove } in
  (* for sv/int parameters, just do a merge *)
  let sv_inst = sv_inst_merge inst.sv_inst sv_inst in
  { inst with set_inst; seq_inst; sv_inst },
  List.map (fun i -> SvMap.find i rel) ls_i.ia_int

(* ===> used in graph_join: seg_intro and seg_ext and seg_ext_ext
 * ===> see what is done in the list domain at that level *)
(* Deal with instantiation from is_le_seg:
 *  operations exactly like in is_le_seg_int *)
let is_le_seg_inst
    (ls_i: seg_edge) (* set pars from the is_le output *)
    (ls_o: seg_edge) (* set pars from the join output being constructed *)
    (inst: join_inst) (* instantiation being constructed *)
    (sv_inst: sv_inst) (* sv instantiation from is_le *)
    (oset_inst: is_le_setv_inst option)  (* set instantiation from is_le *)
    (seq_inst: is_le_seqv_inst)  (* seq instantiation from is_le *)
    (rel: sv SvMap.t)   (* sv mapping inferred by the inclusion *)
    : join_inst * sv list * sv list =
  hseg_sanity_check ~isok:1 "inst_utils,is_le_seg_inst,i" ls_i;
  hseg_sanity_check ~isok:1 "inst_utils,is_le_seg_inst,o" ls_o;
  let set_inst =
    match oset_inst with
    | None -> inst.set_inst
    | Some set_inst ->
        let setpar_maps =
          gen_map ls_i.se_eargs.ias_set ls_o.se_eargs.ias_set SvMap.empty in
        { inst.set_inst with
          colvi_eqs =
            rename_is_le_inst CK_set set_inst.cvi_def inst.set_inst.colvi_eqs
            setpar_maps;
          colvi_fresh = SvSet.union set_inst.cvi_new inst.set_inst.colvi_fresh;
          colvi_prove = set_inst.cvi_cons @ inst.set_inst.colvi_prove } in
  let seqpar_maps =
    let f = List.map_flatten (fun (x,y) -> [x; y]) in
    let lseq1 = f ls_i.se_eargs.ias_seq in
    let lseq2 = f ls_o.se_eargs.ias_seq in
    gen_map lseq1 lseq2 SvMap.empty in
  let seq_inst =
    { inst.seq_inst with
      (* Marking new seqv introduced in inclusion test as *added*.
       * TODO: use function similar to g_jinst_set_addctrs_post_isle *)
      colvi_add =
        seq_inst.cvi_def
        |> SvMap.bindings
        |> List.map (fun (i, _) -> SvMap.find i seqpar_maps)
        |> SvSet.of_list
        |> SvSet.union inst.seq_inst.colvi_add ;
      colvi_eqs =
        rename_is_le_inst CK_seq seq_inst.cvi_def inst.seq_inst.colvi_eqs
        seqpar_maps;
      colvi_fresh = SvSet.union seq_inst.cvi_new inst.seq_inst.colvi_fresh;
      colvi_prove = seq_inst.cvi_cons @ inst.seq_inst.colvi_prove } in
  let sv_inst = sv_inst_merge inst.sv_inst sv_inst in
  { inst with set_inst; seq_inst; sv_inst},
  List.map (fun i -> SvMap.find i rel) ls_i.se_sargs.ia_int,
  List.map (fun i -> SvMap.find i rel) ls_i.se_dargs.ia_int

(* ===> applied at the very end of the join algorithm; poorly documented
 * ===> check whether a similar step is done in the list domain
 *)
(* Guess constraints among set parameters and numerical parameters
 * from output graph *)
let guess_cons (jout: graph)
    (jin_l: graph) ~(vsat_l: vsat) (sv_instl: sv_inst)
    (jin_r: graph) ~(vsat_r: vsat) (sv_instr: sv_inst)
    (rel: node_relation)
    : sv SvMap.t
    * set_expr SvMap.t * set_expr SvMap.t
    * sv_inst * sv_inst =
  let loc_debug = true in
  let f_map il ir acc =
    List.fold_left2
      (fun acc i j -> SvMap.add i j (SvMap.add j i acc)) acc il ir in
  (* find all elements in a list in node_relation, produces pairs *)
  let do_rel (ia_intl: sv list) (rel: node_relation): (sv * sv) list =
    List.map (Nrel.find_p rel) ia_intl in
  (* find all elements of a list in a basic map, produces a list of SVs *)
  let do_inj (ia_intl: sv list) (inj: sv SvMap.t): sv list =
    List.map (fun a -> SvMap.find a inj) ia_intl in
  (* separate function to enrich SV instantiations *)
  let do_sv_inst (ia_intl: (sv * sv) list) (ia_intr: (sv * sv) list)
      (sv_instl: sv_inst) (sv_instr: sv_inst) ~(pr: pars_rec)
      : sv_inst * sv_inst =
    let _, istl, istr =
      List.fold_left2
        (fun (i, accl, accr) (al, ar) (bl, br) ->
          let ipt = IntMap.find i pr.pr_int in
          if ipt.ipt_add || ipt.ipt_const then
            (* TODO: why are Eq and Add treated exactly the same way *)
            let cl = Nc_cons (Apron.Tcons1.EQ, Ne_var al, Ne_var bl) in
            let cr = Nc_cons (Apron.Tcons1.EQ, Ne_var ar, Ne_var br) in
            if loc_debug then
              Log.info "tmp,guess_cons[%s]\n  cl=%a\n  cr=%a"
                (if ipt.ipt_const then "Eq" else "Add")
                n_cons_fpr cl n_cons_fpr cr;
            i+1,
            { accl with sv_cons = cl :: accl.sv_cons },
            { accr with sv_cons = cr :: accr.sv_cons }
          else if ipt.ipt_decr then
            let cl = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var bl, Ne_var al) in
            let cr = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var br, Ne_var ar) in
            i+1,
            { accl with sv_cons = cl :: accl.sv_cons },
            { accr with sv_cons = cr :: accr.sv_cons }
          else if ipt.ipt_incr then
            let cl = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var al, Ne_var bl) in
            let cr = Nc_cons (Apron.Tcons1.SUPEQ, Ne_var ar, Ne_var br) in
            i+1,
            { accl with sv_cons = cl :: accl.sv_cons },
            { accr with sv_cons = cr :: accr.sv_cons }
          else i+1, accl, accr
        ) (0, sv_instl, sv_instr) ia_intl ia_intr in
    istl, istr in
  (* for each node of the output graph, tries to refine parameters for segment
   * edges:
   * - when the destination SV of the segment carries empty or points-to,
   *   using inclusion check
   * - when the destination SV of the segment carries an inductive or segment
   *   using the parameters of that edge
   *)
  SvMap.fold
    (fun key n (acco, accl, accr, sv_instl, sv_instr) ->
      match n.n_e with
      | Hemp | Hpt _ | Hind _ ->
          acco, accl, accr, sv_instl, sv_instr
      | Hseg seg ->
          let dnode = SvMap.find seg.se_dnode jout.g_g in
          match dnode.n_e with
          | Hemp | Hpt _ ->
              if seg.se_ind.i_spars = 0  && seg.se_ind.i_ipars = 0 then
                acco, accl, accr, sv_instl, sv_instr
              else
                let ind = seg.se_ind in
                let isl, isr = Nrel.find_p rel seg.se_dnode in
                (*  verify that inclusion holds in both sides *)
                let le_res_l, iel =
                  Graph_algos.is_le_ind ~submem:false ind jin_l isl
                    ~vsat:vsat_l in
                let le_res_r, ier =
                  Graph_algos.is_le_ind ~submem:false ind jin_r isr
                    ~vsat:vsat_r in
                begin
                  match le_res_l, le_res_r with
                  | Ilr_le_rem ilreml, Ilr_le_rem ilremr ->
                      assert (ilreml.ilr_seqctr_r = [ ]);
                      assert (ilremr.ilr_seqctr_r = [ ]);
                      let parsl = do_inj iel.ie_args.ia_int ilreml.ilr_svrtol in
                      let parsr = do_inj ier.ie_args.ia_int ilremr.ilr_svrtol in
                      let ind_pars =
                        List.map2 (fun a b -> a, b) parsl parsr in
                      let seg_pars = do_rel seg.se_dargs.ia_int rel in
                      let sv_instl, sv_instr =
                        Inst_utils.sv_inst_merge ilreml.ilr_svinst sv_instl,
                        Inst_utils.sv_inst_merge ilremr.ilr_svinst sv_instr in
                      let sv_instl, sv_instr =
                        do_sv_inst ind_pars seg_pars sv_instl sv_instr
                          ~pr:ind.i_pkind in
                      (* XR: add some precisions wrt what is left todo here *)
                      (* JG: TODO *)
                      if ilreml.ilr_setinst.cvi_new = SvSet.empty
                          && ilreml.ilr_setinst.cvi_cons = []
                          && ilremr.ilr_setinst.cvi_new = SvSet.empty
                          && ilremr.ilr_setinst.cvi_cons = [] then
                        let smapl, smapr =
                          gen_map iel.ie_args.ia_set seg.se_eargs.ias_set
                            SvMap.empty,
                          gen_map ier.ie_args.ia_set seg.se_eargs.ias_set
                            SvMap.empty in
                        let accl =
                          rename_is_le_inst CK_set
                            ilreml.ilr_setinst.cvi_def accl smapl in
                        let accr =
                          rename_is_le_inst CK_set
                            ilremr.ilr_setinst.cvi_def accr smapr in
                        acco, accl, accr, sv_instl, sv_instr
                      else acco, accl, accr, sv_instl, sv_instr
                  | _, _ -> acco, accl, accr, sv_instl, sv_instr
                end
          | Hind ie ->
              assert (seg.se_ind.i_name = ie.ie_ind.i_name);
              let pars_l, pars_r =
                do_rel ie.ie_args.ia_int rel,
                do_rel seg.se_dargs.ia_int rel in
              let sv_instl, sv_instr =
                do_sv_inst pars_l pars_r sv_instl sv_instr
                  ~pr:ie.ie_ind.i_pkind in
              let nacco = f_map seg.se_eargs.ias_set ie.ie_args.ia_set acco in
              if not (nacco == acco) then
                Log.info "WARNING, modif";
              let acco = nacco in
              acco, accl, accr, sv_instl, sv_instr
          | Hseg se ->
              assert (seg.se_ind.i_name = se.se_ind.i_name);
              hseg_sanity_check ~isok:1 "graph_join,guess_cons" se;
              let pars_l, pars_r =
                do_rel se.se_sargs.ia_int rel,
                do_rel seg.se_dargs.ia_int rel in
              let sv_instl, sv_instr =
                do_sv_inst pars_l pars_r sv_instl sv_instr
                  ~pr:se.se_ind.i_pkind in
              (* Note: we used to apply f_map on the set component;
               *       at this point, this is not needed anymore;
               *    => wa may revise this; TODO, check *)
              acco, accl, accr, sv_instl, sv_instr
    ) jout.g_g
    (SvMap.empty, SvMap.empty, SvMap.empty, sv_instl, sv_instr)
