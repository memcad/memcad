(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_val_subm.ml
 **       lifting of a numerical domain into a submemory abstraction
 **       (together with a main memory)
 ** Xavier Rival, Pascal Sotin, 2012/05/11 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def

open Ast_sig
open Col_sig
open Dom_sig
open Dom_subm_sig
open Graph_sig
open Nd_sig
open Col_sig
open Seq_sig
open Set_sig
open Svenv_sig
open Vd_sig

open Dom_utils
open Graph_utils
open Nd_utils
open Sv_utils
open Svenv_utils

open Apron

(** TODO:
 **  0. counting:
 **     - support the modulo with respect to the stride in the submem
 **     - eventually, move this support in the graph, with modulo information
 **       on SV values, when they denote addresses
 **     - add the guards on the environment
 **
 **  1. uniformize the treatment of the add_rem, and synchronization
 **
 **  - localization function
 **  - sanity check questions
 **  - finish function symvars_merge
 **  - delegation in transfer functions
 **)


(** Error report *)
module Log =
  Logger.Make(struct let section = "dv_subm_" and level = Log_level.DEBUG end)
(* whether or not to print full debug information *)
let full_debug = true
let show_sanity_check = true


(** Node kind *)
type node_kind =
  | Nk_address
  | Nk_contents


(** Module construction *)
module Make_Vals_Subm = functor (Dv: DOM_VALCOL) -> functor (S: SUBMEM_SIG) ->
  (struct
    let module_name = "dom_val_subm"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%s -> %s\n%a%a"
        module_name Dv.module_name module_name S.module_name
        Dv.config_fpr () S.config_fpr ()
    module S = Dom_subm_graph.Submem

    (** Dom ID *)
    let dom_id = ref (sv_unsafe_of_int (-1), "msubm")

    (** Type of abstract values *)
    (* each sub-memory is attached to a base address and a content node
     * (abbr c-node)
     *  - snodes maps each id for a node living in a sub-memory
     *    to the sub-memory and sub-node
     *  - sub-node indexes are negative integers
     *)
    type t =
        { (* main memory: underlying domain *)
          t_main:   Dv.t;
          (* base address node -> content node *)
          t_bases:  (sv, sv) Bi_fun.t;
          (* content node -> sub-memory *)
          t_subs:   S.t SvMap.t;
          (* sub-nodes localization: global -> (sub-memory, sub-node) *)
          t_snodes: (sv * sv) SvMap.t;
          (* next subnode index *)
          t_snxt: int }

    (** Domain initialization *)
    (* Domain initialization to a set of inductive definitions *)
    let init_inductives (g: SvGen.t) (s: StringSet.t): SvGen.t =
      let g, k = SvGen.gen_key g in (* this domain generates keys *)
      if sv_upg then dom_id := k, snd !dom_id;
      S.init_inductives g s

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t =
      { t_main   = Dv.bot;
        t_bases  = Bi_fun.empty_inj;
        t_subs   = SvMap.empty;
        t_snodes = SvMap.empty;
        t_snxt   = -1; }
    let is_bot (x: t): bool =
      Dv.is_bot x.t_main
    (* Top element *)
    let top: t =
      { t_main   = Dv.top;
        t_bases  = Bi_fun.empty_inj;
        t_subs   = SvMap.empty;
        t_snodes = SvMap.empty;
        t_snxt   = -1; }

    (** Pretty-printing *)
    let t_fpri (sn: sv_namer) (ind: string) (fmt: form) (x: t): unit =
      Dv.t_fpri sn ind fmt x.t_main;
      if full_debug then
        F.fprintf fmt "%sglobal dict (%d): <|%a|>\n" ind x.t_snxt
          (SvMap.t_fpr ";"
             (fun fmt (i,j) -> F.fprintf fmt "(%a,%a)" sv_fpr i sv_fpr j))
          x.t_snodes;
      if x.t_subs != SvMap.empty then
        let nind = "  "^ind in
        (* printing of base addresses *)
        if full_debug then
          begin
            F.fprintf fmt "%sbases:\n" ind;
            Bi_fun.iter_dir
              (fun i -> F.fprintf fmt "%s    %a => %a\n" ind sv_fpr i sv_fpr)
              x.t_bases
          end;
        (* printing of sub-memories contents *)
        SvMap.iter
          (fun i sub ->
            F.fprintf fmt "%s%a:\n%a" ind sv_fpr i (S.t_fpri nind) sub
          ) x.t_subs

    (** Sanity checks *)
    (* Sanity properties:
     *  - all sub-memories should be sane
     * (other properties to add)
     * TODO: sanity check
     *  - global nodes referring to a sub-memory should be negative
     *  - snodes should map keys to each sub-memory according to its
     *    actual contents
     *  - snodes should belong to [t_snxt+1, -1]
     *)
    let sanity_check (ctx: string) (x: t): unit =
      let error (msg: string) =
        Log.fatal_exn "sanity_check<%s> fails (domsub): %s" ctx msg in
      try
        SvMap.iter (fun _ s -> S.sanity_check ctx s) x.t_subs;
        let submem_nodes =
          SvMap.fold
            (fun glo (sub, loc) acc ->
              let glo = sv_to_int glo in
              if glo >= 0 then
                error "found positive global key for submem"
              else if glo <= x.t_snxt then
                error "too low sub index"
              else
                let ksub =
                  try SvMap.find sub acc with Not_found -> SvSet.empty in
                SvMap.add sub (SvSet.add loc ksub) acc
            ) x.t_snodes SvMap.empty in
        SvMap.iter
          (fun sub set ->
            try
              let smem = SvMap.find sub x.t_subs in
              (* ??? are the local or global keys ??? *)
              let smemkeys = S.get_keys smem in
              if !flag_debug_submem then
                begin
                  Log.force "Check, %s, looking for subm %a" ctx sv_fpr sub;
                  Log.force " - computed keys: %a" svset_fpr set;
                  Log.force " - actual keys: %a" svset_fpr smemkeys;
                end;
              if not (SvSet.equal set smemkeys) then
                error "submem nodes do not correspond to index"
            with Not_found ->
              error (F.asprintf "unbound submem number %a" sv_fpr sub)
          ) submem_nodes;
      with e ->
        Log.info "Before sanity check:\n%a" (t_fpri SvMap.empty "  ") x;
        raise e
    (* For sanity check *)
    let check_nodes ~(svs: SvSet.t) ~(colvs: col_kinds) (x: t): bool =
      Dv.check_nodes ~svs ~colvs x.t_main

    (** Conversions functions *)
    (* Checks whether it is a sub-node *)
    let is_subnode (x: t) (i: sv): bool = SvMap.mem i x.t_snodes
    (* From addresses to contents and sub-memories *)
    let addr_2cont (x: t) (addr: sv): sv =
      try Bi_fun.image addr x.t_bases
      with Not_found -> Log.fatal_exn "submem addr %a not found" sv_fpr addr
    let cont_2submem (x: t) (cont: sv): S.t =
      try SvMap.find cont x.t_subs
      with Not_found -> Log.fatal_exn "submem val %a not found" sv_fpr cont
    let addr_2submem (x: t) (addr: sv): S.t =
      cont_2submem x (addr_2cont x addr)
    let glo_2subnode (ctx: string) (x: t) (i: sv): sv * sv =
      try SvMap.find i x.t_snodes
      with Not_found -> Log.fatal_exn "%s failed to retrieve %a" ctx sv_fpr i
    let cont_2addr (x: t) (cont: sv): sv =
      try Bi_fun.inverse_inj cont x.t_bases
      with Not_found -> Log.fatal_exn "submem cont %a not found" sv_fpr cont

    (** Internal utilities *)
    (* Check no impact if modif on global SV *)
    let may_impact_sv_mod (i: sv) (x: t): bool =
      try
        SvMap.iter
          (fun _ sub ->
            if S.may_impact_sv_mod i sub then raise Stop
          ) x.t_subs;
        false
      with Stop -> true
    (* Offsets simplification *)
    let make_osimplifier (x: t) (o: Offs.t): Offs.t =
      Offs.of_n_expr (Dv.simplify_n_expr x.t_main (Offs.to_n_expr o))
    let subnode_sv_bind (ig: sv) (loc: sv * sv) (x: t): t =
      if !flag_debug_submem then
        Log.force "subnode_sv_bind %a -> %a" sv_fpr ig sv_fpr (snd loc);
      { x with t_snodes = SvMap.add ig loc x.t_snodes }
    (* Adding a node to a sub-memory *)
    let submem_sv_add (nk: node_kind) (x: t) (cont: sv) (sub: S.t)
        : t * sv * sv * S.t =
      (* global key for the address *)
      let x, nglo =
        assert (x.t_snxt < 0);
        let n = sv_unsafe_of_int x.t_snxt in
        { x with
          t_main   = Dv.sv_add n x.t_main;
          t_snxt   = x.t_snxt - 1; }, n in
      (* local addition of the address *)
      let nloc, sub =
        match nk with
        | Nk_address  -> S.sv_add_address nglo sub
        | Nk_contents -> S.sv_add_contents nglo sub in
      (* binding of the global key *)
      let x = subnode_sv_bind nglo (cont, nloc) x in
      x, nglo, nloc, sub

    (** Internal utilities, submem environment *)
    let sub_env_add (o: Offs.t) (i: sv) (sub: S.t): S.t =
      let omaxl = Bounds.to_off_list (S.get_omax sub) in
      List.iter (Log.info "@@@nnn: %a <= %a" Offs.t_fpr o Offs.t_fpr) omaxl ;
      S.env_add o i sub
    (* reduction of the whole environment *)
    let sub_env_reduce (x: t): t =
      Log.info "sub_env_reduce";
      let m =
        SvMap.fold
          (fun _ sub m ->
            let olmin = Bounds.to_off_list (S.get_omin sub)
            and olmax = Bounds.to_off_list (S.get_omax sub) in
            OffMap.fold
              (fun o delta m ->
                let m =
                  List.fold_left
                    (fun m omax ->
                      Log.info "@@@r: %a < %a     [%d]"
                        Offs.t_fpr o Offs.t_fpr omax delta;
                      let ne = Offs.size_to_n_expr (Offs.sub omax o) in
                      let nc = Nc_cons (Tcons1.SUPEQ, ne, Ne_csti delta) in
                      Dv.guard true nc m
                    ) m olmax in
                List.fold_left
                  (fun m omin ->
                    Log.info "@@@l: %a <= %a" Offs.t_fpr omin Offs.t_fpr o;
                    let ne = Offs.size_to_n_expr (Offs.sub o omin) in
                    let nc = Nc_cons (Tcons1.SUPEQ, ne, Ne_csti 0) in
                    Dv.guard true nc m
                  ) m olmin
              ) (S.get_off_ds sub) m
          ) x.t_subs x.t_main in
      { x with t_main = m }
    (* addition functions with and without reduction *)
    let sub_env_add_reduce (o: Offs.t) (i: sv) (sub: S.t) (x: t): S.t * t =
      let omaxl = Bounds.to_off_list (S.get_omax sub) in
      let x =
        List.fold_left
          (fun accx omax ->
            Log.info "@@@y: %a <= %a" Offs.t_fpr o Offs.t_fpr omax;
            let ne = Offs.size_to_n_expr (Offs.sub omax o) in
            let nc = Nc_cons (Tcons1.SUPEQ, ne, Ne_csti 0) in
            { x with t_main = Dv.guard true nc x.t_main }
          ) x omaxl in
      Log.info "after additions:\n%a" (t_fpri SvMap.empty "   ") x;
      S.env_add o i sub, x

    (** Unfolding support *)
    let tag_unfold_exn (cont: sv) (f: 'a -> 'b) (x: 'a): 'b =
      try f x
      with
      | Unfold_request (Uloc_main n, d) ->
          raise (Unfold_request (Uloc_sub (cont, n), d))

    (** Basic operations on SVs *)
    let sv_add ?(mark: bool = true) (sv: sv) (x: t): t =
      { x with t_main = Dv.sv_add sv x.t_main }
    let sv_rem (id: sv) (x: t): t =
      let rem_submem_contents (id: sv) (x: t): t =
        (* Removal of a sub-memory contents
         *  - unlink its base address
         *  - unlink its index entries *)
        let nbases = Bi_fun.rem_inv id x.t_bases in
        let nsubs = SvMap.remove id x.t_subs in
        let nsnodes, nnum =
          SvMap.fold
            (fun i (cont, _) acc ->
              if cont = id then
                SvMap.remove i (fst acc), Dv.sv_rem i (snd acc)
              else acc
            ) x.t_snodes (x.t_snodes, x.t_main) in
        { x with
          t_main   = Dv.sv_rem id nnum;
          t_bases  = nbases;
          t_subs   = nsubs;
          t_snodes = nsnodes; } in
      (* TODO:
       * - the first case does not seem to occur
       * - in the last case, there is one occurence of a deleted offset in
       *   the environment; it should just be processed as such...
       *)
      if Bi_fun.mem_dir id x.t_bases then
        (* we drop all info about the sub-memory itself first,
         * and then remove the base afterwards *)
        (* though, we keep the contents, node, with no info about it
         * (this case is rather unsatisfactory: contents should always
         *  be removed first; to INVESTIGATE) *)
        let cont = Bi_fun.image id x.t_bases in
        Log.warn "REM_NODE: removes sub-memory address before contents!";
        let x = rem_submem_contents cont x in
        { x with t_main = Dv.sv_add cont (Dv.sv_rem id x.t_main) }
      else if SvMap.mem id x.t_subs then
        rem_submem_contents id x
      else
        begin
          if may_impact_sv_mod id x then
            begin
              Log.info "sv_rem,disposing %a in\n%a" sv_fpr id
                (t_fpri SvMap.empty "   ") x;
              Log.warn "sv_rem,should drop sub-mem";
            end;
          { x with t_main = Dv.sv_rem id x.t_main }
        end

    (** Basic operations on COLVs *)
    let colv_add (ck: col_kind) ~(root: bool)
        ?(kind: set_par_type option = None)
        ?(name: string option = None) (id: sv)
        (info: colv_info option) (x: t): t =
      { x with
        t_main =
          Dv.colv_add ck ~root:root ~kind:kind ~name:name id info x.t_main }
    let colv_rem (colv: sv) (x: t): t =
      { x with t_main = Dv.colv_rem colv x.t_main }
    let colv_is_root (sv: sv) (x: t): bool = Dv.colv_is_root sv x.t_main
    let colv_get_roots (t: t): SvSet.t = Dv.colv_get_roots t.t_main

    (** Renaming and environment management *)
    (* Renaming (e.g., post join) *)
    let symvars_srename
        ?(mark: bool = true) (om: (Offs.t * sv) OffMap.t)
        (nm: (sv * Offs.t) node_mapping)
        (colvmo: colv_map_complete option) (x: t): t =
      (* TODO: errors suspected regarding to this "mark" parameter
       *       it should probably be removed! *)
      if !flag_debug_submem then
        Log.info "S.symvars_srename,BEFORE:\n%a" (t_fpri SvMap.empty "  ") x;
      (* for now, only singletons supported for addresses and contents nodes *)
      let f_one (i: sv): sv = (* works iff *one* image *)
        try
          let (ni, s) = SvMap.find i nm.nm_map in
          if s = SvSet.empty then ni
          else
            begin
              Log.info "mapping:\n%a" node_mapping_fpr nm;
              Log.fatal_exn "symvars_srename several images: %a" sv_fpr i
            end
        with
        | Not_found ->
            let msg = F.asprintf "symvars_srename no img: %a" sv_fpr i in
            raise (FailMsg msg) in
      (* - renaming inside the underlying value abstraction *)
      let main = Dv.symvars_srename ~mark:mark om nm colvmo x.t_main in
      (* - add nodes corresponding to "new offsets" (added in the join when
       *   offsets could not be unified *)
      let main = OffMap.fold (fun _ (_, i) -> Dv.sv_add ~mark:mark i) om main in
      (* - renaming in sub-memories and add equalities synthesized
       *   when new offsets generated *)
      let nsubs, eqs, rems =
        SvMap.fold
          (fun icont sub (acc_s, acc_e, acc_r) ->
            let iaddr = S.get_addr sub in
            let nicont = f_one icont in
            assert (not (SvMap.mem nicont acc_s));
            let nm = { nm with
                       nm_suboff = fun o -> nm.nm_suboff (iaddr, o) } in
            if true then (* temporary message *)
              Log.info "about to rename in underlying submem";
            (* if this does not work, we should try to drop the submemory *)
            match S.symvars_srename om nm sub with
            | None -> acc_s, acc_e, (icont, sub) :: acc_r
            | Some (sub, eqs) ->
                SvMap.add nicont sub acc_s, eqs @ acc_e, acc_r
          ) x.t_subs (SvMap.empty, [ ], [ ]) in
      (* - adding equality constraints synthesized above *)
      let main = List.fold_left (fun acc e -> Dv.guard true e acc) main eqs in
      (* - indexes that should be removed properly *)
      let nbases, nsnodes, main =
        let remset =
          List.fold_left (fun acc (s, _) -> SvSet.add s acc) SvSet.empty rems in
        List.fold_left
          (fun (nbases, nsnodes, main) (svcont, sub) ->
            let base = S.get_addr sub in
            let nbases = Bi_fun.rem_dir base nbases in
            let nsnodes, urem, main =
              SvMap.fold
                (fun i (b, glo) ((acck, accr, main) as acc) ->
                  if SvSet.mem b remset then
                    SvMap.remove i acck, SvSet.add glo accr, Dv.sv_rem i main
                  else acc
                ) nsnodes (nsnodes, SvSet.empty, main) in
            nbases, nsnodes, main
          ) (x.t_bases, x.t_snodes, main) rems in
      (* - renaming inside the snodes field *)
      let nsnodes =
        SvMap.fold
          (fun mid (sub, lid) acc ->
            SvMap.add mid (f_one sub, lid) acc
          ) nsnodes SvMap.empty in
      (* - renaming base addresses *)
      let nbases =
        Bi_fun.fold_dom
          (fun base content acc ->
            let nbase = f_one base in
            let ncontent = f_one content in
            assert (not (Bi_fun.mem_dir nbase acc));
            Bi_fun.add nbase ncontent acc
          ) nbases Bi_fun.empty_inj in
      if !flag_debug_submem then
        Log.force "srename:\n%a\nmapping:\n%a" (t_fpri SvMap.empty "   ") x
          node_mapping_fpr nm;
      let x =
        { t_main   = main;
          t_bases  = nbases;
          t_subs   = nsubs;
          t_snodes = nsnodes;
          t_snxt   = x.t_snxt; } in
      if !flag_debug_submem then
        Log.force "S.symvars_srename, AFTER:\n%a" (t_fpri SvMap.empty "  ") x;
      sanity_check "symvars_srename, after" x;
      x
    (* Synchronization of the SV environment *)
    let sve_sync_top_down (svm: svenv_mod) (t: t): t =
      svenv_mod_doit (fun i _ -> sv_add i) sv_rem
        (Log.fatal_exn "mod: %a" sv_fpr) svm t
    (* Check the symbolic vars correspond exactly to given set *)
    let symvars_check (s: SvSet.t) (x: t): bool =
      sanity_check "symvars_check" x;
      let sub_keys = SvMap.fold (fun i _ -> SvSet.add i) x.t_snodes s in
      Dv.symvars_check sub_keys x.t_main
    let symvars_filter (s: SvSet.t) ?(col_vars: SvSet.t = SvSet.empty) (x: t) =
      Log.fatal_exn "symvars_filter in subm"
    let add_subnode (sub: sv) (sn: sv) (x: t): t * sv =
      assert (x.t_snxt < 0);
      let n = sv_unsafe_of_int x.t_snxt in
      { x with
        t_main   = Dv.sv_add n x.t_main;
        t_snodes = SvMap.add n (sub, sn) x.t_snodes;
        t_snxt   = x.t_snxt - 1; }, n
    (* Maintaining consistency with the symbolic names in the sub-memory:
     *  - gets set of locally removed nodes (g), and perform global removal;
     *  - gets set of locally added nodes (l), and perform global addition *)
    let sync_addrem (x: t): t =
      SvMap.fold
        (fun isub sub x ->
          let sub, add, rem = S.sync_addrem sub in
          (* treat removal of local nodes *)
          SvSet.iter (fun irem -> assert (sv_to_int irem < 0)) rem;
          let x =
            SvSet.fold
              (fun iglo x ->
                { x with
                  t_snodes = SvMap.remove iglo x.t_snodes;
                  t_main   = Dv.sv_rem iglo x.t_main }
              ) rem { x with t_subs = SvMap.add isub sub x.t_subs } in
          (* treat additions *)
          SvSet.fold
            (fun iloc x ->
              let x, iglo = add_subnode isub iloc x in
              let sub = SvMap.find isub x.t_subs in
              let sub = S.register_node iloc iglo sub in
              { x with t_subs = SvMap.add isub sub x.t_subs }
            ) add x
        ) x.t_subs x

    (** Submemory introduction *)
    (* Merging into a new variable *)
    let symvars_merge
        (stride: int) (* stride of the structure being treated *)
        (base: sv)   (* node serving as a base address of a block (gen mem) *)
        (sv: sv)     (* node serving as a new block content *)
        (block_desc: (Bounds.t * Offs.svo * Offs.size) list) (*merged contents*)
        (extptrs: OffSet.t)  (* offsets of external pointers into the block *)
        (x: t)
        : t =
      if !flag_debug_submem then
        begin
          Log.force ("\n\nCall to symvars_merge:\n" ^^
                     "  - stride: %d\n" ^^
                     "  - base: %a\n  - sv: %a\n" ^^
                     "  - block:")
            stride sv_fpr base sv_fpr sv;
          List.iter
            (fun (b, (i, o), _) ->
              Log.force "       %a => (%a,%a)%s" Bounds.t_fpr b sv_fpr i
                Offs.t_fpr o (if i = base then "   [submem entry]" else "")
            ) block_desc;
          Log.force "  - ext ptrs to: %a" (OffSet.t_fpr "; ") extptrs;
          let bs =
            Bi_fun.fold_dom (fun i _ -> SvSet.add i) x.t_bases SvSet.empty in
          let nbs = SvSet.cardinal bs in
          Log.force "  - other bases: %d" nbs;
          Bi_fun.iter_dir (fun i _ -> Log.force "      %a => ." sv_fpr i)
            x.t_bases;
        end;
      (* pre-filtering of the block description
       *  -> to see whether we are merging into a pre-existing sub-memory:
       *     i.e., we distinguish two cases;
       *     - either there is no sub-memory at all
       *     - or there is one sub-memory at the beginning of the block;
       *  -> we should expand this with more possibilities *)
      let expanding, block_desc =
        match block_desc with
        | [ ] -> false, [ ]
        | (bnd0, on0, _) :: block_desc1 ->
            (* block_desc1 should not contain any sub-memory *)
            List.iter
              (fun (_, (i1, _), _) ->
                if SvMap.mem i1 x.t_subs then
                  Log.fatal_exn "sub-memories beyond beg"
              ) block_desc1;
            if SvMap.mem (fst on0) x.t_subs then
              (* first block element is a sub-memory *)
              true, block_desc1
            else
              (* no sub-memory in the first item *)
              false, block_desc in
      (* pre-sorting of the block description, along the stride *)
      let block_decomp =
        let rec aux accu ongoing bd =
          match bd with
          | [] ->
              Log.info " . remaining elts: %d" (List.length ongoing);
              List.iter
                (fun (b, on, _) ->
                  Log.info "    %a: %a" Bounds.t_fpr b onode_fpr on
                ) ongoing;
              accu
          | (bnd, dst, sz) :: bd0 ->
              let b0, b1 = Bounds.modulo_split stride bnd in
              if Bounds.is_zero b1 then (* aligned on stride *)
                aux ((b0, ((b1, dst, sz) :: ongoing)) :: accu) [] bd0
              else (* not aligned on stride; push and continue *)
                aux accu ((b1, dst, sz) :: ongoing) bd0 in
        aux [] [] (List.rev block_desc) in
      (* if there is no sub-memory, create one *)
      (* Extraction of the existing sub-memory or creation of a new one *)
      let x, sub =
        if Bi_fun.mem_dir base x.t_bases then
          (* there is already a sub-memory here; we should extend it *)
          let cont = Bi_fun.image base x.t_bases in
          assert expanding;
          assert (SvMap.mem cont x.t_subs);
          let sub  = SvMap.find cont x.t_subs in
          if !flag_debug_submem then
            Log.force "  - old submem:\n%a" (S.t_fpri "      ") sub;
          (* - removal of the pre-existing sub-memory that is being merged
           * - renaming of all references to the previous sub-memory *)
          let snodes =
            SvMap.fold
              (fun sn old_binding ->
                let p =
                  if fst old_binding = cont then sv, snd old_binding
                  else old_binding in
                SvMap.add sn p
              ) x.t_snodes SvMap.empty in
          { x with
            t_bases  = Bi_fun.rem_dir base x.t_bases;
            t_subs   = SvMap.remove cont x.t_subs;
            t_snodes = snodes; }, sub
        else
          (* we attempt to create a sub-memory *)
          let sub = S.empty base sv stride Bounds.zero Bounds.zero in
          assert (not expanding);
          x, sub in
      (* Addition of address nodes into the sub-memory *)
      let x, sub, block_decomp =
        List.fold_left
          (fun (x, sub, acc_block) (bnd_stride, sub_block) ->
            if !flag_debug_submem then
              Log.force "Phase 1, Sub-block: %a (%d)"
                Bounds.t_fpr bnd_stride (List.length sub_block);
            (* this first method for searching for the local address is
             * a bit unclear to me *)
            let offs_list = Bounds.to_off_list bnd_stride in
            let offs =
              List.fold_left
                (fun acc o ->
                  if OffSet.mem o extptrs then OffSet.add o acc else acc
                ) OffSet.empty offs_list in
            let b_already_there =
              List.fold_left
                (fun acc o -> acc || S.env_mem o sub) false offs_list in
            if b_already_there then
              let oloc_addr =
                OffSet.fold
                  (fun o acc ->
                    match acc with
                    | Some _ -> acc
                    | None -> try Some (S.env_find o sub) with Not_found -> acc
                  ) offs None in
              let loc_addr =
                match oloc_addr with
                | Some a -> a
                | None ->
                    (* no offset found as part of ext pointers:
                     * we search internal pointers from inside the sub-memory
                     * (2nd method to search for the local address) *)
                    let i =
                      List.fold_left
                        (fun acc o ->
                          match acc with
                          | Some _ -> acc
                          | None ->
                              if S.env_mem o sub then Some (S.env_find o sub)
                              else None
                        ) None offs_list in
                    match i with
                    | Some a -> a
                    | None ->
                        Log.fatal_exn "symvars_merge: found no local address" in
              let sub = S.node_assume_placed loc_addr sub in
              if !flag_debug_submem then
                Log.info "Existing offset @ node %a" sv_fpr loc_addr;
              x, sub, (loc_addr, bnd_stride, sub_block) :: acc_block
            else
              (* TODO: extend the sub-memory scope here *)
              let x, glo_addr, loc_addr, sub =
                submem_sv_add Nk_address x sv sub in
              (* addition of the environment if needed *)
              let sub =
                if offs != OffSet.empty && !flag_debug_submem then
                  begin
                    Log.info "Adding %d offset" (OffSet.cardinal offs);
                    OffSet.iter (Log.info "  - %a" Offs.t_fpr) offs;
                  end;
                OffSet.fold (fun o -> sub_env_add o loc_addr) offs sub in
              x, sub, (loc_addr, bnd_stride, sub_block) :: acc_block
          ) (x, sub, []) block_decomp in
      (* Update the max offset of the sub-memory *)
      let sub =
        match block_decomp with
        | [ ] -> sub
        | (_, b, _) :: _ -> S.update_max b stride sub in
      (* Addition of edges into the sub-memory *)
      let x, sub, eqs =
        List.fold_left
          (fun (x, sub, eqs) (addr, bnd_stride, sub_block) ->
            if !flag_debug_submem then
              Log.force "Ph2,Sub-block: %a" Bounds.t_fpr bnd_stride;
            (* construction of the sub-block *)
            List.fold_left
              (fun (x, sub, eqs) (bnd_off, (idst, odst), sz) ->
                let x, sub, cont, eqs =
                  if idst = base then
                    x, sub, S.env_find odst sub, eqs
                  else
                    (* same operations for the contents key as for addr key
                     * (we may factor those) *)
                    let x, glo_cont, cont, sub =
                      submem_sv_add Nk_contents x sv sub in
                    let eqs =
                      if Offs.is_zero odst then (glo_cont, idst) :: eqs
                      else eqs in
                    x, sub, cont, eqs in
                (* addition of th ecell in the sub heap *)
                if !flag_debug_submem then
                  Log.force "Addition in sub-block: %a + %a -> %a + %a"
                    sv_fpr addr Bounds.t_fpr bnd_off sv_fpr cont
                    Offs.t_fpr odst;
                x, S.add_cell addr bnd_off sz cont sub, eqs
              ) (x, sub, eqs) sub_block
          ) (x, sub, [ ]) (List.rev block_decomp) in
      (* reduction into the sub-memory *)
      let sub = S.post_build sub in
      (* feeding the environment *)
      (* for now, we only carry out the node addition *)
      let x = sv_add sv x in
      if !flag_debug_submem then
        Log.force "Adding submem (addr: %a; cont: %a)" sv_fpr base sv_fpr sv;
      (* taking equalities into the global env *)
      let num =
        List.fold_left
          (fun num (glo, loc) ->
            if !flag_debug_submem then
              Log.force "Equality: %a = %a" sv_fpr glo sv_fpr loc;
            Dv.guard true (Nc_cons (Tcons1.EQ, Ne_var glo, Ne_var loc)) num
          ) x.t_main eqs in
      let r =
        { x with
          t_main  = num;
          t_bases = Bi_fun.add base sv x.t_bases;
          t_subs  = SvMap.add sv sub x.t_subs; } in
      if !flag_debug_submem then
        Log.force "after add:\n%a" (t_fpri SvMap.empty "  ") r;
      sanity_check "symvars_merge, after" r;
      r

    (** Lattice and join operators *)
    (* Comparison *)
    let is_le (x0: t) (x1: t) (sat_diseq: sv -> sv -> bool): bool =
      if Dv.is_le x0.t_main x1.t_main sat_diseq then
        try
          SvMap.iter
            (fun cont sub0 ->
              let sub1 =
                try SvMap.find cont x1.t_subs
                with Not_found -> raise Stop in
              if !flag_debug_submem then
                Log.force "Is_le status:\n - left:\n%a - right:\n %a"
                  (t_fpri SvMap.empty "    ") x0
                  (t_fpri SvMap.empty "    ") x1;
              let b = S.is_le (Dv.sat x0.t_main) sub0 sub1 in
              if not b then raise Stop
            ) x0.t_subs;
          true
        with Stop -> false
      else false
    (* Upper bound: serves as join and widening *)
    let upper_bnd ?hint (k: join_kind) (x0: t) (x1: t): t =
      if !flag_debug_submem then
        Log.force "JOIN:\n-left:\n%a-right:\n%a"
          (t_fpri SvMap.empty "  ") x0 (t_fpri SvMap.empty "  ") x1;
      (* Make both elements compatible:
       *  - if sub-memory only in one side, then downgrade it to raw node *)
      let drop_incompat (xa: t) (xb: t): t =
        Bi_fun.fold_dom
          (fun basea suba x ->
            if Bi_fun.mem_dir basea xb.t_bases then x
            else
              (* sub-memory information about suba should be dropped:
               *  - add suba to main memory, as a node with no information;
               *  - remove entries about suba and maina
               *  - filter out nodes in the sub-graph *)
              let snodes =
                let img =
                  try S.get_keys (SvMap.find suba x.t_subs)
                  with Not_found -> Log.fatal_exn "sanity issue" in
                Log.info "ffound keys: %a" svset_fpr img;
                SvSet.fold SvMap.remove img x.t_snodes in
              Log.warn "Submem: about to drop a sub-memory";
              { t_main   = Dv.sv_add suba x.t_main;
                t_bases  = Bi_fun.rem_dir basea x.t_bases;
                t_subs   = SvMap.remove suba x.t_subs;
                t_snodes = snodes;
                t_snxt   = x.t_snxt }
          ) xa.t_bases xa in
      let x0 = drop_incompat x0 x1 in
      let x1 = drop_incompat x1 x0 in
      let sat0 = Dv.sat x0.t_main and sat1 = Dv.sat x1.t_main in
      (* Initialization of a relation for remapping:
       *  identity function over positive indexes *)
      let init_rel = Nrel.empty in
      (* Compute joins of sub-memories *)
      let subs, snodes, nrel, snxt =
        Bi_fun.fold_dom
          (fun base cont (accsubs, accsn, accrel, acci) ->
            assert
              (try Bi_fun.image base x1.t_bases = cont with Not_found -> false);
            let sub0 =
              try SvMap.find cont x0.t_subs
              with Not_found -> Log.fatal_exn "join, submem not found, l" in
            let sub1 =
              try SvMap.find cont x1.t_subs
              with Not_found -> Log.fatal_exn "join, submem not found, r" in
            (* computation of roots, and launch of the join *)
            let jsub, rel, snodes, acci =
              S.upper_bnd acci accrel accsn (base, cont) sat0 sub0 sat1 sub1 in
            SvMap.add cont jsub accsubs, snodes, rel, acci
          ) x0.t_bases (SvMap.empty, SvMap.empty, init_rel, -1) in
      sanity_check "join,compat,l" x0;
      sanity_check "join,compat,r" x1;
      let map0, map1 =
        let extract_init (snodes: (sv * sv) SvMap.t)
            : (sv * Offs.t) node_mapping =
          let s = SvMap.fold (fun i _ -> SvSet.add i) snodes SvSet.empty in
          { nm_map = SvMap.empty;
            nm_rem = s;
            nm_suboff = fun _ -> (Log.info "nmap(1)"; true) } in
        let init0 = extract_init x0.t_snodes
        and init1 = extract_init x1.t_snodes in
        SvMap.fold
          (fun i (i0, i1) (acc0, acc1) ->
            Nd_utils.add_to_mapping i0 i acc0,
            Nd_utils.add_to_mapping i1 i acc1
          ) nrel.n_pi (init0, init1) in
      (* TODO: parameter None to symvars_srename NOT normal *)
      let main0 = Dv.symvars_srename OffMap.empty map0 None x0.t_main in
      let main1 = Dv.symvars_srename OffMap.empty map1 None x1.t_main in
      let main  = Dv.upper_bnd k main0 main1 in
      sub_env_reduce { t_main   = main;
                       t_bases  = x0.t_bases;
                       t_subs   = subs;
                       t_snodes = snodes;
                       t_snxt   = snxt }

    (** Checks a constraint is satisfied (i.e., attempts to prove it) *)
    let sat (x: t) (c: n_cons): bool =
      (* Note: we should delegate this to the sub-memory if needed *)
      Dv.sat x.t_main c
    let set_sat (sc: set_cons) (x: t): bool = Dv.set_sat sc x.t_main
    let seq_sat (sc: seq_cons) (x: t): bool = Dv.seq_sat sc x.t_main

    (** Condition test *)
    let guard ?(no_apron=false) (b: bool) (c: n_cons) (x: t): t =
      (* additional constraints synthesis, when constraints related to
       *  => if the constraint is of the form b+o0=b+o1,
       *  => if there is a sub-memory at base b,
       *     and b+o0=>n0, b+o1=>n1 in the sub-memory, then guard n0=n1 *)
      let x =
        match c with
        | Nc_cons (Tcons1.EQ | Tcons1.DISEQ as rel, e, Ne_csti 0)
        | Nc_cons (Tcons1.EQ | Tcons1.DISEQ as rel, Ne_csti 0, e) ->
            begin
              match Nd_utils.n_expr_decomp_base_off e with
              | None -> x
              | Some (eb, eoff) ->
                  if Bi_fun.mem_dir eb x.t_bases then
                    let sub = addr_2submem x eb in
                    try
                      let n = S.env_find (Offs.of_n_expr eoff) sub in
                      let cc =
                        S.up_n_cons sub (Nc_cons (rel, Ne_var n, Ne_csti 0)) in
                      { x with t_main = Dv.guard ~no_apron b cc x.t_main }
                    with Not_found -> x
                  else x
            end
        | Nc_cons (Tcons1.EQ | Tcons1.DISEQ as rel, e0, e1) ->
            begin
              let d0 = Nd_utils.n_expr_decomp_base_off e0
              and d1 = Nd_utils.n_expr_decomp_base_off e1 in
              match (d0, e0), (d1, e1) with
              | (None, _), (None, _) -> x
              | (None, e), (Some (ba, off), _)
              | (Some (ba, off), _), (None, e) ->
                  if Bi_fun.mem_dir ba x.t_bases then
                    let sub = addr_2submem x ba in
                    match e with
                    | Ne_var _ ->
                        let offs = Offs.of_n_expr off in
                        let lnd = S.env_find offs sub in
                        let cc =
                          Nc_cons (rel, e, S.up_n_expr sub (Ne_var lnd)) in
                        { x with t_main = Dv.guard ~no_apron b cc x.t_main }
                    | _ -> x
                  else x
              | (Some (b0, off0), _), (Some (b1, off1), _) ->
                  if b0 = b1 && Bi_fun.mem_dir b0 x.t_bases then
                    let sub = addr_2submem x b0 in
                    try
                      let f (off: n_expr): sv =
                        S.env_find (Offs.of_n_expr off) sub in
                      let n0 = f off0 and n1 = f off1 in
                      let cc =
                        S.up_n_cons sub
                          (Nc_cons (rel, Ne_var n0, Ne_var n1)) in
                      { x with t_main = Dv.guard ~no_apron b cc x.t_main }
                    with Not_found -> x
                  else x
            end
        | _ -> x in
      (* evaluate the guard in the main value abstraction (always doable) *)
      let x = { x with t_main = Dv.guard ~no_apron b c x.t_main } in
      (* if localizable in a single sub-memory, propagate down *)
      Log.info "locating constraint: %a" n_cons_fpr c;
      let _, osubmem =
        Nd_utils.n_cons_fold
          (fun i ((is_sub, osub) as acc) ->
            if is_sub && sv_to_int i < 0 then
              let mem, _ = glo_2subnode "guard-0" x i in
              if osub = None || osub = Some mem then true, Some mem
              else false, None
            else acc
          ) c (true, None) in
      match osubmem with
      | None -> x
      | Some isub ->
          if !flag_debug_submem then Log.info "located submem %a" sv_fpr isub;
          (* TODO: if there are global nodes, we should give up here !!! *)
          let f i = (* computes if local node; if not, global *)
            if is_subnode x i then snd (glo_2subnode "guard-1" x i) (* local *)
            else i (* global *) in
          let c = Nd_utils.n_cons_map f c in
          let sub = cont_2submem x isub in
          let submem = S.guard c sub in
          { x with t_subs = SvMap.add isub submem x.t_subs }
    let set_guard (sc: set_cons) (x: t): t =
      { x with t_main = Dv.set_guard sc x.t_main }
    let seq_guard (sc: seq_cons) (x: t): t =
      { x with t_main = Dv.seq_guard sc x.t_main }

    (** Assignment *)
    let assign (dst: sv) (ex: n_expr) (x: t): t =
      Log.info "ydest: %b" (may_impact_sv_mod dst x);
      if may_impact_sv_mod dst x then
        begin
          Log.info "impact sv:  %a <= %a \n%a" sv_fpr dst
            n_expr_fpr ex (t_fpri SvMap.empty "  ") x;
          (* drop all information *)
          let x = sv_add dst (sv_rem dst x) in
          Log.info "dropping information about %a" sv_fpr dst;
          let x = { x with t_main = Dv.assign dst ex x.t_main } in
          Log.todo_exn "assign, submem drop:\n%a" (t_fpri SvMap.empty "  ") x
        end
      else if SvMap.mem dst x.t_subs then
        Log.todo_exn "delegate assign"
      else { x with t_main = Dv.assign dst ex x.t_main }
    let write_sub (sd: sub_dest) (size: int) (rv: n_rval) (x: t): t =
      (* extract the sub-memory and the location *)
      let base, cont, off, sub, l_loc =
        match sd with
        | Sd_env (base, off) ->
            let sub = addr_2submem x base in
            let l_loc = (* extraction of the submem node to modify *)
              let osimp = make_osimplifier x in
              match S.env_localize osimp off sub with
              | None -> Log.fatal_exn "write_sub, offset not found in submem"
              | Some l_loc -> l_loc in
            base, S.get_cont sub, off, sub, l_loc
        | Sd_int (inode, o) ->
            let i, j = glo_2subnode "write_sub" x inode in
            let sub = cont_2submem x i in
            Log.info "found %a, %a" sv_fpr i sv_fpr j;
            S.get_addr sub, S.get_cont sub, o, sub, (j, o) in
      let f_debug = false in
      if f_debug then
        Log.info "write called: %a @ {{{%a}}} ::== []"
          sv_fpr base Offs.t_fpr off;
      (* prepare the sub-memory, by adding a node for the new value *)
      let prepare_submem (): t * sv * S.t =
        let x, glo, _, sub = submem_sv_add Nk_contents x cont sub in
        x, glo, sub in
      (* perform a mutation inside the sub-memory
       *  - do the write in the sub-memory
       *  - update the sub-memory table *)
      let perform_mutation lloc (x, nex, sub, main) =
        let nsub = tag_unfold_exn cont (S.write (sat x) lloc nex) sub in
        { x with
          t_subs = SvMap.add cont nsub x.t_subs;
          t_main = main } in
      (* case split on the right hand side *)
      match rv with
      | Rv_addr (n, o) ->
          if f_debug then
            Log.info "write_sub:\n - offset found: %a\n - rv-addr: %a,%a"
              onode_fpr l_loc sv_fpr n Offs.t_fpr o;
          if S.env_mem o sub then Log.fatal_exn "already there"
          else if n = base then
            (* assignment of an address inside the same sub-memory *)
            (* - add a local node for the offset *)
            let x, r_glo, r_loc, sub = submem_sv_add Nk_contents x cont sub in
            let sub, x = sub_env_add_reduce o r_loc sub x in
            if f_debug then
              Log.info "sub-mem, stage 1:\n%a" (S.t_fpri "  ") sub;
            (* does not work for relative offsets ?
             *  ex: 8*i+4  *)
            perform_mutation l_loc (x, Ne_var r_glo, sub, x.t_main)
          else Log.fatal_exn "write_sub: pointer to other external node"
      | Rv_expr ex ->
          if f_debug then
            Log.info "write_sub:\n - offset found: %a\n - expr: %a"
              onode_fpr l_loc Nd_utils.n_expr_fpr ex;
          (* - add a node in the sub-memory if needed
           * - perform numeric assignment if needed *)
          let x, nex, sub, main =
            match ex with
            | Ne_var rglo ->
                if sv_to_int rglo < 0 then
                  (* the mutation is internal to the sub-memory *)
                  x, ex, sub, x.t_main
                else (* the mutation points to outside the sub-memory *)
                  let x, nglo, sub = prepare_submem () in
                  x, Ne_var nglo, sub, Dv.assign nglo ex x.t_main
            | _ ->
                (* proceed to a node creation, and assign in the numerics
                 * with a trivial write in the submemory ? *)
                let x, nglo, sub = prepare_submem () in
                x, Ne_var nglo, sub, Dv.assign nglo ex x.t_main in
          (* perform the mutation *)
          perform_mutation l_loc (x, nex, sub, main)
    (* Simplification of expressions *)
    let simplify_n_expr (x: t) (ex: n_expr): n_expr =
      Dv.simplify_n_expr x.t_main ex

    (** Array operations *)
    let sv_array_add _ = Log.fatal_exn "sv_array_add in subm"
    let sv_array_address_add _ = Log.fatal_exn "sv_array_address_add in subm"
    let is_array_address _ = Log.fatal_exn "is_array_address in subm"
    let sv_array_deref _ = Log.fatal_exn "sv_array_deref in subm"
    let sv_array_materialize _ = Log.fatal_exn "sv_graph_materialize in subm"

    (** Submemory specific functions *)
    (* Checks whether a node is of sub-memory type *)
    let is_submem_address (i: sv) (x: t): bool =
      Bi_fun.mem_dir i x.t_bases
    let is_submem_content (i: sv) (x: t): bool =
      SvMap.mem i x.t_subs
    (* Read of a value inside a submemory block *)
    let submem_read (sat: n_cons -> bool)
        (addr: sv) (* symbolic variable denoting the sub-memory address *)
        (off: Offs.t) (sz: int) (* offset and size *)
        (x: t): Offs.svo =
      let cont = addr_2cont x addr in
      let sub = addr_2submem x addr in
      let osimplifier = make_osimplifier x in
      tag_unfold_exn cont (S.read_sub_base_off osimplifier off sz) sub
    let submem_deref (sat: n_cons -> bool)
        (src: sv) (* symbolic variable denoting the sub-node address *)
        (off: Offs.t) (sz: int) (* offset and size *)
        (x: t): Offs.svo =
      let cont, iintern = glo_2subnode "submem_deref" x src in
      Log.info "deref in submemory of cont (%a,%a) [%a,%d]\n%a" sv_fpr cont
        sv_fpr iintern Offs.t_fpr off sz (t_fpri SvMap.empty "   ") x;
      let sub = cont_2submem x cont in
      tag_unfold_exn cont (S.read_sub_internal iintern off sz) sub
    (* Localization of a node in a sub-memory *)
    let submem_localize (iglo: sv) (x: t): sub_localize =
      let isub, iloc = glo_2subnode "submem_localize" x iglo in
      let sub = cont_2submem x isub in
      match S.localize_offset_of_node iloc sub with
      | Some off -> (* offset found *)
          let ibasis = cont_2addr x isub in
          Log.warn "SUBMEM_LOCALIZE";
          Sl_found (ibasis, off)
      | None ->
          (* If the node corresponds to an address in the sub-memory,
           * allocate an offset for it and bind it *)
          if S.localize_node_in_block iglo sub then Sl_inblock isub
          else Sl_unk (* fail to provide a localization *)
    (* Binding a node to a sub-memory *)
    let submem_bind (isub: sv) (iglo: sv) (o: Offs.t) (x: t): t * Offs.svo =
      let addr = cont_2addr x isub in
      let _, iloc = glo_2subnode "submem_bind" x iglo in
      let sub, x = sub_env_add_reduce o iloc (cont_2submem x isub) x in
      Log.info "@@@@@binding";
      { x with t_subs = SvMap.add isub sub x.t_subs }, (addr, o)
    (* Unfolding *)
    let unfold (cont: sv) (n: sv) (udir: unfold_dir) (x: t)
        : (sv SvMap.t * t) list =
      sanity_check "unfold,in" x;
      let sub = cont_2submem x cont in
      let l = S.unfold (Dv.sat x.t_main) n udir sub in
      List.map
        (fun (nsub, renaming, lcons) ->
          let main =
            List.fold_left (fun acc cons -> Dv.guard true cons acc)
              x.t_main lcons in
          let x =
            sync_addrem { x with
                          t_subs   = SvMap.add cont nsub x.t_subs;
                          t_main   = main } in
          sanity_check "unfold,out" x;
          renaming, x
        ) l
    let expand (id: sv) (nid: sv) (x: t): t =
      Log.todo_exn "expand in subm"
    let compact (lid: sv) (rid: sv) (x: t): t =
      Log.todo_exn "compact in subm"
    let meet (lx: t) (rx: t): t =
      Log.todo_exn "meet in subm"

    (** Unhandled operations on svs (irrelevant for subdom ?) *)
    (* TODO: I think we could add support and propagate to underlying domain *)
    (* Forget the information about an SV *)
    let sv_forget (sv: sv) (x: t): t =
      Log.todo_exn "sv_forget in subm"
    (* Export range information *)
    let sv_bound (sv: sv) (x: t): interval =
      Log.todo_exn "sv_bound in subdom"

    (** Temporaries specific for array *)
    let check (op: vcheck_op) (x: t): bool =
      match op with
      | VC_ind (i, off, iname) ->
          (* retrieval of the sub-memory, and underlying ind_check *)
          let sub = addr_2submem x i in
          S.ind_check (Dv.sat x.t_main) off iname sub
      | _ -> Log.fatal_exn "array check in submem"
    let assume (op: vassume_op) (x: t): t =
      match op with
      | VA_array -> Log.fatal_exn "array assume in submem"
      | _ -> Log.fatal_exn "unexpected assume"

    (** Extract the set of all SVs *)
    let get_svs (x: t): SvSet.t =
      Log.todo_exn "get_sv in dom_valset_array"
    (** Extract all SVs that are equal to a given SV *)
    let get_eq_class (i: sv) (x: t): SvSet.t =
      Log.fatal_exn "get_eq_class in submem"
    (* Get the set of all svs that majorate a given SV *)
    let get_upper_svs (id: sv) (x: t): SvSet.t =
      Log.fatal_exn "get_upper_svs in submem"
    (* Get the set of all svs that majorate a given SV *)
    let get_lower_svs (id: sv) (x: t): SvSet.t =
      Log.fatal_exn "get_lower_svs in submem"
  end: DOM_VALCOL)
