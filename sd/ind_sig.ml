(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_sig.ml
 **       data-types for the inductive definitions.
 ** Xavier Rival, 2011/06/29 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Ind_form_sig
open Vd_sig

open Ind_inst


(** Definitions taken from the instantiation from inductive definitions *)
(* Types from the module exposed *)
type pathstr   = FInd.pathstr
type apath     = FInd.apath
type pformatom = FInd.pformatom
type pform     = FInd.pform
type cell      = FInd.cell
type heapatom  = FInd.heapatom
type hform     = FInd.hform


(** Rules *)
(* Rule kind *)
type ir_kind =
  | Ik_unk                  (* unknown type (no hint available for this rule) *)
  | Ik_empz                 (* empty heap fragment, null ptr *)
  | Ik_range of int * int   (* non null ptr, non empty frag, range [n,m[ *)
(* Rule of an inductive definition *)
type irule =
    { (** Components of the rule *)
      ir_num:  int;           (* number of new SVs *)
      ir_nnum: SvSet.t;       (* number of new int SVs *)
      ir_snum: SvSet.t;       (* number of new Set COLVs *)
      ir_qnum: SvSet.t;       (* number of new Seq COLVs *)
      ir_typ:  ntyp SvMap.t;  (* types of the new variables *)
      ir_heap: hform;         (* heap part *)
      ir_pure: pform;         (* pure part *)
      ir_kind: ir_kind;       (* kind of the rule *)
      (** Elements derived by inductive definition analysis *)
      ir_uptr: IntSet.t;      (* unused ptr parameters *)
      ir_uint: IntSet.t;      (* unused int parameters *)
      ir_uset: IntSet.t;      (* unused set parameters *)
      ir_useq: IntSet.t;      (* unused seq parameters *)
      ir_unone: bool;         (* true iff it uses no parameter at all *)
      ir_cons: aform option;  (* guard condition of this rule *) }


(** General inductive definition properties to be found by analysis *)

(* Whether parameters can be viewed as:
 *  - constant across all calls;
 *  - of additive kind (for integers and sets)
 *    (this kind is very useful on segments) *)
type int_par_rec =
    { ipt_const: bool (* constant over recursive calls *) ;
      ipt_incr:  bool (* increasing: recursive call greater than main call *) ;
      ipt_decr:  bool (* decreasing: recursive call smaller than main call *) ;
      ipt_add:   bool (* additive over recursive calls *) }

(* - const: parameters that never change along chains of calls
 * - mono: parameters that can be weakened by taking superset
 * - add:  parameters with additive monoid properties over segments/inds concat
 * - head: parameters describing the sum of SVs that are origin of same
 *         inductive edges in the whole call unfolding *)
type pars_rec =
    { (* maps each ptr par to a boolean indicating whether constant or not *)
      pr_ptr_const:  bool IntMap.t ;
      (* int parameter field (const/add) *)
      pr_int:        int_par_rec IntMap.t ;
      (* set parameter field (const/head/add) *)
      pr_set:        Set_sig.set_par_type IntMap.t;
      (* seq parameter field (const/head) *)
      pr_seq:        Seq_sig.seq_par_type IntMap.t; }


(** Inductive definitions *)
type ind =
    { (** Components of the definition *)
      i_name:    string;     (* name *)
      i_ppars:   int;        (* number of int parameters *)
      i_ipars:   int;        (* number of ptr parameters *)
      i_spars:   int;        (* number of set parameters *)
      i_qpars:   int;        (* number of seq parameters *)
      i_rules:   irule list; (* rules *)
      i_srules:  irule list; (* subset of i_rules: only for segments *)

      (** Elements that should help the analysis (derived by ind. analysis) *)
      i_mt_rule:  bool;      (* existence of a rule with null ptr, emp heap *)
      i_emp_ipar: int;       (* existence of rule with emp heap and int param *)
      i_reds0:    bool;      (* may a segment with root=0 be reduced to empty *)
      (* size of blocks, if fixed and identical for all rules *)
      i_blksize:  int option;

      (* TODO: are i_dirs and i_may_self_dirs the same or not ? *)
      (* set of "directions" for induction, that is offset that can lead to
       * the next inductive call *)
      i_dirs:    Offs.OffSet.t;
      (* offset which may contain addresses of structures described by the
       * same inductive definition *)
      i_may_self_dirs: Offs.OffSet.t;
      i_pr_pars: IntSet.t;       (* set of parameters for prev fields *)
      i_fl_pars: int IntMap.t;   (* parameter => field storing it *)
      i_pr_offs: Offs.OffSet.t;  (* set of prev fields *)
      i_list:    (int * int) option; (* next off+size for list like defs *)
      (* constraints of inductive definition parameters, such as, i=0, i!=0,
       * this constraints could be used to perform reductions, like,
       *  i=0 ==> E = \emptyset *)
      i_rules_cons:      (aform * aform list * sform list * qform list) list;
      (* behavior of inductive definition parameters:
       *  - ptr parameters may be constant
       *  - int and set parameters may be constant or additive
       * one field describes all rules; the second only empty rules *)
      i_pkind:           pars_rec;
      i_pkind_emp:       pars_rec;
      (* behavior of inductive definition parameters:
       *  - a ptr parameter may reach another ptr parameter following
       * a path *)
      i_ppath:           apath list ;
      (* local unfolding set/seq parameters *)
      i_nonloc_unf_pars: (int * col_kind) list; (* index, set/seq *)
    }

