(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_join.ml
 **       Join for the list domain
 ** Xavier Rival, 2014/03/04 *)
open Data_structures
open Lib
open Sv_def

open Graph_sig (* for graph relation *)
open Ind_sig
open Inst_sig
open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

open Gen_join

open Col_utils
open Ind_utils
open Inst_utils
open List_utils
open Nd_utils
open Set_utils
open Seq_utils
open Sv_utils

open List_inst
open Graph_algos

(** Error report *)
module Log =
    Logger.Make(struct let section = "l_join__" and level = Log_level.DEBUG end)

(** Aborting a rule *)
exception Abort_rule of string

(** Join algorithm state *)
type join_config =
    { (** Iteration strategy *)
      (* Rules that could be applied right away *)
      j_rules:      rules;
      (** Arguments *)
      (* Arguments (left and right) *)
      j_inl:        lmem;
      j_inr:        lmem;
      (* Satisfaction functions for (left|right) argument *)
      j_vsat_l:     vsat;
      j_vsat_r:     vsat;
      (** Relations among arguments *)
      (* Relation between arguments/output nodes *)
      j_rel:        node_relation;
      (* Hint: nodes that should be treated in priority *)
      j_hint:       (sv * sv -> bool);
      (* instantiation constraints, following from is_le facts *)
      j_instset_l:  set_colv_inst;
      j_instset_r:  set_colv_inst;
      j_instseq_l:  seq_colv_inst;
      j_instseq_r:  seq_colv_inst;
      (** Outputs *)
      j_out:        lmem; (* join result *)
    }

(** Pretty-printing of configurations *)
let join_config_fpri ?(pp_rules: bool = false) (ind: string) (msg: string)
    (fmt: form) (j: join_config): unit =
  let nind0 = F.asprintf "  %s" ind in
  let nind1 = F.asprintf "   %s" ind in
  let fpi = lmem_fpri nind0 in
  F.fprintf fmt "%s----------------\n" ind;
  F.fprintf fmt "%s%s:\n%s- L:\n%a%s- R:\n%a%s- O:\n%a%s- M:\n%a" ind msg
    ind fpi j.j_inl ind fpi j.j_inr ind fpi j.j_out
    ind (Graph_utils.Nrel.nr_fpri nind0) j.j_rel;
  F.fprintf fmt "%s- Set instantiation left:\n%a"
    ind (set_colv_inst_fpri nind1) j.j_instset_l;
  F.fprintf fmt "%s- Set instantiation right:\n%a"
    ind (set_colv_inst_fpri nind1) j.j_instset_r;
  F.fprintf fmt "%s- Seq instantiation left:\n%a"
    ind (seq_colv_inst_fpri nind1) j.j_instseq_l;
  F.fprintf fmt "%s- Seq instantiation right:\n%a"
    ind (seq_colv_inst_fpri nind1) j.j_instseq_r;
  if !Flags.flag_dbg_join_strategy && pp_rules then
    F.fprintf fmt "%a" rules_fpr j.j_rules;
  F.fprintf fmt "%s----------------\n" ind

(** Generation of seq parameters to build an inductive predicate *)
let build_ind_args (ld: l_def) (lm: lmem) (ptrargs: sv list option)
  : lmem * l_call =
  let lm, ptr_args =
    match ld.ld_ptr, ptrargs with
    | _ , Some l -> lm, l
    | None, None -> lm, []
    | Some pi, None ->
        List.fold_left
          (fun (lm, args) _ ->
            let i, lm = sv_add_fresh Ntaddr lm in
            lm, i :: args)
          (lm, [ ]) (List.rev pi.p_params) in
  let lm, set_args =
    match ld.ld_set with
    | None -> lm, []
    | Some si ->
        List.fold_left
          (fun (lm, args) k ->
            let i, lm = colv_add_fresh (Ct_set (Some k)) lm in
            lm, i :: args
          ) (lm, [ ]) (List.rev si.s_params) in
  let lm, seq_args =
    match ld.ld_seq with
    | None -> lm, []
    | Some qi ->
        List.fold_left
          (fun (lm, args) k ->
            let i, lm = colv_add_fresh (Ct_seq (Some k)) lm in
            lm, i :: args
          ) (lm, [ ]) (List.rev qi.q_params) in
  let lc = { lc_def = ld ;
             lc_ptrargs = ptr_args ;
             lc_setargs = set_args ;
             lc_seqargs = seq_args } in
  lm, lc
let build_seg_args (ld: l_def) (lm: lmem) (ptrargs: (sv * sv) list option)
  : lmem * l_call * lseg_call =
  let lm, ptr_args, ptr_args_end =
    match ld.ld_ptr, ptrargs with
    | _ , Some l -> lm, List.map fst l, List.map snd l
    | None, None -> lm, [], []
    | Some pi , None ->
        List.fold_left
          (fun (lm, args, args_end) k ->
            let i, lm = sv_add_fresh Ntaddr lm in
            let j, lm = sv_add_fresh Ntaddr lm in
            lm, i::args, j::args_end)
          (lm, [ ], [ ]) (List.rev pi.p_params) in
  let lm, set_args =
    match ld.ld_set with
    | None -> lm, []
    | Some si ->
        List.fold_left
          (fun (lm, args) k ->
            let i, lm = colv_add_fresh (Ct_set (Some k)) lm in
            lm, i :: args
          ) (lm, [ ]) (List.rev si.s_params) in
  let lm, seq_args =
    match ld.ld_seq with
    | None -> lm, []
    | Some qi ->
        List.fold_left
          (fun (lm, args) k ->
            let i, lm = colv_add_fresh (Ct_seq (Some k)) lm in
            lm, i :: args
          ) (lm, [ ]) (List.rev qi.q_params) in
  let lc = { lc_def = ld ;
         lc_ptrargs = ptr_args ;
         lc_setargs = set_args ;
         lc_seqargs = seq_args } in
  let lsegc = { lc_def = ld ;
                lc_ptrargs = ptr_args_end } in
  lm, lc, lsegc

(** Operations on configurations and mappings *)
(* Searches for a pair (sv_l, sv_r) in the configuration; adds it if needed *)
let find_in_config (p: sv * sv) (j: join_config): join_config * sv =
  try j, Graph_utils.Nrel.find j.j_rel p
  with Not_found ->
    let nl = sv_find (fst p) j.j_inl in
    let nr = sv_find (snd p) j.j_inr in
    let t  = ntyp_merge nl.ln_typ nr.ln_typ in
    let ni, nout = sv_add_fresh t j.j_out in
    let ln_odesc =
      match nl.ln_odesc, nr.ln_odesc with
      | None  , None   -> None
      | None  , Some _ -> nr.ln_odesc
      | Some _, None   -> nl.ln_odesc
      | Some l, Some r ->
          if l = r then Some l
          else Log.fatal_exn "find_in_config: ln_odesc not match" in
    let nout =
      let no = sv_find ni nout in
      { nout with
        lm_mem = SvMap.add ni { no with ln_odesc = ln_odesc } nout.lm_mem } in
    let nrel = Graph_utils.Nrel.add j.j_rel p ni in
    { j with
      j_out = nout;
      j_rel = nrel; }, ni

(** Management of applicable rules *)
(* General function *)
let collect_rules_node_gen
    (is_prio: sv * sv -> bool) (* whether pt rules are prioritary (init) *)
    (il: sv) (ir: sv) (* nodes in both inputs *)
    (jc: join_config): join_config =
  let jr =
    collect_rules_sv_gen is_prio false false jc.j_rel
      il (sv_kind il jc.j_inl) ir (sv_kind ir jc.j_inr) jc.j_rules in
  { jc with j_rules = jr }

(* Node collection at some point in the algorithm *)
let collect_rules_node (il: sv) (ir: sv) (jc: join_config)
    : join_config =
  let fun_prio (svl, svr) =
    let node_l = SvMap.find svl jc.j_inl.lm_mem in
    let node_r = SvMap.find svr jc.j_inr.lm_mem in
    node_l.ln_prio && node_r.ln_prio in
  collect_rules_node_gen fun_prio il ir jc
(* Computes the set of rules at initialization *)
let init_rules (jc: join_config): join_config =
  SvPrMap.fold
    (fun (il, ir) _ ->
      collect_rules_node_gen jc.j_hint il ir
    ) jc.j_rel.n_inj jc

(** Unification functions *)
(* We keep the format of the unification functions, to avoid too much
 * divergence from graph_join, but these should be a lot simpler here
 * as all offsets, bounds and sizes are supposed to be constant. *)
let gen_unify (f_is_const: 'a -> bool) (f_eq: 'a -> 'a -> bool)
    (bl: 'a) (br: 'a) (j: join_config): join_config * 'a =
  assert (f_is_const bl && f_is_const br);
  if f_eq bl br then j, bl else raise Stop
let unify_bounds bl br j = gen_unify Bounds.t_is_const Bounds.eq bl br j
let unify_offsets bl br j = gen_unify Offs.t_is_const Offs.eq bl br j
let unify_sizes bl br j = gen_unify Offs.size_is_const Offs.size_eq bl br j

(** Interface over inclusion checking *)
(* For full inductive predicates *)
let n_is_le_ind
    (ld: l_def)                 (* definition to be used *)
    (x: lmem)                   (* shape to compare and weaken *)
    (is: sv)                    (* source *)
    ~(vsat: vsat)               (* satisfiability function *)
    : is_le_res * l_call =
  let xw = sv_add is Ntaddr lmem_empty in
  let xw = { xw with
             lm_nkey    = x.lm_nkey ;
             lm_colvkey = x.lm_colvkey ;
             lm_colvkind = x.lm_colvkind } in
  let xw, lc = build_ind_args ld xw None in
  let xw = list_edge_add is lc xw in
  let inj = Aa_maps.singleton is is in
  let r =
    List_is_le.is_le_weaken_check x SvSet.empty SvSet.empty ~vsat_l:vsat
      xw inj Aa_maps.empty in
  r, lc
(* For segment predicates *)
let n_is_le_lseg
    (lc: l_call)                (* definition to be used *)
    (lsegc: lseg_call)          (* definition to be used *)
    (x: lmem)                   (* shape to compare and weaken *)
    (is: sv) (id: sv)           (* source and destination *)
    (intro: bool)               (* whether it is a segment introduction *)
    ~(vsat: vsat)               (* satisfiability function *)
    : is_le_res * l_call * lseg_call =
  let xw = sv_add is Ntaddr (sv_add id Ntaddr lmem_empty) in
  let xw = { xw with
             (* This line crashes one of the experiments in big_code *)
             (* it is not clear why, TODO, check this !!! *)
             (*lm_nkey    = x.lm_nkey ; *)
             lm_colvkey  = x.lm_colvkey ;
             lm_colvkind = x.lm_colvkind } in
  let added = SvSet.of_list [is; id] in
  let xw, _ =
    List.fold_left
      (fun (xw, added) arg ->
        if SvSet.mem arg added then xw, added
        else sv_add arg Ntaddr xw, SvSet.add arg added)
      (xw, added) (lc.lc_ptrargs @ lsegc.lc_ptrargs) in
  let xw, lc, lsegc = build_seg_args lc.lc_def xw None in
  let xw = lseg_edge_add is id lc lsegc xw in
  let inj = Aa_maps.add id id (Aa_maps.singleton is is) in
  let r =
    List_is_le.is_le_weaken_check x
      (SvSet.singleton is) (* segment begin: no unfold empty from it *)
      (SvSet.singleton id) (* segment end: stop SV *)
      ~vsat_l:vsat xw inj Aa_maps.empty in
  r, lc, lsegc

(** Auxilliary functions supporting graph rewrites *)
(* seperate an inductive into a segment and an inductive edge in the out *)
let sep_ind_out (is: sv) (id: sv) (j: join_config): join_config =
  let n_is = sv_find is j.j_out in
  let n_id = sv_find id j.j_out in
  let aux lc lm =
    let lm, lseg, lsegc, lind, set_cons, seq_cons = split_indpredpars lc lm in
    let set_inst_fix inst =
      Log.info "tmp,prefix:\n%a" (set_colv_inst_fpri "  ") inst;
      let i = inst
        |> jinst_set_addpars_icall lind
        |> jinst_set_addpars_icall lseg
        |> jinst_set_rempars_icall lc  in
      Log.info "tmp,postfix:\n%a" (set_colv_inst_fpri "  ") i;
      { i with colvi_props = set_cons @ i.colvi_props } in
    let seq_inst_fix inst =
      let i = inst
        |> jinst_seq_addpars_icall lind
        |> jinst_seq_addpars_icall lseg
        |> jinst_seq_rempars_icall lc  in
      { i with colvi_props = seq_cons @ i.colvi_props } in
    { j with
      j_out       = lseg_edge_add is id lseg lsegc lm;
      j_instset_l = set_inst_fix j.j_instset_l;
      j_instset_r = set_inst_fix j.j_instset_r;
      j_instseq_l = seq_inst_fix j.j_instseq_l;
      j_instseq_r = seq_inst_fix j.j_instseq_r; }, lind in
  match n_is.ln_e, n_id.ln_e with
  | Lhlist lc, Lhemp ->
      let j, lind = aux lc (list_edge_rem is j.j_out) in
      { j with j_out = list_edge_add id lind j.j_out }
  | Lhlseg (lc, d, lsegc), Lhemp ->
      let j, lind = aux lc (lseg_edge_rem is j.j_out) in
      { j with j_out = lseg_edge_add id d lind lsegc j.j_out }
  | _ -> j

(** Approximate candidate selection:
 *   - tries to find information in the nearby inductive predicates
 *   - if none is available, tries to guess a set of solutions from pt fields;
 *   - fails for empty
 *  Limitation: will not generate any set constraint at this point
 *  (doing so would require guessing conditions, which is not easy at this
 *   stage; this problem would occur in the case of the construction of
 *   structures) *)
let select_list_candidate (i: sv) (lm: List_sig.lmem): l_call list =
  let nl = sv_find i lm in
  let do_ld ld = [ { lc_def  = ld;
                     lc_ptrargs = [] ;
                     lc_setargs = [] ;
                     lc_seqargs = [] } ] in
  match nl.ln_odesc with
  | Some ld -> do_ld ld
  | None ->
      match nl.ln_e with
      | Lhlist lc -> do_ld lc.lc_def
      | Lhlseg (lc, _, lsegc) -> do_ld lc.lc_def
      | Lhpt mc ->
          let lc_of_i i = { ld_name    = None;
                            ld_emp_csti = 0;
                            ld_emp_mcons = [];
                            ld_next_mcons = [];
                            ld_m_offs = [];
                            ld_submem = None;
                            ld_nextoff = i;
                            ld_nextdoff= 0; (* JG: TODO *)
                            ld_prevpar = None ;
                            ld_size    =  Block_frag.byte_size mc;
                            ld_onexts  = [ ];
                            ld_ptr     = None;
                            ld_set     = None;
                            ld_seq     = None  } in
          let ld_of_i i = { lc_def  = lc_of_i i;
                            lc_ptrargs = [ ] ;
                            lc_setargs = [ ] ;
                            lc_seqargs = [ ] } in
          let l1 = List.map fst (Block_frag.map_to_list mc) in
          let loffs = List.map (fun b -> Offs.to_int (Bounds.to_offs b)) l1 in
          List.map ld_of_i loffs
      | Lhemp ->
          [ ] (* no solution *)

(** Join rules *)
(* pt-pt *)
let apply_pt_pt
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = sv_find isl j.j_inl in
  let nr = sv_find isr j.j_inr in
  assert (nl.ln_odesc = nr.ln_odesc);
  match nl.ln_e, nr.ln_e with
  | Lhpt mcl, Lhpt mcr ->
      let sz_l = Block_frag.cardinal mcl and sz_r = Block_frag.cardinal mcr in
      let j =
        if sz_l != sz_r then Log.todo_exn "pt-pt; !="
        else
          Block_frag.fold_list2_base
            (fun osl osr (pl: pt_edge) (pr: pt_edge) j ->
              let j, u_os = unify_bounds osl osr j in
              let j, u_size = unify_sizes pl.pe_size pr.pe_size j in
              let dl_n, dl_o = pl.pe_dest
              and dr_n, dr_o = pr.pe_dest in
              let j, dd_o = unify_offsets dl_o dr_o j in
              let j, id = find_in_config (dl_n, dr_n) j in
              let pe = { pe_size = u_size ;
                         pe_dest = id, dd_o } in
              collect_rules_node dl_n dr_n
                { j with
                  j_out = pt_edge_block_append (is, u_os) pe j.j_out }
            ) mcl mcr j in
      let vrules = invalidate_rules isl isr Ipt Ipt j.j_rules in
      { j with
        j_inl   = pt_edge_block_destroy ~remrc:true isl j.j_inl;
        j_inr   = pt_edge_block_destroy ~remrc:true isr j.j_inr;
        j_rules = vrules; }
  | _, _ -> Log.fatal_exn "pt-pt: improper case"

(* ind-ind [par ptr OK, par int OK]
 *   consume edges and produce a matching one *)
let apply_ind_ind
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = sv_find isl j.j_inl in
  let nr = sv_find isr j.j_inr in
  match nl.ln_e, nr.ln_e with
  | Lhlist ldl, Lhlist ldr ->
      if l_call_compare ldl ldr = 0 then
        let j, ptrargs =
          List.fold_left2
            (fun (j, ptrargs) ptr_l ptr_r ->
              let j, ptr_o = find_in_config (ptr_l, ptr_r) j in
              j, ptr_o :: ptrargs)
            (j, []) (List.rev ldl.lc_ptrargs) (List.rev ldr.lc_ptrargs) in
        let out, ldo = build_ind_args ldl.lc_def j.j_out (Some ptrargs) in
        let vrules = invalidate_rules isl isr Iind Iind j.j_rules in
        { j with
          j_rules     = vrules;
          j_inl       = list_edge_rem isl j.j_inl;
          j_inr       = list_edge_rem isr j.j_inr;
          j_out       = list_edge_add is ldo out;
          j_instset_l = jinst_set_addcstr_icall_eqs ldl ldo j.j_instset_l;
          j_instset_r = jinst_set_addcstr_icall_eqs ldr ldo j.j_instset_r;
          j_instseq_l = jinst_seq_addcstr_icall_eqs ldl ldo j.j_instseq_l;
          j_instseq_r = jinst_seq_addcstr_icall_eqs ldr ldo j.j_instseq_r;
        }
      else Log.fatal_exn "join: inductive edges fails"
  | _, _ -> Log.fatal_exn "ind-ind: improper case"

(* ind-weak: weakening the right side to match an inductive in the left side
 * weak-ind: weakening the left side to match an inductive in the right side *)
let apply_gen_ind_weak
    (side: side) (* side of the rule (side being weakened) *)
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  (* "s": side argument used as a guide
   * "o": other argument being weakened *)
  let rname = sel_side side ("ind-weak", "weak-ind") in
  let iso, iss = get_sides side (isl, isr) in
  let ino, ins = get_sides side (j.j_inl, j.j_inr) in
  (* r -> side; l -> other side *)
  let ns = sv_find iss ins in
  match ns.ln_e with
  | Lhlist lds ->
      let out, lcout = build_ind_args lds.lc_def j.j_out None in
      let vsato, _ = get_sides side (j.j_vsat_l, j.j_vsat_r) in
      let instseto, instsets = get_sides side (j.j_instset_l, j.j_instset_r) in
      let instseqo, instseqs = get_sides side (j.j_instseq_l, j.j_instseq_r) in
      let le_res, lcwk = n_is_le_ind lds.lc_def ino iso ~vsat:vsato in
      begin
        match le_res with
        | `Ilr_le (ilrem, _) ->
            (* the rule succeeds; perform the weakening *)
            let setvemb, seqvemb = colv_embedding_split ilrem.ilr_colv_rtol in
            let set_cons_o =
              List_inst.l_jinst_set_addctrs_post_isle ~msg:"indwk"
                (setv_type ino) lcwk lcout ilrem.ilr_sv_rtol setvemb
                ilrem.ilr_setctr_r instseto in
            let seq_cons_o =
              List_inst.l_jinst_seq_addctrs_post_isle ~msg:"indwk"
                (seqv_type ino) lcwk lcout ilrem.ilr_sv_rtol seqvemb
                ilrem.ilr_seqctr_r instseqo in
            let set_cons_s =
              jinst_set_addcstr_icall_eqs lds lcout instsets in
            let seq_cons_s =
              jinst_seq_addcstr_icall_eqs lds lcout instseqs in
            let j =
              match side with
              | Lft ->
                  let vrules =
                    invalidate_rules isl isr (Iblob (SvSet.singleton isl))
                      (Iblob ilrem.ilr_svrem_l) j.j_rules in
                  { j with
                    j_rules     = vrules;
                    j_inl       = list_edge_rem isl j.j_inl;
                    j_inr       = ilrem.ilr_lmem_l;
                    j_instset_l = set_cons_s;
                    j_instset_r = set_cons_o;
                    j_instseq_l = seq_cons_s;
                    j_instseq_r = seq_cons_o; }
              | Rgh ->
                  let vrules =
                    invalidate_rules isl isr (Iblob ilrem.ilr_svrem_l)
                      (Iblob (SvSet.singleton isr)) j.j_rules in
                  { j with
                    j_rules     = vrules;
                    j_inl       = ilrem.ilr_lmem_l;
                    j_inr       = list_edge_rem isr j.j_inr;
                    j_instset_l = set_cons_o;
                    j_instset_r = set_cons_s;
                    j_instseq_l = seq_cons_o;
                    j_instseq_r = seq_cons_s; } in
            { j with j_out = list_edge_add is lcout out; }
        | `Ilr_not_le -> (* rule application failed *)
            if Flags.flag_join_bypass_fail_rules then j (* Rule by-pass *)
            else Log.fatal_exn "%s: fails" rname
      end
  | _ -> j
let apply_weak_ind is isl isr j = apply_gen_ind_weak Rgh is isl isr j
let apply_ind_weak is isl isr j = apply_gen_ind_weak Lft is isl isr j

(* seg-intro *)
let apply_gen_seg_intro
    (side: side) (* side of the rule, i.e., where the empty chunk is found *)
    (is: sv)  (* source SV in the target graph *)
    (isl: sv) (* source SV in the left graph *)
    (isr: sv) (* source SV in the right graph *)
    (j: join_config): join_config =
  (* "s": empty segment introduced
   * "o": weakens a chunk of a graph into a segment *)
  let rname    = sel_side side ("seg-intro-l", "seg-intro-r") in
  let iso, iss = get_sides side (isl, isr) in
  let cho, chs = get_sides side ('l', 'r') in
  let ino, ins = get_sides side (j.j_inl, j.j_inr) in
  let vsato, _ = get_sides side (j.j_vsat_l, j.j_vsat_r) in
  (* rule abortion procedure *)
  let rule_abort (j: join_config): join_config =
    Log.info "tmp,aborting rule, split rule becomes active\n";
    match side with (* add facility to split rules *)
    | Lft -> { j with j_rules = rules_add_splitind_l (isl, isr) j.j_rules }
    | Rgh -> { j with j_rules = rules_add_splitind_r (isl, isr) j.j_rules } in
  (* extract other siblings *)
  let allsibl = sel_side side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
  let siblings = SvMap.find_err "siblings incons, seg_intro" iss allsibl in
  assert (SvSet.mem iso siblings);
  let siblings = SvSet.remove iso siblings in
  (* destination node in the other side *)
  let ido =
    let card = SvSet.cardinal siblings in
    if card = 0 then Log.fatal_exn "no sibling";
    let n = SvSet.min_elt siblings in
    if card > 1 then
      Log.warn "%s found too many siblings: %a ; picking %a\n"
        rname svset_fpr siblings sv_fpr n;
    n in
  if !Flags.flag_dbg_join_list then
    Log.info "%s attempt: S%c:%a -> (S%c:%a,D%c:%a)" rname chs sv_fpr iss
      cho sv_fpr iso cho sv_fpr ido;
  (* destination node in the result *)
  let id =
    let pd = get_sides side (ido, iss) in
    SvPrMap.find_err "seg_intro:pair" pd j.j_rel.n_inj in
  let lds = select_list_candidate iss ins in
  let lds = select_list_candidate iso ino @ lds in
  (* tries to apply the rule using a given list description *)
  let do_ld (ld: l_call) =
    if !Flags.flag_dbg_join_list then
      Log.info "%s_inductive_definition: %s" rname
        (match ld.lc_def.ld_name with None -> "list" | Some s -> s);
    let out, lcout, lsegcout = build_seg_args ld.lc_def ino None in
    (* check the segment inclusion for the "other" side *)
    let instseto, _ = get_sides side (j.j_instset_l, j.j_instset_r) in
    let instseqo, _ = get_sides side (j.j_instseq_l, j.j_instseq_r) in
    if !Flags.flag_dbg_join_list then
      Log.info "instset:\n%a" (set_colv_inst_fpri "  ") instseto ;
    let le_res_o, lcwk, lsegcwk =
      n_is_le_lseg lcout lsegcout ino iso ido true ~vsat:vsato in
    (* for the other side, we generate a segment edge, and set the additive
     * set parameters to empty, this keeps the soundness (sinfo def. below) *)
    match le_res_o with
    | `Ilr_le (ilrem, _) ->
        (* We construct the ptr arguments *)
        let j, ptrargs =
          let find_in_injo ptr = SvMap.find ptr ilrem.ilr_sv_rtol in
          let ptrargs_src = List.map find_in_injo lcwk.lc_ptrargs in
          let ptrargs_dst = List.map find_in_injo lsegcwk.lc_ptrargs in
          let find_in_rel (j, acc, i) iaso iado =
            let set = SvSet.of_list [iaso; iado] in
            let candidates =
              Graph_utils.Nrel.mult_bind_candidates side j.j_rel set in
            if !Flags.flag_dbg_join_list then
              Log.info "Par =.(%a)==.(%a)=> candidates: %a (set %a)"
                sv_fpr iaso sv_fpr iado svset_fpr candidates svset_fpr set;
            let card = SvSet.cardinal candidates in
            let par =
              if card = 1 then
                SvSet.min_elt candidates
              else if card = 0 then
                (* Backup solution:
                 * see if there is an inductive edge in ins at node iss
                 * if so, try to "guess" the parameter there, to be used
                 * for both ends of the (empty) segment being
                 * introduced *)
                let ns = sv_find iss ins in
                match ns.ln_e with
                | Lhlist lc
                | Lhlseg (lc, _, _) ->
                    List.nth lc.lc_ptrargs i
                | _ ->
                    Log.todo_exn
                      "seg-intro add heuristics for points-to and empty cases"
              else
                Log.fatal_exn
                  "seg-intro: too many or not enough candidates" in
            let find_out j iao par = match side with
              | Lft -> find_in_config (par, iao) j
              | Rgh -> find_in_config (iao, par) j in
            let j, outs = find_out j iaso par in
            let j, outd = find_out j iado par in
            (j, (outs, outd)::acc, i+1) in
          let j, ptrargs, _ =
            List.fold_left2 find_in_rel (j, [], 0) ptrargs_src ptrargs_dst in
          j, ptrargs in
        let out, lcout, lsegcout =
          build_seg_args ld.lc_def j.j_out (Some ptrargs) in
        let vrules =
          match side with
          | Lft ->
              invalidate_rules isl isr Isiblings Inone
                (invalidate_rules isl ido Inone (Iblob ilrem.ilr_svrem_l)
                   j.j_rules)
          | Rgh ->
              invalidate_rules isl isr Inone Isiblings
                (invalidate_rules ido isr (Iblob ilrem.ilr_svrem_l) Inone
                   j.j_rules) in
        let setvemb, seqvemb = colv_embedding_split ilrem.ilr_colv_rtol in
        let setcons_o =
          List_inst.l_jinst_set_addctrs_post_isle ~msg:"segint"
            (setv_type ino) lcwk lcout ilrem.ilr_sv_rtol setvemb
            ilrem.ilr_setctr_r instseto in
        let seqcons_o =
          List_inst.l_jinst_seq_addctrs_post_isle ~msg:"segint"
            (seqv_type ino) lcwk lcout ilrem.ilr_sv_rtol seqvemb
            ilrem.ilr_seqctr_r instseqo in
        let instset_s =
          List_inst.l_jinst_set_addcstr_segemp lcout
            (sel_side side (j.j_instset_l, j.j_instset_r)) in
        let instseq_s =
          List_inst.l_jinst_seq_addcstr_segemp lcout
            (sel_side side (j.j_instseq_l, j.j_instseq_r)) in
        let j =
          match side with
          | Lft ->
              { j with
                j_inr        = ilrem.ilr_lmem_l;
                j_instset_r  = setcons_o;
                j_instset_l  = instset_s;
                j_instseq_r  = seqcons_o;
                j_instseq_l  = instseq_s }
          | Rgh ->
              { j with
                j_inl        = ilrem.ilr_lmem_l;
                j_instset_l  = setcons_o;
                j_instset_r  = instset_s;
                j_instseq_l  = seqcons_o;
                j_instseq_r  = instseq_s }
        in
        let fi_set = (* guess inst from neighbours (constant parameters) *)
          jinst_set_guesscstr_segemp id lcout j.j_out in
        let fi_seq = (* same as above, but this function does nothing *)
          jinst_seq_guesscstr_segemp id lcout j.j_out in
        { j with
          j_rules = vrules;
          j_out   = lseg_edge_add is id lcout lsegcout out;
          j_instset_r  = fi_set j.j_instset_r;
          j_instset_l  = fi_set j.j_instset_l;
          j_instseq_r  = fi_seq j.j_instseq_r;
          j_instseq_l  = fi_seq j.j_instseq_l; }
    | _ -> rule_abort j in
  (* TODO:
   * since the rule above does abort when it fails, this function never
   * does any recursive call; this means that, when several inductive
   * candidates are found only the first one may be considered. *)
  let rec do_lds = function
    | [ ] -> rule_abort j
    | ld :: lds -> let jj = do_ld ld in if j == jj then do_lds lds else jj in
  do_lds lds
let apply_seg_intro_l is isl isr j = apply_gen_seg_intro Lft is isl isr j
let apply_seg_intro_r is isl isr j = apply_gen_seg_intro Rgh is isl isr j

(* seg-ext *)
(* => auxilliary function for the double seg rule case *)
let aux_seg_ext_both_sides
    ~(is: sv)
    ~(isl: sv) (* source node in the left graph *)
    ~(isr: sv) (* source node in the right graph *)
    ~(id: sv)
    ~(idr: sv)
    ~(lcout: l_call)
    ~(out: lmem)
    ~(ldl: l_call)
    (j: join_config): join_config =
  (* standard is_le_seg failed so we attempt to weaken chunks to segments in
   * both sides of the join and to continue with remainders in both sides;
   * thus we let inclusion check guess the end point of segments *)
  if !Flags.flag_dbg_join_list then
    Log.info "seg-ext++, start %a" nsv_fpr id;
  let is_le_l, lcwkl = n_is_le_ind ldl.lc_def j.j_inl isl ~vsat:j.j_vsat_l in
  if !Flags.flag_dbg_join_list then
    Log.info "seg-ext++, islehalfway";
  let is_le_r, lcwkr = n_is_le_ind ldl.lc_def j.j_inr isr ~vsat:j.j_vsat_r in
  match is_le_l, is_le_r with
  | `Ilr_le (ilreml, _),`Ilr_le (ilremr, _) ->
      if !Flags.flag_dbg_join_list then
        Log.info "seg-ext++A:\n is: %a\n id: %a\n setl: %a\n setr: %a\n sta:\n%a"
          nsv_fpr is nsv_fpr id
          (gen_list_fpr "" set_cons_fpr " /\\ ") ilreml.ilr_setctr_r
          (gen_list_fpr "" set_cons_fpr " /\\ ") ilremr.ilr_setctr_r
          (join_config_fpri "   " "Status") j;
      let setvembl, seqvembl = colv_embedding_split ilreml.ilr_colv_rtol in
      let setvembr, seqvembr = colv_embedding_split ilremr.ilr_colv_rtol in
      let set_cons_l =
        List_inst.l_jinst_set_addctrs_post_isle ~msg:"seg-ext++l"
          (setv_type j.j_inl) lcwkl lcout ilreml.ilr_sv_rtol setvembl
          ilreml.ilr_setctr_r j.j_instset_l in
      let set_cons_r =
        List_inst.l_jinst_set_addctrs_post_isle ~msg:"seg-ext++r"
          (setv_type j.j_inr) lcwkr lcout ilremr.ilr_sv_rtol setvembr
          ilremr.ilr_setctr_r j.j_instset_r in
      let seq_cons_l =
        List_inst.l_jinst_seq_addctrs_post_isle ~msg:"seg-ext++l"
          (seqv_type j.j_inl) lcwkl lcout ilreml.ilr_sv_rtol seqvembl
          ilreml.ilr_seqctr_r j.j_instseq_l in
      let seq_cons_r =
        List_inst.l_jinst_seq_addctrs_post_isle ~msg:"seg-ext++r"
          (seqv_type j.j_inr) lcwkr lcout ilremr.ilr_sv_rtol seqvembr
          ilremr.ilr_seqctr_r j.j_instseq_r in
      let vrules =
        invalidate_rules isl isr (Iblob (SvSet.singleton isl))
          (Iblob ilremr.ilr_svrem_l) j.j_rules in
      let fi_set = (* guess inst *)
        jinst_set_guesscstr_segemp id lcout j.j_out in
      let fi_seq = (* as above, but this function does nothing *)
        jinst_seq_guesscstr_segemp id lcout j.j_out in
      let j = { j with
                j_rules     = vrules;
                j_inl       = ilreml.ilr_lmem_l;
                j_inr       = ilremr.ilr_lmem_l;
                j_out       = list_edge_add is lcout out;
                j_instset_l = fi_set set_cons_l;
                j_instset_r = fi_set set_cons_r;
                j_instseq_l = fi_seq seq_cons_l;
                j_instseq_r = fi_seq seq_cons_r; } in
      if !Flags.flag_dbg_join_list then
        Log.info "seg-ext++,midpoint:\n%a\n" (join_config_fpri "   " "Status") j;
      let j =
        (* TODO: incorrect code; it should be done only for head parameters *)
        let c0 = Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, Ne_var idr) in
        if !Flags.flag_dbg_join_list then
          Log.info "tmp,vsat_r: %b" (j.j_vsat_r.vs_num c0);
        if j.j_vsat_r.vs_num c0 then j
        else
          let np_r =
            match lcout.lc_def.ld_set with
            | None -> Log.fatal_exn "seg_ext fails"
            | Some set_r ->
                match set_r.s_uf_seg with
                | None -> Log.fatal_exn "seg_ext fails"
                | Some uf ->
                    try List.nth lcout.lc_setargs uf
                    with _ -> Log.fatal_exn "seg_ext: uf_find" in
          let colvi_prove =
            S_mem (idr, S_var np_r) :: j.j_instset_r.colvi_prove in
          let instr = { j.j_instset_r with colvi_prove } in
          (* This last bit is surprising : ut since there is no
           * membership connector in seq domain we cannot handle it
           * JG:TODO ask XR ! *)
          { j with j_instset_r = instr } in
      sep_ind_out is id j
  | _ -> Log.fatal_exn "seg_ext fails"
let apply_seg_ext
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = sv_find isl j.j_inl in
  match nl.ln_e with
  | Lhlseg (ldl, idl, lsegl) ->
      (* extract a "tentative" destination in the right side *)
      let idr, guessed =
        let s = SvMap.find_val SvSet.empty idl j.j_rel.n_l2r in
        if !Flags.flag_dbg_join_shape then
          Log.info "set found: %a" svset_fpr s;
        let card = SvSet.cardinal s in
        if card = 1 then SvSet.min_elt s, false
        else
          (* Backup solution:
           * - if card > 1,
           *    see if there is a segment in the right side, and if so
           *    look if its destination works as well
           * - if card = 0,
           *    see if there is a segment in the right side and use it... *)
          let nr = sv_find isr j.j_inr in
          match nr.ln_e with
          | Lhlseg (ldr, idr, lsegr) ->
              if SvSet.mem idr s then idr, false
              else if card = 0 then
                begin (* no other solution anyway *)
                  Log.warn "seg-ext => next segment (not sure about end-point)";
                  idr, true
                end
              else Log.fatal_exn "seg-ext backup failed (seg)"
          | _ -> Log.fatal_exn "seg-ext backup failed (other)" in
      begin
        let out, lcout, lsegcout = build_seg_args ldl.lc_def j.j_out None in
        let j, id = find_in_config (idl, idr) j in
        let is_le, lcwk, lsegcwk =
          n_is_le_lseg lcout lsegcout j.j_inr isr idr false ~vsat:j.j_vsat_r in
        if !Flags.flag_dbg_join_list then
          Log.info "%s Inclusion test DONE" __LOC__;
        match is_le with
        | `Ilr_le (ilrem, _) ->
            let j, ptrargs =
              (* We find the "real" ptr_arguments the following way :
               * - For each arguments [ptr_l] in the list/seg call, we find
               * the sv [ptr_r] matching with it during the inclusion test
               * - Using [ptr_l, ptr_r], we find [ptr_o] matching the two
               * arguments.
               * *)
              let find ptr = SvMap.find ptr ilrem.ilr_sv_rtol in
              let handle_args j args args_out =
                List.fold_left2
                  (fun (j, args) ptr_l ptr_wk ->
                    let ptr_r = find ptr_wk in
                    let j, ptr_o = find_in_config (ptr_l, ptr_r) j in
                    j, ptr_o::args)
                  (j, []) args args_out in
              let j, args_src =
                handle_args j ldl.lc_ptrargs lcwk.lc_ptrargs in
              let j, args_dst =
                handle_args j lsegl.lc_ptrargs lsegcwk.lc_ptrargs in
              j, List.map2 (fun x y -> x, y) args_src args_dst in
            let out, lcout, lsegcout =
              build_seg_args ldl.lc_def j.j_out (Some ptrargs) in
            let out = lseg_edge_add is id lcout lsegcout out in
            let setvemb, seqvemb = colv_embedding_split ilrem.ilr_colv_rtol in
            let set_cons_r =
              List_inst.l_jinst_set_addctrs_post_isle ~msg:"segext"
                (setv_type j.j_inr) lcwk lcout ilrem.ilr_sv_rtol setvemb
                ilrem.ilr_setctr_r j.j_instset_r in
            let seq_cons_r =
              List_inst.l_jinst_seq_addctrs_post_isle ~msg:"segext"
                (seqv_type j.j_inr) lcwk lcout ilrem.ilr_sv_rtol seqvemb
                ilrem.ilr_seqctr_r j.j_instseq_r in
            let set_cons_l =
              jinst_set_addcstr_icall_eqs ldl lcout j.j_instset_l in
            let seq_cons_l =
              jinst_seq_addcstr_icall_eqs ldl lcout j.j_instseq_l in
            let j =
              if guessed then collect_rules_node idl idr j
              else j in
            let vrules =
              invalidate_rules isl isr (Iblob (SvSet.singleton isl))
                (Iblob ilrem.ilr_svrem_l) j.j_rules in
            let fi_set = (* guess inst *)
              jinst_set_guesscstr_segemp id lcout j.j_out in
            let fi_seq = (* guess inst, but this function does nothing *)
              jinst_seq_guesscstr_segemp id lcout j.j_out in
            { j with
              j_rules      = vrules;
              j_inl        = lseg_edge_rem isl j.j_inl;
              j_inr        = ilrem.ilr_lmem_l;
              j_out        = out;
              j_instset_l  = fi_set set_cons_l;
              j_instset_r  = fi_set set_cons_r;
              j_instseq_l  = fi_seq seq_cons_l;
              j_instseq_r  = fi_seq seq_cons_r }
        | `Ilr_not_le ->
            aux_seg_ext_both_sides ~is ~isl ~isr ~id ~idr ~lcout ~out ~ldl j
      end
  | _ -> (* we may check the rule was not invalidated ? *)
      Log.fatal_exn "seg-ext: improper case"

(* list-intro *)
let apply_list_intro
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = sv_find isl j.j_inl in
  let nr = sv_find isr j.j_inr in
  match nl.ln_e, nr.ln_e with
  | Lhemp, Lhpt mcr ->
      (* 1. search for candidate inductive definitions *)
      let lds = select_list_candidate isr j.j_inr in
      (* 2. verify that inclusion holds in both sides *)
      let do_ld (ld: l_call) =
        if !Flags.flag_dbg_join_list then
          Log.info "list-intro_inductive_definition: %s"
            (Option.value ~default:"list" ld.lc_def.ld_name);
        let out, lcout = build_ind_args ld.lc_def j.j_out None in
        let le_res_r, lcwk_r =
          n_is_le_ind ld.lc_def j.j_inr isr ~vsat:j.j_vsat_r in
        let le_res_l, lcwk_l =
          n_is_le_ind ld.lc_def j.j_inl isl ~vsat:j.j_vsat_l in
        match le_res_l,  le_res_r with
        | `Ilr_le (ilreml, _), `Ilr_le (ilremr, _) ->
            let setvembl, seqvembl = colv_embedding_split ilreml.ilr_colv_rtol in
            let setvembr, seqvembr = colv_embedding_split ilremr.ilr_colv_rtol in
            let setcons_l =
              List_inst.l_jinst_set_addctrs_post_isle ~msg:"lint,l"
                (setv_type j.j_inl) lcwk_l lcout ilreml.ilr_sv_rtol setvembl
                ilreml.ilr_setctr_r j.j_instset_l in
            let setcons_r =
              List_inst.l_jinst_set_addctrs_post_isle ~msg:"lint,r"
                (setv_type j.j_inl) lcwk_r lcout ilremr.ilr_sv_rtol setvembr
                ilremr.ilr_setctr_r j.j_instset_r in
            let seqcons_l =
              List_inst.l_jinst_seq_addctrs_post_isle ~msg:"lint,l"
                (seqv_type j.j_inl) lcwk_l lcout ilreml.ilr_sv_rtol seqvembl
                ilreml.ilr_seqctr_r j.j_instseq_l in
            let seqcons_r =
              List_inst.l_jinst_seq_addctrs_post_isle ~msg:"lint,r"
                (seqv_type j.j_inl) lcwk_r lcout ilremr.ilr_sv_rtol seqvembr
                ilremr.ilr_seqctr_r j.j_instseq_r in
            let vrules =
              invalidate_rules isl isr (Iblob (SvSet.singleton isl))
                (Iblob ilremr.ilr_svrem_l) j.j_rules in
            { j with
              j_rules      = vrules;
              j_inl        = ilreml.ilr_lmem_l;
              j_inr        = ilremr.ilr_lmem_l;
              j_out        = list_edge_add is lcout out;
              j_instset_l  = setcons_l;
              j_instset_r  = setcons_r;
              j_instseq_l  = seqcons_l;
              j_instseq_r  = seqcons_r; }
        | _ ->
            Log.warn "list-intro: verification of inclusion failed\n";
            j in
      let rec do_lds = function
        | [ ] -> j
        | ind :: inds ->
            let jj = do_ld ind in if j == jj then do_lds inds else jj in
      do_lds lds
  | Lhemp, Lhemp ->
      j (* another rule was applied before, we can abort that one *)
  | _, _ ->
      Log.fatal_exn "indintro: improper case"

(* seperate inductive edge into a segment and an inductive edge *)
let apply_gen_sep_ind (side: side) (is: sv) (isl: sv) (isr: sv)
    (j: join_config): join_config =
  (* [XR]: while reading this rule again, I cannot understand it well
   *  - what are the roles of "s" (side) and "o" (other side) ?
   *  - inl, and inr are not modified;
   *  - but the satisfaction function "sato" is called...
   *)
  (* side resolution:
   *  - "l" -> "s"
   *  - "r" -> "o" *)
  if !Flags.flag_dbg_join_list then
    Log.info "tmp,Segment splitting rule";
  let iso, iss = get_sides side (isl, isr) in
  let cho, chs = get_sides side ('l', 'r') in
  let vsato, _ = get_sides side (j.j_vsat_l, j.j_vsat_r) in
  (* destination node in the other side *)
  let ido =
    let allsibl = sel_side side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
    calc_split_ind_dest_o allsibl iso iss in
  if !Flags.flag_dbg_join_list then
    Log.info "sep-ind attempt: S%c:%a -> (S%c:%a,D%c:%a)"
      chs nsv_fpr iss cho nsv_fpr iso cho nsv_fpr ido;
  (* destination node in the result *)
  let id =
    let pd = get_sides side (ido, iss) in
    SvPrMap.find_err "sep_ind:pair" pd j.j_rel.n_inj in
  let nl_id, nl_is = sv_find id j.j_out, sv_find is j.j_out in
  match nl_id.ln_e, nl_is.ln_e with
  | Lhemp, Lhlist l_call ->
      (* having inductive edge from source node *)
      begin
        match l_call.lc_def.ld_set with
        | None -> j
        | Some set ->
            match set.s_uf_seg with
            | None -> j
            | Some n ->
                try
                  let np = List.nth l_call.lc_setargs n in
                  let c = Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, Ne_var ido) in
                  if !Flags.flag_dbg_join_list then
                    Log.info "tmp,split-ind,null test: %b\n" (vsato.vs_num c);
                  if vsato.vs_num c then
                    (* This case corresponds to a split with ido at the end *)
                    begin
                      if !Flags.flag_dbg_join_list then
                        Log.info "tmp,Splitting, null node";
                      sep_ind_out is id j
                    end
                  else
                    let f inst =
                      let inst =
                        match side with
                        | Lft -> j.j_instset_r
                        | Rgh -> j.j_instset_l in
                      try
                        let c = S_mem (ido, SvMap.find np inst.colvi_eqs) in
                        { inst with colvi_prove = c :: inst.colvi_prove }
                      with
                      | Not_found -> inst in
                    let j =
                      match side with
                      | Lft -> { j with j_instset_r = f j.j_instset_r }
                      | Rgh -> { j with j_instset_l = f j.j_instset_l } in
                    sep_ind_out is id j
                with Invalid_argument _ ->
                  Log.fatal_exn "gen_sep_ind: parameter does not match in ind"
      end
  | _, _ -> j

let apply_sep_ind_l is isl isr j = apply_gen_sep_ind Lft is isl isr j
let apply_sep_ind_r is isl isr j = apply_gen_sep_ind Rgh is isl isr j

(** The recursive join function *)
let rec rec_join (jc: join_config): join_config =
  (* Find the next rule to apply, and trigger it *)
  match rules_next jc.j_rules with
  | None -> (* there is no more rule to apply, so we return current config *)
      if !Flags.flag_dbg_join_list then
        Log.info "no more rule applies;\n%a" rules_fpr jc.j_rules;
      jc
  | Some (k, (il, ir), rem_rules) ->
      if !Flags.flag_dbg_join_list then
        Log.info "%a" (join_config_fpri ~pp_rules:true "" "J-situation") jc;
      let jc = { jc with j_rules = rem_rules } in
      let is = SvPrMap.find_err "rec_join:pair" (il, ir) jc.j_rel.n_inj in
      if !Flags.flag_dbg_join_list then
        Log.info "Join-Treating (%a,%a) (%a)" sv_fpr il sv_fpr ir rkind_fpr k;
      let nj =
        try
          match k with
          | Rpp           -> apply_pt_pt       is il ir jc
          | Rii           -> apply_ind_ind     is il ir jc
          | Rweaki        -> apply_weak_ind    is il ir jc
          | Riweak        -> apply_ind_weak    is il ir jc
          | Rsegext       -> apply_seg_ext     is il ir jc
          | Rsegintro Lft -> apply_seg_intro_l is il ir jc
          | Rsegintro Rgh -> apply_seg_intro_r is il ir jc
          | Rindintro     -> apply_list_intro  is il ir jc
          | Rsplitind Lft -> apply_sep_ind_l   is il ir jc
          | Rsplitind Rgh -> apply_sep_ind_r   is il ir jc
          | Rsegext_ext   -> Log.todo_exn "Seg-ext rule"
        with
        | Abort_rule _ -> jc in
      rec_join nj

(** The main list join function *)
let join
    (xl: lmem)                (* left graph *)
    ~(vsatl:vsat)   (* satisfiability function, left argument *)
    (xr: lmem)                (* right graph *)
    ~(vsatr:vsat)   (* satisfiability function, right argument *)
    (ho: hint_bg option)      (* optional hint *)
    (r: node_relation)        (* initial node relation *)
    (srel: node_relation)     (* initial set var relation *)
    (noprio: bool)            (* whether to NOT make roots prioretary *)
    (xo: lmem)                (* pre-constructed output *)
    : join_output =
  let h =
    match noprio, ho with
    | true, _ -> fun _ -> false
    | false, None -> fun _ -> true
    | false, Some s ->
        let prio_nodes =
          let f_ind_args (i: sv) (lc: l_call) (lsegc: lseg_call): unit =
            if List.mem i lc.lc_ptrargs || List.mem i lsegc.lc_ptrargs then
              raise Stop in
          let f_pred_node (i: sv) (m: lmem): bool =
            if !Flags.flag_dbg_join_list then
              Log.debug "Calling f_pred_node,\n  i = %i" (Sv_utils.sv_to_int i);
            let prevs = sv_pred i m in
            if !Flags.flag_dbg_join_list then
              Log.debug "prevs = { %a }" (SvSet.t_fpr ", ") prevs ;
            try
              SvSet.iter
                (fun pre_id ->
                  if !Flags.flag_dbg_join_list then
                    Log.debug "pred_id = %a" sv_fpr pre_id ;
                  match (sv_find pre_id m).ln_e with
                  | Lhemp
                  | Lhpt _ -> ()
                  | Lhlist _ -> raise Stop
                  | Lhlseg (lc, id, lsegc) -> f_ind_args i lc lsegc
                ) prevs;
              if !Flags.flag_dbg_join_list then
                Log.force "f_node concluded false on %a" sv_fpr i;
              false
            with Stop ->
              if !Flags.flag_dbg_join_list then
                Log.force "f_node concluded true on %a" sv_fpr i;
              true in
          let f_node (i: sv) (m: lmem) : bool =
            match (sv_find i m).ln_e with
            | Lhpt block ->
                Block_frag.fold_base
                  (fun _ pe b -> b || f_pred_node (fst pe.pe_dest) m)
                  block false
            | _ -> false in
          SvMap.fold
            (fun i _ acc ->
              let il, ir = SvMap.find i r.n_pi in
              if f_node il xl || f_node ir xr then
                begin
                  if !Flags.flag_dbg_join_list then
                      Log.debug "prio_nodes: Adding %a - %a" sv_fpr il sv_fpr ir;
                  Aa_maps.add ir il acc
                end
              else acc)
            xo.lm_mem s.hbg_live in
        fun (i, j) ->
          try Aa_maps.find j prio_nodes = i
          with Not_found -> false in
  let jc = init_rules { j_rules     = empty_rules;
                        j_inl       = xl;
                        j_vsat_l    = vsatl;
                        j_inr       = xr;
                        j_vsat_r    = vsatr;
                        j_rel       = r;
                        j_hint      = h;
                        j_instset_l = set_colv_inst_empty;
                        j_instset_r = set_colv_inst_empty;
                        j_instseq_l = seq_colv_inst_empty;
                        j_instseq_r = seq_colv_inst_empty;
                        j_out       = xo; } in
  let out = rec_join jc in
  let out =
    let f_all_segs = jinst_set_guesscstr_segs_postjoin out.j_out in
    { out with
      j_instset_l = f_all_segs out.j_instset_l;
      j_instset_r = f_all_segs out.j_instset_r; } in
  (* Display, if verbose mode or if join not perfect *)
  let ind = "     " in
  let bpi = lmem_fpri ind in
  let nl = num_edges out.j_inl and nr = num_edges out.j_inr in
  if !Flags.flag_dbg_join_shape || nl != 0 || nr != 0 then
    begin
      Log.force
        "\n\nFinal [%d,%d]:\n- Left shape:\n%a- Right shape:\n%a- Out:\n%a"
        nl nr bpi out.j_inl bpi out.j_inr bpi out.j_out;
      Log.force "- Rel:\n%a" (Graph_utils.Nrel.nr_fpri ind) out.j_rel;
      Log.force "- Inst-set-l:\n%a" (set_colv_inst_fpri "   ") out.j_instset_l;
      Log.force "- Inst-set-r:\n%a" (set_colv_inst_fpri "   ") out.j_instset_r;
      Log.force "- Inst-seq-l:\n%a" (seq_colv_inst_fpri "   ") out.j_instseq_l;
      Log.force "- Inst-seq-r:\n%a" (seq_colv_inst_fpri "   ") out.j_instseq_r;
    end;
  { out       = out.j_out;
    rel       = out.j_rel;
    instset_l = out.j_instset_l;
    instset_r = out.j_instset_r;
    instseq_l = out.j_instseq_l;
    instseq_r = out.j_instseq_r; }
