(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_mem_low_list.ml
 **       a simplified inductive domain based on lists
 **       - no arbitrary inductives;
 **       - set parameters.
 ** Xavier Rival, 2014/02/23 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def

open Ast_sig
open Col_sig
open Dom_sig
open Graph_sig
open Ind_sig
open Inst_sig
open List_sig
open Nd_sig
open Svenv_sig
open Spec_sig
open Vd_sig

open Col_utils
open Dom_utils
open Graph_utils
open Inst_utils
open List_utils
open Nd_utils
open Set_sig
open Set_utils
open Seq_sig
open Seq_utils
open Sv_utils
open Vd_utils

open Apron

(** Error report *)
module Log =
  Logger.Make(struct let section = "dm_ll___" and level = Log_level.DEBUG end)

(** Simplified functor:
 *  This module supports a lot fewer things than the dom_mem_low_shape does
 *   - simpler cell resolution;
 *   - no sub-memories;
 *   - no backward unfolding;
 **)
module DBuild = functor (Dv: DOM_VALCOL) ->
  (struct
    (** Module name *)
    let module_name = "[list]"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name Dv.module_name Dv.config_fpr ()
    (** Dom ID *)
    let dom_id: mod_id ref = ref (sv_unsafe_of_int (-1), "list")

    (** Type of symbolic indexes *)
    type sym_index = sv (* int for the list domain implementation *)
    type off_sym_index = sym_index * Offs.t

    (** Abstract value types *)
    type t =
        { (* memory contents *)
          t_mem:    lmem;
          (* abstraction of values *)
          t_num:    Dv.t;
          (* local modification *)
          t_svemod: svenv_mod }

    (** Domain initialization to a set of inductive definitions *)

    (* Domain initialization to a set of inductive definitions *)
    let init_inductives (g: SvGen.t) (s: StringSet.t): SvGen.t =
      let g, k = SvGen.gen_key g in (* this domain generates keys *)
      if s != StringSet.empty then
        Log.fatal_exn "init_inductives not allowed";
      if sv_upg then dom_id := k, snd !dom_id;
      g
    let inductive_is_allowed (name: string): bool =
      not ((Ind_utils.ind_find name).i_list = None)

    (** Fixing sets of keys *)
    let sve_sync_bot_up (x: t): t * svenv_mod =
      { x with t_svemod = svenv_empty }, x.t_svemod

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t =
      { t_mem    = lmem_empty;
        t_num    = Dv.bot;
        t_svemod = svenv_empty; }
    let is_bot (x: t) = Dv.is_bot x.t_num
    (* Top element, with empty set of roots *)
    let top (): t =
      { t_mem    = lmem_empty;
        t_num    = Dv.top;
        t_svemod = svenv_empty; }

    (** Pretty-printing and output *)
    let t_fpri (ind: string) (fmt: form) (x: t): unit =
      F.fprintf fmt "%a%a" (List_utils.lmem_fpri ind) x.t_mem
        (Dv.t_fpri SvMap.empty ind) x.t_num
    (* External output *)
    let ext_output (o: output_format) (base: string) (namer: namer) (x: t)
      : unit =
      match o with
      | Out_dot (vars, visu_opts) ->
          if !Flags.very_silent_mode then
            (* don't slow down memcad during benchmarks *)
            Log.warn "no pdf output in very_silent_mode"
          else
            let options = List.map Graph_utils.visu_opt_of_string visu_opts in
            let dot_fn = Printf.sprintf "%s.dot" base in
            let long_label =
              F.asprintf "%s.pdf\n%a" base (Dv.t_fpri SvMap.empty "") x.t_num in
            with_out_file dot_fn
              (List_visu.output_dot options long_label vars x.t_mem namer);
            let dot_to_pdf =
              Printf.sprintf "dot -Tpdf %s -o %s.pdf" dot_fn base in
            ignore (run_command dot_to_pdf)

    (** Sanity checks *)
    let sanity_check (ctxt: string) (x: t): unit =
      (* - collect SVs in the memory abstraction
       * - check that the numerical domain speaks about exacty these SVs *)
      if !Flags.flag_sanity_env then
        let snodes = List_utils.sv_get_all x.t_mem in
        if Flags.flag_sanity_env_pp then
          Log.info "domlist,sanity_check<%s>, memory nodes: { %a }\n%a"
            ctxt (SvSet.t_fpr "; ") snodes (svenv_fpri "  ") x.t_svemod;
        let is_valid = Dv.symvars_check snodes x.t_num in
        if Flags.flag_sanity_env_pp then
          Log.info "domlist,sanity_check<%s>: %b" ctxt is_valid;
        if not is_valid then
          begin
            Log.info "check_nodes(%s): inconsistent num keys" ctxt;
            Log.fatal_exn "check_nodes failed"
          end;
        Log.info "domlist,sanity_check<%s>: ok" ctxt
    let sanity_sv (s: SvSet.t) (x: t): bool =
      Log.force "Sanity_sv: { %a }\n%a" (SvSet.t_fpr ",") s
        (lmem_fpri "   ") x.t_mem;
      List_utils.sanity_check "sanity_sv" x.t_mem;
      (* if it makes it that far, then the test is passed *)
      true

    (** Management of symbolic variables *)
    (* Ensuring consistency of SVs *)
    let sve_fix (msg: string) (x: t): t =
      let loc_debug = false in
      if loc_debug then
        Log.info "Doing sve_fix (%s):\n%a" msg (t_fpri "  ") x;
      let m0, svm = List_utils.sve_sync_bot_up x.t_mem in
      if loc_debug then
        Log.info "playing add_rem:\n%a" (svenv_fpri "  ") svm;
      { t_mem    = m0;
        t_num    = Dv.sve_sync_top_down svm x.t_num;
        t_svemod = svenv_join x.t_svemod svm }
    (* Check whether an SV can be created in the current domain *)
    let sv_is_allowed ?(attr: node_attribute = Attr_none) (nt: ntyp)
        (na: nalloc) (x: t): bool = true
    (* Addition of a fresh symbolic variable *)
    let sv_add_fresh ?(attr: node_attribute = Attr_none) ?(root: bool = false)
        (nt: ntyp) (na: nalloc) (x: t): sym_index * t =
      (* consider adding a sanity check *)
      let i, m0 = List_utils.sv_add_fresh ~root:root nt x.t_mem in
      i, sve_fix "sv_add_fresh" { x with t_mem = m0 }
    let sv_get_info (i: sv) (x: t): nalloc option * ntyp option =
      Log.warn "sv_get_info imprecise";
      None, None
    let gc (roots: sym_index uni_table) (x: t): t =
      let mem, s = List_utils.gc roots x.t_mem in
      let num =
        SvSet.fold
          (fun i acc ->
            try
              if Dv.colv_is_root i acc then acc else Dv.colv_rem i acc
            with _ -> acc (* was already removed... *)
          ) s x.t_num in
      sve_fix "gc" { x with
                     t_mem = mem;
                     t_num = num; }

    (** Management of COLvs (Set/Seq) *)
    let colv_add (setv: sv) (ck: col_kind) (t: t): t =
      let mem = colv_add setv ck t.t_mem in
      let num = Dv.colv_add ck ~root:false ~name:None setv None t.t_num in
      { t with t_mem = mem ; t_num = num }
    let colv_add_fresh (root: bool) (s: string) (ck: col_kind) (t: t)
        : sv * t * colv_info option =
      let k =
        match ck with
        | CK_set -> Ct_set None
        | CK_seq -> Ct_seq None in
      let i, mem = colv_add_fresh ~root k t.t_mem in
      i, { t with
           t_num = Dv.colv_add ck ~root ~name:(Some s) i None t.t_num;
           t_mem = mem }, None
    let colv_delete (i: sv) (t: t): t =
      let num = Dv.colv_rem i t.t_num in
      { t with
        t_mem = colv_rem i t.t_mem;
        t_num = num }
    let colv_delete_non_root (i: sv) (x: t): t =
      if Dv.colv_is_root i x.t_num then x
      else colv_delete i x

    (** Utility functions for the manipulation of abstract values *)
    (* Satifying function in the value domain *)
    let sat_val (t: t) (c: n_cons): bool =
      if !Flags.flag_debug_block_sat then
        Log.force "sat_val,sat called: %a\n%a" n_cons_fpr c
          (Dv.t_fpri SvMap.empty "      ") t.t_num;
      Dv.sat t.t_num c

    (** Graph encoding (used for guiding join) *)
    let encode (disj: sv) (n: namer) (x: t)
        : renamed_path list * IntSvPrSetSet.t * sv =
      Log.todo_exn "encode"

    (** Comparison and Join operators *)
    (* Function to check constraint satisfaction on a t *)
    let make_sat (x: t) (nn: n_cons): bool =
      match nn with
      | Nc_cons (Tcons1.DISEQ, Ne_csti 0, Ne_var i)
      | Nc_cons (Tcons1.DISEQ, Ne_var i, Ne_csti 0) ->
          if List_utils.pt_edge_mem (i, Offs.zero) x.t_mem then true
          else Dv.sat x.t_num nn
      | _ -> Dv.sat x.t_num nn
    let make_setsat (x: t) (sc: set_cons): bool = Dv.set_sat sc x.t_num
    let make_seqsat (x: t) (sc: seq_cons): bool = Dv.seq_sat sc x.t_num
    let make_vsat (x: t): vsat =
      { vs_num = (make_sat x) ;
        vs_set = (make_setsat x) ;
        vs_seq = (make_seqsat x) }

    (* col variable collection: containing two parts: collecting all the *
     * col variables from the shape and the root col variables in the col *
     * domain *)
    let colv_col (x: lmem): SvSet.t =
      let colv =
        SvMap.fold
          (fun i n acc ->
            match n.ln_e with
            | Lhlist ld ->
                List.fold_left (fun acc i -> SvSet.add i acc) acc
                  (ld.lc_setargs@ld.lc_seqargs)
            | Lhlseg (ld, _, _) ->
                List.fold_left (fun acc i -> SvSet.add i acc) acc
                  (ld.lc_setargs@ld.lc_seqargs)
            | _ -> acc
          )  x.lm_mem SvSet.empty in
      colv
      |> SvSet.union (setv_roots x)
      |> SvSet.union (seqv_roots x)

    (* Join and widening *)
    let join
        (j: join_kind)
        (hso: sym_index hint_bs option)
        (lso: (Offs.svo lint_bs) option)
        (roots_rel: sym_index tri_table)
        (colroots_rel: (sv, col_kind) tri_map)
        ((xl, jl): t * join_ele)
        ((xr, jr): t * join_ele)
        : t * svenv_upd * svenv_upd =
      let loc_debug = false in
      sanity_check "join-l,before" xl; sanity_check "join-r,before" xr;
      (* Printing functions used thoughout *)
      let pp_status_inst msg set_instl seq_instl set_instr seq_instr =
        Log.force "Instantiations, %s:\n- left:\n%a%a- right:\n%a%a" msg
          (set_colv_inst_fpri "   ") set_instl (seq_colv_inst_fpri "   ")
          seq_instl (set_colv_inst_fpri "   ") set_instr
          (seq_colv_inst_fpri "   ") seq_instr in
      let pp_status_num msg num_l num_r =
        Log.force "Num status,%s:\n- left:\n%a\n- right:\n%a" msg
          (Dv.t_fpri SvMap.empty "      ") num_l
          (Dv.t_fpri SvMap.empty "      ") num_r in
      (* apparently, the hint has been deprecated; clean it up ? *)
      let hgo = Option.map (fun h -> { hbg_live = h.hbs_live }) hso in
      (* Computation of the node_relation *)
      let nrel =
        Aa_sets.fold
          (fun (il, ir, io) acc -> Graph_utils.Nrel.add acc (il, ir) io)
          roots_rel Graph_utils.Nrel.empty in
      (* Computation of setvar_relation *)
      (* JG:TODO Add qrel for list_join *)
      let srel =
        Aa_maps.fold
          (fun (il, ir, io) _ acc -> Graph_utils.Nrel.add acc (il, ir) io)
          colroots_rel Graph_utils.Nrel.empty in
      (* Creation of the initial graph *)
      let m_init, (col_toaddl, col_toaddr), col_torem =
        init_from_roots nrel.n_pi xl.t_mem xr.t_mem in
      let col_torem =
        SvMap.fold (fun i _ -> SvSet.add i) col_torem SvSet.empty in
      let xl = SvMap.fold colv_add col_toaddl xl
      and xr = SvMap.fold colv_add col_toaddr xr in
      if !flag_dbg_join_num || loc_debug then
        Log.force "Homogeneous:\n- left: \ncol:%a\n%a\n- right: \ncol:%a\n%a"
          col_kinds_fpr col_toaddl (Dv.t_fpri SvMap.empty "    ") xl.t_num
          col_kinds_fpr col_toaddr (Dv.t_fpri SvMap.empty "    ") xr.t_num;
      (* Computation of graph join, and of new graph relation *)
      let vsatl = make_vsat xl and vsatr = make_vsat xr in
      let jo =
        List_join.join xl.t_mem ~vsatl xr.t_mem ~vsatr hgo nrel srel
          false m_init in
      (* Preparation of the mapping for used to nodes rename *)
      let svmap_l, svmap_r =
        Gen_join.extract_mappings (sv_get_all xl.t_mem) (sv_get_all xr.t_mem)
          jo.rel in
      (* Complete instantiation of colvs following guesses (if any) *)
      if !flag_dbg_join_num || loc_debug then
        begin
          pp_status_inst "join-out" jo.instset_l jo.instseq_l
            jo.instset_r jo.instseq_r;
          pp_status_num "join-out" xl.t_num xr.t_num;
        end;
      let set_roots = setv_roots xl.t_mem in
      let set_instl =
        jinst_set_post_join_refine set_roots vsatl.vs_set jo.instset_l in
      let set_instr =
        jinst_set_post_join_refine set_roots vsatr.vs_set jo.instset_r in
      let seq_roots = seqv_roots xl.t_mem in
      let seq_instl =
        jinst_seq_post_join_refine seq_roots vsatl.vs_seq jo.instseq_l in
      let seq_instr =
        jinst_seq_post_join_refine seq_roots vsatr.vs_seq jo.instseq_r in
      (* When either instantiation has non bound variables,
       * inherit from the other *)
      let set_instl = colv_inst_strengthen set_instr set_instl in
      let set_instr = colv_inst_strengthen set_instl set_instr in
      let seq_instl = colv_inst_strengthen seq_instr seq_instl in
      let seq_instr = colv_inst_strengthen seq_instl seq_instr in
      (* Instantiation of the set.seq variables *)
      if !flag_dbg_join_num || loc_debug then
        pp_status_inst "refined" set_instl seq_instl set_instr seq_instr;
      let f_add_setcons (inst: (set_expr, set_cons) colv_inst) xnum =
        Inst_utils.gen_colv_inst_fold_ctr
          ~expr_fpr:set_expr_fpr ~cons_fpr:set_cons_fpr
          ~colv_add:(fun colv -> Dv.colv_add CK_set ~root:false colv None)
          ~colv_rem:Dv.colv_rem
          ~guard:Dv.set_guard ~eqcons:(fun i ex -> (S_eq (S_var i, ex)))
          ~sat:Dv.set_sat inst xnum in
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "Before set inst" xl.t_num xr.t_num;
      let num_l = f_add_setcons set_instl xl.t_num in
      let num_r = f_add_setcons set_instr xr.t_num in
      let f_add_seqcons (inst: seq_colv_inst) xnum =
        Inst_utils.gen_colv_inst_fold_ctr
          ~expr_fpr:seq_expr_fpr ~cons_fpr:seq_cons_fpr
          ~colv_add:(fun colv -> Dv.colv_add CK_seq ~root:false colv None)
          ~colv_rem:Dv.colv_rem
          ~guard:Dv.seq_guard ~eqcons:(fun i ex -> (Seq_equal (Seq_var i, ex)))
          ~sat:Dv.seq_sat inst xnum in
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "After set inst" num_l num_r;
      let num_l = f_add_seqcons seq_instl num_l in
      let num_r = f_add_seqcons seq_instr num_r in
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "Inst done" num_l num_r;
      (* Removing the setvs that are not in the join result *)
      (* This seems redundant with the above *)
      Log.info "tmp,setv,join,num";
      let num_l = SvSet.fold Dv.colv_rem col_torem num_l in
      let num_r = SvSet.fold Dv.colv_rem col_torem num_r in
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "Removal done" num_l num_r;
      (* Renaming the svs *)
      let instantiate (map: unit node_mapping) (n1: Dv.t) =
        let om = OffMap.empty
        and map = { map with nm_suboff = (fun _ -> false) } in
        Dv.symvars_srename om map None n1 in
      let num_l = instantiate svmap_l num_l in
      let num_r = instantiate svmap_r num_r in
      if !flag_dbg_join_num || loc_debug then
        pp_status_num "Renaming of SV done:" num_l num_r;
      (* Join in the num + set domain *)
      let n_out =
        match j with
        | Jjoin | Jwiden -> Dv.upper_bnd j num_l num_r
        | Jdweak ->
            Log.fatal_exn "gen_join does not implement directed weakening" in
      if !flag_dbg_join_num || loc_debug then
        Log.force "Numeric join result:\n%a"
          (Dv.t_fpri SvMap.empty "  ") n_out;
      (* Graph SV/COLV fixes *)
      let m_out, _ = List_utils.sve_sync_bot_up jo.out in
      let m_out = SvSet.fold colv_rem col_torem m_out in
      let m_out = Inst_utils.colv_inst_fold_rem colv_rem jo.instset_l m_out in
      let t_out = { t_mem    = m_out;
                    t_num    = n_out;
                    t_svemod = svenv_empty } in
      sanity_check "join,after" t_out;
      let t_num =
        Dv.symvars_filter (sv_get_all t_out.t_mem)
          ~col_vars:(colv_col t_out.t_mem) t_out.t_num in
      (* Result *)
      let t_out = { t_out with t_num = t_num } in
      if loc_debug then Log.info "join-exit:\n%a" (t_fpri "   ") t_out;
      t_out, svenv_upd_embedding svmap_l, svenv_upd_embedding svmap_r

    (* Checks if the left argument is included in the right one *)
    let is_le (ninj: sym_index bin_table) (cinj: colv_emb)
        (xl: t) (xr: t): svenv_upd option =
      let loc_debug = false in
      sanity_check "is_le,l,before" xl;
      sanity_check "is_le,r,before" xr;
      (* launching the graph algorithm and processing its output *)
      let is_le =
        List_is_le.is_le xl.t_mem ~vsat_l:(make_vsat xl) xr.t_mem ninj cinj in
      match is_le with
      | `Ilr_not_le ->
          if loc_debug || !flag_dbg_is_le_gen then (* temptemp *)
            Log.force "ISLE Comparison: shape did not conclude le!";
          None
      | `Ilr_le (ilrem, b) ->
          if ilrem.ilr_setctr_r != [] || ilrem.ilr_seqctr_r != [] then
            Log.info "Ilr_le: non empty list of predicates";
          if loc_debug || !flag_dbg_is_le_gen then
            begin
              Log.force "ISLE Comparison: is_le holds in the shape!";
              Log.force "Numerics:\n%aMapping:\n%a\n%a"
                (Dv.t_fpri SvMap.empty "  ") xr.t_num
                (gen_svmap_fpr "; " nsv_fpr nsv_fpr) ilrem.ilr_sv_rtol
                (gen_svmap_fpr "; " setv_fpr setvset_fpr)
                (SvMap.map fst ilrem.ilr_colv_rtol)
            end;
          let nodes = sv_get_all xl.t_mem in
          (* instantiation of node relation *)
          let inj_rel: (sv * Offs.t) node_mapping =
            SvMap.fold
              (fun i j -> Nd_utils.add_to_mapping j i) ilrem.ilr_sv_rtol
              { nm_map    = SvMap.empty ;
                nm_rem    = nodes;
                nm_suboff = fun _ -> true } in
          (* instantiation of the set relation *)
          (* instantiation of set variable relation *)
          let sinj_rel =
            SvMap.fold
              (fun i j acc_map ->
                SvSet.fold
                  (fun j_e acc_map ->
                    Col_utils.add_to_mapping j_e i acc_map
                  ) j acc_map
              ) (colv_embedding_comp_project CK_set ilrem.ilr_colv_rtol)
              set_colv_mapping_empty in
          let qinj_rel =
            SvMap.fold
              (fun i j acc_map ->
                SvSet.fold
                  (fun j_e acc_map ->
                    Col_utils.add_to_mapping j_e i acc_map
                  ) j acc_map
              ) (colv_embedding_comp_project CK_seq ilrem.ilr_colv_rtol)
              seq_colv_mapping_empty in
          let n_l =
            Dv.symvars_srename ~mark:false OffMap.empty inj_rel
              (Some { cmc_set = sinj_rel; cmc_seq = qinj_rel }) xl.t_num in
          let b = b && Dv.is_le n_l xr.t_num (fun i j -> false) in
          let r =
            if b then
              let svu = svenv_upd_embedding { inj_rel with
                                              nm_suboff = fun _ -> true } in
              Some svu
            else None in
          if !flag_dbg_is_le_gen then
            Log.force "Numeric comparison: %s\nleft:\n%aright:\n%a"
              (if r = None then "false" else "true")
              (Dv.t_fpri SvMap.empty "   ") n_l
              (Dv.t_fpri SvMap.empty "   ") xr.t_num;
          r
    let directed_weakening _ = Log.todo_exn "dw"
    (* Unary abstraction, sort of incomplete canonicalization operator *)
    let local_abstract (ho: sym_index hint_us option) (x: t): t =
      sanity_check "local_abstract,before" x;
      (* computation of the hint for underlying *)
      (*  i.e., nodes corresponding to values of hint variables *)
      let hu =
        let m = x.t_mem in
        let f h =
          Aa_sets.fold
            (fun root acc ->
              let nm, pte =
                pt_edge_extract (sat_val x)
                  (root, Offs.zero) Flags.abi_ptr_size m in
              assert (nm == m);
              assert (Offs.is_zero (snd pte.pe_dest));
              Aa_sets.add (fst pte.pe_dest) acc
            ) h.hus_live h.hus_live in
        Option.map f ho in
      (* weak local abstraction on graphs *)
      let m, inj, _cvinj, s_info, q_info =
        List_abs.local_abstract ~stop:hu ~vsat:(make_vsat x) x.t_mem in
      (* prove set domain imply relation *)
      let svs = List_utils.sv_get_all m in
      let colvs = colv_col m in
      let num = Dv.symvars_filter svs ~col_vars:colvs x.t_num in
      let o = { t_mem    = m;
                t_num    = num;
                t_svemod = svenv_empty; } in
      sanity_check "local_abstract,after" o;
      o


    (** Reduction into the graph *)
    (* Performs reduction in the graph, and extends a renamer *)
    let reduce_graph (gr: lguard_res) (renamer: sv SvMap.t) (x: t)
        : sv SvMap.t * t =
      match gr with
      | Gr_no_info -> renamer, x
      | Gr_bot -> raise Bottom
      | Gr_sveq (svr, svk) ->
          SvMap.add svr svk renamer,
          { x with t_mem = lmem_rename_sv svr svk x.t_mem }


    (** Unfolding support *)
    (* Classical forward unfolding, returs a list of disjuncts,
     * together with a renaming function (for cases where nodes are renamed) *)
    let unfold (i: sv) (x: t) (u: unfold_dir): (sv SvMap.t * t) list =
      let l = List_mat.unfold i false x.t_mem u in
      let f acc ur =
        if !flag_dbg_dom_list then
          Log.debug "\n\nnew unfold begin...\n%a" (lmem_fpri "  ") ur.ur_lmem;
        let x = sve_fix "unfold" { x with t_mem = ur.ur_lmem; } in
        let m = x.t_mem in
        let n =
          SvMap.fold (fun i k -> Dv.colv_add k ~root:false i None)
            ur.ur_newcolvs x.t_num in
        let n = List.fold_left (fun n c -> Dv.guard true c n) n ur.ur_cons in
        let n = List.fold_left (fun n c -> Dv.set_guard c n) n ur.ur_setcons in
        let n = List.fold_left (fun n c -> Dv.seq_guard c n) n ur.ur_seqcons in
        if !flag_dbg_dom_list then
          Log.debug "seq_cons =\n\t%a" (* JG : Keep this log ? *)
            (gen_list_fpr "[ ]" seq_cons_fpr "\n\t") ur.ur_seqcons;
        let x =
          { x with
            t_mem = SvMap.fold (fun cv _ -> colv_rem cv) ur.ur_remcolvs m;
            t_num = SvMap.fold (fun cv _ -> Dv.colv_rem cv) ur.ur_remcolvs n} in
        try
          (* reduction phase *)
          let renamer, x =
            List.fold_left
              (fun (renamer, x) c ->
                reduce_graph (lmem_guard c x.t_mem) renamer x
              ) (SvMap.empty, x) ur.ur_cons in
          if !flag_dbg_dom_list then
            begin
              Log.debug "result after reduction:\n%a" (t_fpri "  ") x;
              Log.debug "unfold finished...\n";
            end;
          (renamer, x) :: acc
        with
        | Bottom -> acc in
      let l = List.fold_left f [ ] (List.rev l) in
      let l = List.filter (fun (_, x) -> not (is_bot x)) l in
      l


    (** Cell operations and nonlocal_unfolding *)
    (* Cell creation and deletion *)
    let cell_create ?(attr:node_attribute = Attr_none)
        ((si, so): Offs.svo) (sz: Offs.size) (nt: ntyp) (x: t): t =
      sanity_check "create,before" x;
      let dst, x = sv_add_fresh nt Nnone x in (* destination *)
      let pe = { pe_size = sz ; (* new points-to edge *)
                 pe_dest = dst, Offs.zero } in
      let bsrc = si, Bounds.of_offs so in
      let rx = { x with t_mem = pt_edge_block_append bsrc pe x.t_mem } in
      sanity_check "create,after" rx;
      rx
    let cell_delete ?(free:bool = true) ?(root:bool = false)
        (i: sv) (x: t): t =
      let m = if root then List_utils.sv_unroot i x.t_mem else x.t_mem in
      let m = List_utils.pt_edge_block_destroy ~remrc:true i m in
      let m = if free then m (*JG:TODO *) else m in
      sve_fix "cell_delete" { x with t_mem = m }
    (* Reading of a cell and nonlocal unfolding (both are mutually dependent) *)
    let cell_read_one (src: Offs.svo) (sz: int) (x: t): t * Offs.svo =
      let nt, pte = pt_edge_extract (sat_val x) src sz x.t_mem in
      if not (Offs.size_to_int_opt pte.pe_size = Some sz) then
        Log.fatal_exn "cell_read: not the right size (%d,%a)" sz
          Offs.size_fpr pte.pe_size;
      sve_fix "cell_read_one" { x with t_mem = nt }, pte.pe_dest
    let rec cell_read (is_resolve: bool) (src: Offs.svo) (sz: int) (x: t)
        : (t * Offs.svo * Offs.svo option) list =
      sanity_check "cell_read,before" x;
      let rl = cell_read_exc Flags.max_unfold src sz x in
      List.iter (fun (rx,_,_) -> sanity_check "cell_read,after" rx) rl;
      rl
    and cell_read_exc (count: int) (src: Offs.svo) (sz: int) (x: t)
        : (t * Offs.svo * Offs.svo option) list =
      try
        let x, cont = cell_read_one src sz x in
        [ x, src, Some cont ]
      with
      | Graph_sig.No_edge mess ->
          Log.info "cell_read: No_edge exn caught, non local unfold";
          begin
            try
              nonlocal_unfold src sz x
            with
            | exn ->
                Log.warn "Exn: %s" (Printexc.to_string exn);
                [ x, src, None ]
          end
      | Graph_sig.Unfold_request (ul, dir) ->
          let i =
            match ul with
            | Uloc_main i -> i
            | Uloc_sub _ -> Log.fatal_exn "sub-memories not supported" in
          if count > 0 then
            let ncount = count - 1 in
            let l = unfold i x dir in
            let f (r, u) =
              let i, o = src in
              let ni = try SvMap.find i r with Not_found -> i in
              cell_read_exc ncount (ni, o) sz u in
            List.flatten (List.map f l)
          else Log.fatal_exn "reached the maximal number of unfoldings"
    (* Non local unfolding support *)
    and nonlocal_unfold (src: Offs.svo) (sz: int) (x: t)
        : (t * Offs.svo * Offs.svo option) list =
      let loc_debug = false in
      (* XR: this algorithm does not sound so satisfactory to me
       * - other option: look for the sets that contain i
       * - for each such set search for a decomposition of it based on the
       * parameters that are unfold parameters + unfolded nodes *)
      let isrc = fst src and offsrc = snd src in
      (* Global search through the abstract heap, for an SV carrying an
       * inductive or segment predicate, such that SV isrc may be an
       * element of the structure *)
      (* XR: what happens when there are several solutions ? *)
      let svs_inds, ld_def_opt =
        SvMap.fold
          (fun key elt (acc, ld_def_opt) ->
            match elt.ln_e with
            | Lhemp | Lhpt _ -> (acc, ld_def_opt)
            | Lhlist { lc_def; lc_setargs }
            | Lhlseg ({ lc_def; lc_setargs }, _, _) ->
                match lc_def.ld_set with
                | None      -> (acc, ld_def_opt)
                | Some seti ->
                    match seti.s_uf_seg with
                    | None -> (acc, ld_def_opt)
                    | Some argnum ->
                        let arg = List.nth lc_setargs argnum in
                        SvMap.add key (arg, elt.ln_e) acc, Some lc_def
          ) x.t_mem.lm_mem (SvMap.empty, None) in
      let ld_def =
        match ld_def_opt with
        | None -> raise (Failure "nonlocal_unfold")
        | Some ld_def -> ld_def in
      (* Collect SVs that could be the address of a points-to block
       * of the correct size *)
      let svs_pts =
        SvMap.fold
          (fun sv elt acc ->
            match elt.ln_e with
            | Lhpt pt_edge_blk ->
                begin
                  if ld_def.ld_size = Block_frag.byte_size pt_edge_blk then
                    SvSet.add sv acc
                  else
                    acc
                end
            | Lhemp
            | Lhlist _
            | Lhlseg _ -> acc
          ) x.t_mem.lm_mem SvSet.empty in
      (* Expression that needs to be satisfied to allow non local unfolding *)
      let set_cons =
        let setvs =
          SvMap.fold (fun _ (e,_) -> SvSet.add e) svs_inds SvSet.empty in
        let set_expr = make_setexp ~is_disjoin:true ~setvs ~svs:svs_pts () in
        S_mem (isrc, set_expr) in
      if loc_debug then
        Log.info "tmp,nlu,consider ctrs: %a => %b" set_cons_fpr set_cons
          (Dv.set_sat set_cons x.t_num);
      if Dv.set_sat set_cons x.t_num then
        (* Compute the disjunction of cases *)
        let split_inds: t list =
          SvMap.fold
            (fun sv (arg, heap_frag) acc ->
              let transform lc fadd frem =
                let lm, lseg, lsegc, lind, setcons, setv_torem, setv_add =
                  gen_ind_setpars x.t_mem lc in
                let lm = fadd lind (frem sv lm) in
                let lm = lseg_edge_add sv isrc lseg lsegc lm in
                let num =
                  SvSet.fold
                    (fun colv -> Dv.colv_add CK_set ~root:false colv None)
                    setv_add x.t_num in
                let num =
                  List.fold_left
                    (fun acc c -> Dv.set_guard c acc) num setcons in
                let num = Dv.set_guard (S_mem (isrc, S_var arg)) num in
                let xx = (* remove all non root COLVs to remove *)
                  SvSet.fold colv_delete_non_root setv_torem
                    { x with t_mem = lm; t_num = num } in
                xx :: acc in
              match heap_frag with
              | Lhemp
              | Lhpt _ -> Log.fatal_exn "nonlocal_unfold emp/Lhpt"
              | Lhlist lc ->
                  transform lc (list_edge_add isrc) list_edge_rem
              | Lhlseg (lc, nid, lsegc) ->
                  transform lc (fun lc -> lseg_edge_add isrc nid lc lsegc)
                    lseg_edge_rem
            ) svs_inds [] in
        let ret =
          SvSet.fold
            (fun elt acc ->
              let num =
                Dv.guard true
                  (Nc_cons (Apron.Tcons1.EQ, Ne_var isrc, Ne_var elt))
                  x.t_num in
              let x = { x with t_num = num } in
              let rl = cell_read_exc Flags.max_unfold (elt, offsrc) sz x in
              List.iter (fun (rx,_,_) -> sanity_check "cell_read,after" rx) rl;
              List.rev_append rl acc
            ) svs_pts [ ] in
        List.fold_left
          (fun acc elt ->
            let rl = cell_read_exc Flags.max_unfold src sz elt in
            List.iter (fun (rx,_,_) -> sanity_check "cell_read,after" rx) rl;
            List.rev_append rl acc
          ) ret split_inds
      else
        Log.fatal_exn "tmp,nlu,fails,returning []"
    (* Checks for the existence of a cell, without actually exposing
     * its content *)
    let cell_resolve (src: Offs.svo) (size: int) (x: t)
        : (t * Offs.svo * bool) list =
      let f (x, src, op_cont) = x, src, op_cont != None in
      List.map f (cell_read false src size x)
    (* Over-writing the content of a cell *)
    let cell_write (ntyp: ntyp)
        (dst: Offs.svo) (size: int) (ne: n_expr) (x: t): t =
      sanity_check "cell_write,before" x;
      let write_in_main (x: t) (nval: Offs.svo): t =
        let pte = { pe_size = Offs.size_of_int size;
                    pe_dest = nval } in
        let nt = pt_edge_replace (sat_val x) dst pte x.t_mem in
        sve_fix "cell_write" { x with t_mem = nt } in
      let non_ptr_assign ( ) =
        let nn, x = sv_add_fresh ntyp Nnone (sve_fix "non_ptr_assign" x) in
        if !Flags.flag_dbg_dom_list then
          Log.info "Non_ptr_assign: %a\n%a" sv_fpr nn (t_fpri "  ") x;
        (* Issue: some node stayed there and is allocated again... *)
        write_in_main { x with t_num = Dv.assign nn ne x.t_num }
          (nn, Offs.zero) in
      let rx =
        match ntyp with
        | Ntaddr ->
            begin
              match decomp_lin_opt ne with
              | None -> non_ptr_assign ( )
              | Some (ir, offr) -> write_in_main x (ir, Offs.of_n_expr offr)
            end
        | _ -> non_ptr_assign ( ) in
      sanity_check "cell_write,after" rx;
      rx


    (** Transfer functions *)
    let guard (c: n_cons) (x: t): t =
      try
        let _, x = reduce_graph (lmem_guard c x.t_mem) SvMap.empty x in
        { x with t_num = Dv.guard true c x.t_num }
      with Bottom -> { x with t_num = Dv.bot }
    let sat (c: n_cons) (x: t): bool = Dv.sat x.t_num c

    let tr_setcon (x: svo setprop): set_cons =
      let tr_int i =
        match i with
        | None -> 0
        | Some i -> i in
      let s_var i = S_var (sv_unsafe_of_int i) in
      match x with
      | Sp_sub (l, r) -> S_subset (s_var l.colv_uid, s_var r.colv_uid)
      | Sp_mem ((i, o), r) -> (* check the off be zero for now*)
          let o =tr_int (Offs.to_int_opt o) in
          if o = 0 then
            S_mem (i, s_var r.colv_uid)
          else Log.fatal_exn "tr_setcon: offset is not eauqal to zero"
      | Sp_emp l -> S_eq (S_empty, s_var l.colv_uid)
      | Sp_euplus (l, (i, o), r) ->
          let o = tr_int (Offs.to_int_opt o) in
          if o = 0 then
            S_eq (s_var l.colv_uid, S_uplus (S_node i, s_var r.colv_uid))
          else Log.fatal_exn "tr_setcon: offset is not eauqal to zero"

    let tr_seqcon (x: svo seqprop): seq_cons =
      let tr_int i =
        match i with
        | None -> 0
        | Some i -> i in
      let rec tr_seqexp (x: svo seqexpr): seq_expr =
        match x with
        | Qe_empty -> Seq_empty
        | Qe_var x -> Seq_var (sv_unsafe_of_int x.colv_uid)
        | Qe_val (i, o) ->
            let o = tr_int (Offs.to_int_opt o) in
            if o <> 0 then
              Log.fatal_exn "tr_setcon: offset is not eauqal to zero";
            Seq_val i
        | Qe_sort s -> Seq_sort (tr_seqexp s)
        | Qe_concat (s1, s2) ->
            Seq_Concat [tr_seqexp s1; tr_seqexp s2] |> normalise
      in
      match x with
      | Qp_equal (s1, s2) ->
          Seq_equal (tr_seqexp s1, tr_seqexp s2)

    (** Assuming and checking inductive edges *)
    let ind_extract_list (name: string): l_def =
      try StringMap.find name !list_ind_defs
      with Not_found -> Log.fatal_exn "definition: %s not found" name

    let assume (op: meml_log_form) (t: t): t =
      match op with
      | SL_set x ->
          let x = tr_setcon x in
          { t with t_num = Dv.set_guard x t.t_num }
      | SL_seq x ->
          let x = tr_seqcon x in
          { t with t_num = Dv.seq_guard x t.t_num }
      | SL_ind (ic, (i, off)) ->
          assert (Offs.is_zero off);
          let ld = ind_extract_list ic.ic_name in
          let ptr_pars, set_pars, seq_pars =
            match ic.ic_pars with
            | None -> [ ], [ ], [ ]
            | Some ind_pars ->
                let f i = sv_unsafe_of_int i.colv_uid in
                List.map
                  (fun (sv, off) -> assert (Offs.is_zero off); sv)
                  ind_pars.ic_ptr,
                List.map (fun i -> sv_unsafe_of_int i.colv_uid) ind_pars.ic_set,
                List.map f ind_pars.ic_seq in
          let lc = { lc_def  = ld;
                     lc_ptrargs = ptr_pars ;
                     lc_setargs = set_pars;
                     lc_seqargs = seq_pars } in
          { t with
            t_mem = List_utils.list_edge_add i lc t.t_mem;
            t_num = t.t_num }
      | SL_seg (ic1, (i1, off1), ic2, (i2, off2))->
          assert (Offs.is_zero off1);
          (* JG:TODO Do we need to add the same assertion for off2 ?
           * This code is temporarily  Ask XR for final commit *)
          let ld = ind_extract_list ic1.ic_name in
          let ptr_pars, set_pars, seq_pars =
            match ic1.ic_pars with
            | None -> [ ], [ ], [ ]
            | Some ind_pars ->
                let f i = sv_unsafe_of_int i.colv_uid in
                Log.error "ind_pars.ic_ptr = %a"
                  (gen_list_fpr " "
                     (fun fmt (sv, off) ->
                       Format.fprintf fmt "{%a, %a}" sv_fpr sv Offs.t_fpr off)
                     "; ") ind_pars.ic_ptr;
                List.map fst ind_pars.ic_ptr,
                List.map (fun i -> sv_unsafe_of_int i.colv_uid) ind_pars.ic_set,
                List.map f ind_pars.ic_seq in
          let lc = { lc_def  = ld;
                     lc_ptrargs = ptr_pars;
                     lc_setargs = set_pars;
                     lc_seqargs = seq_pars } in
          let ptr_pars =
            match ic2.ic_pars with
            | None -> []
            | Some ind_pars ->
              List.map (fun i -> fst i) ind_pars.ic_ptr in
          let lsegc = { lc_def = ld;
                        lc_ptrargs = ptr_pars } in
          { t with
            t_mem = List_utils.lseg_edge_add i1 i2 lc lsegc t.t_mem;
            t_num = t.t_num }
      | SL_array ->
          { t with t_num = Dv.assume VA_array t.t_num }

    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    let mark_prio (lv: svo) (x: t) =
      let sv, off = lv in
      assert (Offs.is_zero off);
      { x with t_mem = mark_prio sv x.t_mem }

    let ind_unfold (u: unfold_dir) ((n,o): Offs.svo) (x: t): t list =
      if u != Udir_fwd then Log.fatal_exn "unfold: only forward supported";
      if List_utils.sv_is_ind n x.t_mem then List.map snd (unfold n x u)
      else [ x ]

    let check (op: meml_log_form) (x: t): bool =
      let create_mem ((ic, i): svo gen_ind_call * sv)
          (icro: (svo gen_ind_call * sv) option) : lmem * node_emb =
        let ld = ind_extract_list ic.ic_name in
        let ptr_pars, set_pars, seq_pars =
          match ic.ic_pars with
          | None -> [ ], [ ], [ ]
          | Some ind_pars ->
              (* JG: fold on reversed list avoids non-tail recusrion, but the
               * code is less clear that a simple List.map *)
              List.fold_left
                (fun acc (i, off) -> assert (Offs.is_zero off); i :: acc)
                [ ] (List.rev ind_pars.ic_ptr),
              List.fold_left (fun acc i -> sv_unsafe_of_int i.colv_uid :: acc)
                [ ] (List.rev ind_pars.ic_set),
              List.fold_left
                (fun acc i -> sv_unsafe_of_int i.colv_uid :: acc)
                [ ] (List.rev ind_pars.ic_seq) in
        let lm, sv_added =
          List.fold_left
            (fun (lm, sv_added) sv ->
              if SvSet.mem sv sv_added then lm, sv_added
              else sv_add sv Ntaddr lm, SvSet.add sv sv_added)
            (lmem_empty, SvSet.empty) (i :: ptr_pars) in
        let lm =
          SvSet.fold (fun i -> List_utils.colv_add ~root:true i CK_set)
            x.t_mem.lm_colvroots lm in
        let lm =
          List.fold_left
            (fun lm i ->
              if SvSet.mem i lm.lm_colvroots then lm
              else List_utils.colv_add i CK_set lm
            ) lm set_pars in
        let lm =
          List.fold_left
            (fun lm i ->
              if SvSet.mem i lm.lm_colvroots then lm
              else List_utils.colv_add i CK_seq lm
            ) lm seq_pars in
        let lc = { lc_def  = ld;
                   lc_ptrargs = ptr_pars;
                   lc_setargs = set_pars;
                   lc_seqargs = seq_pars } in
        let inj =
          List.fold_left (fun acc i -> Aa_maps.add i i acc)
            Aa_maps.empty (i :: lc.lc_ptrargs) in
        match icro with
        | None -> list_edge_add i lc lm, inj
        | Some (icr, j) ->
            assert (icr.ic_name = ic.ic_name);
            let ptr_pars =
              match icr.ic_pars with
              | None -> [ ]
              | Some ind_pars ->
                  assert (ind_pars.ic_seq = [ ] && ind_pars.ic_set = [ ]);
                  List.fold_left
                    (fun acc (i, off) -> assert (Offs.is_zero off); i :: acc)
                    [ ] (List.rev ind_pars.ic_ptr) in
            let lm, sv_added =
              List.fold_left
                (fun (lm, sv_added) sv ->
                  if SvSet.mem sv sv_added then lm, sv_added
                  else sv_add sv Ntaddr lm, SvSet.add sv sv_added)
                (lm, sv_added) (j :: ptr_pars) in
            let lsegc = { lc_def = ld;
                          lc_ptrargs = ptr_pars } in
            let inj = List.fold_left
                (fun acc i -> Aa_maps.add i i acc)
                inj (j :: lsegc.lc_ptrargs) in
            lseg_edge_add i j lc lsegc lm, inj in
      let create_inj (x: t): colv_emb =
        let cinj =
          SvSet.fold
            (fun i inj ->
              assert (Dv.colv_is_root i x.t_num);
              let ck =
                SvMap.find i x.t_mem.lm_colvkind
                |> col_par_type_to_kind
              in
              Aa_maps.add i (i, ck) inj
            ) (colv_roots x.t_mem) Aa_maps.empty in
        cinj in
      let check_res (r: is_le_res) (x: t) =
        match r with
        | `Ilr_not_le -> false
        | `Ilr_le (ilrem, _) ->
            (* Set et Seq constraints are all checked in is_le *)
            Log.info "set constraints: %d" (List.length ilrem.ilr_setctr_r);
            Log.info "seq constraints: %d" (List.length ilrem.ilr_seqctr_r);
            if ilrem.ilr_setctr_r != [ ] || ilrem.ilr_seqctr_r != [ ] then
              Log.info "ind_check:\n%a" (t_fpri "   ") x;
            let res =
              List.fold_left
                (fun acc c ->
                  let fv i = SvMap.find i ilrem.ilr_sv_rtol in
                  let fsetv i =
                    let s, _ = SvMap.find i ilrem.ilr_colv_rtol in
                    if s = SvSet.empty then raise Not_found
                    else SvSet.min_elt s in
                  try
                    let cc = s_cons_map fv fsetv c in
                    let is_sat = Dv.set_sat cc x.t_num in
                    if not is_sat then Log.warn "constraint failed";
                    acc && is_sat
                  with Not_found ->
                    Log.warn "not found, discarding constraint!";
                    acc
                ) true ilrem.ilr_setctr_r in
            List.fold_left
              (fun acc c ->
                let fv i = SvMap.find i ilrem.ilr_sv_rtol in
                let fseqv i =
                  let s, _ = SvMap.find i ilrem.ilr_colv_rtol in
                  if s = SvSet.empty then raise Not_found
                  else SvSet.min_elt s in
                try
                  let cc = q_cons_map fv fseqv c in
                  let is_sat = Dv.seq_sat cc x.t_num in
                  if not is_sat then Log.warn "constraint failed";
                  acc && is_sat
                with Not_found ->
                  Log.warn "not found, discarding constraint!";
                  acc
              ) res ilrem.ilr_seqctr_r
      in
      match op with
      | SL_set sp ->
          let sp = tr_setcon sp in
          Dv.set_sat sp x.t_num
      | SL_seq qp ->
          let qp = tr_seqcon qp in
          Dv.seq_sat qp x.t_num
      | SL_seg (icl, (i, offl), icr, (j, offr)) ->
          sanity_check "seg_ind_check,before" x;
          assert (gen_ind_call_list_dom_support icl);
          assert (Offs.is_zero offl);
          assert (gen_ind_call_list_dom_support icr);
          assert (Offs.is_zero offr);
          let tref, inj = create_mem (icl, i) (Some (icr, j)) in
          let sinj = create_inj x in
          let r =
            List_is_le.is_le_weaken_check x.t_mem
              SvSet.empty SvSet.empty ~vsat_l:(make_vsat x) tref inj sinj in
          check_res r x
      | SL_ind (ic, (i, off)) ->
          sanity_check "ind_check,before" x;
          assert (gen_ind_call_list_dom_support ic);
          assert (Offs.is_zero off);
          let tref =
            let ld = ind_extract_list ic.ic_name in
            let ptr_pars, set_pars, seq_pars =
              match ic.ic_pars with
              | None -> [ ], [ ], [ ]
              | Some ind_pars ->
                  List.fold_left
                    (fun acc (i, off) -> assert (Offs.is_zero off); i :: acc)
                    [ ] (List.rev ind_pars.ic_ptr),
                  List.fold_left
                    (fun acc i -> sv_unsafe_of_int i.colv_uid :: acc)
                    [ ] (List.rev ind_pars.ic_set),
                  List.fold_left
                    (fun acc i -> sv_unsafe_of_int i.colv_uid :: acc)
                    [ ] (List.rev ind_pars.ic_seq) in
            let lm = sv_add i Ntraw lmem_empty in
            (* TODO: check why this is done only for sets *)
            (* Because we should use typing information from lm_colvkind ! *)
            let lm =
              SvSet.fold (fun i -> List_utils.colv_add ~root:true i CK_set)
                x.t_mem.lm_colvroots lm in
            let lm =
              List.fold_left
                (fun lm i ->
                  if SvSet.mem i lm.lm_colvroots then lm
                  else List_utils.colv_add i CK_set lm
                ) lm set_pars in
            (*let lm =
              SvSet.fold (List_utils.seqv_add true) x.t_mem.lm_seqvroots lm in*)
            (* ^^ This is where the Seq COLV were added in the list memory*)
            let lm =
              List.fold_left
                (fun lm i ->
                  if SvSet.mem i lm.lm_colvroots then lm
                  else List_utils.colv_add i CK_seq lm
                ) lm seq_pars in
            let lc = { lc_def  = ld;
                       lc_ptrargs = ptr_pars ;
                       lc_setargs = set_pars;
                       lc_seqargs = seq_pars } in
            list_edge_add i lc lm in
          let inj = Aa_maps.add i i Aa_maps.empty in
          let cinj = create_inj x in
          let r =
            List_is_le.is_le_weaken_check x.t_mem
              SvSet.empty SvSet.empty ~vsat_l:(make_vsat x) tref inj cinj in
          begin
            match r with
            | `Ilr_not_le -> false
            | `Ilr_le (ilrem, _) ->
                (* Set et Seq constraints are all checked in is_le *)
                Log.info "set constraints: %d" (List.length ilrem.ilr_setctr_r);
                Log.info "seq constraints: %d" (List.length ilrem.ilr_seqctr_r);
                if ilrem.ilr_setctr_r != [ ] || ilrem.ilr_seqctr_r != [ ] then
                  Log.info "ind_check:\n%a" (t_fpri "   ") x;
                List.fold_left
                  (fun acc c ->
                    let fv i = SvMap.find i ilrem.ilr_sv_rtol in
                    let fsetv i =
                      let s, _ = SvMap.find i ilrem.ilr_colv_rtol in
                      if s = SvSet.empty then raise Not_found
                      else SvSet.min_elt s in
                    try
                      let cc = s_cons_map fv fsetv c in
                      let is_sat = Dv.set_sat cc x.t_num in
                      if not is_sat then Log.warn "constraint failed";
                      acc && is_sat
                    with Not_found ->
                      Log.warn "not found, discarding constraint!";
                      acc
                  ) true ilrem.ilr_setctr_r
          end
      | SL_array -> Dv.check VC_array x.t_num

    (** Construction from formulas and checking of formulas *)
    let from_s_formula (_: colvar StringMap.t) (f: s_formula) : t * s_pre_env =
      Log.todo_exn "from_s_formula: unsupported"
    let sat_s_formula (_: sv StringMap.t)
        (_: (sv * col_kind * colvar) StringMap.t)
        (f: s_formula) (_: t): bool =
      Log.todo_exn "sat_s_formula: unsupported"

  end: DOM_MEM_LOW)
