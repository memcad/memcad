(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_mat.ml
 **       materialization support for the list abstract domain
 ** Xavier Rival, 2014/03/02 *)
open Lib
open Data_structures
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_sig
open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

open List_utils
open Set_utils
open Seq_utils
open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "l_mat___" and level = Log_level.DEBUG end)

let debug_module = false

(** Unfolding primitive *)
let unfold (i: sv) (only_non_emp: bool) (lm: lmem) (dir: unfold_dir)
  : unfold_result list =
  (* Computation of the set of setv to remove *)
  let colv_to_remove (lc: l_call) (lm: lmem) =
    let colv_rem_acc (k: col_kind) (l: sv list) (acc: col_kinds): col_kinds =
      List.fold_left
        (fun acc i ->
          if colv_is_root lm i then acc
          else SvMap.add i k acc
        ) acc l in
    colv_rem_acc CK_seq lc.lc_seqargs
      (colv_rem_acc CK_set lc.lc_setargs SvMap.empty) in
  (* Creation of a single points-to edge at origin point *)
  let create_pt
      (src: sv)
      (nt: ntyp)
      (off: int)
      (doff: int)
      (size: int)
      ?(dest = None)
      (x: lmem)
    : sv * lmem =
    let j, x = match dest with
      | None -> sv_add_fresh nt x
      | Some j -> j, x in
    let bn = src, Bounds.of_int off in
    let pe = { pe_size = Offs.size_of_int size;
               pe_dest = j, Offs.of_int doff } in
    j, pt_edge_block_append ~nochk:true bn pe x in
  (* Add the pointer edges and prepare for the inductive edges *)
  (*  returns:
   *   - mapping of offsets to svs
   *   - list of new calls
   *   - new memory state
   *   - setv kinds *)
  let create_ld_block (src: sv) (ld: l_def) (lc: l_call)
      (odest: sv option) (lm: lmem)
      : sv IntMap.t * (sv * l_call) list * lmem * col_kinds =
    let gen_real_ptrpar (lm: lmem) (dp: ptri option)
        (actual: sv list) (off_2nid: sv IntMap.t)
        : (sv list) =
      match dp with
      | None -> []
      | Some ptri ->
          let params =
            List.map
              (fun ptr_var ->
                match ptr_var with
                | List_sig.Pv_this -> i
                | List_sig.Pv_actual i -> List.nth actual i
                | List_sig.Pv_field off -> IntMap.find off off_2nid
              ) (List.rev ptri.p_params) in
          params in
    (* TODO: share the two functions below (and also types seti/seqi) *)
    (* Create real set argument for l_call *)
    (*  returns:
     *   - reversed list of arguments
     *   - new memory state
     *   - setv kinds *)
    let gen_real_setpar (lm: lmem) (ds: seti option) (ncolvs: col_kinds)
        : (sid list) * lmem * col_kinds =
      match ds with
      | None -> [ ], lm, ncolvs
      | Some sel ->
          let params, lm, ncolvs, _ =
            List.fold_left
              (fun (acc, lm, ncolvs, i) skind ->
                let setv, lm = colv_add_fresh (Ct_set (Some skind)) lm in
                if debug_module then
                  Log.debug "freshly generated setvar: %a" setv_fpr setv;
                setv :: acc, lm, SvMap.add setv CK_set ncolvs, i + 1
              ) ([ ], lm, ncolvs, 0) (List.rev sel.s_params) in
          params, lm, ncolvs in
    (* Create real seq argument for l_call *)
    (*  returns:
     *   - reversed list of arguments
     *   - new memory state
     *   - seqv kinds *)
    let gen_real_seqpar (lm: lmem) (ds: seqi option) (ncolvs: col_kinds)
        : (sid list) * lmem * col_kinds =
      match ds with
      | None -> [ ], lm, ncolvs
      | Some sel ->
          let params, lm, ncolvs, _ =
            List.fold_left
              (fun (acc, lm, ncolvs, i) skind ->
                let seqv, lm = colv_add_fresh (Ct_seq (Some skind)) lm in
                if debug_module then
                  Log.debug "freshly generated seqvar: %a" seqv_fpr seqv;
                seqv :: acc, lm, SvMap.add seqv CK_seq ncolvs, i + 1
              ) ([ ], lm, ncolvs, 0) (List.rev sel.q_params) in
          params, lm, ncolvs in
    (* - off_2nid map from offset to new created sv, at first it is empty
     * - lind_edges store the list information need be add
     * - create a pointer edge the destination node of which
     *   should carry the list inductive *)
    let add_pt_edge
        (off: int)
        (ldd: l_def)
        (doff: int)
        (lm: lmem)
        (off_2nid: sv IntMap.t)
        (ncolvs: col_kinds)
        : (int * sv) * (sv * l_call) * lmem * col_kind SvMap.t =
      let n_id, lm =
        if dir = Udir_bwd && ldd = ld then
          let dest = match odest with
            | Some d -> d
            | None -> assert false in
          create_pt src Ntaddr off doff Flags.abi_ptr_size ~dest:(Some dest) lm
        else
          create_pt src Ntaddr off doff Flags.abi_ptr_size lm in
      let ptrargs = gen_real_ptrpar lm ldd.ld_ptr [] off_2nid in
      (* JG:TODO extract off_2nid before creating recursive call ! *)
      let setargs, lm, ncolvs = gen_real_setpar lm ldd.ld_set ncolvs in
      let seqargs, lm, ncolvs = gen_real_seqpar lm ldd.ld_seq ncolvs in
      let lc = { lc_def = ldd ;
                 lc_ptrargs = ptrargs ;
                 lc_setargs = setargs ;
                 lc_seqargs = seqargs } in
      (off, n_id), (n_id, lc), lm, ncolvs in
    (* - creation of all pointer edges according to the order of offset *)
    let l_cell: l_cell = IntMap.empty in
    (* First we add recursive & nested calls *)
    let l_cell =
      List.fold_left
        (fun acc (off, ld, doff) -> IntMap.add off (Call (ld, doff)) acc)
        l_cell ((ld.ld_nextoff, ld, ld.ld_nextdoff) :: ld.ld_onexts) in
    (* Then we add cells [this->off |-> @pi] *)
    let l_cell =
      match ld.ld_ptr with
      | None -> l_cell
      | Some { p_off = p_off } ->
          IntMap.fold
            (fun off (ptr, doff) acc ->
              match List.nth_opt lc.lc_ptrargs ptr with
              | Some p -> IntMap.add off (Ptr (p, doff)) acc
              | None ->
                  Log.fatal_exn "%s : Cannot find %ith parameter" __LOC__ ptr
            ) p_off l_cell in
    (* then we add «content» cell *)
    let l_cell =
      let rec aux curr_off l_cell acc =
        if curr_off > ld.ld_size then
          Log.fatal_exn "%s : exceeding cell size !" __LOC__
        else if curr_off = ld.ld_size then
          acc
        else if IntMap.is_empty l_cell then
          IntMap.add curr_off (Data (ld.ld_size - curr_off)) acc
        else if fst @@ IntMap.min_binding l_cell = curr_off then
          let l_cell = IntMap.remove curr_off l_cell in
          let new_off = curr_off + Flags.abi_ptr_size in
          aux new_off l_cell acc
        else
          let next_off, _ = IntMap.min_binding l_cell in
          let size = next_off - curr_off in
          let acc = IntMap.add curr_off (Data size) acc in
          let new_off = next_off + Flags.abi_ptr_size in
          let l_cell = IntMap.remove next_off l_cell in
          aux new_off l_cell acc in
      aux 0 l_cell l_cell in
    let off_2nid, lind_edges, lm, ncolvs =
      IntMap.fold
        (fun off content (off_2nid, lind_edges, lm, ncolvs) ->
          match content with
          | Call (ldd, doff) ->
              let (off, n1), x, lm, ncolvs =
                add_pt_edge off ldd doff lm off_2nid ncolvs in
              let off_2nid = IntMap.add off n1 off_2nid in
              off_2nid, (x :: lind_edges), lm, ncolvs
          | Data size ->
              let j, lm = create_pt src Ntraw off 0 size lm in
              let off_2nid = IntMap.add off j off_2nid in
              off_2nid, lind_edges, lm, ncolvs
          | Ptr (p, doff) ->
              let j, lm =
                create_pt src Ntaddr off doff Flags.abi_ptr_size
                  ~dest:(Some p) lm in
              let off_2nid = IntMap.add off j off_2nid in
              off_2nid, lind_edges, lm, ncolvs
        ) l_cell (IntMap.empty, [ ], lm, SvMap.empty) in
    off_2nid, List.rev lind_edges, lm, ncolvs in
  let ptrpar_2_real (dp: ptri option) (actual: sv list) (x: lmem)
      : (sv list * lmem) =
    match dp with
    | None -> [], x
    | Some ptri ->
        let params, x =
          List.fold_left
            (fun (ptrargs, x) ptr_var ->
              match ptr_var with
              | List_sig.Pv_this ->
                  let nid, x = sv_add_fresh Ntaddr x in
                  nid :: ptrargs, x
              | List_sig.Pv_actual i ->
                  List.nth actual i :: ptrargs, x
              | List_sig.Pv_field off ->
                  Log.fatal_exn "Cannot determine field in backward unfolding"
            ) ([], x) (List.rev ptri.p_params) in
        params, x in
  (* - add the set constraints in ld_def to set_cons
   * - p is the set args of the inductive egde needed unfold *)
  let setequ_2_real (this: sv) (of_2n: sv IntMap.t) (p: sid list)
      (l: (sv * l_call) list) (se: set_equa)
      : set_cons =
    (* deal with offset to symbolic variables *)
    let selt_2_real (e: set_elt): sv =
      match e with
      | Se_this -> this
      | Se_field f ->
          try IntMap.find f of_2n
          with Not_found -> Log.fatal_exn "offset without match sv" in
    (* deal with set var in ld_def to real set args *)
    let setv_2_real (s: set_var): sid =
      match s with
      | Sv_actual x ->
          begin
            try List.nth p x
            with
            | Failure _ ->
                Log.fatal_exn "current: cannot find real set parameter"
          end
      | Sv_nextpar x ->
         begin
           match l with
           | [] -> Log.fatal_exn "next: cannot find next ldesc"
           | (_, lst) :: _ ->
               try List.nth lst.lc_setargs x
               with
               | Failure _ ->
                   Log.fatal_exn "nth: cannot find set arguments of next"
          end
      | Sv_subpar (x, y) ->
          begin
            try
              let _, n = List.nth l (x+1) in
              List.nth n.lc_setargs x (* JG:TODO *)
            with
            | Failure _ -> Log.fatal_exn "nth: cannot find nth ldesc"
          end in
    let setdef_2_real (sd: set_def): set_expr =
      match sd with
      | Sd_var v -> S_var (setv_2_real v)
      | Sd_uplus (e, v) ->
          S_uplus (S_node (selt_2_real e), S_var (setv_2_real v))
      | Sd_eunion (e, v) ->
          S_union (S_node (selt_2_real e), S_var (setv_2_real v))
      | Sd_union (v0, v1) ->
          S_union (S_var (setv_2_real v0), S_var (setv_2_real v1)) in
    match se with
    | Se_mem (x, y) -> S_mem (selt_2_real x, setdef_2_real y)
    | Se_eq  (x, y) -> S_eq (S_var (setv_2_real x), setdef_2_real y) in
  let setequ_2_real (this: sv) (of_2n: sv IntMap.t) (p: sid list)
      (l: (sv * l_call) list) (se: set_equa)
      : set_cons =
    let r = setequ_2_real this of_2n p l se in
    if debug_module then
      Log.debug "unfold %a => %a" set_equa_fpr se Set_utils.set_cons_fpr r;
    r in
  let add_setcons (c: set_cons) (l: set_cons list): set_cons list =
    match c with (* pruning empty constraints *)
    | S_eq (S_var x, S_var y) -> if x = y then l else c :: l
    | _ -> c :: l in
  let seqequ_2_real (this: sv) (of_2n: sv IntMap.t) (p: sid list)
      (l: (sv * l_call) list) (se: seq_equa)
      : seq_cons =
    (* deal with offset to symbolic variables*)
    let seq_val_2_real (se: seq_elt) : sv =
      match se with
      | Qe_this -> this
      | Qe_field f ->
          try IntMap.find f of_2n
          with Not_found -> Log.fatal_exn "offset without match sv" in
    let seq_var_2_real (seqv: seq_var) : sv =
      match seqv with
      | Qv_actual x ->
          begin
            try List.nth p x
            with
            | Failure _ ->
                Log.fatal_exn "current: cannot find real seq parameter"
          end
      | Qv_nextpar x ->
          begin
            match l with
            | [] -> Log.fatal_exn "next: cannot find next ldesc"
            | (_, lst) :: _ ->
                try List.nth lst.lc_seqargs x
                with
                | Failure _ ->
                    Log.fatal_exn "nth: cannot find set arguments of next"
          end in
    let rec inv_seq_def (sd: seq_def) : seq_def =
      match sd with
      | Qd_empty | Qd_val _ | Qd_var _ -> sd
      | Qd_concat l ->
          let l = l |> List.rev |> List.map inv_seq_def in
          Qd_concat l
      | Qd_sort sd ->
        Qd_sort (inv_seq_def sd) in
    let rec seq_def_2_real (sd: seq_def) : seq_expr =
      match sd with
      | Qd_empty -> Seq_empty
      | Qd_val x -> Seq_val (seq_val_2_real x)
      | Qd_var x -> Seq_var (seq_var_2_real x)
      | Qd_sort sd -> Seq_sort (seq_def_2_real sd)
      | Qd_concat l -> Seq_Concat (List.map seq_def_2_real l) |> normalise  in
    match se with
    | Qe_eq (v, sd) ->
        match dir with
        | Udir_fwd ->
            Seq_equal (Seq_var (seq_var_2_real v), seq_def_2_real sd)
        | Udir_bwd -> (* For bwd unfolding we have to change seq constraint *)
            Seq_equal (Seq_var (seq_var_2_real v),
                       seq_def_2_real @@ inv_seq_def sd) in
  let add_seqcons (c: seq_cons) (l: seq_cons list): seq_cons list =
    match c with (* pruning empty constraints *)
    | Seq_equal (Seq_var x, Seq_var y) when x = y -> l
    | _ -> c :: l in
  let ln = sv_find i lm in
  (* general construction of the empty case *)
  let make_empty (lc: l_call) remcolvs (oidst: (sv * lseg_call) option) =
    let lm, cons =
      match oidst with
      | Some (idst, lsegc) ->
          let lm = lseg_edge_rem i lm in
          let cons =
            List.map2
              (fun ptr_left ptr_right ->
                Nc_cons (Apron.Tcons1.EQ, Ne_var ptr_left, Ne_var ptr_right))
              (i::lc.lc_ptrargs) (idst::lsegc.lc_ptrargs) in
          lm, cons
      | None ->
          list_edge_rem i lm,
          [Nc_cons (Apron.Tcons1.EQ, Ne_csti lc.lc_def.ld_emp_csti, Ne_var i) ]
    in
    let setparas =
      match lc.lc_def.ld_set with
      | None -> []
      | Some x -> x.s_params in
    let setcons =
      try
        List.fold_left2
          (fun setcons i j ->
            if j.st_add || j.st_head then
              S_eq (S_var i, S_empty) :: setcons
            else setcons
          ) [ ] lc.lc_setargs setparas
      with Invalid_argument _ ->
        Log.fatal_exn "list_unfold: set parameters does not match" in
    let seqparas =
      match lc.lc_def.ld_seq with
      | None -> []
      | Some x -> x.q_params in
    let seqcons =
      try
        List.fold_left2
          (fun seqcons i j ->
            if j.sq_add then
              Seq_equal (Seq_var i, Seq_empty) :: seqcons
            else seqcons
          ) [ ] lc.lc_seqargs seqparas
      with Invalid_argument _ ->
        Log.fatal_exn "list_unfold: seq parameters does not match" in
    { ur_lmem     = lm;
      ur_cons     = cons;
      ur_mcons    = lc.lc_def.ld_emp_mcons;
      ur_setcons  = setcons;
      ur_seqcons  = seqcons;
      ur_newcolvs = SvMap.empty;
      ur_remcolvs = remcolvs } in
  match ln.ln_e with
  | Lhpt _ -> Log.fatal_exn "unfold to empty / points-to"
  | Lhemp -> Log.fatal_exn "non-local unfold"
  | Lhlist lc ->
      assert (dir = Udir_fwd);
      let remcolvs = colv_to_remove lc lm in
      (* empty segment *)
      let ur_empty = make_empty lc remcolvs None in
      (* inductive of length at least 1 *)
      let ur_next =
        let ln = { ln with ln_e = Lhemp } in
        let lm = { lm with lm_mem = SvMap.add i ln lm.lm_mem } in
        let o2n, n2l, lm, ncolvs = create_ld_block i lc.lc_def lc None lm in
        (* JG:TODO : cf. remarque sur l'appel de la fonction ! *)
        let lm =
          List.fold_left (fun lm (i, ld) -> list_edge_add i ld lm) lm n2l in
        let setcons =
          match lc.lc_def.ld_set with
          | None -> [ ]
          | Some x ->
              List.fold_left
                (fun setcons l ->
                  add_setcons (setequ_2_real i o2n lc.lc_setargs n2l l) setcons
                ) [ ] x.s_equas in
        let seqcons =
          match lc.lc_def.ld_seq with
          | None -> [ ]
          | Some x ->
              List.fold_left
                (fun seqcons l ->
                  add_seqcons (seqequ_2_real i o2n lc.lc_seqargs n2l l) seqcons
                ) [ ] x.q_equas in
        { ur_lmem     = lm;
          ur_cons     = [ Nc_cons (Apron.Tcons1.DISEQ,
                                   Ne_csti lc.lc_def.ld_emp_csti, Ne_var i) ];
          ur_mcons    = lc.lc_def.ld_next_mcons;
          ur_setcons  = setcons;
          ur_seqcons  = seqcons;
          ur_newcolvs = ncolvs;
          ur_remcolvs = remcolvs } in
      [ ur_next; ur_empty ]
  | Lhlseg (lc, idst, lsegc) ->
      let remcolvs = colv_to_remove lc lm in
      (* empty segment *)
      let ur_empty = make_empty lc remcolvs (Some (idst,lsegc)) in
      (* segment of length at least 1 *)
      let ur_next =
        (* TODO: check if there is an issue with this list edge *)
        let n_lm = lseg_edge_rem i lm in
        (* First we determine the source : the nid of unfolded block *)
        let src = match dir with
          | Udir_fwd -> i
          | Udir_bwd ->
              match lc.lc_def.ld_prevpar with
              | None ->
                  Log.fatal_exn
                    "trying to backward unfold list without prev field"
              | Some i -> List.nth lsegc.lc_ptrargs i in
        (* When we unfold backward, the ptrargs are the args *at the end* *)
        let n_lc, n_lm = match dir with
          | Udir_fwd -> lc, n_lm
          | Udir_bwd ->
              let ptrargs, n_lm =
                ptrpar_2_real lsegc.lc_def.ld_ptr lsegc.lc_ptrargs n_lm in
              { lc with lc_ptrargs = ptrargs }, n_lm in
        let o2n, n2l, n_lm, ncolvs =
          create_ld_block src lc.lc_def n_lc (Some idst) n_lm in
        (* JG:TODO Idem *)
        let o_ff, o_lc = List.hd n2l in
        let o_ff, idst, o_lc, o_lsegc, n_lm =
          match dir with
          | Udir_fwd -> o_ff, idst, o_lc, lsegc, n_lm
          | Udir_bwd ->
              i, src,
              { o_lc with lc_ptrargs = lc.lc_ptrargs },
              { lsegc with lc_ptrargs = n_lc.lc_ptrargs },
              n_lm in
        let n_lm = lseg_edge_add o_ff idst o_lc o_lsegc n_lm in
        let n_lm =
          List.fold_left (fun m (i, lc) -> list_edge_add i lc m)
            n_lm (List.tl n2l) in
        let setcons =
          match lc.lc_def.ld_set with
          | None -> [ ]
          | Some x ->
              List.fold_left
                (fun setcons l ->
                  add_setcons (setequ_2_real src o2n lc.lc_setargs n2l l)
                    setcons
                ) [ ]  x.s_equas in
        let seqcons =
          match lc.lc_def.ld_seq with
          | None -> [ ]
          | Some x ->
              List.fold_left
                (fun seqcons l -> (* JG:TODO : idem *)
                  add_seqcons (seqequ_2_real src o2n lc.lc_seqargs n2l l)
                    seqcons
                ) [ ] x.q_equas in
        { ur_lmem     = n_lm ;
          ur_cons     = [ Nc_cons (Apron.Tcons1.DISEQ,
                                   Ne_csti lc.lc_def.ld_emp_csti, Ne_var i) ] ;
          ur_mcons    = lc.lc_def.ld_next_mcons;
          ur_setcons  = setcons;
          ur_seqcons  = seqcons;
          ur_newcolvs = ncolvs;
          ur_remcolvs = remcolvs } in
      if only_non_emp then [ ur_next ]
      else [ ur_next ; ur_empty ]
