(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_gen_functor.ml
 **       functor producing types for inductive predicaates and specifications
 ** Xavier Rival, 2023/06/08 *)
open Data_structures
open Lib

open Ind_form_sig


(** Parameters *)
module type FORMPAR =
  sig
    (** Parameter types *)
    type offset
    (** Utilities *)
    val offset_to_int_opt: offset -> int option
    (** Printers *)
    val offset_fpr: form -> offset -> unit
    val offset_dest_fpr: form -> offset -> unit
  end


(** Functor to generate elements of inductive definitions/specifications *)
module MakeSymForms = functor (P: FORMPAR) ->
  struct
    (** Main types (in particular for inductive predicates *)
    (** Heap *)
    (* Cell *)
    type cell =
      { ic_off:  P.offset;         (* offset *)
        ic_size: int;              (* size, in bytes *)
        ic_dest: formal_arith_arg; (* destination *)
        ic_doff: P.offset;         (* offset in the destination node *) }
    (* Heap atom *)
    type heapatom =
      | Hacell of cell            (* a cell, to unfold into a points-to edge *)
      | Haind of indcall  (* an inductive, to unfold into segment of inductive *)
      | Haseg of indcall * indcall (* Haseg (A, B) is segment A *= B *)
    (* Heap formula; separating conjunction of heap atoms *)
    type hform =
      heapatom list (* separating conjunction *)
    (** Pure components *)
    (* Path expression *)
    type pathstr =
      | Pe_epsilon                  (* empty string *)
      | Pe_seg of P.offset list     (* star pattern access path *)
      | Pe_single of P.offset list  (* single step access path *)
    (* Path formula *)
    type apath = formal_ptr_arg * pathstr * formal_ptr_arg
    (* Pure formula *)
    type pformatom =
      | Pf_alloc of int   (* size allocated bytes from root pointer *)
      | Pf_set of sform   (* set predicates *)
      | Pf_seq of qform   (* sequences predicates *)
      | Pf_arith of aform (* arithmethic predicates *)
      | Pf_path of apath  (* access path predicates *)
    type pform = pformatom list (* conjunction *)
    (** Types to be used only for spec *)
    type s_sval = string
    type s_off =
      | So_int of P.offset
      | So_sval of int * s_sval * P.offset   (* (k,sv,o) means k*sv + o *)
    type s_range = s_off * s_off (* base offset + size *)
    (** Printers *)
    open Ind_form_utils
    let cell_fpr (fmt: form) (c: cell): unit =
      let arrow =
        if c.ic_size = 4 then "|->" else Printf.sprintf "|-%d->" c.ic_size in
      F.fprintf fmt "this->%a %s %a%a" P.offset_fpr c.ic_off arrow
        formal_arith_arg_fpr c.ic_dest P.offset_dest_fpr c.ic_doff
    let heapatom_fpr (fmt: form): heapatom -> unit = function
      | Hacell c -> cell_fpr fmt c
      | Haind  i -> indcall_fpr fmt i
      | Haseg (i0, i1) -> F.fprintf fmt "%a *= %a" indcall_fpr i0 indcall_fpr i1
    let hform_fpr: form -> hform -> unit = gen_list_fpr "emp" heapatom_fpr " * "
    let pathstr_fpr (fmt: form) (pstr: pathstr): unit =
      match pstr with
      | Pe_epsilon -> F.fprintf fmt "~"
      | Pe_seg flds ->
          F.fprintf fmt "(%a)^*" (gen_list_fpr "" P.offset_fpr "+") flds
      | Pe_single flds ->
          F.fprintf fmt "(%a)" (gen_list_fpr "" P.offset_fpr "+") flds
    let apath_fpr (fmt: form) (p: apath): unit =
      let (sc, pt, dt) = p in
      F.fprintf fmt "(%a, %a, %a)" formal_ptr_arg_fpr sc
        pathstr_fpr pt formal_ptr_arg_fpr dt
    let pformatom_fpr (fmt: form): pformatom -> unit = function
      | Pf_alloc sz -> F.fprintf fmt "alloc( this, %d )" sz
      | Pf_set f -> sform_fpr fmt f
      | Pf_seq f -> qform_fpr fmt f
      | Pf_arith f -> aform_fpr fmt f
      | Pf_path f -> apath_fpr fmt f
    let pform_fpr: form -> pform -> unit = gen_list_fpr "" pformatom_fpr " & "
    let s_sval_fpr (fmt: form) (sv: s_sval): unit =
      F.fprintf fmt "%s" sv
    let s_off_fpr (fmt: form) (so: s_off): unit =
      match so with
      | So_int i -> P.offset_fpr fmt i
      | So_sval (k, sv, o) ->
          F.fprintf fmt "%d * %a + %a" k s_sval_fpr sv P.offset_fpr o
    let s_range_fpr (fmt: form) ((base,size): s_range): unit =
      let default () =
        F.fprintf fmt "->%a[%a]" s_off_fpr base s_off_fpr size in
      match base, size with
      | So_int b, So_int s ->
          if P.offset_to_int_opt b = Some 0
              && P.offset_to_int_opt s = Some 4 then ()
          else default ()
      | base, So_int s ->
          if P.offset_to_int_opt s = Some 4 then
            F.fprintf fmt "->%a" s_off_fpr base
          else default ()
      | base, size -> default ()
  end
