(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_mem_low_sum_sep.ml
 **       Low level memory abstraction based on separating conjunction
 **       summaries, with predicates defined by extension over sets
 ** Vincent Rebiscoul and Xavier Rival, 2019/03/04 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def
open Timer

open Ast_sig
open Col_sig
open Dom_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Spec_sig
open Sum_sep_sig
open Svenv_sig
open Vd_sig

open Dom_utils
open Sum_sep_utils
open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "dm_sp___" and level = Log_level.DEBUG end)

(** The shape domain *)
module DBuild = functor (Dv: DOM_VALCOL) ->
  (struct
    (** Module name *)
    let module_name = "[sepsum]"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name Dv.module_name Dv.config_fpr ()
    (** Dom ID *)
    let dom_id: mod_id ref = ref (sv_unsafe_of_int (-1),"sepsum")

    (** Type of abstract elements *)
    type t =
        { (* memory contents *)
          t_mem:    smem;
          (* abstraction of values *)
          t_num:    Dv.t;
          (* local modification *)
          t_svemod: svenv_mod }

    (** Domain initialization to a set of inductive definitions *)

    (* Domain initialization to a set of inductive definitions *)
    let init_inductives (g: SvGen.t) (s: StringSet.t): _ =
      if s != StringSet.empty then Log.fatal_exn "init_inductives not allowed";
      let g = Dv.init_inductives g s in
      let g, k = SvGen.gen_key g in (* this domain generates keys *)
      if sv_upg then dom_id := k, snd !dom_id;
      g
    let inductive_is_allowed (_: string): bool = false

    (** Fixing sets of keys *)
    let sve_sync_bot_up (x: t): t * svenv_mod =
      Log.todo "sve_sync_bot_up";
      x, svenv_empty

    (* Sanity check *)
    let sanity_sv (_: SvSet.t) (x: t): bool =
      Log.todo "sanity_sv: unimp";
      true

    (** Lattice elements *)

    (* Bottom element *)
    let bot: t =
      { t_mem    = smem_empty;
        t_num    = Dv.bot;
        t_svemod = svenv_empty; }
    let is_bot (x: t): bool =
      Log.todo "is_bot: unimp";
      false

    (* Top element, with empty set of roots *)
    let top (): t =
      { t_mem    = smem_empty;
        t_num    = Dv.top;
        t_svemod = svenv_empty }

    (* Pretty-printing *)
    let t_fpri ind f x = Log.todo_exn "todo: t_fpri"

    (* External output *)
    let ext_output (_: output_format) (_: string) (_: namer) (_: t): unit =
      Log.todo_exn "todo: ext_output"


    (** Management of symbolic variables *)

    (* Will that symb. var. creation be allowed by the domain? *)
    let sv_is_allowed ?(attr: node_attribute = Attr_none) (nt: ntyp)
        (na: nalloc) (x: t): bool =
      Log.todo_exn "todo: sv_is_allowed"

    (* Add a symbolic variable, with a newly generated id
     *  (by default, not considered root by the underlying domain) *)
    let sv_add_fresh
        ?(attr: node_attribute = Attr_none) ?(root: bool = false)
        (t: ntyp) (na: nalloc) (x: t): sv * t =
      Log.todo_exn "sv_add_fresh"

    (* Recover information about a symbolic variable *)
    (* For now, only nalloc and ntyp *)
    let sv_get_info (i: sv) (x: t): nalloc option * ntyp option =
      Log.todo_exn "sv_get_info"

    (* Garbage collection *)
    let gc (roots: sv uni_table) (x: t): t =
      x (* nothing to do for GC in this module (!!! for now !!!) *)

    (* graph encode *)
    let encode (disj: sv) (n: namer) (x: t)
        : renamed_path list * IntSvPrSetSet.t * sv =
      (* no need for encode in this module *)
      Log.todo_exn "encode"


    (** Comparison and Join operators *)

    (* Checks if the left argument is included in the right one *)
    let is_le (roots_rel: sv bin_table) (_: colv_emb)
        (xl: t) (xr: t): svenv_upd option =
      Log.todo "is_le";
      None

    (* Generic comparison (does both join and widening) *)
    let join (j: join_kind) (hso: sv hint_bs option)
        (lso: (Offs.svo lint_bs) option)
        (roots_rel: sv tri_table)
        (colroots_rel: (sv, col_kind) tri_map)
        ((xl, jl): t * join_ele) ((xr, jr): t * join_ele)
        : t * svenv_upd * svenv_upd =
      Log.todo_exn "join"

    (* Directed weakening; over-approximates only the right element *)
    let directed_weakening _ = Log.todo_exn "dw"

    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    let local_abstract (ho: sv hint_us option) (x: t): t =
      Log.todo_exn "local_abstract"


    (** Cell operations *)

    (* Creation *)
    let cell_create ?(attr:node_attribute = Attr_none)
        ((si, so): svo) (sz: Offs.size) (nt: ntyp) (x: t): t =
      Log.todo_exn "cell_create"

    (* Deletion *)
    let cell_delete ?(free:bool = true) ?(root:bool = false)
        (i: sv) (x: t): t =
      Log.todo_exn "cell_delete"

    (* Read the content of a cell *)
    let cell_read
        (is_resolve: bool) (* whether call from cell-resolve  *)
        (src: svo)       (* address of the cell to read *)
        (sz: int)          (* size of the cell to read *)
        (x: t)             (* abstract memory input *)
        : (t * svo * svo option) list =
      Log.todo_exn "cell_read"

    (* Resolve a cell, i.e., materialization *)
    let cell_resolve
        (src: svo) (* address of the cell to resolve *)
        (size: int)  (* size *)
        (x: t)       (* abstract memory input *)
        : (t * svo * bool) list =
      Log.todo_exn "cell_resolve"

    (* Write a cell *)
    let cell_write
        (ntyp: ntyp) (* type of the value being assigned *)
        (dst: svo) (* address of the cell to over-write *)
        (size: int)  (* size of the chunk to write into the memory *)
        (ne: n_expr) (* right hand side as an expression over SVs *)
        (x: t)       (* input abstract memory state *)
        : t (* output abstract memory *) =
      Log.todo_exn "cell_write"


    (** Transfer functions for the analysis *)

    (* Condition test *)
    let guard (c: n_cons) (x: t): t =
      Log.todo_exn "guard"

    (* Checking that a constraint is satisfied *)
    let sat (c: n_cons) (x: t): bool =
      Log.todo_exn "sat"


    (** Set/Seq domain *)

    (* Adding / removing set variables *)
    let colv_add_fresh (_: bool) (_: string) (_: col_kind) (_: t)
        : sv * t * colv_info option =
      Log.todo_exn "colv_add_fresh"
    let colv_delete (_: sv) (_: t): t =
      Log.todo_exn "colv_delete"

    (* Assume construction *)
    let assume (op: meml_log_form) (t: t): t =
      Log.todo_exn "assume"

    (* Check construction, that an inductive be there *)
    let check (op: meml_log_form) (t: t): bool =
      Log.todo_exn "check"

    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    let mark_prio (lv: svo) (x: t) : t =
      Log.todo_exn "%s mark_prio to implement\n" __LOC__

    (** Unfolding, assuming and checking inductive edges *)

    (* Unfold *)
    let ind_unfold (u: unfold_dir) (lv: svo) (t: t): t list =
      Log.todo_exn "ind_unfold (no support for inductives)"

    (** Construction from formulas and checking of formulas *)
    let from_s_formula (_: colvar StringMap.t) (f: s_formula) : t * s_pre_env =
      Log.todo_exn "from_s_formula: unsupported"
    let sat_s_formula (_: sv StringMap.t)
        (_: (sv * col_kind * colvar) StringMap.t)
        (f: s_formula) (_: t): bool =
      Log.todo_exn "sat_s_formula: unsupported"

  end: DOM_MEM_LOW)
