(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_abs.ml
 **       experimental partial unary abstraction algorithm
 ** Xavier Rival, 2014/04/03 *)
open Data_structures
open Flags
open Lib
open Sv_def

open Ast_sig
open Ind_sig
open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

open List_utils
open Sv_utils


(** Improvements to consider:
 *   - document hints, or remove if not needed
 *   - consider reducing segments as well
 **)

(** Error report *)
module Log =
  Logger.Make(struct let section = "l_abs___" and level = Log_level.DEBUG end)

(* Local debug flag *)
let debug_module = false

(** Function trying to perform a weakening at a specific SV i *)
let local_abstract_one
    ~(stop: sv Aa_sets.t option) (* SVs to stop at *)
    (i: sv)                   (* SV considered as a basis for the weakening *)
    (ld: l_call)               (* candidate call *)
    ~(vsat: vsat)              (* satisfiability test function *)
    (m: lmem)
    (rel: sv SvMap.t)    (* used to project out SVs which have been used *)
    : lmem * (sv SvMap.t) * Col_sig.colv_embedding_comp (* COLV mapping *)
      * (set_cons list) * (seq_cons list) =
  if debug_module then
    Log.debug "CALL local abstract-shape at SV %a" sv_fpr i;
  (* Creation of a memory state with only a list edge *)
  let mlist = list_edge_add i ld (sv_add i Ntaddr lmem_empty) in
  let key =
    SvMap.fold
      (fun id _ key ->
        if id <> i then
          SvGen.use_key key id
        else key
      ) rel (mlist.lm_nkey) in
  (* Exclude the source node from the hing *)
  let stop = Option.map (Aa_sets.remove i) stop in
  (* Application of the inclusion check algorithm *)
  let le_res =
    let inj = Aa_maps.singleton i i in
    (* for now, we do not attempt to reduce segments *)
    List_is_le.is_le_weaken_guess ~stop:stop m
      SvSet.empty SvSet.empty (* no segment *)
      ~vsat_l:vsat { mlist with lm_nkey = key }
      inj Aa_maps.empty in
  (* If inclusion finds something, weaken *)
  match le_res with
  | `Ilr_not_le -> m, SvMap.empty, SvMap.empty, [ ], [ ]
  | `Ilr_le_list ilrem ->
      (list_edge_add i ld ilrem.ilr_lmem_l, ilrem.ilr_sv_rtol,
       ilrem.ilr_colv_rtol, ilrem.ilr_setctr_r, ilrem.ilr_seqctr_r)
  | `Ilr_le_lseg (ilrem, it) ->
      let lsegc: lseg_call = { lc_def = ld.lc_def ;
                               lc_ptrargs = [] } in
      (lseg_edge_add i it ld lsegc ilrem.ilr_lmem_l, ilrem.ilr_sv_rtol,
       ilrem.ilr_colv_rtol, ilrem.ilr_setctr_r, ilrem.ilr_seqctr_r)

(** Main weakening function; tries to apply weakening at each point *)
let local_abstract
    ~(stop: sv Aa_sets.t option) (* SVs to stop at *)
    ~(vsat: vsat)              (* satisfiability test function *)
    (m: lmem)
    : lmem * (sv SvMap.t) * Col_sig.colv_embedding_comp
      * (set_cons list) * (seq_cons list) =
  if debug_module then Log.debug "CALL local abstract";
  (* First step: gather candidates for weakening *)
  let candidates =
    SvMap.fold
      (fun i ln acc ->
        match ln.ln_e, ln.ln_odesc with
        | Lhemp, _ | Lhlist _, _ | Lhlseg _, _ | _, None ->
            acc
        | Lhpt _, Some ld ->
            let lc =
              if ld.ld_ptr = None && ld.ld_set = None && ld.ld_seq = None then
                  { lc_def     = ld ;
                    lc_ptrargs = [] ;
                    lc_setargs = [] ;
                    lc_seqargs = [] }
              else Log.todo_exn "get_set_args" in
            SvMap.add i lc acc
      ) m.lm_mem (SvMap.empty) in
  (* 2. attempts to perform weakening for each candidate *)
  SvMap.fold
    (fun i lc (macc, inj_acc, cvinj_acc, set_info_acc, seq_info_acc) ->
      let macc, inj, cvinj, s_info, q_info =
        local_abstract_one ~stop:stop i lc ~vsat macc inj_acc in
      let sub_merge k x y =
        match x, y with
        | None, None | None, Some _ -> y
        | Some _, None -> x
        | Some l, Some r ->
            if l = r then Some l
            else Log.fatal_exn "local_abstract: merge not match" in
      let sub_merge_col _ x y =
        match x, y with
        | None, None | None, Some _ -> y
        | Some _, None -> x
        | Some (l, kl), Some (r, kr) ->
            if kl = kr then Some (SvSet.union l r, kl)
            else Log.fatal_exn "sub_merge_col: col_kinds do not match" in
      let cvinj_acc : Col_sig.colv_embedding_comp = cvinj_acc in
      ( macc,
        SvMap.merge sub_merge inj inj_acc,
        SvMap.merge sub_merge_col cvinj_acc cvinj,
        List.rev_append s_info set_info_acc,
        List.rev_append q_info seq_info_acc )
    ) candidates (m, SvMap.empty, SvMap.empty, [ ], [ ])
