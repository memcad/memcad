%{
(** MemCAD analyzer
 **
 ** ind_parser.mly
 ** inductive definitions parser
 ** Xavier Rival, 2011/05/22 *)
open Data_structures
open Sv_def

open Ast_sig
open C_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open Spec_sig

open Apron
open Array_ind_sig

open Sv_utils

let debug = false

(* open struct ... end is a workaround to prevent type alias to leak
   in the types of the generated .mli file where the alias is not
   defined. See Menhir manual section 4.1.1 for more details. *)

open struct module PS = Parsed_spec end
module AP = Ai_parsed_spec.FAIParsed

let s_henv_init = function
  | None, None -> PS.Se_emp
  | None, Some (a, b) -> PS.Se_map (StringMap.add a b StringMap.empty)
  | Some s, None -> PS.Se_symbols [ s ]
  | Some _, Some _ -> failwith "s_henv_init double definition"
let s_henv_add p env =
  match p, env with
  | (None, None), _ -> env
  | (None, Some (a, b)), PS.Se_emp ->
      PS.Se_map (StringMap.add a b StringMap.empty)
  | (None, Some (a, b)), PS.Se_map env -> PS.Se_map (StringMap.add a b env)
  | (Some s, None), PS.Se_emp -> PS.Se_symbols [ s ]
  | (Some s, None), PS.Se_symbols t -> PS.Se_symbols (s :: t)
  | (None, Some _), PS.Se_symbols _
  | (Some _, None), PS.Se_map _
  | (Some _, Some _), _ -> failwith "s_henv_add double definition"

let so_int i = FSpec.So_int (FPSpec.Numeric (Offs.of_int i))
%}

%token <int>    V_int
%token <int>    V_size_contains
%token <string> V_string

%token <int>    V_par_int
%token <int>    V_par_ptr
%token <int>    V_par_set
%token <int>    V_par_seq
%token <int>    V_par_maya
%token <int>    V_par_nmaya

%token <int>    V_new_var

%token T_semicolon_semicolon

%token (* T_at *) T_arrow T_contains T_defeq T_notequal
%token T_setmem T_setequal

%token T_lsub T_rsub T_semicolon T_setincluded
%token T_lt T_gt T_colon T_comma T_dot T_pipe T_le T_ge
%token T_lbrace T_rbrace T_lbrack T_rbrack T_lpar T_rpar
%token T_equal
%token T_and T_plus T_minus T_star T_segimp T_epsilon T_union
%token T_uplus

%token T_eof

%token T_alloc
%token T_addr
%token T_emp
%token T_int
%token T_prec
%token T_raw
%token T_set
%token T_seq
%token T_sort
%token T_this
%token T_idx
%token T_subi
%token T_subp
%token T_true

%token T_cst
%token T_env
%token T_form
%token T_goal
%token T_heap
%token T_pure

%token T_size
%token T_min
%token T_max

%token T_at_old

%type <Spec_sig.p_iline list>        main_ilines
%type <Spec_sig.Parsed_spec.ind list> main_indlist
%type <Spec_sig.Parsed_spec.ind>     main_ind
%type <Spec_sig.Parsed_spec.ind>     inductive
%type <Spec_sig.Parsed_spec.irule list> irules
%type <Spec_sig.Parsed_spec.irule>   irule
%type <int * ntyp list>             nvars
%type <ntyp list>                   opt_ntypes
%type <ntyp>                        ntype
%type <Spec_sig.Parsed_spec.hform>   fheap
%type <Spec_sig.Parsed_spec.hform>   theap
%type <Spec_sig.Parsed_spec.cell>    cell
%type <indcall>                     indcall
%type <formal_main_arg>             indpoint
%type <formal_int_args>             intargs
%type <Spec_sig.Parsed_spec.pform>                       fpure
%type <Spec_sig.Parsed_spec.pformatom>                   tpure
%type <Spec_sig.Parsed_spec.pformatom>           tpurealloc
%type <sform>                       tpureset
%type <aform>                       tpurearith
%type <aexpr>                       tarith
%type <aexpr>                       tarith0
%type <aexpr>                       tarith1
%type <sexpr>                       sterm
%type <formal_int_arg>              iatom
%type <Spec_sig.Parsed_spec.offset>  field
%type <string list>                   string_list
%type <Array_ind_sig.Ai_parsed_spec.array_ind list>  array_indlist
%type <Array_ind_sig.Ai_parsed_spec.array_ind>       array_ind
%type <Array_ind_sig.Ai_parsed_spec.array_ind>       array_inductive

%type <Spec_sig.p_iline list>                      main_specs
%type <Spec_sig.Parsed_spec.ind>                   ms_ind
%type <string list>                               ms_prec
%type <string * string>                           ms_bind
%type <Spec_sig.Parsed_spec.s_henv>                env_formula
%type <Spec_sig.Parsed_spec.s_formula>             sl_formula
%type <Spec_sig.Parsed_spec.s_henv * Spec_sig.Parsed_spec.s_hform> sl_hformula
%type <Spec_sig.Parsed_spec.s_pterm list>          sl_pformula
%type <Spec_sig.Parsed_spec.s_pterm>               sl_pterm
%type <Spec_sig.Parsed_spec.s_pexpr>               sl_pexpr
%type <Spec_sig.Parsed_spec.s_range>               sl_range
%type <Spec_sig.s_sval list>                       sl_args
%type <Spec_sig.s_sval>                            sl_arg
%type <Spec_sig.s_sval>                            sl_val
%type <(string option * (string * string) option) * Spec_sig.Parsed_spec.s_hterm list> sl_hterm

%start main_ilines main_indlist main_ind array_indlist array_ind main_specs
%%

main_ilines:
| T_eof
    { [ ] }
| inductive main_ilines
    { Piind $1 :: $2 }
| T_prec T_colon string_list T_dot main_ilines
    { Piprec $3 :: $5 }
| V_string T_colon V_string T_dot main_ilines
    { Pibind ($1, $3) :: $5 }

main_indlist:
| T_eof
    { [ ] }
| inductive main_indlist
    { $1 :: $2 }

array_indlist:
| T_eof
    { [ ] }
| array_inductive array_indlist
    { $1 :: $2 }

array_ind:
| array_inductive T_eof
    { $1 }

main_ind:
| inductive T_eof
    { $1 }

inductive:
| name = V_string T_lt ind_params T_gt c_type_ref_opt = c_type_ref? T_defeq rules = irules T_dot
    { let ppars, ipars, spars, qpars = $3 in
      { name;
        ppars;
        ipars;
        spars;
        qpars;
        c_type_ref_opt;
        rules; }
    }

ind_params:
| V_int T_comma V_int
    { $1, $3, 0, 0 }
| V_int T_comma V_int T_comma V_int
    { $1, $3, $5, 0 }
| V_int T_comma V_int T_comma V_int T_comma V_int
    { $1, $3, $5, $7 }

array_inductive:
| ai_name = V_string T_lt ai_ppars = V_int T_comma ai_ipars = V_int T_gt
  c_type_ref_opt = c_type_ref? ai_submem = submem ai_mpars = woffset T_defeq
  ai_rules = airules T_dot
    { { c_type_ref_opt;
        array_ind = {
            ai_submem;
            ai_name;
            ai_ppars;
            ai_ipars;
            ai_mpars;
            ai_rules;
            ai_mt_rule = false;
            ai_emp_ipar = -1; } } : Ai_parsed_spec.array_ind
    }

irules:
|
    { [ ] }
| irule irules
    { $1 :: $2 }

irule:
| T_pipe nvars = nvars T_minus heap = fheap T_minus pure = fpure
    { let num, typ = nvars in { num; typ; heap; pure } }

airules:
| { [ ] }
| l = nonempty_airules { l }

%inline nonempty_airules:
| T_pipe nvars = nvars T_minus fheap = fheap_or_true T_minus tl = airules_tl
    { let n, t, h =
        match fheap with
        | None -> 0, SvMap.empty, []
        | Some heap ->
            let n, t = nvars in
            n, Sv_utils.svmap_of_list t, heap in
      tl (fun pure ->
        { AP.air_num   = n;
          AP.air_typ   = t;
          AP.air_heap  = h;
          AP.air_pure  = pure;
          AP.air_kind  = Aik_unk;
          AP.air_unone = false; }) }

(* There is an ambiguity for airules tail when looking to `|', which can denote
   both another airule or a pure maya cardinality predicate.
   The ambiguity is solved by looking for the next token, whether an matom or an
   nvar: we use %inline rules to discriminate between nonempty_aifpure and
   nonempty_airules.
  *)
airules_tl:
| { (fun k -> [k []]) }
| hd = nonempty_aifpure tl = airules
    { (fun k -> k hd :: tl) }
| tl = nonempty_airules
    { (fun k -> k [] :: tl) }

fheap_or_true:
| h = fheap { Some h }
| T_true { None }

nvars:
| T_lbrack V_int opt_ntypes T_rbrack
    { $2, $3 }

opt_ntypes:
|   { [ ] }
| ntype opt_ntypes
    { $1 :: $2 }

ntype:
| T_addr   { Ntaddr }
| T_int    { Ntint }
| T_raw    { Ntraw }
| T_set    { Ntset }
| T_seq    { Ntseq }

fheap:
| theap
    { $1 }
| theap T_star fheap
    { $1 @ $3 }

theap:
| T_emp
    { [ ] }
| cell
    { [ FSpec.Hacell $1 ] }
| indcall
    { [ FSpec.Haind $1 ] }
| indcall T_segimp indcall
    { [ FSpec.Haseg ($1, $3) ] }

cell:
| T_this T_arrow field T_contains aatom
    { { ic_off  = $3;
        ic_size =  4;
        ic_dest = $5;
        ic_doff = FPSpec.Numeric Offs.zero } }
| T_this T_arrow field V_size_contains aatom
    { { ic_off  = $3;
        ic_size = $4;
        ic_dest = $5;
        ic_doff = FPSpec.Numeric Offs.zero } }
| T_this T_arrow field T_contains aatom T_arrow field
    { { ic_off  = $3;
        ic_size =  4;
        ic_dest = $5;
        ic_doff = $7 } }
| T_this T_arrow field V_size_contains aatom T_arrow field
    { { ic_off  = $3;
        ic_size = $4;
        ic_dest = $5;
        ic_doff = $7 } }

indcall:
| indpoint T_dot V_string T_lpar indargs T_rpar
    { let p, i, s, q = $5 in { ii_maina = $1;
                               ii_ind   = $3;
                               ii_argp  = p ;
                               ii_argi  = i ;
                               ii_args  = s ;
                               ii_argq  = q ; } }

indpoint:
| T_this
    { Fa_this }
| V_new_var
    { (Fa_var_new $1 : formal_main_arg) }

indargs:
|
    { [ ], [ ], [ ], [ ] }
| ptrargs T_pipe intargs T_pipe setargs
    { $1, $3, $5, [ ] }
| ptrargs T_pipe intargs T_pipe setargs T_pipe seqargs
    { $1, $3, $5, $7 }

ptrargs:
|
    { [ ] }
| patom
    { [ $1 ] }
| patom T_comma ptrargs
    { $1 :: $3 }

intargs:
|
    { [ ] }
| iatom
    { [ $1 ] }
| iatom T_comma intargs
    { $1 :: $3 }

setargs:
|
    { [ ] }
| satom
    { [ $1 ] }
| satom T_comma setargs
    { $1 :: $3 }

seqargs:
|
    { [ ] }
| qatom
    { [ $1 ] }
| qatom T_comma seqargs
    { $1 :: $3 }

submem:
| T_lsub T_subi T_colon V_int T_rsub
    { Some { acc_type = Access_by_offset;
             groups = $4;} }
| T_lsub T_subp T_colon V_int T_rsub
    { Some { acc_type = Access_by_pointer;
             groups = $4;} }

woffset:
|
  { [ ] }
| T_lpar offset T_rpar
    { $2 }

offset:
| V_int
    { $1::[] }
| T_idx
    { (-1)::[] }
| T_idx T_semicolon offset
    { (-1)::$3}
| V_int T_semicolon offset
    { $1::$3}

nonempty_aifpure:
| hd = aitpure tl = aifpure_tail
    { hd :: tl }

aifpure_tail:
| { [] }
| T_and tl = separated_nonempty_list(T_and, aitpure) { tl }

fpure:
|
    { [ ] }
| tpure
    { [ $1 ] }
| tpure T_and fpure
    { $1 :: $3 }

%inline aitpure:
| tpure
   { (Ai_Pf $1 : Array_ind_sig.Ai_parsed_spec.ai_pformatom) }
| tpuremaya
   { AP.Ai_Pf_maya $1 }

tpure:
| tpurealloc
    { $1 }
| tpurearith
    { FSpec.Pf_arith $1 }
| tpureset
    { FSpec.Pf_set $1 }
| tpureseq
    { FSpec.Pf_seq $1 }
| tpurepath
    { FSpec.Pf_path $1 }

%inline tpuremaya:
| aatom T_setmem matom
    {Ai_Mf_mem ($1,$3)}
| matom T_setequal T_lbrace T_rbrace
    {Ai_Mf_empty $1}
| matom T_setequal matom
    {Ai_Mf_equal_s ($1,$3)}
| matom T_setincluded matom
    {Ai_Mf_included ($1,$3)}
| matom T_equal tarith
    {Ai_Mf_cons (Cbeq,$1,$3)}
| matom T_lt tarith
    {Ai_Mf_cons (Cblt,$1,$3)}
| matom T_gt tarith
    {Ai_Mf_cons (Cbgt,$1,$3)}
| matom T_le tarith
    {Ai_Mf_cons (Cble,$1,$3)}
| matom T_ge tarith
    {Ai_Mf_cons (Cbge,$1,$3)}
| T_pipe matom T_pipe T_equal V_int
    {Ai_Mf_cardinality ($2,$5)}

tpurealloc:
| T_alloc T_lpar T_this T_comma V_int T_rpar
    { FSpec.Pf_alloc $5 }

(* The case V_new_var T_setequal V_new_var is excluded because it is
   ambiguous (is it an equality between sets or seqs?). *)
%inline tpureset:
| aatom T_setmem satom
    { Sf_mem ($1, $3) }
| l = satom_var T_setequal r = sterm_not_var
    { Sf_equal (l, r) }
| l = satom_not_var T_setequal r = sterm
    { Sf_equal (l, r) }
| satom T_setequal T_lbrace T_rbrace
    { Sf_empty $1 }

%inline tpureseq:
| qatom = qatom_not_var T_setequal qterm = qterm_basis
    { Qf_equal (qatom, qterm) }
| qatom = qatom_var T_setequal qterm = qterm_basis_not_var
    { Qf_equal (qatom, qterm) }

tpurepath:
| T_lpar patom T_comma a_path T_comma patom T_rpar
   { ($2, $4, $6) }

a_path:
| T_epsilon
    { Pe_epsilon }
| T_lpar flds T_rpar T_star
    { Pe_seg $2 }
| flds
    { Pe_single $1 }

flds:
| fields = separated_nonempty_list(T_plus, field)
    { fields }

tpurearith:
| tarith T_equal tarith
    { Af_equal ($1, $3) }
| tarith T_notequal tarith
    { Af_noteq ($1, $3) }
| tarith T_gt tarith
    { Af_sup ($1, $3) }
| tarith T_ge tarith
    { Af_supeq ($1, $3) }
| tarith T_lt tarith
    { Af_sup ($3, $1) }
| tarith T_le tarith
    { Af_supeq ($3, $1) }

tarith:
| tarith0
    { $1 }

tarith0:
| tarith1
    { $1 }
| tarith0 T_plus tarith1
    { Ae_plus ($1, $3) }

tarith1:
| V_int
    { Ae_cst $1 }
| aatom
    { Ae_var $1 }
| T_size T_lpar qatom T_rpar
    { Ae_colvinfo (SIZE, $3)}
| T_min  T_lpar qatom T_rpar
    { Ae_colvinfo (MIN, $3)}
| T_max  T_lpar qatom T_rpar
    { Ae_colvinfo (MAX, $3)}

sterm:
| satom
    { Se_var $1 }
| t = sterm_not_atom { t }

sterm_not_var:
| satom_not_var
    { Se_var $1 }
| t = sterm_not_atom { t }

sterm_not_atom:
| satoms T_plus T_lbrace aatom T_rbrace
    { Se_uplus ($1, $4) }
| satoms T_union T_lbrace aatom T_rbrace
    { Se_union ($1, $4) }

qterm:
| qterm_basis
    { $1 }
| qterm_basis T_dot qterm
    { let e2 = $3 in
      match e2 with
      | Qe_concat l -> Qe_concat ($1::l)
      | _ -> Qe_concat [$1; e2] }

qterm_basis:
| qatom
    { Qe_var $1 }
| t = qterm_basis_not_atom { t }

qterm_basis_not_var:
| qatom_not_var
    { Qe_var $1 }
| t = qterm_basis_not_atom { t }

qterm_basis_not_atom:
| T_lbrack T_rbrack
    { Qe_empty }
| T_lbrack aatom T_rbrack
    { Qe_val $2 }
| T_sort T_lpar qterm T_rpar
    { Qe_sort $3 }
| T_lpar qterm = qterm T_rpar
    { qterm }

satoms:
| satom
    { [$1] }
| satom T_comma satoms
    { $1 :: $3 }


patom:
| T_this
    { Fa_this }
| V_new_var
    { Fa_var_new $1 }
| V_par_ptr
    { Fa_par_ptr $1 }

iatom:
| V_new_var
    { Fa_var_new $1 }
| V_par_int
    { Fa_par_int $1 }

%inline satom:
| a = satom_var { a }
| a = satom_not_var { a }

%inline satom_var:
| V_new_var
    { (Fa_var_new $1 : Ind_form_sig.formal_set_arg) }

satom_not_var:
| V_par_set
    { Fa_par_set $1 }

%inline qatom:
| a = qatom_var { a }
| a = qatom_not_var { a }

%inline qatom_var:
| V_new_var
    { (Fa_var_new $1 : Ind_form_sig.formal_seq_arg) }

qatom_not_var:
| V_par_seq
    { (Fa_par_seq $1 : Ind_form_sig.formal_seq_arg) }

aatom:
| T_this
    { Fa_this }
| V_new_var
    { Fa_var_new $1 }
| V_par_ptr
    { Fa_par_ptr $1 }
| V_par_int
    { Fa_par_int $1 }

matom:
| V_par_maya
    { Fa_par_maya $1 }
| V_par_nmaya
    { Fa_par_nmaya $1 }

custom_field(X):
| symbolic_offset = X
    { let c_type_ref_opt, items = symbolic_offset in
      FPSpec.Symbolic { FPSpec.pos            = fst $loc;
                        FPSpec.c_type_ref_opt = c_type_ref_opt;
                        FPSpec.items          =  items } }
| offset = V_int
    { FPSpec.Numeric (Offs.of_int offset) }

let field := custom_field(symbolic_offset)

let field_restrict := custom_field(symbolic_offset_restrict)

c_type_ref:
| T_lbrack c_type_ref = V_string T_rbrack { c_type_ref }

symbolic_offset:
| c_type_ref_opt = ioption(c_type_ref) head = symbolic_offset_item
  tail = symbolic_offset_item*
    { (c_type_ref_opt, head :: tail) }

symbolic_offset_restrict:
| T_lpar offset = symbolic_offset T_rpar { offset }
| c_type_ref_opt = ioption(c_type_ref) item = symbolic_offset_item
    { (c_type_ref_opt, [item]) }

%inline symbolic_offset_item:
| T_dot field_name = V_string
    { FPSpec.Field_name field_name }
| T_lbrack offset = V_int T_rbrack
    { FPSpec.Array_subscript offset }

string_list:
| { [ ] }
| V_string string_list { $1 :: $2 }

main_specs:
| T_eof
    { [ ] }
| ms_const main_specs
    { let a, b, c = $1 in Picst (a,b,c) :: $2 }
| ms_ind main_specs
    { Piind $1 :: $2 }
| ms_prec main_specs
    { Piprec $1 :: $2 }
| ms_bind main_specs
    { let (a,b) = $1 in Pibind (a,b) :: $2 }
| T_form V_string opt_params T_defeq sl_formula T_dot main_specs
    { if debug then Format.printf "parsed: form '%s'\n" $2;
      Piform ($2, $3, $5) :: $7 }
| T_env  V_string opt_symbs T_defeq env_formula T_dot main_specs
    { if debug then Format.printf "parsed: env '%s'\n" $2;
      Pienv ($2, $5) :: $7 }
| T_heap V_string opt_symbs T_defeq sl_hformula T_dot main_specs
    { assert (fst $5 = PS.Se_emp);
      if debug then Format.printf "parsed: heap '%s'\n" $2;
      Piheap ($2, $3, snd $5) :: $7 }
| T_pure V_string opt_symbs T_defeq sl_pformula T_dot main_specs
    { if debug then Format.printf "parsed: form '%s'\n" $2;
      Pipure ($2, $3, $5) :: $7 }
| ms_goal main_specs
    { let (a,b,c,d,e) = $1 in
      let _ = if debug then Format.printf "parsed: form '%s'\n" a in
      Pigoal (a, (b,c,d,e)) :: $2 }

ms_const:
| T_cst V_string opt_symbs T_defeq sl_pexpr T_dot
    { $2, $3, $5 }

ms_ind:
| inductive
    { if debug then Format.printf "parsed: inductive\n"; $1 }

ms_prec:
| T_prec T_colon string_list T_dot
    { if debug then Format.printf "parsed: type-prec\n"; $3 }

ms_bind:
| V_string T_colon V_string T_dot
    { if debug then Format.printf "parsed: type-bind\n"; $1, $3 }

ms_goal:
| T_goal V_string opt_symbs c_formula proc_call c_formula T_dot
    { ($2, $3, $4, $5, $6) }

opt_symbs:
| { [] }
| T_lbrack symbs T_rbrack
    { $2 }
symbs:
| { [] }
| symb { [ $1 ] }
| symb T_semicolon symbs { $1 :: $3 }
symb:
| T_int V_string { failwith "int symbol" }
| T_set V_string { CK_set, $2 }
| T_seq V_string { CK_seq, $2 }

opt_params:
| { [] }
| T_lbrack params T_rbrack
    { $2 }
params:
| { [] }
| param { [ $1 ] }
| param T_semicolon params { $1 :: $3 }
param:
| T_addr V_string { It_ptr, $2 }
| T_int V_string { It_num, $2 }
| T_set V_string { It_set, $2 }
| T_seq V_string { It_seq, $2 }

c_formula:
| T_lbrace sl_formula T_rbrace
    { $2 }

proc_call:
| result_binding = ioption(result_binding)
  function_name = V_string
  T_lpar arguments = string_comma_list T_rpar
    { { result_binding; function_name; arguments } }

result_binding:
| x = V_string T_equal { x }

env_formula:
| env_term
    { s_henv_init $1 }
| env_term T_star env_formula
    { s_henv_add $1 $3 }

env_term:
| T_env T_lpar V_string T_rpar
    { Some $3, None }
| V_string T_semicolon_semicolon sl_val
    { None, Some ($1, $3) }

sl_formula:
| sl_hformula
    { let a, b = $1 in Sf_extended (a, b, [ ]) }
| sl_pformula
    { Sf_extended (Parsed_spec.Se_emp, [ ], $1) }
| sl_hformula T_and sl_pformula
    { let a, b = $1 in PS.Sf_extended (a, b, $3) }

sl_hformula:
| sl_hterm
    { let a, c = $1 in s_henv_init a, c}
| sl_hterm T_star sl_hformula
    { let a, b = $1 and c, d = $3 in s_henv_add a c, b @ d }

sl_comp_range:
| V_int T_star sl_val T_plus field
    { FSpec.So_sval ($1, $3, $5) }
| T_minus V_int T_star sl_val T_plus field
    { FSpec.So_sval (-$2, $4, $6) }

sl_range:
| T_arrow field_restrict
    { FSpec.So_int $2, so_int 4 }
| T_arrow field_restrict T_lbrack field T_rbrack
    { FSpec.So_int $2, FSpec.So_int $4 }
| T_arrow field_restrict T_lbrack sl_comp_range T_rbrack
    { FSpec.So_int $2, $4 }
| T_arrow T_lpar sl_comp_range T_rpar
    { $3, FSpec.So_int (FPSpec.Numeric (Offs.of_int 4)) }
| T_arrow T_lpar sl_comp_range T_rpar T_lbrack field T_rbrack
    { $3, FSpec.So_int $6 }
| T_arrow T_lpar sl_comp_range T_rpar T_lbrack sl_comp_range T_rbrack
    { $3, $6 }

sl_hterm:
| T_env T_lpar V_string T_rpar
    { (Some $3, None), [ ] }
| T_form T_lpar V_string T_rpar m_optargs
    { (None, None), [ Sht_fsymbol ($3, $5) ] }
| T_heap T_lpar V_string T_rpar m_optargs
    { (None, None), [ PS.Sht_hsymbol ($3, $5) ] }
| T_emp
    { (None, None), [ ] }
| V_string T_semicolon_semicolon sl_val
    { (None, Some ($1, $3)), [ ] }
| sl_val T_contains sl_val
    { (None, None), [ Sht_cell ($1, (so_int 0, so_int 4), $3, (so_int 0, so_int 4)) ] }
| sl_val sl_range T_contains sl_val
    { (None, None), [ Sht_cell ($1, $2, $4, (so_int 0, so_int 4)) ] }
| sl_val sl_range T_contains sl_val sl_range
    { (None, None), [ Sht_cell ($1, $2, $4, $5) ] }
| sl_hind
    { let a,(b,c) = $1 in
      (None, None), [ Sht_ind (a,(b,c)) ] }
| sl_hind T_segimp sl_hind
    { let a,(b,c) = $1 and d,(e,f) = $3 in
      assert (a = d);
      (None, None), [ PS.Sht_seg (a, (b, c), (e, f)) ] }

sl_hind:
| sl_val T_dot V_string T_lpar sl_iargs T_rpar
    { ($3, ($1, $5)) }

sl_pformula:
| sl_pterm
    { [ $1 ] }
| sl_pterm T_and sl_pformula
    { $1 :: $3 }

sl_pterm:
| T_pure T_lpar V_string T_rpar m_optargs
    { Spt_symbol ($3, $5) }
| sl_pexpr T_equal sl_pexpr
    { Spt_cmp (S_ceq, $1, $3) }
| sl_pexpr T_notequal sl_pexpr
    { Spt_cmp (S_cnoteq, $1, $3) }
| sl_pexpr T_le sl_pexpr
    { Spt_cmp (S_cleq, $1, $3) }
| sl_pexpr T_lt sl_pexpr
    { Spt_cmp (S_clt, $1, $3) }
| sl_pexpr T_ge sl_pexpr
    { Spt_cmp (S_cleq, $3, $1) }
| sl_pexpr T_gt sl_pexpr
    { Spt_cmp (S_clt, $3, $1) }
| sl_pexpr T_setmem sl_pexpr
    { Spt_cmp (S_setmem, $1, $3) }
| sl_pexpr T_setincluded sl_pexpr
    { Spt_cmp (S_subseteq, $1, $3) }
| sl_pexpr T_setequal sl_pexpr
    { PS.Spt_cmp (S_colequal, $1, $3) }

m_optargs:
|                          { [ ] }
| args = m_brackargs       { args }
m_brackargs:
| T_lbrack args = m_args T_rbrack { args }
m_args:
|                          { [ ] }
| sl_pexpr                 { [ $1 ] }
| sl_pexpr T_comma m_args  { $1 :: $3 }

sl_pexpr:
(* We have: sl_pexpr -> sl_pexpradd -> sl_pexpr_sub -> sl_pexpr0 *)
| sl_pexpradd
    { $1 }
| sl_pexpru
    { $1 }
| T_lpar sl_pexprc T_rpar
    { $2 }
sl_pexpradd:
| sl_pexpradd T_plus sl_pexprsub
    { PS.Spe_bop (Badd, $1, $3) }
| sl_pexprsub
    { $1 }
sl_pexprsub:
| sl_pexprsub T_minus sl_pexpr0
    { PS.Spe_bop (Bsub, $1, $3) }
| sl_pexpr0
    { $1 }
sl_pexpru:
| sl_pexpr0 T_uplus sl_pexpr0
    { PS.Spe_bop (Buplus, $1, $3) }
| sl_pexpr0 T_uplus sl_pexpru
    { PS.Spe_bop (Buplus, $1, $3) }
sl_pexprc:
| sl_pexpr0 T_dot sl_pexpr0
    { PS.Spe_bop (Bconcat, $1, $3) }
| sl_pexpr0 T_dot sl_pexprc
    { PS.Spe_bop (Bconcat, $1, $3) }

sl_pexpr0:
| T_lbrack T_rbrack
    { Parsed_spec.Spe_empty }
| T_lbrace T_rbrace
    { PS.Spe_empty }
| T_lbrace expr = sl_pexpr0 T_rbrace args = m_brackargs?
    { match args with
      | None -> PS.Spe_set [ expr ]
      | Some args ->
          let s =
            match expr with
            | PS.Spe_sval s -> s
            | _ ->
                failwith (
                  Printf.sprintf "String excepted at line %d"
                    (fst $loc(expr)).Lexing.pos_lnum) in
          PS.Spe_symbol (s, args)
    }
| T_lbrack sl_pexpr0 T_rbrack
    { PS.Spe_seq [ $2 ] }
| T_sort T_lpar sl_pexpr T_rpar
    { PS.Spe_sort ( $3 ) }
| T_min T_lpar sl_val T_rpar
    { PS.Spe_info (MIN, $3)}
| T_max T_lpar sl_val T_rpar
    { PS.Spe_info (MAX, $3)}
| T_size T_lpar sl_val T_rpar
    { PS.Spe_info (SIZE, $3)}
| V_int
    { PS.Spe_int $1 }
| T_minus i = V_int
    { PS.Spe_int (-i) }
| sl_val
    { PS.Spe_sval $1 }
| sv = sl_val T_at_old
    { PS.Spe_old_sval sv }
| T_lpar sl_pexpr T_rpar
    { $2 }

sl_iargs:
| sl_args
    { {num = []; ptr = $1; set = []; seq = []} }
| sl_args T_pipe sl_args
    { {num = $3; ptr = $1; set = []; seq = []} }
| sl_args T_pipe sl_args T_pipe sl_args
    { {num = $3; ptr = $1; set = $5; seq = []} }
| sl_args T_pipe sl_args T_pipe sl_args T_pipe sl_args
    { {num = $3; ptr = $1; set = $5; seq = $7} }

sl_args:
|
    { [] }
| sl_arg
    { [ $1 ] }
| sl_arg T_comma sl_args
    { $1 :: $3 }

sl_arg:
| sl_val
    { $1 }

sl_val:
| V_string
    { $1 }

string_comma_list:
| { [ ] }
| V_string { [$1] }
| V_string T_comma string_comma_list { $1 :: $3 }
