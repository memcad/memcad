(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: inst_utils.mli
 **       utilities for the instantiation of set parameters
 ** Xavier Rival, Huisong Li, 2015/04/05 *)
open Data_structures
open Sv_def

open Col_sig
open Ind_sig
open Inst_sig
open Set_sig
open Seq_sig
open Nd_sig

(** SV instantiations basic functions *)
(* Empty *)
val sv_inst_empty: sv_inst
(* Pretty-printing *)
val sv_inst_fpri: string -> form -> sv_inst -> unit
(* Merging sv_inst in join and sv_inst return from is_le:
 * as is_le only returns instantiation on fresh sv, therefore,
 * this, merging will just join these two sv_inst *)
val sv_inst_merge: sv_inst -> sv_inst -> sv_inst
(* Instantiation of symbolic variables according to equality constraints,
 * that is, when a fresh sv (instantiable sv) is constrainted to be equal
 * to a numerical expression with only non fresh sv, then, we instantiate
 * this fresh sv to the expression *)
val sv_inst_equalities: n_cons list -> SvSet.t -> n_expr SvMap.t
  -> n_expr SvMap.t * n_cons list * n_cons list
(* SV instantiation: first instantiate some fresh sv according to equality
 * constraints, and then, if a non-instantiated sv \alpha is constrainted
 * to be, l_e <= \alpha <= r_e, and l_e and r_e only contain already
 * instantiated sv, then, if we can prove that l_e <= r_e, we can
 * instantiate \alpha to be a value from l_e to r_e *)
val sv_instantiation: sv_inst -> (n_cons -> bool) -> sv_inst
(* Deal with some constraints left from sv_instantiation function:
 * as an example, for constraint \alpha <= \beta, where both \alpha and
 * \beta are fresh sv, and in sv_inst, we have e1 <= \alpha, \beta <= e2,
 * in this case, if we can prove that e1 <= e2, then, we can instantiate
 * \alpha be a value from e1 to \beta, and instantiate \beta be a value
 * from \alpha to e2 *)
val do_sv_inst_left_ctrs: sv_inst -> (n_cons -> bool) -> sv_inst
(* Instantiation according to the weakening type of integer parameters *)
val typed_sv_instantiation: sv_inst -> int_par_rec SvMap.t
  -> (n_cons -> bool) -> sv_inst
(* Rename left constraints in sv_inst and prove them *)
val prove_sv_cons: sv_inst -> (n_cons -> bool) -> sv_inst

(** COLV is_le instantiation (unsorted) *)
val empty_inst: ('a, 'b) is_le_colv_inst
val is_le_setv_inst_fpri: string -> form -> is_le_setv_inst -> unit
val is_le_seqv_inst_fpri: string -> form -> is_le_seqv_inst -> unit
val gen_eq_colctr: colv_embedding ->
  (set_expr, set_cons) is_le_colv_inst * (seq_expr, seq_cons) is_le_colv_inst

(** COLV instantiations basic functions *)
(* Empty *)
val set_colv_inst_empty: set_colv_inst
val seq_colv_inst_empty: seq_colv_inst
(* Pretty-printing *)
val set_colv_inst_fpri: string -> form -> set_colv_inst -> unit
val seq_colv_inst_fpri: string -> form -> seq_colv_inst -> unit
(* Comparison of instantiations (temporary code) *)
val set_colv_inst_compare: set_colv_inst -> set_colv_inst -> bool * string
(* Add a new COLV *)
val colv_inst_add: sv -> ('a,'b) colv_inst -> ('a,'b) colv_inst
(* Remove a previously added COLV *)
val colv_inst_rem: sv -> ('a,'b) colv_inst -> ('a,'b) colv_inst
(* Polymorphic operations on colv_inst *)
val colv_inst_bind: sv -> 'a -> ('a,'b) colv_inst
  -> ('a,'b) colv_inst
val colv_inst_nobind: sv -> ('a,'b) colv_inst -> ('a,'b) colv_inst
val colv_inst_guess: sv -> sv -> ('a,'b) colv_inst -> ('a,'b) colv_inst
val colv_inst_over_bind: sv -> 'a -> ('a,'b) colv_inst -> ('a,'b) colv_inst
val colv_inst_make_guess: sv -> 'a -> ('a,'b) colv_inst -> ('a,'b) colv_inst
val colv_inst_strengthen: ('a,'b) colv_inst -> ('a,'b) colv_inst
  -> ('a,'b) colv_inst
(* Basic fold functions *)
val colv_inst_fold_add: (sv -> 'a -> 'a) -> ('b,'c) colv_inst -> 'a -> 'a
val colv_inst_fold_rem: (sv -> 'a -> 'a) -> ('b,'c) colv_inst -> 'a -> 'a

(** Management of internal dimensions in the instantiation *)
val colv_inst_add_pars: sv list -> ('a,'b) colv_inst -> ('a,'b) colv_inst
val colv_inst_rem_pars: sv list -> ('a,'b) colv_inst -> ('a,'b) colv_inst

(** Below are specific funtions.
 ** Conventions:
 **  . set_/seq_ kind of collection
 **  . jinst     join instantiation functions
 **  . gen_      function meant to be encapsulated in List_inst/Graph_inst
 **)

(** Generic functions for instantiation in join *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
val gen_jinst_set_addcstr_icall_eqs: sv list -> sv list
  -> set_colv_inst -> set_colv_inst
val gen_jinst_seq_addcstr_icall_eqs: sv list -> sv list
  -> seq_colv_inst -> seq_colv_inst
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
val gen_jinst_set_addcstr_segemp: sv list -> set_par_type list -> set_colv_inst
  -> set_colv_inst
(* Guess constraints from constant parameters on segments *)
val gen_jinst_set_guesscstr_segemp: sv list -> sv list -> set_par_type list
  -> set_colv_inst -> set_colv_inst

(** Integrating unresolved set constraints from is_le into inst *)
val gen_jinst_set_addctrs_post_isle:
    (sv -> set_par_type option)  (* Extraction of Set COLV types *)
  -> sv SvMap.t (* Map set args from isle template to join output *)
    -> sv SvMap.t                (* Is_le mapping for symbolic variables *)
      -> SvSet.t SvMap.t         (* Is_le mapping for set parameters *)
        -> set_cons list         (* Constraints to process *)
          -> set_colv_inst       (* Previous instantiation *)
            -> set_colv_inst     (* Finished instantiation *)

(** Post join instantiation refinement; specific code to seq/set *)
(* SETs:
 *  - saturation of equalities of the for Si = Sj
 *  - guesses about membership constraints *)
val jinst_set_post_join_refine: SvSet.t -> (set_cons -> bool) -> set_colv_inst
  -> set_colv_inst
(* SEQs:
 *  - saturation of equalities of the for Si = Sj *)
val jinst_seq_post_join_refine: SvSet.t -> (seq_cons -> bool) -> seq_colv_inst
  -> seq_colv_inst

(** Integrating results in colv_inst into the numerical domain *)
(* Function written as a generic set of iterators over adds, rems, constrs *)
(* obvious candidate to make a functor *)
val gen_colv_inst_fold_ctr:
    expr_fpr:(form -> 'a -> unit)
  -> cons_fpr:(form -> 'b -> unit)
    -> colv_add:(sv -> 'c -> 'c)
      -> colv_rem:(sv -> 'c -> 'c)
        -> guard:('b -> 'c -> 'c)
          -> eqcons:(sv -> 'a -> 'b)
            -> sat:('b -> 'c -> bool)
              -> ('a,'b) colv_inst -> 'c -> 'c
