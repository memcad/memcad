(** MemCAD analyzer
 **
 ** ind_lexer.mll
 ** inductive definitions lexer
 ** Xavier Rival, 2011/05/22 *)
{
open Ind_parser
open Graph_sig
open Graph_utils
(* Debugging *)
let debug = false
(* Integer tokens extraction *)
let extract_p i s =
  let n = String.length s in
  assert (n > i);
  int_of_string (String.sub s i (n-i))
(* Extraction of the size out of an arrow *)
let extract_size_arrow (s: string): int =
  let n = String.length s in
  assert (n > 4);
  int_of_string (String.sub s 2 (n-4))
(* String token hashes *)
let hash_string_tokens = Hashtbl.create 10
let _ =
  let lst_string_tokens =
    [ "alloc",    T_alloc ;
      "addr",     T_addr ;
      "emp",      T_emp ;
      "true",     T_true ;
      "int",      T_int ;
      "prec",     T_prec ;
      "raw",      T_raw ;
      "set",      T_set ;
      "seq",      T_seq ;
      "sort",     T_sort ;
      "idx",      T_idx ;
      "subi",     T_subi ;
      "subp",     T_subp ;
      "this",     T_this ;
      (* additional symbols for spec files *)
      "$cst",     T_cst ;
      "$env",     T_env ;
      "$form",    T_form ;
      "$formula", T_form ;
      "$goal",    T_goal ;
      "$heap",    T_heap ;
      "$pure",    T_pure ;
      "__colv_len", T_size;
      "__colv_min", T_min ;
      "__colv_max", T_max ] in
  List.iter (fun (str, tok) -> Hashtbl.add hash_string_tokens str tok)
    lst_string_tokens
let retrieve_string_tok (s: string) =
  if debug then Format.printf "searching: %s " s;
  try Hashtbl.find hash_string_tokens s
  with Not_found -> if debug then Format.printf "\tV_string\n"; V_string s
let retrieve_pstring_tok (s: string) =
  if debug then Format.printf "searching: %s " s;
  try Hashtbl.find hash_string_tokens s
  with Not_found -> failwith (Format.asprintf "String not found: %s" s)
}

rule token = parse
| ' ' | '\t'              { token lexbuf }
| '\n'                    { Lexing.new_line lexbuf; token lexbuf }
| '%'[^'\n']*('\n' | eof)   (* line of comments *)
                          { Lexing.new_line lexbuf; token lexbuf }
| '@''i'(['0'-'9'])+      { V_par_int (extract_p 2 (Lexing.lexeme lexbuf)) }
| '@''p'(['0'-'9'])+      { V_par_ptr (extract_p 2 (Lexing.lexeme lexbuf)) }
| '@''s'(['0'-'9'])+      { V_par_set (extract_p 2 (Lexing.lexeme lexbuf)) }
| '@''q'(['0'-'9'])+      { V_par_seq (extract_p 2 (Lexing.lexeme lexbuf)) }
| '@''m'(['0'-'9'])+      { V_par_maya (extract_p 2 (Lexing.lexeme lexbuf)) }
| '@''n'(['0'-'9'])+      { V_par_nmaya (extract_p 2 (Lexing.lexeme lexbuf)) }
| '$'(['0'-'9'])+         { let n = extract_p 1 (Lexing.lexeme lexbuf) in
                            V_new_var n }
| '$'(['a'-'z'])+         { retrieve_pstring_tok (Lexing.lexeme lexbuf) }
| "|-"['0'-'9']+'-''>'    { let str = Lexing.lexeme lexbuf in
                            V_size_contains (extract_size_arrow str) }
| ['0'-'9']+              { V_int (int_of_string (Lexing.lexeme lexbuf)) }
| (['a'-'z']|['A'-'Z']|'_')(['a'-'z']|['A'-'Z']|'_'|['0'-'9'])*
                          { let str = Lexing.lexeme lexbuf in
                            retrieve_string_tok str }
| "@old"                  { T_at_old }
| "::"                    { T_semicolon_semicolon }
| ":="                    { T_defeq }
| "->"                    { T_arrow }
| ':'                     { T_colon }
| "|->"                   { T_contains }
| "!="                    { T_notequal }
| "=="                    { T_setequal }
| "<="                    { T_le }
| ">="                    { T_ge }
| '#'                     { T_setmem }
| "#="                    { T_setincluded }
| "*="                    { T_segimp }
| "++"                    { T_uplus }
| '<'                     { T_lt }
| '>'                     { T_gt }
| ','                     { T_comma }
| ';'                     { T_semicolon }
| '.'                     { T_dot }
| '|'                     { T_pipe }
| '{'                     { T_lbrace }
| '}'                     { T_rbrace }
| '['                     { T_lbrack }
| ']'                     { T_rbrack }
| '('                     { T_lpar }
| ')'                     { T_rpar }
| '='                     { T_equal }
| '&'                     { T_and }
| '+'                     { T_plus }
| '^'                     { T_union }
| '-'                     { T_minus }
| '*'                     { T_star }
| "(-"                    { T_lsub }
| "-)"                    { T_rsub }
| '~'                     { T_epsilon }
| eof                     { T_eof }
