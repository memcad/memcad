(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_visu_sig.ml
 **       NODE and EGDE module signatures for graph output
 **       Francois Berenger, 2016/04/25 *)
open Data_structures
open Sv_def

open Graph_sig

module type NODE =
  sig
    type t
    val create: node -> namer -> IntSet.t SvMap.t -> t
    val is_leaf: node -> bool
    val successors: node -> SvSet.t
    val successors_offs: node -> (step * sv) list
    val is_inductive_src: t -> bool
    val fpr: ?prefix:string -> ?compact_pp:bool -> SvSet.t -> form -> t -> unit
  end

type args = sv list * sv list * sv list * sv list

type args_seg = sv list * sv list * sv list * (sv * sv) list
module type EDGE =
  sig
    type t =
      | Empty of sv
      | Inductive of string * sv * args
      | Segment of string * sv * args * Offs.OffSet.t * args_seg * sv * args
      | Points_to of (sv * Offs.t * sv * Offs.t) list
    val of_node: node -> t
    val list_offsets: t list -> IntSet.t SvMap.t
    val fpr: ?prefix: string -> form -> t * node SvMap.t -> unit
  end
