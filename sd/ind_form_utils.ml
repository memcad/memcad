(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_form_utils.ml
 **       utilities for basic data-types for the inductive definitions.
 ** Xavier Rival, 2023/06/08 *)
open Data_structures
open Lib

open Ast_utils
open Ind_form_sig


(** Conversion functions *)

let formal_arg_of_main (fa: formal_main_arg): formal_arg =
  match fa with
  | Fa_this -> Fa_this
  | Fa_var_new i -> Fa_var_new i
let formal_arg_of_ptr (fa: formal_ptr_arg): formal_arg =
  match fa with
  | Fa_this -> Fa_this
  | Fa_var_new i -> Fa_var_new i
  | Fa_par_ptr i -> Fa_par_ptr i
let formal_arg_of_int (fa: formal_int_arg): formal_arg =
  match fa with
  | Fa_var_new i -> Fa_var_new i
  | Fa_par_int i -> Fa_par_int i
let formal_arg_of_set (fa: formal_set_arg): formal_arg =
  match fa with
  | Fa_var_new i -> Fa_var_new i
  | Fa_par_set i -> Fa_par_set i
let formal_arg_of_seq (fa: formal_seq_arg): formal_arg =
  match fa with
  | Fa_var_new i -> Fa_var_new i
  | Fa_par_seq i -> Fa_par_seq i
let formal_arg_of_arith (fa: formal_arith_arg): formal_arg =
  match fa with
  | Fa_this -> Fa_this
  | Fa_var_new i -> Fa_var_new i
  | Fa_par_int i -> Fa_par_int i
  | Fa_par_ptr i -> Fa_par_ptr i


(** Pretty-printing *)

(* Parameter types *)
let indpar_type_fpr (fmt: form): indpar_type -> unit = function
  | It_ptr -> F.fprintf fmt "addr"
  | It_num -> F.fprintf fmt "int"
  | It_set -> F.fprintf fmt "set"
  | It_seq -> F.fprintf fmt "seq"

(* Parameters *)
let formal_arg_fpr (fmt: form): formal_arg -> unit = function
  | Fa_this      -> F.fprintf fmt "this"
  | Fa_var_new i -> F.fprintf fmt "$%d" i
  | Fa_par_ptr i -> F.fprintf fmt "@p%d" i
  | Fa_par_int i -> F.fprintf fmt "@i%d" i
  | Fa_par_set i -> F.fprintf fmt "@s%d" i
  | Fa_par_seq i -> F.fprintf fmt "@q%d" i
let formal_main_arg_fpr (fmt: form) (fa: formal_main_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_main fa)
let formal_ptr_arg_fpr (fmt: form) (fa: formal_ptr_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_ptr fa)
let formal_int_arg_fpr (fmt: form) (fa: formal_int_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_int fa)
let formal_set_arg_fpr (fmt: form) (fa: formal_set_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_set fa)
let formal_seq_arg_fpr (fmt: form) (fa: formal_seq_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_seq fa)
let formal_arith_arg_fpr (fmt: form) (fa: formal_arith_arg): unit =
  formal_arg_fpr fmt (formal_arg_of_arith fa)
let formal_ptr_args_fpr = gen_list_fpr "" formal_ptr_arg_fpr ", "
let formal_int_args_fpr = gen_list_fpr "" formal_int_arg_fpr ", "
let formal_set_args_fpr = gen_list_fpr "" formal_set_arg_fpr ", "
let formal_seq_args_fpr = gen_list_fpr "" formal_seq_arg_fpr ", "
let indcall_fpr (fmt: form) (i: indcall): unit =
  if i.ii_argp != [ ] || i.ii_argi != [ ] || i.ii_args != [ ] then
    F.fprintf fmt "%a.%s(%a|%a|%a|%a)"
      formal_main_arg_fpr i.ii_maina i.ii_ind
      formal_ptr_args_fpr i.ii_argp formal_int_args_fpr i.ii_argi
      formal_set_args_fpr i.ii_args formal_seq_args_fpr i.ii_argq
  else F.fprintf fmt "%a.%s()" formal_main_arg_fpr i.ii_maina i.ii_ind

(* Pure formulas *)
let sexpr_fpr (fmt: form): sexpr -> unit = function
  | Se_var s -> F.fprintf fmt "%a" formal_set_arg_fpr s
  | Se_uplus (s, x) ->
      F.fprintf fmt "%a + { %a }"
        (gen_list_fpr " " formal_set_arg_fpr "+") s formal_arith_arg_fpr x
  | Se_union (s, x) ->
      F.fprintf fmt "%a u { %a }"
        (gen_list_fpr " " formal_set_arg_fpr "u") s formal_arith_arg_fpr x
let rec qexpr_fpr (fmt: form): qexpr -> unit = function
  | Qe_var s -> F.fprintf fmt "%a" formal_seq_arg_fpr s
  | Qe_empty -> F.fprintf fmt "[ ]"
  | Qe_val v -> F.fprintf fmt "[%a]" formal_arith_arg_fpr v
  | Qe_concat l -> gen_list_fpr "" qexpr_fpr "·" fmt l
  | Qe_sort s -> F.fprintf fmt "sort(%a)" qexpr_fpr s
let rec aexpr_fpr (fmt: form): aexpr -> unit = function
  | Ae_cst c -> F.fprintf fmt "%d" c
  | Ae_colvinfo (ik, v) -> F.fprintf fmt "%a(%a)"
      info_kind_fpr ik formal_seq_arg_fpr v
  | Ae_var x -> F.fprintf fmt "%a" formal_arith_arg_fpr x
  | Ae_plus (e0, e1) -> F.fprintf fmt "%a + %a" aexpr_fpr e0 aexpr_fpr e1
let sform_fpr (fmt: form): sform -> unit = function
  | Sf_mem (x, s) ->
      F.fprintf fmt "%a # %a" formal_arith_arg_fpr x formal_set_arg_fpr s
  | Sf_equal (x, s) ->
      F.fprintf fmt "%a == %a" formal_set_arg_fpr x sexpr_fpr s
  | Sf_empty x ->
      F.fprintf fmt "%a == { }" formal_set_arg_fpr x
let qform_fpr (fmt: form): qform -> unit = function
  | Qf_equal (x, s) ->
      F.fprintf fmt "%a == %a" formal_seq_arg_fpr x qexpr_fpr s
let aform_fpr (fmt: form): aform -> unit = function
  | Af_equal (e0, e1) -> F.fprintf fmt "%a = %a" aexpr_fpr e0 aexpr_fpr e1
  | Af_noteq (e0, e1) -> F.fprintf fmt "%a != %a" aexpr_fpr e0 aexpr_fpr e1
  | Af_sup (e0, e1) -> F.fprintf fmt "%a > %a" aexpr_fpr e0 aexpr_fpr e1
  | Af_supeq (e0, e1) -> F.fprintf fmt "%a => %a" aexpr_fpr e0 aexpr_fpr e1

(* Operators *)
let s_bop_fpr (fmt: form): s_bop -> unit = function
  | Bconcat -> F.fprintf fmt ".."
  | Buplus  -> F.fprintf fmt "U+"
  | Badd    -> F.fprintf fmt "+"
  | Bsub    -> F.fprintf fmt "-"
