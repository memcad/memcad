(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_env.ml
 **       the environment abstract domain
 ** Xavier Rival, 2011/05/29 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Col_sig
open Dom_sig
open Graph_sig
open Graph_encode
open Ind_sig
open Nd_sig
open Svenv_sig
open Spec_sig
open Vd_sig

open Ast_utils
open Dom_utils
open Sv_utils


(* To consider:
 * there is quite a bit of code duplication in the following codes:
 * - merge make_setroots_rel into make_roots_rel
 * - merge table_setroots_compute into table_roots_compute *)


(** Error report *)
module Log =
  Logger.Make(struct let section = "d_env___" and level = Log_level.DEBUG end)


(** Functor lifting a shape domain into an environment domain *)
module Dom_env = functor (D: DOM_MEM_EXPRS) ->
  (struct
    (** Name *)
    (* For timing tags *)
    let module_name: string = "Env"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()

    (** Type of abstract values *)
    type t =
        { (* map each variable into a NODE ID and a typ *)
          t_vi:   (sv * typ) VarMap.t ;
          (* map each collection (set or seq) variable into an Id *)
          t_ci:   (sv * col_kind * colv_info option) ColVarMap.t;
          (* underlying abstract value *)
          t_u:    D.t }

    (** Basic components and functions *)
    (* Bottom element *)
    let bot (t: t): t = { t with t_u = D.bot }
    let is_bot (t: t): bool = D.is_bot t.t_u
    (* Top element, with provided set of roots *)
    let top (): t =
      { t_vi  = VarMap.empty ;
        t_ci  = ColVarMap.empty ;
        t_u   = D.top () }
    (* Pretty-printing *)
    let t_fpri (ind: string) (fmt: form) (t: t): unit =
      if is_bot t then
        F.fprintf fmt "%s_|_" ind
      else
        let ii = "  "^ind in
        F.fprintf fmt "%s{\n" ind;
        VarMap.iter
          (fun v (id, tp) ->
            let name = v.v_name in
            let msg =
              if tp = Tunk then " (WARN: environment contains [unknown-type])"
              else "" in
            F.fprintf fmt "%s&%s(%d): %a -> %a%s\n" ii name v.v_id
              typ_fpr tp Nd_utils.nsv_fpr id msg
          ) t.t_vi;
        ColVarMap.iter
          (fun colv (i, ck, _info) ->
            let k =
              match ck with
              | CK_set -> "S"
              | CK_seq -> "Q" in
            F.fprintf fmt "%sColv: %s => %s%a\n" ii colv.colv_name k sv_fpr i
          ) t.t_ci;
        F.fprintf fmt "%a%s}" (D.t_fpri ii) t.t_u ind
    (* External output *)
    let ext_output (o: output_format) (base: string) (x: t): unit =
      let roots =
        VarMap.fold
          (fun v (i, _) -> SvMap.add i (v.v_name, sv_unsafe_of_int v.v_id))
          x.t_vi SvMap.empty in
      let roots = SvMap.filter (fun _ (name, _) -> name <> "") roots in
      let o = match o with
        | Out_dot (_::_ , _) -> o
        | Out_dot ([], opts ) ->
            let all_vars = roots |> SvMap.values |> List.map fst in
            Out_dot (all_vars, "CC" :: opts) in
      D.ext_output o base (fun i -> SvMap.find i roots) x.t_u
    (* SVE fixing *)
    let sve_fix (t: t): t =
      let roots =
        VarMap.fold (fun _ (i, _) -> Aa_sets.add i) t.t_vi Aa_sets.empty in
      (* get rid of sym var env pending synchros, that should have been
       * processed, after checking none of the variable addresses corresponds
       * to one of the removed or modified keys
       * (added is ok, when roots are created) *)
      let t_u, svm = D.sve_sync_bot_up t.t_u in
      let f_check ctxt s =
        PSet.iter
          (fun i ->
            if PSet.mem i s then
              Log.fatal_exn "root %a have been %s!" sv_fpr i ctxt
          ) roots in
      f_check "removed" svm.svm_rem;
      f_check "modified" svm.svm_mod;
      (* result with no synchronization to take into account *)
      { t with t_u = t_u }

    (* Garbage collection *)
    let gc (t: t): t =
      let roots =
        VarMap.fold (fun _ (i, _) -> Aa_sets.add i) t.t_vi Aa_sets.empty in
      let roots =
        ColVarMap.fold
          (fun _ (_, _, info) acc ->
            match info with
            | None -> acc
            | Some {min; max; size} ->
                acc |> Aa_sets.add min |> Aa_sets.add max |> Aa_sets.add size)
          t.t_ci roots in
      sve_fix { t with t_u = D.gc roots t.t_u }

    (** Management of variables *)

    (* Get a variable *)
    let var_find (t: t) (v: var): sv =
      try fst (VarMap.find v t.t_vi)
      with Not_found -> Log.fatal_exn "var %s not found" v.v_name
    let colvar_find (t: t) (v: colvar): sv =
      try
        let sv , _, _ = ColVarMap.find v t.t_ci in sv
      with Not_found -> Log.fatal_exn "collection var %s not found" v.colv_name
    let var_info_find (t: t) (v: var_info): sv =
      match v with
      | Var v -> var_find t v
      | Info (cv, ik) ->
          match ColVarMap.find_opt cv t.t_ci, ik with
          | None, _ ->
              Log.fatal_exn "colvar %s not in dom_env" cv.colv_name
          | Some (sv, ck, None), _ ->
              Log.fatal_exn "colvar %s has no info in dom_env" cv.colv_name
          | Some (_, _, Some {min; _}), MIN -> min
          | Some (_, _, Some {max; _}), MAX-> max
          | Some (_, _, Some {size; _}), SIZE -> size

    (* Add a new variable *)
    let unary_op (op: env_op) (x: t): t =
      match op with
      | EO_add_var v ->
          let x = sve_fix x in
          let n_address, u = D.create_mem_var v.v_typ x.t_u in
          assert (not (VarMap.mem v x.t_vi));
          let x = sve_fix { x with
                            t_vi = VarMap.add v (n_address, v.v_typ) x.t_vi ;
                            t_u  = u } in
          Array_node.varm := x.t_vi; (* XR: seems like a gross hack *)
          x
      | EO_del_var v ->
          let sv = var_find x v in
          gc { x with
               t_vi = VarMap.remove v x.t_vi ;
               t_u  = D.delete_mem_var sv x.t_u }
      | EO_add_colvar s ->
          let sid, u, info = D.colv_add s.colv_name s.colv_kind x.t_u in
          let x = { x with t_u = u } in
          assert (not (ColVarMap.mem s x.t_ci));
          { x with t_ci = ColVarMap.add s (sid, s.colv_kind, info) x.t_ci }
      | EO_rem_colvar v ->
          let sv, x =
            match v.colv_kind with
            | CK_set ->
                colvar_find x v, { x with t_ci = ColVarMap.remove v x.t_ci }
            | CK_seq ->
                colvar_find x v, { x with t_ci = ColVarMap.remove v x.t_ci } in
          gc { x with t_u = D.colv_rem sv x.t_u }


    (** Comparison and Join operators *)
    (* Injection from right to left pre-computation *)
    let make_roots_rel (x0: t) (x1: t): sv bin_table =
      VarMap.fold
        (fun v (n0,tp0) acc ->
          let n1,tp1 =
            try VarMap.find v x1.t_vi
            with Not_found ->
              Log.fatal_exn "incompatible environments in roots_compute" in
          assert (tp0 = tp1); (* real type equality! *)
          Aa_maps.add n1 n0 acc
        ) x0.t_vi Aa_maps.empty
    (* Injection from right set var to left set var pre_compution *)
    let make_colroots_rel (x0: t) (x1: t): colv_emb * sv bin_table =
      ColVarMap.fold
        (fun v (n0, ck0, info0) (acc_colv, acc_n) ->
          let n1, ck1, info1 =
            try ColVarMap.find v x1.t_ci
            with
            | Not_found ->
                Log.fatal_exn
                  "incompatible environments in set_roots_compute" in
          let _ = assert (ck0 = ck1) in
          let acc_colv = Aa_maps.add n1 (n0, ck0) acc_colv in
          let acc_n =
            match info0, info1 with
            | None, None -> acc_n
            | Some info0, Some info1 ->
                acc_n
                |> Aa_maps.add info1.min info0.min
                |> Aa_maps.add info1.max info0.max
                |> Aa_maps.add info1.size info0.size
            | Some _, None | None, Some _ ->
                Log.fatal_exn "inconsitent information" in
          acc_colv, acc_n
        ) x0.t_ci (Aa_maps.empty, Aa_maps.empty)

    (* encode of graph *)
    let encode (disj: sv) (l: var list) (x: t)
        : renamed_path list * IntSvPrSetSet.t * sv =
      let var_map =
        if l = [] then
          VarMap.fold
            (fun var (nid, _) acc ->
              if !Flags.flag_dbg_dom_env then
                Log.info "encode,var: %s" var.v_name;
              SvMap.add nid (var.v_name, sv_unsafe_of_int var.v_id) acc
            ) x.t_vi SvMap.empty
        else
          List.fold_left
            (fun acc var ->
              let nid = var_find x var in
              SvMap.add nid (var.v_name, sv_unsafe_of_int var.v_id) acc
            ) SvMap.empty l in
      let namer = fun nid -> SvMap.find nid var_map in
      D.encode disj namer x.t_u

    (* Checks if the left argument is included in the right one *)
    let is_le (xl: t) (xr: t): bool =
      if !Flags.flag_dbg_is_le_gen then
        Log.force "[Env,al] start is_le\n%a\n%a"
          (t_fpri "  ") xl (t_fpri "  ") xr;
      (* Gather roots *)
      let roots_rel    = make_roots_rel xl xr in
      let colroots_rel, roots_rel' = make_colroots_rel xl xr in
      let roots_rel = Aa_maps.fold Aa_maps.add roots_rel' roots_rel in
      (* Underlying domain comparison *)
      let b = D.is_le roots_rel colroots_rel xl.t_u xr.t_u in
      if !Flags.flag_dbg_is_le_gen then
        Log.force "[Env,al] return is_le: %b" b;
      b
    (* Root pre-computation *)
    let table_roots_compute (x0: t) (x1: t)
        : (sv * typ) VarMap.t * sv tri_table =
      if x0.t_vi == x1.t_vi then
        (* return the same table, without a physical modification *)
        let r =
          VarMap.fold
            (fun _ (i,_) acc ->
              Aa_sets.add (i, i, i) acc
            ) x0.t_vi Aa_sets.empty in
        x0.t_vi, r
      else
        (* - roots will be consistent with the left argument
         * - table is computed accordingly *)
        let r =
          VarMap.fold
            (fun v (n0,tp0) accr ->
              let n1,tp1 =
                try VarMap.find v x1.t_vi
                with Not_found ->
                  Log.fatal_exn "incompatible environments in roots_compute" in
              assert (tp0 = tp1); (* real type equality! *)
              Aa_sets.add (n0, n1, n0) accr
            ) x0.t_vi Aa_sets.empty in
        x0.t_vi, r
    (* Set Root pre-computation *)
    let table_colroots_compute (x0: t) (x1: t):
        (sv * col_kind * colv_info option) ColVarMap.t * (sv, col_kind) tri_map =
      if x0.t_ci == x1.t_ci then
        (* return the same table, without a physical modification *)
        let r =
          ColVarMap.fold (fun v (i, ck, _) acc -> Aa_maps.add (i, i, i) ck acc)
            x0.t_ci Aa_maps.empty in
        x0.t_ci, r
      else
        (* - roots will be consistent with the left argument
         * - table is computed accordingly *)
        let r =
          ColVarMap.fold
            (fun v (n0, _, _) accr ->
              try
                let n1, ck, _ = ColVarMap.find v x1.t_ci in
                Aa_maps.add (n0, n1, n0) ck accr
              with Not_found ->
                Log.fatal_exn
                  "incompatible environments in colroots_compute"
            ) x0.t_ci Aa_maps.empty in
        x0.t_ci, r

    (* Hint translation *)
    let compute_hint_bin (x0: t) (x1: t) (he: hint_be): sv hint_bs =
      let s =
        VarSet.fold
          (fun cv acc ->
            try
              let il, _ = VarMap.find cv x0.t_vi in
              let ir, _ = VarMap.find cv x1.t_vi in
              Aa_maps.add ir il acc
            with
            | Not_found ->
                Log.fatal_exn "hint-bin, variable %s not found" cv.v_name
          ) he.hbe_live Aa_maps.empty in
      { hbs_live = s }

    (* Lint translation *)
    let compute_lint_bin (x0: t) (x1: t) (dv: VarSet.t) (le: var lint_be)
      : (sv tlval) lint_bs =
      let dead =
        VarSet.fold (fun ele acc -> (Lvar ele, ele.v_typ) :: acc)
          dv le.lbe_dead in
      let s =
        List.fold_left
          (fun acc lv ->
            try
              let lv1_u = map_tlval (var_find x0) lv in
              let lv2_u = map_tlval (var_find x1) lv in
              Aa_maps.add lv1_u lv2_u acc
            with
            | Not_found ->
                Log.fatal_exn "lint-bin, lval %a not found" vtlval_fpr lv
          ) Aa_maps.empty  dead in
      { lbs_dead = s }

    (* translate the encode graph *)
    let tr_encode (x: t) (g: abs_graph): abs_graph =
      let g_i, n_i, disj_i = Graph_encode.reduce_to_seg g in
      let t_vi = VarMap.fold (fun var (nid, _)  acc ->
        SvMap.add (sv_unsafe_of_int var.v_id) nid acc) x.t_vi SvMap.empty in
      let rename (n: IntSvPrSet.t) (m: IntSvPrSet.t IntSvPrSetMap.t) =
        try IntSvPrSetMap.find n m, m
        with Not_found ->
          let r_n = IntSvPrSet.fold
              (fun (off, uid) acc ->
                try
                  let nid = SvMap.find uid t_vi in
                  IntSvPrSet.add (off, nid) acc
                with Not_found -> Log.fatal_exn "tr_encode, var not found"
              ) n IntSvPrSet.empty in
          r_n, IntSvPrSetMap.add n r_n m in
      let g_i, _ =
        List.fold_left
          (fun (acc, m) (sc, p, dt) ->
            let r_sc, m = rename sc m in
            let r_dt, m = rename dt m in
            (r_sc,p, r_dt)::acc, m
          )  ([], IntSvPrSetMap.empty)  g_i in
      let n_i =
        List.fold_left
          (fun acc (sc, _, dt ) ->
            IntSvPrSetSet.add sc (IntSvPrSetSet.add dt acc)
          ) IntSvPrSetSet.empty g_i in
      (g_i, n_i, disj_i)

    let output_join =
      let c = ref 0 in
      fun l r o ->
        let () = incr c in
        let filename =
          Printf.sprintf "%s-join-%i-0" (Flags.out_prefix ()) !c in
        List.iter
          ( fun (graph, label) ->
            ext_output (Out_dot ([], [])) (filename^label) graph          )
          [l, "-inl"; r, "-inr"; o, "-out"]

    (* Generic function for join and widening *)
    let gen_join (j: join_kind) (ho: hint_be option) (lo: (var lint_be) option)
        ((x0, je0): t * join_ele) ((x1, je1): t * join_ele): t =
      if !Flags.flag_dbg_join_gen then
        Log.force "[Env,al] start %a\n%a\n%a"
          join_kind_fpr j (t_fpri "  ") x0 (t_fpri "  ") x1;
      let all_var =
        VarMap.fold
          (fun var _ acc -> VarSet.add var acc) x0.t_vi VarSet.empty in
      let dead_var = match ho with
        | None -> VarSet.empty
        | Some ho -> VarSet.diff all_var ho.hbe_live in
      (* Hint for the underlying domain *)
      let hu = Option.map (compute_hint_bin x0 x1) ho in
      let lu = Option.map (compute_lint_bin x0 x1 dead_var) lo in
      (* translate encode graph *)
      let inl =
        x0.t_u,
        ext_graph (Option.map (tr_encode x0) je0.abs_gi)
          (Option.map (tr_encode x0) je0.abs_go) in
      let inr =
        x1.t_u,
        ext_graph (Option.map (tr_encode x1) je1.abs_gi)
           (Option.map (tr_encode x1) je1.abs_go) in
      (* Gather the roots association table *)
      let ntable, roots_rel = table_roots_compute x0 x1 in
      let ctable, colroots_rel = table_colroots_compute x0 x1 in
      (* Underlying domain (graph+num) join *)
      let under =
        if j = Jdweak then
          D.directed_weakening hu roots_rel colroots_rel x0.t_u x1.t_u
        else
          D.join j hu lu roots_rel colroots_rel inl inr in
      (* Output construction *)
      let res = { t_vi = ntable ;
                  t_ci = ctable ;
                  t_u  = under } in
      if !Flags.flag_dbg_join_gen then
        Log.force "[Env,al] return %a\n%a" join_kind_fpr j (t_fpri "    ") res;
      res
    (* Join and widening *)
    let join (ho: hint_be option) (lo: (var lint_be) option)
        (x0: t * join_ele) (x1: t * join_ele): t =
      gen_join Jjoin ho lo x0 x1
      |> fun o ->
        if !Flags.flag_enable_ext_export_all
          then output_join (fst x0) (fst x1) o;
        o
    let widen (ho: hint_be option) (lo: (var lint_be) option)
        (x0: t *join_ele ) (x1: t *join_ele): t =
      gen_join Jwiden ho lo x0 x1
      |> fun o ->
        if !Flags.flag_enable_ext_export_all
          then output_join (fst x0) (fst x1) o;
        o
    (* Directed weakening; over-approximates only the right element *)
    let directed_weakening (ho: hint_be option) (x0: t) (x1: t): t =
      gen_join Jdweak ho None
        (x0, ext_graph None None) (x1, ext_graph None None)

    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    let local_abstract (ho: hint_ue option) (t: t): t =
      if !Flags.flag_dbg_loc_abs then
        Log.force "local_abstract,call:\n%a" (t_fpri "  ") t;
      let t = gc t in
      (* computation of a hint for the underlying domain *)
      let hu =
        let f h =
          let s =
            VarSet.fold
              (fun cv acc ->
                try Aa_sets.add (fst (VarMap.find cv t.t_vi)) acc
                with Not_found -> acc
              ) h.hue_live Aa_sets.empty in
          { hus_live = s } in
        Option.map f ho in
      (* this is very temporary: numeric domain gets modified *)
      let n_u = D.local_abstract hu t.t_u in
      let tt = gc { t with t_u = n_u } in
      if !Flags.flag_dbg_loc_abs then
        Log.force "local_abstract,result:\n%a" (t_fpri "  ") tt;
      tt

    (** Transfer functions for the analysis *)
    (* Assignment operator *)
    let assign (lv: var tlval) (ex: var texpr) (t: t): t list =
      let f: var -> sv = var_find t in
      let lv_u = map_tlval f lv and ex_u = map_texpr f ex in
      let lt_u = D.assign lv_u ex_u t.t_u in
      List.map
        (fun u ->
          let t1 = { t with t_u = u } in
          if Flags.do_gc_on_assign then gc t1
          else t1
        ) lt_u
    (* Condition test *)
    let guard (b: bool) (ex: var_info texpr) (t: t): t list =
      let ex_u = map_texpr (var_info_find t) ex in
      let lt_u = D.guard b ex_u t.t_u in
      List.map (fun u -> sve_fix { t with t_u = u }) lt_u
    (* Checking that a constraint is satisfied; returns over-approx sat *)
    let sat (ex: var_info texpr) (t: t): bool =
      let ex_u = map_texpr (var_info_find t) ex in
      D.sat ex_u t.t_u
    (* Memory alloc/free *)
    let memory (op: var_mem_op) (t: t): t list =
      let to_map =
        match op with
        | MO_alloc (lv, ex) ->
            let lv_u = map_tlval (var_find t) lv in
            let ex_u = map_texpr (var_find t) ex in
            D.memory (MO_alloc (lv_u, ex_u)) t.t_u
        | MO_dealloc lv ->
            let lv_u = map_tlval (var_find t) lv in
            D.memory (MO_dealloc lv_u) t.t_u in
      List.map (fun u -> sve_fix { t with t_u = u }) to_map

    (** Set domain *)
    let fvar (t: t): var -> sv = var_find t
    let fsetvar (t: t) (v: colvar): sv =
      colvar_find t v
    let fseqvar (t: t) (v: colvar): sv =
      colvar_find t v
    (* Guard and sat functions for set & seq properties *)
    let prepare_setprop (t: t) (ls: var tlval setprop): sv tlval setprop =
      let Refl = Sv_def.eq in
      map_setprop (fvar t) (fsetvar t) ls
    let prepare_seqprop (t: t) (ls: var tlval seqprop): sv tlval seqprop =
      let Refl = Sv_def.eq in
      map_seqprop (fvar t) (fseqvar t) ls
    let prep_log_form (t: t) = function
      | SL_set ls ->
          let ls = prepare_setprop t ls in
          SL_set ls
      | SL_seq ls ->
          let ls = prepare_seqprop t ls in
          SL_seq ls
      | SL_ind (ic, lv) ->
          let lv_u = map_tlval (fvar t) lv in
          let ic_u : (sv Ast_sig.lval * Ast_sig.typ) gen_ind_call =
            let Refl = Sv_def.eq in
            map_gen_ind_call (map_tlval (fvar t)) ~g:(fsetvar t)
            ~h:(fseqvar t) ic in
          SL_ind (ic_u, lv_u)
      | SL_seg (ic, lv, ic_e, lv_e) ->
          let lv_u, lv_ue = map_tlval (fvar t) lv, map_tlval (fvar t) lv_e in
          let (ic_u, ic_ue) :
              (sv Ast_sig.lval * Ast_sig.typ) gen_ind_call *
              (sv Ast_sig.lval * Ast_sig.typ) gen_ind_call =
            let Refl = Sv_def.eq in
            map_gen_ind_call (map_tlval (fvar t)) ~g:(fsetvar t)
              ~h:(fseqvar t) ic,
            map_gen_ind_call (map_tlval (fvar t)) ~g:(fsetvar t)
              ~h:(fseqvar t) ic_e in
          SL_seg (ic_u, lv_u, ic_ue, lv_ue)
      | SL_array ->
          SL_array
    let assume (op: state_log_form) (t: t): t =
      { t with t_u = D.assume (prep_log_form t op) t.t_u }
    let check (op: state_log_form) (t: t): bool =
      D.check (prep_log_form t op) t.t_u

    (** Analysis control *)
    (* Reduction + node relocalization *)
    let reduce_localize (lv: var tlval) (x: t): t option =
      Log.todo_exn "reduce_localize"
    (* Eager reduction *)
    let reduce_eager (x: t): t list =
      Log.todo_exn "reduce_eager"

    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    let mark_prio (lv: var tlval) (x: t): t =
      let lv_u = map_tlval (var_find x) lv in
      { x with t_u = D.mark_prio lv_u x.t_u }

    (** Assuming and checking inductive edges *)
    (* Unfold *)
    let ind_unfold (u: unfold_dir) (lv: var tlval) (x: t): t list =
      let lv_u = map_tlval (var_find x) lv in
      let l_u = D.ind_unfold u  lv_u x.t_u in
      List.map (fun u -> { x with t_u = u }) l_u
    (* Assume construction *)
    (* Check construction, that an inductive be there *)

    (** Temporary, to short-cut disjunctions *)
    let assert_one (lx: t list): t =
      match lx with
      | [ x ] -> x
      | [ ] | _ :: _ :: _ -> Log.fatal_exn "assert_one"

    (** Construction from formulas and checking of formulas *)
    (* Remarks on the management of environment:
     *  - all variables should be allocated in from_s_formula
     *    i.e., no variable existing in the state but not in the s_formula
     *  - we cannot support multiple variables with the same name, e.g., in
     *    the case of neste scopes (TODO, consider how to fix this)
     *)
    let from_s_formula
        (vm: var StringMap.t)
        (params: colvar StringMap.t)
        (f: s_formula)
        : t =
      let u, (pre_env, pre_colv_env) = D.from_s_formula params f in
      let env =
        StringMap.fold
          (fun vname sv acc ->
            let v =
              try StringMap.find vname vm
              with Not_found -> Log.fatal_exn "var not found: %s" vname in
            VarMap.add v (sv, Tunk) (* TODO!!! *) acc
          ) pre_env VarMap.empty in
      let colv_env =
        StringMap.fold
          (fun vname (colv, ck, info) acc ->
            let colvar =
              try StringMap.find vname params
              with Not_found -> Log.fatal_exn "colvar not found: %s" vname in
            ColVarMap.add colvar (colv, ck, info) acc)
          pre_colv_env ColVarMap.empty in
      { t_vi = env ;
        t_ci = colv_env ;
        t_u  = u }
    let sat_s_formula (l: var list) (cl: colvar StringMap.t)
        (f: s_formula) (t: t): bool =
      if !Flags.flag_dbg_dom_env then
        Log.info "sat_s_formula:\n%a\n%a" Spec_utils.s_formula_fpr f
          (t_fpri "  ") t;
      let he, hf, pf = f in
      let find_sym =
        VarMap.fold
          (fun cvar (sv, _) acc ->
            try
              let svname = StringMap.find cvar.v_name he in
              StringMap.add svname sv acc
            with Not_found -> acc
          ) t.t_vi StringMap.empty in
      let colsym =
        StringMap.mapi
          (fun cvname cvar ->
            let sv, ck, _ = ColVarMap.find cvar t.t_ci in
            sv, ck, cvar
          ) cl in
      D.sat_s_formula find_sym colsym f t.t_u
  end: DOM_ENV)


(** Timer instance of the environment domain (act as a functor on top
 ** of the domain itself) *)
module Dom_env_timing = functor (De: DOM_ENV) ->
  (struct
    module T = Timer.Timer_Mod( struct let name = "Env" end )
    let module_name = "dom_env_timing"
    let config_fpr = T.app2 "config_fpr" De.config_fpr
    type t = De.t
    let bot = T.app1 "bot" De.bot
    let is_bot = T.app1 "is_bot" De.is_bot
    let top = T.app1 "top" De.top
    let t_fpri = T.app3 "t_fpri" De.t_fpri
    let ext_output = T.app3 "ext_output" De.ext_output
    let gc = T.app1 "gc" De.gc
    let encode = T.app3 "encode" De.encode
    let is_le = T.app2 "is_le" De.is_le
    let gen_join = T.app5 "gen_join" De.gen_join
    let join = T.app4 "join" De.join
    let widen = T.app4 "widen" De.widen
    let directed_weakening = T.app3 "directed_weakening" De.directed_weakening
    let local_abstract = T.app2 "local_abstract" De.local_abstract
    let assign = T.app3 "assign" De.assign
    let guard = T.app3 "guard" De.guard
    let sat = T.app2 "sat" De.sat
    let memory = T.app2 "memory" De.memory
    let unary_op = T.app2 "unary_op" De.unary_op
    let assume = T.app2 "assume" De.assume
    let check = T.app2 "check" De.check
    let reduce_localize = T.app2 "reduce_localize" De.reduce_localize
    let reduce_eager = T.app1 "reduce_eager" De.reduce_eager
    let mark_prio = T.app1 "mark_prio" De.mark_prio
    let ind_unfold = T.app3 "ind_unfold" De.ind_unfold
    let assert_one = T.app1 "assert_one" De.assert_one
    let from_s_formula = T.app3 "from_s_formula" De.from_s_formula
    let sat_s_formula = T.app4 "sat_s_formula" De.sat_s_formula
  end: DOM_ENV)
