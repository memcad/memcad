(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: inst_utils.ml
 **       utilities for the instantiation of set parameters
 ** Xavier Rival, Huisong Li, 2015/04/05 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_sig
open Inst_sig
open Nd_sig
open Set_sig
open Seq_sig

open Graph_utils
open Nd_utils
open Set_utils
open Seq_utils
open Sv_utils
open Vd_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "ins_uts_" and level = Log_level.DEBUG end)
let debug_module = false

(** SV instantiations basic functions *)
(* Empty *)
let sv_inst_empty =
  { sv_fresh = SvSet.empty;
    sv_ie    = SvSet.empty;
    sv_eqs   = SvMap.empty;
    sv_low   = SvMap.empty;
    sv_up    = SvMap.empty;
    sv_eqlow = SvMap.empty;
    sv_equp  = SvMap.empty;
    sv_cons = [ ]; }
(* Pretty-printing *)
let sv_inst_fpri (ind: string) (fmt: form) (inst: sv_inst): unit =
  F.fprintf fmt "%sInstantiable svs: { %a }\n" ind nsvset_fpr inst.sv_fresh;
  SvMap.iter
    (fun sv ex ->
      F.fprintf fmt "%s%a |== %a\n" ind nsv_fpr sv n_expr_fpr ex
    ) inst.sv_eqs;
  let f_fpr op fmt m =
    SvMap.iter
      (fun sv exs ->
        F.fprintf fmt "%s%a %s %a\n" ind nsv_fpr sv op
          (gen_list_fpr "" n_expr_fpr ";") exs
      ) m in
  F.fprintf fmt "%a%a%a%a" (f_fpr "|>") inst.sv_low (f_fpr "|<") inst.sv_up
    (f_fpr "|>=") inst.sv_eqlow (f_fpr "|<=") inst.sv_equp;
  F.fprintf fmt "%sLeft constraints: %d cstr(s)" ind (List.length inst.sv_cons);
  List.iter (fun c -> F.fprintf fmt "%s  %a\n" ind n_cons_fpr c) inst.sv_cons
(* Merging sv_inst in join and sv_inst return from is_le:
 * as is_le only returns instantiation on fresh sv, therefore,
 * this, merging will just join these two sv_inst *)
let sv_inst_merge (instl: sv_inst) (instr: sv_inst): sv_inst =
  assert((SvSet.inter instl.sv_fresh instr.sv_fresh) = SvSet.empty);
  let f_merge key el er =
    Log.fatal_exn "attempt at double instantiation" in
  { sv_fresh = SvSet.union instl.sv_fresh instr.sv_fresh;
    sv_ie    = SvSet.union instl.sv_ie instr.sv_ie;
    sv_cons  = instl.sv_cons @ instr.sv_cons;
    sv_eqs   = SvMap.union f_merge instl.sv_eqs instr.sv_eqs;
    sv_low   = SvMap.union f_merge instl.sv_low instr.sv_low;
    sv_up    = SvMap.union f_merge instl.sv_up instr.sv_up;
    sv_eqlow = SvMap.union f_merge instl.sv_eqlow instr.sv_eqlow;
    sv_equp  = SvMap.union f_merge instl.sv_equp instr.sv_equp; }
(* Instantiation of symbolic variables according to equality constraints,
 * that is, when a fresh sv (instantiable sv) is constrainted to be equal
 * to a numerical expression with only non fresh SV, then, we instantiate
 * this fresh sv to the expression *)
let sv_inst_equalities (cons: n_cons list) (fsvs: SvSet.t)
    (inst: n_expr SvMap.t): (n_expr SvMap.t) * n_cons list * n_cons list =
  (* compute fresh svs that has been instantiated and fresh svs that
   * still not instantiated *)
  let inst_vars =
    SvMap.fold (fun i _ acc -> SvSet.add i acc) inst SvSet.empty in
  let fsvs = SvSet.diff fsvs inst_vars in
  (* instantiation of a sv *)
  let rec do_inst_i (n: sv) (svs: SvSet.t) (inst: n_expr SvMap.t)
      (ctrs: n_cons list): n_expr SvMap.t =
    if SvMap.mem n inst then inst
    else
      let f e inst =
        Nd_utils.n_expr_fold
          (fun i acc -> if SvSet.mem i svs then false else acc) e true in
      let f_sub e inst =
        Nd_utils.n_expr_subst
          (fun i -> try SvMap.find i inst with Not_found -> Ne_var i) e in
      match ctrs with
      | [] -> inst
      | ctr :: tl ->
          begin
            match ctr with
            | Nc_rand
            | Nc_bool _ -> do_inst_i n svs inst tl
            | Nc_cons (Apron.Tcons1.EQ, e0, e1) ->
                if e0 = Ne_var n &&f e1 inst then
                  SvMap.add n (f_sub e1 inst) inst
                else if e1 = Ne_var n &&f e0 inst then
                  SvMap.add n (f_sub e0 inst) inst
                else
                  do_inst_i n svs inst tl
            | _ -> do_inst_i n svs inst tl
          end in
  (* instantiation of a set of svs, and return instantiation and non
   * instantiated svs *)
  let rec do_inst (svs: SvSet.t) (inst: n_expr SvMap.t) (ctrs: n_cons list)
      : n_expr SvMap.t * SvSet.t =
    let svs_res, inst_res =
      SvSet.fold
        (fun s (accs, acci) ->
          let acci = do_inst_i s (SvSet.diff svs accs) acci ctrs in
          if SvMap.mem s acci then
            (SvSet.add s accs), acci
          else accs, acci
        ) svs (SvSet.empty, inst) in
    if SvSet.is_empty svs_res then inst_res, svs
    else do_inst (SvSet.diff svs svs_res) inst_res ctrs in
  (* do instantiation of a set of svs *)
  let inst, noninst_vars = do_inst fsvs inst cons in
  (* seperate constraints on left side into two parts: one parts need to be
     proved, the other parts is for future instantiation *)
  let ctr_to_prove, ctr_to_inst =
    List.fold_left
      (fun (accp, acci) ctr ->
        let no_uninst_svs =
          Nd_utils.n_cons_fold
            (fun i acc -> if SvSet.mem i noninst_vars then false else acc)
            ctr true in
        if no_uninst_svs then
          let ctr =
            Nd_utils.n_cons_subst
              (fun i -> try SvMap.find i inst with Not_found -> Ne_var i)
              ctr in
          ctr :: accp, acci
        else accp, ctr :: acci
      ) ([], []) cons in
  inst, ctr_to_prove, ctr_to_inst
(* SV instantiation: first instantiate some fresh sv according to equality
 * constraints, and then, if a non-instantiated sv \alpha is constrainted
 * to be, l_e <= \alpha <= r_e, and l_e and r_e only contain already
 * instantiated sv, then, if we can prove that, l_e <= r_e, we can instantiate
 * \alpha to be a value from l_e to r_e *)
let sv_instantiation (sv_inst: sv_inst)
    (is_sat: n_cons -> bool): sv_inst =
  (* SV instantiation according to eq numerical constraints *)
  let nd_inst_eq, ctr_to_prove, ctr_to_inst =
    sv_inst_equalities sv_inst.sv_cons sv_inst.sv_fresh sv_inst.sv_eqs in
  let sv_inst =
    let ie =
      SvMap.fold (fun k _ acc -> SvSet.add k acc) nd_inst_eq sv_inst.sv_ie in
    { sv_inst with
      sv_ie   = ie;
      sv_eqs  = nd_inst_eq;
      sv_cons = ctr_to_inst; } in
  let non_inst_svs = SvSet.diff sv_inst.sv_fresh sv_inst.sv_ie in
  (* check whether a expression has non instantiated svs *)
  let no_fsv e =
    Nd_utils.n_expr_fold
      (fun i acc -> if SvSet.mem i non_inst_svs then false else acc)
      e true in
  (* update the instantiation of a sv according to a constraint *)
  let do_inst_cons (sv: sv) (ctr: n_cons) (inst: sv_inst): sv_inst * bool =
    match ctr with
    | Nc_rand | Nc_bool _ -> inst, false
    | Nc_cons (Apron.Tcons1.SUP, el, er) ->
        if el = Ne_var sv && no_fsv er
            &&
          List.for_all
            (fun x -> is_sat (Nc_cons (Apron.Tcons1.SUP, x, er)))
            (SvMap.find sv inst.sv_up @ SvMap.find sv inst.sv_equp)
        then
          let sv_low = SvMap.find sv inst.sv_low in
          {inst with sv_low = SvMap.add sv (er::sv_low) inst.sv_low;}, true
        else if er = Ne_var sv && no_fsv el
            &&
          List.for_all
            (fun x -> is_sat (Nc_cons (Apron.Tcons1.SUP, el, x)))
            (SvMap.find sv inst.sv_low @ SvMap.find sv inst.sv_eqlow)
        then
          let sv_up = SvMap.find sv inst.sv_up in
          { inst with sv_up = SvMap.add sv (el :: sv_up) inst.sv_up }, true
        else inst, false
    | Nc_cons (Apron.Tcons1.SUPEQ, el, er) ->
        if el = Ne_var sv && no_fsv er
            &&
          List.for_all
            (fun ele -> is_sat (Nc_cons (Apron.Tcons1.SUP, ele, er)))
            (SvMap.find sv inst.sv_up)
            &&
          List.for_all
            (fun ele -> is_sat (Nc_cons (Apron.Tcons1.SUPEQ, ele, er)))
            (SvMap.find sv inst.sv_equp)
        then
          let sv_eqlow = SvMap.find sv inst.sv_eqlow in
          { inst with
            sv_eqlow = SvMap.add sv (er :: sv_eqlow) inst.sv_eqlow }, true
        else
          if er = Ne_var sv && no_fsv el
              &&
            List.for_all
              (fun ele -> is_sat (Nc_cons (Apron.Tcons1.SUP, el, ele)))
              (SvMap.find sv inst.sv_low)
              &&
            List.for_all
              (fun ele -> is_sat (Nc_cons (Apron.Tcons1.SUPEQ, el, ele)))
              (SvMap.find sv inst.sv_eqlow)
          then
            let sv_equp = SvMap.find sv inst.sv_equp in
            { inst with
              sv_equp = SvMap.add sv (el :: sv_equp) inst.sv_equp }, true
          else inst, false
    | _ -> inst, false in
  (*  instantiate a sv according to a list of constraints *)
  let do_inst_sv (sv: sv) (inst: sv_inst): sv_inst  =
    let inst =
      if not (SvMap.mem sv inst.sv_eqs || SvMap.mem sv inst.sv_low
         || SvMap.mem sv inst.sv_up || SvMap.mem sv inst.sv_eqlow
         || SvMap.mem sv inst.sv_equp)
      then
        (* init instantiation for sv *)
        { inst with
          sv_low   = SvMap.add sv [] inst.sv_low;
          sv_up    = SvMap.add sv [] inst.sv_up;
          sv_eqlow = SvMap.add sv [] inst.sv_eqlow;
          sv_equp  = SvMap.add sv [] inst.sv_equp; }
      else inst in
    let inst, ctrs =
      List.fold_left
        (fun (acci, acct) ctr ->
          let acci, b = do_inst_cons sv ctr acci in
          if b then acci, acct
          else acci, ctr :: acct
        ) (inst, []) inst.sv_cons in
    { inst with sv_cons = ctrs } in
  (* instantiate each non instantiated sv *)
  let sv_inst =
    SvSet.fold (fun sv inst -> do_inst_sv sv inst) non_inst_svs sv_inst in
  sv_inst
(* Deal with some constraints left from sv_instantiation function:
 * as an example, for constraint \alpha <= \beta, where both \alpha and
 * \beta are fresh sv, and in sv_inst, we have e1 <= \alpha, \beta <= e2,
 * in this case, if we can prove that e1 <= e2, then, we can instantiate
 * \alpha be a value from e1 to \beta, and instantiate \beta be a value
 * from \alpha to e2. *)
let do_sv_inst_left_ctrs (sv_inst: sv_inst)
    (is_sat: n_cons -> bool): sv_inst =
  let f e inst =
    Nd_utils.n_expr_fold
      (fun i acc ->
        if SvSet.mem i inst.sv_fresh && not (SvMap.mem i sv_inst.sv_eqs) then
          false
        else acc)
      e true in
  let do_sup inst lexprs uexprs op =
    List.for_all
      (fun le ->
        List.for_all
          (fun ue ->
            f le inst && f ue inst && is_sat (Nc_cons (op, ue, le))
          ) uexprs
      ) lexprs in
  let do_all_sup sv_inst sl sr =
    assert (not (SvMap.mem sl sv_inst.sv_eqs || SvMap.mem sr sv_inst.sv_eqs));
    let sl_low = SvMap.find sl sv_inst.sv_low in
    let sl_eqlow = SvMap.find sl sv_inst.sv_eqlow in
    let sr_up = SvMap.find sr sv_inst.sv_up in
    let sr_equp = SvMap.find sr sv_inst.sv_equp in
    do_sup sv_inst sl_low sr_up Apron.Tcons1.SUP
      && do_sup sv_inst sl_eqlow sr_up Apron.Tcons1.SUP
      && do_sup sv_inst sl_low sr_equp Apron.Tcons1.SUP
      && do_sup sv_inst sl_eqlow sr_equp Apron.Tcons1.SUPEQ in
  let do_ctr sv_inst ctr =
    match ctr with
    | Nc_rand | Nc_bool _ -> { sv_inst with sv_cons = ctr :: sv_inst.sv_cons }
    | Nc_cons (Apron.Tcons1.SUPEQ, Ne_var sl, Ne_var sr) ->
        if do_all_sup sv_inst sl sr then
          { sv_inst with
            sv_equp = SvMap.add sr
              (Ne_var sl :: SvMap.find sr sv_inst.sv_equp) sv_inst.sv_equp;
            sv_eqlow = SvMap.add sl
              (Ne_var sr :: SvMap.find sl sv_inst.sv_eqlow) sv_inst.sv_eqlow }
        else
          { sv_inst with
            sv_cons = ctr :: sv_inst.sv_cons }
    | Nc_cons (Apron.Tcons1.EQ, Ne_var sl, Ne_var sr) ->
        if do_all_sup sv_inst sl sr && do_all_sup sv_inst sr sl then
          let eqs =
            SvMap.add sl (Ne_var sr)
              (SvMap.add sr (Ne_var sl) sv_inst.sv_eqs) in
          { sv_inst with sv_eqs = eqs }
      else { sv_inst with sv_cons = ctr :: sv_inst.sv_cons }
    | _ -> { sv_inst with sv_cons = ctr :: sv_inst.sv_cons } in
  List.fold_left do_ctr { sv_inst with sv_cons = [] } sv_inst.sv_cons
(* Instantiation according to the weakening type of integer parameters *)
let rec typed_sv_instantiation (sv_inst: sv_inst)
    (ipt: int_par_rec SvMap.t) (is_sat: n_cons -> bool): sv_inst =
  let loc_debug = false in
  let f_no_fresh e sv_inst =
    Nd_utils.n_expr_fold
      (fun i acc -> not (SvSet.mem i sv_inst.sv_fresh) && acc) e true in
  let sat_min_or_max (is_min: bool) (el: n_expr) (er: n_expr): bool =
    if is_min then
      is_sat (Nc_cons (Apron.Tcons1.SUPEQ,el,er))
    else
      is_sat (Nc_cons (Apron.Tcons1.SUPEQ,er,el)) in
  let rec find_min_or_max (is_min: bool) (e: n_expr) (es: n_expr list)
      (sv_inst: sv_inst): n_expr option =
    match es with
    | [] -> Some e
    | h :: tl ->
        if f_no_fresh h sv_inst then
          if sat_min_or_max is_min h e then
            find_min_or_max is_min e tl sv_inst
          else if sat_min_or_max is_min e h then
            find_min_or_max is_min h tl sv_inst
          else None
        else None in
  let check_min_or_max (is_min: bool) (e: n_expr) (es: n_expr list)
      (sv_inst: sv_inst): bool =
    List.for_all
      (fun ele ->
        let b =
          if f_no_fresh ele sv_inst then
            if is_min then
              is_sat (Nc_cons (Apron.Tcons1.SUP,ele,e))
            else
              is_sat (Nc_cons (Apron.Tcons1.SUP,e,ele))
          else false in
        if loc_debug then
          Log.info "check_min_or_max: %b" b;
        b
      ) es in
  let f_min_or_max (is_min: bool) (n: sv) (sv_inst: sv_inst): sv_inst =
    let e_exprs, o_exprs =
      if is_min then
        SvMap.find n sv_inst.sv_equp, SvMap.find n sv_inst.sv_up
      else
        SvMap.find n sv_inst.sv_eqlow, SvMap.find n sv_inst.sv_low in
    match e_exprs with
    | [] -> sv_inst
    | h :: tl ->
        if f_no_fresh h sv_inst then
          match find_min_or_max is_min h tl sv_inst with
          | None -> sv_inst
          | Some min_e ->
              let f_rename n ex map =
                SvMap.map
                  (List.map
                     (Nd_utils.n_expr_subst
                        (fun i -> if i = n then ex else Ne_var i))) map in
              if check_min_or_max is_min min_e o_exprs sv_inst then
                { sv_inst with
                  sv_ie    = SvSet.add n sv_inst.sv_ie;
                  sv_eqs   = SvMap.add n min_e sv_inst.sv_eqs;
                  sv_low   = f_rename n min_e (SvMap.remove n sv_inst.sv_low);
                  sv_up    = f_rename n min_e (SvMap.remove n sv_inst.sv_up);
                  sv_eqlow = f_rename n min_e (SvMap.remove n sv_inst.sv_eqlow);
                  sv_equp  = f_rename n min_e (SvMap.remove n sv_inst.sv_equp);
                }
              else sv_inst
        else sv_inst in
  let res_sv_inst =
    SvMap.fold
      (fun n ipr acc ->
        if SvSet.mem n acc.sv_ie then acc
        else
          if ipr.ipt_decr then      f_min_or_max false n acc
          else if ipr.ipt_incr then f_min_or_max true  n acc
          else acc
      ) ipt sv_inst in
  if SvSet.equal res_sv_inst.sv_ie sv_inst.sv_ie then res_sv_inst
  else typed_sv_instantiation res_sv_inst ipt is_sat
(* Rename left constraints in sv_inst and prove them *)
let prove_sv_cons (sv_inst: sv_inst) (is_sat: n_cons -> bool): sv_inst =
  let cons =
    List.map
      (fun ctr ->
        Nd_utils.n_cons_subst
          (fun i ->
            try SvMap.find i sv_inst.sv_eqs
            with Not_found -> Ne_var i
          ) ctr
      ) sv_inst.sv_cons in
  let cons = List.filter (fun ctr -> not (is_sat ctr)) cons in
  { sv_inst with sv_cons = cons }

(** COLV is_le instantiation (unsorted) *)
(* Empty element *)
let empty_inst = { cvi_def  = SvMap.empty;
                   cvi_cons = [];
                   cvi_new  = SvSet.empty; }
(* Printing *)
let is_le_colv_inst_fpri
    (cv_fpr: form -> sv -> unit)
    (a_fpr: form -> 'a -> unit) (b_fpr: form -> 'b -> unit)
    (ind: string) (fmt: form) (is: ('a,'b) is_le_colv_inst): unit =
  F.fprintf fmt "%sRight colv to left col_expr:\n" ind;
  SvMap.iter
    (fun colv expr ->
      F.fprintf fmt "%s  %a => %a\n" ind cv_fpr colv a_fpr expr
    ) is.cvi_def;
  F.fprintf fmt "%sConstraints on left col_expr:\n" ind;
  List.iter (F.fprintf fmt "%s  %a\n" ind b_fpr) is.cvi_cons;
  F.fprintf fmt "%sColvs added: " ind;
  SvSet.iter (F.fprintf fmt "%a; " cv_fpr) is.cvi_new;
  F.fprintf fmt "\n"
let is_le_setv_inst_fpri =
  is_le_colv_inst_fpri setv_fpr set_expr_fpr set_cons_fpr
let is_le_seqv_inst_fpri =
  is_le_colv_inst_fpri seqv_fpr seq_expr_fpr seq_cons_fpr
(* ??? *)
let gen_eq_colctr (cur_si: colv_embedding)
    : (set_expr, set_cons) is_le_colv_inst
    * (seq_expr, seq_cons) is_le_colv_inst =
  SvMap.fold
    (fun id (st, kind) (inst_set, inst_seq) ->
      let e =
        try SvSet.choose st
        with Not_found ->
          Log.fatal_exn "colv_embedding has empty set image" in
      let remainder = SvSet.remove e st in
      match kind with
      | CK_set ->
          let cvi_def = SvMap.add id (S_var e) inst_set.cvi_def in
          let cvi_cons =
            SvSet.fold (fun ele acc -> Set_utils.make_equality e ele :: acc)
              remainder inst_set.cvi_cons in
          { inst_set with cvi_def; cvi_cons }, inst_seq
      | CK_seq ->
          let cvi_def = SvMap.add id (Seq_var e) inst_seq.cvi_def in
          let cvi_cons =
            SvSet.fold (fun ele acc -> Seq_utils.make_equality e ele :: acc)
              remainder inst_seq.cvi_cons in
          inst_set, { inst_seq with cvi_def; cvi_cons }
    ) cur_si.n_img (empty_inst, empty_inst)

(** COLV instantiations basic functions *)
(* Empty *)
let colv_inst_empty (k: col_kind) =
  { colvi_kind  = k;
    colvi_add   = SvSet.empty;
    colvi_rem   = SvSet.empty;
    colvi_eqs   = SvMap.empty;
    colvi_guess = SvMap.empty;
    colvi_none  = SvSet.empty;
    colvi_props = [ ];
    colvi_prove = [ ];
    colvi_fresh = SvSet.empty; }
let set_colv_inst_empty = colv_inst_empty CK_set
let seq_colv_inst_empty = colv_inst_empty CK_seq
(* Pretty-printing *)
let colv_inst_fpri
    (afpr: form -> 'a -> unit) (bfpr: form -> 'b -> unit)
    (ind: string) (fmt: form) (inst: ('a,'b) colv_inst): unit =
  let colv_fpr = colv_fpr inst.colvi_kind in
  F.fprintf fmt "%sadd: %a\n%srem: %a\n" ind (colvset_fpr inst.colvi_kind)
    inst.colvi_add ind (colvset_fpr inst.colvi_kind) inst.colvi_rem;
  SvMap.iter
    (fun setv -> F.fprintf fmt "%seq: %a |=> %a\n" ind colv_fpr setv afpr)
    inst.colvi_eqs;
  SvMap.iter
    (fun setv ->
      F.fprintf fmt "%sguess: %a |:> N[%a]\n" ind colv_fpr setv sv_fpr)
    inst.colvi_guess;
  SvSet.iter
    (F.fprintf fmt "%snone: %a |=> ???\n" ind colv_fpr)
    inst.colvi_none;
  List.iter (F.fprintf fmt "%s+ %a\n" ind bfpr) inst.colvi_props;
  List.iter (F.fprintf fmt "%s? |- %a\n" ind bfpr) inst.colvi_prove;
  F.fprintf fmt "%sfresh colvars:{ %a }\n" ind (colvset_fpr inst.colvi_kind)
    inst.colvi_fresh
let set_colv_inst_fpri = colv_inst_fpri set_expr_fpr set_cons_fpr
let seq_colv_inst_fpri = colv_inst_fpri seq_expr_fpr seq_cons_fpr

(* Comparison of instantiations (temporary code) *)
let set_colv_inst_compare (c0: set_colv_inst) (c1: set_colv_inst)
    : bool * string =
  let loc_debug = false in
  let module M = struct exception Stop of string end in
  if loc_debug then
    Log.debug "compare:\n%a%a" (set_colv_inst_fpri "  ") c0
      (set_colv_inst_fpri "") c1;
  try
    let flist msg l0 l1 =
      if List.length l0 != List.length l1 then
        raise (M.Stop (F.asprintf "colvi_%s::length" msg));
      List.iter2
        (fun p0 p1 ->
          if p0 != p1 then raise (M.Stop (F.asprintf "colvi_%s::elements" msg))
        ) l0 l1 in
    let fmap msg f m0 m1 =
      if not (SvMap.cardinal m0 = SvMap.cardinal m1) then
        raise (M.Stop (F.asprintf "colvi_%s::size" msg));
      SvMap.iter
        (fun svn x0 ->
          try
            if not (f x0 (SvMap.find svn m1)) then
              raise (M.Stop (F.asprintf "colvi_%s::diffcontent" msg))
          with
          | Not_found -> raise (M.Stop (F.asprintf "colvi_%s::diffdomain" msg))
        ) m0 in
    if not (SvSet.equal c0.colvi_add c1.colvi_add) then
      raise (M.Stop "colvi_add");
    if not (SvSet.equal c0.colvi_rem c1.colvi_rem) then
      raise (M.Stop "colvi_rem");
    fmap "eqs" (=) c0.colvi_eqs c1.colvi_eqs;
    fmap "guess" (=) c0.colvi_guess c1.colvi_guess;
    if not (SvSet.equal c0.colvi_none c1.colvi_none) then
      raise (M.Stop "colvi_none");
    flist "props" c0.colvi_props c1.colvi_props;
    flist "prove" c0.colvi_prove c1.colvi_prove;
    if not (SvSet.equal c0.colvi_fresh c1.colvi_fresh) then
      raise (M.Stop "colvi_fresh");
    if loc_debug then
      Log.debug "compare => equal\n";
    true, ""
  with M.Stop s ->
    if loc_debug then
      Log.debug "compare => inequal, %s\n" s;
    false, s

(* Add a new COLV *)
let colv_inst_add (colv: sv) (inst: ('a,'b) colv_inst): ('a,'b) colv_inst =
  let loc_debug = false in
  if loc_debug then Log.info "colv_inst_add: %a" (colv_fpr CK_set) colv;
  assert (not (SvSet.mem colv inst.colvi_add));
  assert (not (SvSet.mem colv inst.colvi_rem));
  { inst with colvi_add = SvSet.add colv inst.colvi_add }

(* Remove a previously added COLV *)
let colv_inst_rem (colv: sv) (inst: ('a,'b) colv_inst): ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  assert (not (SvSet.mem colv inst.colvi_rem));
  { inst with colvi_rem = SvSet.add colv inst.colvi_rem }

(* Is the COLV colv already bound in inst ? *)
let colv_inst_bound (colv: sv) (inst: ('a,'b) colv_inst): bool =
  (SvMap.mem colv inst.colvi_eqs
 || SvSet.mem colv inst.colvi_none
 || SvMap.mem colv inst.colvi_guess)

(* Bind setv to eq in inst *)
let colv_inst_bind (colv: sv) (eq: 'a) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  let colv_fpr = colv_fpr inst.colvi_kind in
  if SvMap.mem colv inst.colvi_eqs then
    (* TODO: this code means some double binding is happening;
     *       => eventually fix this *)
    if SvMap.find colv inst.colvi_eqs = eq then
      begin
        Log.warn "colv_inst_bind: %a, redo same instantiation" colv_fpr colv;
        inst
      end
    else Log.fatal_exn "colv_inst_bind: %a already specified" colv_fpr colv
  else if colv_inst_bound colv inst then
    Log.fatal_exn "colv_inst_bind: %a already specified" colv_fpr colv
  else { inst with colvi_eqs = SvMap.add colv eq inst.colvi_eqs }

(* Mark colv as unknown, with no information *)
let colv_inst_nobind (colv: sv) (inst: ('a,'b) colv_inst): ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  let colv_fpr = colv_fpr inst.colvi_kind in
  if colv_inst_bound colv inst then
    Log.fatal_exn "colv_inst_nobind: %a already specified" colv_fpr colv;
  { inst with colvi_none = SvSet.add colv inst.colvi_none }

(* Mark colv as unknown, can be "guessed" later using an element *)
let colv_inst_guess (colv: sv) (sv: sv) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  let colv_fpr = colv_fpr inst.colvi_kind in
  if colv_inst_bound colv inst then
    Log.fatal_exn "colv_inst_guess: %a already specified" colv_fpr colv;
  { inst with colvi_guess = SvMap.add colv sv inst.colvi_guess }

(* Overwrite a previously done binding *)
let colv_inst_over_bind (colv: sv) (ex: 'a) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  if SvSet.mem colv inst.colvi_none then
    colv_inst_bind colv ex
      { inst with colvi_none = SvSet.remove colv inst.colvi_none }
  else Log.fatal_exn "colv_inst_make_guess: colv not guessable"

(* Constrain colv, by "guessing" it as ex *)
let colv_inst_make_guess (colv: sv) (ex: 'a) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  assert (SvSet.mem colv inst.colvi_add);
  if SvMap.mem colv inst.colvi_guess then
    colv_inst_bind colv ex
      { inst with colvi_guess = SvMap.remove colv inst.colvi_guess }
  else Log.fatal_exn "colv_inst_make_guess: colv not guessable"

(* Strengthening of an instantiation *)
let colv_inst_strengthen (iref: ('a,'b) colv_inst) (itgt: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  SvSet.fold
    (fun colv acc ->
      try colv_inst_over_bind colv (SvMap.find colv iref.colvi_eqs) acc
      with Not_found -> acc
    ) itgt.colvi_none itgt

(* Basic fold functions *)
let colv_inst_fold_add (f: sv -> 'a -> 'a) (i: ('b,'c) colv_inst) (x: 'a): 'a =
  SvSet.fold f i.colvi_add x
let colv_inst_fold_rem (f: sv -> 'a -> 'a) (i: ('b,'c) colv_inst) (x: 'a): 'a =
  SvSet.fold f i.colvi_rem x

(** Management of internal dimensions in the instantiation *)
let colv_inst_add_pars (l: sv list) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  List.fold_left (fun a i -> colv_inst_add i a) inst l
let colv_inst_rem_pars (l: sv list) (inst: ('a,'b) colv_inst)
    : ('a,'b) colv_inst =
  List.fold_left (fun a i -> colv_inst_rem i a) inst l

(** Generic functions for instantiation in join *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
let gen_jinst_gen_addcstr_icall_eqs
    (f: sv -> 'a)   (* convert a COLV into an expression *)
    (lc_i: sv list) (* call from the join input *)
    (lc_o: sv list) (* call from the join output being constructed *)
    (inst: ('a,'b) colv_inst): ('a,'b) colv_inst =
  assert (List.length lc_i = List.length lc_o);
  List.fold_left2
    (fun acc ii io ->
      colv_inst_bind io (f ii) (colv_inst_add io acc)
    ) inst lc_i lc_o
let gen_jinst_set_addcstr_icall_eqs =
  gen_jinst_gen_addcstr_icall_eqs (fun i -> S_var i)
let gen_jinst_seq_addcstr_icall_eqs =
  gen_jinst_gen_addcstr_icall_eqs (fun i -> Seq_var i)
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
let gen_jinst_set_addcstr_segemp (lcargs: sv list) (argst: set_par_type list)
    (inst: set_colv_inst): set_colv_inst =
  try
    List.fold_left2
      (fun inst l kind ->
        let inst = colv_inst_add l inst in
        (* If a parameter is "monotone", then we can fix it to be anything,
         * it will always work for the empty segment *)
        if kind.st_add || kind.st_head then
          colv_inst_bind l S_empty inst
        else if kind.st_mono then
          colv_inst_nobind l inst
        else Log.todo_exn "unhandled kind"
      ) inst lcargs argst
  with Not_found -> Log.fatal_exn "l_call_empty: set parameters not match"
(* Guess constraints from constant parameters on segments *)
let gen_jinst_set_guesscstr_segemp (lcargs: sv list) (lc_extr_args: sv list)
    (argst: set_par_type list) (inst: set_colv_inst): set_colv_inst =
  let _, _, inst =
    List.fold_left
      (fun (lc_args, lc_ex_args, inst_set) par_kind ->
        let inst_set =
          if par_kind.st_add || par_kind.st_head then inst_set
          else if par_kind.st_const then
            let l, r = List.hd lc_args, List.hd lc_ex_args in
            let inst_set =
              if SvSet.mem l inst_set.colvi_none &&
                SvMap.mem r inst_set.colvi_eqs then
                let c =
                  SvMap.find_err "eq_const:over" r inst_set.colvi_eqs in
                Log.warn "zorb,eq_const: over_bind: %a, %a" setv_fpr l
                  setv_fpr r;
                colv_inst_over_bind l c inst_set
              else inst_set in
            inst_set
          else Log.todo_exn "unhandled kind-1" in
        List.tl lc_args, List.tl lc_ex_args, inst_set
      ) (lcargs, lc_extr_args, inst) argst in
  inst

(** Integrating unresolved set constraints from is_le into inst *)
(* Simplifying a series of set constraints *)
let rewrite_set_ctrs (tokeep: sv -> bool) (sc: set_cons list): set_cons list =
  let loc_debug = false in
  let sc_keep, sc_rewrite =
    List.fold_left
      (fun (acck, accr) c ->
        match c with
        | S_eq (S_var setv, e) ->
            if tokeep setv then c :: acck, accr
            else acck, SvMap.add setv e accr
        | _ -> c :: acck, accr
      ) ([ ], SvMap.empty) sc in
  let rec aux (m: int) l =
    let ismod = ref false in
    let fmod setv =
      try
        let rew = SvMap.find setv sc_rewrite in
        ismod := true;
        rew
      with Not_found -> S_var setv in
    let l = List.map (transform_set_cons fmod) l in
    if !ismod then
      if m < 5 then aux (m+1) l
      else
        begin
          Log.warn "still rewriting...";
          l
        end
    else l in
  let lsc = aux 0 sc_keep in
  if loc_debug then
    List.iter (fun c -> Log.info " rewrite_set_cons: %a" set_cons_fpr c) lsc;
  lsc
(* Generic funtion for sets *)
let gen_jinst_set_addctrs_post_isle
    (findkind: sv -> set_par_type option) (* Extraction of Set COLV types *)
    (call_vars: sv SvMap.t)(* Map set args from isle template to join output *)
    (svmap:   sv SvMap.t)       (* Is_le mapping for SV *)
    (setvmap: SvSet.t SvMap.t)  (* Is_le mapping for SETV parameters *)
    (sc: set_cons list)         (* Constraints to process *) (* over what ??? *)
    (inst: set_colv_inst)       (* Previous instantiation *)
    : set_colv_inst =
  let loc_debug = false in
  if loc_debug then
    begin
      Log.info "Inst-Post-IsLe:\n inst:\n%a constraints:%a"
        (set_colv_inst_fpri "  ") inst
        (gen_list_items_first_fpr ~iind:(3,16) set_cons_fpr) sc;
      Log.info " svmap:%a colvmap:%a call_SVs:%a"
        (SvMap.t_fpr_line ~iind:(9,16) ~lret:true ~kfpr:nsv_fpr nsv_fpr) svmap
        (SvMap.t_fpr_line ~iind:(7,16) ~lret:true ~kfpr:setv_fpr setvset_fpr)
        setvmap (SvMap.t_fpr_line ~iind:(6,16) ~kfpr:nsv_fpr nsv_fpr) call_vars
    end;
  let sv_rename i = SvMap.find_err "sv renaming" i svmap in
  let setv_rename i =
    try SvSet.min_elt (SvMap.find i setvmap)
    with Not_found -> Log.fatal_exn "setv renaming" in
  let setv_output i = SvMap.find_err "post_isle:mapping" i call_vars in
  (* index the set indexes that are not mapped *)
  let set_unmapped =
    let fsetv i s = if SvMap.mem i setvmap then s else SvSet.add i s in
    let aux_set_cons s e = set_cons_fold ~fsetv:fsetv e s in
    List.fold_left aux_set_cons SvSet.empty sc in
  (* Inspection functions *)
  let setv_is_unmapped (i: sv): bool = SvSet.mem i set_unmapped in
  let setv_is_mapped (i: sv): bool = not (setv_is_unmapped i) in
  let setv_is_call (i: sv): bool = SvMap.mem i call_vars in
  let inspect_setv ?(pp: bool = false) (i: sv): bool * bool =
    let is_call = setv_is_call i in
    let is_unmapped = setv_is_unmapped i in
    if loc_debug && pp then
      Log.info "    %a: %s-%s" setv_fpr i
        (if is_call then "parameter:t" else "generated:f")
        (if is_unmapped then "unmapped:t" else "mapped  :f");
    is_call, is_unmapped in
  let inspect_set_cons =
    set_cons_iter ~fsetv:(fun sv -> ignore (inspect_setv ~pp:true sv)) in
  (* Adding constraints over a setv *)
  let add_ctr i c acc =
    if loc_debug then Log.info "    +cons on %a" setv_fpr i;
    if SvMap.mem i acc then
      Log.fatal_exn "already a constraint on %a" setv_fpr i
    else SvMap.add i c acc in
  (* First, we process the constraints in several phases *)
  (* Phase 0. Inspect all the variables used and print constraints *)
  if loc_debug then
    List.iter
      (fun c ->
        Log.info "Inst,Post-IsLe,Set,Ph-C-0: %a" set_cons_fpr c;
        inspect_set_cons c
      ) sc;
  let _sc =
    let tokeep setv = setv_is_mapped setv || setv_is_call setv in
    rewrite_set_ctrs tokeep sc in
  (* Phase 1. Search for sets equal to empty, and simplify them away *)
  let remain, ctrs =
    List.fold_left
      (fun (acc_remain, acc_ctr) c ->
        if loc_debug then
          Log.info "Inst,Post-IsLe,Set,Ph-C-1: %a" set_cons_fpr c;
        match c with
        | S_eq (S_var i, S_empty) | S_eq (S_empty, S_var i) ->
            if inspect_setv i = (false, true) then
              acc_remain, add_ctr i `Is_empty acc_ctr
            else c :: acc_remain, acc_ctr
        | _ -> c :: acc_remain, acc_ctr
      ) ([ ], SvMap.empty) sc in
  let remain =
    let aux_setv i =
      try if SvMap.find i ctrs = `Is_empty then S_empty else S_var i
      with Not_found -> S_var i in
    List.map (transform_set_cons aux_setv) remain in
  (* Phase 2. Search the singletons and make a table of equalities
   *          among SETVs
   *  - simplify singletons
   *  - simplify and put aside constraints all variables of which are
   *    resolved *)
  let remain, equalities, proofs =
    let remain, ctrs, uf, eqcl =
      (* computes a union find to describe equalities, and a set of equality
       *  classes *)
      List.fold_left
        (fun (acc_remain, acc_ctr, uf, eqcl) c ->
          if loc_debug then
            Log.info "Inst,Post-IsLe,Set,Ph-C-2: %a" set_cons_fpr c;
          match c with
          | S_eq (S_var i, S_node j) | S_eq (S_node j, S_var i) ->
              if inspect_setv i = (false, true) (* generated, unmapped *) then
                acc_remain, add_ctr i (`Is_singleton j) acc_ctr, uf, eqcl
              else c :: acc_remain, acc_ctr, uf, eqcl
          | S_eq (S_var i, S_var j) ->
              let uf =
                if Union_find.mem i uf then uf else Union_find.add i uf in
              let uf =
                if Union_find.mem j uf then uf else Union_find.add j uf in
              let ii, uf = Union_find.find i uf in
              let jj, uf = Union_find.find j uf in
              let uf = Union_find.union ii jj uf in
              acc_remain, acc_ctr, uf, SvSet.add ii (SvSet.remove jj eqcl)
          | _ -> c :: acc_remain, acc_ctr, uf, eqcl
        ) ([ ], ctrs, Union_find.empty, SvSet.empty) (List.rev remain) in
    let renamer, equalities, proofs =
      (* for each equality class, decide to which SetV to rename it,
       * rename free SetVs and extend proof obligations for non free SetVs *)
      SvSet.fold
        (fun ii (accr, acce, accp) ->
          let cii = Union_find.find_class ii uf in
          let rename_class_to cpar =
            List.fold_left
              (fun accr v -> if v = cpar then accr else SvMap.add v cpar accr)
              accr cii in
          let call = List.filter setv_is_call cii in
          let mapped = List.filter setv_is_mapped cii in
          (* TODO:
           *  - when a mapping exists: USE IT to resolve variable
           *  - when no mapping exists: resolve equalities (done)
           *  - when several mapping exist: we need to prove it no ? *)
          let tgt_name =
            match call with
            | [ ] -> Log.fatal_exn "post_isle: no setv from call"
            | [ cpar ] -> cpar
            | _ ->
                Log.todo_exn "post_isle rename equalities, several choices" in
          let f_list fmt l =
            let s = List.fold_left (fun a x -> SvSet.add x a) SvSet.empty l in
            setvset_fpr fmt s in
          if loc_debug then
            begin
              let f fmt x =
                F.fprintf fmt "  %a=>%a" setv_fpr x setv_fpr (setv_rename x) in
              Log.info
                "class of %a:\n [ %a ] => %a\n - map:%a\n - call: %a\n%a"
                setv_fpr ii f_list cii setv_fpr tgt_name f_list mapped
                f_list call (gen_list_fpr "" f "\n") mapped
            end;
          let acce =
            match mapped with
            | [ ] -> acce
            | x :: _ -> SvMap.add tgt_name (setv_rename x) acce in
          let proofs =
            let rec aux l =
              match l with
              | [ ] | [ _ ] -> [ ]
              | a :: (b :: c as d) ->
                  S_eq (S_var (setv_rename a),
                        S_var (setv_rename b)) :: aux d in
            aux mapped in
          rename_class_to tgt_name, acce, proofs @ accp
        ) eqcl (SvMap.empty, SvMap.empty, [ ]) in
    let aux_setv i =
      try
        match SvMap.find i ctrs with
        | `Is_singleton k -> S_node k
        | _ -> S_var i
      with Not_found -> S_var (SvMap.find_val i i renamer) in
    List.map (transform_set_cons aux_setv) remain, equalities, proofs in
  (* Phase 3. Search for membership constraints, that can guide instantiation *)
  (*          and collect equalities of the form SETV=expr, where expr is not
   *          empty, a singleton, or a SETV, and then simplify them *)
  let remain, ctrs =
    let remain, ctrs, defs =
      List.fold_left
        (fun (acc_remain, acc_ctr, acc_defs) c ->
          if loc_debug then
            Log.info "Inst,Post-IsLe,Set,Ph-C-3: %a" set_cons_fpr c;
          match c with
          | S_eq (S_var i, e) ->
              if setv_is_call i then c :: acc_remain, acc_ctr, acc_defs
              else
                let b =
                  SvSet.fold (fun i a -> a && setv_is_mapped i)
                    (set_expr_setvs e) true in
                if b then acc_remain, acc_ctr, SvMap.add i e acc_defs
                else c :: acc_remain, acc_ctr, acc_defs
          | S_mem (i, S_var j) ->
              if setv_is_call j then
                acc_remain, add_ctr j (`Has_elt i) acc_ctr, acc_defs
              else
                begin
                  Log.warn "Inst,Post-IsLe,Set, dropped unhandled setin constr";
                  acc_remain, acc_ctr, acc_defs
                end
          | _ -> Log.todo_exn "unhandled set constraint (0)"
        ) ([ ], ctrs, SvMap.empty) remain in
    let aux_setv i = SvMap.find_val (S_var i) i defs in
    List.map (transform_set_cons aux_setv) remain, ctrs in
  (* XR: 2023/02/10: we should see SETVs that could/should be eliminated *)
  (* Now, build the result from the accumulated constraints *)
  (* - check empty *)
  let is_empty i = try SvMap.find i ctrs = `Is_empty with Not_found -> false in
  (* 0. simplify linear equalities with an empty member *)
  let res =
    List.fold_left
      (fun acc c ->
        if loc_debug then
          Log.info "Inst,Post-IsLe,Set,Phase-R-0: %a" set_cons_fpr c;
        match c with
        | S_eq (S_var i, S_uplus (S_node j, S_var k))
        | S_eq (S_var i, S_uplus (S_var k, S_node j)) ->
            if is_empty k then
              Log.todo_exn "Inst,Post-IsLe,Set, untransformed, empty"
            else if setv_is_unmapped i && setv_is_call i
                && setv_is_mapped k then
              colv_inst_bind (setv_output i)
                (S_uplus (S_node (sv_rename j), S_var (setv_rename k))) acc
            else (* still left to do *)
              begin
                Log.warn "may be unmapped: %a, dropped" setv_fpr i;
                acc
              end
        | S_eq (S_var i, S_uplus (S_var j, S_var k)) ->
            if setv_is_unmapped i && setv_is_call i
                && setv_is_mapped j && setv_is_mapped k then
              colv_inst_bind (setv_output i)
                (S_uplus (S_var (setv_rename j), S_var (setv_rename k))) acc
            else
              begin
                Log.warn "renaming issue: %a [%b,%b,%b,%b]" setv_fpr i
                  (setv_is_unmapped i) (setv_is_call i)
                  (setv_is_mapped j) (setv_is_mapped k);
                acc
              end
        | S_eq (S_var i, S_node j) ->
            if setv_is_call i then
              colv_inst_bind (setv_output i) (S_node (sv_rename j)) acc
            else (* to a renaming on i *)
              Log.todo_exn "Inst,Post-IsLe,Set, rename %a = %a"
                setv_fpr i setv_fpr j
        | S_eq (S_var i, e) ->
            if setv_is_call i then
              let e =
                try s_expr_map sv_rename setv_rename e
                with Not_found -> Log.fatal_exn "renaming failed" in
              colv_inst_bind (setv_output i) e acc
            else
              Log.fatal_exn "Inst,Post-IsLe,Set,renaming unbound %a" setv_fpr i
        | _ ->
            Log.todo_exn "Inst,Post-IsLe,Set, unhandled set constraint (2)"
      ) { inst with colvi_prove = proofs @ inst.colvi_prove } remain in
  (* 1. when the only constraints about a set S are of the form x \in S,
   *    let S = { x } *)
  let res =
    SvMap.fold
      (fun i ctr acc ->
        match ctr with
        | `Is_empty -> acc
        | `Is_renamed j ->
            Log.todo_exn "Inst,Post-IsLe,Set, add renamed: %a, %d" setv_fpr i j
        | `Is_singleton _ -> acc
        | `Has_elt j -> (* means x is only specified as having x as element *)
            colv_inst_guess (setv_output i) (sv_rename j) acc
      ) ctrs res in
  (* 2. when an equality x => y has been found as well as x contains z,
   *    keep x => y, and move property "x contains z" in the to check pile *)
  let res =
    SvMap.fold
      (fun i j acc ->
        let outi = setv_output i in
        if loc_debug then
          Log.info "tmp,Mapping %a [%a] as an element of %a => %b" setv_fpr i
            setv_fpr outi setv_fpr j (SvMap.mem outi acc.colvi_guess);
        if SvMap.mem outi acc.colvi_guess then
          let c = SvMap.find_err "inst-0" outi acc.colvi_guess in
          let cons = (S_mem (c, S_var j)) in
          colv_inst_bind outi (S_var j)
            { acc with
              colvi_guess = SvMap.remove outi acc.colvi_guess;
              colvi_prove = cons :: acc.colvi_prove }
        else colv_inst_bind outi (S_var j) acc
      ) equalities res in
  (* 3. mark any other SETV related to this call as unbound *)
  let inst =
    SvMap.fold
      (fun i j acc ->
        if loc_debug then
          Log.info "No spec on %a: %a => %s" setv_fpr i setv_fpr j
            (if colv_inst_bound j acc then "bound" else "unbound");
        if colv_inst_bound j acc then acc
        else { acc with colvi_none = SvSet.add j acc.colvi_none }
      ) call_vars res in
  (* SETVs that are mapped in is_le can be immediately added to the
   * instantiation (no resolution needed) *)
  let inst =
    SvMap.fold
      (fun i s acc ->
        if loc_debug then
          Log.info "considering: %a [is_call:%b]" setv_fpr i (setv_is_call i);
        if setv_is_call i then
          begin
            (* TODO: check if we are not losing some equalities here *)
            let j = SvMap.find_err "inst-1" i call_vars in
            if loc_debug then
              Log.info "parameter mapped : %a => %a" setv_fpr j setvset_fpr s;
            if SvSet.mem j acc.colvi_none then
              begin
                if SvSet.cardinal s > 1 then Log.fatal_exn "mapping";
                colv_inst_over_bind j (S_var (SvSet.min_elt s)) acc
              end
            else acc
          end
        else acc
      ) setvmap inst in
  if loc_debug then
    Log.info "processed ctrs:\n%a" (set_colv_inst_fpri "  ") inst;
  inst

(** Post join instantiation refinement; specific code to seq/set *)
(* SETs:
 *  - saturation of equalities of the for Si = Sj
 *  - guesses about membership constraints *)
let jinst_set_post_join_refine
    (roots: SvSet.t)            (* set roots *)
    (set_sat: set_cons -> bool) (* sat function; to prove constraints *)
    (inst: set_colv_inst)       (* initial instantiation *)
    : set_colv_inst =
  let loc_debug = false in
  (* 1. improve instantiation by binding roots instead of others
   *    if S[i] |=> S[k] and xnum |- S[k] = S[j], with S[j] root, then
   *    we use S[i] |=> S[j] instead *)
  let inst =
    SvMap.fold
      (fun setv ex acc ->
        match ex with
        | S_var setv0 ->
            if loc_debug then
              Log.info "jinst_set_post_join_refine,equality: %a => %a"
                setv_fpr setv0 set_expr_fpr ex;
            if SvSet.mem setv0 roots || SvSet.mem setv0 inst.colvi_rem then acc
            else
              (* setv0 is not root; search for roots provably equal to it *)
              let rootsok =
                SvSet.fold
                  (fun i acc ->
                    if set_sat (S_eq (S_var i, S_var setv0)) then
                      SvSet.add i acc
                    else acc
                  ) roots SvSet.empty in
              (* if any such root exists, map setv to it instead of setv0 *)
              if SvSet.cardinal rootsok > 0 then
                (* replace with the lowest root ID *)
                let root = SvSet.min_elt rootsok in
                { inst with
                  colvi_eqs = SvMap.add setv (S_var root) inst.colvi_eqs }
              else acc
        | _ -> acc
      ) inst.colvi_eqs inst in
  (* 2. adjust guesses based on membership *)
  let inst =
    SvMap.fold
      (fun setv sv acc ->
        let l =
          SvSet.fold
            (fun root acc ->
              if set_sat (S_mem (sv, S_var root)) then root :: acc
              else acc
            ) roots [ ] in
        match l with
        | root0 :: _ -> colv_inst_make_guess setv (S_var root0) acc
        | _ -> acc
      ) inst.colvi_guess inst in
  inst
(* SEQs:
 *  - saturation of equalities of the for Si = Sj *)
let jinst_seq_post_join_refine
    (roots: SvSet.t)
    (seq_sat: seq_cons -> bool)
    (inst: seq_colv_inst)
    : seq_colv_inst =
  (* 1. improve instantiation by binding roots instead of others
   *    if S[i] |=> S[k] and xnum |- S[k] = S[j], with S[j] root, then
   *    we use S[i] |=> S[j] instead *)
  let inst =
    SvMap.fold
      (fun seqv ex acc ->
        match ex with
        | Seq_var seqv0 ->
            if SvSet.mem seqv0 roots then acc
            else
              let rootsok =
                SvSet.fold
                  (fun i acc ->
                    if seq_sat (Seq_equal (Seq_var i, Seq_var seqv0)) then
                      SvSet.add i acc
                    else acc
                  ) roots SvSet.empty in
              if SvSet.cardinal rootsok > 0 then
                (* replace with the lowest root ID *)
                let root = SvSet.min_elt rootsok in
                { inst with
                  colvi_eqs = SvMap.add seqv (Seq_var root) inst.colvi_eqs }
              else acc
        | _ -> acc
      ) inst.colvi_eqs inst in
  inst

(** Integrating results in colv_inst into the numerical domain *)
(* Function written as a generic set of iterators over adds, rems, constrs *)
(* obvious candidate to make a functor *)
let gen_colv_inst_fold_ctr
    ~(expr_fpr: form -> 'a -> unit) ~(cons_fpr: form -> 'b -> unit)
    ~(colv_add: sv -> 'c -> 'c) ~(colv_rem: sv -> 'c -> 'c)
    ~(guard: 'b -> 'c -> 'c) ~(eqcons: sv -> 'a -> 'b) ~(sat: 'b -> 'c -> bool)
    (inst: ('a,'b) colv_inst) (xnum: 'c) =
  let loc_debug = false in
  let prefix = match inst.colvi_kind with CK_set -> "set" | CK_seq -> "seq" in
  (* - add new setvs *)
  let xnum = colv_inst_fold_add colv_add inst xnum in
  (* - add new constraints *)
  let xnum =
    SvMap.fold
      (fun i ex acc ->
        if loc_debug then
          Log.info "Adding_%scons: %a |=> %a" prefix
            (colv_fpr inst.colvi_kind) i expr_fpr ex;
        guard (eqcons i ex) acc
      ) inst.colvi_eqs xnum in
  let xnum =
    List.fold_left
      (fun acc c ->
        if loc_debug then
          Log.info "Adding_%s-prop: %a" prefix cons_fpr c;
        guard c acc
      ) xnum inst.colvi_props in
  (* - verify that the instantiation is admissible *)
  List.iter
    (fun c ->
      if loc_debug then Log.info "verifying set constraint %a" cons_fpr c;
      if not (sat c xnum) then
        Log.fatal_exn "set instantiation fails: %a" cons_fpr c
    ) inst.colvi_prove;
  (* - remove the deprecated setvs *)
  if loc_debug then Log.info "tmp,setv,f_add_colcons";
  colv_inst_fold_rem colv_rem inst xnum
