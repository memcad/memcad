(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_inst.ml
 **       Instantiation of set parameters for the list domain join
 ** Xavier Rival, 2015/07/16 *)
open Data_structures
open Lib
open Sv_def

open Inst_sig
open List_sig
open Set_sig
open Seq_sig

open Inst_utils
open List_utils
open Set_utils
open Seq_utils
open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "l_ins___" and level = Log_level.DEBUG end)

(** Management of internal dimensions in the instantiation *)
(* Addition/removal of COLVs coming from inductive/segments
 *  (no constraints are added; these functions only add/remove the parameters *)
let jinst_set_addpars_icall (lc: l_call): set_colv_inst -> set_colv_inst =
  Inst_utils.colv_inst_add_pars lc.lc_setargs
let jinst_set_rempars_icall (lc: l_call): set_colv_inst -> set_colv_inst =
  Inst_utils.colv_inst_rem_pars lc.lc_setargs
let jinst_seq_addpars_icall (lc: l_call): seq_colv_inst -> seq_colv_inst =
  Inst_utils.colv_inst_add_pars lc.lc_seqargs
let jinst_seq_rempars_icall (lc: l_call): seq_colv_inst -> seq_colv_inst =
  Inst_utils.colv_inst_rem_pars lc.lc_seqargs

(** Constraints added to instantiation by introduction rule *)
(* Add to instantiation constraints associated with empty segment for
 * introduction in join; first argument is introduced segment parameters
 *  for empty segments:   "head" and "add" parameters are empty
 *                        "const"          parameters are not constrained *)
let l_jinst_set_addcstr_segemp (lc: l_call) (inst: set_colv_inst)
    : set_colv_inst =
  let args =
    match lc.lc_def.ld_set with
    | None -> [ ]
    | Some e -> e.s_params in
  Inst_utils.gen_jinst_set_addcstr_segemp lc.lc_setargs args inst
let l_jinst_seq_addcstr_segemp (lc: l_call) (inst: seq_colv_inst)
    : seq_colv_inst =
  let args =
    match lc.lc_def.ld_seq with
    | None -> [ ]
    | Some e -> e.q_params in
  try
    List.fold_left2
      (fun inst l kind ->
        let inst = Inst_utils.colv_inst_add l inst in
        (* If a parameter is "monotone", then we can fix it to be anything,
         * it will always work for the empty segment *)
        if kind.sq_add || kind.sq_head then
          begin
            Log.debug "binding : %a -> [ ]" seqv_fpr l;
            Inst_utils.colv_inst_bind l Seq_empty inst
          end
        else if kind.sq_const then
          Inst_utils.colv_inst_nobind l inst
        else Log.todo_exn "unhandled kind"
      ) inst lc.lc_seqargs args
  with Not_found -> Log.fatal_exn "l_call_empty: seq parameters not match"

(** Integrating unresolved set constraints from is_le into inst *)
(* For sets based on the general function *)
let l_jinst_set_addctrs_post_isle
    ~(msg: string)               (* Location of the call *)
    (findkind: sv -> set_par_type option) (* Extraction of Set COLV types *)
    (lc_comp: l_call)            (* Call used for the comparison *)
    (lc_out:  l_call)            (* Call produced in the output *)
    (svmap:   sv SvMap.t)        (* Is_le mapping for symbolic variables *)
    (setvmap: SvSet.t SvMap.t)   (* Is_le mapping for set parameters *)
    (sc: set_cons list)          (* Constraints to process *)
    (inst: set_colv_inst)        (* Previous instantiation *)
    : set_colv_inst =
  let loc_debug = false in
  if loc_debug then
    Log.debug "l_jinst_set_addctrs_post_isle(%s),pre: %d\n%aCtrs:\n%a" msg
      (List.length lc_out.lc_setargs) (set_colv_inst_fpri "    ") inst
      (gen_list_fpr "" set_cons_fpr ", ") sc;
  (* Part that seems specific to lists *)
  let inst =
    List.fold_left (fun acc i -> colv_inst_add i acc)
      inst lc_out.lc_setargs in
  (* set indexes that are generated *)
  let call_vars: sv SvMap.t =
    List.fold_left2 (fun acc iw io -> SvMap.add iw io acc)
      SvMap.empty lc_comp.lc_setargs lc_out.lc_setargs in
  let nsinsto =
    Inst_utils.gen_jinst_set_addctrs_post_isle findkind call_vars
      svmap setvmap sc inst in
  if loc_debug then
    Log.debug "l_jinst_set_addctrs_post_isle(%s),pre: %b (..)" msg
      (nsinsto = inst);
  nsinsto
(* For seqs *)
let l_jinst_seq_addctrs_post_isle
    ~(msg: string)               (* Location of the call *)
    (findkind: sv -> seq_par_type option) (* Extraction of Seq COLV types *)
    (lc_comp: l_call)            (* Call used for the comparison *)
    (lc_out:  l_call)            (* Call produced in the output *)
    (svmap:   sv SvMap.t)        (* Is_le mapping for symbolic variables *)
    (seqvmap: SvSet.t SvMap.t)   (* Is_le mapping for seq parameters *)
    (sc: seq_cons list)          (* Constraints to process *)
    (inst: seq_colv_inst)        (* Previous instantiation *)
    : seq_colv_inst =
  (* First, we log ! *)
  let loc_debug = false in
  if loc_debug then
    Log.info "l_jinst_seq_addctrs_post_isle(%s),pre:\n%a\nseq_cons = \n  %a"
      msg (Inst_utils.seq_colv_inst_fpri "  ") inst
      (gen_list_fpr "[]" seq_cons_fpr "\n  ") sc;
  (* For renaming SVs we use the provided map *)
  let rename_sv sv =
    try SvMap.find sv svmap
    with Not_found -> sv in
  (* For renaming Seq COLVs we first try to use seqvmap and if it fails,
   * we map seq arguments from lc_comp to lc_out *)
  let rename_seqv =
    let seqvmap_compout =
      List.fold_left2
        (fun map setv_comp setv_out ->
          assert (not (SvMap.mem setv_comp map));
          SvMap.add setv_comp setv_out map)
      SvMap.empty
      lc_comp.lc_seqargs
      lc_out.lc_seqargs in
    fun seqv ->
      try
        SvSet.min_elt (SvMap.find seqv seqvmap)
      with Not_found ->
        try SvMap.find seqv seqvmap_compout
        with Not_found -> seqv
    in
  (* We add the Seq COLVs defined in lc_out in inst *)
  let inst =
    List.fold_left (fun acc i -> Inst_utils.colv_inst_add i acc)
      inst lc_out.lc_seqargs in
  (* We rename the seq_cons using the rename_* functions *)
  let sc = List.map (q_cons_map rename_seqv rename_sv) sc in
  if loc_debug then
    Log.info "l_jinst_seq_addctrs_post_isle(%s),post: seq_cons = \n  %a" msg
      (gen_list_fpr "[]" seq_cons_fpr "\n  ") sc;
  (* We return the instanciation and we add the constraints *)
  { inst with colvi_props = sc @ inst.colvi_props }

(** Instantiation obtained from a pair of calls
 *   (maintains the set parameters associated to the call) *)
(* Add to instantiation constraints associated with an inductive edge or
 * segment matching *)
let jinst_set_addcstr_icall_eqs (lc_i: l_call) (lc_o: l_call)
    : set_colv_inst -> set_colv_inst =
  Inst_utils.gen_jinst_set_addcstr_icall_eqs lc_i.lc_setargs lc_o.lc_setargs
let jinst_seq_addcstr_icall_eqs (lc_i: l_call) (lc_o: l_call)
    : seq_colv_inst -> seq_colv_inst =
  Inst_utils.gen_jinst_seq_addcstr_icall_eqs lc_i.lc_seqargs lc_o.lc_seqargs

(** List specific instantiation heuristics *)
(* Try to guess some instantiation constraints for const set parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
(* [XR]: check this instantiation scheme later *)
let jinst_set_guesscstr_segemp (is: sv) (lc: l_call) (lm: lmem)
    (inst_set: set_colv_inst): set_colv_inst =
  let argst =
    match lc.lc_def.ld_set with
    | None -> [ ]
    | Some e -> e.s_params in
  try
    let lnode = SvMap.find_err "jinst_set_guesscstr_segemp" is lm.lm_mem in
    let lcext =
      match lnode.ln_e with
      | Lhlist l_c -> l_c
      | Lhlseg (l_c, nid, lsegc) -> l_c
      | Lhemp | Lhpt _ -> raise Not_found in
    assert (l_call_compare lc lcext = 0);
    Inst_utils.gen_jinst_set_guesscstr_segemp lc.lc_setargs lcext.lc_setargs
      argst inst_set
  with Not_found ->
    inst_set
(* Apply l_const_inst exhaustively to compute as many instantiation constraints,
 * as post-analysis step *)
let jinst_set_guesscstr_segs_postjoin (lm: lmem)
    (inst_set: set_colv_inst): set_colv_inst =
  SvMap.fold
    (fun key ele acc ->
      match ele.ln_e with
      | Lhlseg (lc, eid, lsegc) -> jinst_set_guesscstr_segemp eid lc lm acc
      | Lhpt _ | Lhemp | Lhlist _  -> acc
    ) lm.lm_mem inst_set

(** List specific instantiation heuristics, for seqs *)
(* Try to guess some instantiation constraints for const seq parameters
 * when a segment or an inductive edge is introduced, using an edge nearby
 * (other segment or full inductive predicate with the same inductive kind) *)
(* JG: This function should not be useful for my work....
 * It's left here for the sake of ... *)
let jinst_seq_guesscstr_segemp (is: sv) (lc: l_call) (lm: lmem)
    (inst_seq: seq_colv_inst): seq_colv_inst =
  if true then (* This function does nothing ! *)
    inst_seq
  else
    let args =
      match lc.lc_def.ld_seq with
      | None -> [ ]
      | Some e -> e.q_params in
    try
      let lnode = SvMap.find_err "l_call_const_inst_seq" is lm.lm_mem in
      let lc_extract =
        match lnode.ln_e with
        | Lhlist l_c -> l_c
        | Lhlseg (l_c, nid, lsegc) -> l_c
        | Lhemp | Lhpt _ -> raise Not_found in
      assert (l_call_compare lc lc_extract = 0);
      let _, _, inst_seq =
        List.fold_left
          (fun (lc_args, lc_ex_args, inst_seq) par_kind ->
            let inst_seq =
              if par_kind.sq_add || par_kind.sq_head then inst_seq
              else if par_kind.sq_const then
                let l, r = List.hd lc_args, List.hd lc_ex_args in
                let inst_seq =
                  if SvSet.mem l inst_seq.colvi_none &&
                    SvMap.mem r inst_seq.colvi_eqs then
                    let c =
                      SvMap.find_err "eq_const:over" r inst_seq.colvi_eqs in
                    Inst_utils.colv_inst_over_bind l c inst_seq
                  else inst_seq in
                inst_seq
              else Log.todo_exn "unhandled kind-1" in
            List.tl lc_args, List.tl lc_ex_args, inst_seq
          ) (lc.lc_seqargs, lc_extract.lc_seqargs, inst_seq) args in
      inst_seq
    with Not_found -> inst_seq
(* Apply l_const_inst exhaustively to compute as many instantiation constraints,
 * as post-analysis step *)
let jinst_seq_guesscstr_segs_postjoin (lm: lmem)
    (inst_seq: seq_colv_inst): seq_colv_inst =
  SvMap.fold
    (fun key ele acc ->
      match ele.ln_e with
      | Lhlseg (lc, eid, lsegc) -> jinst_seq_guesscstr_segemp eid lc lm acc
      | Lhpt _ | Lhemp | Lhlist _  -> acc
    ) lm.lm_mem inst_seq
