(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: sum_sep_utils.ml
 **       Basic functions for abstraction based on separating conjunction
 **       summaries, with predicates defined by extension over sets
 ** Vincent Rebiscoul and Xavier Rival, 2019/03/04 *)
open Data_structures
open Lib

open Sum_sep_sig

(* Empty graph *)
val smem_empty: smem

