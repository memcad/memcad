(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_utils.ml
 **       utility functions on graphs
 ** Xavier Rival, 2011/05/21 *)
open Data_structures
open Flags
open Lib
open Offs
open Apron
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open Nd_sig
open Svenv_sig
open Set_sig
open Dom_sig
open Inst_sig
open Vd_sig

open Gen_dom

open Ast_utils
open Ind_utils
open Nd_utils
open Set_utils
open Sv_utils
open Vd_utils


(** Possible improvements:
 **  - efficiency gain in graph_rename_ids: do not traverse the whole graph,
 **    but use the "prev" parameters instead
 **)


(** Error report *)
module Log =
  Logger.Make(struct let section = "g_uts___" and level = Log_level.DEBUG end)

let ind_args_seg_to_ind_args ias =
  { ia_ptr = ias.ias_ptr;
    ia_int = ias.ias_int;
    ia_set = ias.ias_set;
    ia_seq = List.map (fun (x,_) -> x) ias.ias_seq}

(** Creation *)
(* Takes a set of inductive definitions *)
let graph_empty (inds: StringSet.t): graph =
  { g_inds       = inds;
    g_nkey       = SvGen.empty;
    g_g          = SvMap.empty;
    g_svemod     = Dom_utils.svenv_empty;
    g_roots      = SvSet.empty;
    g_colvkey    = SvGen.empty;
    g_colvroots  = SvSet.empty;
    g_colvbindex = SvMap.empty;
    g_colvkind   = SvMap.empty;
    g_colvinfo   = SvMap.empty; }


(** Pretty-printing *)
(* Format printers *)
let nid_fpr: form -> sv -> unit = Nd_utils.nsv_fpr
let onode_fpr (fmt: form) ((ni,off): Offs.svo): unit =
  if Flags.flag_pp_off0 || not (Offs.is_zero off) then
    F.fprintf fmt "%a%a" nid_fpr ni Offs.t_fpr off
  else nid_fpr fmt ni
let svo_fpr: form -> Offs.svo -> unit = onode_fpr
let bnode_fpr (fmt: form) ((ni,bnd): bnode): unit =
  if Flags.flag_pp_off0 || not (Bounds.is_zero bnd) then
    F.fprintf fmt "%a%a" nid_fpr ni Bounds.t_fpr bnd
  else nid_fpr fmt ni
let nalloc_fpr (fmt: form): nalloc -> unit = function
  | Nheap    -> F.fprintf fmt "[heap]"
  | Nstack   -> F.fprintf fmt "[stack]"
  | Nstatic  -> F.fprintf fmt "[static]"
  | Nnone    -> F.fprintf fmt "[none]"
  | Nsubmem  -> F.fprintf fmt "[submem]"
let nalloc_fprs (fmt: form): nalloc -> unit = function
  | Nheap    -> F.fprintf fmt "hp"
  | Nstack   -> F.fprintf fmt "sk"
  | Nstatic  -> F.fprintf fmt "sc"
  | Nnone    -> F.fprintf fmt ""
  | Nsubmem  -> F.fprintf fmt "su"
let node_attribute_fpr (fmt: form) (a: node_attribute): unit =
  match a with
  | Attr_none -> F.fprintf fmt "(noattr)"
  | Attr_ind attr -> F.fprintf fmt "(attr: %s)" attr
  | Attr_array (st, None) -> F.fprintf fmt "(array stride: %d)" st
  | Attr_array (st, Some _) -> F.fprintf fmt "(array stride: %d + fields)" st
let pt_edge_fpr (fmt: form) ((src, pe): bnode * pt_edge): unit =
  let s_arrow =
    if Flags.flag_pp_size4 || Offs.size_to_int_opt pe.pe_size != Some 4 then
      F.asprintf "-%a->" Offs.size_fpr pe.pe_size
    else "->" in
  F.fprintf fmt "%a %s %a" bnode_fpr src s_arrow onode_fpr pe.pe_dest
let ind_args_fpr (fmt: form) (ia: ind_args): unit =
  let l_fpr = gen_list_fpr "" (fun f -> F.fprintf f "<%a>" sv_fpr) "," in
  let lset_fpr = gen_list_fpr "" (fun f -> F.fprintf f "S[%a]" sv_fpr) "," in
  let lseq_fpr = gen_list_fpr "" (fun f -> F.fprintf f "Q[%a]" sv_fpr) "," in
  F.fprintf fmt "%a|%a|%a|%a"
    l_fpr ia.ia_ptr l_fpr ia.ia_int
    lset_fpr ia.ia_set lseq_fpr ia.ia_seq
let ind_args_seg_fpr (fmt: form) (ia: ind_args_seg): unit =
  let l_fpr = gen_list_fpr "" (fun f -> F.fprintf f "<%a>" sv_fpr) "," in
  let lset_fpr = gen_list_fpr "" (fun f -> F.fprintf f "S[%a]" sv_fpr) "," in
  let lseq_fpr = gen_list_fpr ""
    (fun f (sl, sr) -> F.fprintf f "Q[%a]☒Q[%a]" sv_fpr sl sv_fpr sr ) "," in
  F.fprintf fmt "%a|%a|%a|%a"
    l_fpr ia.ias_ptr l_fpr ia.ias_int
    lset_fpr ia.ias_set lseq_fpr ia.ias_seq
let ind_edge_fpr (fmt: form) (ie: ind_edge): unit =
  F.fprintf fmt "==%s(%a)==>" ie.ie_ind.i_name ind_args_fpr ie.ie_args
let seg_edge_fpr (fmt: form) (se: seg_edge): unit =
  F.fprintf fmt "=%s(%a)==(%a)==%s(%a)=>"
    se.se_ind.i_name ind_args_fpr se.se_sargs ind_args_seg_fpr se.se_eargs
    se.se_ind.i_name ind_args_fpr se.se_dargs
let block_fpria (ind: string) (fmt: form) ((is, mc): sv * pt_edge Block_frag.t)
    : unit =
  Block_frag.iter_list_base
    (fun bnd (pe: pt_edge) ->
      F.fprintf fmt "%s%a\n" ind pt_edge_fpr ((is, bnd), pe)
    ) mc
let heap_frag_fpri (ind: string) (fmt: form) ((is, e): sv * heap_frag): unit =
  match e with
  | Hemp -> ()
  | Hpt mc -> block_fpria ind fmt (is, mc)
  | Hind ie ->
      F.fprintf fmt "%s%a %a\n" ind nid_fpr is ind_edge_fpr ie
  | Hseg se ->
      F.fprintf fmt "%s%a %a %a\n"
        ind nid_fpr is
        seg_edge_fpr se
        nid_fpr se.se_dnode
let node_fpri (indent: string) (fmt: form) (n: node): unit =
  if !flag_pp_nodeinfos then
    begin
      F.fprintf fmt "%s%a: %a" indent nid_fpr n.n_i ntyp_fpr n.n_t;
      if Flags.flag_pp_nodeattr then
        F.fprintf fmt " %a" node_attribute_fpr n.n_attr;
      if Flags.flag_pp_nodealloc then
        nalloc_fpr fmt n.n_alloc;
      F.fprintf fmt "\n%a" (heap_frag_fpri indent) (n.n_i, n.n_e);
      if Flags.flag_back_index && !Flags.flag_dbg_back_index then
        F.fprintf fmt "%s%a preds: %a\n" indent nid_fpr n.n_i
          Nd_utils.nsvset_fpr n.n_preds
    end
  else (* otherwise, print only edges *)
    heap_frag_fpri indent fmt (n.n_i, n.n_e)
let graph_fpri (ind: string) (fmt: form) (g: graph): unit =
  let colv_roots =
    SvMap.fold
      (fun cv k acc ->
        if SvSet.mem cv g.g_colvroots then SvMap.add cv k acc
        else acc
      ) g.g_colvkind SvMap.empty in
  F.fprintf fmt "%sCOLVs: { %a }\n" ind col_kinds_fpr
    (Col_utils.col_par_type_map_to_col_kinds g.g_colvkind);
  F.fprintf fmt "%sSV-roots: { %a }\n" ind Nd_utils.nsvset_fpr g.g_roots;
  F.fprintf fmt "%sCOLV-roots: { %a }\n" ind col_kinds_fpr
    (Col_utils.col_par_type_map_to_col_kinds colv_roots);
  SvMap.iter (fun _ (n: node) -> node_fpri ind fmt n) g.g_g;
  SvMap.iter
    (fun colv info ->
      let ptype = SvMap.find colv g.g_colvkind in
      Format.fprintf fmt "%s%a |-> %a\n" ind
        (colv_fpr (Col_utils.col_par_type_to_col_kind ptype)) colv
        Col_utils.colv_info_fpr info
    ) g.g_colvinfo;
  if !Flags.flag_dbg_symvars then
    F.fprintf fmt "%sSVE-Mod-graph:\n%a" ind
      (Dom_utils.svenv_fpri (ind^"  ")) g.g_svemod
(* Compact node output *)
let heap_frag_fprc (fmt: form): heap_frag -> unit = function
  | Hemp -> F.fprintf fmt "[emp]"
  | Hpt _ -> F.fprintf fmt "[pt]"
  | Hind _ -> F.fprintf fmt "[ind]"
  | Hseg _ -> F.fprintf fmt "[seg]"
(* Algorithms results *)
let is_le_res_fpr (fmt: form): is_le_res -> unit = function
  | Ilr_not_le -> F.fprintf fmt "not_le"
  | Ilr_le_rem _ -> F.fprintf fmt "le_rem [..]"
  | Ilr_le_ind _ -> F.fprintf fmt "le_ind [..]"
  | Ilr_le_seg _ -> F.fprintf fmt "le_seg [..]"
(* Hints pretty-printing *)
let hint_ug_fpr (fmt: form) (h: hint_ug): unit =
  F.fprintf fmt "live: %a" (Aa_sets.t_fpr "; " sv_fpr) h.hug_live
let hint_bg_fpr (fmt: form) (h: hint_bg): unit =
  F.fprintf fmt "live: %a"
    (Aa_maps.t_fpr "" "\n"
       (fun fmt (i, j) -> F.fprintf fmt "%a => %a" sv_fpr j sv_fpr i))
    h.hbg_live
(* Node embedding pretty-printing *)
let node_emb_fpr (fmt: form) (ni: node_emb): unit =
  Aa_maps.t_fpr "" "\n"
    (fun fmt (i, j) -> F.fprintf fmt "%a => %a" sv_fpr i sv_fpr j) fmt ni
(* Freshsets *)
let freshset_fpr (fmt: form) (fs: freshset): unit =
  F.fprintf fmt "<ptr:%a | int:%a | col:{ %a }>"
    nsvset_fpr fs.f_int nsvset_fpr fs.f_ptr col_kinds_fpr fs.f_col
(* Results of unfolding *)
let unfold_res_fpri (ind: string) (fmt: form) (ur: unfold_res): unit =
  let nind = "  "^ind in
  let fiter n f l = List.iter (F.fprintf fmt "%s%s: %a\n" ind n f) l in
  let newcolvs = SvMap.map fst ur.ur_newcolvs in
  F.fprintf fmt "%sUnfold result graph:\n%a" ind (graph_fpri nind) ur.ur_g;
  fiter "Cons" Nd_utils.n_cons_fpr ur.ur_cons;
  fiter "Set-cons" Set_utils.set_cons_fpr ur.ur_setcons;
  fiter "Seq-cons" Seq_utils.seq_cons_fpr ur.ur_seqcons;
  F.fprintf fmt "%sNew-SVs: %a\n%sNew-COLVs: %a\n%sRem-COLVs: %a\n"
    ind Nd_utils.nsvset_fpr ur.ur_news ind col_kinds_fpr newcolvs
    ind col_kinds_fpr ur.ur_remcolvs

(** Reachability from a node *)
let pt_edge_reach_acc (acc: SvSet.t) (pte: pt_edge): SvSet.t =
  let s = Offs.size_sym_var_ids_add acc pte.pe_size in
  let s = Offs.t_sym_var_ids_add s (snd pte.pe_dest) in
  SvSet.add (fst pte.pe_dest) s
let pt_edge_reach (pte: pt_edge): SvSet.t = pt_edge_reach_acc SvSet.empty pte
let pt_edge_block_frag_reach (m: pt_edge Block_frag.t): SvSet.t =
  Block_frag.reach pt_edge_reach_acc m
let ind_args_reach_acc (x: graph) (ia: ind_args) (acc: SvSet.t): SvSet.t =
  let add_args acc args = List.fold_left (fun a i -> SvSet.add i a) args acc in
  let add_info acc colv =
    match SvMap.find_opt colv x.g_colvinfo with
    | None -> acc
    | Some {min; max; size} ->
        acc |> SvSet.add min |> SvSet.add max |> SvSet.add size in
  let add_colvs colv_args acc = List.fold_left add_info acc colv_args in
  acc
  |> add_colvs ia.ia_set
  |> add_colvs ia.ia_seq
  |> add_args ia.ia_int
  |> add_args ia.ia_ptr

let ind_args_reach_acc_seg (x: graph) (ia: ind_args_seg) (acc: SvSet.t)
    : SvSet.t =
  let add_args acc args = List.fold_left (fun a i -> SvSet.add i a) args acc in
  let add_info acc colv =
    match SvMap.find_opt colv x.g_colvinfo with
    | None -> acc
    | Some {min; max; size} ->
        acc |> SvSet.add min |> SvSet.add max |> SvSet.add size in
  let add_colvs colv_args acc = List.fold_left add_info acc colv_args in
  acc
  |> add_colvs ia.ias_set
  |> add_colvs (List.map_flatten (fun (x,y) -> [x; y]) ia.ias_seq)
  |> add_args ia.ias_int
  |> add_args ia.ias_ptr

let ind_args_reach (ia: ind_args) (x: graph): SvSet.t =
  ind_args_reach_acc x ia SvSet.empty
let ind_edge_reach (ie: ind_edge) (x: graph): SvSet.t =
  ind_args_reach ie.ie_args x
let seg_edge_reach (se: seg_edge) (x: graph): SvSet.t =
  SvSet.singleton se.se_dnode
  |> ind_args_reach_acc x se.se_sargs
  |> ind_args_reach_acc_seg x se.se_eargs
  |> ind_args_reach_acc x se.se_dargs
(* Main reachability function for a node *)
let node_reach (n: node) (x: graph): SvSet.t =
  match n.n_e with
  | Hemp -> SvSet.empty
  | Hpt pt -> pt_edge_block_frag_reach pt
  | Hind ie -> ind_edge_reach ie x
  | Hseg se -> seg_edge_reach se x

(** Sanity checks *)
let graph_sanity_check (loc: string) (g: graph): unit =
  (* auxilliary functions *)
  let error (s: string): unit =
    Log.fatal_exn "Graph sanity check fails (%s): %s\n%a"
      loc s (graph_fpri "") g in
  let m_back: SvSet.t SvMap.t ref = ref SvMap.empty in
  let add_backs (pre: sv) (posts: SvSet.t): unit =
    SvSet.iter
      (fun post ->
        let o = try SvMap.find post !m_back with Not_found -> SvSet.empty in
        let n = SvSet.add pre o in
        m_back := SvMap.add post n !m_back
      ) posts in
  (* temporary check on the hseg edges, for the collection arguments *)
  let check_sd_ind_args ia =
    if ia.ia_set != [ ] || ia.ia_seq != [ ] then
      Log.warn "HSEG (%s): needs modification (src|tgt)" loc in
  let check_seg_args  ia =
    if ia.ias_ptr != [ ] || ia.ias_int != [ ] then
      Log.warn "HSEG (%s): needs modification (edge)" loc in
  SvMap.iter
    (fun i n ->
      match n.n_e with
      | Hseg hs ->
          check_sd_ind_args hs.se_sargs;
          check_sd_ind_args hs.se_dargs;
          check_seg_args hs.se_eargs;
      | _ -> ( )
    ) g.g_g;
  (* TODO check consistency of colv back_index *)
  (* soundness of the key-gen *)
  let nodes = SvMap.fold (fun i _ -> SvSet.add i) g.g_g SvSet.empty in
  SvGen.sanity_check ("graph:"^loc) nodes g.g_nkey;
  (* table of nodes *)
  SvMap.iter
    (fun i n ->
      (* node index should be consistent *)
      if i != n.n_i then error (Format.asprintf "node name: %a" sv_fpr i);
      (* nodes pointed to should belong to the graph *)
      let r = node_reach n g in
      SvSet.iter
        (fun j ->
          if not (SvMap.mem j g.g_g) then
            error (Format.asprintf "node %a pointed to by %a" sv_fpr j sv_fpr i)
        ) r;
      add_backs i r
    ) g.g_g;
  (* Checking colvs and information consistency *)
  let check_exist (sv: sv) = if not @@ SvMap.mem sv g.g_g then
    let msg =
      F.asprintf "Information variable %a is not in graph" sv_fpr sv in
    error msg in
  SvMap.iter
    (fun colv { min; max; size } ->
      if not @@ SvMap.mem colv g.g_colvkind then
        error (F.asprintf "Colv %a has info but is not in graph" sv_fpr colv);
      check_exist min;
      check_exist max;
      check_exist size
    ) g.g_colvinfo;
  (* back indexes should be consistent *)
  if Flags.flag_back_index then
    SvMap.iter
      (fun i n ->
        let found_pres =
          try SvMap.find i !m_back with Not_found -> SvSet.empty in
        if not (SvSet.equal found_pres n.n_preds) then
          error (Format.asprintf "prevs do not mach for node %a" sv_fpr i)
      ) g.g_g;
  (* nodes in g_add should belong to the graph *)
  PMap.iter
    (fun i _ ->
      if not (SvMap.mem i g.g_g) then
        error (Format.asprintf "g_add; node %a" sv_fpr i)
    ) g.g_svemod.svm_add;
  (* nodes in g_rem should not belong to the graph *)
  PSet.iter
    (fun i ->
      if SvMap.mem i g.g_g then
        error (Format.asprintf "g_rem; node %a" sv_fpr i)
    ) g.g_svemod.svm_rem

(** Temporary code, sanity check on the hseg type *)
let hseg_sanity_check ~(isok: int) (loc: string) (se: seg_edge): unit =
  let show = false in
  if isok < 0 then
    begin
      assert (se.se_eargs.ias_set = [] && se.se_eargs.ias_seq = []);
      assert (se.se_sargs.ia_set = [] && se.se_sargs.ia_seq = []);
      assert (se.se_dargs.ia_set = [] && se.se_dargs.ia_seq = []);
      if show then Log.info "HSEG-OK(unsupported colv, no colv)"
    end
  else if isok > 0 then
    (* TODO eliminate these cases; should be useless now *)
    let str =
      if isok > 5 then "untouched"
      else if isok = 1 then "updated, should be ok"
      else "touched, doubtful" in
    Log.info "HSEG (%s) call... %s[%d]\n" loc str isok
  else if show then
    Log.info "HSEG-OK (%s)\n" loc;
  (* there should be no collection argument at source or destination *)
  let do_sdargs side ia =
    if ia.ia_set != [ ] then
      Log.warn "HSEG (%s): hseg needs modification (%s,set)" loc side;
    if ia.ia_seq != [ ] then
      Log.warn "HSEG (%s): hseg needs modification (%s,seq)" loc side in
  do_sdargs "src" se.se_sargs;
  do_sdargs "tgt" se.se_dargs;
  (* edge arguments should match *)
  if se.se_eargs.ias_int != [ ] || se.se_eargs.ias_ptr != [ ] then
    Log.warn "HSEG (%s): hseg needs modification (edge, extra int or ptr)" loc;
  if List.length se.se_eargs.ias_set != se.se_ind.i_spars then
    Log.warn "HSEG (%s): hseg needs modification (edge, set missing)" loc;
  if List.length se.se_eargs.ias_seq != se.se_ind.i_qpars then
    Log.warn "HSEG (%s): hseg needs modification (edge, seq missing)" loc

(** Checks whether an inductive predicates satisfies conditions for hseg *)
let is_ind_collection_sane (loc: string) (ind: ind): unit =
  (* TODO: duplicated type, consider merging the seq and set par types *)
  IntMap.iter
    (fun i st ->
      if not (Set_utils.set_par_type_is_const st
            || Set_utils.set_par_type_is_add st) then
        Log.fatal_exn "HSEG,setpar(%s) %d : %a\n" loc i
          Set_utils.set_par_type_fpr st
    ) ind.i_pkind.pr_set;
  IntMap.iter
    (fun i st ->
      if not (Seq_utils.seq_par_type_is_const st
            || Seq_utils.seq_par_type_is_add st) then
        Log.fatal_exn "HSEG,seqpar(%s) %d : %a\n" loc i
          Seq_utils.seq_par_type_fpr st
    ) ind.i_pkind.pr_seq



(** Management of back indexes *)
(* addition of a back index *)
let add_bindex (san: bool) (* sanity check after ? *)
    (c: string) (pre: sv) (post: sv) (t: graph): graph =
  if !Flags.flag_dbg_back_index then
    Log.force "add_bindex<%s>: %a->%a\n" c sv_fpr pre sv_fpr post;
  let pnode =
    try SvMap.find post t.g_g
    with Not_found ->
      Log.fatal_exn "add_bindex:\n%a\nnf <%s>: %a"
        (graph_fpri "  ") t c sv_fpr post in
  let npnode = { pnode with
                 n_preds = SvSet.add pre pnode.n_preds } in
  let tt = { t with g_g = SvMap.add post npnode t.g_g } in
  if san then
    graph_sanity_check (F.asprintf "add_bindex,after<%s>" c) tt;
  tt
let add_bindexes (san: bool) (* sanity check after ? *)
    (c: string) (pre: sv) (posts: SvSet.t) (t: graph): graph =
  let tt = SvSet.fold (add_bindex false c pre) posts t in
  if san then
    graph_sanity_check (F.asprintf "add_bindexes,after<%s>" c) tt;
  tt
(* removal of a back index,
 * with a check function, which may inhibit the removal,
 * used for points-to edges modification that may not kill
 * a back index due to multiple occurrences *)
let rem_bindex_chk (f: sv -> bool)
    (san: bool) (* sanity check after ? *)
    (c: string) (* context *)
    (pre: sv)  (post: sv) (t: graph): graph =
  if not (f post) then
    let pnode =
      try SvMap.find post t.g_g
      with Not_found -> Log.fatal_exn "rem_bindex, nf: %a" sv_fpr post in
    if !Flags.flag_dbg_back_index then
      Log.force "rem_bindex<%s>: %a->%a" c sv_fpr pre sv_fpr post;
    let npnode = { pnode with
                   n_preds = SvSet.remove pre pnode.n_preds } in
    let tt = { t with g_g = SvMap.add post npnode t.g_g } in
    if san then
      graph_sanity_check (Printf.sprintf "rem_bindex_chk[%s],after" c) tt;
    tt
  else t
let rem_bindex = rem_bindex_chk (fun _ -> false)
let rem_bindexes san c pre posts t =
  let tt = SvSet.fold (rem_bindex false c pre) posts t in
  if san then
    graph_sanity_check (Printf.sprintf "rem_bindexes[%s],after" c) tt;
  tt
let rem_bindexes_chk f san c pre posts t =
  let tt = SvSet.fold (rem_bindex_chk f false c pre) posts t in
  if san then
    graph_sanity_check (Printf.sprintf "rem_bindexes_chk[%s],after" c) tt;
  tt


(** Number of edges (serves as a weak emptiness test) *)
let num_edges (t: graph): int =
  SvMap.fold
    (fun _ n acc ->
      match n.n_e with
      | Hemp -> acc
      | Hpt _ | Hind _ | Hseg _ -> acc + 1
    ) t.g_g 0


(** Management of SVs *)

(* Node membership *)
let node_mem (id: sv) (t: graph): bool = SvMap.mem id t.g_g
(* Node accessor *)
let node_find (id: sv) (t: graph): node =
  try SvMap.find id t.g_g
  with Not_found ->
    Log.fatal_exn "node_find: node %a not found:\n%a" sv_fpr id
      (graph_fpri "  ") t
(* Kind of the memory region attached to an SV *)
let sv_kind (i: sv) (t: graph): region_kind =
  match (node_find i t).n_e with
  | Hemp   -> Kemp
  | Hpt _  -> Kpt
  | Hind _ -> Kind
  | Hseg _ -> Kseg

(* Addition of a new node with known id (crashes if already exists) *)
let sv_add ?(attr: node_attribute = Attr_none) ?(root: bool = false)
    ?(pt_prio: bool = false) (id: sv)
    (nt: ntyp) (na: nalloc) (t: graph): graph =
  assert (not (SvMap.mem id t.g_g));
  assert (not (PSet.mem id t.g_svemod.svm_rem));
  let node_empty = { n_i     = id ;
                     n_t     = nt ;
                     n_e     = Hemp ;
                     n_alloc = na ;
                     n_preds = SvSet.empty ;
                     n_ptprio = pt_prio ;
                     n_attr  = attr } in
  let svm = { t.g_svemod with svm_add = PMap.add id nt t.g_svemod.svm_add } in
  (* adds a fresh node with known id *)
  { t with
    g_nkey   = SvGen.use_key t.g_nkey id ;
    g_g      = SvMap.add id node_empty t.g_g ;
    g_svemod = svm;
    g_roots  = if root then SvSet.add id t.g_roots else t.g_roots }
(* Addition of a new, fresh node *)
let sv_add_fresh ?(attr: node_attribute = Attr_none) ?(root: bool = false)
    ?(pt_prio: bool = false) (nt: ntyp) (na: nalloc) (t: graph): sv * graph =
  let _, i = SvGen.gen_key t.g_nkey in
  i, sv_add ~attr:attr ~root:root ~pt_prio:pt_prio i nt na t
(* Releasing a root *)
let sv_unroot (id: sv) (t: graph): graph =
  if not (SvSet.mem id t.g_roots) then
    Log.fatal_exn "sv_unroot not called on root";
  { t with g_roots = SvSet.remove id t.g_roots }
(* Removal of a node *)
let sv_rem (id: sv) (t: graph): graph =
  if SvSet.mem id t.g_roots then Log.fatal_exn "sv_rem called on root node";
  let gadd, grem =
    if PMap.mem id t.g_svemod.svm_add then
      begin
        (* this may happen in the presence of inductive parameters *)
        Log.warn "removing node that was just added";
        PMap.remove id t.g_svemod.svm_add, t.g_svemod.svm_rem
      end
    else t.g_svemod.svm_add, PSet.add id t.g_svemod.svm_rem in
  if not (SvMap.mem id t.g_g) then
    Log.fatal_exn "sv_rem: node %a does not exist" sv_fpr id;
  let posts = node_reach (SvMap.find id t.g_g) t in
  let g_colvbindex = SvMap.map (SvSet.filter ((<>) id)) t.g_colvbindex in
  let t =
    rem_bindexes false "sv_rem" id posts
      { t with
        g_g      = SvMap.remove id t.g_g ;
        g_colvbindex ;
        g_svemod = { t.g_svemod with
                     svm_add = gadd;
                     svm_rem = grem } } in
  { t with g_nkey = SvGen.release_key t.g_nkey id }

(** Operations on COLV kinds *)
let setv_type (g: graph) (setv: sv): set_par_type option =
  try
    match SvMap.find setv g.g_colvkind with
    | Ct_set k -> k
    | Ct_seq _ -> Log.fatal_exn "setv_type"
  with Not_found -> None
let col_par_type_to_kind: col_par_type -> col_kind = function
  | Ct_set _ -> CK_set
  | Ct_seq _ -> CK_seq
let col_kind_to_par_type: col_kind -> col_par_type = function
  | CK_set -> Ct_set None
  | CK_seq -> Ct_seq None
let col_kinds_of_ind_args_acc (ia: ind_args) (acc: col_kinds): col_kinds =
  List.fold_left (fun acc i -> SvMap.add i CK_seq acc)
    (List.fold_left (fun acc i -> SvMap.add i CK_set acc) acc ia.ia_set)
    ia.ia_seq

let col_kinds_of_seg_args_acc (ia: ind_args_seg) (acc: col_kinds): col_kinds =
  List.fold_left (fun acc (i,j) -> SvMap.add j CK_seq (SvMap.add i CK_seq acc))
    (List.fold_left (fun acc i -> SvMap.add i CK_set acc) acc ia.ias_set)
    ia.ias_seq

let col_kinds_of_ind_edge_acc (ie: ind_edge) (acc: col_kinds): col_kinds =
  col_kinds_of_ind_args_acc ie.ie_args acc
let col_kinds_of_seg_edge_acc (se: seg_edge) (acc: col_kinds): col_kinds =
  col_kinds_of_ind_args_acc se.se_sargs
    (col_kinds_of_ind_args_acc se.se_dargs
       (col_kinds_of_seg_args_acc se.se_eargs acc))
let col_kinds_diff (ck0: col_kinds) (ck1: col_kinds): col_kinds =
  SvMap.fold (fun i _ -> SvMap.remove i) ck1 ck0

(** Management of COLVs *)
let info_add_fresh (root: bool) (k: col_kind) (x: graph): colv_info * graph =
  let min,  x = sv_add_fresh ~root Ntint Nnone x in
  let size, x = sv_add_fresh ~root Ntint Nnone x in
  let max,  x = sv_add_fresh ~root Ntint Nnone x in
  {min; max; size}, x
let is_info (sv: sv) (g: graph) : bool =
  SvMap.exists
    (fun _ {min; max; size} -> sv = min || sv = max || sv = size)
    g.g_colvinfo

let colv_add_fresh ?(root: bool = false) (k: col_par_type) (x: graph)
    : sv * graph =
  let kg, colv = SvGen.gen_key x.g_colvkey in
  let x =
    if !Flags.flag_colv_info then
      let info, x = info_add_fresh root (col_par_type_to_kind k) x in
      { x with g_colvinfo =  SvMap.add colv info x.g_colvinfo }
    else x in
  let roots = if root then SvSet.add colv x.g_colvroots else x.g_colvroots in
  let x = { x with
            g_colvkey   = kg;
            g_colvroots = roots;
            g_colvbindex = SvMap.add colv SvSet.empty x.g_colvbindex;
            g_colvkind  = SvMap.add colv k x.g_colvkind } in
  colv, x

let colv_add ?(root: bool = false) (colv: sv)
    ?(info: colv_info option = None) (ck: col_kind) (x: graph): graph =
  if Flags.flag_dbg_graph_basic then
    Log.info "colv_add %a (%s root)" (colv_fpr ck) colv
      (if root then "" else "non");
  let x =
    match info with
    | None when not !Flags.flag_colv_info -> x
    | Some _ when not !Flags.flag_colv_info -> assert false
    | None ->
        let info, x = info_add_fresh root ck x in
        { x with g_colvinfo =  SvMap.add colv info x.g_colvinfo }
    | Some ({min; max; size} as info) ->
        let x = x
          |> sv_add ~root min Ntint Nnone
          |> sv_add ~root max Ntint Nnone
          |> sv_add ~root size Ntint Nnone in
        { x with g_colvinfo =  SvMap.add colv info x.g_colvinfo } in
  let roots = if root then SvSet.add colv x.g_colvroots else x.g_colvroots in
  { x with
    g_colvkey   = SvGen.use_key x.g_colvkey colv;
    g_colvroots = roots;
    g_colvbindex = SvMap.add colv SvSet.empty x.g_colvbindex;
    g_colvkind  = SvMap.add colv (col_kind_to_par_type ck) x.g_colvkind }
let colv_rem (colv: sv) (x: graph): graph =
  let str =
    match SvMap.find colv x.g_colvkind with
    | Ct_set _ -> "S"
    | Ct_seq _ -> "Q" in
  if Flags.flag_dbg_graph_basic then
    Log.debug "Removing colv %s[%a]" str sv_fpr colv;
  let x =
    match SvMap.find_opt colv x.g_colvinfo with
    | None -> x
    | Some {max; min; size} ->
        let x =
          if SvSet.mem colv x.g_colvroots then
            x |> sv_unroot max |> sv_unroot min |> sv_unroot size
          else x in
        try x |> sv_rem max |> sv_rem min |> sv_rem size
        with Failure _ -> x in (* because gc might have collect the nodes *)
  let g_colvkey = SvGen.release_key x.g_colvkey colv in
  let g_colvroots = SvSet.remove colv x.g_colvroots in
  let g_colvkind = SvMap.remove colv x.g_colvkind in
  let g_colvinfo = SvMap.remove colv x.g_colvinfo in
  let g_colvbindex = SvMap.remove colv x.g_colvbindex in
  { x with g_colvkey; g_colvroots; g_colvkind; g_colvinfo; g_colvbindex }
(* roots of set/seq type *)
let col_par_type_to_kind = function
  | Ct_set _ -> CK_set
  | Ct_seq _ -> CK_seq
let colv_all (k: col_kind) (x: graph): SvSet.t =
  let equivk i = col_par_type_to_kind i = k in
  SvMap.fold (fun sv i a -> if equivk i then SvSet.add sv a else a)
    x.g_colvkind SvSet.empty
let colv_get_all (x: graph): col_kinds =
  SvMap.map col_par_type_to_kind x.g_colvkind
let setv_all: graph -> SvSet.t = colv_all CK_set
let seqv_all: graph -> SvSet.t = colv_all CK_seq
let setv_roots (x: graph): SvSet.t = SvSet.inter x.g_colvroots (setv_all x)
let seqv_roots (x: graph): SvSet.t = SvSet.inter x.g_colvroots (seqv_all x)
let colv_get_roots (x: graph): col_kinds =
  SvMap.filter (fun cv _ -> SvSet.mem cv x.g_colvroots) (colv_get_all x)
let colv_is_root (x: graph) (colv: sv): bool =
  SvSet.mem colv x.g_colvroots
(** [find_info colv x] finds ths soored information for [colv] in [x]
    - return [None] iff {!Flags.flag_colv_info} is set to [false] *)
let find_info (colv: sv) (x: graph): colv_info option =
  assert (SvMap.mem colv x.g_colvkind);
  let info = SvMap.find_opt colv x.g_colvinfo in
  if Option.is_some info then assert (!Flags.flag_colv_info);
  info

(** [gen_fresh_inst non_inst inst ck t] adds fresh collection variables in [t]
  corresponding to the ones in [non_inst].
  It returns [sv_map, t, new_colv], where [sv_map] maps from [non_inst] to
  [new_colv].
  The [inst] argument is for sanity check.*)
let gen_fresh_inst
  (non_inst: SvSet.t)
  (inst: 'a SvMap.t)
  (ck: col_kind)
  (t: graph)
  : sv SvMap.t * graph * SvSet.t =
  let col_par_type = col_kind_to_par_type ck in
  SvSet.fold
    (fun e (def_map, t, set) ->
      assert (not (SvMap.mem e inst));
      let key, t = colv_add_fresh col_par_type t in
      SvMap.add e key def_map, t, SvSet.add key set
    ) non_inst (SvMap.empty, t, SvSet.empty)

(* generate the guard constraint that an inductive edge being empty *)
let cons_emp_ind (n: sv) (ie: ind_edge): n_cons option =
  (* functions that return a node *)
  let fetch_ptr_par (i: int): sv =
    try List.nth ie.ie_args.ia_ptr i
    with Failure _ ->
      Log.fatal_exn "emp_ind: ptr par out of range" in
  let fetch_int_par (i: int): sv =
    try List.nth ie.ie_args.ia_int i
    with Failure _ ->
      Log.fatal_exn "emp_ind: int par out of range" in
  let map_formal_arith_arg: formal_arith_arg -> sv = function
    | Fa_this      -> n
    | Fa_var_new i -> Log.fatal_exn "emp_ind: unexpected new var"
    | Fa_par_int i -> fetch_int_par i
    | Fa_par_ptr i -> fetch_ptr_par i in
  (* compute predicates *)
  let rec map_aexpr (ae: aexpr) : n_expr =
    match ae with
    | Ae_cst i -> Ne_csti i
    | Ae_var fa -> Ne_var (map_formal_arith_arg fa)
    | Ae_colvinfo (ik, x) -> Log.fatal_exn "emp_ind: unexpected colv info"
    | Ae_plus (_, _) -> Log.fatal_exn "emp_ind: unexpected expression" in
  match Ind_utils.emp_rule_cons ie.ie_ind with
  | None -> None
  | Some af ->
      match af with
      | Af_equal (Ae_var Fa_this, re) ->
          Some (Nc_cons (Apron.Tcons1.EQ, Ne_var n, map_aexpr re))
      | _ -> None


(** Generation of set parameters to build an inductive predicate *)
let build_set_args (nset: int) (g: graph): graph * sv list =
  let rec f i g sargs =
    if i = 0 then g, sargs
    else
      let arg, g = colv_add_fresh (Ct_set None) g in
      f (i - 1) g (arg :: sargs) in
  f nset g []


(** Computations on int_wk_typ; to replace with int_par_rec type *)
(* merge two integer weaken type *)
let merge_int_par_type (iptl: int_par_rec) (iptr: int_par_rec): int_par_rec =
  if iptl = iptr then iptl
  else { ipt_const = iptl.ipt_const && iptr.ipt_const ;
         ipt_incr  = iptl.ipt_incr  && iptr.ipt_incr  ;
         ipt_decr  = iptl.ipt_decr  && iptr.ipt_decr  ;
         ipt_add   = iptl.ipt_add   && iptr.ipt_add }

(* convert types from inductive edge to segment end point *)
let seg_end_int_par_type (int_typ: int_par_rec IntMap.t): int_par_rec IntMap.t =
  let f_ipt ipt =
    { ipt with
      ipt_incr = ipt.ipt_decr ;
      ipt_decr = ipt.ipt_incr } in
  IntMap.map f_ipt int_typ

(* type a list of integer parameters *)
let int_par_type_iargs_e (ie: ind_args) (int_typ: int_par_rec IntMap.t)
    (acc: int_par_rec SvMap.t): int_par_rec SvMap.t =
  let do_i_type (n: sv) (acc: int_par_rec SvMap.t) (wk: int_par_rec)
      : int_par_rec SvMap.t =
    try SvMap.add n (merge_int_par_type (SvMap.find n acc) wk) acc
    with Not_found -> SvMap.add n wk acc in
  let _, acc =
    List.fold_left
      (fun (index, acc) n ->
        let ipt = IntMap.find index int_typ in
        index+1, do_i_type n acc ipt
      ) (0, acc) ie.ia_int in
  acc

(* compute weaken type for integer parameters in a graph *)
let int_par_type_iargs_g (g: graph) (f_rel: sv -> sv)
    : int_par_rec SvMap.t =
  SvMap.fold
    (fun key node acc ->
      match node.n_e with
      | Hemp
      | Hpt _ -> acc
      | Hind ie ->
          let ipt = ie.ie_ind.i_pkind.pr_int in
          int_par_type_iargs_e ie.ie_args ipt acc
      | Hseg se ->
          let sal = f_rel key in
          let dal = f_rel se.se_dnode in
          let sint_wk_typ = se.se_ind.i_pkind.pr_int in
          let dint_wk_typ = seg_end_int_par_type sint_wk_typ in
          (* eargs not to be considered as this function is only about
	   * integer parameters *)
          hseg_sanity_check ~isok:4 "graph_utils,int_par_type_iargs_g" se;
          if sal = dal then
            int_par_type_iargs_e se.se_sargs sint_wk_typ acc
          else
            int_par_type_iargs_e se.se_dargs dint_wk_typ
              (int_par_type_iargs_e se.se_sargs sint_wk_typ acc)
    ) g.g_g SvMap.empty

(* choose the integer parameters weaken type *)
let ind_seg_edge_int_par_typ (is: sv) (ie: ind_edge) (sat: n_cons -> bool)
    : pars_rec =
  match cons_emp_ind is ie with
  | None -> ie.ie_ind.i_pkind
  | Some ctr ->
      if sat ctr then ie.ie_ind.i_pkind_emp
      else ie.ie_ind.i_pkind


(** Management of SVs *)
(* Checks whether a node is the root of an inductive *)
let node_is_ind (n: sv) (g: graph): bool =
  match (node_find n g).n_e with
  | Hemp | Hpt _ -> false
  | Hind _ | Hseg _ -> true
(* get name of ind. def. attached to that node if any, None else *)
let ind_of_node (n: node): string option =
  match n.n_attr with
  | Attr_ind ind_name -> Some ind_name
  | Attr_array _ | Attr_none -> None
(* Checks whether a node has points-to edges *)
let node_is_pt (n: sv) (g: graph): bool =
  match (node_find n g).n_e with
  | Hpt m -> not (Block_frag.is_empty m)
  | Hemp | Hind _ | Hseg _ -> false
(* Checks whether a node is placed in memory *)
let node_is_placed (n: node): bool =
  match n.n_alloc with
  | Nheap | Nstack | Nstatic | Nsubmem -> true
  | Nnone -> false
(* Asserts the node must be placed; crashes otherwise *)
let node_assert_placed (ctx: string) (n: node): unit =
  if not !Flags.flag_ignore_n_alloc && not (node_is_placed n) then
    Log.fatal_exn "%s: no node.n_alloc |%a|" ctx sv_fpr n.n_i
(* Assume a node is placed
 * (when its value is discovered to be a valid address; used for sub-mem) *)
let node_assume_placed (i: sv) (g: graph): graph =
  let n = node_find i g in
  let n = { n with
            n_alloc  = Nheap ;
            n_t      = Ntaddr } in
  { g with g_g = SvMap.add i n g.g_g }


(* Returns the set of all nodes *)
let get_all_nodes (g: graph): SvSet.t =
  SvMap.fold (fun i _ acc -> SvSet.add i acc) g.g_g SvSet.empty
(* Returns the successors of a node as an explicit points-to edge source *)
let get_pt_successors_of_node (n: sv) (g: graph): SvSet.t option =
  match (node_find n g).n_e with
  | Hpt m ->
      let nds =
        Block_frag.fold_base
          (fun _ p acc -> SvSet.add (fst p.pe_dest) acc) m SvSet.empty in
      Some nds
  | Hemp -> Some SvSet.empty
  | Hind _ | Hseg _ -> None
(* Returns the predecessors of a node *)
let get_predecessors_of_node (n: sv) (g: graph): SvSet.t =
  (node_find n g).n_preds
(* Collect offsets from a base address *)
let collect_offsets (pre: sv) (g: graph) (acc: Offs.OffSet.t): Offs.OffSet.t =
  match (node_find pre g).n_e with
  | Hpt mc ->
      Block_frag.fold_base
        (fun _ pe acc -> Offs.OffSet.add (snd pe.pe_dest) acc) mc acc
  | _ -> acc

(** Points-to edges operations *)
(* Existence of a points-to edge *)
let pt_edge_mem ((is,os): Offs.svo) (t: graph): bool =
  let nsrc = node_find is t in
  match nsrc.n_e with
  | Hemp | Hind _ | Hseg _ -> false
  | Hpt mc -> Block_frag.mem (Bounds.of_offs os) mc
(* Creation of a block points to edge *)
let pt_edge_block_create (is: sv) (pe: pt_edge) (t: graph): graph =
  let nsrc = node_find is t in
  node_assert_placed "pt_edge_block_create" nsrc;
  let block =
    match nsrc.n_e with
    | Hemp ->
        let bnd_lo = Bounds.zero
        and bnd_hi = Bounds.add_size Bounds.zero pe.pe_size in
        Block_frag.create_block_span bnd_lo bnd_hi pe
    | _ -> Log.fatal_exn "pt_edge_block_create: improper edge" in
  let nsrc = { nsrc with n_e = Hpt block } in
  (* Fixing the back indexes *)
  let btargets = pt_edge_reach pe in
  add_bindexes true "pt_edge_block_create" is btargets
    { t with g_g = SvMap.add is nsrc t.g_g }
(* Appends a field at the end of a block *)
let pt_edge_block_append
    ?(nochk: bool = false) (* de-activates check that bounds coincide (join) *)
    ((is, bnd): bnode) (pe: pt_edge) (t: graph): graph =
  let nsrc = node_find is t in
  node_assert_placed "pt_edge_block_append" nsrc;
  let oblock =
    match nsrc.n_e with
    | Hemp -> Block_frag.create_empty bnd
    | Hpt block -> block
    | _ -> Log.fatal_exn "pt_edge_block_append: improper edge" in
  let nblock =
    Block_frag.append_tail ~nochk: nochk bnd
      (Bounds.add_size bnd pe.pe_size) pe oblock in
  let nsrc = { nsrc with n_e = Hpt nblock } in
  (* Fixing the back indexes *)
  let btargets = Bounds.t_sym_var_ids_add (pt_edge_reach pe) bnd in
  add_bindexes true "pt_edge_block_append" is btargets
    { t with g_g = SvMap.add is nsrc t.g_g }

(* Removal of a bunch of points-to edges from a node *)
let pt_edge_block_destroy (is: sv) (t: graph): graph =
  let n = node_find is t in
  match n.n_e with
  | Hpt _ ->
      let nn = { n with n_e = Hemp } in
      let posts = node_reach n t in
      rem_bindexes false "pt_edge_block_destroy" is posts
        { t with g_g = SvMap.add is nn t.g_g }
   | _ -> Log.fatal_exn "pt_edge_block_destroy: not a points-to edge"

(* Finding a pt_edge, for internal use (no size used, so cannot be used to
 *  dereference an edge) *)
let pt_edge_find
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (t: graph): pt_edge =
  let nsrc = node_find is t in
  let bnd = Bounds.of_offs os in
  if nsrc.n_t != Ntaddr then
    Log.warn "Issue in pt_edge_find; type of src is %a\n" ntyp_fpr nsrc.n_t;
  match nsrc.n_e with
  | Hemp | Hind _ | Hseg _ -> Log.fatal_exn "pt_edge_find: does not exist (1)"
  | Hpt mc ->
      try Block_frag.find_addr_sat sat bnd mc
      with Not_found -> Log.fatal_exn "pt_edge_find: does not exist (2)"

(* Try to decide if an offset range is in a single points-to edge
 *  of a fragmentation, and if so, return its destination *)
let pt_edge_find_interval
    (sat: n_cons -> bool)
    (is: sv) (* node representing the base address *)
    (min_off: Offs.t) (* minimum offset of the range being looked for *)
    (size: int)       (* size of the range being looked for *)
    (t: graph): pt_edge option =
  match (node_find is t).n_e with
  | Hemp | Hind _ | Hseg _ -> None
  | Hpt mc ->
      let bnd_low = Bounds.of_offs min_off in
      let bnd_hi  = Bounds.of_offs (Offs.add_int min_off size) in
      try Some (Block_frag.find_chunk_sat sat bnd_low bnd_hi mc)
      with Not_found -> None

(* Splitting of a points-to edge *)
exception Retry of graph
let pt_edge_split
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): bnode) (mid_point: Offs.size) (t: graph): graph =
  if Flags.flag_dbg_graph_basic then
    Log.debug "call to pt_edge_split\n - %a\n - %a\n"
      Bounds.t_fpr os Offs.size_fpr mid_point;
  let old_pt = pt_edge_find sat (is, Bounds.to_offs os) t in
  assert (Offs.size_leq sat mid_point old_pt.pe_size);
  let sv_old_dest = fst old_pt.pe_dest in
  let old_src = node_find sv_old_dest t in
  let sz0 = mid_point
  and sz1 = Offs.size_sub_size old_pt.pe_size mid_point in
  let typ0, typ1 =
    match old_src.n_t with
    | Ntraw | Ntint -> Log.warn "imprecise split"; Ntraw, Ntraw
    | Ntaddr -> Ntraw, Ntraw
    | Ntset -> Log.fatal_exn "cannot split a set node"
    | Ntseq -> Log.fatal_exn "cannot split a seq node" in
  let n0, t = sv_add_fresh typ0 old_src.n_alloc t in
  let n1, t = sv_add_fresh typ1 old_src.n_alloc t in
  let pe_0 = { pe_size = sz0 ; pe_dest = n0, Offs.zero }
  and pe_1 = { pe_size = sz1 ; pe_dest = n1, Offs.zero } in
  let nsrc = node_find is t in
  node_assert_placed "pt_edge_split" nsrc;
  let bndmid = Bounds.add_size os mid_point in
  let bndhi = Bounds.add_size os old_pt.pe_size in
  if Flags.flag_dbg_graph_basic then
    Log.debug "PT_SPLIT:\n - pos %a\n - siz %a\n - res %a\n"
      Bounds.t_fpr os Offs.size_fpr old_pt.pe_size Bounds.t_fpr bndhi;
  let nblock =
    let oblock =
      match nsrc.n_e with
      | Hemp -> Block_frag.create_empty os
      | Hpt block -> block
      | _ -> Log.fatal_exn "pt_edge_split: improper edge" in
    Block_frag.split_sat sat os bndmid bndhi pe_0 pe_1 oblock in
  let nsrc = { nsrc with n_e = Hpt nblock } in
  let f_post_check (i: sv): bool =
    Block_frag.fold_base
      (fun _ pe acc -> acc || fst pe.pe_dest = i) nblock false in
  (* Fixing of the back indexes *)
  let btargets =
    SvSet.add n0 (Bounds.t_sym_var_ids_add (SvSet.singleton n1) bndmid) in
  add_bindexes true "pt_edge_split" is btargets
    (rem_bindex_chk f_post_check false "pt_edge_split" is
       sv_old_dest { t with g_g = SvMap.add is nsrc t.g_g })

(* Experimental algorithm for edge search *)
let pt_edge_search
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    (mc: pt_edge Block_frag.t) (bnd: Bounds.t) (sz: Offs.size)
    : (Bounds.t * Offs.size) option =
  Block_frag.search_sat sat (fun pe -> pe.pe_size) mc bnd sz

(* Retrieval algorithm that encapsulates the search for extract and replace *)
let pt_edge_retrieve
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,bnd): bnode) (mc: pt_edge Block_frag.t) (old_sz: Offs.size)
    (t: graph): pt_edge =
  try Block_frag.find_addr_sat sat bnd mc
  with
  | Not_found ->
      (* Search for an edge containing the one being searched for *)
      match pt_edge_search sat mc bnd old_sz with
      | Some (fo, fsz) ->
          let nt = pt_edge_split sat (is,fo) fsz t in
          raise (Retry nt)
      | None ->
          raise (No_edge (F.asprintf "nf in pt_edge_retrieve (off): %a"
                            Bounds.t_fpr bnd))

(* Retrieval algorithm that encapsulates the search for extract and replace *)
let pt_edge_localize_in_graph
    (sat: n_cons -> bool)
    ((i,o): Offs.svo) (size: int) (t: graph): pt_edge option =
  let n = node_find i t in
  match n.n_e with
  | Hpt mc ->
      begin
        let b_min = Bounds.of_offs o in
        let b_max = Bounds.add_int b_min size in
        Log.info "\tfound node, searching bound[%a] %a -> %a\n"
          sv_fpr i Bounds.t_fpr b_min Bounds.t_fpr b_max;
        try Some (Block_frag.find_chunk_sat sat b_min b_max mc)
        with Not_found -> None
      end
  | _ -> None

(* Treating the case of Hemp in pt_edge_replace and pt_edge_extract
 * by searching for an opportunity for backward unfolding *)
let pt_edge_localize_in_empty (n: node) (is: sv) (t: graph) =
  (* experimental code to see whether there is an opportunity for
   * backward unfolding:
   *  - check the prevs of "is"
   *  - see if there is a segment where "is" is a back parameter
   *)
  (* HS, possible improvement:
   *   extend the search to other nodes, that are equal to another node,
   *   that would support backward unfolding
   *   (most likely at the call point of this function) *)
  let candidates =
    SvSet.fold
      (fun i acc ->
        let ni = node_find i t in
        match ni.n_e with
        | Hemp | Hpt _ | Hind _ -> acc
        | Hseg se ->
            let _, acc =
              List.fold_left
                (fun (k, acc) a ->
                  let b_par = IntSet.mem k se.se_ind.i_pr_pars in
                  if a = is && b_par then
                    k + 1, SvSet.add i acc
                  else k + 1, acc
                ) (0, acc) se.se_dargs.ia_ptr in
            acc
      ) n.n_preds SvSet.empty in
  let card = SvSet.cardinal candidates in
  if card = 1 then
    let candidate = SvSet.min_elt candidates in
    raise (Unfold_request (Uloc_main candidate, Udir_bwd))
  else
    let msg = if card = 0 then "no edge" else F.asprintf "%d elts" card in
    Log.info "pt_edge_localize_in_empty (%a) [%s]:\n%a"
      nsv_fpr is msg (graph_fpri "  ") t;
    raise (No_edge "pt_edge_localize_in_empty failed")

(* Extend the search to other nodes, that are equal to another node *)
let pt_edge_localize_in_equal (sat: n_cons -> bool)
    ((is, _): Offs.svo) (t: graph): SvPrSet.t =
  SvMap.fold
    (fun key knode acc ->
      match knode.n_e with
      | Hpt _ ->
          if sat (Nc_cons (Apron.Tcons1.EQ, Ne_var key, Ne_var is)) then
            SvPrSet.add (key, is) acc
          else acc
      | Hind _ ->
          (* XR: TODO; this case crashes bigrt 9001c
           *     but is useful in the veriamos tests
           * we need to understand the dynamics of this, and possibly
           *   - amend it or
           *   - fold it into the case above *)
          Log.info "CAREFULCASE %a, %a => %b\n" sv_fpr key sv_fpr is
            (sat (Nc_cons (Apron.Tcons1.EQ, Ne_var key, Ne_var is)));
          if sat (Nc_cons (Apron.Tcons1.EQ, Ne_var key, Ne_var is)) then
            SvPrSet.add (key, is) acc
          else acc
      | _ -> acc
    ) t.g_g SvPrSet.empty

(* Replacement of a points-to edge by another *)
let pt_edge_replace
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (pte: pt_edge): graph -> graph =
  let bnd = Bounds.of_offs os in
  let rec aux (t: graph): graph =
    let n = node_find is t in
    if !flag_dbg_graph_blocks || !flag_dbg_trigger_unfold then
      Log.info "Call to pt_edge_replace.aux:\n before: %a after:    %a\n"
        (heap_frag_fpri "  ") (is, n.n_e) pt_edge_fpr ((is, bnd), pte);
    match n.n_e with
    | Hemp ->
        (* Look for an opportunity to do backward unfolding *)
        pt_edge_localize_in_empty n is t
    | Hpt mc ->
        begin
          try
            let old_pe = pt_edge_retrieve sat (is,bnd) mc pte.pe_size t in
            if !flag_dbg_graph_blocks then
              Log.info "Comparing sizes:\n - %a\n - %a\n"
                Offs.size_fpr pte.pe_size Offs.size_fpr old_pe.pe_size;
            match Offs.size_order sat pte.pe_size old_pe.pe_size with
            | Some c ->
                if c = 0 then (* pte.pe_size = old_pe.pe_size *)
                  (* the edges found matches exactly the region to overwrite *)
                  let nmc = Block_frag.replace_sat sat bnd pte mc in
                  let nn = { n with n_e = Hpt nmc } in
                  let f_post_check (i: sv): bool =
                    Block_frag.fold_base
                      (fun _ pe acc -> acc || fst pe.pe_dest = i) nmc false in
                  let tt =
                    add_bindexes true "pt_edge_replace" is (pt_edge_reach pte)
                      (rem_bindexes_chk f_post_check false "pt_edge_replace"
                         is (pt_edge_reach old_pe)
                         { t with g_g = SvMap.add is nn t.g_g })
                  in
                  graph_sanity_check "pt_edge_replace" tt;
                  tt
                else if c < 0 then (* pte.pe_size < old_pe.pe_size then*)
                  (* only part of the edge needs be overwritten;
                   * we should split it *)
                  aux (pt_edge_split sat (is,bnd) pte.pe_size t)
                else (* c > 0, i.e., pte.pe_size > old_pe.pe_size *)
                  (* we would have to merging edges together... *)
                  Log.fatal_exn "pt_edge_replace: edges merging unsupported"
            | None ->
                if Offs.size_leq sat pte.pe_size old_pe.pe_size then
                  aux (pt_edge_split sat (is,bnd) pte.pe_size t)
                else Log.fatal_exn "incomparable sizes"
          with
          | Retry nt -> aux nt
        end
    | Hind _ ->
        if !flag_dbg_trigger_unfold then
          Log.info "pt_edge_replace: inductive found at %a, to unfold!\n"
            nsv_fpr is;
        raise (Unfold_request (Uloc_main is, Udir_fwd))
    | Hseg _ ->
        if !flag_dbg_trigger_unfold then
          Log.info "pt_edge_replace: segment found at %a, to unfold!\n"
            nsv_fpr is;
        raise (Unfold_request (Uloc_main is, Udir_fwd)) in
  aux

(* Extracts a points-to edge: reads, after split if necessary *)
let pt_edge_extract
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (isz: int): graph -> graph * pt_edge =
  let bnd = Bounds.of_offs os in
  let sz = Offs.size_of_int isz in
  let rec aux (t: graph): graph * pt_edge =
    let n = node_find is t in
    if !flag_dbg_graph_blocks then
      Log.force "Call to pt_edge_extract.aux:  (%a,%a)\n"
        sv_fpr is Offs.t_fpr os;
    let pt_extract mc =
      try
        let pte = pt_edge_retrieve sat (is,bnd) mc sz t in
        match Offs.size_order sat pte.pe_size sz with
        | Some c ->
            if c = 0 then (* equality, pte.pe_size = sz *)
              (* the edge found matches the size of the deref cell *)
              t, pte
            else if c > 0 then (* pte.pe_size > sz *)
              (* only part of the edge should be read; we should split *)
              aux (pt_edge_split sat (is,bnd) sz t)
            else (* c < 0, i.e., pte.pe_size < sz *)
              raise (No_edge "size mismatch")
        | None -> raise (No_edge "incomparable sizes")
      with
      | Retry nt ->
          aux nt in
    match n.n_e with
    | Hemp ->
        (* Look for an opportunity to do backward unfolding *)
        pt_edge_localize_in_empty n is t
    | Hpt mc ->
      pt_extract mc
    | Hind _ ->
        if !flag_dbg_trigger_unfold then
          Log.force "pt_edge_extract: inductive found at %a, to unfold!\n"
            nsv_fpr is;
        raise (Unfold_request (Uloc_main is, Udir_fwd))
    | Hseg _ ->
        if !flag_dbg_trigger_unfold then
          Log.force "pt_edge_extract: segment found at %a, to unfold!\n"
            nsv_fpr is;
        raise (Unfold_request (Uloc_main is, Udir_fwd)) in
  aux


(** Inductive parameters applications *)
(* Empty set of arguments *)
let ind_args_empty: ind_args =
  { ia_ptr = [ ] ;
    ia_int = [ ];
    ia_set = [ ];
    ia_seq = [ ] }
let ind_args_s_empty: ind_args_seg =
  { ias_ptr = [ ] ;
    ias_int = [ ];
    ias_set = [ ];
    ias_seq = [ ] }
(* Making lists of arguments *)
let rec ind_args_1_make (typ: ntyp) (i: int) (t: graph): sv list * graph =
  if i = 0 then [ ], t
  else
    let l0, t0 = ind_args_1_make typ (i-1) t in
    let i1, t1 = sv_add_fresh typ Nnone t0 in
    i1 :: l0, t1
(* Making lists of set arguments *)
let rec ind_sargs_make (i: int) (t: graph): sv list * graph =
  if i = 0 then [ ], t
  else
    let l0, t0 = ind_sargs_make (i-1) t in
    let i1, t1 = colv_add_fresh (Ct_set None) t0 in
    i1 :: l0, t1
(* Making lists of seq arguments *)
let rec ind_qargs_make (i: int) (t: graph): sv list * graph =
  if i = 0 then [ ], t
  else
    let l0, t0 = ind_qargs_make (i-1) t in
    let i1, t1 = colv_add_fresh (Ct_seq None) t0 in
    i1 :: l0, t1

let rec seg_qargs_make (i: int) (t: graph): (sv * sv) list * graph =
  if i = 0 then [ ], t
  else
    let l0, t0 = seg_qargs_make (i-1) t in
    let i1, t1 = colv_add_fresh (Ct_seq None) t0 in
    let i2, t2 = colv_add_fresh (Ct_seq None) t1 in
    (i1, i2) :: l0, t2

(* Auxiliary function to build fresh sets *)
let freshset_empty: freshset =
  { f_ptr = SvSet.empty ;
    f_int = SvSet.empty ;
    f_col = SvMap.empty }
let make_freshset (lp: sv list) (li: sv list) (ls: sv list) (lq: sv list)
    : freshset =
  let f = List.fold_left (fun acc sv -> SvSet.add sv acc) SvSet.empty in
  let g k l a = List.fold_left (fun a cv -> SvMap.add cv k a) a l in
  let c = g CK_set ls (g CK_seq lq SvMap.empty) in
  { f_ptr = f lp ;
    f_int = f li ;
    f_col = c }

(* Adding the elements of a freshset to a graph *)
let freshset_add (fs: freshset) (t: graph): graph =
  let t = SvSet.fold (fun i -> sv_add i Ntint Nnone) fs.f_int t in
  let t = SvSet.fold (fun i -> sv_add i Ntaddr Nnone) fs.f_ptr t in
  SvMap.fold (fun i k -> colv_add i k) fs.f_col t

(* Make a new inductive edge with fresh arg nodes
 *  - ptr arguments are given
 *  - int and collection arguments are generated fresh *)
let ind_edge_make (iname: string) ~(srcptr: sv list) (t: graph)
    : ind_edge * graph * freshset =
  let ind = Ind_utils.ind_find iname in
  let lint, g1 = ind_args_1_make Ntint ind.i_ipars t in
  let lset, g1 = ind_sargs_make ind.i_spars g1 in
  let lseq, g1 = ind_qargs_make ind.i_qpars g1 in
  let ie = { ie_ind  = ind ;
             ie_args = { ia_ptr = srcptr ;
                         ia_int = lint ;
                         ia_set = lset ;
                         ia_seq = lseq  } }
  and fs = make_freshset [] lint lset lseq in
  ie, g1, fs

(* Make a new segment edge with fresh non pointer arg nodes
 *  - ptr arguments are given
 *  - int arguments may be given
 *  - collection arguments are generated fresh *)
let seg_edge_make (iname: string)
    ~(srcptr: sv list) ~(dstptr: sv list) ?(int_par = None) (idr: sv) (t: graph)
    : seg_edge * graph * freshset =
  let ind = Ind_utils.ind_find iname in
  (* Check that all collection parameters are add or const, otherwise fail *)
  is_ind_collection_sane "graph_utils,seg_edge_make" ind;
  (* Construct the basic elements of the edge *)
  let lint, dint, intgen, g1 =
    match int_par with
    | None ->
        let lint, g1 = ind_args_1_make Ntint ind.i_ipars t in
        let dint, g1 = ind_args_1_make Ntint ind.i_ipars g1 in
        lint, dint, lint @ dint, g1
    | Some (lint, dint) -> lint, dint, [ ], t in
  let lset, g1 = ind_sargs_make ind.i_spars g1 in
  let lseq, g1 = seg_qargs_make ind.i_qpars g1 in
  let se = { se_ind   = ind ;
             se_eargs = { ind_args_s_empty with
                          ias_set = lset ;
                          ias_seq = lseq } ;
             se_sargs = { ind_args_empty with
                          ia_ptr = srcptr ;
                          ia_int = lint ; } ;
             se_dargs = { ind_args_empty with
                          ia_ptr = dstptr ;
                          ia_int = dint ; } ;
             se_dnode = idr; } in
  hseg_sanity_check ~isok:1 "graph_utils,seg_edge_make" se;
  let lseq = List.map_flatten (fun (x,y) -> [x; y]) lseq in
  let fs = make_freshset [] (lint @ dint) lset lseq in
  se, g1, fs

(** Operations common to inductive and segment edges *)
let ind_seg_pre_add_check (msg: string) (n: node): unit =
  (* checks: source node should be an address, with no memory materialized
   *  -> we want to fix these eventually *)
  if n.n_t != Ntaddr then
    Log.info "%s_add: node may not be an address" msg;
  if n.n_alloc != Nnone then
    Log.info
      "%s_add: possible double representation of allocation node %a"
      msg nsv_fpr n.n_i

(** Utilities shared for seg and ind edges *)
(* Utilities to check sane edges are being added *)
let check_valid_args_sv (g: graph) (op: string) (svkind: string)
    (l: sv list): unit =
  List.iter
    (fun sv ->
      if not (SvMap.mem sv g.g_g) then
        Log.fatal_exn "INVALID %s, %s arg %a not known" op svkind nsv_fpr sv
    ) l
let check_valid_args_colv (g: graph) (op: string) (colvkind: string)
    (l: sv list): unit =
  List.iter
    (fun colv ->
      if not (SvGen.key_is_used g.g_colvkey colv) then
        Log.fatal_exn "INVALID %s, %s arg %a not known" op colvkind nsv_fpr colv
    ) l
let check_valid_args_ind_edge (g: graph) (op: string) (ie: ind_edge): unit =
  check_valid_args_sv g op "ptr" ie.ie_args.ia_ptr;
  check_valid_args_sv g op "int" ie.ie_args.ia_int;
  check_valid_args_colv g op "set" ie.ie_args.ia_set;
  check_valid_args_colv g op "seq" ie.ie_args.ia_seq
let check_valid_args_seg_edge (g: graph) (op: string) (se: seg_edge): unit =
  check_valid_args_sv g op "ptr" se.se_sargs.ia_ptr;
  check_valid_args_sv g op "int" se.se_sargs.ia_int;
  check_valid_args_sv g op "ptr" se.se_dargs.ia_ptr;
  check_valid_args_sv g op "int" se.se_dargs.ia_int;
  check_valid_args_colv g op "set" se.se_eargs.ias_set;
  check_valid_args_colv g op "seq" (List.map fst se.se_eargs.ias_seq);
  check_valid_args_colv g op "seq" (List.map snd se.se_eargs.ias_seq)

(** Inductive edges operations *)
(* Retrieve an inductive edge *)
(* Note: this function seems unused now... *)
let ind_edge_find (is: sv) (t: graph): ind_edge =
  let nsrc =
    try SvMap.find is t.g_g
    with Not_found -> Log.fatal_exn "nf in ind_edge_find (src)" in
  match nsrc.n_e with
  | Hind ie -> ie
  | _ -> Log.fatal_exn "ind_edge_find: no ind edge!"
(* Inductive edge addition *)
let ind_edge_add (is: sv) (ie: ind_edge) (t: graph): graph =
  (* enforcing consistency with the inductive *)
  if List.length ie.ie_args.ia_ptr != ie.ie_ind.i_ppars then
    Log.fatal_exn "ind_edge_add, ptr args (%s): %d-%d" ie.ie_ind.i_name
      (List.length ie.ie_args.ia_ptr) (ie.ie_ind.i_ppars);
  if List.length ie.ie_args.ia_int != ie.ie_ind.i_ipars then
    Log.fatal_exn "ind_edge_add, int args (%s): %d-%d" ie.ie_ind.i_name
      (List.length ie.ie_args.ia_int) (ie.ie_ind.i_ipars);
  if List.length ie.ie_args.ia_set != ie.ie_ind.i_spars then
    Log.fatal_exn "ind_edge_add, set args (%s): %d-%d" ie.ie_ind.i_name
      (List.length ie.ie_args.ia_set) (ie.ie_ind.i_spars);
  let iname = ie.ie_ind.i_name in
  if not (StringSet.mem iname t.g_inds) then
    Log.fatal_exn "seg_add: inductive %s not in graph" iname;
  (* source node retrieval *)
  let nsrc =
    try SvMap.find is t.g_g
    with Not_found -> Log.fatal_exn "nf in ind_edge_add (src)" in
  ind_seg_pre_add_check "ind_edge" nsrc;
  (* checks that the parameters are already in the graph *)
  check_valid_args_ind_edge t "ind_edge_add" ie;
  (* addition of the edge *)
  match nsrc.n_e with
  | Hemp ->
      let nsrc1 =
        { nsrc with
          n_e    = Hind ie ;
          n_attr = Attr_ind ie.ie_ind.i_name } in
      let g_g = SvMap.add is nsrc1 t.g_g in
      let g_colvbindex =
        List.fold_left
          (fun acc_index colv ->
            let set = SvMap.find colv acc_index in
            let set = SvSet.add is set in
            SvMap.add colv set acc_index
          ) t.g_colvbindex (ie.ie_args.ia_set @ ie.ie_args.ia_seq) in
      let reach = ind_edge_reach ie t in
      add_bindexes true "ind_edge_add" is reach { t with g_g; g_colvbindex }
  | Hpt _ | Hind _ | Hseg _ ->
      Log.fatal_exn "ind_edge_add: there is already an edge"
let ind_edge_rem (is: sv) (t: graph): graph =
  let n = node_find is t in
  match n.n_e with
  | Hind ie ->
      (* argument nodes appear only on that inductive edge;
       * therefore, they should all be removed now *)
      let reach = ind_edge_reach ie t in
      let nn = { n with
                 n_e = Hemp } in
      let g_g = SvMap.add is nn t.g_g in
      let g_colvbindex =
        List.fold_left
          (fun acc_index colv ->
            let set = SvMap.find colv acc_index in
            let set = SvSet.remove is set in
            SvMap.add colv set acc_index)
          t.g_colvbindex
          (ie.ie_args.ia_set@ie.ie_args.ia_seq) in
      let t0 =
        rem_bindexes true "ind_edge_rem" is reach { t with g_g; g_colvbindex } in
      t0
  | _ -> Log.fatal_exn "cannot find an inductive edge to remove"
(* Function extracting *one* *inductive* edge (for is_le) *)
let ind_edge_extract_single (g: graph): bool * (sv * ind_edge) option =
  let found: (sv * ind_edge) option ref = ref None (* inductive edge found *)
  and several = ref false in
  SvMap.iter
    (fun i e ->
      match e.n_e with
      | Hemp -> ( )
      | Hpt _ | Hseg _ -> several := true
      | Hind ie ->
          assert (List.length ie.ie_args.ia_int = 0);
          match !found with
          | None   -> found := Some (i, ie)
          | Some _ -> several := true
    ) g.g_g ;
  !several, !found


(** Segment edges operations *)
(* Retrieve an inductive edge *)
let seg_edge_find (is: sv) (t: graph): seg_edge =
  let nsrc =
    try SvMap.find is t.g_g
    with Not_found -> Log.fatal_exn "nf in seg_edge_find (src)" in
  match nsrc.n_e with
  | Hseg se -> se
  | _ -> Log.fatal_exn "seg_edge_find: no segment edge!"

(* Addition of a segment edge *)
let seg_edge_add
    (is: sv)
    (se: seg_edge)
    (g: graph): graph =
  let n = node_find is g in
  ind_seg_pre_add_check "seg_edge" n;
  let iname = se.se_ind.i_name in
  if not (StringSet.mem iname g.g_inds) then
    Log.fatal_exn "seg_add: inductive %s not in graph" iname;
  (* checks that the parameters are already in the graph *)
  check_valid_args_seg_edge g "seg_edge_add" se;
  match n.n_e with
  | Hemp ->
      let nn = { n with
                 n_e = Hseg se } in
      let g_g = SvMap.add is nn g.g_g in
      let g_colvbindex =
        List.fold_left
          (fun acc_index colv ->
            let set = SvMap.find colv acc_index in
            let set = SvSet.add is set in
            SvMap.add colv set acc_index)
          g.g_colvbindex
          (se.se_eargs.ias_set @
           List.fold_left
             (fun acc (x,y) -> x :: y :: acc) [] se.se_eargs.ias_seq) in
      let reach = seg_edge_reach se g in
      add_bindexes true "seg_edge_add" is reach
        { g with g_g; g_colvbindex }
  | _ -> Log.fatal_exn "cannot add a segment over existing edge"

(* Removal of a segment edge *)
let seg_edge_rem (is: sv) (t: graph): graph =
  let n = node_find is t in
  match n.n_e with
  | Hseg se ->
      let nn = { n with n_e = Hemp } in
      let g_g = SvMap.add is nn t.g_g in
      let g_colvbindex =
        List.fold_left
          (fun acc_index colv ->
            let set = SvMap.find colv acc_index in
            let set = SvSet.add is set in
            SvMap.add colv set acc_index
          ) t.g_colvbindex
          (se.se_eargs.ias_set @
           List.fold_left
             (fun acc (x,y) -> x :: y :: acc) [] se.se_eargs.ias_seq) in
      let reach = seg_edge_reach se t in
      rem_bindexes true "seg_edge_rem" is reach
        { t with g_g; g_colvbindex }
  | _ -> Log.fatal_exn "cannot find a semgent edge to remove"
(* find sources of segments to some given node *)
let seg_edge_find_to (is: sv) (t: graph): SvSet.t =
  let n = node_find is t in
  SvSet.fold
    (fun ispred acc ->
      let npred = node_find ispred t in
      match npred.n_e with
      | Hseg se -> if se.se_dnode = is then SvSet.add ispred acc else acc
      | _ -> acc
    ) n.n_preds SvSet.empty


(** Inductives and segments *)
(* Extraction of an inductive or segment edge *)
let ind_or_seg_edge_find_rem (is: sv) (t: graph)
    : ind_edge                  (* inductive edge (at the source) *)
    * (sv * ind_args_seg * ind_args) option (* destination if segment *)
    * graph                             (* remaining graph        *) =
  let n =
    try SvMap.find is t.g_g
    with Not_found -> Log.fatal_exn "nf in ind_or_seg_edge_find_rem" in
  let t_rem = { t with g_g = SvMap.add is { n with n_e = Hemp } t.g_g } in
  let error (msg: string) =
    Log.fatal_exn "ind_or_seg_edge_find_rem: not found: %s at %a\n%a"
      msg sv_fpr is (graph_fpri "  ") t in
  match n.n_e with
  | Hemp -> error "empty"
  | Hpt _ -> error "points-to"
  | Hind ie ->
      let reach = ind_edge_reach ie t_rem in
      ie, None, rem_bindexes true "ind_or_seg_edge_find_rem" is reach t_rem
  | Hseg se ->
      hseg_sanity_check ~isok:1 "graph_utils,ind_or_seg_edge_find_rem" se;
      let reach = seg_edge_reach se t_rem in
      { ie_ind  = Ind_utils.ind_find se.se_ind.i_name ;
        ie_args = se.se_sargs },
      Some (se.se_dnode, se.se_eargs, se.se_dargs),
      rem_bindexes true "ind_or_seg_find_rem" is reach t_rem
(* Asserting that some inductives have no parameters of a certain kind
 * (useful for weakening rules that do not support parameters yet) *)
let assert_no_ptr_arg (msg: string) (a: ind_args): unit =
  if List.length a.ia_ptr != 0 then
    Log.fatal_exn "rule no ptr arg violation: %s" msg
let assert_no_int_arg (msg: string) (a: ind_args): unit =
  if List.length a.ia_int != 0 then
    Log.fatal_exn "rule no int arg violation: %s" msg
let assert_no_arg (msg: string) (a: ind_args): unit =
  assert_no_ptr_arg msg a;
  assert_no_int_arg msg a


(** Functions for all kinds of edges *)
(* Removal of all edges at a node *)
let edge_rem_any (is: sv) (t: graph): graph =
  let n = node_find is t in
  match n.n_e with
  | Hemp -> t
  | Hpt _ -> pt_edge_block_destroy is t
  | Hind _ -> ind_edge_rem is t
  | Hseg _ -> seg_edge_rem is t


(** Memory management *)
(* Allocation and free of a memory block *)
let mark_alloc (id: sv) (sz: int) (t: graph): graph =
  let n = node_find id t in
  if n.n_alloc = Nstack || n.n_alloc = Nstatic || n.n_alloc = Nheap then
    (* Means the node already considered allocated;
     *  probably due to constraints that a node is allocated remain
     *  although a segment has been folded from those nodes *)
    Log.info "node was already known to be allocated %a %a"
      sv_fpr id nalloc_fpr n.n_alloc;
  let nn = { n with n_alloc = Nheap } in
  { t with g_g = SvMap.add id nn t.g_g }
let mark_free (id: sv) (t: graph): graph =
  let n = node_find id t in
  match n.n_alloc with
  | Nheap ->
      let nn = { n with n_alloc = Nnone } in
      { t with g_g = SvMap.add id nn t.g_g }
  | _ -> Log.fatal_exn "cannot free not allocated node"


(** Tests that can be (partly) evaluated in the graphs *)
(* Equalities generated by the knowledge a segment be empty *)
let empty_segment_equalities (nbase: sv) (se: seg_edge)
    : SvPrSet.t =
  hseg_sanity_check ~isok:0 "graph_utils,empty_segment_equalities" se;
  assert (List.length se.se_sargs.ia_ptr = List.length se.se_dargs.ia_ptr);
  assert (List.length se.se_sargs.ia_int = List.length se.se_dargs.ia_int);
  let f_pair (src: sv) (dst: sv): SvPrSet.t =
    SvPrSet.singleton (src, dst) in
  let f_pairs: SvPrSet.t -> sv list -> sv list -> SvPrSet.t =
    List.fold_left2 (fun acc src dst -> SvPrSet.add (src, dst) acc) in
  f_pairs (f_pairs (f_pair nbase se.se_dnode)
             se.se_sargs.ia_ptr se.se_dargs.ia_ptr)
    se.se_sargs.ia_int se.se_dargs.ia_int
(* Reduction of a segment known to be empty *)
let red_empty_segment (nbase: sv) (g: graph)
    : graph * SvPrSet.t * set_cons list * SvSet.t =
  let se = seg_edge_find nbase g in
  let set_cons =
    List.fold_lefti
      (fun i acc setv ->
        match IntMap.find_opt i se.se_ind.i_pkind.pr_set with
        | Some k ->
            if k.st_head || k.st_add then S_eq (S_var setv, S_empty) :: acc
            else acc
        | _ -> acc
      ) [] se.se_eargs.ias_set in
  let seqv_empty = se.se_eargs.ias_seq
    |> List.fold_left (fun acc (x,y) -> x :: y :: acc) []
    |> SvSet.of_list in
  seg_edge_rem nbase g, empty_segment_equalities nbase se, set_cons, seqv_empty
(* Experimental: Reduction of empty points-to edges *)
let red_empty_points_to (sat: n_cons -> bool) (g: graph): graph =
  (* for now, we do not do anything, we just search for possible reductions
   * then:
   * - prune edge
   * - do sanity checks in order to see if there are indexing bugs *)
  SvMap.fold
    (fun sv n g ->
      match n.n_e with
      | Hpt pt ->
          let npt, l = Block_frag.clean_empty_entries sat pt in
          let nn = { n with n_e = Hpt npt } in
          List.fold_left
            (fun g pte ->
              rem_bindex false "reduction-empty-pte" sv (fst pte.pe_dest) g
            ) { g with g_g = SvMap.add sv nn g.g_g } l
      | _ -> g
    ) g.g_g g
(* Graph renaming, for the reduction *)
let graph_rename_ids (renaming: sv SvMap.t) (g: graph): graph =
  assert (g.g_svemod = Dom_utils.svenv_empty);
  SvSet.iter (fun i -> assert (not (SvMap.mem i renaming))) g.g_roots;
  let do_nid (i: sv): sv = try SvMap.find i renaming with Not_found -> i in
  let do_ind_args (ia: ind_args): ind_args =
    { ia with
      ia_ptr = List.map do_nid ia.ia_ptr;
      ia_int = List.map do_nid ia.ia_int } in
  let do_ind_args_seg (ia: ind_args_seg): ind_args_seg =
    { ia with
      ias_ptr = List.map do_nid ia.ias_ptr;
      ias_int = List.map do_nid ia.ias_int } in
  let do_pt_edge (pe: pt_edge): pt_edge =
    { pe with
      pe_dest = do_nid (fst pe.pe_dest), snd pe.pe_dest } in
  let do_heap_frag: heap_frag -> heap_frag = function
    | Hemp -> Hemp
    | Hpt m -> Hpt (Block_frag.map_bound (fun x -> x) do_pt_edge m)
    | Hind ie -> Hind { ie with
                        ie_args = do_ind_args ie.ie_args }
    | Hseg se ->
        hseg_sanity_check ~isok:0 "graph_utils,graph_rename_ids" se;
        Hseg { se with
               se_eargs = do_ind_args_seg se.se_eargs;
               se_sargs = do_ind_args se.se_sargs;
               se_dargs = do_ind_args se.se_dargs;
               se_dnode = do_nid se.se_dnode } in
  let do_preds (s: SvSet.t) =
    SvSet.fold (fun i -> SvSet.add (do_nid i)) s SvSet.empty in
  let do_node (n: node): node =
    { n with
      n_e     = do_heap_frag n.n_e;
      n_preds = do_preds n.n_preds } in
  if renaming = SvMap.empty then g
  else
    let nodes = SvMap.map do_node g.g_g in
    SvMap.fold
      (fun irem ikeep acc ->
        let nkeep = node_find ikeep acc in
        let nrem = node_find irem acc in
        let nkeep =
          { nkeep with n_preds = SvSet.union nkeep.n_preds nrem.n_preds } in
        { acc with g_g = SvMap.add ikeep nkeep acc.g_g }
      ) renaming { g with g_g = nodes }
(* Reduction: merging nodes that denote the same concrete value *)
let graph_merge_eq_nodes
    (sat: n_cons -> bool)
    (eqreds: SvPrSet.t)   (* a set of pairs of SVs found equal *)
    (g: graph): graph * sv SvMap.t =
  (* filtering eqpreds: removing diagonal elements
   * (those equalities trivially hold) *)
  let eqreds = SvPrSet.filter (fun (i0,i1) -> i0 != i1) eqreds in
  (* Optional debug *)
  if true && !flag_dbg_eqred then
    begin
      Log.info "called graph_merge_eq_nodes\n%a" (graph_fpri "  ") g;
      SvPrSet.iter
        (fun (i,j) -> Log.info " %a => %a\n" sv_fpr i sv_fpr j) eqreds
    end;
  (* We don't want colv info svs to be merged with other svs.ic_int
   * so we exclude them from the [eqreds] set *)
  let filter_info (i1, i2) =
    (not @@ is_info i1 g) && (not @@ is_info i2 g) in
  let eqreds = SvPrSet.filter filter_info eqreds in
  (* Computation of a collection of partitions of equal nodes *)
  let eqpart =
    SvPrSet.fold
      (fun (i,j) acc ->
        let f k = try SvMap.find k acc with Not_found -> SvSet.singleton k in
        let si = f i and sj = f j in
        let s = SvSet.union si sj in
        SvSet.fold (fun k -> SvMap.add k s) s acc
      ) eqreds SvMap.empty in
  (* Construction of a list of sets of equal nodes *)
  let eqparts =
    SvMap.fold
      (fun i s acc ->
        if s = SvSet.empty || i != SvSet.min_elt s then acc
        else s :: acc
      ) eqpart [ ] in
  (* Auxiliary function to determine if an inductive edge is necessarily Emp *)
  let ind_is_emp (i: sv): bool =
    let pred_fails ie (p: pformatom): bool =
      try
        let fconv (fa: formal_arith_arg): sv =
          match fa with
          | Fa_this -> i
          | Fa_var_new _ -> raise Stop
          | Fa_par_int j -> List.nth ie.ie_args.ia_int j
          | Fa_par_ptr j -> List.nth ie.ie_args.ia_ptr j in
        let target =
          match p with
          | Pf_arith (Af_noteq (Ae_var v, Ae_cst 0))
          | Pf_arith (Af_noteq (Ae_cst 0, Ae_var v)) -> (* try to show = 0 *)
              Nc_cons (Apron.Tcons1.EQ, Ne_var (fconv v), Ne_csti 0)
          | Pf_arith (Af_noteq (Ae_var v0, Ae_var v1)) -> (* try to show = *)
              Nc_cons (Apron.Tcons1.EQ, Ne_var (fconv v0), Ne_var (fconv v1))
          | _ -> raise Stop in
        sat target
      with Stop -> false in
    match (node_find i g).n_e with
    | Hind ie ->
        List.for_all
          (fun ir ->
            (* either rule is empty *)
            ir.ir_heap = [ ] ||
            (* or rule generates non empty heap; prove it does not apply *)
            List.exists (fun p -> not (pred_fails ie p)) ir.ir_pure
          ) ie.ie_ind.i_rules
    | Hseg _ -> true (* segment edges may always be empty *)
    | _ -> Log.fatal_exn "ind_is_emp called on non ind or seg" in
  (* Condition testing (locally) *)
  let is_null (i: sv): bool =
    sat (Nc_cons (Apron.Tcons1.EQ, Ne_var i, Ne_csti 0)) in
  let is_non_null (i: sv): bool =
    sat (Nc_cons (Apron.Tcons1.DISEQ, Ne_var i, Ne_csti 0)) in
  (* Collapsing one group of equal nodes *)
  let f_do_one_group (g, ren) s =
    (* In this partition, points-to, indsegs, empty edges *)
    let spt, sis_e, sis_ne, se =
      SvSet.fold
        (fun i (ap, aie, ain, ae) ->
          match (node_find i g).n_e with
          | Hemp -> ap, aie, ain, SvSet.add i ae
          | Hind _ | Hseg _ ->
              if ind_is_emp i then ap, SvSet.add i aie, ain, ae
              else ap, aie, SvSet.add i ain, ae
          | Hpt _ -> SvSet.add i ap, aie, ain, ae
        ) s (SvSet.empty, SvSet.empty, SvSet.empty, SvSet.empty) in
    (* Reduction to bottom if two inductive edges are proved non null
     * or can be simultaneously be proved null and non null *)
    let isnull, isnonnull =
      SvSet.fold
        (fun i (a, b) ->
          let nn = is_non_null i in
          (* TODO: check this case; we should also prove all rules
           * that apply are non empty to eliminate the case. *)
          if nn && b then raise Bottom;
          a || is_null i, b || nn)
        (SvSet.union sis_e sis_ne) (false, false) in
    if isnull && isnonnull then raise Bottom;
    let npt = SvSet.cardinal spt in
    let nisne = SvSet.cardinal sis_ne in
    if !flag_dbg_eqred then
      Log.info "nodesz: %d; E:%d,P:%d,I(e):%d,I(ne):%d\n" (SvSet.cardinal s)
        (SvSet.cardinal se) npt (SvSet.cardinal sis_e)
        (SvSet.cardinal sis_ne);
    if npt + nisne > 1 then
      (* Several points-to edges with equal address; cannot work *)
      Log.fatal_exn "graph_red_eq_nodes; multiple non-empty edges"
    else if npt = 1 then
      (* One points-to edge
       * -> we check all inductives are empty and rename all to ipt *)
      if sis_ne = SvSet.empty then
        let ipt = SvSet.min_elt spt in
        let ren =
          SvSet.fold (fun oi a -> SvMap.add oi ipt a)
            (SvSet.union se sis_e) ren in
        let g = SvSet.fold ind_edge_rem sis_e g in
        g, ren
      else Log.fatal_exn "COULD NOT PROVE EMPTY"
    else (* no points-to edge; for now, we do nothing, for now *)
      if nisne = 1 then
        (* one non-empty inductive; rename everything else to it *)
        let iind = SvSet.min_elt sis_ne in
        let ren =
          SvSet.fold (fun oi a -> SvMap.add oi iind a)
            (SvSet.union se sis_e) ren in
        let g = SvSet.fold ind_edge_rem sis_e g in
        g, ren
      else if sis_e = SvSet.empty then
        (* no inductive, select minimum SV among empty edges *)
        let iemp = SvSet.min_elt se in
        let ren =
          SvSet.fold (fun oi a -> SvMap.add oi iemp a)
            (SvSet.remove iemp se) ren in
        g, ren
      else
        (* some empty inductives; select minimum SV of empty inductive *)
        let iind = SvSet.min_elt sis_e in
        let sis_e = SvSet.remove iind sis_e in
        let ren =
          SvSet.fold (fun oi a -> SvMap.add oi iind a)
            (SvSet.union sis_e se) ren in
        let g = SvSet.fold ind_edge_rem sis_e g in
        g, ren in
  (* Iteration over the list of elements of the partition of equal nodes *)
  let g, ren = List.fold_left f_do_one_group (g, SvMap.empty) eqparts in
  if !flag_dbg_eqred then
    Log.debug "Renaming produced by graph_merge_eq_nodes: %a\n"
      (SvMap.t_fpr "," sv_fpr) ren;
  (* perform the renaming of all incoming edges of renamed nodes *)
  let g_renamed = graph_rename_ids ren g in
  (* rename the nodes *)
  SvMap.fold (fun irem _ -> sv_rem irem) ren g_renamed, ren
(* Whether a graph is compatible with a node being equal to zero *)
let graph_guard_node_null (v0: sv) (g: graph): guard_res =
  match (node_find v0 g).n_e with
  | Hpt _ ->
      (* v0 is an address, with fields; so _|_
       * (application of the separation principle) *)
      Gr_bot
  | Hseg se ->
      if se.se_ind.i_reds0 then
        (* we can deduce we have an empty segment at that point
         * possible improvement: we could also propagage to the segment dest *)
        Gr_emp_seg v0
      else Gr_no_info
  | Hind ie ->
      if ie.ie_ind.i_mt_rule then
        Gr_emp_ind v0
      else Gr_bot
  | _ -> Gr_no_info

(* return true only if the two pt blocks have share range *)
let pt_pt_disequal (b0: pt_edge Block_frag.t) (b1: pt_edge Block_frag.t): bool =
  let bfst_0, bend_0 = Block_frag.first_bound b0, Block_frag.end_bound b0 in
  let bfst_0, bend_0 = Offs.to_int (Bounds.to_offs bfst_0),
                       Offs.to_int (Bounds.to_offs bend_0) in
  let bfst_1, bend_1 = Block_frag.first_bound b1, Block_frag.end_bound b1 in
  let bfst_1, bend_1 = Offs.to_int (Bounds.to_offs bfst_1),
                       Offs.to_int (Bounds.to_offs bend_1) in
  (max bfst_0 bfst_1)< (min bend_0 bend_1)

(* return true only if pt block has share range with each non-empty rule
   of ind, and the empty rule of ind constraints that address = null *)
let pt_ind_disequal (ie: ind) (b: pt_edge Block_frag.t): bool =
  let bfst, bend = Block_frag.first_bound b, Block_frag.end_bound b in
  let bfst, bend =
    Offs.to_int (Bounds.to_offs bfst), Offs.to_int (Bounds.to_offs bend) in
  List.for_all
    (fun ele ->
      match ele.ir_kind with
      | Ik_unk -> false
      | Ik_empz -> true
      | Ik_range (l, r) -> max l bfst < min r bend
    ) ie.i_rules

(* return true only if all rules of both inds share some offset *)
let ind_ind_disequal (iel: ind) (ier: ind): bool =
  List.for_all
    (fun ele ->
      match ele.ir_kind with
       | Ik_unk -> false
       | Ik_empz -> true
       | Ik_range (l, r) ->
           List.for_all
             (fun eler ->
               match eler.ir_kind with
               | Ik_unk -> false
               | Ik_empz -> true
               | Ik_range (lr, rr) -> max l lr < min r rr
             ) ier.i_rules
    ) iel.i_rules

let reduce_pt_pt m0 m1 c0 c1 =
  let in_bound (c: int) (m: pt_edge Block_frag.t) =
    let b = Block_frag.end_bound m in
    Bounds.(compare b @@ of_int c) >= 0 in
  if Block_frag.is_not_empty m0
      && Block_frag.is_not_empty m1
      && pt_pt_disequal m0 m1
      && in_bound c0 m0
      && in_bound c1 m1 then
    Gr_bot
  else Gr_no_info

(* whether a graph + a condition are not compatible
 * (i.e., we should consider _|_ *)
let graph_guard (b: bool) (c: n_cons) (g: graph): guard_res =
  let loc_debug = false in
  match c, b with
  | Nc_cons (Apron.Tcons1.EQ, Ne_var v0, Ne_csti 0), true
  | Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, Ne_var v0), true
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_var v0, Ne_csti 0), false
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_csti 0, Ne_var v0), false ->
      (* Check whether node v0 may be null in g *)
      graph_guard_node_null v0 g
  | Nc_cons (Apron.Tcons1.EQ, e0, Ne_csti 0), true
  | Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, e0), true
  | Nc_cons (Apron.Tcons1.DISEQ, e0, Ne_csti 0), false
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_csti 0, e0), false ->
      (* If e0 writes down as v0+..., check whether v0 may be null in g *)
      begin
        match e0 with
        | Ne_bin (Apron.Texpr1.Add, Ne_var v0, _)
        | Ne_bin (Apron.Texpr1.Add, _, Ne_var v0) ->
            graph_guard_node_null v0 g
        | _ -> Gr_no_info
      end
  | Nc_cons (Apron.Tcons1.EQ, Ne_var v0, Ne_var v1), true
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_var v0, Ne_var v1), false ->
      if loc_debug then
        Log.debug "node equality in the graph::: %a %a\n" sv_fpr v0 sv_fpr v1;
      if v0 != v1 then
        match (node_find v0 g).n_e, (node_find v1 g).n_e with
        | Hpt m0, Hpt m1 -> reduce_pt_pt m0 m1 0 0
        (* cases where an equality may be deduced in the graph;
         * to reduce shortly thereafter *)
        | Hemp, Hemp ->
            if v0 < v1 then Gr_equality (v1, v0)
            else Gr_equality (v0, v1)
        | Hemp, _ -> Gr_equality (v0, v1)
        | _, Hemp -> Gr_equality (v1, v0)
        (* segment with destination corresponding to a region and
         * first element describing another region if non empty
         *  => imposes the emptiness of the segment *)
        (* JG: shouldn't we check that the destination node of the segment
         * is indeed the other node ? *)
        (* XR: TODO: check *)
        | Hseg se, Hpt b -> Gr_emp_seg v0
        | Hpt b, Hseg se -> Gr_emp_seg v1
        | Hseg se, Hind ie -> Gr_emp_seg v0
        | Hind ie, Hseg se -> Gr_emp_seg v1
        | Hseg sel, Hseg ser ->
            if sel.se_dnode = v1 then
              Gr_emp_seg v0
            else if ser.se_dnode = v0 then
              Gr_emp_seg v1
            else
              Gr_no_info
        | Hind ie0, Hind ie1 ->
            if ind_ind_disequal ie0.ie_ind ie1.ie_ind &&
              ie0.ie_ind.i_mt_rule && ie1.ie_ind.i_mt_rule then
              Gr_equality (v0, v1)
            else
              begin
                Log.warn
                  "graph equality test; precision might have been gained";
                Gr_no_info
              end
        (* HS: when one side is ind, and other is pt *)
        | Hind ie, Hpt b ->
            let dis_range = pt_ind_disequal ie.ie_ind b in
            if dis_range && ie.ie_ind.i_mt_rule  then
              Gr_bot
            else if dis_range && not ie.ie_ind.i_mt_rule then
              begin
                if ie.ie_ind.i_emp_ipar = -1 then
                  Gr_emp_ind v0
                else
                  let ptr = List.nth ie.ie_args.ia_ptr ie.ie_ind.i_emp_ipar in
                  match (node_find ptr g).n_e with
                  | Hpt m0 ->
                      if Block_frag.is_not_empty m0
                          && Block_frag.is_not_empty b
                          && pt_pt_disequal m0 b then
                        Gr_bot
                      else Gr_emp_ind v0
                  | _ -> Gr_emp_ind v0
              end
            else
              begin
                Log.warn
                  "graph equality test; precision might have been gained";
                Gr_no_info
              end
        | Hpt b, Hind ie ->
            let dis_range = pt_ind_disequal ie.ie_ind b in
            if dis_range && ie.ie_ind.i_mt_rule  then
              Gr_bot
            else if dis_range && not ie.ie_ind.i_mt_rule  then
              Gr_emp_ind v1
            else
              begin
                Log.warn
                  "graph equality test; precision might have been gained";
                Gr_no_info
              end
      else Gr_no_info
  | Nc_cons (Apron.Tcons1.EQ,
      Ne_bin (Apron.Texpr1.Add, Ne_var v0, Ne_csti c0),
      Ne_bin (Apron.Texpr1.Add,Ne_var v1, Ne_csti c1)), true
  | Nc_cons (Apron.Tcons1.DISEQ,
      Ne_bin (Apron.Texpr1.Add, Ne_var v0, Ne_csti c0),
      Ne_bin (Apron.Texpr1.Add,Ne_var v1, Ne_csti c1)), false  when v0 <> v1 ->
      begin
        match (node_find v0 g).n_e, (node_find v1 g).n_e with
        | Hpt m0, Hpt m1 -> reduce_pt_pt m0 m1 c0 c1
        | _, _ -> Gr_no_info
      end
  | _, _ -> Gr_no_info


(** Sat for conditions that can be partly evaluated in the graphs *)
(* Check if an SV disequality constraint is satisfied in a graph *)
let sat_graph_diseq (ls_cur_l: graph) (i: sv) (j: sv): bool =
  if i = j then false
  else
    let i_node = SvMap.find i ls_cur_l.g_g in
    let j_node = SvMap.find j ls_cur_l.g_g in
    match i_node.n_e, j_node.n_e with
    | Hpt _, Hpt _ -> true (* distinct SV carrying points-to => different *)
    | _ -> false


(** Segment splitting *)
let seg_split (is: sv) (g: graph)
    : graph * sv * Seq_sig.seq_cons list
    * (col_kind * colv_info option) SvMap.t =
  let se = seg_edge_find is g in
  (* integers and set parameters not supported yet *)
  let ind = se.se_ind in
  assert (ind.i_ipars = 0);
  assert (ind.i_spars = 0);
  (* creation of a middle node *)
  let im, g = sv_add_fresh Ntaddr Nnone g in
  (* creation of a set of parameters at the middle node *)
  let lpars, g =
    let rec aux i (l, g) =
      if i < 0 then (l, g)
      else
        let ii, g = sv_add_fresh Ntaddr Nnone g in
        aux (i-1) (ii :: l, g) in
    aux (ind.i_ppars-1) ([ ], g) in
  let lint, g = ind_args_1_make Ntint ind.i_ipars g in
  let lset, g = ind_sargs_make ind.i_spars g in
  let margs = { ia_ptr = lpars;
                ia_int = lint;
                ia_set = lset;
                ia_seq = [] } in
  (* remove the former segment edge *)
  let g = seg_edge_rem is g in
  (* this function does not support the segment edge parameters yet *)
  assert (se.se_eargs.ias_set = [] && se.se_sargs.ia_set = []);
  (* build and add the two new segment edges *)
  let g, lseq0, lseq1, eacc, colvacc, sacc =
    List.fold_left
      (fun (gacc, l0acc, l1acc, eacc, colvacc, sacc) (x,y) ->
        let colv1, gacc = colv_add_fresh (Ct_seq None) gacc in
        let info1 = find_info colv1 gacc in (*it must be none*)
        let colv2, gacc = colv_add_fresh (Ct_seq None) gacc in
        let info2 = find_info colv2 gacc in (*it must be none*)
        let colv3, gacc = colv_add_fresh (Ct_seq None) gacc in
        let info3 = find_info colv3 gacc in (*it must be none*)
        let colv4, gacc = colv_add_fresh (Ct_seq None) gacc in
        let info4 = find_info colv4 gacc in (*it must be none*)
        gacc, (colv1, colv2) :: l0acc, (colv3, colv4) :: l1acc,
        Seq_sig.Seq_equal
          (Seq_sig.Seq_var x,
          Seq_sig.Seq_Concat([Seq_sig.Seq_var colv1; Seq_sig.Seq_var colv3])) ::
        Seq_sig.Seq_equal
          (Seq_sig.Seq_var y,
          Seq_sig.Seq_Concat([Seq_sig.Seq_var colv4; Seq_sig.Seq_var colv2])) ::
        eacc,
        colvacc
        |> SvMap.add colv1 (CK_seq, info1) |> SvMap.add colv2 (CK_seq, info2)
        |> SvMap.add colv3 (CK_seq, info3) |> SvMap.add colv4 (CK_seq, info4),
        sacc |> SvSet.add colv1 |> SvSet.add colv2)
    (g, [], [], [], SvMap.empty, SvSet.empty) se.se_eargs.ias_seq in
  let lseq0, lseq1 = List.rev lseq0, List.rev lseq1 in
  let se0 = { se_ind   = ind;
              se_eargs = {ind_args_s_empty with ias_seq = lseq0};
              se_sargs = se.se_sargs;
              se_dargs = margs;
              se_dnode = im }
  and se1 = { se_ind   = ind;
              se_eargs = {ind_args_s_empty with ias_seq = lseq1};
              se_sargs = margs;
              se_dargs = se.se_dargs;
              se_dnode = se.se_dnode } in
  (*hseg_sanity_check ~isok:(-1) "graph_utils,seg_split,0" se0;
  hseg_sanity_check ~isok:(-1) "graph_utils,seg_split,1" se1;*)
  let g = seg_edge_add im se1 (seg_edge_add is se0 g) in
  g, im, eacc, colvacc


(** Utilities *)
let node_attribute_lub (a0: node_attribute) (a1: node_attribute)
    : node_attribute =
  match a0, a1 with
  | Attr_array (stl, ll), Attr_array (str, lr) ->
      if stl = str && ll = lr then a0
      else Attr_none
  | Attr_ind il, Attr_ind ir ->
      if String.compare il ir = 0 then a0
      else Attr_none
  | _, _ -> Attr_none
(* For join, creation of a graph with a set of roots, from two graphs *)
let init_graph_from_roots
    ?(tmpexp: bool = false)
    ~(submem: bool) (* whether we consider a sub-memory or not *)
    (m: (sv * sv) SvMap.t)
    (msetv: (sv * sv) SvMap.t)
    (mseqv: (sv * sv) SvMap.t)
    (gl: graph) (gr: graph): graph * (col_kinds * col_kinds) * col_kinds =
  let loc_debug = false in
  (* Preparing SVs *)
  let graph =
    SvMap.fold
      (fun ii (il,ir) acc ->
        let nl = node_find il gl in
        let nr = node_find ir gr in
        let nt =
          if nl.n_t = nr.n_t then nl.n_t
          else
            match nl.n_t, nr.n_t with
            | Ntraw, nt | nt, Ntraw -> nt
            | _ -> Log.fatal_exn "init graph, nt" in
        let nalloc =
          if submem then Nsubmem
          else if nl.n_alloc = nr.n_alloc then nl.n_alloc
          else
            match nl.n_alloc, nr.n_alloc with
            | Nnone, _ | _, Nnone -> Nnone
            | _ ->
                Log.fatal_exn "init graph, nalloc: %a | %a"
                  nalloc_fpr nl.n_alloc nalloc_fpr nr.n_alloc in
        let r =
          match SvSet.mem il gl.g_roots, SvSet.mem ir gr.g_roots with
          | true, true -> true
          | false, false -> false
          | _ -> Log.fatal_exn "init_graph, root" in
        sv_add ~attr: (node_attribute_lub nl.n_attr nr.n_attr)
          ~root:(not submem && r) ii nt nalloc acc
      ) m (graph_empty gl.g_inds) in
  (* Preparing COLVs, experimental; much to share with the list function *)
  let col_roots =
    let roots_l = colv_get_roots gl and roots_r = colv_get_roots gr in
    assert (SvMap.cardinal roots_l = SvMap.cardinal roots_r);
    roots_l in
  let colvl = colv_get_all gl and colvr = colv_get_all gr in
  let mapdiff m s = SvMap.filter (fun cv _ -> not (SvMap.mem cv s)) m in
  let col_toadd_l = mapdiff colvr colvl
  and col_toadd_r = mapdiff colvl colvr in
  (* JG: Since the key generator is the same for both set and sequence variables,
   * we may have S[i] in the left and Q[i] in the right.
   * Is there a test with both set and seq variables necessiting this check ? *)
  (* SvMap.iter
    (fun cv kl ->
      try assert (kl = SvMap.find cv colvr)
      with Not_found -> ( )
    ) colvl; *)
  let all_colv = SvMap.fold SvMap.add colvl colvr in
  let non_roots_colv =
    SvMap.filter (fun cv _ -> not (SvMap.mem cv col_roots)) all_colv in
  if loc_debug then
    begin
      Log.debug "init_graph_from_roots:\nRoots: %a\nL-all: %a\nR-all: %a"
        col_kinds_fpr col_roots   col_kinds_fpr colvl       col_kinds_fpr colvr;
      Log.debug "L-add: %a\nR-add: %a\nAll:   %a\nNroot: %a"
        col_kinds_fpr col_toadd_l col_kinds_fpr col_toadd_r
        col_kinds_fpr all_colv    col_kinds_fpr non_roots_colv
    end;
  (* Preparing COLVs *)
  let add_col_roots (k: col_kind) (m: (sv * sv) SvMap.t) (g: graph): graph =
    let add ii (il, ir) acc =
      let info = SvMap.find_opt il gl.g_colvinfo in
      colv_add ~root:(colv_is_root gl il) ii ~info k acc in
    SvMap.fold add m g in
  let graph = add_col_roots CK_seq mseqv (add_col_roots CK_set msetv graph) in
  (* Experimental ADD *)
  let graph =
    if tmpexp then
      SvMap.fold
        (fun i k acc ->
          if loc_debug then
            Log.debug "ColADD: %a (%b)" (colv_fpr k) i (SvMap.mem i col_roots);
          if SvMap.mem i col_roots then acc
          else colv_add i k acc
        ) all_colv graph
    else graph in
  graph, (col_toadd_l, col_toadd_r), non_roots_colv

(* retrieve node with index 'n' from graph 'g' *)
let get_node (n: sv) (g: graph): node =
  SvMap.find n g.g_g

(* A sub-memory specific utility function:
 * - given a (node,offset) pair, search whether there exist an edge
 *   pointing to it *)
let node_offset_is_referred (g: graph) ((i, o): sv * Offs.t): bool =
  let n = node_find i g in
  try
    SvSet.iter
      (fun pred ->
        let npred = node_find pred g in
        match npred.n_e with
        | Hemp -> ( )
        | Hpt block ->
            Block_frag.iter_base
              (fun _ pe ->
                if i = fst pe.pe_dest && Offs.eq o (snd pe.pe_dest) then
                  raise True
              ) block
        | Hind _ | Hseg _ ->
            (* we should search for references there too... *)
            ( )
      ) n.n_preds;
    false
  with True -> true

(** Latex output *)
let latex_output (roots: (sv, string) PMap.t) (chan: out_channel)
    (g: graph): unit =
  (* - construction of the "simplified graph"
   * - strongly connected components and topological order
   * - node ordering
   * - at some point, treat direct pt successors
   *)

  let module G = Graph.Graph(SvOrd) in
  let pos_graph, roots =
    SvMap.fold
      (fun is n (accp, accr) ->
        if PMap.mem is roots then
          match n.n_e with
          | Hpt mc -> (* mc should contain only one element at offset zero *)
              let v = Block_frag.find_addr Bounds.zero in
              accp, SvMap.add is v accr
          | _ -> Log.fatal_exn "latex_output, crashed on root"
        else
          let accp = G.node_add is accp in
          let accp =
            match n.n_e with
            | Hemp
            | Hind _ -> accp
            | Hpt mc ->
                Block_frag.fold_base
                  (fun _ pe accp ->
                    let dest = fst pe.pe_dest in
                    G.edge_add is dest (G.node_add dest accp)
                  ) mc accp
            | Hseg se ->
                G.edge_add is se.se_dnode (G.node_add se.se_dnode accp) in
          accp, accr
      ) g.g_g (G.empty, SvMap.empty) in
  let _ = G.tarjan pos_graph in
  Log.todo_exn "latex output (1)"


(** Ensuring consistency with numerical values *)
let sve_sync_bot_up (g: graph): graph * svenv_mod =
  { g with g_svemod = Dom_utils.svenv_empty }, g.g_svemod


(** Garbage collection support *)
(* Incremental garbage collection:
 *  -> this funciton is purely based on the back indexes;
 *   i.e, a node with no back_index, and that is not a root is removed
 *  -> for now, we rely on an externally supplied set;
 *     later on, use the g_roots field and trigger gc anywhere *)
(* Improvements to make in the GC code:
 *  - rely on a g_roots field for the root communication ?
 *  - do things even more incrementally: try removing a node
 *    when an edge to it gets removed
 *  - we need to study why is gc_incr not more effective
 *  - gc functions temporarily break invariants of back pointers
 *  - gc functions should warn when they detect a points to edge that
 *    needs to be removed *)
(** [gc_colv g] removes from [g] all colv that :
    - are non-roots and
    - that have empty b-index
    and returns this set of remove colv *)
let gc_colv (g: graph) : graph * SvSet.t =
  let colv_to_rem =
    SvMap.fold
      (fun colv bindex acc ->
        if SvSet.is_empty bindex && not @@ SvSet.mem colv g.g_colvroots then
          SvSet.add colv acc
        else acc
      ) g.g_colvbindex SvSet.empty in
  SvSet.fold colv_rem colv_to_rem g, colv_to_rem
let gc_incr (roots: sv Aa_sets.t) (g: graph): graph =
  if Flags.flag_gc_stat then incr Statistics.gc_stat_incr_num;
  (* gc should only be performed on states with no node pending treatment *)
  assert (g.g_svemod = Dom_utils.svenv_empty);
  (* proceed with the garbage collection *)
  SvMap.fold
    (fun i n acc ->
      if SvSet.cardinal n.n_preds > 0 || Aa_sets.mem i roots then
        acc
      else
        begin
          if !Flags.flag_dbg_back_index then
            Log.info "gc_incr removing node %a\n" sv_fpr i;
          if Flags.flag_gc_stat then incr Statistics.gc_stat_incr_rem;
          sv_rem i acc
        end
    ) g.g_g g
(* Removal of all nodes not reachable from a set of roots *)
let gc (roots: sv Aa_sets.t) (g: graph): graph * SvSet.t =
  if Flags.flag_gc_stat then incr Statistics.gc_stat_full_num;
  (* gc should only be performed on states with no node pending treatment *)
  assert (g.g_svemod = Dom_utils.svenv_empty);
  (* perform an incremental collection first, if required *)
  let g =
    if Flags.flag_gc_incr then gc_incr roots g
    else g in
  if !Flags.flag_gc_full then
    (* Exhaustive traversal, saturation of set of roots *)
    let reach =
      let r = ref SvSet.empty
      and todos = ref roots in
      let add_dest (i: sv): unit =
        if not (SvSet.mem i !r) then
          todos := Aa_sets.add i !todos in
      while !todos != Aa_sets.empty do
        let i = Aa_sets.min_elt !todos in
        r := SvSet.add i !r;
        todos := Aa_sets.remove i !todos;
        try
          let n = SvMap.find i g.g_g in
          let s = node_reach n g in
          SvSet.iter add_dest s
        with Not_found -> ( )
      done;
      !r in
    let nremoved =
      SvMap.fold
        (fun i _ acc ->
          if SvSet.mem i reach then acc
          else
            begin
              if !Flags.flag_dbg_back_index then
                Log.force "gc_full removing node %a\n" sv_fpr i;
              if Flags.flag_gc_stat then incr Statistics.gc_stat_full_rem;
              SvSet.add i acc
            end
        ) g.g_g SvSet.empty in
    g
    |> SvSet.fold edge_rem_any nremoved
    |> SvSet.fold sv_rem nremoved
    |> gc_colv
  else gc_colv g


(** Functions on node injections (for is_le) *)
module Nemb =
  struct
    let empty: node_embedding =
      { n_img = SvMap.empty ;
        n_pre = SvMap.empty }
    (* Pretty-printing, compact version *)
    let ne_fpr (fmt: form) (ni: node_embedding): unit =
      SvMap.t_fpr "\n" sv_fpr fmt ni.n_img
    (* Pretty-printing, long version *)
    let ne_full_fpri (ind: string) (fmt: form) (inj: node_embedding): unit =
      SvMap.iter
        (fun i j -> F.fprintf fmt "%s%a => %a\n" ind nsv_fpr i nsv_fpr j)
        inj.n_img
    (* Tests membership *)
    let mem (i: sv) (ni: node_embedding): bool =
      SvMap.mem i ni.n_img
    (* Find an element in the mapping *)
    let find (i: sv) (ni: node_embedding): sv =
      SvMap.find i ni.n_img
    (* Add an element to the mapping *)
    let add (i: sv) (j: sv) (ni: node_embedding): node_embedding =
      try
        assert (SvMap.find i ni.n_img = j);
        ni
      with
      | Not_found ->
          let npre =
            let oset =
              try SvMap.find j ni.n_pre with Not_found -> SvSet.empty in
            SvSet.add i oset in
          { n_img = SvMap.add i j ni.n_img ;
            n_pre = SvMap.add j npre ni.n_pre }
      | Assert_failure _ as exn ->
        if !Flags.flag_dbg_symvars then
          Log.debug "%a already mapped to %a ≠ %a"
            sv_fpr i sv_fpr (SvMap.find i ni.n_img) sv_fpr j;
        raise exn
    (* Initialization *)
    let init (m: node_emb): node_embedding =
      Aa_maps.fold add m empty
    (* Extraction of siblings information *)
    let siblings (ni: node_embedding): SvSet.t SvMap.t =
      SvMap.fold
        (fun j pre acc ->
          if SvSet.cardinal pre > 1 then SvMap.add j pre acc
          else acc
        ) ni.n_pre SvMap.empty
  end


(** Functions on node weak injections (for directed weakening) *)
module Nwkinj =
  struct
    let empty: node_wkinj =
      { wi_img  = SvMap.empty;
        wi_l_r  = SvMap.empty;
        wi_lr_o = SvPrMap.empty }
    (* Verification of existence of a mapping *)
    let mem (wi: node_wkinj) ((il, ir): sv * sv) (io: sv): bool =
      try
        let cl, co = SvMap.find ir wi.wi_img in
        cl = il && co = io
      with
      | Not_found -> false
    (* Find function, may raise Not_found *)
    let find (wi: node_wkinj) (p: sv * sv): sv =
      SvPrMap.find p wi.wi_lr_o
    (* Addition of a mapping *)
    let add (wi: node_wkinj) ((il, ir): sv * sv) (io: sv): node_wkinj =
      if SvMap.mem ir wi.wi_img then
        Log.fatal_exn "Node_weak_inj: cannot override %a" sv_fpr ir
      else
        let os = try SvMap.find il wi.wi_l_r with Not_found -> SvSet.empty in
        assert (not (SvPrMap.mem (il,ir) wi.wi_lr_o));
        { wi_img  = SvMap.add ir (il, io) wi.wi_img;
          wi_l_r  = SvMap.add il (SvSet.add ir os) wi.wi_l_r;
          wi_lr_o = SvPrMap.add (il,ir) io wi.wi_lr_o }
    (* Pretty-printing, long and indented version *)
    let wi_fpri (ind: string) (fmt: form) (wi: node_wkinj): unit =
      F.fprintf fmt "%sInjection:\n" ind;
      SvPrMap.iter
        (fun (il, ir) ii ->
          F.fprintf fmt "%s  (%a,%a) => %a\n" ind sv_fpr il
              sv_fpr ir sv_fpr ii
        ) wi.wi_lr_o
  end


(** Functions on node relations (for join) *)
module Nrel =
  struct
    let empty =
      { n_inj = SvPrMap.empty ;
        n_pi  = SvMap.empty ;
        n_l2r = SvMap.empty ;
        n_r2l = SvMap.empty ; }
    (* Consistency check *)
    let check (nr: node_relation): bool =
      try
        SvPrMap.iter
          (fun (il,ir) ii ->
            let kl, kr = SvMap.find ii nr.n_pi in
            if kl != il || kr != ir then raise Stop
          ) nr.n_inj ;
        SvMap.iter
          (fun ii ip ->
            if SvPrMap.find ip nr.n_inj != ii then raise Stop
          ) nr.n_pi ;
        true
      with
      | Stop -> false
      | Not_found -> false
    (* Verification of existence of a mapping *)
    let mem (nr: node_relation) (pn: sv * sv) (nn: sv): bool =
      try SvPrMap.find pn nr.n_inj = nn
      with Not_found -> false
    (* Find function, may raise Not_found *)
    let find (nr: node_relation) (pn: sv * sv): sv =
      SvPrMap.find pn nr.n_inj
    (* find a pair from out *)
    let find_p (nr: node_relation) (p:sv): sv * sv =
      SvMap.find p nr.n_pi
    (* Addition of a mapping *)
    let add (nr: node_relation) ((pl, pr) as pn: sv * sv)
        (nn: sv): node_relation =
      if SvPrMap.mem pn nr.n_inj then
        Log.info "trying to re-add: (%a, %a) => %a [%a]" sv_fpr pl
          sv_fpr pr sv_fpr nn sv_fpr (SvPrMap.find pn nr.n_inj);
      assert (not (SvPrMap.mem pn nr.n_inj));
      assert (not (SvMap.mem nn nr.n_pi));
      let l2r =
        let o = try SvMap.find pl nr.n_l2r with Not_found -> SvSet.empty in
        SvMap.add pl (SvSet.add pr o) nr.n_l2r in
      let r2l =
        let o = try SvMap.find pr nr.n_r2l with Not_found -> SvSet.empty in
        SvMap.add pr (SvSet.add pl o) nr.n_r2l in
      { n_inj = SvPrMap.add pn nn nr.n_inj ;
        n_pi  = SvMap.add nn pn nr.n_pi ;
        n_l2r = l2r ;
        n_r2l = r2l ; }
    (* To string, long version *)
    let nr_fpri (ind: string) (fmt: form) (nr: node_relation): unit =
      F.fprintf fmt "%sInjection:\n" ind;
      SvPrMap.iter
        (fun (il, ir) ii ->
          F.fprintf fmt "%s  (%a,%a) => %a\n" ind sv_fpr il sv_fpr ir sv_fpr ii
        ) nr.n_inj;
      F.fprintf fmt "%sProjection:\n" ind;
      SvMap.iter
        (fun ii (il, ir) ->
          F.fprintf fmt "%s  %a => (%a,%a)\n" ind sv_fpr ii sv_fpr il sv_fpr ir
        ) nr.n_pi;
      F.fprintf fmt "%sOk: %b\n" ind (check nr)
    (* To string, compact version *)
    let nr_fprc (fmt: form) (nr: node_relation): unit =
      F.fprintf fmt "{ ";
      let isdeb = ref true in
      SvPrMap.iter
        (fun (il, ir) ii ->
          let sep = if !isdeb then "" else "; " in
          isdeb := false;
          F.fprintf fmt "%s(%a,%a) => %a" sep sv_fpr il sv_fpr ir sv_fpr ii
        ) nr.n_inj;
      F.fprintf fmt " }"
    (* Collect nodes associated to other side *)
    let siblings (s: side) (i: sv) (nr: node_relation): SvSet.t =
      try SvMap.find i (if s = Lft then nr.n_l2r else nr.n_r2l)
      with Not_found -> SvSet.empty
    (* Search for nodes that have multiple right matches *)
    let mult_bind_candidates (side: side)
        (nr: node_relation) (b: SvSet.t): SvSet.t =
      SvMap.fold
        (fun il r acc ->
          if SvSet.subset b r then SvSet.add il acc
          else acc
        ) (if side = Lft then nr.n_l2r else nr.n_r2l) SvSet.empty
    (* Fold function *)
    let fold (f: sv -> (sv * sv) -> 'a -> 'a) (nr: node_relation): 'a -> 'a =
      SvMap.fold f nr.n_pi
  end
let add_colvinfo_nrel (l: graph) (r: graph) (o: graph)
    (colrel: node_relation) (nrel: node_relation)
    : node_relation =
  let get_info colv x =
    SvMap.find_opt colv x.g_colvinfo in
  let add_info colvl colvr colvo nrel =
    match get_info colvl l, get_info colvr r, get_info colvo o with
    | None, None, None -> nrel
    | Some il, Some ir, Some io ->
        let nrel = Nrel.add nrel (il.min, ir.min) io.min in
        let nrel = Nrel.add nrel (il.max, ir.max) io.max in
        let nrel = Nrel.add nrel (il.size, ir.size) io.size in
        nrel
    | il, ir, io ->
        Log.fatal_exn "%a: %a\n%a: %a\n%a: %a"
          sv_fpr colvl (Option.t_fpr Col_utils.colv_info_fpr) il
          sv_fpr colvr (Option.t_fpr Col_utils.colv_info_fpr) ir
          sv_fpr colvo (Option.t_fpr Col_utils.colv_info_fpr) io in
  SvMap.fold (fun colvo (colvl, colvr) nrel -> add_info colvl colvr colvo nrel)
    colrel.n_pi nrel


(** Utilities over steps *)
let is_segment = function
  | Segment _ -> true
  | _ -> false
let is_offset = function
  | Offset  _ -> true
  | _ -> false

(** Operations on abs_graph_arg: helping join *)
(* Check if memory starting from SV could be weakened into a segment *)
let sv_seg_weaken (gl: abs_graph_arg option) (sl: sv): bool =
  try
    let gl = Option.get gl in
    let onode = sl, Offs.zero in
    List.exists
      (fun ele ->
         ele.sc = onode && is_segment (List.hd ele.pth)
         && ele.sc <> ele.dt ) gl
  with
  | Not_found -> false
  | Invalid_argument _ -> false
  | Failure _ -> false

(* Check if memory starting from SV could have a segment intro *)
let sv_seg_intro (gl: abs_graph_arg option) (sl: sv): bool =
  try
    let gl = Option.get gl in
    let onode = sl, Offs.zero in
    List.exists
      (fun ele -> ele.sc = onode && is_segment (List.hd ele.pth)) gl
  with
  | Not_found -> false
  | Invalid_argument _ -> false
  | Failure _ -> false

(* Check whether a segment extension could be performed *)
let is_bseg_ext (gl: abs_graph_arg option) (sl: sv)
    (gr: abs_graph_arg option) (sr: sv): bool =
  sv_seg_weaken gl sl && sv_seg_weaken gr sr
let is_bseg_intro  (gl: abs_graph_arg option) (sl: sv)
    (gr: abs_graph_arg option) (sr: sv): bool =
  sv_seg_intro gl sl && sv_seg_intro gr sr

(* choose destination nodes for seg_intro from encode graph *)
let choose_dst (sc: sv) (ag: abs_graph_arg option)
    (sibs: SvSet.t): SvSet.t =
  try
    let ag = Option.get ag in
    let sc = sc, Offs.zero in
    let send =
      List.fold_left
        (fun acc ele ->
          if ele.sc = sc && is_segment (List.hd ele.pth) then
            SvSet.add (fst ele.dt) acc
          else acc
        ) SvSet.empty ag in
    SvSet.inter send sibs
  with
  | Invalid_argument _ -> SvSet.empty

(** Operations on "join_arg" type: join with additional arguments *)
(* Translation of join argument *)
let tr_join_arg (sat: n_cons -> bool) ((g, x): graph * join_ele)
    : graph * join_arg =
  let tr_node (n: IntSvPrSet.t) (x: graph): Offs.svo * graph =
    try
      let os, is = IntSvPrSet.choose n in
      let n = IntSvPrSet.remove (os, is) n in
      let x, pte =
        pt_edge_extract sat (is, Offs.of_int os) Flags.abi_ptr_size x in
      IntSvPrSet.fold
        (fun (os, is) (onode, x) ->
          let x, pte =
            pt_edge_extract sat (is, Offs.of_int os) Flags.abi_ptr_size x in
          assert (pte.pe_dest = onode);
          onode, x
        ) n (pte.pe_dest, x)
    with Not_found -> Log.fatal_exn "tr_join_arg, node not found" in
  let tr_abs_g ((g, n, _disj_n): abs_graph) (x: graph): abs_graph_arg * graph =
    List.fold_left
      (fun (acc, x) (sc, p, dt) ->
        let sc, x = tr_node sc x in
        let dt, x = tr_node dt x in
        { sc  = sc;
          dt  = dt;
          pth = p } :: acc, x
      ) ([], x) g in
  let f (ag: abs_graph option) (g: graph): abs_graph_arg option * graph =
    match ag with
    | None -> None, g
    | Some ag -> let ag, g = tr_abs_g ag g in Some ag, g in
  let abs_gi, g = f x.abs_gi g in
  let abs_go, g = f x.abs_go g in
  g, { abs_gi = abs_gi;
       abs_go = abs_go; }

(* Find end points for segment extension *)
let seg_extension_end (is: sv) (arg: join_arg): (sv * string) option =
  match arg.abs_go with
  | None -> None
  | Some abs_g ->
      try
        let pt =
          List.find
            (fun ele ->
              ele.sc = (is, Offs.zero)
                &&
              begin
                match ele.pth with
                | [] -> false
                | seg :: tl -> is_segment seg
              end
            ) abs_g in
        assert (snd pt.dt = Offs.zero);
        let x =
          match List.hd pt.pth with
          | Segment (name, _) -> name
          | _ -> Log.fatal_exn "improper case" in
        Some (fst pt.dt, x)
      with Not_found -> None

(* Find a node in encoded graph *)
let encode_node_find (is: sv) (arg: join_arg): bool =
  match arg.abs_gi with
  | None -> false
  | Some gi -> List.exists (fun ele -> fst ele.sc = is) gi

(* Remove node and related edges in encoded graph *)
let remove_node (is: sv) (id: sv) (arg: join_arg): join_arg =
  let f ele = (fst ele.sc <> is) || ( fst ele.dt <> id) in
  let i = Option.map (List.filter f) arg.abs_gi in
  let o = Option.map (List.filter f ) arg.abs_go in
  { abs_gi = i;
    abs_go = o }

(* Consume pt in encoded graph *)
let encode_pt_pt (ons: Offs.svo) (dons: Offs.svo) (arg: join_arg): join_arg =
  let consume_pt ((is, offs): sv * Offs.t) ((ds, dof): sv * Offs.t)
      (arg: abs_graph_arg): abs_graph_arg =
    (* HLI: add some comment after tests *)
    List.fold_left
      (fun acc ele ->
        let default = ele :: acc in
        if ele.sc <> (is, Offs.zero) then default
        else
          begin
            match ele.pth with
            | [ Offset off ] ->
                if off <> offs then default
                else
                  begin
                    assert ((ds, dof) = ele.dt);
                    acc
                  end
            | Offset off :: t ->
                if off <> offs then default
                else
                  (* (assert ((ds, dof) <> ele.dt); *)
                  { ele with
                    sc = (ds, dof);
                    pth = t } :: acc
            | _ -> ele :: acc
          end
      ) [ ] arg in
  { abs_gi = Option.map (consume_pt ons dons) arg.abs_gi;
    abs_go = Option.map (consume_pt ons dons) arg.abs_go; }

(* Collect all inductive definitions used in a graph *)
let collect_inds (g: graph): StringSet.t =
  SvMap.fold
    (fun key ele acc ->
      match ele.n_e with
      | Hemp -> acc
      | Hpt _ -> acc
      | Hind ind_edge -> StringSet.add ind_edge.ie_ind.i_name acc
      | Hseg seg_edge -> StringSet.add seg_edge.se_ind.i_name acc
    ) g.g_g StringSet.empty

let visu_opt_of_string (opt: string): visu_option =
  match opt with
  | "CC" -> Connex_component
  | "SUCC" -> Successors
  | "CUTL" -> Cut_leaves
  | s -> failwith ("visu_opt_of_string: " ^ s)

(** SV collapsings *)
(* Pretty-printing *)
let sv_collapsing_fpr (fmt: form) (nc: sv_collapsing): unit =
  let f fmt (_, on, sz) =
    F.fprintf fmt "%a[%a]" onode_fpr on Offs.size_fpr sz in
  F.fprintf fmt "%a |-> {%a} [%a]" sv_fpr nc.svcol_base
    (gen_list_fpr "" f ",") nc.svcol_block
    (Offs.OffSet.t_fpr "; ") nc.svcol_extptr

(** SV submem modifications *)
(* Empty *)
let sv_submem_mod_empty =
  { sub_fold    = SvMap.empty;
    sub_new_off = [ ]; }
(* Pretty-printing *)
let sv_submem_mod_fpr (ind: string) (fmt: form) (svm: sv_submem_mod)
    : unit =
  let sep = Format.sprintf "\n%s  " ind in
  let new_off_fpr fmt (o_old, i_new, o_new) =
    Format.fprintf fmt "%a, %a, %a" Offs.t_fpr o_old sv_fpr i_new
      Offs.t_fpr o_new in
  F.fprintf fmt "%sCollapsing:%s%a" ind sep
    (SvMap.t_fpr sep sv_collapsing_fpr) svm.sub_fold;
  F.fprintf fmt "%sNew offsets:%s%a" ind sep
    (gen_list_fpr "[]" new_off_fpr sep) svm.sub_new_off

(** Experimental flag (temporary for fixing instantiation in join) *)
let do_exp_jinst: bool ref = ref false


(** Computation of a hint for join *)
(* Utilities *)
let rec compute_hint_aux (l: sv list) (acc1: SvPrSet.t): SvPrSet.t =
  match l with
  | [] -> acc1
  | hd :: tl ->
      let acc3 =
        List.fold_left (fun acc2 id -> SvPrSet.add (hd, id) acc2) acc1 tl in
      compute_hint_aux tl acc3
let rec compute_hint_aux_list =
  List.fold_left (fun acc l -> compute_hint_aux l acc) SvPrSet.empty
(* Main function, called from the join function in dom_shape *)
let compute_hint_simpl (g: graph): SvPrSet.t =
  let listify_seg l1 l2 =
    List.map2 (fun (x1, x2) (y1, y2) -> [x1; y1; y2; x2]) l1 l2 in
  let listify_ind l1 l2 =
    List.map2 (fun (x1, x2) y -> [x1; y; x2]) l1 l2 in
  let map_node_seg, map_node_ind =
    SvMap.fold
      (fun id n (accs, acci) ->
        match n.n_e with
        | Hseg s -> SvMap.add id s accs, acci
        | Hind i -> accs, SvMap.add id i acci
        | _ -> accs, acci
      ) g.g_g (SvMap.empty, SvMap.empty) in
  let list =
    SvMap.fold
      (fun id seg acc ->
        let l1 =
          let f seg' = listify_seg seg.se_eargs.ias_seq seg'.se_eargs.ias_seq in
          Option.fold ~none: [] ~some:f
            (SvMap.find_opt seg.se_dnode map_node_seg) in
        let l2 =
          let f ind = listify_ind seg.se_eargs.ias_seq ind.ie_args.ia_seq in
          Option.fold ~none: [] ~some:f
            (SvMap.find_opt seg.se_dnode map_node_ind) in
        l1 @ l2 @ acc
      ) map_node_seg []
  in compute_hint_aux_list list
