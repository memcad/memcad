(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_utils.ml
 **       utilities for the simplified list domain
 ** Xavier Rival, 2014/02/24 *)
open Data_structures
open Flags
open Graph
open Lib
open Sv_def

open Ast_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Svenv_sig
open Vd_sig

open Gen_dom

open Ind_inst

open Dom_utils
open Graph_utils
open Ind_form_utils
open Ind_utils
open Inst_utils
open Set_utils
open Seq_utils
open Sv_utils
open Vd_utils

open FInd


(** Error report *)
module Log =
  Logger.Make(struct let section = "l_uts___" and level = Log_level.DEBUG end)

let debug_module = false

(** Empty memory *)
let lmem_empty: lmem =
  { lm_mem       = SvMap.empty;
    lm_nkey      = SvGen.empty;
    lm_roots     = SvSet.empty;
    lm_svemod    = svenv_empty;
    lm_dangle    = SvSet.empty;
    lm_colvkey   = SvGen.empty;
    lm_colvroots = SvSet.empty;
    lm_colvkind  = SvMap.empty;
    lm_refcnt    = SvMap.empty; }

(** Pretty-printing *)
(* To formatters *)
(* ldesc: full pretty-printer, to troubleshoot the list inductive contents
 *        (for other purposes, the shorter pretty-printer should be used) *)
let set_elt_fpr (fmt: form): set_elt -> unit = function
  | Se_this    -> F.fprintf fmt "this"
  | Se_field i -> F.fprintf fmt "[fld:%d]" i
let set_var_fpr (fmt: form): set_var -> unit = function
  | Sv_actual i      -> F.fprintf fmt "[act:%d]" i
  | Sv_nextpar i     -> F.fprintf fmt "[next:%d]" i
  | Sv_subpar (i, j) -> F.fprintf fmt "[sub:(%d,%d)]" i j
let set_def_fpr (fmt: form): set_def -> unit = function
  | Sd_var s -> set_var_fpr fmt s
  | Sd_uplus (e, s) -> F.fprintf fmt "{ %a } + %a" set_elt_fpr e set_var_fpr s
  | Sd_eunion (e, s) -> F.fprintf fmt "{ %a } $u %a" set_elt_fpr e set_var_fpr s
  | Sd_union (s0, s1) -> F.fprintf fmt " %a $u %a" set_var_fpr s0 set_var_fpr s1
let set_equa_fpr (fmt: form): set_equa -> unit = function
  | Se_mem (e, s) -> F.fprintf fmt "%a $in %a" set_elt_fpr e set_def_fpr s
  | Se_eq (s0, s1) -> F.fprintf fmt "%a $eq %a" set_var_fpr s0 set_def_fpr s1
let seq_elt_fpr (fmt: form): seq_elt -> unit = function
  | Qe_this -> F.fprintf fmt "this"
  | Qe_field i -> F.fprintf fmt "[fld:%d]" i
let seq_var_fpr (fmt: form): seq_var -> unit = function
  | Qv_actual i      -> F.fprintf fmt "[act:%d]" i
  | Qv_nextpar i     -> F.fprintf fmt "[next:%d]" i
let rec seq_def_fpr (fmt: form): seq_def -> unit = function
  | Qd_empty -> F.fprintf fmt "[ ]"
  | Qd_var var -> seq_var_fpr fmt var
  | Qd_val v -> seq_elt_fpr fmt v
  | Qd_concat l -> gen_list_fpr "" seq_def_fpr "." fmt l
  | Qd_sort s -> F.fprintf fmt "sort(%a)" seq_def_fpr s
let seq_equa_fpr (fmt: form): seq_equa -> unit = function
  | Qe_eq (v, s) -> F.fprintf fmt "%a == %a" seq_var_fpr v seq_def_fpr s
let seti_fpr (fmt: form) (si: seti): unit =
  F.fprintf fmt "[%a]" (gen_list_fpr "" set_par_type_fpr ", ") si.s_params;
  List.iter (fun p -> F.fprintf fmt " & (%a)" set_equa_fpr p) si.s_equas
let seqi_fpr (fmt: form) (si: seqi): unit =
  F.fprintf fmt "[%a]" (gen_list_fpr "" seq_par_type_fpr ", ") si.q_params;
  List.iter (fun p -> F.fprintf fmt " & (%a)" seq_equa_fpr p) si.q_equas
let rec l_def_fpri (i: string) (fmt: form) (def: l_def): unit =
  F.fprintf fmt "%sList{0-%d;n@%d}\n" i def.ld_size def.ld_nextoff;
  let ni = "  "^i in
  let foff fmt doff = if doff != 0 then F.fprintf fmt "@%d" doff in
  List.iter
    (fun (o, ld0, doff) ->
      F.fprintf fmt "%s@%d:\n%a%a" i o (l_def_fpri ni) ld0 foff doff
    ) def.ld_onexts;
  begin
    match def.ld_set with
    | None -> ()
    | Some cons -> F.fprintf fmt "%s%a\n" i seti_fpr cons
  end;
  begin
    match def.ld_seq with
    | None -> ()
    | Some cons -> F.fprintf fmt "%s%a\n" i seqi_fpr cons
  end
let l_call_fpri (i: string) (fmt: form) (lc: l_call): unit =
  F.fprintf fmt "Call to\n%a" (l_def_fpri i) lc.lc_def;
  List.iter (F.fprintf fmt "%sset_par at: %a\n" i sv_fpr) lc.lc_setargs;
  List.iter (F.fprintf fmt "%sseq_par at: %a\n" i sv_fpr) lc.lc_seqargs
(* ldesc: compact pretty printer
 *        (to use when displaying abstract values) *)
let l_call_fprc (fmt: form) (lc: l_call): unit =
  begin
    match lc.lc_def.ld_name with
    | None ->
        F.fprintf fmt "list(0-%d;n@%d;" lc.lc_def.ld_size
          lc.lc_def.ld_nextoff
    | Some s  -> F.fprintf fmt "%s(" s
  end;
  F.fprintf fmt "%a||" (gen_list_fpr "" Nd_utils.nsv_fpr ",") lc.lc_ptrargs;
  F.fprintf fmt "%a|" (gen_list_fpr "" setv_fpr ",") lc.lc_setargs;
  F.fprintf fmt "%a" (gen_list_fpr "" seqv_fpr ",") lc.lc_seqargs;
  F.fprintf fmt ")"
let lseg_call_fprc (fmt: form) (lsegc: lseg_call): unit =
  begin
    match lsegc.lc_def.ld_name with
    | None ->
        F.fprintf fmt "list(0-%d;n@%d;" lsegc.lc_def.ld_size
          lsegc.lc_def.ld_nextoff
    | Some s -> F.fprintf fmt "%s(" s
  end;
  if lsegc.lc_ptrargs != [] then
    gen_list_fpr "" sv_fpr "," fmt lsegc.lc_ptrargs;
  F.fprintf fmt ")"
let l_call_ptrvars_fpr (fmt: form) (lc: l_call): unit =
  match lc.lc_ptrargs with
  | [] -> ()
  | lc_args ->
      F.fprintf fmt "\\{"; (* backslash needed by dot *)
      List.iteri
        (fun i sv ->
          if i <> 0 then Format.fprintf fmt ",p%a" sv_fpr sv
          else Format.fprintf fmt "p%a" sv_fpr sv
        ) (List.rev lc_args);
      F.fprintf fmt "\\}" (* backslash needed by dot *)
let l_call_setvars_fpr (fmt: form) (lc: l_call): unit =
  match lc.lc_setargs with
  | [] -> ()
  | lc_args ->
      F.fprintf fmt "\\{"; (* backslash needed by dot *)
      List.iteri
        (fun i sv ->
          if i <> 0 then F.fprintf fmt ",s%a" sv_fpr sv
          else F.fprintf fmt "s%a" sv_fpr sv
        ) (List.rev lc_args);
      F.fprintf fmt "\\}" (* backslash needed by dot *)
let l_call_seqvars_fpr (fmt: form) (lc: l_call): unit =
  match lc.lc_seqargs with
  | [] -> ()
  | lc_args ->
      F.fprintf fmt "\\{"; (* backslash needed by dot *)
      List.iteri
        (fun i sv ->
          if i <> 0 then F.fprintf fmt ",q%a" sv_fpr sv
          else F.fprintf fmt "q%a" sv_fpr sv
        ) (List.rev lc_args);
      F.fprintf fmt "\\}" (* backslash needed by dot *)
let lheap_frag_fpri (ind: string) (is: sv) (fmt: form)
    : lheap_frag -> unit = function
  | Lhemp -> ()
  | Lhpt pte -> block_fpria ind fmt (is, pte)
  | Lhlist l ->
      F.fprintf fmt "%s%a ==%a==>\n" ind nid_fpr is l_call_fprc l
  | Lhlseg (l, d, l') ->
      F.fprintf fmt "%s%a ==%a====%a==> %a\n"
        ind nid_fpr is l_call_fprc l lseg_call_fprc l' nid_fpr d
let lnode_fpri (ind: string) (fmt: form) (n: lnode): unit =
  lheap_frag_fpri ind n.ln_i fmt n.ln_e
let lmem_fpri ?(refcount: bool = false) (ind: string) (fmt: form)
    (x: lmem): unit =
  if Flags.flag_pp_list_detail then
    F.fprintf fmt "%scolvs: %a\n" ind svset_fpr (SvGen.get_nodes x.lm_colvkey);
  if !Flags.flag_dbg_symvars then
    begin
      let colv_roots =
        SvMap.fold
          (fun cv k acc ->
            if SvSet.mem cv x.lm_colvroots then SvMap.add cv k acc
            else acc
          ) x.lm_colvkind SvMap.empty in
      F.fprintf fmt "%sCOLVs: { %a }\n" ind col_kinds_fpr
        (Col_utils.col_par_type_map_to_col_kinds x.lm_colvkind);
      F.fprintf fmt "%sSV-roots: { %a }\n" ind Nd_utils.nsvset_fpr x.lm_roots;
      F.fprintf fmt "%sCOLV-roots: { %a }\n"  ind col_kinds_fpr
        (Col_utils.col_par_type_map_to_col_kinds colv_roots);
    end;
  if !Flags.flag_dbg_dom_list && Flags.flag_pp_list_detail  then
    F.fprintf fmt "%sdangling: { %a }\n" ind (SvSet.t_fpr ", ") x.lm_dangle;
  SvMap.iter
    (fun _ (n: lnode) ->
      if !Flags.flag_dbg_dom_list && !Flags.flag_dbg_dom_lpreds then
        F.fprintf fmt "%spredecessors of node %a: { %a }\n"
          ind sv_fpr n.ln_i (SvMap.t_fpr ", " int_fpr)
          (SvMap.find_err "lmem_fpri: broken RC" n.ln_i x.lm_refcnt);
      F.fprintf fmt "%a" (lnode_fpri ind) n
    ) x.lm_mem;
  if refcount then
    SvMap.iter
      (fun i set ->
        F.fprintf fmt "%s%a => { %a }\n" ind sv_fpr i
          (SvMap.t_fpr ", " int_fpr) set
      ) x.lm_refcnt;
  if flag_sanity_env_pp then
    F.fprintf fmt "%a" (svenv_fpri ind) x.lm_svemod

(** content printing *)
let l_content_fpr (fmt: form) = function
  | Data i ->  F.fprintf fmt "data <- %d ->" i
  | Ptr (p, doff) ->
      F.fprintf fmt "|->%a" sv_fpr p;
      if doff != 0 then F.fprintf fmt "@{%i}" doff
  | Call (ld, doff) ->
      let s = match ld.ld_name with | None -> "List" | Some s -> s in
      F.fprintf fmt "|-> %s" s;
      if doff != 0 then F.fprintf fmt "@{%i}" doff

(** Reachability information in the graph: SVs that are used in a graph *)
let lnode_reach (ln: lnode): SvSet.t =
  match ln.ln_e with
  | Lhemp    -> SvSet.empty
  | Lhpt mc  -> Graph_utils.pt_edge_block_frag_reach mc
  | Lhlist _ -> SvSet.empty
  | Lhlseg (_, n, _) -> SvSet.singleton n
let lmem_reach (m: lmem) (start: SvSet.t): SvSet.t =
  let rec aux (acc_done, to_follow) =
    if to_follow = SvSet.empty then acc_done
    else
      let c = SvSet.choose to_follow in
      let ln = SvMap.find_err "lmem_reach" c m.lm_mem in
      let to_follow =
        SvSet.fold
          (fun i acc_to_follow ->
            if SvSet.mem i acc_done then acc_to_follow
            else SvSet.add i acc_to_follow
          ) (lnode_reach ln) to_follow in
      aux (SvSet.add c acc_done, SvSet.remove c to_follow) in
  aux (SvSet.empty, start)

let nodes_of_lmem (mem: lmem): lnode list =
  SvMap.values mem.lm_mem

(** Sanity checks *)
(* - all references should belong to the graph
 * - consistency between prevs, refcount, and edges
 * - all offsets should be integers
 *)
let sanity_check (loc: string) (x: lmem): unit =
  if !flag_sanity_ldom then
    (* auxilliary functions *)
    let report (msg: string) =
      Log.warn "sanity_check: failed (%s): %s\n %a"
        loc msg (lmem_fpri ~refcount:true "  ") x in
    let check_successor (d: sv): unit =
      if not (SvMap.mem d x.lm_mem) then
        report (F.asprintf "successor not in the graph %a" sv_fpr d) in
    let check_bound (bnd: Bounds.t): unit =
      if not (Bounds.t_is_const bnd) then
        Log.fatal_exn "non constant bound: %a" Bounds.t_fpr bnd in
    let check_offset (off: Offs.t): unit =
      if not (Offs.t_is_const off) then
        Log.fatal_exn "non constant offset: %a" Offs.t_fpr off in
    (* - lm_refcnt and lm_mem should have the same numbers of elements *)
    if SvMap.cardinal x.lm_refcnt != SvMap.cardinal x.lm_mem then
      Log.fatal_exn "refcount and abstract memory mismatch";
    (* - all references to successors should belong to the graph *)
    let prevs, setvs, seqvs =
      let f i j a =
        let om = SvMap.find_val SvMap.empty i a in
        let ov = SvMap.find_val 0 j om in
        SvMap.add i (SvMap.add j (ov + 1) om) a in
      let f_l_call_set (lc: l_call) (s: SvSet.t): SvSet.t =
        List.fold_left (fun s i -> SvSet.add i s) s lc.lc_setargs in
      let f_l_call_seq (lc: l_call) (s: SvSet.t): SvSet.t =
        List.fold_left (fun s i -> SvSet.add i s) s lc.lc_seqargs in
      SvMap.fold
        (fun i ln (accp, accsetvs, accseqvs) ->
          match ln.ln_e with
          | Lhemp -> (accp, accsetvs, accseqvs)
          | Lhpt mc ->
              Block_frag.iter_bound check_bound mc;
              let accp =
                Block_frag.fold_base
                  (fun _ pe acc ->
                    check_offset (Offs.of_size pe.pe_size);
                    check_offset (snd pe.pe_dest);
                    f (fst pe.pe_dest) i acc
                  ) mc accp in
              accp, accsetvs, accseqvs
          | Lhlist c -> accp, f_l_call_set c accsetvs, f_l_call_seq c accseqvs
          | Lhlseg (c, d, c') ->
              check_successor d;
              f d i accp, f_l_call_set c accsetvs, f_l_call_seq c accseqvs
        ) x.lm_mem (SvMap.empty, SvSet.empty, SvSet.empty) in
    (* - consistency of the COLVs *)
    let colvs = SvMap.fold (fun v _ -> SvSet.add v) x.lm_colvkind SvSet.empty in
    SvGen.sanity_check "COLV occurrences" colvs x.lm_colvkey;
    (* - all root COLVs should be known COLVs *)
    if not (SvSet.subset x.lm_colvroots colvs) then
      Log.fatal_exn "some unknown COLV roots";
    (* - all used COLVs should known *)
    if not (SvSet.subset (SvSet.union setvs seqvs) colvs) then
      Log.fatal_exn "some unknown COLV uses";
    (* - consistency between prevs and refcount, and edges *)
    SvMap.iter
      (fun i p ->
        let cp = SvMap.find_val SvMap.empty i prevs in
        SvMap.iter
          (fun j n ->
            let cn = SvMap.find_val 0 j cp in
            if cn != n then
              let msg = F.asprintf "[%a,%a,%d!=%d]" sv_fpr i sv_fpr j cn n in
              report ("refcount set mismatch\n     "^msg)
          ) p
      ) x.lm_refcnt;
    (* - consistency of dangle: should contain exactly non reachable SVs *)
    let reach = lmem_reach x x.lm_roots in
    if SvSet.inter reach x.lm_dangle != SvSet.empty then
      report "reachable SVs in dangle";
    let n_c = SvSet.cardinal reach + SvSet.cardinal x.lm_dangle
    and n_e = SvMap.cardinal x.lm_mem in
    if n_c != n_e then
      report (F.asprintf "dangle SVs mismatch\n -r: {%a}\n -d: {%a}"
                (SvSet.t_fpr ",") reach (SvSet.t_fpr ",") x.lm_dangle);
    ( )

(** Management of SVs *)
let sve_sync_bot_up (m: lmem): lmem * svenv_mod =
  { m with lm_svemod = Dom_utils.svenv_empty }, m.lm_svemod

(** Symbolic variables operations *)
(* Membership and find *)
let sv_mem (i: sv) (x: lmem): bool = SvMap.mem i x.lm_mem
let sv_find (i: sv) (x: lmem): lnode =
  try SvMap.find i x.lm_mem
  with
  | Not_found ->
      Log.info "sv_find %a fails:\n%a" sv_fpr i (lmem_fpri "  ") x;
      Log.fatal_exn "node %a not found" sv_fpr i
(* Find all sv predecessors. this includes backward pointer in inductive
  predicate calls *)
let sv_pred (i: sv) (x: lmem): SvSet.t =
  let prevs =
    match SvMap.find_opt i x.lm_refcnt with
    | None -> SvSet.empty
    | Some refcnt ->
        SvMap.fold
          (fun pred _ acc -> SvSet.add pred acc)
          refcnt SvSet.empty in
  let prevs =
    SvMap.fold
      (fun pred node acc ->
        match node.ln_e with
        | Lhemp | Lhpt _ -> acc
        | Lhlist lc | Lhlseg (lc, _, _) ->
            match lc.lc_def.ld_prevpar with
            | None -> acc
            | Some back_ptr ->
                if List.nth lc.lc_ptrargs back_ptr = i then
                  SvSet.add pred acc
                else
                  acc)
      x.lm_mem prevs in
  prevs

(* Checking whether an SV is dangling *)
let sv_is_dangle (i: sv) (x: lmem): bool = SvSet.mem i x.lm_dangle
(* Making an SV dangling (i.e., could be quickly removed) *)
let rec sv_dangle_add (i: sv) (x: lmem): lmem =
  if SvSet.mem i x.lm_dangle then x
  else
    let x = { x with lm_dangle = SvSet.add i x.lm_dangle } in
    let r = lnode_reach (sv_find i x) in
    SvSet.fold sv_dangle_add_check r x
and sv_dangle_add_check (i: sv) (x: lmem): lmem =
  if SvSet.mem i x.lm_roots then x
  else
    let old_prevs =
      try SvMap.find i x.lm_refcnt
      with Not_found -> Log.fatal_exn "NF in move_dangle %a" sv_fpr i in
    if debug_module then
      Log.debug "old_prevs: {%a}" (SvMap.t_fpr ", " int_fpr) old_prevs;
    let b_subset =
      SvMap.fold
        (fun i _ acc -> acc && SvSet.mem i x.lm_dangle) old_prevs true in
    if b_subset then sv_dangle_add i x (* i is dangling *)
    else x
(* Removing an SV from dangling (i.e., should be preserved) *)
let rec sv_dangle_rem (i: sv) (x: lmem): lmem =
  if SvSet.mem i x.lm_dangle then
    let x = { x with lm_dangle = SvSet.remove i x.lm_dangle } in
    let r = lnode_reach (sv_find i x) in
    SvSet.fold sv_dangle_rem r x (* also move successors out of dangling *)
  else x
(* Information extraction *)
let sv_kind (i: sv) (x: lmem): region_kind =
  match (sv_find i x).ln_e with
  | Lhemp    -> Kemp
  | Lhpt _   -> Kpt
  | Lhlist _ -> Kind
  | Lhlseg _ -> Kseg
(* Addition *)
let sv_add ?(root:bool = false) ?(prio:bool = false)
    (i: sv) (nt: ntyp) (x: lmem): lmem =
  assert (not (SvMap.mem i x.lm_mem));
  let lnode_empty = { ln_i     = i;
                      ln_e     = Lhemp;
                      ln_typ   = nt;
                      ln_prio  = prio;
                      ln_odesc = None } in
  let svm = { x.lm_svemod with svm_add = PMap.add i nt x.lm_svemod.svm_add } in
  { x with
    lm_nkey   = SvGen.use_key x.lm_nkey i;
    lm_mem    = SvMap.add i lnode_empty x.lm_mem;
    lm_svemod = svm;
    lm_refcnt = SvMap.add i SvMap.empty x.lm_refcnt;
    lm_dangle = if root then x.lm_dangle else SvSet.add i x.lm_dangle;
    lm_roots  = if root then SvSet.add i x.lm_roots else x.lm_roots; }
let sv_add_fresh ?(root:bool = false) (nt: ntyp) (x: lmem): sv * lmem =
  let _, i = SvGen.gen_key x.lm_nkey in
  i, sv_add ~root:root i nt x
let sv_rem (i: sv) (x: lmem): lmem =
  if debug_module then Log.debug "sv_rem: %a" sv_fpr i;
  if SvSet.mem i x.lm_roots then Log.fatal_exn "sv_rem called on root node";
  let n = sv_find i x in
  if not (n.ln_e = Lhemp) then Log.warn "sv_rem: SV region is non empty";
  { x with
    lm_mem    = SvMap.remove i x.lm_mem;
    lm_nkey   = SvGen.release_key x.lm_nkey i;
    lm_dangle = SvSet.remove i x.lm_dangle;
    lm_refcnt = SvMap.remove i x.lm_refcnt;
    lm_svemod = svenv_rem i x.lm_svemod }

(* Collects all SVs *)
let sv_get_all (x: lmem): SvSet.t =
  SvMap.fold (fun i _ acc -> SvSet.add i acc) x.lm_mem SvSet.empty
(* Check whether SV is the address of an inductive or segment predicate *)
let sv_is_ind (i: sv) (x: lmem): bool =
  match (sv_find i x).ln_e with
  | Lhemp | Lhpt _ -> false
  | Lhlist _ | Lhlseg _ -> true
(* Operations on roots *)
let sv_root (i: sv) (x: lmem): lmem =
  sv_dangle_rem i { x with lm_roots = SvSet.add i x.lm_roots }
let sv_unroot (i: sv) (x: lmem): lmem =
  if not (SvSet.mem i x.lm_roots) then
     Log.fatal_exn "sv_unroot not called on root";
  sv_dangle_add_check i { x with lm_roots = SvSet.remove i x.lm_roots }

(* Get offsets of maya elements *)
let get_maya_off (i: sv) (x: lmem): int list =
  let ln = SvMap.find i x.lm_mem in
  let lc =
    match ln.ln_e with
    | Lhlist l
    | Lhlseg (l,_,_) -> l
    | _ -> Log.fatal_exn "not a inductive" in
  lc.lc_def.ld_m_offs

(** Management of COLVs *)
let colv_add_fresh ?(root: bool = false) (k: col_par_type) (x: lmem)
    : sv * lmem =
  let kg, colv = SvGen.gen_key x.lm_colvkey in
  let roots = if root then SvSet.add colv x.lm_colvroots else x.lm_colvroots in
  let x =
    { x with
      lm_colvkey   = kg;
      lm_colvroots = roots;
      lm_colvkind  = SvMap.add colv k x.lm_colvkind } in
  colv, x
let colv_add ?(root: bool = false) (colv: sv) (ck: col_kind) (x: lmem): lmem =
  let roots = if root then SvSet.add colv x.lm_colvroots else x.lm_colvroots in
  { x with
    lm_colvkey   = SvGen.use_key x.lm_colvkey colv;
    lm_colvroots = roots;
    lm_colvkind  = SvMap.add colv (col_kind_to_par_type ck) x.lm_colvkind }
let colv_mem (setv: sv) (x: lmem): bool = SvGen.key_is_used x.lm_colvkey setv
let colv_rem (colv: sv) (x: lmem): lmem =
  { x with
    lm_colvkey   = SvGen.release_key x.lm_colvkey colv;
    lm_colvroots = SvSet.remove colv x.lm_colvroots ;
    lm_colvkind  = SvMap.remove colv x.lm_colvkind }
let colv_get_all (lm: lmem): col_kind SvMap.t =
  SvMap.map col_par_type_to_kind lm.lm_colvkind
let colv_get_roots (lm: lmem): col_kind SvMap.t =
  SvMap.filter (fun colv _ -> SvSet.mem colv lm.lm_colvroots) (colv_get_all lm)
let colv_is_root (lm: lmem) (colv: sv): bool =
  SvSet.mem colv lm.lm_colvroots
let colv_roots (lm: lmem): SvSet.t = lm.lm_colvroots
let colv_kind (cv: sv) (lm: lmem): col_kind =
  try col_par_type_to_kind (SvMap.find cv lm.lm_colvkind)
  with Not_found -> Log.fatal_exn "colv_kind: Not_found"

(** Management of Set COLVs *)
let setv_type (lm: lmem) (setv: sv): set_par_type option =
  try
    match SvMap.find setv lm.lm_colvkind with
    | Ct_set k -> k
    | Ct_seq _ -> Log.fatal_exn "setv_type"
  with Not_found -> None
let setv_filter_set (m: col_kind SvMap.t): SvSet.t =
  SvMap.fold (fun cv k acc -> if k = CK_set then SvSet.add cv acc else acc)
    m SvSet.empty
let setv_set (lm: lmem): SvSet.t =
  setv_filter_set (colv_get_all lm)
let setv_roots (lm: lmem): SvSet.t =
  setv_filter_set (colv_get_roots lm)

(** Management of Seq COLVs *)
let seqv_type (lm: lmem) (seqv: sv): seq_par_type option =
  try
    match SvMap.find seqv lm.lm_colvkind with
    | Ct_set _ -> Log.fatal_exn "seqv_type"
    | Ct_seq k -> k
  with Not_found -> None
let seqv_filter_set (m: col_kind SvMap.t): SvSet.t =
  SvMap.fold (fun cv k acc -> if k = CK_seq then SvSet.add cv acc else acc)
    m SvSet.empty
let seqv_set (lm: lmem): SvSet.t =
  seqv_filter_set (colv_get_all lm)
let seqv_roots (lm: lmem): SvSet.t =
  seqv_filter_set (colv_get_roots lm)

(** Management of reference counts and back pointers *)
(* Effect of adding a predicate on prev, involving next *)
let add_refcount (prev: sv) (next: sv) (x: lmem): lmem =
  let old_prevs =
    try SvMap.find next x.lm_refcnt
    with
    | Not_found ->
        Log.info "shape:\n%a" (lmem_fpri "  ") x;
        Log.fatal_exn "NF in add_refcount %a" sv_fpr next in
  let old_count = try SvMap.find prev old_prevs with Not_found -> 0 in
  let new_count = old_count + 1 in
  let new_prevs = SvMap.add prev new_count old_prevs in
  let x = { x with lm_refcnt = SvMap.add next new_prevs x.lm_refcnt; } in
  if SvSet.mem prev x.lm_dangle then x
  else sv_dangle_rem next x
(* Effect of removing a predicate on prev, involving next *)
let rem_refcount (prev: sv) (next: sv) (x: lmem): lmem =
  if debug_module then
    Log.debug "destroying: %a -> %a" sv_fpr prev sv_fpr next;
  let old_prevs =
    try SvMap.find next x.lm_refcnt
    with Not_found ->
      Log.info "rem_rc: %a,%a\n%a" sv_fpr prev sv_fpr next (lmem_fpri "  ") x;
      Log.fatal_exn "NF in rem_refcount %a" sv_fpr next in
  let old_count = SvMap.find_val 0 prev old_prevs in
  let new_count = old_count - 1 in
  let new_prevs =
    if new_count <= 0 then SvMap.remove prev old_prevs
    else SvMap.add prev new_count old_prevs in
  let x = { x with lm_refcnt = SvMap.add next new_prevs x.lm_refcnt } in
  if new_prevs = SvMap.empty
      && (not (SvSet.mem next x.lm_roots)) then sv_dangle_add next x
  else sv_dangle_add_check next x
(* Effect of a series of predicate removals *)
let rem_refcounts (prev: sv) (nexts: SvSet.t) (x: lmem): lmem =
  SvSet.fold (fun next -> rem_refcount prev next) nexts x

(** Memory block operations *)

(* Existence of a points-to edge *)
let pt_edge_mem ((i,o): Offs.svo) (x: lmem): bool =
  let nsrc = sv_find i x in
  match nsrc.ln_e with
  | Lhemp | Lhlist _ | Lhlseg _ -> false
  | Lhpt mc -> Block_frag.mem (Bounds.of_offs o) mc

(* Mark a node with priority level for isle/join *)
let mark_prio (lv: sv) (x: lmem): lmem =
  let lv_node = SvMap.find lv x.lm_mem in
  let lmem = SvMap.add lv { lv_node with ln_prio = true } x.lm_mem in
  { x with lm_mem = lmem }

(* Finding a pt_edge, for internal use (no size used, so cannot be used to
 *  dereference an edge) *)
let pt_edge_find
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (x: lmem): pt_edge =
  let nsrc = sv_find is x in
  let bnd = Bounds.of_offs os in
  if nsrc.ln_typ != Ntaddr then
    Log.info "Issue in pt_edge_find; type of src is %a"
      ntyp_fpr nsrc.ln_typ;
  match nsrc.ln_e with
  | Lhemp | Lhlist _ | Lhlseg _ ->
      Log.fatal_exn "pt_edge_find: does not exist (1)"
  | Lhpt mc ->
      try Block_frag.find_addr_sat sat bnd mc
      with Not_found -> Log.fatal_exn "pt_edge_find: does not exist (2)"

(* Splitting, appending points-to edges *)
let pt_edge_split (sat: n_cons -> bool) ((is,os): bnode)
    (mid_point: Offs.size) (x: lmem): lmem =
  let debug_module = true in
  if debug_module then
    Log.debug "call to pt_edge_split\n - (%a,%a)\n - %a\n%a" sv_fpr is
      Bounds.t_fpr os Offs.size_fpr mid_point (lmem_fpri "   ") x;
  let old_pt = pt_edge_find sat (is, Bounds.to_offs os) x in
  assert (Offs.size_leq sat mid_point old_pt.pe_size);
  let old_src = sv_find (fst old_pt.pe_dest) x in
  let sz0 = mid_point
  and sz1 = Offs.size_sub_size old_pt.pe_size mid_point in
  let n0, x = sv_add_fresh old_src.ln_typ x in
  let n1, x = sv_add_fresh old_src.ln_typ x in
  let pe_0 = { pe_size = sz0 ; pe_dest = n0, Offs.zero }
  and pe_1 = { pe_size = sz1 ; pe_dest = n1, Offs.zero } in
  let nsrc = sv_find is x in
  (* node_assert_placed ? *)
  let bndmid = Bounds.add_size os mid_point in
  let bndhi  = Bounds.add_size os old_pt.pe_size in
  if debug_module then
    Log.debug "pt_edge_split:\n - pos %a\n - siz %a\n - res %a"
      Bounds.t_fpr os Offs.size_fpr old_pt.pe_size Bounds.t_fpr bndhi;
  let nblock =
    let oblock =
      match nsrc.ln_e with
      | Lhemp -> Block_frag.create_empty os
      | Lhpt block -> block
      | _ -> Log.fatal_exn "pt_edge_split: improper edge" in
    Block_frag.split_sat sat os bndmid bndhi pe_0 pe_1 oblock in
  let nsrc = { nsrc with ln_e = Lhpt nblock } in
  let x =
    rem_refcount is (fst old_pt.pe_dest)
      (add_refcount is n0
         (add_refcount is n1
            { x with lm_mem = SvMap.add is nsrc x.lm_mem })) in
  x
let pt_edge_block_append
    ?(nochk: bool = false) (* de-activates check that bounds coincide (join) *)
    ((is, bnd): bnode) (pe: pt_edge) (x: lmem): lmem =
  assert (Bounds.t_is_const bnd);
  sanity_check "before-append" x;
  let nsrc = sv_find is x in
  let oblock =
    match nsrc.ln_e with
    | Lhemp -> Block_frag.create_empty bnd
    | Lhpt block -> block
    | _ -> Log.fatal_exn "pt_edge_block_append: improper edge" in
  let nblock =
    Block_frag.append_tail ~nochk: nochk bnd
      (Bounds.add_size bnd pe.pe_size) pe oblock in
  let nsrc = { nsrc with ln_e = Lhpt nblock } in
  let x = { x with lm_mem = SvMap.add is nsrc x.lm_mem } in
  let x = add_refcount is (fst pe.pe_dest) x in
  sanity_check "after-append" x;
  x
(* Removal of a bunch of points-to edges from a node *)
let pt_edge_block_destroy ?(remrc: bool = true) (is: sv) (x: lmem): lmem =
  sanity_check "block_destroy,before" x;
  let n = sv_find is x in
  match n.ln_e with
  | Lhpt _ ->
      let nn = { n with ln_e = Lhemp } in
      let posts = lnode_reach n in
      if debug_module then
        Log.debug "before block_destroy %a: { %a }\n%a" sv_fpr is
          (SvSet.t_fpr ";") posts (lmem_fpri ~refcount:true "  ") x;
      let x = { x with lm_mem = SvMap.add is nn x.lm_mem } in
      let x = if remrc then rem_refcounts is posts x else x in
      if debug_module then
        Log.debug "after block_destroy %a: { %a }\n%a" sv_fpr is
          (SvSet.t_fpr ";") posts (lmem_fpri ~refcount: true "  ") x;
      sanity_check "block_destroy,after" x;
      x
  | _ -> Log.fatal_exn "pt_edge_block_destroy: not a points-to edge"
(* Try to decide if an offset range is in a single points-to edge
 *  of a fragmentation, and if so, return its destination *)
let pt_edge_find_interval
    (sat: n_cons -> bool)
    (is: sv) (* node representing the base address *)
    (min_off: Offs.t) (* minimum offset of the range being looked for *)
    (size: int)       (* size of the range being looked for *)
    (x: lmem): pt_edge option =
  assert (Offs.t_is_const min_off);
  match (sv_find is x).ln_e with
  | Lhemp | Lhlist _ | Lhlseg _ -> None
  | Lhpt mc ->
      let bnd_low = Bounds.of_offs min_off in
      let bnd_hi  = Bounds.of_offs (Offs.add_int min_off size) in
      try Some (Block_frag.find_chunk_sat sat bnd_low bnd_hi mc)
      with Not_found -> None
(* Experimental algorithm for edge search *)
let pt_edge_search
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    (mc: pt_edge Block_frag.t) (bnd: Bounds.t) (sz: Offs.size)
    : (Bounds.t * Offs.size) option =
  Block_frag.search_sat sat (fun pe -> pe.pe_size) mc bnd sz
(* Retrieval algorithm that encapsulates the search for extract and replace *)
exception Retry of lmem
let pt_edge_retrieve
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,bnd): bnode) (mc: pt_edge Block_frag.t) (old_sz: Offs.size)
    (x: lmem): pt_edge =
  assert (Bounds.t_is_const bnd);
  try Block_frag.find_addr_sat sat bnd mc
  with
  | Not_found ->
      (* Search for an edge containing the one being searched for *)
      match pt_edge_search sat mc bnd old_sz with
      | Some (fo, fsz) ->
          let nt = pt_edge_split sat (is,fo) fsz x in
          raise (Retry nt)
      | None -> Log.fatal_exn "NF in pt_edge_retrieve, no split"

(* Replacement of a points-to edge by another *)
let pt_edge_replace
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (pte: pt_edge): lmem -> lmem =
  assert (Offs.t_is_const os);
  let bnd = Bounds.of_offs os in
  let rec aux (x: lmem): lmem =
    sanity_check "before-replace" x;
    let n = sv_find is x in
    match n.ln_e with
    | Lhemp -> Log.fatal_exn "pt_edge_repace: empty"
    | Lhpt mc ->
        begin
          try
            let old_pe = pt_edge_retrieve sat (is,bnd) mc pte.pe_size x in
            match Offs.size_order sat pte.pe_size old_pe.pe_size with
            | Some c ->
                if c = 0 then (* i.e., pte.pe_size = old_pe.pe_size *)
                  (* the edges found matches exactly the region to overwrite *)
                  let nmc = Block_frag.replace_sat sat bnd pte mc in
                  let nn = { n with ln_e = Lhpt nmc } in
                  add_refcount is (fst pte.pe_dest)
                    (rem_refcount is (fst old_pe.pe_dest)
                       { x with lm_mem = SvMap.add is nn x.lm_mem })
                else if c < 0 then (* i.e., pte.pe_size < old_pe.pe_size then *)
                  (* only part of the edge needs be overwritten;
                   * we should split it *)
                  aux (pt_edge_split sat (is,bnd) pte.pe_size x)
                else (* c > 0, i.e., pte.pe_size > old_pe.pe_size *)
                  (* we would have to merging edges together... *)
                  Log.fatal_exn "pt_edge_replace: edges merging unsupported"
            | None ->
                if Offs.size_leq sat pte.pe_size old_pe.pe_size then
                  aux (pt_edge_split sat (is,bnd) pte.pe_size x)
                else Log.fatal_exn "incomparable sizes"
          with Retry lm ->
            (* for now, we just raise an exception, but "aux lm" should do *)
            (*aux lm*)
            Log.fatal_exn "pt_edge_replace needs to do a retry (untested)"
        end
    | Lhlist _ -> raise (Unfold_request (Uloc_main is, Udir_fwd))
    | Lhlseg _ -> raise (Unfold_request (Uloc_main is, Udir_fwd)) in
  fun x ->
    let x = aux x in
    sanity_check "after-replace" x;
    x
(* Extraction of a points to edge: reads, after split if necessary *)
let pt_edge_extract
    (sat: n_cons -> bool) (* checking of integer satisfiability constraints *)
    ((is,os): Offs.svo) (isz: int): lmem -> lmem * pt_edge =
  assert (Offs.t_is_const os);
  let bnd = Bounds.of_offs os in
  let sz = Offs.size_of_int isz in
  let rec aux (x: lmem): lmem * pt_edge =
    sanity_check "before-extract" x;
    let n = sv_find is x in
    if !flag_dbg_graph_blocks && false then
      Log.force "Call to pt_edge_extract.aux:  (%a,%a)" sv_fpr is Offs.t_fpr os;
    match n.ln_e with
    | Lhemp ->
        (* Trying to find backward unfolding *)
        SvMap.iter
          (fun sv node ->
            match node.ln_e with
            | Lhemp | Lhpt _ | Lhlist _ -> ()
            | Lhlseg (lc, dest, lsegc)->
                match lsegc.lc_def.ld_prevpar with
                | None -> ()
                | Some i ->
                    if List.nth lsegc.lc_ptrargs i = n.ln_i then
                      raise (Unfold_request (Uloc_main sv, Udir_bwd))
                    else ()
          ) x.lm_mem;
        raise (No_edge "pt_edge_extract: empty")
    | Lhpt mc ->
        begin
          try
            let pte = pt_edge_retrieve sat (is,bnd) mc sz x in
            match Offs.size_order sat pte.pe_size sz with
            | Some c ->
                if c = 0 then (* equality, pte.pe_size = sz *)
                  (* the edge found matches the size of the deref cell *)
                  x, pte
                else if c > 0 then (* pte.pe_size > sz *)
                  (* only part of the edge should be read; we should split *)
                  begin
                    aux (pt_edge_split sat (is,bnd) sz x)
                  end
                else (* c < 0, i.e., pte.pe_size < sz *)
                  Log.fatal_exn "size mismatch"
            | None -> Log.fatal_exn "incomparable sizes"
          with Retry lm -> aux lm
        end
    | Lhlist _ -> raise (Unfold_request (Uloc_main is, Udir_fwd))
    | Lhlseg _ -> raise (Unfold_request (Uloc_main is, Udir_fwd)) in
  aux

(* Addition of a list edge *)
let list_edge_add (i: sv) (ld: l_call) (x: lmem): lmem =
  let ln = sv_find i x in
  match ln.ln_e with
  | Lhemp ->
      let ln = { ln with
                 ln_e     = Lhlist ld;
                 ln_odesc = Some ld.lc_def } in
      { x with lm_mem = SvMap.add i ln x.lm_mem }
  | Lhpt _ | Lhlist _ | Lhlseg _ ->
      Log.fatal_exn "list_edge_add: %a\n%a" sv_fpr i (lmem_fpri "  ") x
(* Removal of a list edge *)
let list_edge_rem (i: sv) (x: lmem): lmem =
  let ln = sv_find i x in
  match ln.ln_e with
  | Lhlist _ ->
      let ln = { ln with ln_e = Lhemp } in
      { x with lm_mem = SvMap.add i ln x.lm_mem }
  | Lhemp | Lhpt _ | Lhlseg _ -> Log.fatal_exn "list_edge_rem"

(* Addition of a segment edge *)
let lseg_edge_add (src: sv) (dst: sv) (lc: l_call) (lc': lseg_call) (x: lmem)
  : lmem =
  let ln = sv_find src x in
  match ln.ln_e with
  | Lhemp ->
      let ln = { ln with
                 ln_e     = Lhlseg (lc, dst, lc');
                 ln_odesc = Some lc.lc_def; } in
      add_refcount src dst { x with lm_mem = SvMap.add src ln x.lm_mem }
  | Lhpt _ | Lhlist _ | Lhlseg _ -> Log.fatal_exn "lseg_edge_add"
(* Removal of a segment edge *)
let lseg_edge_rem (i: sv) (x: lmem): lmem =
  let ln = sv_find i x in
  match ln.ln_e with
  | Lhlseg (_, dst, _) ->
      let ln = { ln with ln_e = Lhemp } in
      rem_refcount i dst { x with lm_mem = SvMap.add i ln x.lm_mem }
  | Lhemp | Lhpt _ | Lhlist _ -> Log.fatal_exn "lseg_edge_rem"

let lseg_2list (h: sv) (x: lmem): lmem =
  let ln = sv_find h x in
  match ln.ln_e with
  | Lhlseg (ld, dst, _) ->
      let ln = { ln with ln_e = Lhlist ld} in
      let lm =
        rem_refcount h dst { x with lm_mem = SvMap.add h ln x.lm_mem } in
      let lm = sv_unroot dst lm in
      sv_rem dst lm
  | Lhemp | Lhpt _ | Lhlist _ -> Log.fatal_exn "lseg_2list"

(* Number of remaining edges *)
let num_edges (x: lmem): int =
  SvMap.fold
    (fun _ ln acc ->
      match ln.ln_e with
      | Lhemp -> acc
      | Lhpt _ | Lhlist _ | Lhlseg _ -> 1 + acc
    ) x.lm_mem 0

(** Garbage collection support *)
(* Removal of all nodes not reachable from a set of roots
 * Returns the new memory state and the set of removed setv *)
let gc (roots: sv Aa_sets.t) (x: lmem): lmem * SvSet.t =
  let do_full_gc = false in
  sanity_check "before-gc" x;
  let l_call_col_setv (lc: l_call): SvSet.t =
    List.fold_left (fun acc i -> SvSet.add i acc)
      SvSet.empty lc.lc_setargs in
  (*
  let l_call_col_seqv (lc: l_call): SvSet.t =
    List.fold_left (fun acc i -> SvSet.add i acc)
      SvSet.empty lc.lc_seqargs in
  *)
  (* internal function to dispose a node *)
  let sv_dispose (increm: bool) (i: sv) (x: lmem)
      : lmem * SvSet.t =
    if increm then incr Statistics.gc_stat_incr_rem
    else incr Statistics.gc_stat_full_rem;
    let x, s =
      match (sv_find i x).ln_e with
      | Lhemp -> x, SvSet.empty
      | Lhpt _ -> pt_edge_block_destroy i x, SvSet.empty
      | Lhlist lc ->
          list_edge_rem i x, l_call_col_setv lc
      | Lhlseg (lc, _, _) ->
          lseg_edge_rem i x, l_call_col_setv lc in
    sv_rem i x, s in
  (* internal dispose function that makes no sanity check;
   *  (it is good for the removal of many svs in one shot)
   * it also returns the sets of svs,setvs that are not used anymore *)
  let sv_dispose_series (increm: bool) (i: sv) (lm: lmem)
      : lmem * SvSet.t * SvSet.t =
    if increm then incr Statistics.gc_stat_incr_rem
    else incr Statistics.gc_stat_full_rem;
    let n = sv_find i lm in
    let svs = lnode_reach n in
    let setvs =
      match n.ln_e with
      | Lhemp | Lhpt _ -> SvSet.empty
      | Lhlist lc | Lhlseg (lc, _, _) -> l_call_col_setv lc in
    let svemod =
      if SvSet.mem i lm.lm_roots then lm.lm_svemod
      else svenv_rem i lm.lm_svemod in
    let lm =
      { lm with
        lm_nkey   = SvGen.release_key lm.lm_nkey i;
        lm_mem    = SvMap.remove i lm.lm_mem;
        lm_dangle = SvSet.remove i lm.lm_dangle;
        lm_svemod = svemod;
        lm_refcnt = SvMap.remove i lm.lm_refcnt} in
    lm, svs, setvs in
  (* Incremental GC *)
  let incr_gc x =
    if debug_module then
      Log.debug "doing incr-GC";
    incr Statistics.gc_stat_incr_num;
    let x, svs, setvs =
      SvSet.fold
        (fun i (x, asvs, asetvs) ->
          let x, svs, setvs = sv_dispose_series true i x in
          x, SvMap.add i svs asvs, SvSet.union setvs asetvs
        ) x.lm_dangle (x, SvMap.empty, SvSet.empty) in
    if debug_module then Log.debug "middle of incr-GC";
    SvMap.fold
      (fun prev nexts ->
        SvSet.fold
          (fun next x ->
            try
              let old_prevs = SvMap.find next x.lm_refcnt in
              let old_count = SvMap.find_val 0 prev old_prevs in
              if old_count <= 0 then
                Log.fatal_exn "lmem_gc: reached 0 [%a,%a]\n%a"
                  sv_fpr prev sv_fpr next (lmem_fpri "  ") x;
              let new_count = old_count - 1 in
              let new_prevs =
                if new_count <= 0 then SvMap.remove prev old_prevs
                else SvMap.add prev new_count old_prevs in
              { x with lm_refcnt = SvMap.add next new_prevs x.lm_refcnt }
            with Not_found -> x
          ) nexts
      ) svs x, setvs in
  (* Full GC *)
  let full_gc x =
    if debug_module then Log.debug "doing full-GC";
    incr Statistics.gc_stat_full_num;
    (* Compute reachability *)
    let reach =
      let r = ref SvSet.empty
      and todos = ref roots in
      while !todos != Aa_sets.empty do
        let i = Aa_sets.min_elt !todos in
        r := SvSet.add i !r;
        todos := Aa_sets.remove i !todos;
        let ln = SvMap.find_err "List GC" i x.lm_mem in
        SvSet.iter
          (fun i ->
            if not (SvSet.mem i !r) then todos := Aa_sets.add i !todos)
          (lnode_reach ln)
      done;
      !r in
    (* Remove unreachable nodes *)
    let nremoved =
      SvMap.fold
        (fun i _ acc ->
          if SvSet.mem i reach then acc else SvSet.add i acc
        ) x.lm_mem SvSet.empty in
    let x, s =
      SvSet.fold
        (fun i (x, s_acc) ->
          let x, s = sv_dispose false i x in
          x, SvSet.union s s_acc
        ) nremoved (x, SvSet.empty) in
    sanity_check "after-gc" x; (* to do for set *)
    x, s in
  let x, s = incr_gc x in
  if debug_module then Log.debug "incr-GC done";
  if do_full_gc then
    let x, s1 = full_gc x in
    x, SvSet.union s s1
  else x, s

(** Inductive set parameters construction *)
(* - unfold a seg or an ind edge presented by lc: l_call in the right side
 * - with a new segment
 * - and a new seg or an new ind edge
 * - also returns the set of removed Set COLVs
 * - and          the set of all new Set COLVs *)
let gen_ind_setpars (lm: lmem) (lc: l_call)
    : lmem * l_call * lseg_call * l_call * (set_cons list)
      * SvSet.t * SvSet.t =
  let l_paras =
    match lc.lc_def.ld_set with
    | None -> []
    | Some si -> si.s_params in
  let lm, lseg_args, l_setargs, c, setvs_rem, setvs_add =
    List.fold_left2
      (fun (lm, lseg_args, l_args, c, acc_rem, acc_add) a k ->
        let seg_arg, lm = colv_add_fresh (Ct_set (Some k)) lm in
        let l_arg, lm   = colv_add_fresh (Ct_set (Some k)) lm in
        let c =
          if k.st_const then
            let c = S_eq (S_var l_arg, S_var a) :: c in
            S_eq (S_var seg_arg, S_var a) :: c
          else if k.st_head then
            S_eq (S_var a, S_uplus (S_var seg_arg, S_var l_arg)) :: c
          else Log.todo_exn "get_ind_setpars: unhandled kind" in
        (lm, seg_arg :: lseg_args, l_arg :: l_args, c,
         SvSet.add a acc_rem,
         SvSet.add seg_arg (SvSet.add l_arg acc_add))
      )
      (lm, [ ], [ ], [ ], SvSet.empty, SvSet.empty)
      (List.rev lc.lc_setargs)
      (List.rev l_paras) in
  let lsegc =
    assert (List.length lc.lc_ptrargs = 0);
    { lc_def = lc.lc_def ; lc_ptrargs = lc.lc_ptrargs } in
  lm, { lc with lc_setargs = lseg_args }, lsegc ,
  { lc with lc_setargs = l_setargs }, c, setvs_rem, setvs_add

(* Splitting an inductive predicate into a pair of calls;
 * this function takes care of the parameters and prepares a list of
 * set/seq constraints that should also be taken into account to precisely
 * account for the COLV parameter kinds (const, head, etc) *)
let split_indpredpars (lc: l_call) (lm: lmem)
    : lmem * l_call * lseg_call * l_call * set_cons list * seq_cons list =
  (* first we add the ptr arguments *)
  let ptr_params, lm =
    List.fold
      (fun (new_ptr, lm) _ ->
        let ptr, lmem = sv_add_fresh Ntaddr lm in
        ptr :: new_ptr, lm)
      ([], lm) lc.lc_ptrargs in
  (* Then, we add the set arguments *)
  let set_params =
    match lc.lc_def.ld_set with
    | None -> []
    | Some si -> si.s_params in
  let lm, l0_set, l1_set, set_cons =
    let rec aux ((lm, call0, call1, set_cons) as acc) lcl set_params =
      match lcl, set_params with
      | [ ], [ ] -> acc
      | i :: lcl0, k :: set_params0 ->
          (* generate two keys in the right *)
          let i0, lm = colv_add_fresh (Ct_set (Some k)) lm in (* mapped to il *)
          let i1, lm = colv_add_fresh (Ct_set (Some k)) lm in (* remaining *)
          let setctr =
            if k.st_const then
              [ S_eq (S_var i, S_var i0) ; S_eq (S_var i, S_var i1) ]
            else if k.st_add || k.st_head then
              [ S_eq (S_var i, S_uplus (S_var i0, S_var i1)) ]
            else Log.todo_exn "unhandled kind-0" in
          aux (lm, i0 :: call0, i1 :: call1, setctr @ set_cons) lcl0 set_params0
      | _, _ ->
          Log.fatal_exn "split_indpredpars: set pars of distinct lengths" in
  aux (lm, [ ], [ ], [ ]) lc.lc_setargs set_params in
  (* Then, we add the seq arguments *)
  let seq_params =
    match lc.lc_def.ld_seq with
    | None -> []
    | Some si -> si.q_params in
  let lm, l0_seq, l1_seq, seq_cons =
    let rec aux ((lm, call0, call1, seq_cons) as acc) lcl seq_params =
      match lcl, seq_params with
      | [ ], [ ] -> acc
      | i :: lcl0, k :: seq_params0 ->
          (* generate two keys in the right *)
          let i0, lm = colv_add_fresh (Ct_seq (Some k)) lm in (* mapped to il *)
          let i1, lm = colv_add_fresh (Ct_seq (Some k)) lm in (* remaining *)
          let seqctr =
            if k.sq_const then
              [ Seq_equal (Seq_var i, Seq_var i0) ;  (* i = i0 *)
                Seq_equal (Seq_var i, Seq_var i1) ]  (* i = i1 *)
            else if k.sq_add || k.sq_head then
              [ Seq_equal (Seq_var i, Seq_Concat [Seq_var i0; Seq_var i1]) ]
              (* i = i0·i1 *)
            else Log.todo_exn "unhandled kind-0" in
          aux (lm, i0 :: call0, i1 :: call1, seqctr @ seq_cons) lcl0 seq_params0
      | _, _ ->
          Log.fatal_exn "split_indpredpars: seq pars of distinct lengths" in
    aux (lm, [ ], [ ], [ ]) lc.lc_seqargs seq_params in (* JG:TODO *)
  lm,
  { lc with lc_setargs = List.rev l0_set;
            lc_seqargs = List.rev l0_seq },
  { lc_def     = lc.lc_def ;
    lc_ptrargs = List.rev ptr_params },
  { lc with lc_ptrargs = List.rev ptr_params;
            lc_setargs = List.rev l1_set;
            lc_seqargs = List.rev l1_seq },
  set_cons,
  seq_cons

(* Get definition of the list in a memory *)
let get_def (lm: lmem): l_def=
  let opdef =
    SvMap.fold
      (fun _ ln acc->
        match ln.ln_e with
        | Lhlist lc
        | Lhlseg (lc, _, _) -> Some lc.lc_def
        | _ -> acc
      ) lm.lm_mem None in
  match opdef with
  | Some ldef -> ldef
  | None -> Log.fatal_exn "no ldef in lmem"

(** Reduction *)
(* rename an SV associated to an Lhemp node *)
let lmem_rename_sv (svf: sv) (svt: sv) (l: lmem): lmem =
  if SvSet.mem svf l.lm_roots then
    Log.fatal_exn "lmem_rename_sv: cannot remove root";
  let map = SvMap.singleton svf svt in
  let do_bound = Bounds.rename map in
  let do_nid (sv: sv): sv = if sv = svf then svt else sv in
  let do_pt_edge (pe: pt_edge): pt_edge =
    { pe with pe_dest = do_nid (fst pe.pe_dest), snd pe.pe_dest } in
  let do_ptr_args = List.map do_nid in
  let do_lcall (c: l_call): l_call =
    { c with lc_ptrargs = do_ptr_args c.lc_ptrargs } in
  let do_lsegcall (c: lseg_call): lseg_call =
    { c with lc_ptrargs = do_ptr_args c.lc_ptrargs } in
  let do_lheap_frag: lheap_frag -> lheap_frag = function
    | Lhemp -> Lhemp
    | Lhpt m -> Lhpt (Block_frag.map_bound do_bound do_pt_edge m)
    | Lhlist c -> Lhlist (do_lcall c)
    | Lhlseg (c, i, c') ->
      Lhlseg (do_lcall c, do_nid i, do_lsegcall c') in
  let do_lnode (ln: lnode): lnode =
    { ln with ln_e = do_lheap_frag ln.ln_e } in
  let do_refcount (m: int SvMap.t SvMap.t): int SvMap.t SvMap.t =
    (* - check that no occurrence of svf appears in the preds
     * - move the predecessors of svf into svt *)
    SvMap.iter
      (fun svpost ->
        SvMap.iter
          (fun svpre n ->
            if svpre = svf && n != 0 then Log.error "removed node not empty"
          )
      ) m;
    let recf = try SvMap.find svf m with Not_found -> SvMap.empty
    and rect = try SvMap.find svt m with Not_found -> SvMap.empty in
    let nrect =
      SvMap.fold
        (fun i n acc ->
          let o = try SvMap.find i rect with Not_found -> 0 in
          SvMap.add i (o + 1) acc
        ) recf rect in
    SvMap.add svf SvMap.empty (SvMap.add svt nrect m) in
  let l = { l with
            lm_mem    = SvMap.map do_lnode l.lm_mem;
            lm_refcnt = do_refcount l.lm_refcnt } in
  if sv_is_dangle svf l then l
  else sv_dangle_add svf (sv_dangle_rem svt l)
(* Guard function over lmem; adding certain kinds of constraints *)
let rec lmem_guard (c: n_cons) (l: lmem): lguard_res =
  match c with
  | Nc_cons (Apron.Tcons1.EQ, Ne_var sv, Ne_csti n)
  | Nc_cons (Apron.Tcons1.EQ, Ne_csti n, Ne_var sv) ->
      if n = 0 then
        match (sv_find sv l).ln_e with
        | Lhemp
        | Lhlist _ -> (* reduction brings no information: nothing to do *)
            Gr_no_info
        | Lhpt _ -> (* return bot *)
            (* null pointer is a valid address, so reduce to bottom *)
            Gr_bot
        | Lhlseg (_, svt, _) ->
            (* by construction of the inductive predicates of this domain,
             * it entails the segment is empty and svt is null;
             * for now, we simply propagate the reduction to svt (hope to
             * get a reduction to _|_), but we will able also to remove the
             * segment *)
            Log.warn "lmem_guard: empty segment could be removed";
            lmem_guard (Nc_cons (Apron.Tcons1.EQ, Ne_csti n, Ne_var svt)) l
      else Gr_no_info
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_var _, Ne_csti _)
  | Nc_cons (Apron.Tcons1.DISEQ, Ne_csti _, Ne_var _) ->
      Gr_no_info (* nothing to reduce in this case *)
  | Nc_cons (Apron.Tcons1.EQ, Ne_var sv0, Ne_var sv1) ->
      if sv0 = sv1 then Gr_no_info
      else
        begin
          match (sv_find sv0 l).ln_e, (sv_find sv1 l).ln_e with
          | Lhemp, Lhemp ->
              if sv0 < sv1 then Gr_sveq (sv1, sv0)
              else Gr_sveq (sv0, sv1)
          | Lhemp, _ -> Gr_sveq (sv0, sv1)
          | _, Lhemp -> Gr_sveq (sv1, sv0)
          | Lhpt pt0, Lhpt pt1 ->
              begin
                try
                  Block_frag.iter_bound
                    (fun bound ->
                      if Block_frag.mem bound pt1 then raise Bottom)
                    pt0;
                  Gr_no_info
                with Bottom ->
                  Log.info
                    "Separating conjonction infringed : reduction to bottom";
                  Gr_bot
              end
          | _, _ ->
              Log.todo "reduce_equality unsupported; give up on reduction";
              Gr_no_info
        end
  | Nc_cons (Apron.Tcons1.EQ, e1, e2) ->
    begin
    match Nd_utils.(decomp_lin_opt e1, decomp_lin_opt e2) with
      | Some (v1, Ne_csti e1), Some (v2, Ne_csti e2) when e1 = e2 ->
        lmem_guard (Nc_cons (Apron.Tcons1.EQ, Ne_var v1, Ne_var v2)) l
      | _, _ ->
        Log.warn "lmem_guard: unsupported condition, imprecise result:\n %a\n%a"
          Nd_utils.n_cons_fpr c (lmem_fpri "  ") l;
        Gr_no_info
    end
  | _ ->
      Log.warn "lmem_guard: unsupported condition, imprecise result:\n %a\n%a"
        Nd_utils.n_cons_fpr c (lmem_fpri "  ") l;
      Gr_no_info;;


(** Utilities for join *)
(* Initialization of the join algorithm
 * - both inputs should have the same root COLVs
 * - returns the initial join graph,
 *   the COLVs to add to the left,
 *   the COLVs to add to the right,
 *   the COLVs to remove from both at the end (all introduced COLV except the
 *   roots *)
let init_from_roots (msv: (sv * sv) SvMap.t) (xl: lmem) (xr: lmem):
    lmem * (col_kind SvMap.t * col_kind SvMap.t) * col_kind SvMap.t =
  (* 1. setting up the SVs *)
  let lm =
    SvMap.fold
      (fun ii (il, ir) acc ->
        let nl = sv_find il xl and _ = sv_find ir xr in
        sv_add ~root:true ii nl.ln_typ acc
      ) msv lmem_empty in
  (* 2. setting up the COLVs (should be the same in both sides) *)
  let col_roots =
    let roots_l = colv_roots xl and roots_r = colv_roots xr in
    assert (SvSet.equal roots_l roots_r);
    roots_l in
  (* 3. get all COL vars in both sides *)
  let colvl = colv_get_all xl and colvr = colv_get_all xr in
  (* 4. whatever COL var is in only either side should be added to the other *)
  let mapdiff m s = SvMap.filter (fun cv _ -> not (SvMap.mem cv s)) m in
  let col_toadd_l = mapdiff colvr colvl
  and col_toadd_r = mapdiff colvl colvr in
  (* 5. both maps should agree on common bindings *)
  SvMap.iter
    (fun cv kl ->
      try assert (kl = SvMap.find cv colvr)
      with Not_found -> ( )
    ) colvl;
  (* 6. join of dimensions *)
  let all_colv = SvMap.fold SvMap.add colvl colvr in
  let non_roots_colv =
    SvMap.filter (fun cv _ -> not (SvSet.mem cv col_roots)) all_colv in
  if debug_module then
    Log.debug "init_from_roots:\nL: %a\nR: %a\nL+: %a\nR+: %a\nA: %a\nR: %a"
      col_kinds_fpr colvl col_kinds_fpr colvr col_kinds_fpr col_toadd_l
      col_kinds_fpr col_toadd_r col_kinds_fpr all_colv
      col_kinds_fpr non_roots_colv;
  (* 7. precursor new state *)
  let lm =
    SvMap.fold
      (fun i k acc ->
        if SvGen.key_is_used acc.lm_colvkey i then acc
        else colv_add ~root:(colv_is_root xl i) i k acc
      ) all_colv lm in
  lm, (col_toadd_l, col_toadd_r), non_roots_colv

(* Comparison of list descriptors *)
let l_def_compare (ld0: l_def) (ld1: l_def): int =
  (* Huisong: maybe extend to check other fields *)
  (* here we only compare the name when both have name*)
  let b = match ld0.ld_name, ld1.ld_name with
  | Some n0, Some n1 -> n0 = n1
  | _, _ -> false in
  if b then 0
  else
    begin
      assert (ld0.ld_set = ld1.ld_set
                && ld0.ld_onexts = ld1.ld_onexts);
      let c = ld0.ld_size - ld1.ld_size in
      if c = 0 then ld0.ld_nextoff - ld1.ld_nextoff
      else c
    end
let l_call_compare (lc0: l_call) (lc1: l_call) : int =
  l_def_compare lc0.lc_def lc1.lc_def
let lseg_call_compare (lsegc0: lseg_call) (lsegc1: lseg_call) : int =
  l_def_compare lsegc0.lc_def lsegc1.lc_def


(** Inductive definitions *)
(* Global map of list-like inductive definitions *)
let list_ind_defs: l_def StringMap.t ref = ref StringMap.empty
(* Experimental code, to try to look for list like inductive defs *)
let exp_search_lists ( ): unit =
  let module M = struct exception Stop of string end in (* local bail out *)
  (* Reinitiliazes the global map of list like inductive definitions *)
  list_ind_defs := StringMap.empty;
  let name_2ldef name =
    try StringMap.find name !list_ind_defs
    with Not_found -> raise (M.Stop (Printf.sprintf "%s not found" name)) in
  let aux_rule (iname: string) (ind: ind) (r: irule): l_def =
    let m_subinds, m_fieldsoff, m_idxoff, m_fieldsrev =
      (* m_subinds   : ptr_var -> indcall     *)
      (* m_fieldsoff : off     -> cell        *)
      (* m_idxoff    : ptr_var -> offset      *)
      (* JG:TODO m_fieldsoff & m_fieldsrev are _never_ used ! *)
      List.fold_left
        (fun (accsubs, accfo, accio, accfr) -> function
          | Hacell cell ->
              begin
                match cell.ic_dest, Offs.to_int_opt cell.ic_off,
                Offs.to_int_opt cell.ic_doff with
                | Fa_var_new i, Some o, Some dest_o ->
                    let v = sv_unsafe_of_int i in
                    ( accsubs,
                      IntMap.add o cell accfo,
                      SvMap.add v (o, dest_o) accio,
                      SvMap.add v cell accfr )
                | Fa_par_ptr p, Some o, Some dest_o ->
                    ( accsubs,
                      IntMap.add o cell accfo,
                      accio,
                      accfr )
                | _, _, _ -> raise (M.Stop "field")
              end
          | Haind icall ->
              begin
                match icall.ii_maina with
                | Fa_this -> raise (M.Stop "main field")
                | Fa_var_new i ->
                    let m = SvMap.add (sv_unsafe_of_int i) icall accsubs in
                    m, accfo, accio, accfr
              end
          | Haseg (_, _) -> Log.todo_exn "???032"
        ) (SvMap.empty, IntMap.empty, SvMap.empty, SvMap.empty) r.ir_heap in
    (* find the offset correponding to a pointer variable *)
    let idx_2off idx =
      try SvMap.find idx m_idxoff
      with Not_found -> Log.fatal_exn "symvar %a at no offset" sv_fpr idx in
    (* Extracting a single next field, and tail inductive predicate *)
    Log.debug "m_subinds =\n%a" (SvMap.t_fpr "\n  " indcall_fpr) m_subinds;
    let onext, odnext, callnext, others =
      let self, others =
        SvMap.fold
          (fun i call (self, others) ->
            if String.compare call.ii_ind iname = 0 then
              (idx_2off i, call) :: self, SvMap.remove i others
            else self, others
          ) m_subinds ([ ], m_subinds) in
      match self with
      | [ ] -> raise (M.Stop "no next field")
      | _ :: _ :: _ -> raise (M.Stop "several next fields")
      | [ (onext, odnext), callnext ] -> onext, odnext, callnext, others in
    let ptrpr =
      let tr_formal_ptr_arg : formal_ptr_arg -> _ = function
        | Fa_this -> Pv_this
        | Fa_par_ptr i -> Pv_actual i
        | Fa_var_new i -> Pv_field (fst @@ idx_2off (sv_unsafe_of_int i)) in
      match callnext.ii_argp with
      | [] -> None
      | argp ->
        let p_params = List.map tr_formal_ptr_arg callnext.ii_argp in
        let p_off =
          IntMap.fold
            (fun key cell acc ->
              match cell.ic_dest with
              | Fa_par_ptr p ->
                let doff = Offs.to_int cell.ic_doff in
                IntMap.add key (p, doff) acc
              | _ -> acc )
            m_fieldsoff IntMap.empty in
        Some {p_params = p_params ; p_off = p_off } in
    (* Extracting calls corresponding to nested structures, and the set
     *   parameters of the recursive calls (to construct equalities) *)
    let nest_calls, nest_call_setpars, nest_call_seqpars =
      SvMap.fold
        (fun i ic (c, sp, qp) ->
          let off, doff = idx_2off i in
          ( off, name_2ldef ic.ii_ind, doff) :: c,
            ic.ii_args :: sp,
            ic.ii_argq :: qp )
        others ([ ], [ ], []) in
    (* Make a dicho of new SETv formal variables *)
    let subcall_pars =
      List.fold_lefti
        (fun i r (fa : formal_set_arg) -> match fa with
          | Fa_var_new j ->
              if IntMap.mem j r then
                raise (M.Stop "one new var => several sub-pars")
              else IntMap.add j (Sv_nextpar i) r
          | _ -> r
        ) IntMap.empty callnext.ii_args in
    (* Set predicates *)
    let set_predicates =
      let tr_formal_arith_arg = function
        | Fa_this -> Se_this
        | Fa_var_new i -> Se_field (fst @@ idx_2off (sv_unsafe_of_int i))
        | _ -> raise (M.Stop "tr_formal_arith_arg: not translateable") in
      let tr_formal_set_arg (fa : formal_set_arg) = match fa with
        | Fa_var_new i ->
            IntMap.find_comp (fun ( ) -> raise (M.Stop "Fa_var_new"))
              i subcall_pars
        | Fa_par_set i -> Sv_actual i in
      let tr_sexpr = function
        | Se_var sv -> Sd_var (tr_formal_set_arg sv)
        | Se_uplus ([set], elt) ->
            Sd_uplus (tr_formal_arith_arg elt, tr_formal_set_arg set)
        | _ -> raise (M.Stop "tr_sform: not translateable") in
      let tr_sform = function
        | Sf_mem (elt, set) ->
            Se_mem (tr_formal_arith_arg elt, Sd_var (tr_formal_set_arg set))
        | Sf_equal (set, sexpr) ->
            Se_eq (tr_formal_set_arg set, tr_sexpr sexpr)
        | _ -> raise (M.Stop "tr_sform: not translateable") in
      List.fold_left
        (fun acc -> function
          | Pf_alloc _ | Pf_arith _ | Pf_path _ -> acc
          | Pf_seq _ -> acc
          | Pf_set sf -> tr_sform sf :: acc
        ) [ ] r.ir_pure in
    (* Equalities induced by parameter definitions *)
    let set_predicates =
      let add_call_par f icall acc =
        List.fold_lefti
          (fun ipar acc (fa : formal_set_arg) -> match fa with
            | Fa_var_new _ -> acc
            | Fa_par_set i -> Se_eq (Sv_actual i, Sd_var (f ipar)) :: acc
          ) acc icall in
      List.fold_lefti
        (fun j acc icall -> add_call_par (fun k -> Sv_subpar (j, k)) icall acc)
        (add_call_par (fun j -> Sv_nextpar j) callnext.ii_args set_predicates)
        nest_call_setpars in
    (* Set parameters *)
    let set_pars =
      let l = IntMap.fold (fun i p a -> (i, p) :: a) ind.i_pkind.pr_set [] in
      let l = List.sort (fun (i0,_) (i1,_) -> i0 - i1) l in
      List.map snd l in
    (* Set component of the definition *)
    let setpr =
      let ufseg =
        List.fold_lefti (fun i acc k -> if k.st_head then Some i else acc)
          None set_pars in
      match set_predicates, set_pars with
      | [ ], [ ] -> None
      | _, _ -> Some { s_params = set_pars;
                       s_uf_seg = ufseg;
                       s_equas  = set_predicates } in
    (* sequences predicates*)
    (* Make a dicho of new SEQv formal variables *)
    let subcall_parq =
      List.fold_lefti
        (fun i r (fa : formal_seq_arg) -> match fa with
          | Fa_var_new j ->
              if IntMap.mem j r then
                raise (M.Stop "one new var => several sub-pars")
              else IntMap.add j (Qv_nextpar i) r
          | _ -> r
        ) IntMap.empty callnext.ii_argq in
    let seq_predicates =
      let tr_formal_arith_arg = function
        | Fa_this -> Qe_this
        | Fa_var_new i -> Qe_field (fst @@ idx_2off (sv_unsafe_of_int i))
        | _ -> raise (M.Stop "tr_formal_arith_arg: not translateable") in
      let tr_formal_seq_arg (fa : formal_seq_arg) = match fa with
        | Fa_var_new i ->
            IntMap.find_comp (fun ( ) -> raise (M.Stop "Fa_var_new"))
              i subcall_parq
        | Fa_par_seq i -> Qv_actual i in
      let rec tr_qexpr = function
        | Qe_empty -> Qd_empty
        | Qe_var v -> Qd_var (tr_formal_seq_arg v)
        | Qe_val x -> Qd_val (tr_formal_arith_arg x)
        | Qe_concat l -> Qd_concat (List.map tr_qexpr l)
        | Qe_sort s -> Qd_sort (tr_qexpr s) in
      let tr_qform = function
        | Qf_equal (var, qexpr) ->
            let var = tr_formal_seq_arg var in
            let qexpr = tr_qexpr qexpr in
            Qe_eq (var, qexpr) in
      List.fold_left
        (fun acc -> function
          | Pf_alloc _ | Pf_arith _ | Pf_path _ |Pf_set _ -> acc
          | Pf_seq qf -> tr_qform qf :: acc
        ) [ ] r.ir_pure in
    (* Equalities induced by parameter definitions *)
    (*  -------------------- JG:TODO  -------------------- *)
    (* Seq parameters *)
    let seq_pars =
      let l = IntMap.fold (fun i p a -> (i, p) :: a) ind.i_pkind.pr_seq [] in
      let l = List.sort (fun (i0,_) (i1,_) -> i0 - i1) l in
      List.map snd l in
    (* Seq component of the definition *)
    let seqpr =
      let ufseg =
        List.fold_lefti (fun i acc k -> if k.sq_add then Some i else acc)
          None seq_pars in
      match seq_predicates, seq_pars with
      | [ ], [ ] -> None
      | _, _ -> Some { q_params = seq_pars;
                       q_uf_seg = ufseg;
                       q_equas  = seq_predicates } in
    (* Searching for the size *)
    let size =
      let acc =
        List.fold_left
          (fun acc -> function
            | Pf_alloc s ->
                if acc != None then raise (M.Stop "two sizes"); Some s
            | _ -> acc
          ) None r.ir_pure in
      match acc with
      | None -> raise (M.Stop "no size")
      | Some s -> s in
    (* Searching for potential backward pointer *)
    (* JG:TODO We use the analysis made in ind_utils :
     * we should check this is correct ! *)
    let prevpar =
      if IntSet.cardinal ind.i_pr_pars = 1 then
        Some (IntSet.min_elt ind.i_pr_pars)
      else None in
    Log.debug "backward pointer is ptr parameter : @p%s"
      (match prevpar with | None -> "_" | Some i -> string_of_int i);
    (* Produce result *)
    { ld_name    = Some ind.i_name;
      ld_m_offs  = [];
      ld_submem  = None;
      ld_emp_csti= 0;
      ld_emp_mcons = [];
      ld_next_mcons = [] ;
      ld_nextoff = onext;
      ld_nextdoff = odnext;
      ld_prevpar = prevpar;
      ld_size    = size;
      ld_onexts  = nest_calls;
      ld_ptr     = ptrpr;
      ld_set     = setpr;
      ld_seq     = seqpr } in
  Log.info "searching list defs\n";
  (* Construction of the dependence graph of inductive definitions *)
  let depgraph =
    StringMap.fold
      (fun name ind acc ->
        List.fold_left
          (fun acc rule ->
            List.fold_left
              (fun acc -> function
                | Hacell _ -> acc
                | Haind ic -> StringGraph.edge_add name ic.ii_ind acc
                | Haseg (ic0, ic1) ->
                    StringGraph.edge_add name ic0.ii_ind
                      (StringGraph.edge_add name ic1.ii_ind acc)
              ) acc rule.ir_heap
          ) acc ind.i_rules
      ) !Ind_utils.ind_defs StringGraph.empty in
  (* Topological sort; components with two elements or more should not
   *  be considered (the list domain does not handle cyclic definitions) *)
  let components =
    List.filter (fun s -> StringSet.cardinal s <= 1)
      (StringGraph.tarjan depgraph) in
  let ind_candidates =
    List.fold_left (fun acc s -> StringSet.min_elt s :: acc) [ ] components in
  (* Iteration over the inductive definitions candidate to conversion *)
  List.iter
    (fun name ->
      Log.info "looking at inductive definition %s" name;
      let ind =
        try StringMap.find name !Ind_utils.ind_defs
        with Not_found -> Log.fatal_exn "ind def %s not found" name in
      if ind.i_mt_rule && List.length ind.i_rules = 2 then
        let rule =
          match ind.i_rules with
          | [ r0 ; r1 ] ->
              if r0.ir_kind = Ik_empz then r1
              else if r1.ir_kind = Ik_empz then r0
              else Log.fatal_exn "contradicting rules structure"
          | _ -> Log.fatal_exn "contradicting number of rules" in
        try
          let lc = aux_rule name ind rule in
          Log.info "\nDefinition %s converted into:\n%a\n" name
            (l_def_fpri "    ") lc;
          list_ind_defs := StringMap.add name lc !list_ind_defs
        with
        | M.Stop s -> Log.info "%s not a list like ind def (%s)\n" name s
      else Log.info "%s not list like ind def (rule #)" name
    ) ind_candidates
