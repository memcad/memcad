(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_sig.ml
 **       signatures of the main abstract domains
 ** Xavier Rival, started 2011/05/27 *)
open Data_structures
open Flags
open Lib
open Offs
open Sv_def

open Ast_sig
open Disj_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Col_sig
open Set_sig
open Seq_sig
open Spec_sig
open Svenv_sig
open Vd_sig


(** Location (for the disjunctions domain to keep track of partitions) *)
type location = int (* we should keep this more generic *)


(** Symbolic variables mappings communicated to the domain *)
(* Unary operations that require a set of roots *)
type 'a uni_table = 'a Aa_sets.t
(* Relations between left argument (L), right argument (R) and output (O) *)
type 'a bin_table = ('a, 'a) Aa_maps.t        (* R => L function *)
type 'a tri_table = ('a * 'a * 'a) Aa_sets.t  (* set of (L, R, O) mappings *)
type ('a, 'b) tri_map = ('a * 'a * 'a, 'b) Aa_maps.t
  (* map of (L, R, O)|-> 'b mappings *)


(** Hints to be supplied to graph algorithms *)
(* We use structures here, in anticipation of additional hints added in the
 * near future *)

(* Shape domain level, for local weak abstraction (Unary) *)
type 'a hint_us =
    { hus_live: 'a uni_table }
(* Shape domain level, for widening and join (Binary) *)
type 'a hint_bs =
    { hbs_live: 'a bin_table }
type 'a lint_bs = (* TODO: the "lint_" names is wierd, and should be changed *)
    { lbs_dead: 'a bin_table }

(* Environment level, for local weak abstraction (Unary) *)
type hint_ue =
    { hue_live: VarSet.t (* live variables *) }
(* Environment level, for widening and join (Binary) *)
type hint_be =
    { hbe_live: VarSet.t; (* live variables in both argument *) }
type 'a lint_be =
    { lbe_dead:  ('a tlval) list; (*dead access path*)}

(* Joining element with encoded graph *)
type join_ele =
  { (* the encoding of input graph *)
    abs_gi:   abs_graph option;
    (* the join result of encodings *)
    abs_go:   abs_graph option; }


(** Symbolic variables mapping, returned by lattice operations *)
(* Symbolic variable updater *)
type sv_upd =
  | Svu_mod of sv * SvSet.t (* sv is modified *)
  | Svu_rem                   (* sv is removed *)
(* Symbolic variable environment updater *)
type svenv_upd = sv -> sv_upd

(** Types for commands that are shared across domains *)
(* Inductive predicates (call-src, node-srce) *)
type 'a indt = 'a gen_ind_call * 'a
(* Segment predicates (call-src, node-src, call-dst, node-dst) *)
type 'a segt = 'a gen_ind_call * 'a * 'a gen_ind_call * 'a

(* Logical formulas, used for specifying assumed/checked properties *)
(* State-level abstractions (mem-low, mem-high, state) *)
type 'a gen_st_log_form =
  | SL_array
  | SL_set of 'a setprop  (* set predicate *)
  | SL_seq of 'a seqprop  (* seq predicate *)
  | SL_ind of 'a indt     (* inductive predicate *)
  | SL_seg of 'a segt     (* segment predicate *)
type meml_log_form  = svo gen_st_log_form       (* vars: sv+off *)
type memh_log_form  = sv tlval gen_st_log_form (* vars: sv-lvalues *)
type state_log_form = var tlval gen_st_log_form (* vars: lang-lvalues *)
(* Logical formulas for the "low" level shape abstraction module *)
type meml_cons =
  | Mc_num of n_cons
  | Mc_set of set_cons
  | Mc_seq of seq_cons

(* Environment commands *)
type env_op =
  | EO_add_var of var       (* Add C-level Var *)
  | EO_del_var of var       (* Del C-level Var *)
  | EO_add_colvar of colvar (* Adds a C-level set or seq var *)
  | EO_rem_colvar of colvar (* Removes a C-level set or seq var *)

(* Memory commands *)
type 'a mem_op =
  | MO_alloc of 'a tlval * 'a texpr (* Allocation:       l = malloc( e ) *)
  | MO_dealloc of 'a tlval          (* Deallocation:     free( l )       *)
type int_mem_op = int mem_op
type var_mem_op = var mem_op

(* Unary op in the disj domain *)
type unary_op =
  | UO_env of env_op     (* addition/deletion of (set) variables *)
  | UO_ret of typ option (* addition of return var or not *)
  | UO_mem of var_mem_op (* memory operations *)


(** For initialization of spec formula *)
type s_pre_env = sv StringMap.t * (sv * col_kind * colv_info option) StringMap.t


(** Naming of abstractd domain modules *)
(* A symbolic variable is a pair:
 *  - module ID: index + name of the module that created the symvar
 *  - symvar ID: index of the symvar in that module *)
(* Module id *)
type mod_id = sv * string


(** Signature for array node module *)
module type ARRAY_NODE =
  sig
    include INTROSPECT
    type t
    type op
    val t_fpri: string -> form -> t -> unit
    (** Program variables used as symbolic variables in inductive instances *)
    (* Calculate the similarity of each group from two states *)
    val rankscore: (sv -> bool) -> t -> op -> t -> op
      -> sv SvMap.t -> sv SvMap.t -> (sv * sv * int) list
    (** Utilities *)
    val get_namer: sv -> t -> sv_namer -> sv_namer
    (* Inclusion check on two states with the same parition *)
    val flat_is_le: t -> t -> bool
    (* Join function for two states with the same partition *)
    val flat_join: t -> t -> t
    (* Remove the groups that contain 0 element *)
    val cleanup: t -> op -> t * op
    (* Group mapping to guide inclusion check *)
    val inclmap: t -> op -> t -> op -> (sv -> bool)
      -> t * op * (SvSet.t SvMap.t) * bool
    (* Join on groups with inductive predicates *)
    val pred_join: bool -> t -> op -> t -> op -> (sv -> bool)
        -> t * op * t * op * (sv SvMap.t) * (sv SvMap.t)
    (** Bottom check  *)
    val is_bot: op -> t -> bool
    (** Transfer functions *)
    (* Guard *)
    val guard: bool -> n_cons -> op -> t -> op * t
    (* Find the group in which the array cell is, if there are several groups
     * in the array the cell might be belong to, we create disjunctions *)
    val deref_in_expr: Offs.t -> op -> t -> (sv * op * t) list
    (* Unfold result for inductive predicates *)
    type sub_unfold_result
    (* Materialization on l-value *)
    val materialize_on_lv: t -> Offs.t -> op -> t * op * sv
    (* For a variable, if the groups it belongs to is not determinestic
     * then merge all possible groups;
     * to make the l-value a non-summarizing dimension, split a group
     * which contains only the index varialbe *)
    val materialize: t -> sv -> sv -> sv option -> sub_unfold_result -> op
      -> t * op * sv * sv * sv
    (* Dereference on array cells: from offset to dimensions *)
    val sv_array_deref: SvSet.t -> Offs.t -> op -> t -> op * t
    (* Apply operators split,create on two states
     * to make two states ready for includsion check *)
    val apply_inclmap: SvSet.t SvMap.t -> t -> op -> t -> op
      -> op * t * op * t
    (* Apply operators split,create on two states to make them ready for join*)
    val apply_joinmap: (sv * sv * int) list -> t -> op -> t -> op
      -> op * t * op * t
    (* apply operators split,create,merge on two states to make them
     * ready for widen *)
    val apply_widenmap: (sv * sv * int) list -> t -> op -> t -> op
      -> op * t * op * t
    (* Whether a dimension is live *)
    val is_dimension_in: sv -> t -> bool
    val pre_assign: sv -> op -> t -> op * t
    (* Assignment on array cells *)
    val assign_on_sum: sv -> n_expr -> op -> t -> op * t
    (* Assignment on scalar variables *)
    val assign_on_sca: sv -> n_expr -> op -> t -> t
    (* get a new array content node *)
    val fresh_array_node: sv -> int -> int list -> op -> t * op
    (* Remove all dimensions from a group *)
    val rem_dims: t -> op -> op
    (* Assume a list predicate on a group *)
    val assume_ind: sv -> svo gen_ind_call -> op -> t -> op * t
    (* Assume a list segment on a group *)
    val assume_seg: sv -> svo gen_ind_call -> sv -> svo gen_ind_call
      -> op -> t -> op * t
    (* Check whether predicates on arrays are satisfied  *)
    val array_check: t -> op -> bool
    (* Check whether an inductive predicate is satisfied *)
    val check_ind: sv -> svo gen_ind_call -> op -> t -> bool
    (* Check whether an inductive segment is satisfied *)
    val check_seg: sv -> svo gen_ind_call -> sv -> svo gen_ind_call
      -> op -> t -> bool
  end


(** Signature of the lower layer of the memory abstract domain
 **   - this layer deals with memory cells (creation, read, write)
 **   - memory domain combination operations are done at this level
 **)
module type DOM_MEM_LOW =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (** Domain initialization to a set of inductive definitions *)
    (* Domain initialization to a set of inductive definitions *)
    val init_inductives: SvGen.t -> StringSet.t -> SvGen.t
    val inductive_is_allowed: string -> bool
    (** Fixing sets of keys *)
    val sve_sync_bot_up: t -> t * svenv_mod
    (* Sanity check *)
    val sanity_sv: SvSet.t -> t -> bool
    (** Lattice elements *)
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Top element, with empty set of roots *)
    val top: unit -> t (* this is empty store, in fact *)
    (* Pretty-printing *)
    val t_fpri: string -> form -> t -> unit
    (* External output *)
    val ext_output: output_format -> string -> namer -> t -> unit
    (** Management of symbolic variables *)
    (* Will that symb. var. creation be allowed by the domain? *)
    val sv_is_allowed: ?attr:node_attribute -> ntyp -> nalloc -> t -> bool
    (* Add a symbolic variable, with a newly generated id
     *  (by default, not considered root by the underlying domain) *)
    val sv_add_fresh: ?attr:node_attribute -> ?root: bool -> ntyp -> nalloc
      -> t -> sv * t
    (* Recover information about a symbolic variable *)
    (* For now, only nalloc and ntyp *)
    val sv_get_info: sv -> t -> nalloc option * ntyp option
    (* Garbage collection *)
    val gc: sv uni_table -> t -> t
    (* graph encode *)
    val encode: sv -> namer -> t -> renamed_path list * IntSvPrSetSet.t * sv
    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    val is_le: sv bin_table -> colv_emb -> t -> t
      -> svenv_upd option (* None = false *)
    (* Generic comparison (does both join and widening) *)
    val join: join_kind
      -> sv hint_bs option
      -> (Offs.svo lint_bs) option
      -> sv tri_table               (* SV relation table *)
      -> (sv, col_kind) tri_map     (* COLV relation table *)
      -> t * join_ele
      -> t * join_ele
        -> t * svenv_upd * svenv_upd
    (* Directed weakening; over-approximates only the right element *)
    val directed_weakening: sv hint_bs option
      -> sv tri_table
      -> (sv, col_kind) tri_map
      -> t -> t -> t * svenv_upd
    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    val local_abstract: sv hint_us option -> t -> t
    (** Cell operations *)
    (* Creation *)
    val cell_create: ?attr:node_attribute -> svo -> Offs.size -> ntyp -> t -> t
    (* Deletion *)
    val cell_delete: ?free:bool -> ?root:bool -> sv -> t -> t
    (* Read the content of a cell *)
    val cell_read:
      bool                 (* whether call from cell-resolve  *)
      -> svo               (* address of the cell to read *)
      -> int               (* size of the cell to read *)
        -> t               (* abstract memory input *)
          -> (t            (* possibly modified memory abstraction *)
              * svo        (* address of the read cell, may have changed *)
              * svo option (* Some content in case of success, None otherwise *)
             ) list        (* list of disjuncts *)
    (* Resolve a cell, i.e., materialization *)
    val cell_resolve:
        svo          (* address of the cell to resolve *)
      -> int         (* size *)
        -> t         (* abstract memory input *)
          -> (t      (* possibly modified memory abstraction *)
              * svo  (* address of the resolved cell, may have changed *)
              * bool (* whether cell resolution was succesful *)
             ) list  (* list of disjuncts *)
    (* Write a cell *)
    val cell_write:
        ntyp          (* type of the value being assigned *)
      -> svo          (* address of the cell to over-write *)
        -> int        (* size of the chunk to write into the memory *)
          -> n_expr   (* right hand side as an expression over SVs *)
            -> t -> t (* output abstract memory *)
    (** Transfer functions for the analysis *)
    (* Condition test *)
    val guard: n_cons -> t -> t
    (* Checking that a constraint is satisfied *)
    val sat: n_cons -> t -> bool
    (** Set/Col domain *)
    (* Adding / removing set variables *)
    val colv_add_fresh: bool -> string -> col_kind -> t
      -> sv * t * (colv_info option)
    val colv_delete: sv -> t -> t
    (* Guard and sat functions for set properties *)
    val assume: meml_log_form -> t -> t
    val check:  meml_log_form -> t -> bool
    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    val mark_prio: svo -> t -> t
    (** Unfolding, assuming and checking inductive edges *)
    (* Unfold *)
    val ind_unfold: unfold_dir -> svo -> t -> t list
    (** Construction from formulas and checking of formulas *)
    val from_s_formula: colvar StringMap.t -> s_formula -> t * s_pre_env
    val sat_s_formula: sv StringMap.t
      -> (sv * col_kind * colvar) StringMap.t -> s_formula -> t -> bool
  end


(** Signature of the upper layer of the memory abstract domain
 **   - this layer solves the evaluation of expressions
 **   - it delegates the cell operations to DOM_MEM_LOW
 **   - a lift functor defines this from DOM_MEM_LOW
 **)
module type DOM_MEM_EXPRS =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (** Domain initialization to a set of inductive definitions *)
    (* Domain initialization to a set of inductive definitions *)
    val init_inductives: SvGen.t -> StringSet.t -> SvGen.t
    val inductive_is_allowed: string -> bool
    (** Fixing sets of keys *)
    val sve_sync_bot_up: t -> t * svenv_mod
    (* Sanity check *)
    val sanity_sv: SvSet.t -> t -> bool
    (** Lattice elements *)
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Top element, with empty set of roots *)
    val top: unit -> t (* this is empty store, in fact *)
    (* Pretty-printing *)
    val t_fpri: string -> form -> t -> unit
    (* External output *)
    val ext_output: output_format -> string -> namer -> t -> unit
    (* Garbage collection *)
    val gc: sv uni_table -> t -> t
    (* graph encode *)
    val encode: sv -> namer -> t -> renamed_path list * IntSvPrSetSet.t * sv
    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    val is_le: sv bin_table -> colv_emb -> t -> t -> bool
    (* Generic comparison (does both join and widening) *)
    val join: join_kind
      -> sv hint_bs option
      -> ((sv tlval) lint_bs) option
      -> sv tri_table              (* SV relation table *)
      -> (sv, col_kind) tri_map    (* COLV relation table *)
      -> t * join_ele
      -> t * join_ele
      -> t
    (* Directed weakening; over-approximates only the right element *)
    val directed_weakening: sv hint_bs option
      -> sv tri_table
      -> (sv, col_kind) tri_map
      -> t -> t -> t
    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    val local_abstract: sv hint_us option -> t -> t
    (** Transfer functions for the analysis *)
    (* Assignment *)
    val assign: sv tlval -> sv texpr -> t -> t list
    (* Condition test *)
    val guard: bool -> sv texpr -> t -> t list
    (* Checking that a constraint is satisfied *)
    val sat: sv texpr -> t -> bool
    (* Creation of the memory for a variable *)
    val create_mem_var: typ -> t -> sv * t
    (* Removal of the memory for a variable *)
    val delete_mem_var: sv -> t -> t (* takes SV representing address *)
    (* memory alloc/free *)
    val memory: sv mem_op -> t -> t list
    (** Set/Seq domain *)
    (* add/delete variables *)
    val colv_add: string -> col_kind -> t -> sv * t * (colv_info option)
    val colv_rem: sv -> t -> t
    (* Guard and sat functions for set properties *)
    val assume: memh_log_form -> t -> t
    val check:  memh_log_form -> t -> bool
    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    val mark_prio: sv tlval -> t -> t
    (** Unfolding, assuming and checking inductive edges *)
    (* Unfold *)
    val ind_unfold: unfold_dir -> sv tlval -> t -> t list
    (** Construction from formulas and checking of formulas *)
    val from_s_formula: colvar StringMap.t -> s_formula -> t * s_pre_env
    val sat_s_formula: sv StringMap.t
      -> (sv * col_kind * colvar) StringMap.t -> s_formula -> t -> bool
  end


(** Signature of the env+shape domain *)
module type DOM_ENV =
  sig
    include INTROSPECT
    (** Type of abstract values *)
    type t
    (* Bottom element *)
    val bot: t -> t (* transforms a value into a bottom *)
    val is_bot: t -> bool
    (* Top element, with provided set of roots *)
    val top: unit -> t
    (* Pretty-printing *)
    val t_fpri: string -> form -> t -> unit
    (* External output *)
    val ext_output: output_format -> string -> t -> unit
    (* Garbage collection *)
    val gc: t -> t
    (** Management of variables *)
    val unary_op: env_op -> t -> t
    (* encode graph *)
    val encode: sv -> var list -> t -> renamed_path list * IntSvPrSetSet.t * sv
    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    val is_le: t -> t -> bool
    (* Generic comparison *)
    val gen_join: join_kind -> hint_be option -> (var lint_be) option
      -> t * join_ele -> t * join_ele -> t
    (* Join and widening *)
    val join:  hint_be option -> (var lint_be) option
      -> t * join_ele -> t * join_ele -> t
    val widen: hint_be option -> (var lint_be) option
      -> t * join_ele -> t * join_ele -> t
    val directed_weakening: hint_be option -> t -> t -> t
    (* Unary abstraction, a kind of relaxed canonicalization operator *)
    val local_abstract: hint_ue option -> t -> t
    (** Transfer functions for the analysis *)
    (* Assignment operator *)
    val assign: var tlval -> var texpr -> t -> t list
    (* Condition test *)
    val guard: bool -> var_info texpr -> t -> t list
    (* Checking that a constraint is satisfied; returns over-approx sat *)
    val sat: var_info texpr -> t -> bool
    (* Memory alloc/free *)
    val memory: var_mem_op -> t -> t list
    (** Set domain *)
    (* Guard and sat functions for set properties *)
    val assume: state_log_form -> t -> t
    val check:  state_log_form -> t -> bool
    (** Analysis control *)
    (* Reduction + SV relocalization *)
    val reduce_localize: var tlval -> t -> t option
    (* Eager reduction *)
    val reduce_eager: t -> t list
    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    val mark_prio: var tlval -> t -> t
    (** Assuming and checking inductive edges *)
    (* Unfold *)
    val ind_unfold: unfold_dir -> var tlval -> t -> t list
    (* Check construction, that an inductive be there *)
    (** Temporary, to short-cut disjunctions *)
    val assert_one: t list -> t
    (** Construction from formulas and checking of formulas *)
    val from_s_formula: var StringMap.t -> colvar StringMap.t -> s_formula -> t
    val sat_s_formula: var list -> colvar StringMap.t -> s_formula -> t -> bool
  end

(** Signature of the disjunction domain *)
module type DOM_DISJ =
  sig
    include INTROSPECT
    (* Abstract elements, with or without disjunctions *)
    type t
    (* Disjunction size *)
    val disj_size: t -> int
    (* Bottom element *)
    val bot: t
    val is_bot: t -> bool
    (* Top element, with provided set of roots *)
    val top: unit -> t
    (* Pretty-printing *)
    val t_fpri: string -> form -> t -> unit
    (* External output *)
    val ext_output: output_format -> string -> t -> unit
    (* Garbage collection *)
    val gc: t -> t
    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    val is_le: t -> t -> bool
    (* Generic comparison *)
    val merge_disjuncts: t -> t (* joins symbolic disjunctions *)
    val join:  hint_be option -> (var lint_be) option -> t -> t -> t
    val widen: hint_be option -> (var lint_be) option -> t -> t
      -> t * (t option)
    val directed_weakening: hint_be option -> t -> t -> t
    (** Transfer functions for the analysis *)
    (* Assignment operator *)
    val assign: location -> var tlval -> var texpr -> t -> t list
    (* Condition test *)
    val guard: location -> bool -> var_info texpr -> t -> t list
    (* Checking that a constraint is satisfied; returns over-approx sat *)
    val sat: var_info texpr -> t -> bool
    (** Set domain *)
    (* Adding / removing set variables *)
    val unary_op: unary_op -> t -> t
    (* Guard and sat functions for set properties *)
    val assume: state_log_form -> t -> t
    val check:  state_log_form -> t -> bool
    (** Management of disjunctions *)
    (* Selective disjunct merge *)
    val sel_merge: var list -> hint_be option -> (var lint_be) option -> t -> t
    (* Adding an abs_hist_atom *)
    val push_hist_atom: abs_hist_atom -> t -> t
    (** Analysis control *)
    (* Reduction + SV relocalization *)
    val reduce_localize: var tlval -> t -> t
    (* Eager reduction *)
    val reduce_eager: t -> t
    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    val mark_prio: var tlval -> t -> t
    (** Assuming and checking inductive edges *)
    (* Unfold *)
    val ind_unfold: location -> unfold_dir -> var tlval -> t -> t
    (** Construction from formulas and checking of formulas *)
    val from_s_formula: var StringMap.t -> colvar StringMap.t -> s_formula -> t
    val sat_s_formula: var list -> colvar StringMap.t -> s_formula -> t -> bool
    (** Statistics *)
    (* For now, simply a number of disjuncts *)
    val get_stats: t -> int
  end

module type GRAPH_ENCODE =
  sig
    include INTROSPECT
    type encoded_graph_edges = renamed_path list
    type encoded_graph = encoded_graph_edges * IntSvPrSetSet.t * sv
    val can_widen: encoded_graph -> encoded_graph -> bool
    val encode: sv -> namer -> graph -> encoded_graph
    val join: encoded_graph -> encoded_graph -> encoded_graph option
    val widen: encoded_graph -> encoded_graph -> encoded_graph
    val pp_encoded_graph:
      sv -> string list -> graph -> namer -> string -> out_channel -> unit
    val pp_precisely_encoded_graph:
      sv -> string list -> graph -> namer -> string -> out_channel -> unit
    val reduce_to_seg: abs_graph -> abs_graph
    val renamed_paths_fpr: form -> encoded_graph_edges -> unit
    val fpri: string -> form -> abs_graph_arg option -> unit
    (* the following are for unit tests *)
    exception Cannot_join
    val is_included_any: step list -> step list -> bool
    val join_paths: renamed_path -> renamed_path -> renamed_path option
  end
