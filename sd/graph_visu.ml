(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_visu.ml
 **       export graphs for visualization in the dot format for graphviz
 ** Francois Berenger and Huisong Li, started 2015/02/24 *)
open Data_structures
open Sv_def

open Graph_sig
open Graph_utils
open Graph_visu_atom
open Graph_visu_common

module L  = List
module Pr = Printf

module Log =
  Logger.Make(struct let section = "gv______" and level = Log_level.DEBUG end)

let nodes_of_graph (g: graph): Graph_sig.node list =
  L.map snd (SvMap.bindings g.g_g)

let merge_int_sets (groups: SvSet.t list): SvSet.t =
  L.fold_left SvSet.union SvSet.empty groups

let connected_components (g: graph): SvSet.t list =
  let is_linked_to (n: node) (group: SvSet.t): bool =
    if SvSet.mem n.n_i group then true (* n is in the group *)
    else
      SvSet.exists (* or one of its successors is *)
        (fun s -> SvSet.mem s group) (Node.successors n) in
  let rec loop (nodes: node list) (acc: SvSet.t list): SvSet.t list =
    match nodes with
    | [] -> acc
    | n :: ns ->
        let connected, not_connected = L.partition (is_linked_to n) acc in
        let new_acc = (merge_int_sets connected) :: not_connected in
        loop ns new_acc in
  let nodes = nodes_of_graph g in
  let init = L.map (fun n -> SvSet.singleton n.n_i) nodes in
  loop nodes init

(* remove from 'g' all leaves which are not inductive edges *)
let cut_ordinary_leaves (g: graph): graph =
  let rec loop (to_visit: SvSet.t) (visited: SvSet.t) (acc: SvSet.t) =
    if SvSet.is_empty to_visit then
      acc
    else
      let curr, remaining = SvSet.pop_min to_visit in
      let curr = get_node curr g in
      let nexts = Node.successors curr in
      let nexts = SvSet.filter (fun n -> not (SvSet.mem n visited)) nexts in
      let to_visit' = SvSet.union nexts remaining in
      let visited' = SvSet.add curr.n_i visited in
      let acc' =
        if Node.is_leaf curr then acc
        else SvSet.add curr.n_i acc in
      loop to_visit' visited' acc' in
  let all_nodes = get_all_nodes g in
  let interesting_nodes = loop all_nodes SvSet.empty SvSet.empty in
  let new_map =
    SvMap.filter (fun k _v -> SvSet.mem k interesting_nodes) g.g_g in
  { g with g_g = new_map }

(** [contains_pred sv g] returns [true] iff [sv] has a predecessor:
    - in the graph
    - pointing to [sv]
      (this does not include colv info of inductive edges arguments) *)
let contains_pred (sv: sv) (g: graph) : bool =
  let ok_pred pred =
    match SvMap.find_opt pred g.g_g with
    | Some {n_e = Hpt bf; _} ->
        Block_frag.fold_base (fun _ pe acc -> acc || fst pe.pe_dest = sv)
          bf false
    | Some {n_e = Hseg se; _} -> se.se_dnode = sv
    | _ -> false in
  let node = SvMap.find sv g.g_g in
  SvSet.exists ok_pred node.n_preds

(* pretty print graph out in .dot format for the
 * graphviz tools (dot, dotty, etc.) *)
let pp_all_graph
    (title: string)
    ?(labels= "")
    (g: graph)
    (namer: namer)
    (out: out_channel)
  : unit =
  let fmt = F.formatter_of_out_channel out in
  let nodes = nodes_of_graph g in
  let edges = L.map Edge.of_node nodes in
  let offsets = Edge.list_offsets edges in
  (* graph header *)
  F.fprintf fmt
    ("digraph g {\n" ^^
     "// title\n" ^^
     "labelloc=\"t\";\n" ^^
     "label=\"%s\";\n") title;
  F.fprintf fmt "graph [ rankdir = \"LR\" ];\n";
  (* nodes *)
  L.iter
    (fun n ->
      let node = Node.create n namer offsets in
      F.fprintf fmt "%a" (Node.fpr SvSet.empty) node
    ) nodes;
  if labels <> "" then
    begin
      let color = "\"#6a8d73\"" in
      let shape = "\"note\"" in
      F.fprintf fmt
        "\"big_label\" [shape=%s, penwidth=2, color=%s, label=\"%s\"];\n"
        shape color labels;
      L.iter
        (fun n ->
          if SvSet.mem n.n_i g.g_roots || not @@ contains_pred n.n_i g then
            F.fprintf fmt "\"big_label\" -> \"%a\":f0 [style=invis];"
              Sv_utils.sv_fpr n.n_i
        ) nodes;
    end;
  (* edges *)
  L.iter (fun e -> F.fprintf fmt "%a" (Edge.fpr ~prefix:"") (e, g.g_g)) edges;
  F.fprintf fmt "}\n" (* end of digraph *)

(* pretty print only 'interesting_nodes' from 'g' *)
let pp_pruned_graph
    (title: string)
    ?(labels= "")
    (interesting_nodes: SvSet.t)
    (g: graph)
    (namer: namer)
    (out: out_channel): unit =
  let interesting_graph = filter_nodes interesting_nodes g in
  (* call pp_graph on the graph with an updated list of nodes *)
  pp_all_graph title ~labels interesting_graph namer out

(* graph pruned using the connected component criterion *)
let pp_pruned_graph_cc
    (title: string)
    ?(labels= "")
    (vars_to_keep: StringSet.t)
    (g: graph) (namer: namer) (out: out_channel): unit =
  let components = connected_components g in
  (* keep only connected components which have at least one node *)
  (* we are interested into *)
  let interesting_components =
    L.filter
      (fun c ->
        SvSet.exists
          (fun i ->
            let name = get_name namer i in
            StringSet.mem name vars_to_keep
          ) c
      ) components in
  let interesting_nodes = merge_int_sets interesting_components in
  pp_pruned_graph ~labels title interesting_nodes g namer out

(* graph pruned using the 'successors of' criterion *)
let pp_pruned_graph_succ
    (title: string)
    ?(labels= "")
    (root_vars: StringSet.t)
    (g: graph)
    (namer: namer)
    (out: out_channel): unit =
  let interesting_nodes = successors_only root_vars g namer in
  pp_pruned_graph title ~labels interesting_nodes g namer out

(** graph pruned by removing all "orphans nodes".
    - A root node may be orphan
    - A non-empty node is not an orphan node
    - A node pointed by another node is not an orhpan (see [contains_pred]) *)
let pp_remove_orphans
    (title: string)
    ?(labels= "")
    (g: graph)
    (namer: namer)
    (out: out_channel): unit =
  let interesting_nodes =
    SvMap.fold
      (fun sv node acc ->
        if node.n_e != Hemp then
          SvSet.add sv acc
        else if contains_pred sv g then
          SvSet.add sv acc
        else acc
      ) g.g_g SvSet.empty in
  pp_pruned_graph title ~labels interesting_nodes g namer out

(* 'vars' is a list of variables (their names) that you want to keep in
 * the output graph; if 'vars' is empty: all nodes of the graph are kept *)
let pp_graph
    (title: string)
    ?(labels= "")
    (vars: string list)
    (opts: visu_option list)
    (g: graph)
    (namer: namer)
    (out: out_channel): unit =
  let cc_mode = L.mem Connex_component opts in
  let succ_mode = L.mem Successors opts in
  let cut_leaves_mode = L.mem Cut_leaves opts in
  let g =
    if cut_leaves_mode then cut_ordinary_leaves g
    else g in
  if vars = [] then
    begin
      if cc_mode then Log.warn "pp_graph: CC ignored";
      if succ_mode then Log.warn "pp_graph: SUCC ignored";
      pp_remove_orphans title ~labels g namer out
    end
  else
    let f =
      match cc_mode, succ_mode with
      | false, false -> failwith "pp_graph: use either CC or SUCC"
      | true , true  -> assert(false) (* case filtered out earlier *)
      | true , false -> pp_pruned_graph_cc
      | false, true  -> pp_pruned_graph_succ in
    let var_set = StringSet.of_list vars in
    f title ~labels var_set g namer out
