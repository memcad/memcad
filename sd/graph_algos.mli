(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_algos.mli
 **       elements used for weakening algorithms on graphs
 ** Xavier Rival, 2012/04/10 *)
open Data_structures
open Sv_def

open Graph_sig
open Ind_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

(** Partial inclusion tests into inductive and segment edges *)

val is_le_ind:
    submem: bool (* sub-mem is_le is slightly different: no alloc check *)
  -> ind (* inductive definition to use for weakening *)
    -> graph (* graph to weaken *)
      -> sv (* index of the node from which to weaken *)
        -> vsat: vsat   (* satisfiability test function, left arg *)
          -> is_le_res * ind_edge (* * freshset *)

val is_le_seg:
    submem: bool (* sub-mem is_le is slightly different: no alloc check *)
  -> ind (* inductive definition to use for weakening *)
    -> graph (* graph to weaken *)
      -> sv (* source node from which to weaken *)
        -> sv (* destination node up to which to weaken *)
          -> vsat: vsat   (* satisfiability test function, left arg *)
              -> is_le_res * seg_edge * freshset
