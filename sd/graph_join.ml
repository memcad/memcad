(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_join.ml
 **       graph join algorithm
 ** Xavier Rival, 2011/07/25 *)
open Data_structures
open Lib
open Offs
open Sv_def

open Ast_sig
open Graph_sig
open Graph_encode
open Ind_sig
open Inst_sig
open Nd_sig
open Seq_sig
open Set_sig
open Vd_sig

open Gen_dom
open Gen_join

open Graph_utils
open Ind_utils
open Inst_utils
open Nd_utils
open Set_utils
open Sv_utils

open Graph_algos
open Graph_inst


(** Error report *)
module Log =
  Logger.Make(struct let section = "g_join__" and level = Log_level.DEBUG end)


(** Join algorithm state *)
(* State *)
type join_config =
    { (** Iteration strategy *)
      (* Rules that could be applied right away *)
      j_rules:  rules;
      (** Arguments *)
      (* Arguments (left and right) *)
      j_inl:    graph;
      j_inr:    graph;
      j_jal:    join_arg;
      j_jar:    join_arg;
      (* Satisfaction of boolean|set predicates *)
      j_vsat_l:  vsat;
      j_vsat_r:  vsat;
      (** Relations among arguments *)
      (* Relation between arguments/output nodes *)
      j_rel:    node_relation;
      (* Hint: nodes that should be treated in priority *)
      j_hint:   (sv * sv -> bool);
      (* lint: nodes address should forget value *)
      j_lint:   (Offs.svo * Offs.svo -> bool);
      (* relation pair been dropped XR=>HS: explain more *)
      j_drel:   SvPrSet.t;
      (* instantiation constraints, following from is_le facts *)
      j_inst_l:   join_inst;
      j_inst_r:   join_inst;
      (* temporary fields for set instantiation
       *  collection of constraints of the form
       *       sv \in setv
       *    !! to check     setv empty segment in the other side...
       *    !! to check     monotone parameter...
       * this should be made part of a more general type later...
       *)
      j_tmp_setinst_l: (sv * sv) list ;
      j_tmp_setinst_r: (sv * sv) list ;
      (** Outputs *)
      (* Output under construction *)
      j_out:    graph;
      (** Parameters about the is_le semantics *)
      (* Whether we consider a sub-memory: no alloc check for sub-memories *)
      j_submem: bool; }

(** Aborting a rule *)
exception Abort_rule_skip of string
exception Abort_rule_cont of string * join_config


(** Pretty-printing of configurations *)
let join_config_fpri ?(pp_rules: bool = false) (ind: string) (msg: string)
    (fmt: form) (j: join_config): unit =
  let nind0 = F.asprintf "  %s" ind in
  let nind1 = F.asprintf "   %s" ind in
  let fpi = graph_fpri nind0 in
  F.fprintf fmt "%s----------------\n" ind;
  F.fprintf fmt "%s%s:\n" ind msg;
  F.fprintf fmt "%s- L:\n%a%a\n%s- R:\n%a%a\n%s- O:\n%a%s- M:\n%a"
    ind fpi j.j_inl (Graph_encode.fpri nind0) j.j_jal.abs_go
    ind fpi j.j_inr (Graph_encode.fpri nind0) j.j_jar.abs_go
    ind fpi j.j_out ind (Nrel.nr_fpri nind0)  j.j_rel;
  F.fprintf fmt "%s- Set instantiation left:\n%a"
    ind (Inst_utils.set_colv_inst_fpri nind1) j.j_inst_l.set_inst;
  F.fprintf fmt "%s- Set instantiation right:\n%a"
    ind (Inst_utils.set_colv_inst_fpri nind1) j.j_inst_r.set_inst;
  F.fprintf fmt "%s- Seq instantiation left:\n%a"
    ind (Inst_utils.seq_colv_inst_fpri nind1) j.j_inst_l.seq_inst;
  F.fprintf fmt "%s- Seq instantiation right:\n%a"
    ind (Inst_utils.seq_colv_inst_fpri nind1) j.j_inst_r.seq_inst;
  F.fprintf fmt "%s- Node instantiation left:\n%a"
    ind (Inst_utils.sv_inst_fpri nind1) j.j_inst_l.sv_inst;
  F.fprintf fmt "%s- Node instantiation right:\n%a"
    ind (Inst_utils.sv_inst_fpri nind1) j.j_inst_r.sv_inst;
  if !Flags.flag_dbg_join_strategy && pp_rules then
    F.fprintf fmt "%a" rules_fpr j.j_rules;
  F.fprintf fmt "%s----------------\n" ind


(** Operations on configuration: managing mappings *)
(* Find node the result node corresponding to a pair of nodes in the
 * arguments if it exists;
 * if there is no such image node, allocate one and return it together
 * with the extended configuration *)
let find_in_config (p: sv * sv) (j: join_config): join_config * sv =
  try j, Nrel.find j.j_rel p
  with Not_found ->
    let alloc, typ, att, prio =
      try
        let nl = node_find (fst p) j.j_inl in
        let nr = node_find (snd p) j.j_inr in
        let a = if nl.n_alloc = nr.n_alloc then nl.n_alloc else Nnone in
        let t = ntyp_merge nl.n_t nr.n_t in
        a, t, node_attribute_lub nl.n_attr nr.n_attr, nl.n_ptprio && nr.n_ptprio
      with
      | Failure _ | Not_found ->
          Log.fatal_exn "find_in_config: node not found (%a,%a)"
            sv_fpr (fst p) sv_fpr (snd p) in
    let ni, nout = sv_add_fresh ~attr:att ~pt_prio:prio typ alloc j.j_out in
    let nrel = Nrel.add j.j_rel p ni in
    { j with
      j_out = nout ;
      j_rel = nrel }, ni

(* Find in config, loose version:
 * may drop relation in "side", if the other node has already a mapping
 *  - e.g., if a mapping (3,8) exists and side=Rgh, then
 *    find_in_config_looose (3,9) will return (3,8)
 *  - in the same situation but when (3,6), (3,8) exist in the mapping, the
 *    result is non-deterministic
 *  - when no such mapping exists, it behaves exactly like find_in_config
 * This function can be used when mapping parameters of an inductive predicate
 * that can be ignored (e.g., prev parameter of a dll definition, for the empty
 * rule).
 *)
let find_in_config_loose
    (side: side)         (* side that can be dropped *)
    ((il,ir) as p: sv * sv) (* candidate mapping *)
    (j: join_config): join_config * sv =
  let loc_debug = false in
  let io, is = get_sides side p in
  (* the side shows the empty side, thus, we should take the parameter
   * from the other side *)
  let s = Nrel.siblings (other_side side) io j.j_rel in
  if loc_debug then
    Log.info "find_in_config_loose_%a %a, (%a,%a): { %a }"
      side_fpr side sv_fpr io sv_fpr il sv_fpr ir nsvset_fpr s;
  if s = SvSet.empty then
    (* no solution: revert back to the classical find_in_config *)
    find_in_config p j
  else
    (* at least one solution (possibly many): pick one *)
    let pnew = get_sides side (io, SvSet.min_elt s) in
    let j, inew = find_in_config pnew j in
    if loc_debug then
      Log.info "find_in_config_loose_%a return: %a" side_fpr side sv_fpr inew;
    j, inew

(* Read in node relation; will not modify it (crash if not there) *)
let read_in_config (p: sv * sv) (j: join_config): sv =
  try Nrel.find j.j_rel p
  with Not_found -> Log.fatal_exn "read_in_config: not_found"

(* combine int or pointer parameters from the inputs to the out put *)
let f_combine_side (args: sv list) (argo: sv list) (arg: sv list)
    (rel: node_relation) (side: side): node_relation =
  let l_pairs =
    let al, ar =
      match side with
      | Lft -> args, argo
      | Rgh -> argo, args in
    List.map2 (fun x y -> x, y) al ar in
  List.fold_left2 Nrel.add rel l_pairs arg

(** Management of applicable rules *)
(* Computes the next applicable rules at a node *)
let collect_rules_node_gen
    (is_prio: sv * sv -> bool) (* whether pt rules are prioritary (init) *)
    (il: sv) (ir: sv) (* nodes in both inputs *)
    (jc: join_config): join_config =
  let is_seg_ext =
    Graph_utils.is_bseg_ext jc.j_jal.abs_go il jc.j_jar.abs_go ir in
  let is_seg_intro =
    Graph_utils.is_bseg_intro jc.j_jal.abs_go il jc.j_jar.abs_go ir in
  let jr =
    collect_rules_sv_gen is_prio is_seg_ext is_seg_intro jc.j_rel
      il (sv_kind il jc.j_inl) ir (sv_kind ir jc.j_inr) jc.j_rules in
  { jc with j_rules = jr }
let collect_rules_node (il: sv) (ir: sv) (jc: join_config): join_config =
  collect_rules_node_gen
    (* let node in encoded graph with high priority  *)
    (fun (il,ir) ->
       encode_node_find il jc.j_jal && encode_node_find ir jc.j_jar ||
       try ( (SvMap.find il jc.j_inl.g_g).n_ptprio &&
             (SvMap.find ir jc.j_inl.g_g).n_ptprio    )
       with _ -> false
    )
    il ir jc
(* Computes the set of rules at initialization *)
let init_rules (jc: join_config): join_config =
  SvPrMap.fold
    (fun (il, ir) _ ->
      collect_rules_node_gen jc.j_hint il ir
    ) jc.j_rel.n_inj jc


(** Utility functions for the is_le rules *)

(* Unification of block fragmentations for array analyses
 * Inputs two states with blocks that have different numbers of elements
 *    and performs some abstraction on their structure, to make them
 *    "similar", so that the join algorithm can continue
 * Returns:
 *  - a new join configuration
 *  - a pair of unified blocks *)
type extracted_block = (Bounds.t * Offs.svo * Offs.size) list
let block_array_unify
    (stride: int) (* array, attribute determined stride *)
    (* for each side: node, size, fragmentation of the block *)
    (nl: node) (sl: int) (mcl: pt_edge Block_frag.t) (* left node and block *)
    (nr: node) (sr: int) (mcr: pt_edge Block_frag.t) (* right node and block *)
    (j: join_config) (* join configuration *)
    : join_config * (pt_edge Block_frag.t * pt_edge Block_frag.t) =
  (* assertion that the right side is initially finer than the left one *)
  let sdelta = sr - sl in
  assert (0 < sdelta);
  (* utility function: checks whether the head offset is on stride *)
  let has_first_on_stride (mc: pt_edge Block_frag.t): bool =
    Bounds.is_on_stride stride (Block_frag.first_bound mc) in
  (* 1. collect sdelta elements in the right + 1 *)
  let bnd_low_r = Block_frag.first_bound mcr in
  let ext_r: extracted_block ref = ref [] in
  let mcr_r: pt_edge Block_frag.t ref = ref mcr in
  let span_r: Offs.size ref = ref Offs.size_zero in
  let shift_r () = (* take one element in the right state and move it out *)
    let (b, e), rem = Block_frag.extract_rem_first !mcr_r in
    mcr_r := rem;
    ext_r := (b, e.pe_dest, e.pe_size) :: !ext_r;
    span_r := Offs.size_add_size !span_r e.pe_size in
  for i = 0 to sdelta do (* repeat this for sdelta + 1 elements *)
    shift_r ()
  done;
  (* enriching right hand side graph *)
  let sum_r, tr = sv_add_fresh Ntraw nr.n_alloc j.j_inr in
  let sum_pt_r = { pe_size = Offs.size_of_int 0;
                   pe_dest = sum_r, Offs.zero } in
  let svcol_r = { svcol_base   = nr.n_i;
                svcol_stride = stride;
                svcol_block  = []; (* still to compute *)
                svcol_extptr = Offs.OffSet.empty } in
  (* 2. look whether the right side has size 1 *)
  if sl = 1 then
    let svcol_r = { svcol_r with svcol_block = List.rev !ext_r } in
    let sum_pt_r = { sum_pt_r with pe_size = !span_r } in
    let subm =
      { j.j_inst_r.subm_mod with
        sub_fold = SvMap.add sum_r svcol_r j.j_inst_r.subm_mod.sub_fold } in
    let j = { j with
              j_inr   = tr;
              j_inst_r = { j.j_inst_r with subm_mod = subm } } in
    j, (mcl, Block_frag.append_head bnd_low_r sum_pt_r !mcr_r)
  else
    begin
      (* 3. collect elements up to stride in the right *)
      while not (has_first_on_stride !mcr_r) do
        shift_r ()
      done;
      (* 4. do the same in the left side *)
      let bnd_low_l = Block_frag.first_bound mcl in
      let ext_l: extracted_block ref = ref [] in
      let mcl_l: pt_edge Block_frag.t ref = ref mcl in
      let span_l: Offs.size ref = ref Offs.size_zero in
      let shift_l () =
        let (b, e), rem = Block_frag.extract_rem_first !mcl_l in
        mcl_l := rem;
        ext_l := (b, e.pe_dest, e.pe_size) :: !ext_l;
        span_l := Offs.size_add_size !span_l e.pe_size in
      shift_l ();
      while not (has_first_on_stride !mcl_l) do
        shift_l ()
      done;
      (* 5. enrich graphs and produce result *)
      let svcol_r = { svcol_r with svcol_block = List.rev !ext_r } in
      let sum_pt_r = { sum_pt_r with pe_size = !span_r } in
      let subm_r =
        { j.j_inst_r.subm_mod with
          sub_fold = SvMap.add sum_r svcol_r j.j_inst_r.subm_mod.sub_fold } in
      let sum_l, tl = sv_add_fresh Ntraw nl.n_alloc j.j_inl in
      let svcol_l = { svcol_base   = nl.n_i;
                      svcol_stride = stride;
                      svcol_block  = List.rev !ext_l;
                      svcol_extptr = Offs.OffSet.empty } in
      let sum_pt_l = { pe_size = !span_l;
                       pe_dest = sum_l, Offs.zero } in
      let subm_l =
        { j.j_inst_l.subm_mod with
          sub_fold = SvMap.add sum_l svcol_l j.j_inst_l.subm_mod.sub_fold } in
      let j = { j with
                j_inl   = tl;
                j_inr   = tr;
                j_inst_l = { j.j_inst_l with subm_mod = subm_l };
                j_inst_r = { j.j_inst_r with subm_mod = subm_r } } in
      j, (Block_frag.append_head bnd_low_l sum_pt_l !mcl_l,
          Block_frag.append_head bnd_low_r sum_pt_r !mcr_r)
    end

(* Unification of bounds and offsets:
 *  -> may augment the configuration with new mapping
 *  -> returns a new bound, with symbolic variables in the join
 *     result naming space *)
let unify_gen
    (a_fpr: form -> 'a -> unit)
    (unifier: 'a -> 'a -> (sv * sv * sv) list * 'a)
    (renamer: sv SvMap.t -> 'a -> 'a)
    (xl: 'a) (xr: 'a) (j: join_config)
    : join_config * 'a =
  let uni, b_new = unifier xl xr in
  let j, index =
    List.fold_left
      (fun (j, acc) (svl, svr, svtemp) ->
        let j, svnew = find_in_config (svl, svr) j in
        j, SvMap.add svtemp svnew acc
      ) (j, SvMap.empty) uni in
  let x_new = renamer index b_new in
  j, x_new
let unify_bounds (bl: Bounds.t) (br: Bounds.t) (j: join_config)
    : join_config * Bounds.t =
  unify_gen Bounds.t_fpr Bounds.unify Bounds.rename bl br j
let unify_offsets (offl: Offs.t) (offr: Offs.t) (j: join_config)
    : join_config * Offs.t =
  if Offs.t_is_const offl
      && Offs.t_is_const offr
      && Offs.compare offl offr = 0 then
    (* fast unification of constant offsets *)
    j, offl
  else
    (* non constant offsets, more complex unification algorithm *)
    let unifier o0 o1 =
      match Offs.t_unify o0 o1 with
      | None ->
          (* replaces this with a variable offset:
           *  - variable offsets should be stored in a special map
           *  - renaming of those offsets will require special care...
           * risk: to perform this operation while trying to compensate
           *       another precision issue *)
          raise Stop
      | Some (uni, o) -> uni, o in
    try unify_gen Offs.t_fpr unifier Offs.t_rename offl offr j
    with
    | Stop ->
        (* "normal" unification failed;
         * we switch to a less accurate offset unification, where a new
         * offset is generated (or reused), that consists only in a single
         * variable and is reused later *)
        assert (j.j_inst_l.subm_mod.sub_new_off = [ ]);
        assert (j.j_inst_r.subm_mod.sub_new_off = [ ]);
        let n, out = sv_add_fresh ~attr: Attr_none Ntint Nnone j.j_out in
        let ores = Offs.of_n_expr (Ne_var n) in
        if !Flags.flag_dbg_join_shape then
          Log.info "unify_offsets,binding %a,%a => %a"
            Offs.t_fpr offl Offs.t_fpr offr Offs.t_fpr ores;
        let subm_l =
          { j.j_inst_l.subm_mod with
            sub_new_off = (offl,n,ores) :: j.j_inst_l.subm_mod.sub_new_off } in
        let subm_r =
          { j.j_inst_r.subm_mod with
            sub_new_off = (offr,n,ores) :: j.j_inst_r.subm_mod.sub_new_off } in
        { j with
          j_inst_l = { j.j_inst_l with subm_mod = subm_l };
          j_inst_r = { j.j_inst_r with subm_mod = subm_r };
          j_out   = out },
        ores
let unify_sizes (sl: Offs.size) (sr: Offs.size) (j: join_config)
    : join_config * Offs.size =
  if Offs.size_is_const sl
      && Offs.size_is_const sr
      && Offs.is_zero (Offs.of_size (Offs.size_sub_size sl sr)) then
    j, sl (* sizes are already fixed constants and unified *)
  else
    let unifier o0 o1 =
      match Offs.size_unify sl sr with
      | None ->
          begin
            match Offs.size_to_int_opt o0, Offs.size_to_int_opt o1 with
            | Some c, None ->
                let cons = Nc_cons (Apron.Tcons1.EQ, Ne_csti c,
                                    Offs.size_to_n_expr o1) in
                if j.j_vsat_r.vs_num cons then
                  [ ], Offs.size_of_int c
                else
                  Log.fatal_exn "unify_sizes: none found, const case"
            | _, _ ->
                Log.fatal_exn "unify_size: none found, nonconst case [%a] [%a]"
                  Offs.size_fpr sl Offs.size_fpr sr
          end
      | Some (uni, sz) -> uni, sz in
    unify_gen Offs.size_fpr unifier Offs.size_rename sl sr j

(* Try to guess an inductive definition from a points-to edges map *)
let block_inductive_candidate
    (msg: string)
    (g: graph)        (* graph used to bound a set of inductives *)
    (g_out: graph)
    (g_other: graph)
    (is_seg: bool) (* is it for a segment ? *)
    (mcr: pt_edge Block_frag.t) =
  (* inductives which are compatible with the points-to edges *)
  let m = Graph_strategies.extract_compatible_inductives false mcr in
  (* filtering candidates used in the domain *)
  let m = StringMap.filter (fun s ind -> StringSet.mem s g.g_inds) m in
  if !Flags.flag_dbg_join_shape then
    StringMap.iter (fun s _ -> Log.info "block_inductive_candidate: %s" s) m;
  let l = StringMap.fold (fun _ ind acc -> ind :: acc) m [ ] in
  match l with
  | [ ind ] ->
      if !Flags.flag_dbg_join_shape then
        Log.info "block_inductive_candidate: selected %s" ind.i_name ;
      ind
  | hd :: tl ->
      (* use of the precedence directive to choose an order... *)
      let l =
        match !Ind_utils.ind_prec with
        | [ ] -> l
        | _ ->
            let l_prec =
              List.fold_left
                (fun acc s ->
                  try StringMap.find s m :: acc with Not_found -> acc
                ) [ ] (List.rev !Ind_utils.ind_prec) in
            if l_prec = [] then l
            else l_prec in
      if List.length l = 1 then
        List.hd l
      else
        let use_graph g =
          let inds_in = collect_inds g in
          let l = List.filter (fun ind -> StringSet.mem ind.i_name inds_in) l in
          match l with
          | [] -> None
          | [ind] -> Some ind
          | _::_::_ ->
              let msg = msg^" Too may candidates after seraching in graph" in
              raise (Abort_rule_skip msg) in
        begin
          (* Since the hint inductive may have alredy benn consumed,
           * We look at output graph and other side graph if we cannot find *)
          match use_graph g, use_graph g_out, use_graph g_other with
          | Some ind, _, _
          | None, Some ind, _
          | None, None, Some ind -> ind
          | None, None, None ->
              let msg = msg^": Cannot find inductitive candidates in graph" in
              raise (Abort_rule_skip msg)
        end
  | _ ->
      let msg = Printf.sprintf "%s: too many/not enough candidates" msg in
      raise (Abort_rule_skip msg)


(** utilities for split-ind *)
let split_indedge_pars (ie: ind_edge) (id: sv) (g: graph)
    : graph * seg_edge * ind_edge * set_cons list =
  let loc_debug = false in
  (* TODO: support all types of parameters *)
  (* TODO: we do not mark the removed parameters correctly ! *)
  assert (ie.ie_args.ia_ptr = [ ]);
  assert (ie.ie_args.ia_int = [ ]);
  assert (ie.ie_args.ia_seq = [ ]);
  (* create a new series of set parameters *)
  if loc_debug then
    Log.info "tmp,split_indedge_pars,status:\n%a" (graph_fpri "  ") g;
  let _, g, seg_a_set, ind_a_set, set_cons =
    List.fold_left
      (fun (i, g, seg_a_set, ind_a_set, set_cons) cv ->
        let k = IntMap.find i ie.ie_ind.i_pkind.pr_set in
        let cv0, g = colv_add_fresh (Ct_set (Some k)) g in
        let cv1, g = colv_add_fresh (Ct_set (Some k)) g in
        if loc_debug then
          Log.info "tmp,fresh: %a, %a" setv_fpr cv0 setv_fpr cv1;
        (* TODO: we could share much of this code *)
        let ctrs =
          if k.st_const then
            [ S_eq (S_var cv, S_var cv0) ; S_eq (S_var cv, S_var cv1) ]
          else if k.st_add || k.st_head then
            [ S_eq (S_var cv, S_uplus (S_var cv0, S_var cv1)) ]
          else Log.todo_exn "unhandled parameter kind" in
        i + 1, g, cv0 :: seg_a_set, cv1 :: ind_a_set, ctrs @ set_cons
      ) (0, g, [ ], [ ], [ ]) ie.ie_args.ia_set in
  (* build up both edges *)
  let seg =
    { se_ind   = ie.ie_ind ;
      se_eargs = { ind_args_s_empty with ias_set = List.rev seg_a_set } ;
      se_sargs = ind_args_empty ;
      se_dargs = ind_args_empty ;
      se_dnode = id } in
  let ind =
    { ie_ind   = ie.ie_ind ;
      ie_args  = { ind_args_empty with ia_set = List.rev ind_a_set } } in
  g, seg, ind, set_cons
let split_ind_or_seg ~(is: sv) ~(id: sv) (j: join_config): join_config =
  let loc_debug = false in
  (* TODO: for now, only sets are supported !!! *)
  let n_is = node_find is j.j_out in
  let n_id = node_find id j.j_out in
  let aux ie j =
    (* 1. get the split prepared *)
    let jout, cseg, cind, set_cons = split_indedge_pars ie id j.j_out in
    (* 2. add the new COLVs into the instantiation *)
    let set_colv_inst_fix setinst =
      if loc_debug then
        begin
          Log.info "tmp,prefix:\n%a" (set_colv_inst_fpri "  ") setinst;
          Log.info "tmp,n_is:\n%atmp,cind: %a\ntmp,cseg: %a"
            (node_fpri "  ") n_is ind_edge_fpr cind seg_edge_fpr cseg;
        end;
      let setinst = colv_inst_add_pars cind.ie_args.ia_set setinst in
      let setinst = colv_inst_add_pars cseg.se_eargs.ias_set setinst in
      let setinst = colv_inst_rem_pars ie.ie_args.ia_set setinst in
      if loc_debug then
        Log.info "tmp,postfix:\n%a" (set_colv_inst_fpri "  ") setinst;
      { setinst with colvi_props = set_cons @ setinst.colvi_props } in
    let join_inst_fix jinst =
      { jinst with
        (* TODO: other fields, left out for now *)
        set_inst = set_colv_inst_fix jinst.set_inst } in
    (* 3. modify the edges *)
    if loc_debug then
      Log.info "tmp,dbg: is:%a, id:%a\n%a"
        nsv_fpr is nsv_fpr id (graph_fpri " ") jout;
    let jout = ind_edge_rem is jout in
    let jout = ind_edge_add id cind jout in
    let jout = seg_edge_add is cseg jout in
    { j with
      j_out    = jout;
      j_inst_l = join_inst_fix j.j_inst_l ;
      j_inst_r = join_inst_fix j.j_inst_r } in
  match n_is.n_e, n_id.n_e with
  | Hind ie, Hemp ->
      (* TODO: support all types of parameters *)
      assert (ie.ie_args.ia_ptr = [ ]);
      assert (ie.ie_args.ia_int = [ ]);
      assert (ie.ie_args.ia_seq = [ ]);
      aux ie j
  | _, _ -> Log.todo_exn "split_ind_or_seg other case"


(** Individual join rules *)

(* pt-pt [par ptr OK, par int OK]
 *   consume edges and produce a matching one *)
let apply_pt_pt
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = node_find isl j.j_inl in
  let nr = node_find isr j.j_inr in
  match nl.n_e, nr.n_e with
  | Hpt mcl, Hpt mcr ->
      let sz_l = Block_frag.cardinal mcl and sz_r = Block_frag.cardinal mcr in
      (* If array, we may have to make the blocks compatible *)
      let j =
        if sz_l != sz_r then
          let j, (ul, ur) =
            match nl.n_attr, nr.n_attr with
            | Attr_array (stride_l, _), Attr_array (stride_r, _) ->
                if stride_l = stride_r then
                  block_array_unify stride_l nl sz_l mcl nr sz_r mcr j
                else Log.fatal_exn "unify: %d, %d" stride_l stride_r
            | Attr_array _, _ | _, Attr_array _ -> Log.fatal_exn"Unify part"
            | Attr_none, Attr_none ->
                raise (Abort_rule_skip "no attribute in both sides")
            | _ ->
                Log.fatal_exn "Other case of unify %a, %a"
                  node_attribute_fpr nl.n_attr node_attribute_fpr nr.n_attr in
          Block_frag.fold_list2_bound2
            (fun (osl, el) (osr, er) pl pr j ->
              if !Flags.flag_debug_array_blocks then
                Log.info "Pt-pt,offs:\n -L: %a (%a) -> %a\n -R: %a (%a) -> %a"
                  Bounds.t_fpr osl Offs.size_fpr pl.pe_size Bounds.t_fpr el
                  Bounds.t_fpr osr Offs.size_fpr pr.pe_size Bounds.t_fpr er;
              (* Unification of the lower bound and of the size *)
              let j, os = unify_bounds osl osr j in
              let j, usize =
                let sl = Bounds.sub_t el osl
                and sr = Bounds.sub_t er osr in
                let j, usize = unify_bounds sl sr j in
                j, Offs.to_size (Bounds.to_offs usize) in
              (* Computation of the new edge *)
              let dl_n, dl_o = pl.pe_dest
              and dr_n, dr_o = pr.pe_dest in
              assert (Offs.compare dl_o dr_o = 0);
              let j, id = find_in_config (dl_n, dr_n) j in
              let pe = { pe_size = usize;
                         pe_dest = id, dl_o } in
              let out =
                pt_edge_block_append ~nochk: true (is, os) pe j.j_out in
              collect_rules_node dl_n dr_n { j with j_out = out }
            ) ul ur j
        else
          Block_frag.fold_list2_base
            (fun osl osr (pl: pt_edge) (pr: pt_edge) j ->
              let onl, onr =
                (isl, Bounds.to_offs osl), (isr, Bounds.to_offs osr) in
              let j, u_os = unify_bounds osl osr j in
              let j, u_size = unify_sizes pl.pe_size pr.pe_size j in
              let dl_n, dl_o = pl.pe_dest
              and dr_n, dr_o = pr.pe_dest in
              let j, dd_o = unify_offsets dl_o dr_o j in
              let j, id = find_in_config (dl_n, dr_n) j in
              let pe = { pe_size = u_size ;
                         pe_dest = id, dd_o } in
              (* comsume the pt edge in encoded graph *)
              let j = { j with
                        j_jal = encode_pt_pt onl (dl_n, dl_o) j.j_jal;
                        j_jar = encode_pt_pt onr (dr_n, dr_o) j.j_jar; } in
              (* XR=>HS: explain these cases *)
              let is_prio =
                encode_node_find dl_n j.j_jal &&
                encode_node_find dr_n j.j_jar in
              if !Flags.sel_widen && j.j_lint (onl, onr) && (not is_prio) then
                { j with
                  j_out = pt_edge_block_append (is, u_os) pe j.j_out;
                  j_drel = SvPrSet.add (dl_n, dr_n) j.j_drel }
              else
                collect_rules_node dl_n dr_n
                  { j with
                    j_out = pt_edge_block_append (is, u_os) pe j.j_out }
            ) mcl mcr j in
      let vrules = invalidate_rules isl isr Ipt Ipt j.j_rules in
      { j with
        j_inl   = pt_edge_block_destroy isl j.j_inl;
        j_inr   = pt_edge_block_destroy isr j.j_inr;
        j_rules = vrules; }
  | _, _ -> (* we may check the rule was not invalidated ? *)
      Log.fatal_exn"pt-pt: improper case"

(* ind-ind [par ptr OK, par int OK]
 *   consume edges and produce a matching one *)
let apply_ind_ind
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = node_find isl j.j_inl in
  let nr = node_find isr j.j_inr in
  match nl.n_e, nr.n_e with
  | Hind iel, Hind ier ->
      assert (List.length iel.ie_args.ia_int =
              List.length ier.ie_args.ia_int);
      assert (List.length iel.ie_args.ia_ptr =
              List.length ier.ie_args.ia_ptr);
      assert (List.length iel.ie_args.ia_set =
              List.length ier.ie_args.ia_set);
      assert (List.length iel.ie_args.ia_seq =
              List.length ier.ie_args.ia_seq);
      if Ind_utils.compare iel.ie_ind ier.ie_ind = 0 then
        (* check if inductive edge is empty *)
        let bign_l = Ind_utils.no_par_use_rules_emp iel.ie_ind
            && j.j_vsat_l.vs_num
            (Nc_cons (Apron.Tcons1.EQ, Ne_var isl, Ne_csti 0)) in
        let bign_r = Ind_utils.no_par_use_rules_emp ier.ie_ind
            && j.j_vsat_r.vs_num
            (Nc_cons (Apron.Tcons1.EQ, Ne_var isr, Ne_csti 0)) in
        let l_pargs, j =
	  (* when one inductive edge is empty but not the other *)
          if (bign_l || bign_r) && (bign_l != bign_r) then
          (* JG: The first condition is implied by the second one ! *)
            let side = if bign_l then Lft else Rgh in
            List.fold_left2
              (fun (acc, j) ial iar ->
                let io, is = get_sides side (ial, iar) in
                Log.info "search (%a,%a), could advantage %a"
                  sv_fpr ial sv_fpr iar sv_fpr io;
                let j, ni =
                  try
                    let _ = Nrel.find j.j_rel (ial, iar) in
                    find_in_config (ial, iar) j
                  with Not_found ->
                    find_in_config_loose side (ial, iar) j in
                ni :: acc, j
              ) ([ ], j) iel.ie_args.ia_ptr ier.ie_args.ia_ptr
          else
            List.fold_left2
              (fun (acc_l, acc_j) ial iar ->
                let nj, ia = find_in_config (ial, iar) acc_j in
                ia :: acc_l, nj
              ) ([ ], j) iel.ie_args.ia_ptr ier.ie_args.ia_ptr in
        (* Construction of a new inductive edge for the pair graph *)
        let iptl = ind_seg_edge_int_par_typ isl iel j.j_vsat_l.vs_num in
        let iptr = ind_seg_edge_int_par_typ isr ier j.j_vsat_r.vs_num in
        let ie, out, ___fs =
          ind_edge_make iel.ie_ind.i_name ~srcptr:(List.rev l_pargs) j.j_out in
        let j_inl, inst_l, wk_intargs_l =
          l_call_inst ~skipset:true iel.ie_args j.j_inl ie.ie_args j.j_inst_l
            ~pr:iptl in
        let j_inr, inst_r, wk_intargs_r =
          l_call_inst ~skipset:true ier.ie_args j.j_inr ie.ie_args j.j_inst_r
            ~pr:iptr in
        let rel =
          let Refl = Sv_def.eq in
          f_combine_side wk_intargs_l wk_intargs_r ie.ie_args.ia_int
            j.j_rel Lft in
        let vrules = invalidate_rules isl isr Iind Iind j.j_rules in
        let inst_l =
          let set_inst =
	    jinst_set_addcstr_icall_eqs iel.ie_args ie.ie_args inst_l.set_inst in
          let seq_inst =
	    jinst_seq_addcstr_icall_eqs iel.ie_args ie.ie_args inst_l.seq_inst in
          { inst_l with set_inst; seq_inst } in
        let inst_r =
          let set_inst =
	    jinst_set_addcstr_icall_eqs ier.ie_args ie.ie_args inst_r.set_inst in
          let seq_inst =
	    jinst_seq_addcstr_icall_eqs ier.ie_args ie.ie_args inst_r.seq_inst in
          { inst_r with set_inst; seq_inst } in
        { j with
          j_rules = vrules;
          j_inl   = ind_edge_rem isl j_inl;
          j_inr   = ind_edge_rem isr j_inr;
          j_out   = ind_edge_add is ie out;
          j_inst_l = inst_l;
          j_inst_r = inst_r;
          j_rel   = rel }
      else Log.todo_exn "join: inductive edges fails"
  | _, _ -> (* we may check the rule was not invalidated ? *)
      Log.fatal_exn"ind-ind: improper case"

(* ind-weak [par ptr OK, par int OK]
 * ind-weak: weakening the right side to match an inductive in the left side
 * weak-ind: weakening the left side to match an inductive in the right side *)
let apply_gen_ind_weak
    (side: side) (* side of the rule (weakening side) *)
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let rname = sel_side side ("ind-weak", "weak-ind") in
  let iso, iss = get_sides side (isl, isr) in
  let ino, ins = get_sides side (j.j_inl, j.j_inr) in
  (* r -> side; l -> other side *)
  let ns = node_find iss ins in
  match ns.n_e with
  | Hind ies ->
      let vsato, vsats = get_sides side (j.j_vsat_l, j.j_vsat_r) in
      let insto, insts = get_sides side (j.j_inst_l, j.j_inst_r) in
      (* construction of a graph with just one inductive edge *)
      (* experimental:
       * integer arguments of the inductive are special nodes
       * that might be instantiated *)
      let le_res, ieo =
        Graph_algos.is_le_ind ~submem:j.j_submem ies.ie_ind ino iso
          ~vsat:vsato in
      begin
        match le_res with
        | Ilr_le_rem ilrem ->
            assert (ilrem.ilr_seqctr_r = []);
            (* addition of instantiations of the nodes of "other side" *)
            let j =
              match side with
              | Lft ->
                  let vrules =
                    invalidate_rules isl isr (Iblob (SvSet.singleton isl))
                      (Iblob ilrem.ilr_svrem_l) j.j_rules in
                  { j with
                    j_rules = vrules;
                    j_inl   = ind_edge_rem isl j.j_inl;
                    j_inr   = ilrem.ilr_graph_l; }
              | Rgh ->
                  let vrules =
                    invalidate_rules isl isr (Iblob ilrem.ilr_svrem_l)
                      (Iblob (SvSet.singleton isr)) j.j_rules in
                  { j with
                    j_rules = vrules;
                    j_inl   = ilrem.ilr_graph_l;
                    j_inr   = ind_edge_rem isr j.j_inr; } in
            let make_eq i = Nc_cons (Apron.Tcons1.EQ, Ne_var i, Ne_csti 0) in
            let bign_s =
              Ind_utils.no_par_use_rules_emp ies.ie_ind
                && vsats.vs_num (make_eq iss) in
            let bign_o =
              Ind_utils.no_par_use_rules_emp ies.ie_ind
                && vsato.vs_num (make_eq iso) in
            if bign_s && bign_o then
              (* both inductive edges denote the empty store *)
              j
            else
              (* either of the edges denotes non empty stores *)
              let (ie, gout, ___fs), j =
                let revppars, j =
                  List.fold_left2
                    (fun (acc, j) is io ->
                      (* if bign_s, then we can ignore is, and look for a better
                       * choice in the configuration, if bign_o, then we can
                       * ignore io, and look for a better choice in the
                       * configuration *)
                      let nio, loose =
                        try SvMap.find io ilrem.ilr_svrtol, false
                        with Not_found -> io, true
                      in
                      let p = get_sides side (nio, is) in
                      if bign_s then
                        Log.info "search (%a,%a) (s), could advantage %a"
                          sv_fpr (fst p) sv_fpr (snd p) sv_fpr nio;
                      if bign_o || loose then
                        Log.info "search (%a,%a) (o), could advantage %a"
                          sv_fpr (fst p) sv_fpr (snd p) sv_fpr is;
                      let j, ni =
                        if bign_s then find_in_config_loose side p j
                        else if bign_o || loose then
                          find_in_config_loose (other_side side) p j
                        else find_in_config p j in
                      ni :: acc, j
                    ) ([ ], j) ies.ie_args.ia_ptr ieo.ie_args.ia_ptr in
                let ppars = List.rev revppars in
                ind_edge_make ies.ie_ind.i_name ~srcptr:ppars j.j_out, j in
              let ino, ins = get_sides side (j.j_inl, j.j_inr) in
              let insto, ieo_args_int =
                let set_inst =
                  Graph_inst.g_jinst_set_addctrs_post_isle ~msg:"indwk"
		    ~findkind:(setv_type ino) ~sarg_template:ieo.ie_args.ia_set
                    ~sarg_out:ie.ie_args.ia_set ~svmap:ilrem.ilr_svrtol
                    ~setvmap:ilrem.ilr_colvrtol ~sc:ilrem.ilr_setctr_r
                    ~inst:insto.set_inst in
                is_le_call_inst ieo.ie_args ie.ie_args { insto with set_inst }
                  ilrem.ilr_svinst None ilrem.ilr_seqinst ilrem.ilr_svrtol in
              let j_ins, insts, wk_intargs_s =
                l_call_inst ~skipset:true ies.ie_args ins ie.ie_args insts
                  ~pr:ies.ie_ind.i_pkind in
              let insts =
                let set_inst =
                  jinst_set_addcstr_icall_eqs
                    ies.ie_args ie.ie_args insts.set_inst in
                let seq_inst =
                  gen_jinst_seq_addcstr_icall_eqs
                    ies.ie_args.ia_seq ie.ie_args.ia_seq insts.seq_inst in
                { insts with set_inst ; seq_inst } in
              let rel =
                let l_pairs =
                  let (al : sv list), (ar : sv list) =
                    let Refl = Sv_def.eq in
                    match side with
                    | Lft -> wk_intargs_s, ieo_args_int
                    | Rgh -> ieo_args_int, wk_intargs_s in
                  List.map2 (fun x y -> x, y) al ar in
                List.fold_left2 Nrel.add j.j_rel l_pairs ie.ie_args.ia_int in
              let j =
                match side with
                | Lft ->
                    { j with
                      j_out    = ind_edge_add is ie gout;
                      j_inl    = j_ins;
                      j_rel    = rel;
                      j_inst_r = insto;
                      j_inst_l = insts;
                    }
                | Rgh ->
                    { j with
                      j_out    = ind_edge_add is ie gout ;
                      j_inr    = j_ins;
                      j_rel    = rel;
                      j_inst_l = insto;
                      j_inst_r = insts;
                    } in
              j
        | Ilr_not_le -> (* rule application failed *)
            if Flags.flag_join_bypass_fail_rules then
              j (* By-pass the rule *)
            else Log.fatal_exn "%s: fails" rname
        | Ilr_le_ind _ | Ilr_le_seg _ -> (* those cases should not happen *)
            Log.fatal_exn "%s: absurd is_le result" rname
      end;
  | _ -> (* the rule should probably have been invalidated; give up *)
      j
let apply_weak_ind = apply_gen_ind_weak Rgh
let apply_ind_weak = apply_gen_ind_weak Lft

(* seg-intro [par ptr OK, par int KO] *)
(* Issues left with rule seg-intro:
 *  - depending on the strategy, the siblings to a node may be
 *    considered several times in the course of the algorithm
 *    so a seg-intro may be triggered again; we should avoid that
 *    => this actually happens in several cases, causing by-pass
 *)
let apply_gen_seg_intro
    (side: side) (* side of the rule, i.e., where the empty chunk is found *)
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let loc_debug = false in
  (* side resolution:
   *  - "l" -> "s"
   *  - "r" -> "o" *)
  let rname    = sel_side side ("seg-intro-l", "seg-intro-r") in
  let iso, iss = get_sides side (isl, isr) in
  let cho, chs = get_sides side ('l', 'r') in
  let ino, ins = get_sides side (j.j_inl, j.j_inr) in
  let ego, egs = get_sides side (j.j_jal, j.j_jar) in
  let vsato, _ = get_sides side (j.j_vsat_l, j.j_vsat_r) in
  let insto, insts = get_sides side (j.j_inst_l, j.j_inst_r) in
  let no = node_find iso (sel_side side (j.j_inr, j.j_inl)) in
  (* when bailing out, add some splitting option in exp_jinst mode *)
  let rule_abort (msg: string) (j: join_config) =
    let jr =
      if !do_exp_jinst then
        match side with
        | Lft -> { j with j_rules = rules_add_splitind_l (isl, isr) j.j_rules }
        | Rgh -> { j with j_rules = rules_add_splitind_r (isl, isr) j.j_rules }
      else j in
    if loc_debug then
      Log.info "tmp,Rule %s aborts: %s (%b)" rname msg (jr == j);
    raise (Abort_rule_cont (Printf.sprintf "seg-intro:%s" msg, jr)) in
  (* determine inductive definition *)
  let ind = (* try to determine which inductive to synthesize *)
    let search_avail_def ( ) =
      match no.n_e with
      | Hpt mco -> block_inductive_candidate rname ins j.j_out ino true mco
      | Hseg se -> se.se_ind
      | _ -> rule_abort "seg-intro fails (ind not found)" j
          (*raise (Abort_rule_skip "seg-intro fails (ind not found)")*) in
    if Flags.flag_weaken_use_attribute then
      match no.n_attr with
      | Attr_none | Attr_array _ ->
          (* no attribute, try to search through available definitions *)
          search_avail_def ( )
      | Attr_ind iname ->
          (* attribute gives a hint of the inductive to select *)
          Ind_utils.ind_find iname
    else search_avail_def ( ) in
  (* extract other siblings *)
  let allsibl = sel_side side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
  let siblings =
    try SvMap.find iss allsibl
    with
    | Not_found ->
        (* this should not happen, as this rule should be triggered only
         * when there are siblings in the relation (so isl should be there) *)
        Log.fatal_exn "inconsistent mapping in %s" rname in
  assert (SvSet.mem iso siblings);
  let lcandidates =
    Graph_strategies.extract_segment_directions siblings ind.i_dirs ino in
  let candidates =
    List.fold_left
      (fun acc (src, dst) -> if src = iso then SvSet.add dst acc else acc)
      SvSet.empty lcandidates in
  let siblings = SvSet.remove iso siblings in
  let seg_end = Graph_utils.choose_dst iso ego.abs_go siblings in
  if !Flags.flag_dbg_join_shape then
    Log.info
      "  seg-int,siblings: { %a }\n  seg_end: { %a }\n  candidates: { %a }"
      nsvset_fpr siblings nsvset_fpr seg_end nsvset_fpr candidates;
  (* destination node in the other side *)
  let ido =
    let card_sib = SvSet.cardinal siblings in
    let card_can = SvSet.cardinal candidates in
    let card_end = SvSet.cardinal seg_end in
    if card_sib = 0 then Log.fatal_exn "no sibling";
    if card_sib > 1 && card_end = 1 then
      begin
        Log.info "seg-intro found too many siblings: %a ; picking end: %a"
          svset_fpr siblings sv_fpr (SvSet.min_elt seg_end);
        SvSet.min_elt seg_end
      end
    else if card_sib > 1 && card_can = 1 then
      begin
        Log.info "seg-intro found too many siblings: %a ; picking candidate: %a"
          svset_fpr siblings sv_fpr (SvSet.min_elt candidates);
        SvSet.min_elt candidates
      end
    else
      begin
        let n = SvSet.min_elt siblings in
        Log.info "seg-intro found too many siblings: %a ; picking %a"
          svset_fpr siblings sv_fpr n;
        n
      end in
  if !Flags.flag_dbg_join_shape then
    Log.info "seg-intro attempt: S%c:%a -> (S%c:%a,D%c:%a)"
      chs sv_fpr iss cho sv_fpr iso cho sv_fpr ido;
  (* destination node in the result *)
  let pd = get_sides side (ido, iss) in
  let id =
    try SvPrMap.find pd j.j_rel.n_inj
    with Not_found ->
      Log.fatal_exn "pair (%a,%a) not found" sv_fpr (fst pd) sv_fpr (snd pd) in
  let id_node = node_find id j.j_out in
  (* trigger a weakening attempt (to segment) *)
  let is_continue =
    let is_source = List.exists (fun (x, y) -> x = iso) lcandidates in
    let niss, nido  = node_find iss ins, node_find ido ino in
    let is_rule =
      match (niss.n_attr, niss.n_e), (nido.n_attr, nido.n_e) with
      (* compare the property of end nodes to see if the rule is ok to apply *)
      | (Attr_ind _, Hpt _), (Attr_none, Hemp) -> false
      | _, _ -> true in
    match lcandidates, is_source, is_rule with
    | (x, y)::_, false, _ ->
        raise (Abort_rule_skip "seg-intro: not the best source")
    | (x, y)::_, true, false -> false
    | (x, y)::_, true, true -> true
    | [ ], _, _ ->
        rule_abort "seg-intro: no pair of nodes" j
        (*raise (Abort_rule_skip "seg-intro: no pair of nodes")*) in
  if not is_continue then
    collect_rules_node_gen (fun (l, r) -> l = isl && r = isr) isl isr j
  else
    (* test of inclusion of a part of the right side into a segment edge *)
    let le_res, seo, _ =
      Graph_algos.is_le_seg ~submem:j.j_submem ind ino iso ido
        ~vsat:vsato in
    match le_res with
    | Ilr_le_rem ilrem ->
        assert (ilrem.ilr_seqctr_r = []);
        (* rule success; perform the weakening *)
        (* begin temp *)
        (* this code is ugly and works only for pointer parameters *)
        if !Flags.flag_dbg_join_shape then
          Log.info "Rule seg_intro: should work";
        (* mapping the pointer parameters of the new edge *)
        let find_args (j: join_config) (iao: sv list): sv list =
          List.map
            (fun io ->
              let nio =
                try SvMap.find io ilrem.ilr_svrtol
                with Not_found -> raise (Abort_rule_skip rname) in
              nio
            ) iao in
        hseg_sanity_check ~isok:0 "graph_join,apply_gen_seg_intro,1" seo;
        let ptr_osrc = find_args j seo.se_sargs.ia_ptr in
        let ptr_odst = find_args j seo.se_dargs.ia_ptr in
        (* Computing int parameters *)
        let int_osrc = find_args j seo.se_sargs.ia_int in
        let int_odst = find_args j seo.se_dargs.ia_int in
        (* when the destination node is null, we may need to modify the
         * parameter *)
        let ptr_odst =
          let id_pars_opt =
            match id_node.n_e with
            | Hpt _ | Hemp -> None
            | Hind ie -> Some ie.ie_args
            | Hseg se ->
                hseg_sanity_check ~isok:3 "graph_join,gen_seg_intro,2"seo;
                Some se.se_sargs in
          if vsato.vs_num
              (Nc_cons (Apron.Tcons1.EQ, Ne_var ido, Ne_csti 0)) then
            match id_pars_opt with
            | None -> ptr_odst
            | Some ind_pars ->
                List.map
                  (fun io -> snd (Nrel.find_p j.j_rel io)) ind_pars.ia_ptr
          else ptr_odst in
        (* function used to do something on integer and pointer arguments *)
        let resolve_args (insts: sv_inst) (ins: graph)
            (ia_osrc: sv list) (ia_odst: sv list) (is_ptr: bool)
            : sv list * graph * sv_inst =
          let resolve_arg (acc, i, ins, insts) iaso iado =
            let set = SvSet.add iaso (SvSet.singleton iado) in
            let candidates = Nrel.mult_bind_candidates side j.j_rel set in
            if !Flags.flag_dbg_join_shape then
              Log.info "Par =.(%a)==.(%a)=> candidates: %a (set %a)"
                sv_fpr iaso sv_fpr iado svset_fpr candidates
                svset_fpr set;
            let card = SvSet.cardinal candidates in
            let backup_fresh insts ins =
              let key, ins = sv_add_fresh Ntint Nnone ins in
              let sv_fresh = SvSet.add key insts.sv_fresh in
              if !Flags.flag_dbg_join_shape then
                Log.info "Backup fresh allocation: %a" sv_fpr key;
              key, ins, { insts with sv_fresh } in
            let par, ins, insts =
              if card = 0 then
                (* Backup solution:
                 * see if there is an inductive edge in ins at node iss
                 * if so, try to "guess" the parameter there, to be used
                 * for both ends of the (empty) segment being
                 * introduced *)
                let ns = node_find iss ins in
                match ns.n_e with
                | Hind ns_ie ->
                    if Ind_utils.compare ns_ie.ie_ind ind = 0 then
                      let par =
                        if is_ptr then List.nth ns_ie.ie_args.ia_ptr i
                        else List.nth ns_ie.ie_args.ia_int i in
                      if !Flags.flag_dbg_join_shape then
                        Log.info "seg-in backup par search => %a"
                          sv_fpr par;
                      par, ins, insts
                    else
                      Log.fatal_exn "backup parameter search failed (ind)"
                | Hseg ns_se ->
                    hseg_sanity_check ~isok:3
                      "graph_join,apply_gen_seg_intro,3"ns_se;
                    if Ind_utils.compare ns_se.se_ind ind = 0 then
                      let par =
                        if is_ptr then List.nth ns_se.se_sargs.ia_ptr i
                        else List.nth ns_se.se_sargs.ia_int i in
                      if !Flags.flag_dbg_join_shape then
                        Log.info "seg-in backup par search => %a"
                          sv_fpr par;
                      par, ins, insts
                    else
                      Log.fatal_exn"seg-in backup par search failed (seg)"
                | Hpt mc ->
                    (* If the inductive analysis did find out where
                     * a parameter may be stored try to retrieve it *)
                    if not is_ptr then backup_fresh insts ins else
                    let guessed_par =
                      try IntMap.find i ind.i_fl_pars
                      with Not_found ->
                        Log.fatal_exn
                          "backup parameter search failed (pt)" in
                    let guessed_edge =
                      Block_frag.find_addr
                        (Bounds.of_int guessed_par) mc in
                    fst guessed_edge.pe_dest, ins, insts
                | Hemp ->
                    (* in this case, the edge may have been already
                     * matched, thus, we can get some information from
                     * the out graph *)
                    let nd = node_find id j.j_out in
                    match nd.n_e with
                    | Hind ind_edge ->
                        if Ind_utils.compare ind_edge.ie_ind ind = 0 then
                          let par =
                            if is_ptr then List.nth ind_edge.ie_args.ia_ptr i
                            else List.nth ind_edge.ie_args.ia_int i in
                          let in_pars = SvMap.find par j.j_rel.n_pi in
                          let _, par_s =
                            get_sides side in_pars in
                          if !Flags.flag_dbg_join_shape then
                            Log.info "seg-in backup par search=> %a"
                              sv_fpr par_s;
                          par_s, ins, insts
                        else if not is_ptr then
                          backup_fresh insts ins
                        else
                          Log.fatal_exn
                            "backup parameter search failed (empty)"
                    | _ when not is_ptr -> backup_fresh insts ins
                    | _ ->
                        Log.fatal_exn
                          "backup parameter search failed (empty)"
              else if card = 1 then
                SvSet.min_elt candidates, ins, insts
              else
                Log.fatal_exn
                  "seg-intro: too many or not enough candidates" in
            par :: acc, i + 1, ins, insts in
          let res, _, ins, insts =
            List.fold_left2 resolve_arg ([], 0, ins, insts) ia_osrc ia_odst in
          List.rev res, ins, insts
        in
        let insts_sv = insts.sv_inst in
        let ptr_s, ins, insts_sv =
          resolve_args insts_sv ins ptr_osrc ptr_odst true in
        let int_s, ins, insts_sv =
          resolve_args insts_sv ins int_osrc int_odst false in
        let insts = { insts with sv_inst = insts_sv } in
        let j =
          match side with
          | Lft ->
              { j with
                j_inl    = ins;
                j_inr    = ilrem.ilr_graph_l;
                j_inst_l = insts }
          | Rgh ->
              { j with
                j_inl    = ilrem.ilr_graph_l;
                j_inr    = ins;
                j_inst_r = insts } in
        let create_out_par m_o m_s j =
          let f m0 m1 =
            let mm, j =
              List.fold_left2
                (fun (accm, j) il ir ->
                  let j, m = find_in_config (il, ir) j in
                  m :: accm, j
                ) ([], j) m0 m1 in
            List.rev mm, j in
          match side with
          | Lft -> f m_s m_o
          | Rgh -> f m_o m_s in
        let srcptr, j = create_out_par ptr_osrc ptr_s j in
        let dstptr, j = create_out_par ptr_odst ptr_s j in
        let int_src, j = create_out_par int_osrc int_s j in
        let int_dst, j = create_out_par int_odst int_s j in
        let int_par = Some (int_src, int_dst) in
        (* Creating the final segment *)
        let seg, gout, __fs =
          seg_edge_make ind.i_name ~srcptr ~dstptr ~int_par id j.j_out in
        (* Updating the set parameters instantiation *)
        let nsinsto =
          g_jinst_set_addctrs_post_isle ~msg:"segint" ~findkind:(setv_type ino)
            ~sarg_template:seo.se_eargs.ias_set ~sarg_out:seg.se_eargs.ias_set
            ~svmap:ilrem.ilr_svrtol ~setvmap:ilrem.ilr_colvrtol
            ~sc:ilrem.ilr_setctr_r ~inst:insto.set_inst in
        let insto = { insto with set_inst = nsinsto } in
        (* Creating the remaining parameters in -o graph *)
        let insto, isargo, idargo =
          is_le_seg_inst seo seg insto ilrem.ilr_svinst (*sinst*)None
            ilrem.ilr_seqinst ilrem.ilr_svrtol in
        assert (isargo = int_osrc);
        assert (idargo = int_odst);
        (* Creating the remaining parameters in -s graph *)
        let insts =
          let insts = inst_void_seg seg insts in
          let set_inst =
            Graph_inst.g_jinst_set_addcstr_segemp seg insts.set_inst in
          { insts with set_inst } in
        let j =
          match side with
          | Lft ->
              collect_rules_node isl ido
                { j with
                  j_inr    = ilrem.ilr_graph_l;
                  j_inl    = ins;
                  j_inst_r = insto;
                  j_inst_l = insts;
                  j_jal    = remove_node isl isl j.j_jal;
                  j_jar    = remove_node isr ido j.j_jar }
          | Rgh ->
              collect_rules_node ido isr
                { j with
                  j_inl    = ilrem.ilr_graph_l;
                  j_inr    = ins;
                  j_inst_l = insto;
                  j_inst_r = insts;
                  j_jal    = remove_node isl ido j.j_jal;
                  j_jar    = remove_node isr isr j.j_jar } in
        let vrules =
          match side with
          | Lft ->
              invalidate_rules isl isr Isiblings Inone
                (invalidate_rules isl isr Inone (Iblob ilrem.ilr_svrem_l)
                   j.j_rules)
          | Rgh ->
              invalidate_rules isl isr Inone Isiblings
                (invalidate_rules isl isr (Iblob ilrem.ilr_svrem_l) Inone
                   j.j_rules) in
        { j with
          j_rules = vrules;
          j_out   = seg_edge_add is seg gout }
    | Ilr_not_le -> (* rule application failed *)
        (* HS: instead of failing, we need to try weaken *)
        rule_abort "not-le"
          { j with j_rules = rules_add_weakening (isl, isr) j.j_rules }
    | Ilr_le_seg _ | Ilr_le_ind _ -> (* those cases should not happen *)
        Log.fatal_exn"seg-intro: absurd is_le result"
                      let apply_seg_intro_l = apply_gen_seg_intro Lft
                      let apply_seg_intro_r = apply_gen_seg_intro Rgh


(* seg-ext [par ptr OK, par int KO] *)
(* seg-ext *)
(* => auxilliary function for the double seg rule case *)
let aux_seg_ext_both_sides
    ~(is:  sv)
    ~(isl: sv) (* source node in the left graph *)
    ~(isr: sv) (* source node in the right graph *)
    ~(id:  sv)
    ~(ids: sv) (* should it be idr ??? *)
    ~(ind: ind)
    ~(j: join_config): join_config =
  let loc_debug = false in
  if loc_debug || !Flags.flag_dbg_join_shape then
    Log.info "seg-ext++, start [%a,%a,%a]" nsv_fpr is nsv_fpr isl nsv_fpr isr;
  let isle_l, iel =
    Graph_algos.is_le_ind ~submem:j.j_submem ind j.j_inl isl
      ~vsat:j.j_vsat_l in
  if loc_debug || !Flags.flag_dbg_join_shape then
    Log.info "seg-ext++, islehalfway";
  let isle_r, ier =
    Graph_algos.is_le_ind ~submem:j.j_submem ind j.j_inr isr
      ~vsat:j.j_vsat_r in
  match isle_l, isle_r with
  | Ilr_le_rem ilreml, Ilr_le_rem ilremr ->
      (* ilreml.ilr_seqctr_r, ilremr.ilr_seqctr_r unused
       * ilreml.ilr_svinst  , ilremr.ilr_svinst   unused
       * ilreml.ilr_setinst , ilremr.ilr_setinst  unused
       * ilreml.ilr_seqinst , ilremr.ilr_seqinst  unused *)
      if loc_debug || !Flags.flag_dbg_join_shape then
        begin
          Log.info
            "seg-ext++A:\n is: %a\n id: %a\n setl: %a\n setr: %a\n sta:\n%a"
            nsv_fpr is nsv_fpr id
            (gen_list_fpr "" set_cons_fpr " /\\ ") ilreml.ilr_setctr_r
            (gen_list_fpr "" set_cons_fpr " /\\ ") ilremr.ilr_setctr_r
            (join_config_fpri "   " "Status") j;
          Log.info "instl:\n%ainstr:\n%a\n"
            (Inst_utils.is_le_setv_inst_fpri "   ") ilreml.ilr_setinst
            (Inst_utils.is_le_setv_inst_fpri "   ") ilremr.ilr_setinst
        end;
      (* At this point: seq cons NOT supported, so there should be no SEQV *)
      assert (iel.ie_args.ia_seq = [] && ier.ie_args.ia_seq = []);
      (* lcout => ieout *)
      let ieout, gout, fs =
        (* for now pointer parameters unsupported *)
        assert (ind.i_ppars = 0);
        ind_edge_make ind.i_name ~srcptr:[] j.j_out in
      (* TODO: other constructions of seq_cons here *)
      let set_cons_l =
        Graph_inst.g_jinst_set_addctrs_post_isle ~msg:"seg-ext++l"
          ~findkind:(setv_type j.j_inl) ~sarg_template:iel.ie_args.ia_set
          ~sarg_out:ieout.ie_args.ia_set
          ~svmap:ilreml.ilr_svrtol ~setvmap:ilreml.ilr_colvrtol
          ~sc:ilreml.ilr_setctr_r  ~inst:j.j_inst_l.set_inst in
      let set_cons_r =
        Graph_inst.g_jinst_set_addctrs_post_isle ~msg:"seg-ext++r"
          ~findkind:(setv_type j.j_inl) ~sarg_template:ier.ie_args.ia_set
          ~sarg_out:ieout.ie_args.ia_set
          ~svmap:ilremr.ilr_svrtol ~setvmap:ilremr.ilr_colvrtol
          ~sc:ilremr.ilr_setctr_r  ~inst:j.j_inst_r.set_inst in
      (* TODO: question is this supposed to be empty ? *)
      let fi_set = (* guess inst *)
        jinst_set_guesscstr_segemp id ieout j.j_out in
      let vrules =
        invalidate_rules isl isr (Iblob (SvSet.singleton isl))
          (Iblob ilremr.ilr_svrem_l) j.j_rules in
      let j = { j with
                j_rules  = vrules;
                j_inl    = ilreml.ilr_graph_l;
                j_inr    = ilremr.ilr_graph_l;
                j_out    = ind_edge_add is ieout gout;
                j_inst_l = { j.j_inst_l with
                             set_inst = fi_set set_cons_l };
                j_inst_r = { j.j_inst_r with
                             set_inst = fi_set set_cons_r } } in
      if loc_debug then
        Log.info "seg-ext++,midpoint:\n%a\n"
          (join_config_fpri "   " "Status") j;
      let j =
        let cstr = Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, Ne_var ids) in
        let cond = j.j_vsat_r.vs_num cstr in
        if cond then j
        else
          let np_r =
            let s_heads =
              IntMap.filter (fun _ p -> p.st_head) ind.i_pkind.pr_set in
            if IntMap.cardinal s_heads != 1 then
              Log.fatal_exn "seg-ext++ fails, head param numbers not 1"
            else
              let i_min, _ = IntMap.min_binding s_heads in
              try List.nth ieout.ie_args.ia_set i_min
              with Not_found ->
                Log.fatal_exn "seg-ext++ fails, arg list length issue" in
          (* TODO: discrepancies on the side considered *)
          let colvi_prove =
            S_mem (ids, S_var np_r) :: j.j_inst_r.set_inst.colvi_prove in
          let instset = { j.j_inst_r.set_inst with colvi_prove } in
          { j with j_inst_r = { j.j_inst_r with set_inst = instset } } in
      (* sep_ind_out *)
      split_ind_or_seg ~is ~id j
  | Ilr_not_le _, _ -> failwith "L: not le"
  | _, Ilr_not_le _ -> failwith "R: not le"
  | Ilr_le_ind _, _ -> failwith "L: le_ind"
  | _, Ilr_le_ind _ -> failwith "R: le_ind"
  | Ilr_le_seg _, _ -> failwith "L: le_seg"
  | _, Ilr_le_seg _ -> failwith "R: le_seg"
let apply_seg_ext
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let loc_debug = false in
  let nl = node_find isl j.j_inl in
  let nr = node_find isr j.j_inr in
  (* weak side *)
  let side =
    match nl.n_e, nr.n_e with
    | Hseg _, _ -> Rgh
    | _, Hseg _ -> Lft
    | _, _  -> Log.fatal_exn "indintro: improper case" in
  let strside = sel_side side ("ext-seg", "seg-ext") in
  try
    if loc_debug then
      begin
        Log.info "Join Rule [%s]" strside;
        Log.info "rootl     = %a" nsvset_fpr j.j_inl.g_roots;
        Log.info "colvrootl = %a" (SvSet.t_fpr ";") j.j_inl.g_colvroots;
        Log.info "rootr     = %a" nsvset_fpr j.j_inr.g_roots;
        Log.info "colvrootr = %a" (SvSet.t_fpr ";") j.j_inr.g_colvroots;
      end;
    let iso, iss = get_sides side (isl, isr) in
    let ino, ins = get_sides side (j.j_inl, j.j_inr) in
    let vsato, vsats = get_sides side (j.j_vsat_l, j.j_vsat_r) in
    let no, ns =  get_sides side (nl, nr) in
    let nro, nrs = get_sides side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
    let insto, insts =
      get_sides side (j.j_inst_l, j.j_inst_r) in
    (* match edge in the left graph with a segment *)
    match no.n_e with
    | Hseg seo ->
        let ind = seo.se_ind in
        (* extract destination of sel, and see if it is mapped into sthg *)
        let ido = seo.se_dnode in
        (* extract a "tentative" destination in the right side *)
        let ids, guessed =
          let s =
            try SvMap.find ido nro
            with Not_found -> SvSet.empty in
          if !Flags.flag_dbg_join_shape then
            Log.info "set found: %a" svset_fpr s;
          let card = SvSet.cardinal s in
          if card = 1 then
            let elt = SvSet.min_elt s in
            let nids, nido  = node_find elt ins, node_find ido ino in
            let is_rule =
              (* compare the property of end nodes to see whether the rule
               * may apply *)
              match (nido.n_attr, nido.n_e), (nids.n_attr, nids.n_e) with
              | (Attr_none, Hpt _), (Attr_none, Hemp) -> false
              | _, _ -> true in
            if not is_rule then
              raise (Abort_rule_skip (F.asprintf "%s failed (other)" strside))
            else
              elt, false
          else
            (* Backup solution:
             * - if card > 1,
             *    see if there is a segment in the right side, and if so
             *    look if its destination works as well
             * - if card = 0,
             *    see if there is a segment in the right side and use it... *)
            match ns.n_e with
            | Hseg ses ->
                if SvSet.mem ses.se_dnode s then ses.se_dnode, false
                else if card = 0 then
                  begin (* no other solution anyway *)
                    Log.warn "%s takes next segment (not sure about end-point)"
                      strside;
                    ses.se_dnode, true
                  end
                else Log.fatal_exn "%s backup failed (seg)" strside
            | _ -> raise (Abort_rule_skip
                            (F.asprintf "%s backup failed (?)" strside)) in
        (* creation functions *)
        let find (l, r) j = find_in_config (get_sides side (l, r)) j in
        let f_do_it inj j ial iar =
          let j, l =
            List.fold_left2
              (fun (accj, accl) il ir ->
                let nir =
                  try SvMap.find ir inj
                  with Not_found -> raise (Abort_rule_skip strside) in
                let nj, ni = find (il, nir) accj in
                nj, ni :: accl
              ) (j, [ ]) ial iar in
          j, List.rev l  in
        (* test of inclusion of a part of the right side into a segment edge *)
        let le_res, ses, _ =
          Graph_algos.is_le_seg ~submem:j.j_submem ind ins iss ids
            ~vsat:vsats in
        if !Flags.flag_dbg_join_shape then
          Log.info "%s Inclusion test DONE" __LOC__;
        begin
          match le_res with
          | Ilr_le_rem ilrem ->
              assert (ilrem.ilr_seqctr_r = []);
              (* success case *)
              (* synthesis and addition of a new segment edge *)
              (* mapping the pointer parameters of the new edge *)
              hseg_sanity_check ~isok:3 "graph_join,apply_seg_ext" ses;
              let doit = f_do_it ilrem.ilr_svrtol in
              let j, srcptr = doit j seo.se_sargs.ia_ptr ses.se_sargs.ia_ptr in
              let j, dstptr = doit j seo.se_dargs.ia_ptr ses.se_dargs.ia_ptr in
              let j, intsrc = doit j seo.se_sargs.ia_int ses.se_sargs.ia_int in
              let j, intdst = doit j seo.se_dargs.ia_int ses.se_dargs.ia_int in
              (* => If id is not here, add rules from there !!!  *)
              let j, id = find (ido, ids) j in
              let int_par = Some (intsrc, intdst) in
              let seg, gout, __fs =
                seg_edge_make ind.i_name ~srcptr ~dstptr ~int_par id j.j_out in
              let ino, insto, (isargo, idargo) =
                let insto =
                  let set_inst =
                    jinst_set_addcstr_icall_eqs
                      (ind_args_seg_to_ind_args seo.se_eargs)
                      (ind_args_seg_to_ind_args seg.se_eargs)
                      insto.set_inst in
                  let seq_inst =
                    jinst_seq_addcstr_scall_eqs seo.se_eargs seg.se_eargs
                      insto.seq_inst in
                  { insto with set_inst; seq_inst } in
                l_seg_inst ~skipset:true seo ino seg insto ~pr:ind.i_pkind in
              let insts, isargs, idargs  =
                let nsinsts =
                  g_jinst_set_addctrs_post_isle ~msg:strside
		    ~findkind:(setv_type ins) ~sarg_template:ses.se_eargs.ias_set
                    ~sarg_out:seg.se_eargs.ias_set
                    ~svmap:ilrem.ilr_svrtol ~setvmap:ilrem.ilr_colvrtol
                    ~sc:ilrem.ilr_setctr_r ~inst:insts.set_inst in
                is_le_seg_inst ses seg { insts with set_inst = nsinsts }
                  ilrem.ilr_svinst (*(Some ilrem.ilr_setinst)*)None
                  ilrem.ilr_seqinst ilrem.ilr_svrtol in
              (* there are some case that guessed is true, but we still need
               * collect rules *)
              let idl, idr = get_sides side (ido, ids) in
              let j = collect_rules_node idl idr j in
              let vrules =
                match side with
                | Rgh ->
                    invalidate_rules isl isr (Iblob (SvSet.singleton isl))
                      (Iblob ilrem.ilr_svrem_l) j.j_rules
                | Lft ->
                    invalidate_rules isl isr (Iblob ilrem.ilr_svrem_l)
                      (Iblob (SvSet.singleton isr)) j.j_rules in
              let j =
                match side with
                | Rgh ->
                    { j with
                      j_inl    = seg_edge_rem isl ino;
                      j_inr    = ilrem.ilr_graph_l;
                      j_inst_r = insts;
                      j_inst_l = insto;
                    }
                | Lft ->
                    { j with
                      j_inl    = ilrem.ilr_graph_l;
                      j_inr    = seg_edge_rem isr ino;
                      j_inst_l = insts;
                      j_inst_r = insto;
                    } in
              { j with
                j_rules = vrules;
                j_jal   = remove_node isl idl j.j_jal;
                j_jar   = remove_node isr idr j.j_jar;
                j_out   = seg_edge_add is seg gout}
          | Ilr_not_le -> (* rule application failed *)
              (*let ieout, gout, fs =
                (* missing inj ... *)
                (* for now pointer parameters unsupported *)
                assert (seo.se_ind.i_ppars = 0);
                ind_edge_make ind.i_name ~srcptr:[] j.j_out in*)
              (*
               * note for correspondance with list:
               *  idl => seo.se_dnode = ido
               *  idr => ids
               *  id  => find (ido, ids)
               *)
              let j, id = find (ido, ids) j in
              if loc_debug then
                Log.info "Will now move to %s++: %a" strside nsv_fpr id;
              aux_seg_ext_both_sides ~is ~isl ~isr ~id ~ids ~ind:seo.se_ind ~j
          | Ilr_le_seg _ | Ilr_le_ind _ -> (* those cases should not happen *)
              Log.fatal_exn "%s: absurd is_le result" strside
        end
    | _ -> (* we may check the rule was not invalidated ? *)
        Log.fatal_exn "%s: improper case" strside
  with Abort_rule_skip _ ->
    (* HS: instead of failing, we need to try weaken to inductive edge *)
    { j with j_rules = rules_add_weakening (isl, isr) j.j_rules }


(* seg_ext_ext [par ptr OK, par int KO] *)
let apply_seg_ext_ext
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  try
    (* find destination in both side *)
    let idl, indl_name = Option.get (seg_extension_end isl j.j_jal) in
    let idr, indr_name = Option.get (seg_extension_end isr j.j_jar) in
    assert (indl_name = indr_name);
    if !Flags.flag_dbg_join_shape then
      Log.info "isl: %a, idl: %a; isr: %a, idr: %a" sv_fpr isl sv_fpr idl
        sv_fpr isr sv_fpr idr;
    let ind = Ind_utils.ind_find indl_name in
    (* test of inclusion of a part of the left side into a segment edge *)
    let le_res, sel, _ =
      Graph_algos.is_le_seg ~submem:j.j_submem ind j.j_inl isl idl
        ~vsat:j.j_vsat_l in
    (* test of inclusion of a part of the right side into a segment edge *)
    let re_res, ser, _ =
      Graph_algos.is_le_seg ~submem:j.j_submem ind j.j_inr isr idr
        ~vsat:j.j_vsat_r in
    begin
      match le_res, re_res with
      | Ilr_le_rem ilreml, Ilr_le_rem ilremr ->
          (* some fields are unused:
           *  ilreml.ilr_graph_l,  ilremr.ilr_graph_l
           *  ilreml.ilr_colvrtol, ilremr.ilr_colvrtol
           *  ilreml.ilr_setctr_r, ilremr.ilr_setctr_r  *)
          assert (ilreml.ilr_seqctr_r = [] && ilremr.ilr_seqctr_r = []);
          (* success case *)
          (* synthesis and addition of a new segment edge *)
          (* rename segment parameters to the input*)
          let rename (args: ind_args) (inj: sv SvMap.t): ind_args =
            let p =
              List.fold_left (fun acc ele -> SvMap.find ele inj :: acc)
                [] (List.rev args.ia_ptr) in
            { args with ia_ptr = p } in
          let rename_seg (args: ind_args_seg) (inj: sv SvMap.t): ind_args_seg =
            let p =
              List.fold_left (fun acc ele -> SvMap.find ele inj :: acc)
                [] (List.rev args.ias_ptr) in
            { args with ias_ptr = p } in
          hseg_sanity_check ~isok:3 "graph_join,apply_seg_ext_ext,l" sel;
          hseg_sanity_check ~isok:3 "graph_join,apply_seg_ext_ext,r" ser;
          let sel = { sel with
                      se_sargs = rename sel.se_sargs ilreml.ilr_svrtol;
                      se_eargs = rename_seg sel.se_eargs ilreml.ilr_svrtol;
                      se_dargs = rename sel.se_dargs ilreml.ilr_svrtol; } in
          let ser = { ser with
                      se_sargs = rename ser.se_sargs ilremr.ilr_svrtol;
                      se_eargs = rename_seg ser.se_eargs ilremr.ilr_svrtol;
                      se_dargs = rename ser.se_dargs ilremr.ilr_svrtol; } in
          (* mapping the pointer parameters of the new edge *)
          let f_do_it j ial iar =
            let j, l =
              List.fold_left2
                (fun (accj, accl) il ir ->
                  let nj, ni = find_in_config (il, ir) accj in
                  nj, ni :: accl
                ) (j, [ ]) ial.ia_ptr iar.ia_ptr in
            j, List.rev l in
          let j, srcptr = f_do_it j sel.se_sargs ser.se_sargs in
          let j, dstptr = f_do_it j sel.se_dargs ser.se_dargs in
          (* => If id is not here, add rules from there !!!  *)
          let j, id = find_in_config (idl, idr) j in
          let seg, gout, __fs =
            seg_edge_make ind.i_name ~srcptr ~dstptr id j.j_out in
          let instl, isargl, idargl  =
            is_le_seg_inst sel seg j.j_inst_l ilreml.ilr_svinst
              (Some ilreml.ilr_setinst) ilreml.ilr_seqinst ilreml.ilr_svrtol in
          let instr, isargr, idargr =
            is_le_seg_inst ser seg j.j_inst_r ilremr.ilr_svinst
              (Some ilremr.ilr_setinst) ilremr.ilr_seqinst ilremr.ilr_svrtol in
          hseg_sanity_check ~isok:1 "graph_join,apply_seg_ext_ext,seg" seg;
          let rel =
            f_combine_side isargl isargr seg.se_sargs.ia_int j.j_rel Lft in
          let rel = f_combine_side idargl idargr seg.se_dargs.ia_int rel Lft in
          (* there are some case that guessed is true, but we still need *
           * collect rules*)
          let j = collect_rules_node idl idr j in
          let vrules =
            invalidate_rules isl isr (Iblob ilreml.ilr_svrem_l)
              (Iblob ilremr.ilr_svrem_l) j.j_rules in
          { j with
            j_rules  = vrules;
            j_inl    = ilreml.ilr_graph_l;
            j_inr    = ilremr.ilr_graph_l;
            j_rel    = rel;
            j_inst_r = instr;
            j_inst_l = instl;
            j_jal    = remove_node isl idl j.j_jal;
            j_jar    = remove_node isr idr j.j_jar;
            j_out    = seg_edge_add is seg gout }
      (* rule application failed *)
      | Ilr_not_le, _ ->
          Log.fatal_exn"seg-ext-ext fails: Ile"
      |  _, Ilr_not_le ->
          Log.fatal_exn"seg-ext-ext fails: Ire"
      | _, _ -> (* those cases should not happen *)
          Log.fatal_exn"seg-ext-ext: absurd is_le result"
    end
  with Invalid_argument _ ->
    Log.info "isl: %a, isr: %a" sv_fpr isl sv_fpr isr;
    j

(* split-ind
 *  Configuration: - SVs "is" and "id" are mapped to (iss,iso) and (iss,ido)
 *                 - there is an inductive rule in the output, from is
 *                 - SV ido is part of a head SET parameter of that inductive
 *)
let apply_gen_split_ind
    (side: side) (* side of the rule, i.e., where the empty chunk is found *)
    (is: sv) (isl: sv) (isr: sv) (* source in dest, left, right argument *)
    (j: join_config): join_config =
  let loc_debug = false in
  let iso, iss = get_sides side (isl, isr) in
  let cho, chs = get_sides side ('l', 'r') in
  let vsato, _ = get_sides side (j.j_vsat_l, j.j_vsat_r) in
  (* destination node in the other side *)
  let ido =
    let allsibl = sel_side side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
    calc_split_ind_dest_o allsibl iso iss in
  if !Flags.flag_dbg_join_shape then
    Log.info "sep-ind attempt: S%c:%a -> (S%c:%a,D%c:%a)"
      chs nsv_fpr iss cho nsv_fpr iso cho nsv_fpr ido;
  (* destination node in the result *)
  let id =
    let pd = get_sides side (ido, iss) in
    SvPrMap.find_err "sep_ind:pair" pd j.j_rel.n_inj in
  let n_is = node_find is j.j_out in
  let n_id = node_find id j.j_out in
  if loc_debug then Log.info "tmp,split_ind:look for call";
  match n_is.n_e, n_id.n_e with
  | Hind icall, Hemp ->
      if loc_debug then Log.info "tmp,split_ind:case";
      let ind = icall.ie_ind in
      (* - check whether inductive definition supports it *)
      let get_head_set_pars (ind: ind): IntSet.t =
        IntMap.fold
          (fun i pk a -> if pk.st_head then IntSet.add i a else a)
          ind.i_pkind.pr_set IntSet.empty in
      let heads = get_head_set_pars ind in
      if loc_debug then
        Log.info "tmp,split_ind:cardinal %d" (IntSet.cardinal heads);
      if IntSet.cardinal heads = 0 then j (* no target found, aborts *)
      else
        let hanum = IntSet.min_elt heads in
        if IntSet.cardinal heads > 1 then Log.warn "several heads pars";
        if loc_debug then
          Log.info "tmp,rule,heads: { %a } => %d"
            (IntSet.t_fpr ", ") heads hanum;
        (* 1. get the parameter *)
        let hsetv = List.nth icall.ie_args.ia_set hanum in
        if loc_debug then Log.info "tmp,rule,a,setv: %a" setv_fpr hsetv;
        (* 2. get the set formula in the instantiation *)
        let jinsto, _ = get_sides side (j.j_inst_l, j.j_inst_r) in
        let defo = SvMap.find hsetv jinsto.set_inst.colvi_eqs in
        (* 3. check whether enabling constraints hold; if so, split ind edge *)
        (* TODO: should this not just be checked for sat here but
         *       added to constraints to prove later on the join output ??? *)
        let b = vsato.vs_set (S_mem (ido, defo)) in
        if loc_debug then
          Log.info "tmp,split,setform: %a <: %a ---> %b"
            setv_fpr ido set_expr_fpr defo b;
        if b then
          (* 3.a case where ido is in some head paramter set, so it defines
           *     a splitting segment *)
          split_ind_or_seg ~is ~id j
        else
          let b =
            let c = Nc_cons (Apron.Tcons1.EQ, Ne_csti 0, Ne_var ido) in
            vsato.vs_num c in
          if loc_debug then
            Log.info "tmp,split,null: (ind0rule: %b,  cond: %b)" b
              ind.i_mt_rule;
          if b && ind.i_mt_rule then
            (* 3.b case where ido is null and the inductive admits a null
             *     pointer, empty store case; after splitting, ido is the
             *     last element of the structure *)
            split_ind_or_seg ~is ~id j
          else (* neither condition holds, thus we abort the rule *)
            j
  | _ -> j
let apply_split_ind_l = apply_gen_split_ind Lft
let apply_split_ind_r = apply_gen_split_ind Rgh

(* ind-intro [par ptr OK, part int KO]
 *   introduction of an inductive *)
let apply_ind_intro
    (is: sv)  (* source node in the target graph *)
    (isl: sv) (* source node in the left graph *)
    (isr: sv) (* source node in the right graph *)
    (j: join_config): join_config =
  let nl = node_find isl j.j_inl in
  let nr = node_find isr j.j_inr in
  (* emp side *)
  let side = match nl.n_e, nr.n_e with
    | Hemp, Hpt mcr -> Lft
    | Hpt mcl, Hemp -> Rgh
    | Hemp, Hemp -> Lft
    (*HS: it is possible that one side is seg, and the other is pt *)
    | Hpt mcl, Hseg _ -> Rgh
    | Hseg _ , Hpt mcr -> Lft
    | _, _  -> Log.fatal_exn"indintro: improper case" in
  let _ = sel_side side ("emp-pt", "pt-emp") in
  let iso, iss = get_sides side (isl, isr) in
  let ino, ins = get_sides side (j.j_inl, j.j_inr) in
  let vsato, vsats = get_sides side (j.j_vsat_l, j.j_vsat_r) in
  let no, ns =  get_sides side (nl, nr) in
  let nro, nrs = get_sides side (j.j_rel.n_l2r, j.j_rel.n_r2l) in
  let insto, insts =
    get_sides side (j.j_inst_l, j.j_inst_r) in
  match ns.n_e, no.n_e with
  | _, Hpt mco ->
      (* 1. search for candidate inductive definitions:
       *    - should have an empty rule
       *    - should have a rule matching the rhs signature *)
      let ind =
        match ns.n_attr with
        | Attr_none | Attr_array _ ->
            block_inductive_candidate "indintro" ino j.j_out ins false mco
        | Attr_ind iname ->
            (* attribute gives a hint of the inductive to select *)
            Ind_utils.ind_find iname in
      if !Flags.flag_dbg_join_shape then
        Log.info "Selected candidate %s" ind.i_name;
      if ind.i_ppars != 0 then Log.warn "ind-intro, pointer parameters";
      (*assert (ind.i_ipars = 0);*)
      (* 2. verify that inclusion holds in both sides *)
      let le_res_s, ies =
        Graph_algos.is_le_ind ~submem:j.j_submem ind ins iss ~vsat:vsats in
      let le_res_o, ieo =
        Graph_algos.is_le_ind ~submem:j.j_submem ind ino iso ~vsat:vsato in
      begin
        match le_res_s, le_res_o with
        | Ilr_le_rem ilreml, Ilr_le_rem ilremr ->
            (* ilreml.ilr_graph_l,  ilreml.ilr_graph_l  unused *)
            assert (ilreml.ilr_seqctr_r = [] && ilremr.ilr_seqctr_r = []);
            let ppars =
              Graph_inst.g_jinst_sv_calls
                (ilreml.ilr_svrtol, ies.ie_args.ia_ptr)
                (ilremr.ilr_svrtol, ieo.ie_args.ia_ptr, nro)
                (get_sides side) j.j_rel in
            let ie, gout, ___fs =
              ind_edge_make ies.ie_ind.i_name ~srcptr:ppars j.j_out in
            let insto, iargo =
              is_le_call_inst ieo.ie_args ie.ie_args insto
                ilremr.ilr_svinst (Some ilremr.ilr_setinst) ilremr.ilr_seqinst
                ilremr.ilr_svrtol in
            let insts, iargs =
              is_le_call_inst ies.ie_args ie.ie_args insts
                ilreml.ilr_svinst (Some ilreml.ilr_setinst) ilreml.ilr_seqinst
                ilreml.ilr_svrtol in
            let rel =
              f_combine_side iargs iargo ie.ie_args.ia_int j.j_rel side in
            let vrules =
              match side with
              | Lft ->
                  invalidate_rules isl isr (Iblob ilreml.ilr_svrem_l)
                    (Iblob ilremr.ilr_svrem_l) j.j_rules
              | Rgh ->
                  invalidate_rules isl isr (Iblob ilremr.ilr_svrem_l)
                    (Iblob ilreml.ilr_svrem_l) j.j_rules in
            let j =
              match side with
              | Lft ->
                  { j with
                    j_inl    = ilreml.ilr_graph_l;
                    j_inr    = ilremr.ilr_graph_l;
                    j_inst_r = insto;
                    j_inst_l = insts; }
              | Rgh ->
                  { j with
                    j_inl    = ilremr.ilr_graph_l;
                    j_inr    = ilreml.ilr_graph_l;
                    j_inst_r = insts;
                    j_inst_l = insto; } in
            { j with
              j_rules = vrules;
              j_rel   = rel;
              j_out   = ind_edge_add is ie gout }
        | _, _ ->
            match side with
            | Lft ->
                Log.info "ind-intro: verification of inclusion: %a - %a"
                  is_le_res_fpr le_res_s is_le_res_fpr le_res_o;
                Log.todo_exn "ind-intro: le failed"
            | Rgh ->
                Log.info "ind-intro: verification of inclusion: %a - %a"
                  is_le_res_fpr le_res_o is_le_res_fpr le_res_s;
                Log.todo_exn "ind-intro: le failed"
      end
  | Hemp, Hemp ->
      (* another rule was applied before, we can abort that one *)
      j
  | _, _ ->
      Log.fatal_exn"indintro: improper case"


let reset, output_jc =
  let bc, sc = ref 0, ref 0 in
  (fun () -> incr bc; sc := 1),
  fun (jc: join_config) (rule: string) ->
  let filename = Printf.sprintf "%s-join-%i-%i" (Flags.out_prefix ()) !bc !sc in
  let title = Printf.sprintf "%s-%s" filename rule in
  let _ = incr sc in
  let namer sv = raise Not_found in
  List.iter
    (fun (graph, label) ->
      let labels =
        match label with
        | "-inl" -> F.asprintf "NODE inst\n%aSET inst\n%aSEQ inst\n%a"
           (Inst_utils.sv_inst_fpri "  ") jc.j_inst_l.sv_inst
           (Inst_utils.set_colv_inst_fpri "  ") jc.j_inst_l.set_inst
           (Inst_utils.seq_colv_inst_fpri "  ") jc.j_inst_l.seq_inst
        | "-inr" -> F.asprintf "NODE inst\n%aSET inst\n%aSEQ inst\n%a"
           (Inst_utils.sv_inst_fpri "  ") jc.j_inst_r.sv_inst
           (Inst_utils.set_colv_inst_fpri "  ") jc.j_inst_r.set_inst
           (Inst_utils.seq_colv_inst_fpri "  ") jc.j_inst_r.seq_inst
        | _ -> F.asprintf "NODE rel:\n%a" (Nrel.nr_fpri "  ") jc.j_rel in
      let labels = labels
        |> String.split_on_char '\n'
        |> String.concat "\\l" in
      let chan = open_out @@ filename^label^".dot" in
      Graph_visu.pp_graph ~labels title [] [Successors] graph namer chan;
      close_out_noerr chan;
      let cmd =
        Printf.sprintf "dot -Tpdf %s%s.dot -o %s%s.pdf"
          filename label filename label in
      cmd |> run_command |> ignore
    )
    [jc.j_inl, "-inl"; jc.j_inr, "-inr"; jc.j_out, "-out"]

(** The new join algorithm with worklist over available rules *)
(* This implementation of join relies on a strategy with worklist that
 * iterates over rules.
 *)
let rec s_join (jc: join_config): join_config =
  (* Find the next rule to apply, and trigger it *)
  match rules_next jc.j_rules with
  | None -> (* there is no more rule to apply, so we return current config *)
      if !Flags.flag_dbg_join_shape then
        Log.info "no more rule applies;\n%a" rules_fpr jc.j_rules;
      if !Flags.flag_enable_ext_export_all then
        (let rule = Printf.sprintf "end" in
        output_jc jc rule);
      if SvPrSet.is_empty jc.j_drel then jc
      else (* XR?: comment what this is doing *)
        let jc =
          SvPrSet.fold
            (fun (il, ir) acc ->
              if SvMap.mem il jc.j_inl.g_g && SvMap.mem ir jc.j_inr.g_g then
                collect_rules_node il ir acc
              else acc
            ) jc.j_drel jc in
        s_join { jc with j_drel = SvPrSet.empty }
  | Some (k, (il, ir), rem_rules) ->
      if !Flags.flag_dbg_join_shape then
        Log.info "%a" (join_config_fpri ~pp_rules:true "" "J-situation") jc;
      let jc = { jc with j_rules = rem_rules } in
      let is =
        try SvPrMap.find (il, ir) jc.j_rel.n_inj
        with Not_found ->
          Log.fatal_exn "pair (%a,%a) not found" sv_fpr il sv_fpr ir in
      if !Flags.flag_dbg_join_shape then
        Log.info "Join-Treating (%a,%a) (%a)"
          sv_fpr il sv_fpr ir rkind_fpr k;
      if !Flags.flag_enable_ext_export_all then
        begin
          let rule = F.asprintf "%a-%a-%a" rkind_fpr k sv_fpr il sv_fpr ir in
          output_jc jc rule
        end;
      try
        let nj =
          match k with
          | Rpp           -> apply_pt_pt       is il ir jc
          | Rii           -> apply_ind_ind     is il ir jc
          | Rweaki        -> apply_weak_ind    is il ir jc
          | Riweak        -> apply_ind_weak    is il ir jc
          | Rsegext       -> apply_seg_ext     is il ir jc
          | Rsegintro Lft -> apply_seg_intro_l is il ir jc
          | Rsegintro Rgh -> apply_seg_intro_r is il ir jc
          | Rindintro     -> apply_ind_intro   is il ir jc
          | Rsplitind Lft -> apply_split_ind_l is il ir jc
          | Rsplitind Rgh -> apply_split_ind_r is il ir jc
          | Rsegext_ext   -> apply_seg_ext_ext is il ir jc in
        s_join nj
      with
      | Abort_rule_skip msg ->
          if !Flags.flag_dbg_join_shape then Log.info "Aborting rule %s" msg;
          s_join jc
      | Abort_rule_cont (msg, jr) ->
          if !Flags.flag_dbg_join_shape then Log.info "Aborting rule %s" msg;
          s_join jr

let s_g_join (jc: join_config): join_config =
  s_join (init_rules jc)



(** The main join function *)
(* Performs initialization and triggers either algorithm *)
let join
    ~(submem: bool)        (* whether sub-memory is_le (no alloc check) *)
    ((xl, jl): graph * join_arg) (* left input *)
    ~(vsat_l: vsat) (* left proving function *)
    ((xr, jr): graph * join_arg) (* right input *)
    ~(vsat_r: vsat) (* right proving function *)
    (ho: hint_bg option)   (* optional hint *)
    (lo: lint_bg option)   (* optional nullable node address *)
    (r: node_relation)     (* relation between both inputs *)
    (srel: node_relation)  (* relation between both inputs set vars *)
    (qrel: node_relation)  (* relation between both inputs seq vars *)
    (noprio: bool)         (* whether to NOT make roots prioretary *)
    (o: graph)             (* pre-computed, initial version of output *)
    : graph * node_relation (* extended relation *)
    * (join_inst * (sv * sv) list) (* sv/set inst for left argument *)
    * (join_inst * (sv * sv) list) (* sv/set inst for right argument *) =
  let loc_debug = false in
  if !Flags.flag_dbg_join_shape then
    begin
      Log.info "o.colvkey: %a" SvGen.t_fpr o.g_colvkey;
      Log.info "o.colvkind:\n  %a"
        (SvMap.t_fpr "\n  "
           (fun fmt pt -> Ast_utils.col_kind_fpr fmt (col_par_type_to_kind pt)))
        o.g_colvkind;
      if not !Flags.very_silent_mode then
        Log.info "\n\n[Gr,al]  start join\n\n"
    end;
  assert (xl.g_svemod = Dom_utils.svenv_empty);
  assert (xr.g_svemod = Dom_utils.svenv_empty);
  let _ = reset () in
  let h =
    if noprio then fun _ -> false
    else
      match ho with
      | None -> fun _ -> true
      | Some s ->
          let prio_nodes =
            if Flags.flag_join_parameters_prio then
              (* roots r such that r->n and n is a segment or inductive
               * arguments should be considered prioritary in this case *)
              begin
                (* checks whether i appears as an argument *)
                let f_ind_args (i: sv) (ia: ind_args): unit =
                  if List.mem i ia.ia_ptr || List.mem i ia.ia_int then
                    raise Stop in
                (* checks whether i is an argument *)
                let f_pred_node (i: sv) (g: graph): bool =
                  try (* raises Stop if it finds i an argument *)
                    SvSet.iter
                      (fun pre_id ->
                        match (node_find pre_id g).n_e with
                        | Hemp
                        | Hpt _ -> ()
                        | Hind ie -> raise Stop (* necessary an argument *)
                        | Hseg se ->
                            hseg_sanity_check ~isok:1 "graph_join,join" se;
                            (* only int and ptr arguments *)
                            f_ind_args i se.se_sargs;
                            f_ind_args i se.se_dargs
                      ) (node_find i g).n_preds;
                    if !Flags.flag_dbg_join_shape then
                      Log.info "f_node concluded false on %a" sv_fpr i;
                    false
                  with
                  | Stop ->
                      if !Flags.flag_dbg_join_shape then
                        Log.info "f_node concluded true on %a" sv_fpr i;
                      true in
                let f_node (i: sv) (g: graph): bool =
                  match (node_find i g).n_e with
                  | Hpt mc ->
                      Block_frag.fold_base
                        (fun _ pe b -> b || f_pred_node (fst pe.pe_dest) g)
                        mc false
                  | _ -> false in
                SvMap.fold
                  (fun i _ acc ->
                    try
                      let il, ir = SvMap.find i r.n_pi in
                      if f_node il xl || f_node ir xr then
                        Aa_maps.add ir il acc
                      else acc
                    with Not_found ->
                      (* The only case of non-bound sv is info *)
                      assert (is_info i o); acc
                  ) o.g_g s.hbg_live
              end
            else s.hbg_live in
          fun (i, j) ->
            try (Aa_maps.find j prio_nodes = i) ||
                ( (SvMap.find i xl.g_g).n_ptprio &&
                  (SvMap.find j xr.g_g).n_ptprio    )
            with Not_found -> false in
  let l =
    let is_dead (l, r) lo =
      try (Aa_maps.find l lo.lbg_dead = r)
      with Not_found -> false in
    match lo with
    | None -> fun (l, r) -> false
    | Some lo -> fun (l, r) -> is_dead (l, r) lo in
  let sat_diseq ctr g =
    match ctr with
    | Nc_cons (Apron.Lincons1.DISEQ, Ne_var i, Ne_var j) ->
      sat_graph_diseq g i j
    | _ -> false in
  let j_vsat vsat g =
    { vsat with
      vs_num = (fun ctr -> (vsat.vs_num ctr) || (sat_diseq ctr g)) } in
  let instsetl, instsetr =
    SvMap.fold
      (fun i (l, r) (isl, isr) ->
        SvMap.add i (S_var l) isl, SvMap.add i (S_var r) isr)
      srel.n_pi (SvMap.empty, SvMap.empty) in
  let instseql, instseqr =
    SvMap.fold
      (fun i (l, r) (isl, isr) ->
        SvMap.add i (Seq_var l) isl, SvMap.add i (Seq_var r) isr)
      qrel.n_pi (SvMap.empty, SvMap.empty) in
  let j_inst_l: join_inst =
    { subm_mod = sv_submem_mod_empty;
      sv_inst  = Inst_utils.sv_inst_empty;
      set_inst = { set_colv_inst_empty with colvi_eqs = instsetl };
      seq_inst = { seq_colv_inst_empty with colvi_eqs = instseql }; } in
  let j_inst_r: join_inst =
    { subm_mod = sv_submem_mod_empty;
      sv_inst  = Inst_utils.sv_inst_empty;
      set_inst = { set_colv_inst_empty with colvi_eqs = instsetr };
      seq_inst = { seq_colv_inst_empty with colvi_eqs = instseqr }; } in
  let j = { j_rules  = empty_rules;
            j_inl    = xl;
            j_inr    = xr;
            j_jal    = jl;
            j_jar    = jr;
            j_vsat_l = j_vsat vsat_l xl;
            j_vsat_r = j_vsat vsat_r xr;
            j_rel    = r;
            j_hint   = h;
            j_lint   = l;
            j_drel   = SvPrSet.empty;
            j_inst_l = j_inst_l;
            j_inst_r = j_inst_r;
            j_tmp_setinst_l = [];
            j_tmp_setinst_r = [];
            j_out    = o;
            j_submem = submem } in
  let out = s_g_join j in
  (* deal with the key and svemod *)
  let do_is_le_graph jin jout =
    let g =
      SvMap.fold
        (fun id node acc ->
          if SvMap.mem id acc then acc else SvMap.add id node acc
        ) jout.g_g jin.g_g in
    { jin with
      g_nkey   = jout.g_nkey;
      g_g      = g;
      g_svemod = jout.g_svemod } in
  let guess_eq_set, smapl, smapr, sv_instl, sv_instr =
    guess_cons out.j_out
      (do_is_le_graph j.j_inl out.j_inl) ~vsat_l:j.j_vsat_l out.j_inst_l.sv_inst
      (do_is_le_graph j.j_inr out.j_inr) ~vsat_r:j.j_vsat_r out.j_inst_r.sv_inst
      out.j_rel in
  if loc_debug then
    begin
      Log.info "tmp,guess: cons: %d" (SvMap.cardinal guess_eq_set);
      SvMap.iter
        (fun sv foo ->
          Log.info " %a => %a" sv_fpr sv sv_fpr foo
        ) guess_eq_set;
      if out.j_tmp_setinst_l != [] || out.j_tmp_setinst_r != [] then
        Log.info "tmp: out join, %d, %d"
          (List.length out.j_tmp_setinst_l) (List.length out.j_tmp_setinst_r);
    end;
  (* the following will crash if un-instantiated keys remain
   *  -> TODO: try to augment the instantiation with the additional membership
   *     constraints, before calling instantiate_eq *)
  (* Instantiation related to SETVs; TODO: make consistent with lists *)
  let instset_l = instantiate_eq out.j_inst_l.set_inst guess_eq_set smapl in
  let instset_r = instantiate_eq out.j_inst_r.set_inst guess_eq_set smapr in
  (* Instantiation related to SVs *)
  let instsv_l = sv_instantiation sv_instl out.j_vsat_l.vs_num in
  let instsv_r = sv_instantiation sv_instr out.j_vsat_r.vs_num in
  let instsv_l = do_sv_inst_left_ctrs instsv_l out.j_vsat_l.vs_num in
  let instsv_r = do_sv_inst_left_ctrs instsv_r out.j_vsat_r.vs_num in
  let typed_fresh_svl, typed_fresh_svr =
    int_par_type_sv_wk out.j_out out.j_rel instsv_l instsv_r in
  let instsv_l =
    typed_sv_instantiation instsv_l typed_fresh_svl out.j_vsat_l.vs_num in
  let instsv_r =
    typed_sv_instantiation instsv_r typed_fresh_svr out.j_vsat_r.vs_num in
  let instsv_l = prove_sv_cons instsv_l out.j_vsat_l.vs_num in
  let instsv_r = prove_sv_cons instsv_r out.j_vsat_r.vs_num in
  if loc_debug then
    Log.info "HSEG, before strange assertions:\n(l)\n%a\n(r)\n%a"
      (set_colv_inst_fpri "   ") instset_l
      (set_colv_inst_fpri "   ") instset_r;
  assert (instsv_l.sv_cons = []);
  assert (instsv_r.sv_cons = []);
  let j_inst_l = { out.j_inst_l with
		   set_inst = instset_l;
		   sv_inst  = instsv_l } in
  let j_inst_r = { out.j_inst_r with
		   set_inst = instset_r;
		   sv_inst = instsv_r } in
  let out = { out with j_inst_l; j_inst_r } in
  (* Optional display before return *)
  let bpi = graph_fpri "  " in
  let nl = num_edges out.j_inl and nr = num_edges out.j_inr in
  if !Flags.flag_dbg_join_shape || nl != 0 || nr != 0 || loc_debug then
    begin
      Log.info
        "Final [%d,%d]:\n- Left:\n%a- Right:\n%a- Out:\n%a\n- Rel:\n%a"
        nl nr bpi out.j_inl bpi out.j_inr bpi out.j_out
        (Nrel.nr_fpri "   ") out.j_rel;
      Log.info "- Set instantiation left:\n%a- Set instantiation right:\n%a"
        (set_colv_inst_fpri "   ") out.j_inst_l.set_inst
        (set_colv_inst_fpri "   ") out.j_inst_r.set_inst;
      Log.info "- Seq instantiation left:\n%a- Seq instantiation right:\n%a"
        (seq_colv_inst_fpri "   ") out.j_inst_l.seq_inst
        (seq_colv_inst_fpri "   ") out.j_inst_r.seq_inst;
      Log.info "- Node instantiation left:\n%a"
        (sv_inst_fpri "   ") out.j_inst_l.sv_inst;
      Log.info "- Node instantiation right:\n%a"
        (sv_inst_fpri "   ") out.j_inst_r.sv_inst;
    end;
  out.j_out, out.j_rel,
  (out.j_inst_l, out.j_tmp_setinst_l),
  (out.j_inst_r, out.j_tmp_setinst_r)
