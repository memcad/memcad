(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_utils.mli
 **       utility functions on graphs
 ** Xavier Rival, 2011/05/21 *)
open Data_structures
open Lib
open Offs
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Set_sig
open Svenv_sig
open Dom_sig
open Inst_sig
open Vd_sig

open Gen_dom

(** Creation *)
(* Takes a set of inductive definitions *)
val graph_empty: StringSet.t -> graph
val ind_args_seg_to_ind_args: ind_args_seg -> ind_args

(** Pretty-printing *)
(* Graphs default printers *)
val nid_fpr: form -> sv -> unit
val onode_fpr: form -> svo -> unit
val svo_fpr: form -> svo -> unit
val bnode_fpr: form -> bnode -> unit
val nalloc_fpr: form -> nalloc -> unit
val nalloc_fprs: form -> nalloc -> unit
val node_attribute_fpr: form -> node_attribute -> unit
val pt_edge_fpr: form -> bnode * pt_edge -> unit
val ind_edge_fpr: form -> ind_edge -> unit
val seg_edge_fpr: form -> seg_edge -> unit
val block_fpria: string -> form -> sv * pt_edge Block_frag.t -> unit
val heap_frag_fpri: string -> form -> sv * heap_frag -> unit
val node_fpri: string -> form -> node -> unit
val graph_fpri: string -> form -> graph -> unit
(* Compact node output *)
val heap_frag_fprc: form -> heap_frag -> unit
(* Algorithms results *)
val is_le_res_fpr: form -> is_le_res -> unit
(* Hints pretty-printing *)
val hint_ug_fpr: form -> hint_ug -> unit
val hint_bg_fpr: form -> hint_bg -> unit
(* Node embedding pretty-printing *)
val node_emb_fpr: form -> node_emb -> unit
(* Freshsets *)
val freshset_fpr: form -> freshset -> unit
(* Results of unfolding *)
val unfold_res_fpri: string -> form -> unfold_res -> unit

(** Reachability inside the graph *)
(* this function is exported specifically for the list_domain *)
val pt_edge_block_frag_reach: pt_edge Block_frag.t -> SvSet.t

(** Sanity checks *)
val graph_sanity_check: string -> graph -> unit

(** Temporary code, sanity check on the hseg type *)
val hseg_sanity_check: isok:int -> string -> seg_edge -> unit

(** Checks whether an inductive predicates satisfies conditions for hseg *)
val is_ind_collection_sane: string -> ind -> unit

(** Number of edges (serves as a weak emptiness test) *)
val num_edges: graph -> int

(** Node operations *)
(* Node membership *)
val node_mem: sv -> graph -> bool
(* Node accessor *)
val node_find: sv -> graph -> node
(* Kind of the memory region attached to an SV *)
val sv_kind: sv -> graph -> region_kind
(* Addition of a new node with known id (crashes if already exists) *)
val sv_add: ?attr:node_attribute -> ?root: bool -> ?pt_prio: bool -> sv
  -> ntyp -> nalloc -> graph -> graph
(* Addition of a new, fresh node *)
val sv_add_fresh: ?attr:node_attribute -> ?root: bool -> ?pt_prio:bool
  -> ntyp -> nalloc -> graph -> sv * graph

(* Generation of set parameters to build an inductive predicate *)
val build_set_args: int -> graph -> graph * sv list

(** Computations on int_wk_typ; to replace with int_par_rec type *)
(* merge two integer weaken type *)
val merge_int_par_type: int_par_rec -> int_par_rec -> int_par_rec
(* convert types from inductive edge to segment end point *)
val seg_end_int_par_type: int_par_rec IntMap.t -> int_par_rec IntMap.t
(* type a list of integer parameters *)
val int_par_type_iargs_e: ind_args -> int_par_rec IntMap.t
  -> int_par_rec SvMap.t -> int_par_rec SvMap.t
(* compute weaken type for integer parameters in a graph *)
val int_par_type_iargs_g: graph -> (sv -> sv) -> int_par_rec SvMap.t
(* choose the integer parameters weaken type *)
val ind_seg_edge_int_par_typ: sv -> ind_edge -> (n_cons -> bool) -> pars_rec

(** Operations on COLV kinds *)
val setv_type:            graph -> sv -> set_par_type option
val col_par_type_to_kind: col_par_type -> col_kind
val col_kind_to_par_type: col_kind -> col_par_type
val col_kinds_of_ind_args_acc: ind_args -> col_kinds -> col_kinds
val col_kinds_of_ind_edge_acc: ind_edge -> col_kinds -> col_kinds
val col_kinds_of_seg_edge_acc: seg_edge -> col_kinds -> col_kinds
val col_kinds_diff: col_kinds -> col_kinds -> col_kinds

(** Management of COLVs *)
val is_info:        sv -> graph -> bool
val colv_add_fresh: ?root:bool -> col_par_type -> graph -> sv * graph
val colv_add:       ?root:bool -> sv -> ?info:Vd_sig.colv_info option
  -> col_kind -> graph -> graph
val colv_rem:       sv -> graph -> graph
(* roots of set/seq type *)
val setv_roots: graph -> SvSet.t
val seqv_roots: graph -> SvSet.t
val colv_is_root:   graph -> sv -> bool
val colv_get_roots: graph -> col_kinds
val colv_get_all:   graph -> col_kinds
val find_info: sv -> graph -> colv_info option

val gen_fresh_inst: SvSet.t -> 'a SvMap.t -> col_kind -> graph
  -> sv SvMap.t * graph * SvSet.t
(* generate the guard constraint that an inductive edge being empty *)
val cons_emp_ind: sv -> ind_edge -> n_cons option

(** Management of SVs *)
(* Releasing a root *)
val sv_unroot: sv -> graph -> graph
(* Removal of a node *)
val sv_rem: sv -> graph -> graph
(* Checks whether a node is the root of an inductive *)
val node_is_ind: sv -> graph -> bool
(* Get name of inductive attached to node, if any *)
val ind_of_node: node -> string option
(* Checks whether a node has points-to edges *)
val node_is_pt: sv -> graph -> bool
(* Checks whether a node is placed in memory *)
val node_is_placed: node -> bool
(* Asserts the node must be placed; crashes otherwise *)
val node_assert_placed: string -> node -> unit
(* Assume a node is placed (when its value is discovered to be an address) *)
val node_assume_placed: sv -> graph -> graph

(* Returns the set of all nodes *)
val get_all_nodes: graph -> SvSet.t
(* Returns the successors of a node as an explicit points-to edge source
 * None means we are not sure *)
val get_pt_successors_of_node: sv -> graph -> SvSet.t option
(* Returns the predecessors of a node *)
val get_predecessors_of_node: sv -> graph -> SvSet.t
(* Collect offsets from a base address *)
val collect_offsets: sv -> graph -> Offs.OffSet.t -> Offs.OffSet.t

(** Points-to edges operations *)
(* Existence of a points-to edge *)
val pt_edge_mem: Offs.svo -> graph -> bool
(* Creation of a block points to edge *)
val pt_edge_block_create: sv -> pt_edge -> graph -> graph
(* Appends a field at the end of a block *)
val pt_edge_block_append:
    ?nochk: bool (* de-activates check that bounds coincide (join) *)
  -> bnode -> pt_edge -> graph -> graph
(* Removal of a bunch of points-to edges from a node *)
val pt_edge_block_destroy: sv -> graph -> graph
(* Try to decide if an offset range is in a single points-to edge
 *  of a fragmentation, and if so, return its destination *)
val pt_edge_find_interval: (n_cons -> bool)
  -> sv (* node representing the base address *)
    -> Offs.t (* minimum offset of the range being looked for *)
      -> int       (* size of the range being looked for *)
        -> graph -> pt_edge option
(* Splitting of a points-to edge *)
val pt_edge_split: (n_cons -> bool) -> bnode -> Offs.size -> graph -> graph
(* Retrieval algorithm that encapsulates the search for extract and replace *)
val pt_edge_localize_in_graph:
    (n_cons -> bool) -> Offs.svo -> int -> graph -> pt_edge option
(* Replacement of a points-to edge by another *)
val pt_edge_replace:
    (n_cons -> bool) -> Offs.svo -> pt_edge -> graph -> graph
(* Extraction of a points to edge: reads, after split if necessary *)
val pt_edge_extract:
    (n_cons -> bool) -> Offs.svo -> int -> graph -> graph * pt_edge
(* Extend the search to other nodes, that are equal to another node *)
val pt_edge_localize_in_equal:
    (n_cons -> bool) -> Offs.svo -> graph -> SvPrSet.t

(** Inductive parameters applications *)
(* Empty set of arguments *)
val ind_args_empty: ind_args
val ind_args_s_empty: ind_args_seg

(* Making lists of arguments *)
val ind_args_1_make: ntyp -> int -> graph -> sv list * graph
(* Auxiliary functions to build fresh sets *)
val freshset_empty: freshset
val make_freshset: sv list -> sv list -> sv list -> sv list -> freshset
(* Adding the elements of a freshset to a graph *)
val freshset_add: freshset -> graph -> graph
(* Make a new inductive edge with fresh arg nodes *)
val ind_edge_make: string -> srcptr: sv list -> graph
  -> ind_edge * graph * freshset
(* Make a new segment edge with fresh arg nodes *)
val seg_edge_make: string -> srcptr: sv list -> dstptr: sv list
  -> ?int_par:(sv list * sv list) option -> sv -> graph
    -> seg_edge * graph * freshset

(** Inductive edges operations *)
(* Retrieve an inductive edge *)
val ind_edge_find: sv -> graph -> ind_edge
(* Inductive edge addition, removal *)
val ind_edge_add: sv -> ind_edge -> graph -> graph
val ind_edge_rem: sv -> graph -> graph
(* Function extracting *one* *inductive* edge (for is_le) *)
val ind_edge_extract_single: graph -> bool * (sv * ind_edge) option

(** Segment edges operations *)
(* This code should be temporary *)
val seg_edge_find: sv -> graph -> seg_edge
(* Addition of a segment edge *)
val seg_edge_add: sv -> seg_edge -> graph -> graph
(* Removal of a segment edge *)
val seg_edge_rem: sv -> graph -> graph
(* find sources of segments to some given node *)
val seg_edge_find_to: sv -> graph -> SvSet.t

(** Inductives and segments *)
(* Extraction + Removal of an inductive or segment edge *)
val ind_or_seg_edge_find_rem: sv -> graph
  -> ind_edge * (sv * ind_args_seg * ind_args) option * graph
(* Asserting that some inductives have no parameters of a certain kind
 * (useful for weakening rules that do not support parameters yet) *)
val assert_no_ptr_arg: string -> ind_args -> unit
val assert_no_int_arg: string -> ind_args -> unit
val assert_no_arg:     string -> ind_args -> unit

(** Memory management *)
(* Allocation and free of a memory block *)
val mark_alloc: sv -> int -> graph -> graph
val mark_free: sv -> graph -> graph

(** Tests that can be (partly) evaluated in the graphs *)
(* Equalities generated by the knowledge a segment be empty and empty seqv *)
val empty_segment_equalities: sv -> seg_edge -> SvPrSet.t
(* Reduction of a segment known to be empty *)
val red_empty_segment:
    sv -> graph -> graph * SvPrSet.t * set_cons list * SvSet.t
(* Experimental: Reduction of empty points-to edges *)
val red_empty_points_to: (n_cons -> bool) -> graph -> graph
(* Reduction: merging nodes that denote the same concrete value *)
val graph_merge_eq_nodes:
    (n_cons -> bool) -> SvPrSet.t -> graph -> graph * sv SvMap.t
(* whether a graph + a condition are not compatible
 * (i.e., we should consider _|_) *)
val graph_guard: bool -> n_cons -> graph -> guard_res

(** Sat for conditions that can be partly evaluated in the graphs *)
(* Check if an SV disequality constraint is satisfied in a graph *)
val sat_graph_diseq: graph -> sv -> sv -> bool

(** Segment splitting, for unfolding *)
val seg_split:
    sv -> graph ->
      graph * sv * Seq_sig.seq_cons list * (col_kind * colv_info option) SvMap.t

(** Utilities *)
val node_attribute_lub: node_attribute -> node_attribute -> node_attribute
val get_node: sv -> graph -> node
(* For join, creation of a graph with a set of roots, from two graphs *)
val init_graph_from_roots:
    ?tmpexp:bool ->
      submem:bool -> (sv * sv) SvMap.t -> (sv * sv) SvMap.t -> (sv * sv) SvMap.t
        -> graph -> graph -> graph * (col_kinds * col_kinds) * col_kinds
(* A sub-memory specific utility function:
 * - given a (node,offset) pair, search whether there exist an edge
 *   pointing to it *)
val node_offset_is_referred: graph -> sv * Offs.t -> bool

(** Latex output *)
val latex_output: (sv, string) PMap.t -> out_channel -> graph -> unit

(** Ensuring consistency with numerical values *)
val sve_sync_bot_up: graph -> graph * svenv_mod

(** Garbage collection support *)
(* Removal of all nodes not reachable from a set of roots *)
val gc_incr: sv Aa_sets.t -> graph -> graph
val gc: sv Aa_sets.t -> graph -> graph * SvSet.t

(** Functions on node injections (for is_le) *)
module Nemb:
    sig
      val empty: node_embedding
      (* Pretty-printing, compact version *)
      val ne_fpr: form -> node_embedding -> unit
      (* Pretty-printing, long version *)
      val ne_full_fpri: string -> form -> node_embedding -> unit
      (* Tests membership *)
      val mem: sv -> node_embedding -> bool
      (* Find an element in the mapping *)
      val find: sv -> node_embedding -> sv
      (* Add an element to the mapping *)
      val add: sv -> sv -> node_embedding -> node_embedding
      (* Initialization *)
      val init: node_emb -> node_embedding
      (* Extraction of siblings information *)
      val siblings: node_embedding -> SvSet.t SvMap.t
    end

(** Functions on node weak injections (for directed weakening) *)
module Nwkinj:
    sig
      val empty: node_wkinj
      (* Verification of existence of a mapping *)
      val mem: node_wkinj -> sv * sv -> sv -> bool
      (* Find function, may raise Not_found *)
      val find: node_wkinj -> sv * sv -> sv
      (* Addition of a mapping *)
      val add: node_wkinj -> sv * sv -> sv -> node_wkinj
      (* Pretty-printing, long and indented version *)
      val wi_fpri: string -> form -> node_wkinj -> unit
    end

(** Functions on node relations (for join) *)
module Nrel:
    sig
      val empty: node_relation
      (* Consistency check *)
      val check: node_relation -> bool
      (* Verification of existence of a mapping *)
      val mem: node_relation -> sv * sv -> sv -> bool
      (* Find function, may raise Not_found *)
      val find: node_relation -> sv * sv -> sv
      (* Find function from out, may raise Not_found *)
      val find_p: node_relation -> sv -> sv * sv
      (* Addition of a mapping *)
      val add: node_relation -> sv * sv -> sv -> node_relation
      (* To string, long and indented version *)
      val nr_fpri: string -> form -> node_relation -> unit
      (* To string, compact version *)
      val nr_fprc: form -> node_relation -> unit
      (* Collect nodes associated to other side *)
      val siblings: side -> sv -> node_relation -> SvSet.t
      (* Search for nodes that have multiple right matches *)
      val mult_bind_candidates: side -> node_relation -> SvSet.t -> SvSet.t
      (* Fold function *)
      val fold: (sv -> (sv * sv) -> 'a -> 'a) -> node_relation -> 'a -> 'a
    end

val add_colvinfo_nrel: graph -> graph -> graph
  -> node_relation -> node_relation -> node_relation

(** Operations on encoded graph edge components *)
val is_segment: Graph_sig.step -> bool
val is_offset: Graph_sig.step -> bool

(** Operations on abs_graph_arg: helping join *)
(* Check whether a segment extension could be performed *)
val is_bseg_ext: abs_graph_arg option -> sv
  -> abs_graph_arg option -> sv -> bool
(* Check whether a segment intro will be performed *)
val is_bseg_intro: abs_graph_arg option -> sv
  -> abs_graph_arg option -> sv -> bool

(* choose destination nodes for seg_intro from encode graph *)
val choose_dst: sv -> abs_graph_arg option -> SvSet.t -> SvSet.t

(** Operations on "join_arg" type: join with additional arguments *)
(* Translation of join argument *)
val tr_join_arg: (n_cons -> bool) -> graph * join_ele -> graph * join_arg
(* Find end ponits for segment extension *)
val seg_extension_end: sv -> join_arg -> (sv * string) option
(* Find a node in encoded graph *)
val encode_node_find: sv -> join_arg -> bool
(* Remove node and related edges in encoded graph *)
val remove_node: sv -> sv -> join_arg -> join_arg
(* Consume pt in encoded graph *)
val encode_pt_pt: Offs.svo -> Offs.svo -> join_arg -> join_arg

(* Collect all inductive definitions used in a graph *)
val collect_inds: graph -> StringSet.t

(* Control graphical output *)
val visu_opt_of_string: string -> visu_option

(** SV collapsings *)
(* Pretty-printing *)
val sv_collapsing_fpr: form -> sv_collapsing -> unit

(** SV submem modifications *)
(* Empty *)
val sv_submem_mod_empty: sv_submem_mod
(* Pretty-printing *)
val sv_submem_mod_fpr: string -> form -> sv_submem_mod -> unit

(** Experimental flag (temporary for fixing instantiation in join) *)
val do_exp_jinst: bool ref

(** Computation of a hint for join *)
val compute_hint_simpl: graph -> SvPrSet.t
