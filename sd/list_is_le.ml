(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_is_le.ml
 **       Inclusion checking for the list domain
 ** Xavier Rival, 2014/03/07 *)
open Data_structures
open Flags
open Lib
open Sv_def

open Ast_sig
open Ind_sig
open List_sig
open Nd_sig
open Col_sig
open Set_sig
open Seq_sig
open Vd_sig

open Gen_dom
open Gen_is_le

open Col_utils
open List_utils
open Nd_utils
open Set_utils
open Seq_utils
open Sv_utils
open Vd_utils


(** Error report *)
module Log =
  Logger.Make(struct let section = "l_isle__" and level = Log_level.DEBUG end)

let debug_module = false

(** State of the inclusion checking algorithm *)
type le_state =
    { (** Arguments and mappings from right to left *)
      (* Arguments configurations *)
      ls_cur_l:     lmem ;
      ls_cur_r:     lmem ;
      (* Mapping from right SVs into left SVs *)
      ls_cur_svi:   Graph_sig.node_embedding;
      (* Mapping from right COLVs into left COLVs *)
      ls_cur_cvi:   Col_sig.colv_embedding;
      (** Iteration strategy *)
      (* Pending rules (given as pairs of node names) *)
      ls_rules:     rules ; (* instances of rules pending application *)
      (* Nodes that were removed in the left argument (helps join) *)
      ls_rem_l:     SvSet.t ;
      (** Underlying domain constraints *)
      (* Satisfiability *)
      ls_vsat_l:    vsat ;
      (* Accumulation of right constraints to translate and discharge in
       * the left at a later stage (needed to prove inclusion) *)
      ls_ctr_r:     n_cons list ;
      ls_setctr_r:  set_cons list ;
      ls_seqctr_r:  seq_cons list ;
      (* Set&Seq-variables created by unfolding, during inclusion check *)
      ls_newsetv_r: SvSet.t;
      ls_newseqv_r: SvSet.t;
      (** Termination of the inclusion checking *)
      (* Whether we need to empty both graphs or only the left graph *)
      ls_emp_both:  bool ;
      (* Whether a success configuration has been reached *)
      ls_success:   bool ;
      (** Hints *)
      (* Hint on the left argument: nodes not to split *)
      ls_stop_l:    sv Aa_sets.t option ;
      (* Right remainder, excluded due to hint => left *)
      ls_excl_l:    l_call SvMap.t ;
      (* Optional "no empty unfold SVs", to inhibit unfold non empty *)
      ls_no_unf0:   SvSet.t ;
      (* Optional "end of segment SVs", to inhibit rules from these SVs *)
      ls_end_seg:   SvSet.t ; }
(* Pretty-printing of a configuration *)
let le_state_fpr (fmt: form) (ls: le_state): unit =
  let config_sep: string = "------------------------------------------\n" in
  let fl s f fmt l =
    let ff f fmt x = F.fprintf fmt "  %a" f x in
    if l = [ ] then F.fprintf fmt "%s: <none>\n" s
    else F.fprintf fmt "%s:\n%a\n" s (gen_list_fpr "" (ff f) "\n") l in
  F.fprintf fmt "%sLeft:\n%aRight:\n%a"
    config_sep (lmem_fpri " ") ls.ls_cur_l (lmem_fpri " ") ls.ls_cur_r;
  F.fprintf fmt "Injection:\n%a"
    (Graph_utils.Nemb.ne_full_fpri "  ") ls.ls_cur_svi;
  F.fprintf fmt "Colv Inj:\n%a" (CEmb.ne_full_fpri "  ") ls.ls_cur_cvi;
  (*config_sep;*)
  F.fprintf fmt "%a%a%a"
    (fl "Num_cons_r:" n_cons_fpr) ls.ls_ctr_r
    (fl "Set_cons_r:" set_cons_fpr) ls.ls_setctr_r
    (fl "Seq_cons_r:" seq_cons_fpr) ls.ls_seqctr_r;
  if ls.ls_newsetv_r != SvSet.empty then
    F.fprintf fmt "Newsetv: %a\n" setvset_fpr ls.ls_newsetv_r;
  if ls.ls_newseqv_r != SvSet.empty then
    F.fprintf fmt "Newseqv: %a\n" seqvset_fpr ls.ls_newseqv_r;
  F.fprintf fmt "%s" config_sep

(** Utilities *)

(* Generate a fresh right node, to be mapped with some given node *)
let gen_map_id (nt: ntyp) (il: sv) (ls: le_state): sv * (le_state) =
  let ir, g = sv_add_fresh nt ls.ls_cur_r in
  ir, { ls with
        ls_cur_r = g ;
        ls_cur_svi = Graph_utils.Nemb.add ir il ls.ls_cur_svi }

(* Map the ptr, set & seq arguments of right ind call into the ptr, set & seq
 * arguments of right ind call, in the case where two edges are consumed in the
 * same time:
 *  -> parameters are matched exactly *)
let mapdir_ind_pars, mapdir_seg_pars =
  let mapdir_ptrpars (ptrargs_l: sv list) (ptrargs_r: sv list)
      (ls: le_state) : le_state =
    let ls_cur_svi =
      try
      List.fold_left2
        (fun acc r l ->
        (* we check that this argument is not already bind with another one *)
          if Graph_utils.Nemb.mem r acc then
            if Graph_utils.Nemb.find r acc != l then
              let err = F.asprintf "mapdir_ind_pars %a => %a ≠ %a"
                  sv_fpr r sv_fpr (Graph_utils.Nemb.find r acc) sv_fpr l in
              raise (Le_false err)
            else acc
          else
            Graph_utils.Nemb.add r l acc)
        ls.ls_cur_svi ptrargs_r ptrargs_l
      with Invalid_argument _ ->
        Log.fatal_exn "mapdir_ind_ptrpars: arguments not match" in
    { ls with ls_cur_svi } in
  let mapdir_colpars (ck: col_kind) (colargs_l: sid list) (colargs_r: sid list)
      (emb: colv_embedding): colv_embedding =
    try
      List.fold_left2 (fun acc r l -> Col_utils.CEmb.add r l ck acc)
        emb colargs_r colargs_l
    with Invalid_argument _ ->
      Log.fatal_exn "mapdir_ind_colpars: arguments not match" in
  let mapdir_setpars (setargs_l: sid list) (setargs_r: sid list)
      (ls: le_state) : le_state =
    { ls with
      ls_cur_cvi = mapdir_colpars CK_set setargs_l setargs_r ls.ls_cur_cvi } in
  let mapdir_seqpars (seqargs_l: sid list) (seqargs_r: sid list)
      (ls: le_state) : le_state =
    { ls with
      ls_cur_cvi = mapdir_colpars CK_seq seqargs_l seqargs_r ls.ls_cur_cvi } in
  let mapdir_ind_pars (lcl: l_call) (lcr: l_call) (ls: le_state) : le_state =
    assert (l_call_compare lcl lcr = 0);
    ls
      |> mapdir_ptrpars lcl.lc_ptrargs lcr.lc_ptrargs
      |> mapdir_setpars lcl.lc_setargs lcr.lc_setargs
      |> mapdir_seqpars lcl.lc_seqargs lcr.lc_seqargs
  and mapdir_seg_pars (lsegcl: lseg_call) (lsegcr: lseg_call) (ls: le_state)
    : le_state =
    assert (lseg_call_compare lsegcl lsegcr = 0);
    ls
      |> mapdir_ptrpars lsegcl.lc_ptrargs lsegcr.lc_ptrargs
  in mapdir_ind_pars, mapdir_seg_pars


(* Map the set arguments of right ind call into the set arguments of right
 * ind call, in the case where the right edge is split
 *  -> parameters are adjusted, depending on their properties *)
let mapsplit_ind_setseqpars (lcl: l_call) (lsegl: lseg_call) (lcr: l_call)
    (ls: le_state)
    : le_state * l_call =
  assert (l_call_compare lcl lcr = 0);
  (* First we map the pointer arguments at the begining of the segments *)
  let ls = (* if true then ls else *)
    let ls_cur_svi =
      try
      List.fold_left2
        (fun acc l r ->
        (* we check that this argument is not already bind with another one *)
          if Graph_utils.Nemb.mem r acc then
            if Graph_utils.Nemb.find r acc != l then
              let err = F.asprintf "mapsplit_ind_pars %a => %a & %a" sv_fpr r
                  sv_fpr l sv_fpr (Graph_utils.Nemb.find r acc) in
              raise (Le_false err)
            else acc
          else
            Graph_utils.Nemb.add r l acc)
        ls.ls_cur_svi lcl.lc_ptrargs lcr.lc_ptrargs
      with Invalid_argument _ ->
        Log.fatal_exn "mapdir_ind_ptrpars: arguments not match" in
    { ls with ls_cur_svi } in
  (* ptrargs are fresh and mapped to the ptrargs of the END of the segment *)
  let ls, new_ptrargs =
    List.fold_left
      (fun (ls, acc_ptr) ptr ->
        let new_ptr, ls = gen_map_id Ntaddr ptr ls in
        ls, new_ptr :: acc_ptr )
      (ls,[]) lsegl.lc_ptrargs in
  let set_params =
    match lcl.lc_def.ld_set with
    | None -> []
    | Some si -> si.s_params in
  let ls, new_setargs =
    let rec aux ((ls, acc_call) as acc) lcl lcr params =
      match lcl, lcr, params with
      | [ ], [ ], [ ] -> acc
      | il :: lcl0, ir :: lcr0, k :: params0 ->
          (* generate two keys in the right for the split edges *)
          let ir0, lmr = (* map to il *)
            colv_add_fresh (Ct_set (Some k)) ls.ls_cur_r in
          let ir1, lmr = (* remaining par *)
            colv_add_fresh (Ct_set (Some k)) lmr in
          let setctr =
            if k.st_const then
              [ S_eq (S_var ir, S_var ir0) ; S_eq (S_var ir, S_var ir1) ]
            else if k.st_add || k.st_head then
              [ S_eq (S_var ir, S_uplus (S_var ir0, S_var ir1)) ]
            else Log.todo_exn "unhandled kind" in
          let acc =
            { ls with
              ls_cur_r    = lmr;
              ls_cur_cvi  = Col_utils.CEmb.add ir0 il CK_set ls.ls_cur_cvi;
              ls_setctr_r = setctr @ ls.ls_setctr_r }, ir1 :: acc_call in
          aux acc lcl0 lcr0 params0
      | _, _, _ ->
          Log.fatal_exn "mapsplit_ind_setpars: pars of distinct lengths" in
    aux (ls, [ ]) lcl.lc_setargs lcr.lc_setargs set_params in
  let seq_params =
    match lcl.lc_def.ld_seq with
    | None -> []
    | Some si -> si.q_params in
  let ls, new_seqargs =
    let rec aux ((ls, acc_call) as acc) lcl lcr params =
      match lcl, lcr, params with
      | [ ], [ ], [ ] -> acc
      | il :: lcl0, ir :: lcr0, k :: params0 ->
          (* generate two keys in the right for the split edges *)
          let ir0, lmr = (* map to il *)
            colv_add_fresh (Ct_seq (Some k)) ls.ls_cur_r in
          let ir1, lmr = (* remaining par *)
            colv_add_fresh (Ct_seq (Some k)) lmr in
          let seqctr =
            if k.sq_const then
              [ Seq_equal (Seq_var ir, Seq_var ir0) ;
                Seq_equal (Seq_var ir, Seq_var ir1) ]
            else if k.sq_add then
              [ Seq_equal (Seq_var ir, Seq_Concat [Seq_var ir0; Seq_var ir1]) ]
            else Log.todo_exn "unhandled seq parameter kind" in
          let newseqv = ls.ls_newseqv_r
              |> SvSet.add ir0
              |> SvSet.add ir1 in
          let acc =
            { ls with
              ls_cur_r     = lmr;
              ls_cur_cvi   = CEmb.add ir0 il CK_seq ls.ls_cur_cvi;
              ls_newseqv_r = newseqv;
              ls_seqctr_r  = seqctr @ ls.ls_seqctr_r }, ir1 :: acc_call in
          aux acc lcl0 lcr0 params0
      | _, _, _ ->
          Log.fatal_exn "mapsplit_ind_seqpars: pars of distinct lengths" in
    aux (ls, [ ]) lcl.lc_seqargs lcr.lc_seqargs seq_params in
  ls, { lcl with
        lc_ptrargs = List.rev new_ptrargs ;
        lc_setargs = List.rev new_setargs ;
        lc_seqargs = List.rev new_seqargs }


(** Management of the set of applicable rules *)
(* Collecting applicable rules from a given SV *)
let collect_rules_node_gen =
  let sv_seg_ind i g =
    match (sv_find i g).ln_e with
    | Lhlseg (_, d, _) -> Some d
    | Lhemp | Lhpt _ | Lhlist _ -> None in
  collect_rules_sv_gen sv_kind sv_seg_ind
let collect_rules_node = collect_rules_node_gen false None
let collect_rules_node_st (il: sv) (ir: sv) (ls: le_state): le_state =
  let nr =
    collect_rules_node_gen false ls.ls_stop_l ls.ls_end_seg ls.ls_cur_svi
      ls.ls_cur_l ls.ls_cur_r il ir ls.ls_rules in
  { ls with ls_rules = nr }
(* Initialization: makes prioretary points-to rules *)
let rules_init
    (prio: bool) (* whether available pt-edges should be treated in priority *)
    (es: SvSet.t) (* end of segment(s), if any *)
    (ni: Graph_sig.node_embedding)
    (xl: lmem) (xr: lmem)
    (r: Graph_sig.node_emb)
    : rules =
  if !Flags.flag_dbg_is_le_list then
    Log.force "isle init,l:\n%aisle init,-r:\n%a"
      (lmem_fpri "  ") xl (lmem_fpri "  ") xr;
  let r =
    Aa_maps.fold
      (fun ir il acc ->
        if !Flags.flag_dbg_is_le_list then
          Log.force "collecting at %a,%a" sv_fpr il sv_fpr ir;
        collect_rules_node_gen prio None es ni xl xr il ir acc
      ) r empty_rules in
  r


(** Individual rules *)
(* Unfolding rules that do not appear here (part of unfold):
 *    emp - ind
 *    pt - ind
 *    pt - seg *)
(* Rule pt - pt *)
let apply_pt_pt (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = sv_find isl ls.ls_cur_l in
  let nr = sv_find isr ls.ls_cur_r in
  match nl.ln_e, nr.ln_e with
  | Lhpt mcl, Lhpt mcr ->
      if Block_frag.(byte_size mcl <> byte_size mcr) then
        (let err = Printf.sprintf "pt_pt incompatoble block size %i != %i"
          (Block_frag.byte_size mcl) (Block_frag.byte_size mcr) in
        raise (Le_false err));
      (* check alloc to add again *)
      (* check that the blocks are compatible *)
      let ls =
        Block_frag.fold_base
          (fun os pl ls ->
            let pr =
              try Block_frag.find_addr os mcr
              with _ -> Log.fatal_exn "NF in apply_pt_pt" in
            (* destination offsets *)
            let dl = pl.Graph_sig.pe_dest and dr = pr.Graph_sig.pe_dest in
            let odl = snd dl and odr = snd dr in
            if not (Offs.t_is_const odl && Offs.t_is_const odr
                      && Offs.compare odl odr = 0) then
              raise (Le_false "incompatible destination offsets");
            (* destination fields *)
            let idl = fst dl and idr = fst dr in
            let b_fail_dest =
              try Graph_utils.Nemb.find idr ls.ls_cur_svi != idl
              with Not_found -> false in
            if b_fail_dest then
              begin
                let err =
                  F.asprintf "pt-pt: b_fail_dest at offset: %a"
                    Bounds.t_fpr os in
                raise (Le_false err)
              end;
            collect_rules_node_st idl idr
              { ls with
                ls_cur_svi = Graph_utils.Nemb.add idr idl ls.ls_cur_svi;
                ls_rem_l   = SvSet.add isl ls.ls_rem_l; }
          ) mcl ls in
      let vrules = invalidate_rules isl isr Kpt Kpt ls.ls_rules in
      { ls with
        ls_cur_l = pt_edge_block_destroy ~remrc:true isl ls.ls_cur_l;
        ls_cur_r = pt_edge_block_destroy ~remrc:true isr ls.ls_cur_r;
        ls_rules = vrules; }
  | _, _ -> Log.fatal_exn "pt-pt; improper config"
(* ind - ind *)
let apply_ind_ind (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = sv_find isl ls.ls_cur_l in
  let nr = sv_find isr ls.ls_cur_r in
  match nl.ln_e, nr.ln_e with
  | Lhlist ldl, Lhlist ldr ->
      if l_call_compare ldl ldr = 0 then
        let ls = mapdir_ind_pars ldl ldr ls in
        { ls with
          ls_cur_l = list_edge_rem isl ls.ls_cur_l;
          ls_cur_r = list_edge_rem isr ls.ls_cur_r;
          ls_rem_l = SvSet.add isl ls.ls_rem_l;
          ls_rules = invalidate_rules isl isr Kind Kind ls.ls_rules; }
      else Log.fatal_exn "list descriptors do not match"
  | Lhemp, Lhemp ->
      (* both edges were consumed by another rule somehow;
       * we can discard the application of that rule *)
      ls
  | _, _ -> Log.fatal_exn "ind-ind; improper config"
(* seg - seg *)
let apply_seg_seg (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = sv_find isl ls.ls_cur_l in
  let nr = sv_find isr ls.ls_cur_r in
  match nl.ln_e, nr.ln_e with
  | Lhlseg (ld0, id0, lseg0), Lhlseg (ld1, id1, lseg1) ->
      if l_call_compare ld0 ld1 = 0 then
        if Graph_utils.Nemb.mem id0 ls.ls_cur_svi then
          (* two segments match perfectly *)
          let idl = Graph_utils.Nemb.find id0 ls.ls_cur_svi in
          if idl != id1 then raise (Le_false "segment mapping");
          let ls = ls |> mapdir_ind_pars ld0 ld1
                      |> mapdir_seg_pars lseg0 lseg1 in
          { ls with
            ls_cur_l = lseg_edge_rem isl ls.ls_cur_l;
            ls_cur_r = lseg_edge_rem isr ls.ls_cur_r;
            ls_rem_l = SvSet.add isl ls.ls_rem_l;
            ls_rules = invalidate_rules isl isr Kseg Kseg ls.ls_rules; }
        else (* segment in the left included in a segment in the right *)
          let insrc, ls = gen_map_id Ntaddr id0 ls in
          let ls, r_seg_ld1 = mapsplit_ind_setseqpars ld0 lseg0 ld1 ls in
          let rr =
            ls.ls_cur_r
            |> lseg_edge_rem isr
            |> lseg_edge_add insrc id1 r_seg_ld1 lseg1 in
          collect_rules_node_st id0 insrc
            { ls with
              ls_cur_l = lseg_edge_rem isl ls.ls_cur_l;
              ls_cur_r = rr;
              ls_rem_l = SvSet.add isl ls.ls_rem_l;
              ls_rules = invalidate_rules isl isr Kseg Kseg ls.ls_rules; }
      else Log.todo_exn "unhandled seg-seg case"
  | _, _ -> Log.fatal_exn "seg-seg; improper config"
(* seg - ind *)
let apply_seg_ind (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nl = sv_find isl ls.ls_cur_l in
  let nr = sv_find isr ls.ls_cur_r in
  match nl.ln_e, nr.ln_e with
  | Lhlseg (ldl, idl, lsegcl), Lhlist ldr ->
      if l_call_compare ldl ldr = 0 then
        (* the segment in the left is a part of an inductive in the right *)
        (* remove the inductive edge being matched in the right side graph *)
        let ls = { ls with ls_cur_r = list_edge_rem isr ls.ls_cur_r } in
        (* add a fresh (middle point) node in the right side graph *)
        let insrc, ls = gen_map_id Ntaddr idl ls in
        let ls, ind_ldr = mapsplit_ind_setseqpars ldl lsegcl ldr ls in
        collect_rules_node_st idl insrc
          { ls with
            ls_cur_l = lseg_edge_rem isl ls.ls_cur_l;
            ls_cur_r = list_edge_add insrc ind_ldr ls.ls_cur_r;
            ls_rem_l = SvSet.add isl ls.ls_rem_l;
            ls_rules = invalidate_rules isl isr Kseg Kind ls.ls_rules; }
      else Log.todo_exn "unhandled seg-ind case"
  | _, _ -> Log.fatal_exn "seg-ind; improper config"
(* void - seg *)
let apply_void_seg (isl: sv) (isr: sv) (ls: le_state): le_state =
  let nr = sv_find isr ls.ls_cur_r in
  match nr.ln_e with
  | Lhlseg (ldr, idr, lsegr) ->
      let ext_l =
        try Graph_utils.Nemb.find idr ls.ls_cur_svi
        with Not_found -> Log.fatal_exn "emp-seg: ext not mapped" in
      if ext_l = isl then
        (* We add equality constraints for ptr args *)
        let ls =
          List.fold_left2
            (fun ls ptr_s ptr_d ->
              let find_opt ptr =
                try Some (Graph_utils.Nemb.find ptr ls.ls_cur_svi)
                with Not_found -> None in
              match find_opt ptr_s, find_opt ptr_d with
              | Some ptr_l, None ->
                  { ls with
                    ls_cur_svi = Graph_utils.Nemb.add ptr_d ptr_l ls.ls_cur_svi }
              | None, Some ptr_l ->
                  { ls with
                    ls_cur_svi = Graph_utils.Nemb.add ptr_s ptr_l ls.ls_cur_svi }
              | Some ptr_s, Some ptr_d when ptr_s = ptr_d -> ls
              | None, None
              | Some _ , Some _ ->
                  let cons =
                    Nc_cons ( Apron.Tcons1.EQ, Ne_var ptr_s, Ne_var ptr_d) in
                  Log.info "tmp,ctr_r(vs): %a" n_cons_fpr cons;
                  { ls with ls_ctr_r = cons :: ls.ls_ctr_r } )
            ls ldr.lc_ptrargs lsegr.lc_ptrargs in
        (* the segment can be mapped onto an empty region;
         * we get equalities on additive set variables  *)
        let set_par =
          match ldr.lc_def.ld_set with
          | None -> [ ]
          | Some si -> si.s_params in
        let ls =
          try
            List.fold_left2
              (fun ls r f ->
                if f.st_add || f.st_head then
                  let sr = S_eq (S_var r, S_empty) in
                  { ls with ls_setctr_r = sr :: ls.ls_setctr_r; }
                else ls
              ) ls ldr.lc_setargs set_par
          with
          | Invalid_argument _ ->
              Log.fatal_exn "apply_void_seg: set paramters not match" in
        (* We also add equalities on additive seq variables *)
        let seq_par =
          match ldr.lc_def.ld_seq with
          | None -> [ ]
          | Some si -> si.q_params in
        let ls =
          try
            List.fold_left2
             (fun ls r f ->
               if f.sq_add then
                 let sr = Seq_equal (Seq_var r, Seq_empty) in
                 { ls with ls_seqctr_r = sr :: ls.ls_seqctr_r; }
               else ls
             ) ls ldr.lc_seqargs seq_par
          with
          | Invalid_argument _ ->
              Log.fatal_exn "apply_void_seg: seq paramters not match" in
        { ls with
          ls_cur_r = lseg_edge_rem isr ls.ls_cur_r ;
          ls_rules = invalidate_rules isl isr Kemp Kseg ls.ls_rules }
      else (* segment not mapped into an empty region *)
        raise (Le_false (F.asprintf "segment to non empty: _=_> %a" sv_fpr idr))
  | _ -> Log.fatal_exn "void-seg; improper config"

(* stop rule
 *   this rule is specific to inductive edge search
 *   when a stop SV is encountered *)
let apply_stop_list (isl: sv) (isr: sv) (ls: le_state): le_state=
  (* We may discard the node left in the right graph, and
   * propagate it as a remainder in the right graph!
   * then, if all remainder is of the form  x.i()  it means
   * we may synthesize a (strong) implication edge.
   * To achieve that, we move right inductive edge into a
   * placeholder (ls_excl_l), to be checked at the end. *)
  if !Flags.flag_dbg_is_le_list then
    Log.info "IsLe: reached a stop node, about to stop";
  let nr = sv_find isr ls.ls_cur_r in
  match nr.ln_e with
  | Lhemp | Lhpt _ -> ls (* nothing to do *)
  | Lhlist ld ->
      { ls with
        ls_cur_r  = list_edge_rem isr ls.ls_cur_r ;
        ls_excl_l = SvMap.add isl ld ls.ls_excl_l }
  | Lhlseg _ -> Log.todo_exn "apply_stop_node_ind: segment"


(** Post inclusion check routine *)
(* Checks whether a configuration is a success configuration *)
exception Unmapped
let is_success (ls: le_state): le_state =
  let num_l = num_edges ls.ls_cur_l in
  let num_r = num_edges ls.ls_cur_r in
  if !Flags.flag_dbg_is_le_list then
    Log.info "%aIsLe,Return: %d | %d edges" le_state_fpr ls num_l num_r;
  if (not ls.ls_emp_both || num_l = 0) && num_r = 0 then
    (* Inclusion proved in the memory domain; try to discharge the proof
     * obligations that were generated, and pass the remaining ones, either
     * for instantiation in the join, or for discharging in the valset domain
     * later *)
    (* Translation function for SVs *)
    let sv_trans (i: sv) =
      try Graph_utils.Nemb.find i ls.ls_cur_svi
      with
      | Not_found ->
          Log.info "SV renaming failed (is_le) %a" sv_fpr i;
          raise Unmapped in
    (* Translation function for COLVs *)
    let colv_trans (i: sv): sv =
      try
        let s, ck = SvMap.find i ls.ls_cur_cvi.n_img in
        if SvSet.cardinal s > 1 then
          begin
            Log.info "choices (%a): %a" (colv_fpr ck) i (colvset_fpr ck) s;
            Log.warn "colv_trans needs to choose among several Set COLVs...";
          end;
        (* we select the max element to avoid hitting the roots *)
        if SvSet.cardinal s >= 1 then SvSet.max_elt s
        else Log.fatal_exn "empty set of COLVs (though should be non empty)"
      with
      | Not_found ->
          if !Flags.flag_dbg_is_le_list then
            Log.debug "COLV renaming failed (is_le) %a" sv_fpr i;
          raise Unmapped in
    (* Inclusion established in the list domain; moving to pure predicates *)
    if !Flags.flag_dbg_is_le_list then
      begin
        let r1 = ls.ls_seqctr_r != [] in
        let r0 = r1 || ls.ls_setctr_r != [] in
        Log.info
          "Predicates to look at: %d,%d,%d\n%a%a%a" (List.length ls.ls_ctr_r)
          (List.length ls.ls_setctr_r) (List.length ls.ls_seqctr_r)
          (gen_list_items_fpr ~iind:2 ~lret:r0 n_cons_fpr) ls.ls_ctr_r
          (gen_list_items_fpr ~iind:2 ~lret:r1 set_cons_fpr) ls.ls_setctr_r
          (gen_list_items_fpr ~iind:2 seq_cons_fpr) ls.ls_seqctr_r;
      end;
    (* First, we look at numerical constraints *)
    let l_remain_num_cons =
      (* Before trying to discharge all constraints, we rename them, and
       * move out of the way those that cannot be fully renamed due to
       * node instantiations being required *)
      let renamed_l, non_renamed_r =
        List.fold_left
          (fun (accl, accr) ctr ->
            if !Flags.flag_dbg_is_le_list then
              Log.info "Constraints on the right nodes, to rename: %a"
                n_cons_fpr ctr;
            try
              n_cons_map sv_trans ctr :: accl, accr
            with
            | Unmapped ->
                if !Flags.flag_dbg_is_le_list then
                  Log.info "Rename-num-KO: %a" Nd_utils.n_cons_fpr ctr;
                accl, ctr :: accr
          ) ([ ], [ ]) (List.rev ls.ls_ctr_r) in
      if non_renamed_r != [ ] then
        Log.fatal_exn "Non renamed SVs in num pure constraints";
      (* Discharging of num proof obligations *)
      List.fold_left
        (fun acc lctr ->
          let bres = ls.ls_vsat_l.vs_num lctr in
          if !Flags.flag_dbg_is_le_list then
            Log.info "Verifying num cstr on left: %a => %b"
              Nd_utils.n_cons_fpr lctr bres;
          if bres then acc
          else lctr :: acc
        ) [ ] renamed_l in
    (* Second, we look at set constraints:
     *  - setcons_proved: boolean, ::= "all renamed constraints get proved"
     *  - setcons_rem:    cannot be renamed; preserved for instantiation *)
    let setcons_proved, setcons_rem =
      (* Simplify the constraints, by removing Set COLVs that were added by
       * unfolding during the inclusion check, and that are not mapped *)
      let all_setvs =
        List.fold_left (fun acc c -> SvSet.union acc (set_cons_setvs c))
          SvSet.empty ls.ls_setctr_r in
      let rem_setvs =
        SvSet.filter
          (fun i -> not (SvMap.mem i ls.ls_cur_cvi.n_img)
              && SvSet.mem i ls.ls_newsetv_r) all_setvs in
      if !Flags.flag_dbg_is_le_list then
        Log.debug "Should remove Set COLVs: %a" svset_fpr rem_setvs;
      let setcons = set_cons_prune_setvs rem_setvs ls.ls_setctr_r in
      List.fold_left
        (fun (accb, accl) c ->
          try
            let rc = Set_utils.s_cons_map sv_trans colv_trans c in
            let satres = ls.ls_vsat_l.vs_set rc in
            if !Flags.flag_dbg_is_le_list then
              Log.info "Verifying set cstr on left: %a => %a => %b"
                Set_utils.set_cons_fpr c Set_utils.set_cons_fpr rc satres;
            accb && satres, accl
          with
          | Unmapped ->
              if !Flags.flag_dbg_is_le_list then
                Log.info "Rename-set-KO: %a" Set_utils.set_cons_fpr c;
              accb, c :: accl
        ) (true, [ ]) setcons in
    (* Rename and prove equalities induced by the relation *)
    let setcons_proved =
      SvMap.fold
        (fun i (s, ck) acc ->
          if ck = CK_set && SvSet.cardinal s > 1 then
            let rep = SvSet.min_elt s in
            let others = SvSet.remove rep s in
            SvSet.fold
              (fun j acc ->
                let b = ls.ls_vsat_l.vs_set (S_eq (S_var j, S_var rep)) in
                if !Flags.flag_dbg_is_le_list then
                  Log.info "Adding Set COLV-eq: S[%a] = S[%a]: %b"
                    sv_fpr i sv_fpr j b;
                acc && b
              ) others acc
          else acc
        ) ls.ls_cur_cvi.n_img setcons_proved in
    (* Thirdly, we look at seq constraints:
     *  - seqcons_proved: boolean, ::= "all renamed constraints get proved"
     *  - seqcons_rem:    cannot be renamed; preserved for instantiation *)
    let seqcons_proved, seqcons_rem =
      (* Simplify the constraints, by removing Seq COLVs that were added by
       * unfolding during the inclusion check, and that are not mapped *)
      let all_seqvs =
        List.fold_left
          (fun acc c -> SvSet.union acc (seq_cons_seqvs c))
          SvSet.empty ls.ls_seqctr_r in
      let rem_seqvs =
        SvSet.filter
          (fun i ->
            not (SvMap.mem i ls.ls_cur_cvi.n_img)
              && SvSet.mem i ls.ls_newseqv_r) all_seqvs in
      if !Flags.flag_dbg_is_le_list then
        Log.debug "Should remove Seq COLVs: %a" svset_fpr rem_seqvs;
      let seqcons = seq_cons_prune_seqvs rem_seqvs ls.ls_seqctr_r in
      List.fold_left
        (fun (accb, accl) c ->
          try
            if !Flags.flag_dbg_is_le_list then
              Log.debug "constraint:   %a" Seq_utils.seq_cons_fpr c;
            let rc = Seq_utils.q_cons_map colv_trans sv_trans c in
            if !Flags.flag_dbg_is_le_list then
              Log.debug "constraint => %a" Seq_utils.seq_cons_fpr rc;
            let satres = ls.ls_vsat_l.vs_seq rc in
            if !Flags.flag_dbg_is_le_list then
              Log.debug " satisfaction: %b" satres;
            accb && satres, accl
          with
          | Unmapped ->
              if !Flags.flag_dbg_is_le_list then
                Log.info "Rename-seq-KO: %a" Seq_utils.seq_cons_fpr c;
              accb, c :: accl
        ) (true, [ ]) seqcons in
    (* Rename and prove equalities induced by the relation *)
    let seqcons_proved =
      SvMap.fold
        (fun i (s, ck) acc ->
          if ck = CK_seq && SvSet.cardinal s > 1 then
            let rep = SvSet.min_elt s in
            let others = SvSet.remove rep s in
            SvSet.fold
              (fun j acc ->
                let b =
                  ls.ls_vsat_l.vs_seq (Seq_equal (Seq_var j, Seq_var rep)) in
                if debug_module then
                  Log.debug "Adding Seq COLV-eq: Q[%a] = Q[%a]: %b"
                    sv_fpr i sv_fpr j b;
                acc && b
              ) others acc
          else acc
        ) ls.ls_cur_cvi.n_img seqcons_proved in
    if !Flags.flag_dbg_is_le_list then
      Log.debug "Setcons:%b,%d\nSeqcons:%b,%d"
        setcons_proved (List.length setcons_rem)
        seqcons_proved (List.length seqcons_rem);
    { ls with
      ls_ctr_r    = [ ] ; (* accumulator becomes empty *)
      ls_setctr_r = setcons_rem;
      ls_seqctr_r = seqcons_rem;
      ls_success  = ( setcons_proved
                    && seqcons_proved
                    && List.length l_remain_num_cons = 0) }
  else (* Inclusion could not be established in the memory domain *)
    { ls with ls_success  = false }

(** The new inclusion algorithm, with refactored strategy application *)
(* This function is based on a recursive algorithm implementing a worklist
 * on applicable rules (not on nodes or edges!) *)
let rec is_le_rec (ls: le_state): le_state =
  (* Find out the next rule to apply *)
  match rules_next ls.ls_rules with
  | None ->
      if !Flags.flag_dbg_is_le_list then
        Log.info "IsLe-NoRule:\n%a" le_state_fpr ls;
      ls
  | Some (k, (il, ir), rem_rules) ->
      if !Flags.flag_dbg_is_le_list then
        begin
          Log.info "%aIsLe-Treating (%a,%a): %a"
            le_state_fpr ls sv_fpr il sv_fpr ir rkind_fpr k;
          if !Flags.flag_dbg_is_le_strategy then
            Log.info "isle-rules ready:\n%a" rules_fpr ls.ls_rules
        end;
      let ls = { ls with ls_rules = rem_rules } in
      let ls =
        match k with
        | Rstop -> apply_stop_list  il ir ls
        | Rpp   -> apply_pt_pt      il ir ls
        | Rii   -> apply_ind_ind    il ir ls
        | Rss   -> apply_seg_seg    il ir ls
        | Rsi   -> apply_seg_ind    il ir ls
        | Rvs   -> apply_void_seg   il ir ls
        | Rei   -> is_le_unfold true  il ir ls
        | Rps
        | Rpi   -> is_le_unfold false il ir ls in
      is_le_rec ls
and is_le_unfold
    (hint_empty: bool) (* whether to consider empty rules first or last*)
    (il: sv) (ir: sv) (ls: le_state): le_state =
  if !Flags.flag_dbg_is_le_list then
    Log.force "IsLe triggerring unfolding<%b>" hint_empty;
  let l_mat =
    (* if only non empty unfold allowed, do selective unfolding *)
    let only_non_emp = SvSet.mem ir ls.ls_no_unf0 in
    List_mat.unfold ir only_non_emp ls.ls_cur_r Udir_fwd in
  if !Flags.flag_dbg_is_le_list then
    Log.force "IsLe performed unfolding: %d" (List.length l_mat);
  let els =
    List.fold_left
      (fun acc ur ->
        match acc with
        | Some _ ->
            (* inclusion already found, no other check *)
            acc
        | None ->
            (* inclusion not found yet; we try current disjunct *)
            List.iter
              (fun ctr ->
                if !Flags.flag_dbg_is_le_list then
                  Log.force "Num Predicate added to prove, on right side: %a"
                    Nd_utils.n_cons_fpr ctr
              ) ur.ur_cons;
            try
              let ls0 =
                let nsetvs, nseqvs =
                  SvMap.fold
                    (fun i k (sets,seqs) ->
                      match k with
                      | CK_set -> SvSet.add i sets, seqs
                      | CK_seq -> sets, SvSet.add i seqs
                    ) ur.ur_newcolvs (ls.ls_newsetv_r, ls.ls_newseqv_r) in
                List.iter (fun c -> Log.info "tmp,ctr_r(u): %a" n_cons_fpr c)
                  ur.ur_cons;
                collect_rules_node_st il ir
                  { ls with
                    ls_cur_r     = ur.ur_lmem;
                    ls_ctr_r     = ur.ur_cons @ ls.ls_ctr_r;
                    ls_setctr_r  = ur.ur_setcons @ ls.ls_setctr_r;
                    ls_seqctr_r  = ur.ur_seqcons @ ls.ls_seqctr_r;
                    ls_newsetv_r = nsetvs;
                    ls_newseqv_r = nseqvs; } in
              let ols = is_le_rec ls0 in
              let lsuccess = is_success ols in
              if lsuccess.ls_success then Some lsuccess
              else None
            with
            | Le_false msg ->
                if !Flags.flag_dbg_is_le_list then
                  Log.force "is_success returned and failed: %s" msg;
                (* underlying test may fail, while next succeeds *)
                (* hence, we catch Le_false and return None here *)
                None
      ) None l_mat in
  match els with
  | None -> raise (Le_false "unfold: no successful branch")
  | Some ls ->
      assert ls.ls_success ;
      ls

(** General inclusion functions (inclusion testing engine) *)
let is_le_start (ls: le_state): le_state option =
  (* Iteration *)
  let ols = is_le_rec ls in
  (* Computation of the inclusion check result *)
  let lls = is_success ols in
  if lls.ls_success then
    (* inclusion holds, relation gets forwarded *)
    Some lls
  else
    (* inclusion does not hold, no relation to forward *)
    None
let gen_is_le
    (emp_both: bool)        (* whether both arguments should be fully emptied *)
    (stop: sv Aa_sets.t option) (* optional stop nodes *)
    (xl: lmem)              (* left input *)
    (nou0: SvSet.t)        (* no unfold 0 *)
    (es: SvSet.t)          (* segment end(s), if any *)
    ~(vsat_l: vsat)           (* satisfiability test function, left arg *)
    (xr: lmem)              (* right input *)
    (r: Graph_sig.node_emb) (* injection from right into left *)
    (c_r: Col_sig.colv_emb)  (* injection from right COLV into left COLV *)
    : le_state option =
  let ni = Graph_utils.Nemb.init r in
  let cvi =
    Aa_maps.fold
      (fun colv_r (colv_l, ck) acc ->
        CEmb.add colv_r colv_l ck acc
      ) c_r CEmb.empty in
  let rules = rules_init emp_both es ni xl xr r in
  let ils = { ls_cur_l     = xl ;
              ls_cur_r     = xr ;
              ls_cur_svi   = ni ;
              ls_cur_cvi   = cvi;
              ls_rules     = rules;
              ls_rem_l     = SvSet.empty ;
              ls_vsat_l    = vsat_l ;
              ls_ctr_r     = [ ] ;
              ls_setctr_r  = [ ] ;
              ls_seqctr_r  = [ ] ;
              ls_newsetv_r = SvSet.empty ;
              ls_newseqv_r = SvSet.empty ;
              ls_success   = false ;
              ls_emp_both  = emp_both;
              ls_stop_l    = stop;
              ls_excl_l    = SvMap.empty;
              ls_no_unf0   = nou0;
              ls_end_seg   = es; } in
  try
    if not !very_silent_mode then
      Log.force "start is_le";
    let ob = is_le_start ils in
    if not !very_silent_mode then
      Log.force "return is_le %b" (ob != None);
    ob
  with
  | Le_false s ->
      if not !very_silent_mode then
        Log.force "is_le fails on exception: %s" s;
      None

(** Utility to package the results *)
let isle_convert ls =
  { ilr_lmem_l    = ls.ls_cur_l;
    ilr_svrem_l   = ls.ls_rem_l;
    ilr_sv_rtol   = ls.ls_cur_svi.Graph_sig.n_img;
    ilr_colv_rtol = ls.ls_cur_cvi.n_img;
    ilr_setctr_r  = ls.ls_setctr_r;
    ilr_seqctr_r  = ls.ls_seqctr_r }

(** Full inclusion checking, used in the domain functor *)
let is_le
    (xl: lmem)              (* left input *)
    ~(vsat_l: vsat)         (* satisfiability test function, left arg *)
    (xr: lmem)              (* right input *)
    (r: Graph_sig.node_emb) (* injection from right into left *)
    (* injection from right COLV into left set var *)
    (c_r: Col_sig.colv_emb)
    : is_le_res =
  let res = gen_is_le true None xl SvSet.empty SvSet.empty ~vsat_l xr r c_r in
  match res with
  | None    -> `Ilr_not_le
  | Some ls ->
      if ls.ls_success then `Ilr_le (isle_convert ls, true)
      else `Ilr_not_le

(** Partial inclusion checking function, used for join *)
let is_le_weaken_check
    (xl: lmem)               (* left input *)
    (nou0: SvSet.t)          (* no unfold 0 *)
    (es: SvSet.t)            (* segment end(s), if any *)
    ~(vsat_l: vsat)          (* satisfiability test function, left arg *)
    (xr: lmem)               (* right input *)
    (r: Graph_sig.node_emb)  (* injection from right into left *)
    (cv_r: Col_sig.colv_emb) (* injection from right COLV into left COLV *)
    : List_sig.is_le_res =
  let res = gen_is_le false None xl nou0 es ~vsat_l xr r cv_r in
  match res with
  | None    -> `Ilr_not_le
  | Some ls -> `Ilr_le (isle_convert ls, false)

(** Partial inclusion checking functeion, used for unary abstract *)
let is_le_weaken_guess
    ~(stop: sv Aa_sets.t option) (* optional stop nodes *)
    (xl: lmem)              (* left input *)
    (nou0: SvSet.t)        (* no unfold 0 *)
    (es: SvSet.t)          (* segment end(s), if any *)
    ~(vsat_l: vsat)           (* satisfiability test function, left arg *)
    (xr: lmem)              (* right input *)
    (r: Graph_sig.node_emb) (* injection from right into left *)
    (cv_r: Col_sig.colv_emb) (* injection from right COLV into left COLV *)
    : List_sig.is_le_weaken_guess =
  let res = gen_is_le false stop xl nou0 es ~vsat_l xr r cv_r in
  match res with
  | None -> `Ilr_not_le
  | Some ls ->
      (* - if no stop node found, returns a full inductive
       * - if one stop node found, returns a segment
       * - if several stop nodes found, fails *)
      let ilres = isle_convert ls in
      SvMap.fold
        (fun i ld -> function
          | `Ilr_le_list _ -> `Ilr_le_lseg (ilres, i)
          | _ -> Log.fatal_exn "is_le_weaken: too many solutions"
        ) ls.ls_excl_l (`Ilr_le_list ilres)
