(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: array_ind_utils.mli
 **       data-types for the inductive definitions.
 ** Jiangchao Liu, 2016/03/01 *)
open Data_structures

open Array_ind_sig
open List_sig

(** Set of inductive definitions *)
val array_ind_defs: array_ind StringMap.t ref
val array_list_ind_defs: l_def StringMap.t ref

(** Pretty print  *)
val mform_fpr: form -> mform -> unit
val array_ind_fpr: form -> array_ind -> unit

(** Initialization from parsing *)
val array_ind_init: array_ind list -> unit

(** Find array list definitions from inductive definitions *)
val exp_search_array_lists: unit -> unit

val elaborate_array_ind:
  Spec_sig.Parsed_spec.type_context -> Ai_parsed_spec.array_ind
    -> array_ind option
