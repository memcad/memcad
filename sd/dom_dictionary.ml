(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_dictionary.ml
 **       Dictionary for composite shape abstract domains
 ** Antoine Toubhans, 2014/05/11 *)
open Data_structures
open Lib
open Sv_def

open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "d_dict__" and level = Log_level.DEBUG end)

(* Type for localisation of variables *)
type dic_side =
  | Dic_fst (* var. of the first shape component *)
  | Dic_snd (* var. of the second shape component *)

(* Pretty-printing *)
let dic_side_fpr fmt = function
  | Dic_fst -> F.fprintf fmt "D1"
  | Dic_snd -> F.fprintf fmt "D2"

(** Module signature for dictionary
 ** a dictionary consists of binding local symvar to either
 ** a symvar in D1 or a symvar in D2 *)
module type DIC =
  sig
    include INTROSPECT
    (** Type *)
    type t
    (* Empty environment *)
    val empty: t
    (* Pretty printing *)
    val t_fpri: string -> form -> t -> unit
    (* Sanity check *)
    val sanity_check: string -> t -> unit

    (** Dictionary managment *)
    (* add a fresh binding, takes an underlying symvar,
     * returns a local symvars *)
    val add_fresh: dic_side * sv -> t -> sv * t
    (* remove a binding, takes a local symvars *)
    val rem: sv -> t -> t

    (** Convert symbolic variables *)
    (* from LOC to FST + SND *)
    val local2under: t -> sv -> dic_side * sv
    (* from FST to SEP *)
    val fst2local: t -> sv -> sv
    (* from SND to SEP *)
    val snd2local: t -> sv -> sv

    (** Iterators *)
    val iter: (sv -> (dic_side * sv) -> unit) -> t -> unit
    val fold:
        (sv -> (dic_side * sv) -> 'a -> 'a) -> t -> 'a -> 'a
  end

(** A implementation based on hash functions *)
module Dic_hash =
  (struct
    let module_name = "dic_hash"
    let config_fpr fmt (): unit = () (* leaf module *)
    (* Hash functions *)
    let s_hash (i: sv): dic_side * sv =
      let i = sv_to_int i in
      match i mod 2 with
      | 0 -> Dic_fst, sv_unsafe_of_int (i/2)
      | _ -> Dic_snd, sv_unsafe_of_int ((i-1)/2)
    let s_hash1 (j1: sv): sv =
      sv_unsafe_of_int (2 * sv_to_int j1)
    let s_hash2 (j2: sv): sv =
      sv_unsafe_of_int (2 * sv_to_int j2 + 1)

    (** Types *)
    type t = SvSet.t

    (* Empty environment *)
    let empty: t = SvSet.empty

    (* Pretty printing *)
    let t_fpri (ind: string) (fmt: form) (t: t): unit =
      SvSet.iter
        (fun i ->
          let loc, iu = s_hash i in
          F.fprintf fmt "%s%a\t--->\t(%a, %a)\n"
            ind sv_fpr i dic_side_fpr loc sv_fpr iu
        ) t
    (* Sanity check *)
    let sanity_check (_: string) (_: t): unit = ()

    (** Environment managment *)
    (* add a fresh binding *)
    let add_fresh ((loc, i): dic_side * sv) (e: t): sv * t =
      match loc with
      | Dic_fst ->
          let ilocal = s_hash1 i in
          assert (not (SvSet.mem ilocal e));
          ilocal, SvSet.add ilocal e
      | Dic_snd ->
          let ilocal = s_hash2 i in
          assert (not (SvSet.mem ilocal e));
          ilocal, SvSet.add ilocal e
    (* remove a binding *)
    let rem: sv -> t -> t = SvSet.remove

    (** Convert symbolic variables *)
    (* from SEP to FST + SND *)
    let local2under (_: t): sv -> dic_side * sv = s_hash
    (* from FST to SEP *)
    let fst2local (_: t): sv -> sv = s_hash1
    (* from SND to SEP *)
    let snd2local (_: t): sv -> sv = s_hash2

    (** Iterators *)
    let iter (f: sv -> (dic_side * sv) -> unit): t -> unit =
      SvSet.iter (fun i -> f i (s_hash i))
    let fold (f: sv -> (dic_side * sv) -> 'a -> 'a):
        t -> 'a -> 'a =
      SvSet.fold (fun i -> f i (s_hash i))
  end: DIC)

(** A implementation based on maps *)
module Dic_map =
  (struct
    (** Types *)
    let module_name = "dic_map"
    let config_fpr fmt (): unit = () (* leaf module *)
    type t =
        { t_img:     (dic_side * sv) SvMap.t; (* image DSep -> D1 + D2 *)
          t_rev_fst: sv SvMap.t;              (* image D1 -> DSep *)
          t_rev_snd: sv SvMap.t;              (* image D2 -> DSep *)
          t_nkey:    SvGen.t                  (* key allocator *) }

    (* Empty environment *)
    let empty: t =
      { t_img     = SvMap.empty;
        t_rev_fst = SvMap.empty;
        t_rev_snd = SvMap.empty;
        t_nkey    = SvGen.empty }

    (* Pretty printing *)
    let t_fpri (ind: string) (fmt: form) (t: t): unit =
      SvMap.iter
        (fun i (loc, iu) ->
          F.fprintf fmt "%s%a\t--->\t(%a, %a)\n"
            ind sv_fpr i dic_side_fpr loc sv_fpr iu
        ) t.t_img
    (* Sanity check *)
    let sanity_check (mess: string) (x: t): unit =
      SvMap.iter
        (fun i (loc, j) ->
          match loc with
          | Dic_fst ->
              begin
                try
                  if SvMap.find j x.t_rev_fst != i then
                    Log.fatal_exn "%s: incorrect mapping %a -> %a, %a" mess
                      sv_fpr i dic_side_fpr loc sv_fpr j
                with Not_found ->
                  Log.fatal_exn "%s: mapping %a -> %a, %a has no reverse" mess
                    sv_fpr i dic_side_fpr loc sv_fpr j
              end
          | Dic_snd ->
              begin
                try
                  if SvMap.find j x.t_rev_snd != i then
                    Log.fatal_exn "%s: incorrect mapping %a -> %a, %a" mess
                      sv_fpr i dic_side_fpr loc sv_fpr j
                with Not_found ->
                  Log.fatal_exn "%s: mapping %a -> %a, %a has no reverse" mess
                    sv_fpr i dic_side_fpr loc sv_fpr j
              end
        ) x.t_img

    (** Environmemt managment *)
    (* add a fresh binding *)
    let add_fresh ((loc, i): dic_side * sv) (e: t): sv * t =
      match loc with
      | Dic_fst ->
          assert (not (SvMap.mem i e.t_rev_fst));
          let keys, ilocal = SvGen.gen_key e.t_nkey in
          let r = { e with
                    t_img     = SvMap.add ilocal (Dic_fst, i) e.t_img;
                    t_rev_fst = SvMap.add i ilocal e.t_rev_fst;
                    t_nkey    = keys } in
          ilocal, r
      | Dic_snd ->
          assert (not (SvMap.mem i e.t_rev_snd));
          let keys, ilocal = SvGen.gen_key e.t_nkey in
          let r = { e with
                    t_img     = SvMap.add ilocal (Dic_snd, i) e.t_img;
                    t_rev_snd = SvMap.add i ilocal e.t_rev_snd;
                    t_nkey    = keys } in
          ilocal, r
    (* remove a binding *)
    let rem (i: sv) (e: t): t =
      let rev1, rev2 =
        match SvMap.find i e.t_img with (* this must not fail *)
        | Dic_fst, i1 -> SvMap.remove i1 e.t_rev_fst, e.t_rev_snd
        | Dic_snd, i2 -> e.t_rev_fst, SvMap.remove i2 e.t_rev_snd in
      { t_img     = SvMap.remove i e.t_img;
        t_rev_fst = rev1;
        t_rev_snd = rev2;
        t_nkey    = SvGen.release_key e.t_nkey i }

    (** Convert symbolic variables *)
    (* from SEP to FST + SND *)
    let local2under (e: t) (i: sv): dic_side * sv = SvMap.find i e.t_img
    (* from FST to SEP *)
    let fst2local (e: t) (i1: sv): sv = SvMap.find i1 e.t_rev_fst
    (* from SND to SEP *)
    let snd2local (e: t) (i2: sv): sv = SvMap.find i2 e.t_rev_snd

    (** Iterators *)
    let iter (f: sv -> (dic_side * sv) -> unit) (x: t): unit =
      SvMap.iter f x.t_img
    let fold (f: sv -> (dic_side * sv) -> 'a -> 'a) (x: t): 'a -> 'a =
      SvMap.fold f x.t_img
  end: DIC)
