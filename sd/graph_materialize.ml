(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_materialize.ml
 **       unfolding of graphs, materialization
 ** Xavier Rival, 2011/08/06 *)
open Apron
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

open Graph_utils
open Ind_form_utils
open Ind_utils
open Nd_utils
open Seq_utils
open Set_utils
open Sv_utils
open Vd_utils

open Ind_inst
open FInd


(** Error report *)
module Log =
  Logger.Make(struct let section = "g_mat___" and level = Log_level.DEBUG end)


(** Utilities *)
let map_op = function
  | Af_equal (_, _) -> Tcons1.EQ
  | Af_noteq (_, _) -> Tcons1.DISEQ
  | Af_sup (_, _) -> Tcons1.SUP
  | Af_supeq (_, _) -> Tcons1.SUPEQ

(** Materialize empty segment *)
let materialize_empty_segment (nbase: sv) (g: graph): unfold_res list =
  let seg = seg_edge_find nbase g in
  let _, eqs, setcons, seqv_empty = red_empty_segment nbase g in
  hseg_sanity_check ~isok:8 "graph_materialize,unfold,bwd" seg;
  let seqcons =
    SvSet.fold (fun seqv acc -> Seq_equal (Seq_var seqv, Seq_empty) :: acc)
      seqv_empty [] in
  let remcolvs = col_kinds_of_seg_edge_acc seg SvMap.empty in
  [ { ur_g        = seg_edge_rem nbase g;
      ur_cons     = [ ];
      ur_setcons  = setcons;
      ur_seqcons  = seqcons;
      ur_newcolvs = SvMap.empty;
      ur_remcolvs = remcolvs;
      ur_paths    = [ ];
      ur_eqs      = eqs;
      ur_seqs     = SvPrSet.empty;
      ur_news     = SvSet.empty } ]

(** Materialization of one rule *)
(* returns:
 * - unfolded graph
 * - additional constraints
 * - set of new nodes *)
let materialize_rule
    ~(submem: bool) (* whether sub-memory is_le (no alloc check) *)
    (nbase: sv)
    (ind: ind)
    (iargs: ind_args)
    (ondest: (sv * ind_args_seg * ind_args) option)
    (* dest+args (segment case) *)
    (len1: bool) (* whether segment of length 1 (segment case) *)
    (ir: irule)
    (t: graph)
    : (graph * SvPrSet.t * SvPrSet.t
         * (qid * seq_expr_mrk) list * col_kinds) list
    * n_cons list
    * set_cons list
    * seq_cons_mrk list
    * seq_cons list
    * n_path list * SvSet.t * (col_kind * colv_info option) SvMap.t =
  if !Flags.flag_dbg_materialize then
    Log.info "Materialize rule to %s\n%a\nGraph[%a]:\n%a"
      (match ondest with None -> "full inductive" | Some _ -> "segment")
      (irule_fpri "  ") ir sv_fpr nbase (graph_fpri "  ") t;
  (* create new nodes *)
  let rec aux_gen_nodes (i: int) (t: graph) =
    if i < 0 then t, SvMap.empty, SvSet.empty
    else
      begin
        let tacc, macc, sacc = aux_gen_nodes (i - 1) t in
        let v = sv_unsafe_of_int i in
        let typ =
          try SvMap.find v ir.ir_typ
          with Not_found -> Log.fatal_exn "missing new SV info in inductive" in
        let n, tn = sv_add_fresh typ Nnone tacc in
        tn, SvMap.add v n macc, SvSet.add n sacc
      end in
  let t0, form_to_act, nnodes = aux_gen_nodes (ir.ir_num-1) t in
  let t0, form_to_act, nnodes =
    SvSet.fold
      (fun i (t, macc, sacc) ->
        let sv, t = sv_add_fresh Ntint Nnone t in
        t, SvMap.add i sv macc, SvSet.add sv sacc
      ) ir.ir_nnum (t0, form_to_act, nnodes) in
  let t0, sform_to_act, colnodes =
    SvSet.fold
      (fun i (t, macc, colvacc) ->
        let colv, t = colv_add_fresh (Ct_set None) t in
        let info = find_info colv t in
        t, SvMap.add i colv macc, SvMap.add colv (CK_set, info) colvacc
      ) ir.ir_snum (t0, SvMap.empty, SvMap.empty) in
  let t0, qform_to_act, colnodes =
    SvSet.fold
      (fun i (t, macc, colvacc) ->
        let colv, t = colv_add_fresh (Ct_seq None) t in
        let info = find_info colv t in
        t, SvMap.add i colv macc, SvMap.add colv (CK_seq, info) colvacc
      ) ir.ir_qnum (t0, SvMap.empty, colnodes) in
  let t0, lq, lq2, lq3, ldd, colnodes =
    (* for segment unfolding *)
    match ondest with
    | Some (nd, arge, argd) ->
        List.fold_left
          (fun (gacc, lacc, pacc, eacc, vacc, colvacc) (x,y) ->
            let colv1, gacc = colv_add_fresh (Ct_seq None) gacc in
            let info1 = find_info colv1 gacc in
            if len1 then
              gacc, colv1 :: lacc, Seq_mrk_mrk ( x, y) :: pacc,
              Seq_equal (Seq_var colv1, Seq_Concat [Seq_var x;Seq_var y])::eacc,
              vacc,
              colvacc |> SvMap.add colv1 (CK_seq, info1)
            else
              let colv2, gacc = colv_add_fresh (Ct_seq None) gacc in
              let info2 = find_info colv2 gacc in
              let colv3, gacc = colv_add_fresh (Ct_seq None) gacc in
              let info3 = find_info colv3 gacc in
              gacc, colv1 :: lacc, Seq_mrk_mrk ( x, y) :: pacc,
              Seq_equal (Seq_var colv1, Seq_Concat [Seq_var x;Seq_var y])::eacc,
              SvPrMap.add (x,y) (colv2, colv3) vacc,
              colvacc
              |> SvMap.add colv1 (CK_seq, info1)
              |> SvMap.add colv2 (CK_seq, info2)
              |> SvMap.add colv3 (CK_seq, info3)
        ) (t0, [], [], [], SvPrMap.empty, colnodes) arge.ias_seq
    | _ -> (t0, [], [], [], SvPrMap.empty, colnodes) in
  let lq, lq2 = List.rev lq, List.rev lq2 in
  (* mark alloc node, if necessary *)
  let t0 =
    List.fold_left
      (fun acc -> function
        | Pf_alloc sz -> mark_alloc nbase sz acc
        | _ -> acc
      ) t0 ir.ir_pure in
  if !Flags.flag_dbg_materialize then
    Log.info "Expanded graph:\n%a" (graph_fpri "  ") t0;
  if !Flags.flag_dbg_materialize then
    begin
      Log.info "sform_to_act(Setvars: %a)::\n" (SvSet.t_fpr ";") ir.ir_snum;
      SvMap.iter
        (fun i sr ->
          Log.info "%a->%a\n" sv_fpr i sv_fpr sr
        ) sform_to_act
    end;
  (* functions that return a node *)
  let fetch_ptr_par (i: int): sv =
    try List.nth iargs.ia_ptr i
    with Failure _ -> Log.fatal_exn "materialize: ptr par out of range (mr)" in
  let fetch_int_par (i: int): sv =
    try List.nth iargs.ia_int i
    with Failure _ -> Log.fatal_exn "materialize: int par out of range (mr)" in
  let fetch_set_par (i: int): sv =
    let fail () = Log.fatal_exn "materialize: set par out of range (mr)" in
    try List.nth iargs.ia_set i
    with Failure _ ->
      match ondest with
      | None -> fail ()
      | Some (_, eargs, _) ->
          try List.nth eargs.ias_set i
          with Not_found -> fail () in
  let fetch_seq_par (i: int): sv =
    let fail () = Log.fatal_exn "materialize: seq par out of range (mr)" in
    if len1 then
      try List.nth lq i with Not_found -> fail ()
    else
      try List.nth iargs.ia_seq i
      with Failure _ ->
        match ondest with
        | None -> fail ()
        | Some (_, eargs, _) ->
            try List.nth lq i
            with Not_found -> fail () in
  (* associate a formal node to its real counterpart *)
  let map_node (i: sv): sv =
    try SvMap.find i form_to_act
    with Not_found ->
      Log.fatal_exn "rule formal %a not found in %a" sv_fpr i
        (SvMap.t_fpr "; " sv_fpr) form_to_act in
  let map_formal_arg: formal_arg -> sv = function
    | Fa_this      -> nbase
    | Fa_var_new i -> map_node (sv_unsafe_of_int i)
    | Fa_par_int i -> fetch_int_par i
    | Fa_par_ptr i -> fetch_ptr_par i
    | Fa_par_set i -> fetch_set_par i
    | Fa_par_seq i -> fetch_seq_par i in
  let map_formal_main_arg (fa: formal_main_arg): sv =
    map_formal_arg (formal_arg_of_main fa) in
  let map_formal_ptr_arg (fa: formal_ptr_arg): sv =
    map_formal_arg (formal_arg_of_ptr fa) in
  let map_formal_int_arg (fa: formal_int_arg): sv =
    map_formal_arg (formal_arg_of_int fa) in
  let map_formal_set_arg (fa: formal_set_arg): sv =
    match fa with
    | Fa_var_new i ->
        begin
          try SvMap.find (sv_unsafe_of_int i) sform_to_act
          with Not_found -> Log.fatal_exn "rule formal set %d not found" i
        end
    | Fa_par_set i -> fetch_set_par i in
  let map_formal_seq_arg (fa: formal_seq_arg): sv =
    match fa with
    | Fa_var_new i ->
        begin
          try SvMap.find (sv_unsafe_of_int i) qform_to_act
          with Not_found -> Log.fatal_exn "rule formal seq %d not found" i
        end
    | Fa_par_seq i -> fetch_seq_par i in
  let map_formal_arith_arg (fa: formal_arith_arg): sv =
    map_formal_arg (formal_arg_of_arith fa) in
  (* mapping of a set of arguments to a recursive call to an inductive
   * (might be a full inductive edge or a segment) *)
  let map_ind_args (i: indcall): ind_args =
    (* generation of the lists of arguments *)
    { ia_ptr = List.map map_formal_ptr_arg i.ii_argp;
      ia_int = List.map map_formal_int_arg i.ii_argi;
      ia_set = List.map map_formal_set_arg i.ii_args;
      ia_seq = List.map map_formal_seq_arg i.ii_argq } in
  (* create new edges *)
  let t1 =
    (* for code clarity, the ast of inductives might be better off separating
     * those two elements *)
    let l_cells, l_inds, l_segs =
      List.fold_left
        (fun (ac, ai, ag) -> function
          | Hacell c -> c :: ac, ai, ag
          | Haind  i -> ac, i :: ai, ag
          | Haseg (ic0, ic1) ->
              Ind_utils.assert_ind_no_par "unfold,seg in ind" ind;
              ac, ai, (ic0,ic1) :: ag
        ) ([ ], [ ], [ ]) ir.ir_heap in
    (* materialize all points-to edges once and for all *)
    let t_pt =
      List.fold_left
        (fun tacc c ->
          assert (c.ic_size > 0);
          let pte = { pe_size = Offs.size_of_int c.ic_size ;
                      pe_dest = map_formal_arith_arg c.ic_dest, c.ic_doff } in
          pt_edge_block_append (nbase, Bounds.of_offs c.ic_off) pte tacc
        ) t0 (List.rev l_cells) in
    (* add the segments *)
    let t_pt, keepcols =
      List.fold_left
        (fun (t, keepcols) (ic0, ic1) ->
          assert (ic0.ii_ind = ic1.ii_ind);
          let ind_seg = Ind_utils.ind_find ic0.ii_ind in
          let args1 = map_ind_args ic0 in
          let args2 = map_ind_args ic1 in
          let se =
            let ias_seq =
              List.map2 (fun x y -> x, y) args1.ia_seq args2.ia_seq in
            { se_ind   = ind_seg ;
              se_eargs = { ind_args_s_empty with
                           ias_set = args1.ia_set ;
                           ias_seq = ias_seq } ;
              se_sargs = { ind_args_empty with
                           ia_int = args1.ia_int ;
                           ia_ptr = args1.ia_ptr } ;
              se_dargs = { args2 with ia_seq = [] } ;
              se_dnode = map_formal_main_arg ic1.ii_maina } in
          hseg_sanity_check ~isok:1 "graph_materialize,make,1" se;
          let keepcols = col_kinds_of_seg_edge_acc se keepcols in
          seg_edge_add (map_formal_main_arg ic0.ii_maina) se t, keepcols
        ) (t_pt, SvMap.empty) l_segs in
    (* proceed with the addition of inductives *)
    match ondest with
    | None ->
        let t_u, keepcols =
          List.fold_left
            (fun (tacc, keepcols) i ->
              (* creation of an inductive edge, with the right nodes
               * in the list of arguments *)
              let ie = { ie_ind  = Ind_utils.ind_find i.ii_ind ;
                         ie_args = map_ind_args i } in
              let keepcols = col_kinds_of_ind_edge_acc ie keepcols in
              (* addition of the edge to the graph *)
              ind_edge_add (map_formal_main_arg i.ii_maina) ie tacc, keepcols
            ) (t_pt, keepcols) l_inds in
        [ t_u, SvPrSet.empty, SvPrSet.empty, [], keepcols ]
    | Some (nd, arge, argd) ->
        (* two lists of graphs: 1: with a 0-segment; 2: with a 1-segment *)
        let _, l1 =
          List.fold_left
            (fun (al0, al1) (i: indcall) ->
              let mapped_base = map_formal_main_arg i.ii_maina in
              (* need to do something with arge *)
              let mapped_args = map_ind_args i in
              let f_ind ((g, keepcols): graph * col_kinds): graph * col_kinds =
                let ie = { ie_ind  = Ind_utils.ind_find i.ii_ind;
                           ie_args = mapped_args } in
                let keepcols = col_kinds_of_ind_edge_acc ie keepcols in
                ind_edge_add mapped_base ie g, keepcols in
              let f_seg ((gg, keepcols): graph * col_kinds)
                  : graph * SvPrSet.t * SvPrSet.t
                  * (qid * seq_expr_mrk) list * col_kinds  =
                (* TODO:
                 * we are dividing the arguments of the segment in two;
                 * maybe make it a function reusable everywhere *)
                let (qeqs, iasseq) =
                  if len1 then
                    List.map
                      (fun c -> (c, Seq_mrk_mrk_empty)) mapped_args.ia_seq,
                    []
                  else
                    List.fold_left2
                      (fun (acc1, acc2) (x, y) c ->
                        let s1, s2 = SvPrMap.find (x, y) ldd in
                        (c, Seq_mrk_mrk(s1, s2)) :: acc1, (s1, s2) :: acc2)
                      ([], []) arge.ias_seq mapped_args.ia_seq in
                let seg = { se_ind   = Ind_utils.ind_find i.ii_ind ;
                            se_eargs = { ind_args_s_empty with
                                         ias_set = mapped_args.ia_set ;
                                         ias_seq = List.rev iasseq } ;
                            se_sargs = { ind_args_empty with
                                         ia_int = mapped_args.ia_int ;
                                         ia_ptr = mapped_args.ia_ptr } ;
                            se_dargs = argd;
                            se_dnode = nd } in
                let keepcols = col_kinds_of_seg_edge_acc seg keepcols in
                hseg_sanity_check ~isok:5 "graph_materialize,make,2" seg;
                let gg, eqs =
                  if len1 then gg, empty_segment_equalities mapped_base seg
                  else seg_edge_add mapped_base seg gg, SvPrSet.empty in
                gg, eqs, SvPrSet.empty, List.rev qeqs, keepcols in
              let l_up_ind =
                List.map
                  (fun (g, s0, s1, s2, k) ->
                    let g, k = f_ind (g, k) in
                    g, s0, s1, s2, k
                  ) al1 in
              if i.ii_ind = ind.i_name then
                List.map f_ind al0, (List.map f_seg al0) @ l_up_ind
              else
                List.map f_ind al0, l_up_ind
            ) ([ t_pt, keepcols ], [ ]) l_inds in
        l1 in
  (* compute predicates *)
  let rec map_aexpr: aexpr -> n_expr = function
    | Ae_cst i ->
        Ne_csti i
    | Ae_colvinfo (ik, x) ->
        let seqv = map_formal_seq_arg x in
        let { min; max; size } = SvMap.find seqv t0.g_colvinfo in
        let sv =
          match ik with
          | SIZE -> size
          | MIN -> min
          | MAX -> max in
        Ne_var sv
    | Ae_var fa ->
        Ne_var (map_formal_arith_arg fa)
    | Ae_plus (e0, e1) ->
        Ne_bin (Texpr1.Add, map_aexpr e0, map_aexpr e1) in
  let map_aform: aform -> n_cons = function
    | (Af_equal (e0, e1) | Af_noteq (e0, e1))
    | (Af_sup (e0, e1) | Af_supeq (e0, e1))  as f ->
        let te0 = map_aexpr e0 and te1 = map_aexpr e1 in
        Nc_cons (map_op f, te0, te1) in
  let map_sexpr: sexpr -> set_expr = function
    | Se_var s -> S_var (map_formal_set_arg s)
    | Se_uplus (ss, a) ->
        let n = S_node (map_formal_arith_arg a) in
        List.fold_left
          (fun acc ele ->
            S_uplus (S_var (map_formal_set_arg ele), acc)
          ) n ss
    | Se_union (ss, a) ->
        let n = S_node (map_formal_arith_arg a) in
        List.fold_left
          (fun acc ele ->
            S_union (S_var (map_formal_set_arg ele), acc)
          ) n ss in
  let map_sform: sform -> set_cons = function
    | Sf_mem (a, s) ->
        S_mem ((map_formal_arith_arg a), S_var (map_formal_set_arg s) )
    | Sf_equal (s, se) ->
        S_eq (S_var (map_formal_set_arg s), map_sexpr se)
    | Sf_empty s -> S_eq (S_var (map_formal_set_arg s), S_empty) in
  let rec map_qexpr: qexpr -> seq_expr = function
    | Qe_empty -> Seq_empty
    | Qe_var v -> Seq_var (map_formal_seq_arg v)
    | Qe_val v -> Seq_val (map_formal_arith_arg v)
    | Qe_concat l -> Seq_utils.normalise (Seq_Concat (List.map map_qexpr l))
    | Qe_sort s -> Seq_sort (map_qexpr s) in
  let map_qform: qform -> seq_cons = function
    | Qf_equal (v, x) ->
        Log.info "map_qform %a = %a " sv_fpr (map_formal_seq_arg v)
          Seq_utils.seq_expr_fpr (map_qexpr x);
        Seq_equal (Seq_var (map_formal_seq_arg v), map_qexpr x) in
  let map_path (s, p, d) = map_formal_ptr_arg s, p, map_formal_ptr_arg d in
  (* handling predicates different from allocation *)
  let p_num, p_path, p_set, p_seq, p_seq_sort =
    List.fold_left
      (fun (accp, accpt, accps, accpq, accpqs) -> function
        | Pf_alloc sz -> accp, accpt, accps, accpq, accpqs
        | Pf_arith f -> (map_aform f :: accp), accpt, accps, accpq, accpqs
        | Pf_set sf -> accp, accpt, (map_sform sf :: accps), accpq, accpqs
        | Pf_seq (Qf_equal (v1, Qe_sort (Qe_var v2))) ->
            accp, accpt, accps, accpq,
            Seq_equal (Seq_var (map_formal_seq_arg v1),
                       Seq_sort (Seq_var (map_formal_seq_arg v2))) :: accpqs
        | Pf_seq qf -> accp, accpt, accps, (map_qform qf :: accpq), accpqs
        | Pf_path f -> accp, (map_path f)::accpt, accps, accpq, accpqs
      ) ([], [], [], [], []) ir.ir_pure in
  (* final result *)
  let p_seq =
    List.map (fun seqcons -> Seq_utils.subst_seqcons_list seqcons lq lq2) p_seq
    @ (List.map Seq_utils.seq_cons_to_seq_cons_mrk lq3) in
  t1, p_num, p_set, p_seq, p_seq_sort, p_path, nnodes, colnodes


(** Materialization of an inductive *)
let materialize_ind
    ~(submem: bool) (* whether sub-memory is_le (no alloc check) *)
    (hint_empty: bool option) (* whether to put empty rules first, last or not*)
    (es: bool) (* whether to generate empty segment case (for segment unfold) *)
    (len1: bool) (* whether segment is of length 1 (for segment unfold) *)
    (nbase: sv) (* source: node where unfolding should take place *)
    (t: graph)   (* input graph *)
    : unfold_res list =
  let loc_debug = false in
  let indedge, odest, t0 = ind_or_seg_edge_find_rem nbase t in
  let indname = indedge.ie_ind.i_name in
  let ind = Ind_utils.ind_find indname in
  (* we take hint (in is_le) to order the rules in a "more likely to succeed"
   * order (this is a trick to avoid backtracking, by trying to predict which
   * rule will give a successful comparison) *)
  let rules =
    let rules =
      match odest with
      | None -> ind.i_rules (* full inductive; consider all rules *)
      | Some _ -> ind.i_srules (* segment: only consider segment rules *) in
    match hint_empty with
    | None -> rules
    | Some b ->
        let emp, non_emp =
          List.fold_left
            (fun (ae, ane) ir ->
              if ir.ir_kind = Ik_empz then (* rule has empty heap *)
                ir :: ae, ane
              else (* rule has non empty heap *)
                ae, ir :: ane
            ) ([ ], [ ]) rules in
        (* put empty first if not b (the fold left will put them at the
         * beginning again) *)
        match b, Flags.flag_unfold_sel with
        |  true,  true -> emp
        |  true, false -> non_emp @ emp
        | false,  true -> non_emp
        | false, false -> emp @ non_emp in
  (* case of the empty segment, if we are unfolding a segment and es is true *)
  let emp_seg_case =
    if es then
      match odest with
      | None ->
          (* the edge is a full inductive; no empty segment case *)
          [ ]
      | Some (ndst, aedg, adst) ->
          (* the edge is a segment; generation of the unfolded empty segment *)
          materialize_empty_segment nbase t
    else [ ] in
  (* reconstruction of "virtual" indargs to use for the addition of equalities
   * and deletion of irrelevant COLVs *)
  let indargs =
    match odest with
    | None -> indedge.ie_args
    | Some (_, eargs, _) ->
        let ia_seq = List.map_flatten (fun (x,y) -> [x; y]) eargs.ias_seq in
        { indedge.ie_args with
          ia_set = eargs.ias_set ;
          ia_seq = ia_seq } in
  (* materialization of all the other rules *)
  List.fold_left
    (fun acc ir ->
      if loc_debug then
        Log.info "materialize_ind, rule:\n%a" (irule_fpri "  ") ir;
      let lg, cl, cs, cq, cqs, cp, is, ss =
        materialize_rule ~submem:submem nbase ind indedge.ie_args
          odest len1 ir t0 in
      List.fold_left
        (fun acc (g, eqs, seqs, qeqs, keepcols) ->
          let setcons =
            SvPrSet.fold (fun (l, r) acc -> S_eq (S_var l, S_var r) :: acc)
              seqs cs in
          let seqcons_mrk =
            List.map
              (fun s ->
                let l1, l2 = List.split qeqs in
                Seq_utils.subst_seqcons_list_aux s l1 l2
              ) cq in
          let seqcons = Seq_utils.split_list seqcons_mrk @ cqs in
          let remcolvs = col_kinds_of_ind_args_acc indargs SvMap.empty in
          let remcolvs = col_kinds_diff remcolvs keepcols in
          { ur_g        = g;
            ur_cons     = cl;
            ur_setcons  = setcons;
            ur_seqcons  = seqcons;
            ur_newcolvs = ss;
            ur_remcolvs = remcolvs;
            ur_paths    = cp;
            ur_eqs      = eqs;
            ur_seqs     = SvPrSet.empty;
            ur_news     = is } :: acc
        ) acc lg
    ) emp_seg_case rules


(** Higher level unfold function *)
let unfold
    ~(submem: bool) (* whether sub-memory is_le (no alloc check) *)
    (nbase: sv)     (* source: node where unfolding should take place *)
    (d: unfold_dir) (* direction, for segment edges only *)
    (g: graph)      (* input *)
    : unfold_res list =
  if !Flags.flag_dbg_materialize then
    Log.info "Materialization at: %a" nsv_fpr nbase;
  match d with
  | Udir_fwd -> materialize_ind ~submem:submem None true false nbase g
  | Udir_bwd ->
      (* case of the empty segment *)
      let emp_seg_case = materialize_empty_segment nbase g in
      (* case of the splitting of the segment into two segments *)
      let seg = seg_edge_find nbase g in
      let remcolvs = col_kinds_of_seg_edge_acc seg SvMap.empty in
      let g, nmid, seq_cons, colv = seg_split nbase g in
      if !Flags.flag_dbg_bwd_unfold then
        Log.info "Bwd-unfold, after splitting:\n%a" (graph_fpri "  ") g;
      let non_emp_seg_cases =
        materialize_ind ~submem:submem None false true nmid g
        |> List.map
            (* move the contraints on sequences of the form S_i=[] to the end
             * of ur_seqcons, it is better for seq_dom.ml *)
            (fun unfold_res ->
              let ur_seqcons =
                let l, le =
                  List.fold_left
                    (fun (acc1, acc2) sq ->
                      match sq with
                      | Seq_equal(_, Seq_empty) -> acc1, sq :: acc2
                      | _ -> sq :: acc1, acc2) ([],[])
                    (seq_cons @ unfold_res.ur_seqcons) in
                l @ le in
              { unfold_res with ur_seqcons }
            )
        |> List.map
            (fun unfold_res ->
              let ur_newcolvs =
                SvMap.union (fun _ a b -> Some a) unfold_res.ur_newcolvs colv in
              let ur_remcolvs =
                SvMap.union (fun _ a b -> Some a)
                  unfold_res.ur_remcolvs remcolvs in
              { unfold_res with ur_newcolvs; ur_remcolvs }
            ) in
      (* Putting it all together *)
      emp_seg_case @ non_emp_seg_cases

(* This function only unfold the value constraints of
 * an inductive edge without unfolding the shape. It first finds
 * a rule whose guard condition is satisfied by 'c', then, unfolds
 * all the constraints of this rule. *)
let unfold_num
    (n: sv)    (* source: node where unfolding should take place *)
    (c: n_cons) (* guard *)
    (g: graph)  (* input *)
    : n_cons list * set_cons list * seq_cons list =
  try
    match (node_find n g).n_e with
    | Hind ind_edge ->
        let iargs = ind_edge.ie_args in
        let form_to_act, sform_to_act, qform_to_act =
          SvMap.empty, SvMap.empty, SvMap.empty in
        (* functions that return a node *)
        let fetch_ptr_par (i: int): sv =
          try List.nth iargs.ia_ptr i
          with Failure _ -> Log.fatal_exn "materialize: ptr par out of range" in
        let fetch_int_par (i: int): sv =
          try List.nth iargs.ia_int i
          with Failure _ -> Log.fatal_exn "materialize: int par out of range" in
        let fetch_set_par (i: int): sv =
          try List.nth iargs.ia_set i
          with Failure _ -> Log.fatal_exn "materialize: set par out of range" in
        let fetch_seq_par (i: int): sv =
          try List.nth iargs.ia_seq i
          with Failure _ -> Log.fatal_exn "materialize: seq par out of range" in
        (* associate a formal node to its real counterpart *)
        let map_node (i: sv) (form_to_act): sv =
          try SvMap.find i form_to_act
          with Not_found ->
            Log.fatal_exn "rule formal %a not found" sv_fpr i in
        let map_formal_arg: formal_arg -> sv = function
          | Fa_this      -> n
          | Fa_var_new i -> map_node (sv_unsafe_of_int i) form_to_act
          | Fa_par_int i -> fetch_int_par i
          | Fa_par_ptr i -> fetch_ptr_par i
          | Fa_par_set i -> fetch_set_par i
          | Fa_par_seq i -> fetch_seq_par i in
        let map_formal_set_arg (fa: formal_set_arg) : sv =
          match fa with
          | Fa_var_new i -> map_node (sv_unsafe_of_int i) sform_to_act
          | Fa_par_set i -> fetch_set_par i in
        let map_formal_seq_arg (fa: formal_seq_arg) : sv =
          match fa with
          | Fa_var_new i -> map_node (sv_unsafe_of_int i) qform_to_act
          | Fa_par_seq i -> fetch_seq_par i in
        let map_formal_arith_arg (fa: formal_arith_arg): sv =
          map_formal_arg (formal_arg_of_arith fa) in
        (* compute predicates *)
        let rec map_aexpr (ae: aexpr) : n_expr =
          match ae with
          | Ae_cst i ->
              Ne_csti i
          | Ae_colvinfo (ik, x) ->
              let seqv = map_formal_seq_arg x in
              let { min; max; size } = SvMap.find seqv g.g_colvinfo in
              let sv =
                match ik with
                | SIZE -> size
                | MIN -> min
                | MAX -> max in
              Ne_var sv
          | Ae_var fa ->
              Ne_var (map_formal_arith_arg fa)
          | Ae_plus (e0, e1) ->
              Ne_bin (Texpr1.Add, map_aexpr e0, map_aexpr e1) in
        let map_aform: aform -> n_cons = function
          | (Af_equal (e0, e1) | Af_noteq (e0, e1))
          | (Af_sup (e0, e1) | Af_supeq (e0, e1)) as f ->
              let te0 = map_aexpr e0 and te1 = map_aexpr e1 in
              Nc_cons (map_op f, te0, te1) in
        let map_sexpr: sexpr -> set_expr = function
          | Se_var s -> S_var (map_formal_set_arg s)
          | Se_uplus (ss, a) ->
              let n = S_node (map_formal_arith_arg a) in
              List.fold_left
                (fun acc ele ->
                  S_uplus (S_var (map_formal_set_arg ele), acc)
                ) n ss
          | Se_union (ss, a) ->
              let n = S_node (map_formal_arith_arg a) in
              List.fold_left
                (fun acc ele ->
                  S_union (S_var (map_formal_set_arg ele), acc)
                ) n ss in
        let map_sform: sform -> set_cons = function
          | Sf_mem (a, s) -> S_mem (map_formal_arith_arg a,
                                    S_var (map_formal_set_arg s) )
          | Sf_equal (s, se) ->
              S_eq (S_var (map_formal_set_arg s), map_sexpr se)
          | Sf_empty s -> S_eq (S_var (map_formal_set_arg s), S_empty) in
        let rec map_qexpr: qexpr -> seq_expr = function
          | Qe_empty -> Seq_empty
          | Qe_concat qel -> Seq_Concat (List.map map_qexpr qel)
          | Qe_val a -> Seq_val (map_formal_arith_arg a)
          | Qe_var a -> Seq_var (map_formal_seq_arg a)
          | Qe_sort qe -> Seq_sort (map_qexpr qe)
          in
        let map_qform: qform -> seq_cons = function
          | Qf_equal (qa, qe) ->
              Seq_equal (Seq_var (map_formal_seq_arg qa), map_qexpr qe) in
        let a, al, sl, ql =
          List.find (fun (a, al, sl, ql) -> map_aform a = c)
            ind_edge.ie_ind.i_rules_cons in
        if !Flags.flag_dbg_materialize && (al != [] || sl != []) then
          Log.info
            "unfold-num %s: %d, %d\n- cond: %a\n- suppl:   %a   %a   %a\n"
            ind_edge.ie_ind.i_name
            (List.length al) (List.length sl) Nd_utils.n_cons_fpr c
            (gen_list_fpr "" aform_fpr "\n") al
            (gen_list_fpr "" sform_fpr "\n") sl
            (gen_list_fpr "" qform_fpr "\n") ql;
        List.map map_aform al, List.map map_sform sl, List.map map_qform ql
    | _ -> [], [], []
  with Not_found -> [], [], []


(** Splitting parameters for non-local unfolding *)
let set_par_type_empty = { st_const = false ;
                           st_add   = false ;
                           st_head  = false ;
                           st_mono  = false }
(* TODO: most likely, we can share quite a bit of code *)
(* - usv:  SV corresponding to the node to be found
 * - unfi: rank of the set argument used for the unfolding *)
let nlu_split_ind_pars (mem: graph) (usv: sv) (unfi: int) (ie: ind_edge)
    : graph * ind_args_seg * ind_args * set_cons list * seq_cons list
    * col_kinds * col_kinds =
  let loc_debug = false in
  (* - all parameters should be set for now *)
  let ind = ie.ie_ind in
  assert (ind.i_ppars = 0 && ind.i_ipars = 0);
  (* - for set parameters: iterate, create new, add const/add eqs *)
  let mem, lseg_a, lrem_a, sctrs, colvs_add, colvs_rem, _ =
    List.fold_left
      (fun (mem, lseg_a, lrem_a, cons, colvs_add, colvs_rem, i) a ->
        let k = IntMap.find i ind.i_pkind.pr_set in
        let seg_a, mem = colv_add_fresh (Ct_set (Some k)) mem in
        let rem_a, mem = colv_add_fresh (Ct_set (Some k)) mem in
        if loc_debug then
          Log.info "nlu_split_ind,set-param @%d[%a]:  [%a]-(%a)" i
            set_par_type_fpr k setv_fpr seg_a setv_fpr rem_a;
        let c =
          if k.st_const then
            [ S_eq (S_var seg_a, S_var a); S_eq (S_var rem_a, S_var a) ]
          else if k.st_add || k.st_head then
            [ S_eq (S_var a, S_uplus (S_var seg_a, S_var rem_a)) ]
          else Log.todo_exn "nlu_split_ind_pars: unhandled kind" in
        let c =
          if i = unfi then
            (* TODO: check that the parameter is head *)
            (S_mem (usv, S_var rem_a)) :: c
          else c in
        let lseg_a = seg_a :: lseg_a in
        let lrem_a = rem_a :: lrem_a in
        let cons = c @ cons in
        let colvs_rem = SvMap.add a CK_set colvs_rem in
        let colvs_add =
          SvMap.add seg_a CK_set (SvMap.add rem_a CK_set colvs_add) in
        (mem, lseg_a, lrem_a, cons, colvs_add, colvs_rem, i + 1)
      )
      (mem, [ ], [ ], [ ], SvMap.empty, SvMap.empty, 0)
      ie.ie_args.ia_set in
  let mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, _ =
    List.fold_left
      (fun (mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, i) a ->
        let k = IntMap.find i ind.i_pkind.pr_seq in
        let seg_a_l, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        let seg_a_r, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        let rem_a  , mem = colv_add_fresh (Ct_seq (Some k)) mem in
        if loc_debug then
          Log.info "nlu,split_ind,seq-param @%d[%a]:   [%a-%a]-(%a): ?" i
            seq_par_type_fpr k seqv_fpr seg_a_l seqv_fpr seg_a_r seqv_fpr rem_a;
        let qctrs =
          if k.sq_add then
            let e =
              Seq_Concat [ Seq_var seg_a_l ;
                           Seq_Concat [ Seq_var rem_a ; Seq_var seg_a_r ] ] in
            let c = Seq_equal (Seq_var a, e) in
            c :: qctrs
          else qctrs in
        let qlseg = (seg_a_l, seg_a_r) :: qlseg
        and qlrem = rem_a :: qlrem in
        let colvs_rem = SvMap.add a CK_seq colvs_rem in
        let colvs_add =
          List.fold_left (fun acc cv -> SvMap.add cv CK_seq acc) colvs_add
            [ seg_a_l; seg_a_r; rem_a ] in
        mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, i + 1
      )
      (mem, [ ], [ ], [ ], colvs_add, colvs_rem, 0) ie.ie_args.ia_seq in
  let sa = { ias_ptr = [ ];
             ias_int = [ ];
             ias_set = List.rev lseg_a;
             ias_seq = List.rev qlseg }
  and ia = { ia_ptr  = [ ];
             ia_int  = [ ];
             ia_set  = List.rev lrem_a;
             ia_seq  = List.rev qlrem } in
  if loc_debug then
    begin
      List.iter (fun c -> Log.info "nlu,set-cstr: %a" set_cons_fpr c) sctrs;
      List.iter (fun c -> Log.info "nlu,seq-cstr: %a" seq_cons_fpr c) qctrs;
      Log.info "nlu,Add: %a" col_kinds_fpr colvs_add;
      Log.info "nlu,Rem: %a" col_kinds_fpr colvs_rem
    end;
  mem, sa, ia, sctrs, qctrs, colvs_add, colvs_rem
let nlu_split_seg_pars (mem: graph) (usv: sv) (unfi: int) (se: seg_edge)
    : graph * ind_args_seg * ind_args_seg * set_cons list * seq_cons list
    * col_kinds * col_kinds =
  let loc_debug = false in
  (* - all parameters should be set for now *)
  let ind = se.se_ind in
  assert (ind.i_ppars = 0 && ind.i_ipars = 0);
  (* - for set parameters: iterate, create new, add const/add eqs *)
  let mem, lseg_a, lrem_a, sctrs, colvs_add, colvs_rem, _ =
    List.fold_left
      (fun (mem, lseg_a, lrem_a, cons, colvs_add, colvs_rem, i) a ->
        let k = IntMap.find i ind.i_pkind.pr_set in
        let seg_a, mem = colv_add_fresh (Ct_set (Some k)) mem in
        let rem_a, mem = colv_add_fresh (Ct_set (Some k)) mem in
        if loc_debug then
          Log.info "nlu_split_ind,set-param @%d[%a]:  [%a]-(%a)" i
            set_par_type_fpr k setv_fpr seg_a setv_fpr rem_a;
        let c =
          if k.st_const then
            [ S_eq (S_var seg_a, S_var a); S_eq (S_var rem_a, S_var a) ]
          else if k.st_add || k.st_head then
            [ S_eq (S_var a, S_uplus (S_var seg_a, S_var rem_a)) ]
          else Log.todo_exn "nlu_split_seg_pars: unhandled kind" in
        let c =
          if i = unfi && k.st_head then
            (S_mem (usv, S_var rem_a)) :: c
          else c in
        let lseg_a = seg_a :: lseg_a in
        let lrem_a = rem_a :: lrem_a in
        let cons = c @ cons in
        let colvs_rem = SvMap.add a CK_set colvs_rem in
        let colvs_add =
          SvMap.add seg_a CK_set (SvMap.add rem_a CK_set colvs_add) in
        (mem, lseg_a, lrem_a, cons, colvs_add, colvs_rem, i + 1)
      )
      (mem, [ ], [ ], [ ], SvMap.empty, SvMap.empty, 0)
      se.se_eargs.ias_set in
  let mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, _ =
    List.fold_left
      (fun (mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, i) (a_l, a_r) ->
        let k = IntMap.find i ind.i_pkind.pr_seq in
        let seg_a_l, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        let seg_a_r, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        let rem_a_l, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        let rem_a_r, mem = colv_add_fresh (Ct_seq (Some k)) mem in
        if loc_debug then
          Log.info "nlu,split_ind,seq-param @%d[%a]:   [%a-%a]-(%a-%a): ?" i
            seq_par_type_fpr k seqv_fpr seg_a_l seqv_fpr seg_a_r
            seqv_fpr rem_a_l seqv_fpr rem_a_r;
        let qctrs =
          if k.sq_add then
            let el = Seq_Concat [ Seq_var seg_a_l ; Seq_var rem_a_l ] in
            let cl = Seq_equal (Seq_var a_l, el) in
            let er = Seq_Concat [ Seq_var rem_a_r ; Seq_var seg_a_r ] in
            let cr = Seq_equal (Seq_var a_r, er) in
            cl :: cr :: qctrs
          else qctrs in
        let qlseg = (seg_a_l, seg_a_r) :: qlseg
        and qlrem = (rem_a_l, rem_a_r) :: qlrem in
        let colvs_rem =
          List.fold_left (fun acc cv -> SvMap.add cv CK_seq acc) colvs_rem
            [ a_l; a_r ] in
        let colvs_add =
          List.fold_left (fun acc cv -> SvMap.add cv CK_seq acc) colvs_add
            [ seg_a_l; seg_a_r; rem_a_l; rem_a_r ] in
        mem, qlseg, qlrem, qctrs, colvs_add, colvs_rem, i + 1
      )
      (mem, [ ], [ ], [ ], colvs_add, colvs_rem, 0) se.se_eargs.ias_seq in
  if loc_debug then
    begin
      List.iter (fun c -> Log.info "nlu,set-cstr: %a" set_cons_fpr c) sctrs;
      List.iter (fun c -> Log.info "nlu,seq-cstr: %a" seq_cons_fpr c) qctrs;
      Log.info "nlu,Add: %a" col_kinds_fpr colvs_add;
      Log.info "nlu,Rem: %a" col_kinds_fpr colvs_rem
    end;
  let sa = { ias_ptr = [ ];
             ias_int = [ ];
             ias_set = List.rev lseg_a;
             ias_seq = List.rev qlseg }
  and ra = { ias_ptr = [ ];
             ias_int = [ ];
             ias_set = List.rev lrem_a;
             ias_seq = List.rev qlrem } in
  mem, sa, ra, sctrs, qctrs, colvs_add, colvs_rem
