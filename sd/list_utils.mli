(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_utils.mli
 ** utilities for the simplified list domain
 ** Xavier Rival, 2014/02/24 *)
open Data_structures
open Lib
open Sv_def

open Ast_sig
open Graph_sig
open Ind_sig
open List_sig
open Nd_sig
open Set_sig
open Seq_sig
open Svenv_sig
open Vd_sig

open Gen_dom

open Inst_utils

(** Empty memory *)
val lmem_empty: lmem

val lmem_reach: lmem -> SvSet.t -> SvSet.t

(** Pretty-printing *)
(* To formatters *)
val set_equa_fpr: form -> set_equa -> unit
val l_def_fpri: string -> form -> l_def -> unit
val l_call_fpri: string -> form -> l_call -> unit
val l_call_fprc: form -> l_call -> unit
val lseg_call_fprc: form -> lseg_call -> unit
val l_call_ptrvars_fpr: form -> l_call -> unit
val l_call_setvars_fpr: form -> l_call -> unit
val l_call_seqvars_fpr: form -> l_call -> unit
val lnode_fpri: string -> form -> lnode -> unit
val lmem_fpri: ?refcount:bool -> string -> form -> lmem -> unit
val l_content_fpr: form -> l_content -> unit
(* Get items *)
val get_def: lmem -> l_def
val get_maya_off: sv -> lmem -> int list
val lseg_2list: sv -> lmem -> lmem

(** Sanity checks *)
val sanity_check: string -> lmem -> unit

(** Management of SVs *)
val sve_sync_bot_up: lmem -> lmem * svenv_mod

(** Symbolic variables operations *)
val sv_mem: sv -> lmem -> bool
val sv_find: sv -> lmem -> lnode
val sv_pred: sv -> lmem -> SvSet.t
val sv_kind: sv -> lmem -> region_kind
val sv_add: ?root:bool -> ?prio:bool -> sv -> ntyp -> lmem -> lmem
val sv_add_fresh: ?root:bool -> ntyp -> lmem -> sv * lmem
val sv_rem: sv -> lmem -> lmem
val sv_get_all: lmem -> SvSet.t
val sv_is_ind: sv -> lmem -> bool
(* Root operations *)
val sv_root:   sv -> lmem -> lmem
val sv_unroot: sv -> lmem -> lmem

(** Management of COLVs *)
val colv_add_fresh: ?root:bool -> col_par_type -> lmem -> sv * lmem
val colv_add:       ?root:bool -> sv -> col_kind -> lmem -> lmem
val colv_mem:       sv -> lmem -> bool
val colv_rem:       sv -> lmem -> lmem
val colv_get_all:   lmem -> col_kind SvMap.t
val colv_get_roots: lmem -> col_kind SvMap.t
val colv_is_root:   lmem -> sv -> bool
val colv_roots:     lmem -> SvSet.t
val colv_kind:      sv -> lmem -> col_kind

(** Management of set variables *)
val setv_type: lmem -> sv -> Set_sig.set_par_type option
val setv_set: lmem -> SvSet.t
val setv_roots: lmem -> SvSet.t

(** Management of seq variables *)
val seqv_type: lmem -> sv -> Seq_sig.seq_par_type option
val seqv_set: lmem -> SvSet.t
val seqv_roots: lmem -> SvSet.t

(** Memory block operations *)
(* Existence of a points-to edge *)
val pt_edge_mem: Offs.svo -> lmem -> bool
(* Mark a node with priority level for isle/join *)
val mark_prio: sv -> lmem -> lmem
(* Appends a field at the end of a block *)
val pt_edge_block_append: ?nochk: bool -> bnode -> pt_edge -> lmem -> lmem
(* Removal of a points-to block
 *  remrc: says whether refcounts should be killed (by default true) *)
val pt_edge_block_destroy: ?remrc:bool -> sv -> lmem -> lmem
(* Try to decide if an offset range is in a single points-to edge
 *  of a fragmentation, and if so, return its destination *)
val pt_edge_find_interval: (n_cons -> bool)
  -> sv (* node representing the base address *)
    -> Offs.t (* minimum offset of the range being looked for *)
      -> int       (* size of the range being looked for *)
        -> lmem -> pt_edge option
(* Replacement of a points-to edge by another *)
val pt_edge_replace: (n_cons -> bool) -> Offs.svo -> pt_edge -> lmem -> lmem
(* Extraction of a points to edge: reads, after split if necessary *)
val pt_edge_extract: (n_cons -> bool) -> Offs.svo -> int -> lmem
  -> lmem * pt_edge

(* Addition of a list edge *)
val list_edge_add: sv -> l_call -> lmem -> lmem
(* Removal of a list edge *)
val list_edge_rem: sv -> lmem -> lmem

(* Addition of a segment edge *)
val lseg_edge_add: sv -> sv -> l_call -> lseg_call -> lmem -> lmem
(* Removal of a segment edge *)
val lseg_edge_rem: sv -> lmem -> lmem

(* Number of remaining edges *)
val num_edges: lmem -> int

(** Garbage collection support *)
(* Removal of all nodes not reachable from a set of roots *)
val gc: sv Aa_sets.t -> lmem -> lmem * SvSet.t

(** Inductive set parameters construction *)
(* - unfold a seg or an ind edge presented by lc: l_call in the right side
 * - with a new segment
 * - and a new seg or an new ind edge
 * - also returns the set of removed Set COLVs
 * - and          the set of all new Set COLVs *)
val gen_ind_setpars: lmem -> l_call
  -> lmem * l_call * lseg_call * l_call * (Set_sig.set_cons list)
      * SvSet.t * SvSet.t
(* Splitting an inductive predicate into a pair of calls;
 * this function takes care of the parameters and prepares a list of
 * set constraints that should also be taken into account to precisely
 * account for the Set COLV parameter kinds (const, head, etc) *)
val split_indpredpars: l_call -> lmem
  -> lmem * l_call * lseg_call * l_call * set_cons list * seq_cons list

(** Reduction *)
val lmem_rename_sv: sv -> sv -> lmem -> lmem
val lmem_guard: n_cons -> lmem -> lguard_res

(** Utilities for join *)
(* Initialization of the join graph:
 * returns:
 *  - new shape
 *  - colvs to add in the left (before the join)
 *  - colvs to add in the right (before the join)
 *  - colvs to remove in both sides, after the join
 *)
val init_from_roots:
    (sv * sv) SvMap.t -> lmem -> lmem
      -> lmem * (col_kind SvMap.t * col_kind SvMap.t) * col_kind SvMap.t
(* Comparison of list descriptors *)
val l_call_compare: l_call -> l_call -> int
val lseg_call_compare: lseg_call -> lseg_call -> int

(** Inductive definitions *)
(* Global map of list-like inductive definitions *)
val list_ind_defs: l_def StringMap.t ref
(* Experimental code, to try to look for list like inductive defs *)
val exp_search_lists: unit -> unit

(* Utilities needed for list visualization *)
val nodes_of_lmem: lmem -> lnode list
val lnode_reach: lnode -> SvSet.t
