(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: array_ind_utils.ml
 **       data-types for the inductive definitions.
 ** Jiangchao Liu, 2016/03/01 *)
open Data_structures
open Lib
open Graph
open Sv_def

open Array_ind_sig
open Graph_sig
open Ind_form_sig
open Ind_sig
open List_sig
open Set_sig
open Spec_sig

open Ind_inst

open C_utils
open Ind_form_utils
open Ind_utils
open List_utils
open Spec_utils
open Sv_utils

(** Set of inductive definitions *)
let array_ind_defs: array_ind StringMap.t ref = ref StringMap.empty
let array_list_ind_defs: l_def StringMap.t ref = ref StringMap.empty

(** Error report *)
module Log =
  Logger.Make(struct let section = "arr_iu___" and level = Log_level.DEBUG end)


(** Pretty print  *)
let formal_maya_arg_fpr (fmt: form): formal_maya_arg -> unit = function
  | Fa_par_maya i -> F.fprintf fmt "%cm%d" '@' i
  | Fa_par_nmaya i -> F.fprintf fmt "%cn%d" '@' i

let mform_fpr (fmt: form): mform -> unit = function
  | Ai_Mf_mem (a, m) ->
      F.fprintf fmt "%a # %a" formal_arith_arg_fpr a formal_maya_arg_fpr m
  | Ai_Mf_equal_s (ma0,ma1) ->
      F.fprintf fmt "%a = %a" formal_maya_arg_fpr ma0 formal_maya_arg_fpr ma1
  | Ai_Mf_included (ma0,ma1) ->
      F.fprintf fmt "%a in %a" formal_maya_arg_fpr ma0 formal_maya_arg_fpr ma1
  | Ai_Mf_cons (op,ma,ae) ->
       F.fprintf fmt "%a %a %a" formal_maya_arg_fpr ma
        c_binop_fpr op aexpr_fpr ae
  | Ai_Mf_empty m -> F.fprintf fmt "%a == { }" formal_maya_arg_fpr m
  | Ai_Mf_cardinality (m,v) ->
      F.fprintf fmt " |%a|== %d" formal_maya_arg_fpr m v

let ai_pformatom_fpr (fmt: form): ai_pformatom -> unit = function
  | FAInd.Ai_Pf pf -> F.fprintf fmt "%a" FInd.pformatom_fpr pf
  | FAInd.Ai_Pf_maya f -> mform_fpr fmt f

let ai_pform_fpr: form -> FAInd.ai_pform -> unit =
  gen_list_fpr "" ai_pformatom_fpr " & "

let air_kind_fpr (fmt: form): air_kind -> unit = function
  | Aik_unk -> F.fprintf fmt "unknown"
  | Aik_empi -> F.fprintf fmt "{emp,int}"
  | Aik_true -> F.fprintf fmt " true"

let airule_fpr (fmt: form) (ar: airule): unit =
  F.fprintf fmt "| [%d" ar.air_num;
  SvMap.iter (fun _ -> F.fprintf fmt " %a" ntyp_fpr) ar.air_typ;
  F.fprintf fmt "]\n\t- %a\n\t- %a\n" hform_fpr ar.air_heap
    ai_pform_fpr ar.air_pure

let array_ind_fpr (fmt: form) (ai: array_ind): unit =
  F.fprintf fmt "%s<%d,%d> :=\n%a\n" ai.ai_name ai.ai_ppars
    ai.ai_ipars (gen_list_fpr "" airule_fpr "") ai.ai_rules;
  (* Printing the analysis results *)
  List.iteri
    (fun i air ->
      F.fprintf fmt "  rule %d => %b\n" i air.FAInd.air_unone
    ) ai.ai_rules


(** Analysis on inductive predicates in arays *)
exception Success
exception Failure


(** Computation of ir_kind fields
 *  i.e., when a rule is either necessary null (emp) or non null (non emp) *)

let check_empty_rule (r: airule): bool =
  try
    List.iter
      (function
        | FAInd.Ai_Pf (Pf_arith (Af_equal (Ae_var Fa_this, Ae_cst _)))
        | FAInd.Ai_Pf (Pf_arith (Af_equal (Ae_cst _, Ae_var Fa_this))) ->
            raise Success (* it is null *)
        | _ ->
            Log.info "AI: rule is %a" airule_fpr r;
            raise Failure
      ) r.air_pure;
    false
  with
  | Success -> true
  | Failure -> false

let empty_heap_rule_analysis (i: array_ind): array_ind =
  try
    let b =
      List.fold_left
        (fun acc r ->
          if check_empty_rule r then
            if acc then raise Failure (* found a second empty rule *)
            else true (* found one empty rule *)
          else acc
        ) false i.ai_rules in
    { i with ai_mt_rule = b }
  with
  | Failure -> { i with ai_mt_rule = false } (* multiple empty rules found *)

let ind_rules_kind (ind: array_ind): array_ind =
  let f_rule (r: airule): airule =
    let kind =
      try
        (* check whether the pointer is null *)
        let this_is_null = check_empty_rule r in
        (* check whether the heap region is empty *)
        let region_is_emp = r.air_heap = [ ] in
        match this_is_null, region_is_emp, r.air_kind with
        | _, _, Aik_true -> Aik_true
        | true, true, _ -> Aik_empi
        | false, true, _ -> Log.warn "AI: empty region, non null"; Aik_empi
        | _, _, _ -> Aik_unk
      with Failure -> Aik_unk in
    if !Flags.flag_debug_ind_analysis then
      Log.force "Rule kind: %a" air_kind_fpr kind;
    { r with air_kind = kind } in
  { ind with ai_rules = List.map f_rule ind.ai_rules }


(** Initialization from parsing *)
let array_ind_init (al: array_ind list): unit =
  List.iter
    (fun aind ->
      let name = aind.FAInd.ai_name in
      Log.info "\n AI: ind is \n %a\n" array_ind_fpr aind;
      assert (not (StringMap.mem name !array_ind_defs));
      List.iter
        (fun r ->
          if SvMap.cardinal r.FAInd.air_typ != r.FAInd.air_num then
            Log.warn "incorrect rule in %s" aind.ai_name;
        ) aind.FAInd.ai_rules;
      let aind = empty_heap_rule_analysis aind in
      let aind = ind_rules_kind aind in
      array_ind_defs := StringMap.add name aind !array_ind_defs;
      Log.info "Inductive %s; mt result: %b\n%a"
        aind.ai_name aind.ai_mt_rule array_ind_fpr aind;
    ) al

(** Find array list definitions from inductive definitions *)
let exp_search_array_lists ( ): unit =
  let module M = struct exception Stop of string end in (* local bail out *)
  array_list_ind_defs := StringMap.empty;
  let name_2ldef name =
    try StringMap.find name !array_list_ind_defs
    with Not_found -> raise (M.Stop (Printf.sprintf "%s not found" name)) in
  let aux_rule (iname: string) (aind: array_ind)
      (r: airule) (r_emp: airule): l_def =
    let m_subinds, m_fieldsoff, m_idxoff, m_fieldsrev =
      List.fold_left
        (fun (accsubs, accfo, accio, accfr) -> function
          | FInd.Hacell cell ->
              begin
                match cell.ic_dest, Offs.to_int_opt cell.ic_off with
                | Fa_var_new i, Some o ->
                    let i = sv_unsafe_of_int i in
                    ( accsubs, IntMap.add o cell accfo,
                      SvMap.add i o accio, SvMap.add i cell accfr )
                | _, _ -> raise (M.Stop "field")
              end
          | FInd.Haind icall ->
              begin
                match icall.ii_maina with
                | Fa_this -> raise (M.Stop "main field")
                | Fa_var_new i ->
                    let i = sv_unsafe_of_int i in
                    SvMap.add i icall accsubs, accfo, accio, accfr
              end
          | FInd.Haseg (_, _) -> Log.todo_exn "???033"
        ) (SvMap.empty, IntMap.empty, SvMap.empty, SvMap.empty)
        r.FAInd.air_heap in
    let idx_2off idx =
      try SvMap.find idx m_idxoff
      with Not_found -> Log.fatal_exn "symvar %a at no offset" sv_fpr idx in
    (* Extracting a single next field, and tail inductive predicate *)
    let onext, callnext, others =
      let self, others =
        SvMap.fold
          (fun i call (self, others) ->
            if String.compare call.ii_ind iname = 0 then
              (idx_2off i, call) :: self, SvMap.remove i others
            else self, others
          ) m_subinds ([ ], m_subinds) in
      match self with
      | [ ] -> raise (M.Stop "no next field")
      | _ :: _ :: _ -> raise (M.Stop "several next fields")
      | [ inext, callnext ] -> inext, callnext, others in
    let nest_calls, nest_call_setpars =
      SvMap.fold
        (fun i ic (c, sp) ->
          (idx_2off i, name_2ldef ic.ii_ind) :: c, ic.ii_args :: sp)
        others ([ ], [ ]) in
    let _subcall_pars =
      List.fold_lefti
        (fun i r -> function
          | (Fa_var_new j : formal_set_arg) ->
              let j = sv_unsafe_of_int j in
              if SvMap.mem j r then
                raise (M.Stop "one new var => several sub-pars")
              else SvMap.add j (Sv_nextpar i) r
          | _ -> r
        ) SvMap.empty callnext.ii_args in
    (* Searching for the size *)
    let size =
      let acc =
        List.fold_left
          (fun acc -> function
            | FAInd.Ai_Pf (Pf_alloc s) ->
                if acc != None then raise (M.Stop "two sizes"); Some s
            | _ -> acc
          ) None r.air_pure in
      match acc with
      | None -> raise (M.Stop "no size")
      | Some s -> s in
    let emp_csti,emp_mcons =
      List.fold_left
        (fun (acc_cst,acc_m) node ->
          match node with
          | FAInd.Ai_Pf (Pf_arith (Af_equal ((Ae_cst aint), (Ae_var Fa_this))))
          | FAInd.Ai_Pf (Pf_arith (Af_equal (Ae_var Fa_this, Ae_cst aint))) ->
              aint, acc_m
          | FAInd.Ai_Pf_maya a_m -> acc_cst, a_m :: acc_m
          | _ ->  acc_cst, acc_m
        ) (-1,[]) r_emp.air_pure in
    let next_mcons =
      List.fold_left
        (fun acc node ->
          match node with
          | FAInd.Ai_Pf_maya am -> am:: acc
          | _ -> acc
         ) [] r.air_pure in
    (* Produce result *)
    { ld_name       = Some aind.ai_name;
      ld_m_offs     = aind.ai_mpars;
      ld_submem     = aind.ai_submem;
      ld_emp_csti   = emp_csti;
      ld_emp_mcons  = emp_mcons;
      ld_next_mcons = next_mcons ;
      ld_nextoff    = onext;
      ld_nextdoff   = Log.todo_exn "Need destination offeset";
      ld_prevpar    = None;
      ld_size       = size;
      ld_onexts     = (nest_calls |> ignore;
                       Log.todo_exn "Need destination offets") ;
      ld_ptr        = None;
      ld_set        = None;
      ld_seq        = None } in
  let depgraph =
    StringMap.fold
      (fun name aind acc ->
        List.fold_left
          (fun acc rule ->
            List.fold_left
              (fun acc -> function
                | FInd.Hacell _ -> acc
                | FInd.Haind ic -> StringGraph.edge_add name ic.ii_ind acc
                | FInd.Haseg (_, _) -> Log.todo_exn "???034"
              ) acc rule.FAInd.air_heap
          ) acc aind.FAInd.ai_rules
      ) !array_ind_defs StringGraph.empty in
  let components =
    List.filter (fun s -> StringSet.cardinal s <= 1)
      (StringGraph.tarjan depgraph) in
  let ind_candidates =
    List.fold_left (fun acc s -> StringSet.min_elt s :: acc) [ ] components in
  (* Iteration over the inductive definitions candidate to conversion *)
  List.iter
    (fun name ->
      Log.info "looking at inductive definition %s" name;
      let aind =
        try StringMap.find name !array_ind_defs
        with Not_found -> Log.fatal_exn "ind def %s not found" name in
      Log.info "mt_rule %b\n" aind.ai_mt_rule;
      Log.info "length is %d \n" (List.length aind.ai_rules);
      if aind.ai_mt_rule && List.length aind.ai_rules = 2 then
        let rule, emp_rule =
          match aind.ai_rules with
          | [ r0 ; r1 ] ->
              if r0.air_kind = Aik_empi then r1, r0
              else if r1.air_kind = Aik_empi then r0, r1
              else Log.fatal_exn "contradicting rules structure"
          | _ -> Log.fatal_exn "contradicting number of rules" in
        try
          let lc = aux_rule name aind rule emp_rule in
          Log.info "\nDefinition %s converted into:\n%a\n" name
            (l_def_fpri "    ") lc;
          array_list_ind_defs := StringMap.add name lc !array_list_ind_defs
        with
        | M.Stop s -> Log.info "%s not an array list like ind def (%s)\n" name s
      else Log.info "%s not array list like ind def (rule #)" name
    ) ind_candidates

let elaborate_ai_pformatom (c_type_spec: Parsed_spec.c_type_spec)
      (a: Ai_parsed_spec.ai_pformatom): FAInd.ai_pformatom =
  match a with
  | Ai_Pf a -> FAInd.Ai_Pf (Spec_utils.elaborate_pformatom c_type_spec a)
  | Ai_Pf_maya m -> Ai_Pf_maya m

let elaborate_airule (c_type_spec: Parsed_spec.c_type_spec)
      (r: Ai_parsed_spec.airule): airule =
  { air_num  = r.air_num;
    air_typ  = r.air_typ;
    air_kind = r.air_kind;
    air_unone = r.air_unone;
    air_heap = Spec_utils.elaborate_hform c_type_spec
      r.Ai_parsed_spec.FAIParsed.air_heap;
    air_pure = List.map (elaborate_ai_pformatom c_type_spec) r.air_pure;
 }

let elaborate_array_ind (type_context : Parsed_spec.type_context)
      (ai: Ai_parsed_spec.array_ind): array_ind option =
  let c_type_spec_opt =
    match type_context ai.c_type_ref_opt with
    | Error `Not_found -> None
    | Ok c_type_ref -> Some c_type_ref in
  match c_type_spec_opt with
  | None -> None
  | Some c_type_spec ->
      let rules =
        List.map (elaborate_airule c_type_spec) ai.array_ind.ai_rules in
      let array_ind = ai.Ai_parsed_spec.array_ind in
      Some { FAInd.ai_submem   = array_ind.ai_submem;
             FAInd.ai_name     = array_ind.ai_name;
             FAInd.ai_ppars    = array_ind.ai_ppars;
             FAInd.ai_ipars    = array_ind.ai_ipars;
             FAInd.ai_mpars    = array_ind.ai_mpars;
             FAInd.ai_rules    = rules;
             FAInd.ai_mt_rule  = array_ind.ai_mt_rule;
             FAInd.ai_emp_ipar = array_ind.ai_emp_ipar; }
