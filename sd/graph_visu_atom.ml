(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_encode.mli
 **       basic modules for graphs and encoded graphs output
 ** Francois Berenger and Huisong Li, started 2015/02/24 *)
open Data_structures
open Sv_def

open Graph_sig
open Graph_utils
open Graph_visu_sig
open Ind_utils
open Ind_sig

open Sv_utils

module L  = List
module Pr = Printf

module Log =
  Logger.Make(struct let section = "gv_atom_" and level = Log_level.DEBUG end)

let arg_fpr (fmt: form) (pa, ia, sa, qa) =
  F.fprintf fmt "%a|%a|%a|%a"
    sv_list_fpr pa sv_list_fpr ia sv_list_fpr sa sv_list_fpr qa

let arg_seg_fpr (fmt: form) (pa, ia, sa, qa) =
  let seq_arg_fpr fmt (ql, qr) = F.fprintf fmt "%a■%a" sv_fpr ql sv_fpr qr in
  F.fprintf fmt "%a|%a|%a|%a"
    sv_list_fpr pa sv_list_fpr ia sv_list_fpr sa
    (Lib.gen_list_fpr "" seq_arg_fpr ",") qa
module Edge : EDGE =
struct
  (* types of edges are describbed in graph_sig.ml *)
  type args =
    sv list * sv list * sv list * sv list
  type args_seg =
    sv list * sv list * sv list * (sv * sv) list
  type t =
    | Empty of sv (* sv = node id *)
    | Inductive of string * sv * args (* (ind_name, sv, ind_args) *)
    (* (ind_name, src_sv, src_args, src_offsets, dst_sv, dst_args) *)
    | Segment of string * sv * args * Offs.OffSet.t * args_seg * sv * args
    (* (src_node, src_offset, dst_node, dst_offset) *)
    | Points_to of (sv * Offs.t * sv * Offs.t) list

  let extract_arg (arg: ind_args): args =
    arg.ia_ptr, arg.ia_int, arg.ia_set, arg.ia_seq

  let extract_arg_seg (arg: ind_args_seg): args_seg =
    arg.ias_ptr, arg.ias_int, arg.ias_set, arg.ias_seq

  let extract_ind_args ind =
    extract_arg ind.ie_args

  let extract_seg_args seg =
    hseg_sanity_check ~isok:1 "graph_visu_atom,extract_seg_args" seg;
    (extract_arg seg.se_sargs,extract_arg_seg seg.se_eargs,extract_arg seg.se_dargs)

  let of_node (src: node): t =
    let src_node = src.n_i in
    (* process its outgoing edges *)
    match src.n_e with
    | Hemp     -> Empty src_node
    | Hind ind -> Inductive (ind.ie_ind.i_name, src_node, extract_ind_args ind)
    | Hseg seg ->
        let dst_node = seg.se_dnode in
        let seg_ind = seg.se_ind in
        let ind_name = seg_ind.i_name in
        let src_args, edg_args, dst_args = extract_seg_args seg in
        (* currently in memcad: a segment is only between nodes with
         * the same inductive definition. So we use the more restricted
         * ind.i_self_dirs here instead of ind.i_dirs.
         * This will prevent segments from being extended (offsets
         * at extremities being folded into the segment) too much
         * during join of paths. *)
        let ind_offsets = seg_ind.i_may_self_dirs in
        Segment (ind_name, src_node, src_args, ind_offsets,
                 edg_args, dst_node, dst_args)
    | Hpt block_frag ->
        let edges =
          (* src offs to dest node and offs *)
          let src_to_dst = Block_frag.map_to_list block_frag in
          let res =
            L.map
              (fun (src_bound, dst_pt_edge) ->
                let src_offs = Bounds.to_offs src_bound in
                let dst_node, dst_offs =
                  (Block_frag.block_entry_elt dst_pt_edge).pe_dest in
                (src_node, src_offs, dst_node, dst_offs)
              ) src_to_dst in
          if !Flags.no_ind_prev_fields then
            (* filter out backward edges flagged as such by the ind. def. *)
            match ind_of_node src with
            | None -> res (* node not result of past unfolding *)
            | Some ind_name ->
                let ind = ind_find ind_name in
                let prev_fields = ind.i_pr_offs in
                L.filter
                  (fun (_src_node, src_off, _dst_node, _dst_off) ->
                    true || not (Offs.OffSet.mem src_off prev_fields)
                  ) res
          else
            res in
        Points_to edges

  (* list useful offsets of each node, based on the list of edges,
     so that we can create nodes with only their needed offsets *)
  let list_offsets (edges: t list): IntSet.t SvMap.t =
    let record_offset (k: sv) (v: Offs.t) (m: IntSet.t SvMap.t) =
      let int_offs = Offs.to_int v in
      let map_replace k v m =
        SvMap.add k v (SvMap.remove k m) in
      try
        let prev_set = SvMap.find k m in
        let new_set = IntSet.add int_offs prev_set in
        map_replace k new_set m
      with
      | Not_found -> SvMap.add k (IntSet.of_list [0; int_offs]) m in
    L.fold_left
      (fun acc edge ->
        match edge with
        | Empty _ | Inductive _ -> acc
        | Segment (_ind_name, src_sv, _, offsets, _, _dst_sv, _) ->
            Offs.OffSet.fold (fun off acc2 -> record_offset src_sv off acc2)
              offsets acc
        | Points_to pt_edges ->
            L.fold_left
              (fun acc (src_sv, src_offs, dst_sv, dst_offs) ->
                record_offset src_sv src_offs
                  (record_offset dst_sv dst_offs acc)
              ) acc pt_edges
      ) SvMap.empty edges

  (* allowed_dest_nodes is used for pruning:
   * only edges with a destination in 'allowed_dest_nodes' are converted *)
  let fpr ?(prefix="") (fmt: form) ((n,allowed_dest_nodes): t * node SvMap.t)
      : unit =
    match n with
    | Empty sv -> ()
          (* (\* if you want a dummy reflexive edge instead: *\) *)
          (* F.fprintf fmt "\"%d\" -> \"%d\" [ label = \"emp\" ];\n" *)
          (*   sv sv *)
    | Points_to src_dst_list ->
        L.iter
          (fun (src_sv, src_offs, dst_sv, dst_offs) ->
            if SvMap.mem dst_sv allowed_dest_nodes then
              let src_off = Offs.to_int src_offs in
              let dst_off = Offs.to_int dst_offs in
              F.fprintf fmt "\"%s%a\":f%d -> \"%s%a\":f%d;\n"
                prefix sv_fpr src_sv src_off prefix sv_fpr dst_sv dst_off
          ) src_dst_list
    | Inductive (name, nid, args) ->
        F.fprintf fmt
          "\"%s%a\":f0 -> \"%s%aend\" [ label = \"==%s(%a)==>\" %s ];\n"
          prefix sv_fpr nid prefix sv_fpr nid name arg_fpr args "penwidth = 2.0"
    | Segment (ind_name, src_sv, src_args, _offsets, edg_args,
               dst_sv, dst_args) ->
        if SvMap.mem dst_sv allowed_dest_nodes then
          begin
            F.fprintf fmt "\"%s%a\":f0 -> \"%s%a\":f0"
              prefix sv_fpr src_sv prefix sv_fpr dst_sv;
            F.fprintf fmt " [ label = \"==(%a)=%s(%a)=(%a)==>\" %s ];\n"
              arg_fpr src_args ind_name arg_seg_fpr edg_args arg_fpr dst_args
              "penwidth = 2.0"
          end
end

module Node : NODE =
struct
  type t = { id:      sv      ;
             name:    string   ;
             typ:     ntyp     ; (* node type *)
             alloc:   nalloc   ; (* node allocation type *)
             offsets: IntSet.t ; (* edges src/dest from/to that node *)
             node:    node     } (* corresponding graph node *)

  let get_name_uid (namer: namer) (id: sv): string =
    try
      let name, uid = namer id in
      Format.asprintf "%s.%a" name sv_fpr uid
    with Not_found -> "_"

  let create (n: node) (namer: namer) (offsets: IntSet.t SvMap.t): t =
    let id = n.n_i in
    let name = get_name_uid namer id in
    let typ = n.n_t in
    let alloc = n.n_alloc in
    let offsets =
      try SvMap.find id offsets
      with Not_found -> IntSet.singleton 0 in
    let node = n in
    { id; name; typ; alloc; offsets; node }

  let is_leaf (n: node): bool =
    match Edge.of_node n with
    | Edge.Empty _ -> false && true
    | _ -> false

  (* set of node ids that you can reach from this node *)
  let successors (n: node): SvSet.t =
    match Edge.of_node n with
    | Edge.Empty _
    | Edge.Inductive _ -> SvSet.empty
    | Edge.Segment (_ind_name, _src_nid, _src_args,
                    _offsets, _edg_args, dst_nid, _dst_args) ->
        SvSet.singleton dst_nid
    | Edge.Points_to src_dsts ->
        L.fold_left
          (fun acc (_src_nid, _, dst_nid, _) -> SvSet.add dst_nid acc)
          SvSet.empty src_dsts

  (* same as successors but keeping source node offset followed *)
  let successors_offs (n: node): (step * sv) list =
    match Edge.of_node n with
    | Edge.Empty _ | Edge.Inductive _ -> []
    | Edge.Segment (ind_name, _src_nid, _src_args,
                    src_offs, _edg_args, dst_nid, _dst_args) ->
        let offsets = Offs.OffSet.elements src_offs in
        [ Segment (ind_name, offsets), dst_nid ]
    | Edge.Points_to src_dsts ->
        L.map
          (fun (_src_nid, src_off, dst_nid, _dst_offs) ->
            (Offset src_off, dst_nid)
          ) src_dsts

  (* is node 'n' the source of an inductive edge ? *)
  let is_inductive_src (n: t): bool =
    match Edge.of_node n.node with
    | Edge.Inductive _ -> true
    | _ -> false

  (* is it OK to fold this node *)
  let can_fold (n: node): bool =
    (* not on the stack *)
    n.n_alloc <> Nstack &&
    n.n_alloc <> Nstatic &&
    (* only one outgoing edge *)
    (match Edge.of_node n with
     | Edge.Empty _ | Edge.Inductive _ | Edge.Segment _   -> true
     | Edge.Points_to src_dsts -> List.length src_dsts = 1)

  let fpr ?(prefix="") ?(compact_pp: bool = true) (selected_vars: SvSet.t)
      (fmt: form) (node: t): unit =
    let color = if node.name <> "_" then "blue4" else "black" in
    let label_str =
      IntSet.fold
        (fun offset acc ->
          let str =
            if offset <> 0 then
              acc ^ (F.asprintf " | <f%d> +%d" offset offset)
            else
              let nalloc_str = F.asprintf "%a" nalloc_fprs node.alloc in
              let ntype_str = F.asprintf "%a" ntyp_fprs node.typ in
              let ntype_str =
                if can_fold node.node then ntype_str ^ " f"
                else ntype_str in
              if nalloc_str = "" then
                F.asprintf "<f0> %a=%s: %s" sv_fpr node.id node.name ntype_str
              else
                F.asprintf "<f0> %a=%s: %s %s"
                  sv_fpr node.id node.name nalloc_str ntype_str in
          match Edge.of_node node.node with
          | Edge.Inductive (ind_name, _ind, args) ->
              if compact_pp then
                F.asprintf "%s" str
              else
                F.asprintf "%s %s(%a)\\=\\=\\>" str ind_name arg_fpr args
          | Edge.Empty _ -> str ^ " NULL"
          | _ -> str
        ) node.offsets "" in
    if is_inductive_src node || SvSet.mem node.id selected_vars then
      F.fprintf fmt "\"%s%a\" [fontname=\"bold\" \
        label=\"%s\" shape=\"record\" style=\"bold\"];\n\
        \"%s%aend\" [label=\"\" shape=\"none\"];\n"
        prefix sv_fpr node.id label_str prefix sv_fpr node.id
    else
      F.fprintf fmt "\"%s%a\" [label=\"%s\" shape=\"record\" color=\"%s\"];\n"
        prefix sv_fpr node.id label_str color
end
