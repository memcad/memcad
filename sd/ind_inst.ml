(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: ind_inst.ml
 **       instantiations of inductive predicaates and specifications
 ** Xavier Rival, 2023/06/17 *)
open Data_structures

open Ind_gen_functor


(** Instantiation of functorized symbolic formulas *)
(* Symbolic formulas with Offs.t offsets, for inductive definitions *)
module FPInd =
  (struct
    type offset = Offs.t
    (** Utilities *)
    let offset_to_int_opt = Offs.to_int_opt
    (** Printers *)
    let offset_fpr (fmt: form) (off: Offs.t): unit =
      match Offs.to_int_opt off with
      | None -> F.pp_print_string fmt "?"
      | Some d -> F.pp_print_int fmt d
    let offset_dest_fpr (fmt: form) (off: Offs.t): unit =
      if not (Offs.is_zero off) then
        F.fprintf fmt "->%a" Offs.t_fpr off
  end: FORMPAR with type offset = Offs.t)
module FInd = MakeSymForms( FPInd )
