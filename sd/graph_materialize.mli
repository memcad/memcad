(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_materialize.mli
 **       unfolding of graphs, materialization
 ** Xavier Rival, 2011/08/06 *)
open Data_structures
open Sv_def

open Ast_sig
open Col_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Set_sig
open Seq_sig
open Vd_sig

(** Materialization of an inductive *)
val materialize_ind:
    submem: bool (* whether sub-memory is_le (no alloc check) *)
  -> bool option (* whether to put empty rules first, last or not to care *)
    -> bool (* whether to generate empty segment case (for segment unfold) *)
      -> bool (* whether segment is of length 1 (for segment unfold) *)
        -> sv -> graph -> unfold_res list

(** Higher level unfold function *)
val unfold:
    submem: bool (* whether sub-memory is_le (no alloc check) *)
  -> sv (* source: node where unfolding should take place *)
    -> unfold_dir (* direction, for segment edges only *)
      -> graph    (* input graph *)
        -> unfold_res list

(** unfold only the numerical constraints function *)
val unfold_num:
    sv (* source: node where unfolding should take place *)
  -> n_cons (* guard *)
    -> graph    (* input graph *)
      -> n_cons list * set_cons list * seq_cons list

(** Splitting parameters for non-local unfolding *)
val nlu_split_ind_pars: graph -> sv -> int -> ind_edge ->
  graph * ind_args_seg * ind_args * set_cons list * seq_cons list
    * col_kinds * col_kinds
val nlu_split_seg_pars: graph -> sv -> int -> seg_edge ->
  graph * ind_args_seg * ind_args_seg * set_cons list * seq_cons list
    * col_kinds * col_kinds

