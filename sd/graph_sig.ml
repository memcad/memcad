(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: graph_sig.ml
 **       signatures of the main graph data-types
 ** Xavier Rival, 2011/05/19 *)
open Data_structures
open Offs
open Sv_def

open Ast_sig
open Col_sig
open Ind_sig
open Inst_sig
open Nd_sig
open Svenv_sig
open Set_sig
open Seq_sig
open Vd_sig

(** Improvements to consider:
 ** - try to merge "node_wkinj" and "node_relation" (may not be doable)
 ** - try to use Bi_funs as part of these structures
 **   (would reduce the implementation size, and share code)
 **)


(** Sides *)
type side =
  | Lft
  | Rgh

(** Visualization options *)
type visu_option =
  (* 'Connex_component' is incompatible with 'Successors';
   * use one XOR the other *)
  | Connex_component (* only keep the connex components including
                      * the selected (by variable names) nodes *)
  | Successors (* only keep the successors of the selected nodes *)
  | Cut_leaves (* leaves which are not inductive edges are pruned *)

(** Graph node IDs *)
type svo   = sv * Offs.t   (* node with an offset *)
type bnode = sv * Bounds.t (* node with a bound, i.e., a set of offsets *)

(* Path strings *)
type n_path = sv * pathstr * sv

(** Arguments for inductives *)
(* Each field denotes a list of nodes *)
type ind_args =
    { ia_ptr:     sv list ;
      ia_int:     sv list ;
      ia_set:     sv list ;
      ia_seq:     sv list }
(* XR: TODOs:
 * - not sure what is the convention for the ias_seq field (comment)
 * - there is maybe too much duplication between these types; see if
 *   it is possible to merge and simplify *)
type ind_args_seg =
    { ias_ptr:     sv list ;
      ias_int:     sv list ;
      ias_set:     sv list ;
      ias_seq:     (sv * sv) list }

(** Fresh sets *)
(* denotes sets of SVs/COLVs in a graph; e.g., that can be instantiated *)
type freshset =
    { f_ptr: SvSet.t ;
      f_int: SvSet.t ;
      f_col: col_kinds }

(** Heap regions: atoms *)
(* Points-to edge *)
type pt_edge =
    { pe_size:    Offs.size ; (* size of the cell (in bytes) *)
      pe_dest:    Offs.svo  } (* dest node with offset *)
(* Inductive edge *)
type ind_edge =
    { ie_ind:     ind ;         (* inductive definition *)
      ie_args:    ind_args      (* arguments of the inductive *) }
(* Segment edge *)
(* Conventions:
 *  |se_sargs| = |se_dargs|
 *  se_eargs: arguments either shared or add/head
 *  for a list with:
 *  - prev pointer (dupplicated)
 *  - set of element (head,add,cst) <= all collections
 *  a0.list(prev0) *={se_eargs:set of elements of the segment}= a1.list(prev1)
 *)
type seg_edge =
    { se_ind:     ind ;          (* inductive definition *)
      se_eargs:   ind_args_seg ; (* edge arguments *)
      se_sargs:   ind_args ;     (* arguments at the source site *)
      se_dargs:   ind_args ;     (* arguments at the destination site *)
      se_dnode:   sv             (* destination node *) }

(** Heap regions: descriptions *)
type heap_frag =
  | Hemp
  | Hpt of pt_edge Block_frag.t
  | Hind of ind_edge
  | Hseg of seg_edge
(* Graphviz convention:
 *      Hemp                 no edge from here
 *      Hpt                  regular edges
 *      Hind                 dangling edge (no destination node)
 *      Hseg                 one segment edge
 *)
type nalloc =
  | Nheap        (* heap zone *)
  | Nstack       (* somewhere on the stack *)
  | Nstatic      (* static area *)
  | Nnone        (* not an allocated area *)
  | Nsubmem      (* submem node; no allocation constraint for now *)
(* node specification, characterizes the value it represents, enclosing
 * - the type describing the kind of its value;
 * - the size information (number of bits) *)
type nspec = ntyp * int option
(* node_attribute are hints computed by the analyzer for itself
 *  either none (no information), or some ind_name *)
type node_attribute =
  | Attr_none          (* no attribute *)
  | Attr_ind of string (* has been an inductive of a given name *)
  (* Attribute for an SV that may denote the contents of an array:
   *  - i: stride of a block
   *  - o: list of fields offsets in a block (optional)            *)
  | Attr_array of int (* i *) * int list option (* o *)
type node =
    { n_i:     sv ;           (* index of the current node *)
      n_t:     ntyp ;         (* type of the value represented by the node *)
      n_e:     heap_frag ;    (* heap fragment attached to that node *)
      n_alloc: nalloc ;       (* allocation zone *)
      n_preds: SvSet.t ;      (* nodes pointing to that node *)
      n_ptprio: bool ;        (* this node should have priority in join/isle *)
      n_attr:  node_attribute (* attribute, e.g., inductive setup *) }
type graph =
    { (* Names of the inductive definitions allowed in the graph *)
      g_inds:       StringSet.t ;
      (* Key allocator *)
      g_nkey:       SvGen.t ;
      (* Graph contents *)
      g_g:          node SvMap.t ; (* Table of nodes, edges *)
      (* Local symbolic variable environment modifications *)
      g_svemod:     svenv_mod ;
      (* Roots of the shape graph *)
      g_roots:      SvSet.t ; (* set of root nodes *)
      (* Management of COLVs (Seq/Set) *)
      g_colvkey:    SvGen.t; (* key allocator for the col variables *)
      g_colvroots:  SvSet.t; (* colv which are root *)
      g_colvbindex: SvSet.t SvMap.t; (* colv |-> {sv | sv===(colv)===> } *)
      g_colvkind:   col_par_type SvMap.t; (* kind of colvs *)
      g_colvinfo:   colv_info SvMap.t; (* information on colvs *)
    }
(* Some sanity rules, regarding to sizes (assuming a 32 bits architecture):
 *  - for now, we assume pointer size to be equal to 4
 *  - address nodes should have size 4
 *  - nodes at origin of points-to edges should be of size 4
 *  - when pt edge destination offset is not null, size should be 4
 *    both for the edge and for the destination node
 *)


(** Node relations, for algorithms manipulating graphs *)

(* Injections for comparison algorithms (is_le...)
 * describes a mapping of the nodes of the right graph
 * into those of the left graph (embedding) *)
type node_emb = (sv, sv) Aa_maps.t
type node_embedding =
    { n_img: sv SvMap.t;     (* direct image *)
      n_pre: SvSet.t SvMap.t (* pre-image, used to detect siblings *) }

(* Relations in double traversal algorithms (for weakening and join) *)
(* Version for weakening *)
type node_wkinj =
    { wi_img:  (sv * sv) SvMap.t;   (* direct image R -> (L,O) *)
      wi_l_r:  SvSet.t SvMap.t;     (* image L -> {R} *)
      wi_lr_o: sv SvPrMap.t;        (* image (L,R) -> O *) }
(* Version for join *)
type node_relation =
    { n_inj: sv SvPrMap.t ;       (* (l,r)->res mapping*)
      n_pi:  (sv * sv) SvMap.t ;  (* res->(l,r) mapping*)
      n_l2r: SvSet.t SvMap.t ;    (* key to sets in other side l->r *)
      n_r2l: SvSet.t SvMap.t ;    (* key to sets in other side r->l *) }


(** Algorithms that manipulate graphs *)

(* Outcome of a guard in the graph level *)
type guard_res =
  | Gr_bot                   (* the condition evaluates to _|_ *)
  | Gr_no_info               (* the condition generates no information at all *)
  | Gr_equality of sv * sv   (* renaming to do in the graph and numeric *)
  | Gr_emp_seg of sv         (* the segment at that node is actually empty *)
  | Gr_emp_ind of sv         (* the inductive at that node is actually empty *)

(* Outcome of a partial inclusion checking *)
type ilrem =
    { (* the remainder in the left hand side *)
      ilr_graph_l:  graph;
      (* nodes removed in the left hand side *)
      ilr_svrem_l:  SvSet.t;
      (* mapping inferred by the inclusion *)
      ilr_svrtol:   sv SvMap.t;
      (* Img of COLVs embedding *)
      ilr_colvrtol: SvSet.t SvMap.t;
      (* Instantiation *)
      (* - sv instantiation  *)
      ilr_svinst:   sv_inst;
      (* - setv instantiation *)
      ilr_setinst:  is_le_setv_inst;
      (* - seqv instantiation *)
      ilr_seqinst:  is_le_seqv_inst;
      (* Remaining constraints *)
      (* - un-discharged, un-renamed right set constraints*)
      ilr_setctr_r: set_cons list;
      (* - un-discharged, un-renamed right seq constraints*)
      ilr_seqctr_r: seq_cons list; }
type is_le_res =
  (* the algorithm failed to establish inclusion *)
  | Ilr_not_le
  (* the algorithm succeeds while consuming a sub-graph in the left;
   * it returns various elements used for the join or directed weakening *)
  | Ilr_le_rem of ilrem
  (* remainder in the left reduced to empty
   * corresponds to the syntehsis of an inductive edge
   * (used for weak abstraction) *)
  | Ilr_le_ind of graph
  (* remainder in the left graph is a single inductive edge
   * corresponds to the synthesis of a segment edge
   * (used for weak abstraction) *)
  | Ilr_le_seg of graph * sv * ind_edge * sv SvMap.t

(* Result of unfold/materialization *)
type unfold_res =
    { ur_g:        graph;
      (* Constraints *)
      ur_cons:     n_cons list;   (* num constraints *)
      ur_setcons:  set_cons list; (* set constraints *)
      ur_seqcons:  seq_cons list; (* seq constraints *)
      (* Added keys *)
      ur_news:     SvSet.t;           (* new SVs *)
      ur_newcolvs: (col_kind * colv_info option) SvMap.t;  (* new COLVs *)
      (* Removed keys *)
      ur_remcolvs: col_kind SvMap.t;  (* removed COLVs *)
      (* ??? *)
      ur_paths:    n_path list;
      ur_eqs:      SvPrSet.t; (* equalities, i.e., nodes that can be merged *)
      ur_seqs:     SvPrSet.t; (* equalities, i.e., setvars that can be merged *)
    }


(** Exception used to communicate results *)
(* Localization of the point to unfold
 *   => we may have to generalize this type *)
type unfold_loc =
  | Uloc_main of sv      (* node *)
  | Uloc_sub of sv * sv (* sub-memory contents value and node *)
(* Failure to discover an edge, and request to perform unfolding *)
exception Unfold_request of unfold_loc * unfold_dir
(* Failure to discover an edge, and nothing to be done *)
exception No_edge of string (* message *)

(** Hints for abstract domain operators *)
(* Unary, at Graph level, for local weak abstraction *)
type hint_ug =
    { hug_live: sv Aa_sets.t (* set of live nodes *) }
(* Binary, at Graph level, for join and widening:
 * pairs of live nodes can be treated prioritarily for points-to edges
 * addition *)
type hint_bg =
    { hbg_live: (sv, sv) Aa_maps.t (* set of pairs of live nodes *) }

(* Binary, at Graph level, for join and widening:
 * pairs of dead access can be treated low_priority for points-to edges
 * addition *)
type lint_bg =
    { lbg_dead: (Offs.svo, Offs.svo) Aa_maps.t }

(** Types to be used in the join algorithm *)
(* SV collapsing: merging of several contiguous sequences of bits into one *)
type sv_collapsing =
    { svcol_base:   sv; (* node ID for the base address *)
      svcol_stride: int; (* stride found for the block *)
      svcol_block:  (Bounds.t * Offs.svo * Offs.size) list; (* block struct *)
      svcol_extptr: Offs.OffSet.t (* external pointers to that node *) }
(* Modifications to create/extend sub-memories:
 *  - sub_fold: set of collapsings (for sub-memories)
 *  - sub_new_off:
 *     function to decide admissible sub-memory offsets,
 *     i.e., sub-memory environment offsets to preserve *)
type sv_submem_mod =
    { sub_fold:    sv_collapsing SvMap.t; (* added IDs -> collapse *)
      (* when a pair of offsets cannot be unified, we may create a special
       * offset, that may be used to make a relation, e.g., with sub-memory
       * contents list of elements of the form:
       *       (old offset, new node, new offset) *)
      sub_new_off: (Offs.t * sv * Offs.t) list }
(* All instantiation strcutures wrapped into one to reduce size of code *)
type join_inst =
    { subm_mod: sv_submem_mod;
      sv_inst:  sv_inst;
      set_inst: set_colv_inst;
      seq_inst: seq_colv_inst; }

(** Encoded graphs *)
(* Naming for graph_visu *)
type namer = sv -> string * sv (* node_id -> var_name * var_uid *)
(* Atoms of paths of encoded graph (used as labels) *)
type step =
  (* offset followed exactly once *)
  | Offset of Offs.t
  (* segment followed 0 or more times *)
  | Segment of string * Offs.t list (* (seg_name, allowed_offsets) *)
  (* A baby segment may become a segment in the future,
   * widening of encoded graphs can give birth to baby segments *)
  | Baby_segment of Offs.t list (* allowed_offsets *)

(* A 'renamed_path' is an edge of the encoded graph.
 * It consists in a triple: (src_node, steps, dst_node).
 *
 * A src/dst_node is a SvPrSet.
 * Each pair in this set is made of an offset and a node_id.
 * It means the pointer at given offset in the given node was
 * followed to reach the current node.
 * I.e. a node is renamed according to all it direct predecessors
 * which are roots of the graph. *)

type renamed_path = IntSvPrSet.t * step list * IntSvPrSet.t
(* Encoded grahs *)
type abs_graph =
    renamed_path list (* edges *)
  * IntSvPrSetSet.t   (* nodes *)
  * sv                (* disj. number *)

(** Groups of abs graphs elected to be merged together *)
(* Each group has a representation graph and elements list inside *)
type 'a group = abs_graph * ((abs_graph * 'a) list)
(* Groups *)
type 'a groups = ('a group) list
(* List of encoded graphs *)
type 'a graph_list = (abs_graph * 'a) list

(* Path with a node id [XR? what are the meanings of these fields] *)
type path_arg =
    { sc:     Offs.svo;  (* source node *)
      dt:     Offs.svo;  (* dest node   *)
      pth:    step list; (* path        *) }
(* Encoded graph, as a list of nodes
 * [XR? would it not be more efficient as a map] *)
type abs_graph_arg = path_arg list
(* Graph together with encoded graph *)
type join_arg =
    { (* the encoding of the input graph *)
      abs_gi:   abs_graph_arg option;
      (* the join result of the encodings of two input graphs *)
      abs_go:   abs_graph_arg option; }


(* new encode graph path type *)
type node_path = sv * (step * sv) list * sv
