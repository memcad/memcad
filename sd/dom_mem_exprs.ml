(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: dom_mem_exprs.ml
 **       Memory abstract domain; expressions evaluation layer
 ** Xavier Rival, 2013/08/14 *)
open Data_structures
open Flags
open Lib
open Sv_def

open Ast_sig
open Col_sig
open Dom_sig
open Graph_sig
open Ind_sig
open Nd_sig
open Spec_sig
open Svenv_sig
open Vd_sig

open Apron_utils
open Ast_utils
open Graph_utils
open Nd_utils

open Apron
open Apron_utils
open Sv_utils

(** This module factors some code out of the rest of the shape domain;
 ** it is effectively shared across all dom_mem_low implementations:
 **  - translation of expressions from "int expr" to "n_expr", lvalues,
 **    and conditions
 **  - reading of lvalues
 **  - higher level treatment of assign, guard, sat, alloc *)

(** Error report *)
module Log =
  Logger.Make(struct let section = "dm_xpr__" and level = Log_level.DEBUG end)

let debug_mod = false

(** The build functor *)
module DBuild = functor (D: DOM_MEM_LOW) ->
  (struct
    let module_name = "dom_mem_build"
    let config_fpr fmt (): unit =
      F.fprintf fmt "%s -> %s\n%a" module_name D.module_name D.config_fpr ()

    (** Type of abstract values *)
    type t = D.t (* same as layer below; this layer aims at factoring code *)

    (** Domain initialization to a set of inductive definitions *)
    (* Domain initialization to a set of inductive definitions *)
    let init_inductives (g: SvGen.t) (s: StringSet.t): SvGen.t =
      D.init_inductives g s (* this domain generates no keys *)
    let inductive_is_allowed: string -> bool = D.inductive_is_allowed

    (** Fixing sets of keys *)
    let sve_sync_bot_up: t -> t * svenv_mod = D.sve_sync_bot_up
    let sanity_sv (s: SvSet.t) (x: t): bool = D.sanity_sv s x

    (** Lattice elements *)
    (* Bottom element *)
    let bot: t = D.bot
    let is_bot: t -> bool = D.is_bot
    (* Top element, with empty set of roots *)
    let top: unit -> t = D.top
    (* Pretty-printing *)
    let t_fpri: string -> form -> t -> unit = D.t_fpri
    (* External output *)
    let ext_output: output_format -> string -> namer -> t -> unit =
      D.ext_output

    (* Garbage collection *)
    let gc: sv uni_table -> t -> t = D.gc

    (* Function handling outputs of cell_read/cell_resolve *)
    let cell_read src sz x =
      let f = function
        | x, _, Some cont -> x, cont
        | x, _, None      -> Log.fatal_exn "cell_read fails" in
      List.map f (D.cell_read false src sz x)
    let cell_resolve src sz x =
      let f (x, src, b) =
        if b then x, src else Log.fatal_exn "cell_resolve fails" in
      List.map f (D.cell_resolve src sz x)


    (** Evaluation of l-values and expressions (with possible unfolding) *)

    (* Translation of an expression *)
    let rec tr_expr (ind: string) (x: t) (e: sv expr): (t * n_expr) list =
      let nind = "  "^ind in
      (* value of the variable, augmented of the offset, if any *)
      let onode_2var (n, o): n_expr =
        if Offs.is_zero o then Ne_var n
        else Ne_bin (Texpr1.Add, Ne_var n, Offs.to_n_expr o) in
      match e with
      | Erand ->
          [ x, Ne_rand ]
      | Ecst (Cint i) ->
          [ x, Ne_csti i ]
      | Ecst (Cint64 i) ->
          let ci = Int64.to_int i in let cci = Int64.of_int ci in
          if i != cci then Log.warn "imprecise conversion int64 => int32";
          [ x, Ne_csti ci ]
      | Ecst (Cchar c) ->
          [ x, Ne_cstc c ]
      | Elval tl ->
          List.map (fun (x, v) -> x, onode_2var v) (rd_tlval nind x tl)
      | EAddrOf lv ->
          List.map (fun (x, iv) -> x, onode_2var iv) (tr_tlval nind x lv)
      | Ebin ((Badd | Bsub | Bmul | Bdiv | Bmod) as op, e0, e1) ->
          let t_op = tr_bin_op op in
          List.fold_left
            (fun acc (x, ne0) ->
              List.fold_left
                (fun acc (x, ne1) ->
                  (x, Ne_bin (t_op, ne0, ne1)) :: acc
                ) acc (tr_texpr nind x e1)
            ) [] (tr_texpr nind x e0)
      | Euni (Uneg, e0) ->
          List.map
            (fun (x, ne0) ->
              x, Ne_bin (Texpr1.Sub, Ne_csti 0, ne0))
            (tr_texpr nind x e0)
      | _ ->
          Log.todo_exn "tr_expr: %a" (expr_fpr sv_fpr) e
    and tr_texpr (ind: string) (x: t) (te: sv texpr): (t * n_expr) list =
      if !flag_dbg_dommem_eval then
        Log.force "%s=> tr_texpr<%a>" ind (texpr_fpr sv_fpr) te;
      let r = tr_expr ind x (fst te) in
      if !flag_dbg_dommem_eval then
        Log.force "%s<= tr_texpr<%a> =>    %a" ind (texpr_fpr sv_fpr) te
          (gen_list_fpr "_|_" (fun fmt (_, io) -> n_expr_fpr fmt io) ", ") r;
      r

    (* Translation of an lvalue *)
    and tr_lval (ind: string) (x: t) (lv: sv lval): (t * Offs.svo) list =
      let nind = "  "^ind in
      match lv with
      | Lvar i -> [ x, (i, Offs.zero) ]
      | Lfield (tl0, fld) ->
          let ofld = field_2off fld in
          List.map (fun (x, (i, o)) -> x, (i, Offs.add o ofld))
            (tr_tlval nind x tl0)
      | Lderef e0 ->
          List.map
            (fun (x, ne0) ->
              let v, ee = Nd_utils.decomp_lin ne0 in
              x, (v, Offs.of_n_expr ee)
            ) (tr_texpr nind x e0)
      | Lindex (lv0, e1) ->
          let align =
            match snd lv0 with
            | Tarray (t0,_) -> sizeof_typ t0
            | _ -> Log.fatal_exn "Lindex applied to non array type" in
          if !flag_debug_array_blocks then
            Log.force "%s   align is %d" ind align;
          let l0 = tr_tlval nind x lv0 in
          List.fold_left
            (fun acc (x, (ibase, obase)) ->
              let l1 = tr_texpr nind x e1 in
              List.fold_left
                (fun acc (x, ne1) ->
                  let otot =
                    Offs.add obase (Offs.mul_int (Offs.of_n_expr ne1) align) in
                  (x, (ibase, otot)) :: acc
                ) acc l1
            ) [] l0
    and tr_tlval (ind: string) (x: t) (tl: sv tlval): (t * Offs.svo) list =
      let ofpr fmt x = if !flag_dbg_dommem_evalx then typ_fpr fmt x in
      if !flag_dbg_dommem_eval then
        Log.force "%s=> tr_tlval<%a;%a>" ind ofpr (snd tl)
          (tlval_fpr sv_fpr) tl;
      let r = tr_lval ind x (fst tl) in
      if !flag_dbg_dommem_eval then
        Log.force "%s<= tr_tlval<%a;%a>:  %a" ind ofpr (snd tl)
          (tlval_fpr sv_fpr) tl
          (gen_list_fpr "_|_" (fun fmt (_, io) -> onode_fpr fmt io) ", ") r;
      r

    (* Reading of an lvalue (computation of *lv) *)
    and rd_lval (ind: string) (x: t) (sz: int) (lv: sv lval)
        : (t * Offs.svo) list =
      let res = tr_lval ind x lv in
      List.fold_left
        (fun acc (x, src) -> (cell_read src sz x) @ acc) [ ] res
    and rd_tlval (ind: string) (x: t) (tl: sv tlval)
        : (t * Offs.svo) list =
      let ofpr fmt x = if !flag_dbg_dommem_evalx then typ_fpr fmt x in
      if !flag_dbg_dommem_eval then
        Log.force "%s=> rd_tlval<%a><%a>" ind ofpr (snd tl)
          (tlval_fpr sv_fpr) tl;
      let r = rd_lval ind x (sizeof_typ (snd tl)) (fst tl) in
      if !flag_dbg_dommem_eval then
        Log.force "%s<= rd_tlval<%a><%a>:  %a" ind ofpr (snd tl)
          (tlval_fpr sv_fpr) tl
          (gen_list_fpr "_|_" (fun fmt (_, io) -> onode_fpr fmt io) ", ") r;
      r

    (* Translation of conditions *)
    let tr_tcond (ind: string) (x: t) (tc: sv texpr): (t * n_cons) list =
      let rec aux (x: t) (tc: sv texpr) =
        let te_zero = Ne_csti 0 in
        match fst tc with
        | Ecst (Cint i) -> [ x, Nc_bool (i != 0) ]
        | Euni (Unot, e0) -> (* negation of e0 *)
            begin
              match fst e0 with
              | Elval _ -> (* amounts to e0 == 0 *)
                  let l = tr_texpr ind x e0 in
                  List.map
                    (fun (x, te) -> (x, Nc_cons (Tcons1.EQ, te, te_zero))) l
              | Ecst (Cint i) -> [ x, Nc_bool (i == 0) ]
              | Erand -> Log.todo_exn "tr_tcond: Unot on Erand"
              | Ecst _ -> Log.todo_exn "tr_tcond: Unot on Ecst"
              | Euni (_, _) -> Log.todo_exn "tr_tcond: Unot on Euni"
              | Ebin (op, lhs, rhs) ->
                  begin
                    match op with
                    | Beq -> aux x (Ebin (Bne, lhs, rhs), snd tc)
                    | Bne -> aux x (Ebin (Beq, lhs, rhs), snd tc)
                    | _ ->
                        Log.todo_exn "tr_tcond: Unot on Ebin: %a" bin_op_fpr op
                  end
              | EAddrOf _ -> Log.todo_exn "tr_tcond: Unot on EAddrOf"
            end
        | Ebin ((Bgt | Bge | Beq | Bne) as op, e0, e1) ->
            if (texpr_is_rand_cell e0 && texpr_is_const e1
              || texpr_is_const e0 && texpr_is_rand_cell e1) then
              [ x, Nc_rand ]
            else
              let aop = make_apron_op op in
              let l_left = tr_texpr ind x e0 in
              List.fold_left
                (fun acc (xleft, ne0) ->
                  let l_right = tr_texpr ind xleft e1 in
                  List.fold_left
                    (fun acc (xright, ne1) ->
                      let bchr = xright == xleft in
                      let ne0 =
                        if not bchr then
                          match tr_texpr ind xright e0 with
                          | [ _, ne0 ] -> ne0
                          | _ ->
                              (* this case would mean we need to recompute
                               * the left expression again as the graph changed
                               * due to additional unfolding
                               * (this has not happened yet) *)
                              Log.todo_exn
                                "tr_cond: would need to recompute left side"
                        else ne0 in
                      (xright, Nc_cons (aop, ne0, ne1)) :: acc
                    ) acc l_right
                ) [ ] l_left
        | Elval _ -> (* amounts to e != 0 *)
            let l = tr_texpr ind x tc in
            List.map (fun (x, te) -> x, Nc_cons (Tcons1.DISEQ, te, te_zero)) l
        | Ebin (Band, e0, e1) -> Log.todo_exn "tr_tcond &&"
        | Ebin (Bor, e0, e1) -> Log.todo_exn "tr_tcond ||"
        | _ -> Log.todo_exn "tr_tcond: %a" (texpr_fpr sv_fpr) tc in
      aux x tc (* we may remove the aux function *)

    (* encode of graph *)
    let encode (disj: sv) (l: namer) (x: t) =
      D.encode disj l x

    (** Operations on hints *)
    (* Pretty-printing *)
    let lint_bs_fpr (f: form -> 'a -> unit) (fmt: form) (l: ('a lint_bs) option)
        : unit =
      match l with
      | None -> ()
      | Some l ->
          F.fprintf fmt "dead: %a"
            (Aa_maps.t_fpr "" "\n"
               (fun fmt (i, j) -> F.fprintf fmt "%a => %a" f i f j)) l.lbs_dead
    (* Lint translation *)
    let compute_lint_bin (x0: t) (x1: t) (le: ((sv tlval) lint_bs) option)
        : (Offs.svo lint_bs option) * t * t =
      if debug_mod then Log.info "compute_lint_bin";
      match le with
      | None -> None, x0, x1
      | Some le ->
          let s, x0, x1  =
            Aa_maps.fold
              (fun lv0 lv1 (acc, x0, x1) ->
                try
                  let lv0_u = tr_tlval "" x0 lv0 in
                  let lv1_u = tr_tlval "" x1 lv1 in
                  assert (List.length lv0_u = 1);
                  assert (List.length lv1_u = 1);
                  let x0, on0 = List.hd lv0_u in
                  let x1, on1 = List.hd lv1_u in
                  (Aa_maps.add on0 on1 acc), x0, x1
                with
                | Failure _ -> acc, x0, x1
              ) le.lbs_dead  (Aa_maps.empty, x0, x1) in
          Some { lbs_dead = s }, x0, x1

    (** Comparison and Join operators *)
    (* Checks if the left argument is included in the right one *)
    let is_le (roots: sv bin_table) (colroots: colv_emb)
        (xl: t) (xr: t): bool =
      match D.is_le roots colroots xl xr with
      | Some _ -> true
      | None -> false
    (* Generic comparison (does both join and widening) *)
    let join
        (jk: join_kind) (h: sv hint_bs option)
        (l: ((sv tlval) lint_bs) option)
        (roots: sv tri_table)
        (colroots: (sv, col_kind) tri_map)
        ((xl, jl): t * join_ele) ((xr, jr): t * join_ele): t =
      let l, xl, xr = compute_lint_bin xl xr l in
      if debug_mod then
        Log.info "lint_bin: %a"  (lint_bs_fpr onode_fpr) l;
      let xo, _, _ = D.join jk h l roots colroots (xl, jl) (xr, jr) in
      xo
    (* Directed weakening; over-approximates only the right element *)
    let directed_weakening
        (h: sv hint_bs option)
        (roots: sv tri_table) (colroots: (sv, col_kind) tri_map)
        (xl: t) (xr: t): t =
      let xo, _ = D.directed_weakening h roots colroots xl xr in xo
    (* Unary abstraction, a kind of weaker canonicalization operator *)
    let local_abstract: sv hint_us option -> t -> t =
      D.local_abstract

    (** Transfer functions for the analysis *)
    (* Assignment *)
    let assign (lv: sv tlval) (ex: sv texpr) (x: t): t list =
      (* 1. get ntyp and size from typ *)
      let typ, size =
        match snd ex with
        | Tint (sz, _) -> Ntint, sz
        | Tptr _       -> Ntaddr, abi_ptr_size (* means pointer assignment *)
        | t -> Log.todo_exn "assign: type-to-kind: %a" typ_fpr t in
      (* 2. evaluate l-value *)
      let l = tr_tlval "" x lv in
      (* 3. resolve cells to write on (prepare them for materialization) *)
      let l =
        let f (x, wr_on) = cell_resolve wr_on size x in
        List.map_flatten f l in
      (* 4. write cells *)
      let f ((x, wr_on): t * Offs.svo): t list =
        (* evaluate r-value *)
        let l_ex = tr_texpr "" x ex in
        let f ((x, val_en): t * n_expr): t =
          D.cell_write typ wr_on size val_en x in
        List.map f l_ex in
      List.map_flatten f l

    (* Condition test *)
    let guard (b: bool) (cond: sv texpr) (x: t): t list =
      let rec guard_aux (b: bool) (ct: sv condtree) (x: t): t list =
        match ct, b with
        | Ctrand, _ ->
            [ x ]
        | Ctleaf cnd, _ ->
            let l_cnd = tr_tcond "" x cnd in
            List.map
              (fun (x, conv) ->
                let tcnd =
                  if b then conv
                  else
                    match Nd_utils.neg_n_cons conv with
                    | None -> Log.todo_exn "guard, no negation"
                    | Some nb -> nb in
                D.guard tcnd x
              ) l_cnd
        | Ctland (c0, c1), true | Ctlor (c0, c1), false ->
            List.map_flatten (guard_aux b c1) (guard_aux b c0 x)
        | Ctland (c0, c1), false | Ctlor (c0, c1), true ->
            (guard_aux b c0 x) @ (guard_aux b c1 x) in
      let ct = extract_tree cond in
      let res = guard_aux b ct x in
      res

    (* Checking that a constraint is satisfied *)
    let sat (cond: sv texpr) (x: t): bool =
      let rec sat_aux (c: sv condtree): bool =
        match c with
        | Ctrand ->
            false
        | Ctleaf cnd ->
            List.for_all
              (fun (x, conv) -> D.sat conv x)
              (tr_tcond "" x cnd)
        | Ctland (c0, c1) ->
            sat_aux c0 && sat_aux c1
        | Ctlor (c0, c1) ->
            sat_aux c0 || sat_aux c1 in
      let ct = extract_tree cond in
      sat_aux ct

    (* Checking that several constraints are satisfied *)
    let sat_many (conds: sv texpr list) (x: t): bool =
      List.for_all (fun c -> sat c x) conds

    (* Creation of the memory for a variable *)
    let create_mem_var (typ: typ) (x: t): sv * t =
      (* conversion of types *)
      let fldof_strt (t: typ): int list =
        match t with
        | Tstruct s ->
            List.map (fun ele ->  ele.tsf_size) s.ts_fields
        | _ -> [ sizeof_typ t ] in
      let rec conv_typ (t: typ)
          : (node_attribute * ntyp * int * Offs.t) list =
        match t with
        | Tint (s, _) -> [ Attr_none, Ntint, s, Offs.zero ]
        | Tptr _ -> [ Attr_none, Ntaddr, Flags.abi_ptr_size, Offs.zero ]
        | Tstruct s ->
            List.map
              (fun p ->
                match conv_typ p.tsf_typ with
                | [ a, t, s, o ] ->
                    assert (s = p.tsf_size);
                    a, t, s, Offs.add_int o p.tsf_off
                | _ -> Log.fatal_exn "conv_typ: unsupported nested aggregates"
              ) s.ts_fields
        | Tarray (sub, s) ->
            [ Attr_array (s, Some (fldof_strt sub)),
              Ntraw, sizeof_typ t, Offs.zero ]
        | _ -> Log.fatal_exn "conv_typ: unsupported type: %a" typ_fpr t in
      let lfields = conv_typ typ in
      (* compute node attribute, for sub-mem detection *)
      let nattr =
        match typ with
        | Tarray (sub,_) -> Attr_array (sizeof_typ sub, None)
        | _ -> Attr_none in
      (* add a node for the address and another for the content *)
      let n_address, m0 =
        D.sv_add_fresh ~attr:nattr ~root:true Ntaddr Nstack x in
      let r =
        List.fold_left
          (fun acc (at, gk, size, off) ->
            D.cell_create
              ~attr:at (n_address, off) (Offs.size_of_int size) gk acc
          ) m0 lfields in
      n_address, r

    (** Management of the memory: add/destroy cells *)
    let delete_mem_var: sv -> t -> t = D.cell_delete ~free:false ~root:true

    (* Allocation *)
    let memory (op: sv mem_op) (x: t): t list =
      match op with
      | MO_alloc (lv, ex) ->
          let lv_sz = sizeof_typ (snd lv) in
          assert (lv_sz = abi_ptr_size);
          (* 1. evaluate left value *)
          let l_lv = tr_tlval "" x lv in
          let f ((x, on_addr): t * Offs.svo): t list =
            (* 2. evaluate sizes *)
            let l_sz = tr_texpr "" x ex in
            List.map
              (fun (x, sz) ->
                 (* 3. perform the allocation *)
                 (* create the node representing the contents *)
                 let nc, x = D.sv_add_fresh Ntaddr Nheap x in
                 (* write the cell, corresponding to the allocated zone *)
                 let x =
                   D.cell_create (nc, Offs.zero)
                     (Offs.to_size (Offs.of_n_expr sz)) Ntraw x in
                 D.cell_write Ntaddr on_addr lv_sz (Ne_var nc) x
              ) l_sz in
          List.map_flatten f l_lv
      | MO_dealloc lv ->
          (* 1. read l-value *)
          let l_lv = rd_tlval "" x lv in
          (* 2. materialize the deallocated cells *)
          let l_lv =
            let size = sizeof_typ (snd lv) in
            let f (x, wr_on) = cell_resolve wr_on size x in
            List.map_flatten f l_lv in
          (* 3. deallocate *)
          List.map
            (fun (x, (i, off)) ->
               if not (Offs.is_zero off) then
                 Log.fatal_exn
                   "deallocate applied to an offset not verified be null";
               D.cell_delete ~free:true i x
            ) l_lv

    (** Set/Seq domain *)
    let colv_add (s: string) (ck: col_kind): t -> sv * t * colv_info option =
      D.colv_add_fresh true s ck
    let colv_rem: sv -> t -> t = D.colv_delete

    (** Set domain *)

    (* utility function *)
    let get_one (l: 'a list) (err_msg: string): 'a = match l with
      | [ u ] -> u
      | _ -> Log.fatal_exn "%s" err_msg

    (* Guard and sat functions for set properties *)
    (* set_prepare is common to both set_assume and set_sat *)
    let set_prepare (x: sv tlval setprop) (t: t) (err_msg: string)
      : Offs.svo setprop * t =
      match x with
      | Sp_sub (l, r) -> Sp_sub (l, r), t
      | Sp_mem (e, r) ->
          let t, e = get_one (rd_tlval "" t e) err_msg in
          Sp_mem (e, r), t
      | Sp_emp l -> Sp_emp l, t
      | Sp_euplus (l, e, r) ->
          let t, e = get_one (rd_tlval "" t e) err_msg in
          Sp_euplus (l, e, r), t

    (** Seq domain *)

    (* set_prepare is common to both set_assume and set_sat *)
    let seq_prepare (x: sv tlval seqprop) (t: t) (err_msg: string)
        : Offs.svo seqprop * t =
      let rec seq_expr_prepare (x: sv tlval seqexpr) (t: t)
          : Offs.svo seqexpr * t =
        match x with
        | Qe_empty -> Qe_empty, t
        | Qe_var v -> Qe_var v, t
        | Qe_val v ->
            let t, v = get_one (rd_tlval "" t v) err_msg in
            Qe_val v, t
        | Qe_sort s ->
            let s, t = seq_expr_prepare s t in
            Qe_sort s, t
        | Qe_concat (s1, s2) ->
            let s1, t = seq_expr_prepare s1 t in
            let s2, t = seq_expr_prepare s2 t in
            Qe_concat (s1, s2), t
      in
      match x with
      | Qp_equal (s1, s2) ->
          let s1, t = seq_expr_prepare s1 t in
          let s2, t = seq_expr_prepare s2 t in
          Qp_equal (s1, s2), t

    (** Isle/Join operator specific directives *)
    (* Mark node as prio *)
    let mark_prio (lv: sv tlval) (x: t): t =
      let l_lv = rd_tlval "" x lv in
      match l_lv with
      | [x, lv] -> D.mark_prio lv x
      | _ -> Log.todo_exn "%s mark_prio to implement\n" __LOC__

    (** Unfolding, assuming and checking inductive edges *)

    (* Unfold *)
    let ind_unfold (ud: unfold_dir) (lv: sv tlval) (x: t): t list =
      let l_lv = rd_tlval "" x lv in
      List.map_flatten (fun (x, on) -> D.ind_unfold ud on x) l_lv

    (* Utility function for ind_assume and ind_check
     *  translation of ind_call, assuming no cell need unfolding to be
     *  materialized (this is reasonable, as ind_assume, ind_check should
     *  apply to basic variables / access paths) *)
    let tr_ind_call (x: t) (ic: sv tlval gen_ind_call)
        : t * Offs.svo gen_ind_call =
      let tr_list f l x =
        List.fold_left
          (fun (x, accl) a ->
            let x, b = f a x in x, b :: accl
          ) (x, [ ]) (List.rev l) in
      let f_int_tlval (lv: sv tlval) (x: t): t * Offs.svo =
        match rd_tlval "" x lv with
        | [ y, on ] -> y, on
        | _ -> Log.fatal_exn "tr_ind_call: should not have to unfold" in
      let f_gen_ind_intp (ii: sv tlval gen_ind_intp) (x: t)
          : t * Offs.svo gen_ind_intp =
        match ii with
        | Ii_const n -> x, Ii_const n
        | Ii_lval lv -> let y, on = f_int_tlval lv x in y, Ii_lval on in
      let f_gen_ind_pars (p: sv tlval gen_ind_pars) (x: t)
          : t * Offs.svo gen_ind_pars =
        let x, n_ic_ptr = tr_list f_int_tlval p.ic_ptr x in
        let x, n_ic_int = tr_list f_gen_ind_intp p.ic_int x in
        x, { ic_ptr = n_ic_ptr ;
             ic_int = n_ic_int ;
             ic_set = p.ic_set ;
             ic_seq = p.ic_seq } in
      let x, ind_pars =
        match ic.ic_pars with
        | Some p -> let x, p = f_gen_ind_pars p x in x, Some p
        | None   -> x, None in
      x, { ic_name = ic.ic_name;
           ic_pars = ind_pars }

    (** Check and assume constructions *)
    let prep_log_form (s: string) (x: t) (op: memh_log_form)
        : meml_log_form * t =
      match op with
      | SL_set sp ->
          let err_msg = "set_"^s^": requires further unfolding" in
          let sp, x = set_prepare sp x err_msg in
          SL_set sp, x
      | SL_seq qp ->
          let err_msg = "seq_"^s^": requires further unfolding" in
          let qp, x = seq_prepare qp x err_msg in
          SL_seq qp, x
      | SL_ind (ic, lv) ->
          (* For the sake of simplicity, we assume that no further unfolding
           * is needed in order to materialize cells from which the edge
           * should be added, and for the parameters *)
          let err_msg = "ind_"^s^": requires further unfolding" in
          let x, lv = get_one (rd_tlval "" x lv) err_msg in
          let x, ic = tr_ind_call x ic in
          SL_ind (ic, lv), x
      | SL_seg (ic, lv, ic_e, lv_e) ->
          let err_msg = "seg_"^s^": requires further unfolding" in
          let x, lv = get_one (rd_tlval "" x lv) err_msg in
          let x, ic = tr_ind_call x ic in
          let x, lv_e = get_one (rd_tlval "" x lv_e) err_msg in
          let x, ic_e = tr_ind_call x ic_e in
          SL_seg (ic, lv, ic_e, lv_e), x
      | SL_array -> SL_array, x
    let assume (op: memh_log_form) (x: t): t =
      let op, x = prep_log_form "assume" x op in D.assume op x
    let check (op: memh_log_form) (x: t): bool =
      let op, x = prep_log_form "check" x op in D.check op x

    (** Construction from formulas and checking of formulas *)
    let from_s_formula: colvar StringMap.t -> s_formula -> t * s_pre_env =
      D.from_s_formula
    let sat_s_formula (sym_to_sv: sv StringMap.t)
        (cvs: (sv * col_kind * colvar) StringMap.t)
        (f: s_formula) (t: t): bool =
      D.sat_s_formula sym_to_sv cvs f t
  end: DOM_MEM_EXPRS)


(** Timer instance of the dom_mem_exprs domain (act as a functor on top
 ** of the domain itself) *)
module Dom_mem_exprs_timing = functor (D: DOM_MEM_EXPRS) ->
  (struct
    module T = Timer.Timer_Mod( struct let name = "Mem_exprs" end )
    type t = D.t
    let module_name = "dom_mem_exprs_timing"
    let config_fpr = T.app2 "config_fpr" D.config_fpr
    let init_inductives = T.app2 "init_inductives" D.init_inductives
    let inductive_is_allowed = T.app1 "ind_is_allowed"D.inductive_is_allowed
    let sve_sync_bot_up = T.app1 "sve_sync_bot_up" D.sve_sync_bot_up
    let sanity_sv: SvSet.t -> t -> bool = T.app2 "sanity_sv" D.sanity_sv
    let bot = D.bot
    let is_bot = T.app1 "is_bot" D.is_bot
    let top = T.app1 "top" D.top
    let t_fpri = T.app3 "t_fpri" D.t_fpri
    let ext_output = T.app4 "ext_output" D.ext_output
    let gc = T.app2 "gc" D.gc
    let is_le = T.app4 "is_le" D.is_le
    let join = T.app7 "join" D.join
    let directed_weakening = T.app5 "weaken" D.directed_weakening
    let local_abstract = T.app2 "local_abstract" D.local_abstract
    let assign = T.app3 "assign" D.assign
    let guard = T.app3 "guard" D.guard
    let encode = T.app3 "encode" D.encode
    let sat = T.app2 "sat" D.sat
    let create_mem_var = T.app2 "create_mem_var" D.create_mem_var
    let delete_mem_var = T.app2 "delete_mem_var" D.delete_mem_var
    let memory = T.app2 "memory" D.memory
    let colv_add = T.app3 "colv_add" D.colv_add
    let colv_rem = T.app2 "colv_rem" D.colv_rem
    let assume = T.app2 "assume" D.assume
    let check = T.app2 "check" D.check
    let mark_prio = T.app2 "mark_prio" D.mark_prio
    let ind_unfold = T.app3 "ind_unfold" D.ind_unfold
    let from_s_formula = T.app2 "from_s_formula" D.from_s_formula
    let sat_s_formula = T.app3 "sat_s_formula" D.sat_s_formula
  end: DOM_MEM_EXPRS)
