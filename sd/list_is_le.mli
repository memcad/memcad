(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: list_is_le.mli
 **       Inclusion checking for the list domain
 ** Xavier Rival, 2014/03/07 *)
open Data_structures
open Sv_def

open Col_sig
open Set_sig
open Seq_sig
open List_sig
open Nd_sig
open Vd_sig

(** Full inclusion checking, used in the domain functor *)
val is_le:
    lmem                         (* left input *)
  -> vsat_l:vsat                 (* satisfiability test function, left arg *)
  -> lmem                        (* right input *)
  -> Graph_sig.node_emb          (* injection from right SV into left SV *)
  -> Col_sig.colv_emb            (* injection from right COLV to left COLV *)
  -> List_sig.is_le_res          (* left remainder, possibly right segment *)

(** Partial inclusion checking function, used for join *)
val is_le_weaken_check:
    lmem                         (* left input *)
  -> SvSet.t                     (* no unfold 0 *)
  -> SvSet.t                     (* segment end(s), if any *)
  -> vsat_l:vsat                 (* satisfiability test function, left arg *)
  -> lmem                        (* right input *)
  -> Graph_sig.node_emb          (* injection from right SV into left SV *)
  -> Col_sig.colv_emb            (* injection from right COLV to left COLV *)
  -> List_sig.is_le_res

(** Partial inclusion checking function, used for join *)
val is_le_weaken_guess:
    stop: sv Aa_sets.t option    (* optional stop nodes *)
  -> lmem                        (* left input *)
  -> SvSet.t                     (* no unfold 0 *)
  -> SvSet.t                     (* segment end(s), if any *)
  -> vsat_l:vsat                 (* satisfiability test function, left arg *)
  -> lmem                        (* right input *)
  -> Graph_sig.node_emb          (* injection from right SV into left SV *)
  -> Col_sig.colv_emb            (* injection from right COLV to left COLV *)
  -> List_sig.is_le_weaken_guess
