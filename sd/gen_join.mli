(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: gen_join.mli
 **       Generic elements used in the join algorithm
 ** Xavier Rival, 2015/08/21 *)
open Data_structures
open Sv_def

open Graph_sig
open Nd_sig

open Gen_dom

(** Sided algorithms support *)
(* Display *)
val side_fpr: form -> side -> unit
(* Swap side *)
val other_side: side -> side
(* Side element extraction *)
val sel_side: side -> 'a * 'a -> 'a
val get_sides: side -> 'a * 'a -> 'a * 'a

(** Kinds of rules in the join algorithm *)
type rkind =
  | Rpp | Rii         (* matching of pairs of pt/inductive edges *)
  | Rsegintro of side (* introduction of a segment *)
  | Rsegext           (* extension of a segment *)
  | Riweak            (* inductive in the left; weaken in the right graph *)
  | Rweaki            (* inductive in the right; weaken in the left graph *)
  | Rindintro         (* introduction of an inductive *)
  | Rsplitind of side (* seperate an inductive edge by introducing a segment *)
  | Rsegext_ext       (* extension of segments on both sides *)

(** Status, with set of applicable rules *)
type instances
type rules

(** Utilities and Pretty-printing *)
(* Empty set of applicable rules *)
val empty_rules: rules
(* Pretty-printing *)
val rkind_fpr: form -> rkind -> unit
val rules_fpr: form -> rules -> unit

(** Strategy function, returning the next applicable rule *)
(* current strategy:
 *  1. pt-prio
 *  2. ind-ind
 *  3. seg-intro
 *  4. seg-ext
 *  5. weak-ind, ind-weak
 *  6. pt-pt
 *  7. ind-intro *)
val rules_next: rules -> (rkind * (sv * sv) * rules) option

(** Collecting applicable rules *)
val collect_rules_sv_gen:
    (sv * sv -> bool) (* whether pt rules are prioritary (init) *)
  -> bool             (* whether should be extension as segments*)
  -> bool             (* whether should be seg intro *)
    -> node_relation        (* node relation *)
      -> sv -> region_kind  (* left node  *)
        -> sv -> region_kind  (* right node *)
          -> rules -> rules

(** Invalidation of rules that were performed or disabled, by the application
 ** of another rule *)
(* Kinds of elements to invalidate *)
type invalid =
  | Ipt                (* a points to region *)
  | Iind               (* an inductive predicate *)
  | Inone              (* nothing *)
  | Iblob of SvSet.t  (* a memory blob *)
  | Isiblings          (* siblings of a node *)
(* Case of a local rule *)
val invalidate_rules:
    sv -> sv (* nodes *)
      -> invalid -> invalid (* what is becoming invalid *)
        -> rules -> rules

(** Extraction of mappings *)
val extract_mappings:
    SvSet.t -> SvSet.t
      -> Graph_sig.node_relation
        -> unit node_mapping * unit node_mapping

(** Addition of rules *)
val rules_add_weakening: sv * sv -> rules -> rules
val rules_add_splitind_l: sv * sv -> rules -> rules
val rules_add_splitind_r: sv * sv -> rules -> rules

(** Auxiliary function used both in Graph and in List *)
(* Computation of siblings for the inductive edge split rule *)
val calc_split_ind_dest_o: SvSet.t SvMap.t -> sv -> sv -> sv
