(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: printable_graph.ml
 **       Graph meant to be printed out
 ** Francois Berenger, 2016/11/16 *)
open Data_structures
open Lib
open Sv_def

open Nd_utils
open Sv_utils

module Node = struct
  type kind = Plain | Ind_start | Seg_start

  type t = { id:        sv;
             name:      string;
             kind:      kind;
             nb_fields: int;
             typ:       ntyp }

  let create id name kind nb_fields typ: t =
    begin
      match kind with
      | Ind_start | Seg_start -> assert (nb_fields = 1)
      | _ -> ()
    end;
    { id; name; kind; nb_fields; typ }

  (* graphviz dot format string output *)
  let to_dot_fpr (fmt: form) (node: t): unit =
    let maybe_bold =
      match node.kind with
      | Ind_start -> "fontname=\"bold\" style=\"bold\""
      | _ -> "" in
    let pp_fields fmt () =
      F.fprintf fmt "<f0> %s %a" node.name ntyp_fprs node.typ;
      for i = 1 to node.nb_fields - 1 do
        (* fields are assumed a 32 bits size (4 bytes) *)
        F.fprintf fmt " | <f%d> +%d" (4*i) (4*i)
      done in
    F.fprintf fmt "\"%a\" [%s label=\"%a\" shape=\"record\"];\n"
      nsv_fpr node.id maybe_bold pp_fields ()
end

module Edge =
  struct
    type kind =
      | Simple
      | Segment

    type t =
        { src_nid: sv;  (* source nid *)
          src_off: int; (* offset in source node *)
          dst_nid: sv;  (* destination nid *)
          dst_off: int; (* offset in destination node *)
          label: string;
          kind: kind }

    let create src_nid src_off dst_nid dst_off label kind: t =
      { src_nid; src_off; dst_nid; dst_off; label; kind }

    (* graphviz dot format string output *)
    let to_dot_fpr (fmt: form) (edge: t): unit =
      F.fprintf fmt "\"%a\":f%d -> \"%a\":f%d [ label = \"%s\" %s];\n"
        nsv_fpr edge.src_nid edge.src_off
        nsv_fpr edge.dst_nid edge.dst_off edge.label
        (match edge.kind with | Simple -> "" | Segment -> "penwidth = 2.0 ")
  end

module Graph =
  struct

    open Node
    open Edge

    type t =
        { nodes: Node.t list; (* all nodes *)
          edges: Edge.t list; (* all edges *)
          title: string }

    let create nodes edges title =
      { nodes; edges; title }

        (* graphviz dot format string output *)
    let to_dot_fpr (fmt: form) (graph: t): unit =
      (* header *)
      F.fprintf fmt "digraph g {\n";
      F.fprintf fmt "labelloc=\"t\";\n";
      F.fprintf fmt "label=\"%s\";\n" graph.title;
      F.fprintf fmt "graph [ rankdir = \"LR\" ];\n";
      (* nodes *)
      List.iter (Node.to_dot_fpr fmt) graph.nodes;
      (* edges *)
      List.iter (Edge.to_dot_fpr fmt) graph.edges;
      (* footer *)
      F.fprintf fmt "}\n" (* end of digraph *)

    (* all node ids directly connected to 'nid' *)
    let direct_successors (nid: sv) (graph: t): SvSet.t =
      List.fold_left
        (fun acc edge ->
          if edge.src_nid = nid then SvSet.add edge.dst_nid acc
          else acc
        ) SvSet.empty graph.edges

    (* list all connected components of 'g' *)
    let connected_components (graph: t): SvSet.t list =
      let all_edges = graph.edges in
      let is_connected s1 s2 =
        SvSet.inter s1 s2 <> SvSet.empty in
      let may_merge s1 s2 =
        if is_connected s1 s2 then
          SvSet.union s1 s2
        else
          s2 in
      let rec loop = function
        | [] -> []
        | x :: xs ->
            if List.exists (is_connected x) xs then
              loop (List.map (may_merge x) xs)
            else
              x :: loop xs in
      let init =
        List.fold_left
          (fun acc edge ->
            (SvSet.of_list [edge.src_nid; edge.dst_nid]) :: acc
          ) [] all_edges in
      loop init

    let filter_nodes nodes nids_to_keep =
      List.filter
        (fun node ->
          SvSet.mem node.id nids_to_keep
        ) nodes

    let neg_filter_nodes nodes nids_to_discard =
      List.filter
        (fun node ->
          not (SvSet.mem node.id nids_to_discard)
        ) nodes

    let filter_edges edges nids_to_keep =
      List.filter
        (fun edge ->
          SvSet.mem edge.src_nid nids_to_keep &&
          SvSet.mem edge.dst_nid nids_to_keep
        ) edges

    let connected_component (roots: SvSet.t) (graph: t): t =
      let comps = connected_components graph in
      let selected_comps =
        List.filter
          (fun comp ->
            SvSet.inter roots comp <> SvSet.empty
          ) comps in
      let all_nodes = graph.nodes in
      let all_edges = graph.edges in
      let nids_to_keep =
        List.fold_left SvSet.union SvSet.empty selected_comps in
      let remaining_nodes = filter_nodes all_nodes nids_to_keep in
      let remaining_edges = filter_edges all_edges nids_to_keep in
      create remaining_nodes remaining_edges graph.title

    (* return a new graph where nodes which are not descendants
     * of any of 'root_vars' were removed *)
    let successors_only (roots: SvSet.t) (graph: t): t =
      let all_nodes = graph.nodes in
      let all_edges = graph.edges in
      let rec loop to_visit visited =
        if SvSet.is_empty to_visit then visited
        else
          let curr, remaining = SvSet.pop_min to_visit in
          let nexts = direct_successors curr graph in
          let to_visit' = SvSet.diff (SvSet.union remaining nexts) visited in
          let visited' = SvSet.add curr visited in
          loop to_visit' visited' in
      let nids_to_keep = loop roots SvSet.empty in
      let remaining_nodes = filter_nodes all_nodes nids_to_keep in
      let remaining_edges = filter_edges all_edges nids_to_keep in
      create remaining_nodes remaining_edges graph.title

    (* remove from 'graph' all leaves which are not inductive edges *)
    let cut_ordinary_leaves (graph: t): t =
      let all_nodes = graph.nodes in
      let all_edges = graph.edges in
      let all_nids =
        SvSet.of_list
          (List.map (fun node -> node.id) all_nodes) in
      let nid2node =
        List.fold_left
          (fun acc node ->
            SvMap.add node.id node acc
          ) SvMap.empty all_nodes in
      let edge_start_nids =
        SvSet.of_list
          (List.map (fun edge -> edge.src_nid) graph.edges) in
      let all_leaves = SvSet.diff all_nids edge_start_nids in
      let nids_to_remove =
        SvSet.filter
          (fun nid ->
            let node = SvMap.find nid nid2node in
            node.kind = Node.Plain
          ) all_leaves in
      let remaining_nodes = neg_filter_nodes all_nodes nids_to_remove in
      let remaining_edges =
        List.filter
          (fun edge -> not (SvSet.mem edge.dst_nid nids_to_remove))
          all_edges in
      create remaining_nodes remaining_edges graph.title
  end
