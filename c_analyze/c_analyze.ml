(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: c_analyze.ml
 **       analysis of C code (very restricted analysis)
 ** Xavier Rival, 2011/07/17 *)
open Lib
open Data_structures
open Flags

open Ast_sig
open Ast_utils
open C_sig
open C_analysis_sig
open Disj_sig
open Dom_sig
open Nd_sig
open Vd_sig

open C_utils

(* Current limitations:
 *  - static variables not handled
 *  - analysis of only a main function
 *  - does not pay attention to parameters
 *  - does not analyze function calls in expressions
 *    (we may leave this limitation forever, and use a kind of
 *     equivalent to the "Graph" in Astree)
 *  - consider merging both sides of the join hints
 *)

(** Error report *)
module Log =
  Logger.Make(struct let section = "c_ana___" and level = Log_level.DEBUG end)
let debug = true


(** The abstract interpreter functor:
 **  - M: DOM_C: contains the whole abstract domain
 **  - P: PRE_ANALYSIS: pre-analysis liveness parameters *)
module Make = functor (M: DOM_C) -> functor (P: PRE_ANALYSIS) ->
  (struct
    (** status of the analysis *)
    type t = M.t (* current abstraction *)

    (** Gathering statistics *)
    let stats_accu: Statistics.prog_stats ref = ref IntMap.empty
    let add_stats (loc: int) (x: t): unit =
      let l = try IntMap.find loc !stats_accu with Not_found -> [] in
      stats_accu := IntMap.add loc (M.get_stats x :: l) !stats_accu
    (* Analysis step counter (for debug) *)
    let step = ref 0
    (* Utilities for tests *)
    let boolres_fpr (fmt: form) (b: bool): unit =
      F.fprintf fmt "%s" (if b then "verified" else "failed")


    (** Analyzer context *)
    (* Note: we might move this to dom_c, but we keep it here for now, as
     *       it needs be accessed in the interpreter directly to deal with
     *       recursion *)
    type ctxt =
        { ct_funcalls:  StringSet.t ; (* functions currently on the stack *)
          ct_curfun:    string ;      (* current function name *)
          ct_currec:    bool ;        (* is the current call recursive ? *)
          ct_recdepth:  int  ;        (* iters when analyzing a rec. fun *)
          ct_curfix:    bool ;        (* did we reach a fixpoint ? *)
          ct_entry:     t StringMap.t (* abstract states (@ fun. entries) *) }
    let ctxt_empty: ctxt =
      { ct_funcalls = StringSet.empty ;
        ct_curfun   = "" ;
        ct_currec   = false ;
        ct_recdepth = 0 ;
        ct_curfix   = false ;
        ct_entry    = StringMap.empty }
    let ctxt_call (ct: ctxt) (f: string): ctxt =
      if StringSet.mem f ct.ct_funcalls then
        { ct with ct_currec = true }
      else
        { ct with
          ct_funcalls = StringSet.add f ct.ct_funcalls ;
          ct_curfun   = f }
    let ctxt_entry_get (ct: ctxt) (fname: string): t =
      try StringMap.find fname ct.ct_entry
      with Not_found -> Log.fatal_exn "rec-call, entry retrieval failed"
    let ctxt_entry_add (ct: ctxt) (fname: string) (x: t): ctxt =
      { ct with ct_entry = StringMap.add fname x ct.ct_entry }


    (** Over-approximation of a least-fixpoint *)
    (* Abstract iterations status *)
    type iter_status =
        { is_unroll: int; (* number of unroll iterations left *)
          is_dweak:  int; (* number of directed weakening iterations left *)
          is_join:   int; (* number of join iterations left *)
          is_total:  int; (* number of iterations performed so far *) }
    let iter_status_fpr (fmt: form) (is: iter_status): unit =
      F.fprintf fmt "<%d> (unroll: %d, dweak: %d, join: %d)"
        is.is_total is.is_unroll is.is_dweak is.is_join

    (* Retrieval of live variables at while statement head *)
    let get_live (lc: int): VarSet.t * (var tlval) list =
      let info =  P.live_at lc in
      VarMap.fold
        (fun var paths  (l, d) ->
          VarSet.add var l,
          PathSet.fold (fun p acc -> tr_c_lval (snd p) :: acc) paths d
        ) info (VarSet.empty, [])

    (** Exporting local invariant *)
    let do_export (i: int) (x: t) (vars: string list) (opts: string list): t =
      let base = Printf.sprintf "%s-%d" (out_prefix ()) i in
      M.ext_output (Out_dot (vars, opts)) base x

    (* Computation of an abstract post-fixpoint *)
    let alfp (loc: loc)
        (h: hint_be)
        (l: var lint_be) (* hint at that loop head point *)
        (n_unrolls: int) (* whether or not to perform unroll iterations *)
        (f: t -> t) (x: t): t =
      (* end of the lfp computation: joins unrolls with current *)
      let join_unroll (acc: t option) (x: t): t =
        match acc with
        | None -> x
        | Some y -> M.join (Some h) (Some l) x y in
      (* end of a new unroll iteration, and join with accumulator *)
      let add_unroll (acc: t option) (x: t): t option =
        Some (join_unroll acc x) in
      let rec aux
          (is: iter_status)
          (acc: t option) (* accumulator for unroll iterations *)
          (xn: t): t =
        if !Flags.flag_debug_iter then
          Log.force "ALFP[@%d] Iteration %a; status:\n%a"
            loc iter_status_fpr is (M.t_fpri "    ") xn;
        let fxn = f xn in
        if !Flags.flag_debug_iter then
          Log.force "ALFP[@%d] Output %a\n%a"
            loc iter_status_fpr is (M.t_fpri "  ") fxn;
        (* Testing for stability does not seem to improve on timing *)
        if is.is_unroll > 0 then
          (* unroll iteration:
           *  - currently added to the list of unrolls;
           *  - keep iterating with current *)
          aux { is with
                is_unroll = is.is_unroll - 1;
                is_total  = is.is_total  + 1 } (add_unroll acc xn) fxn
        else if is.is_dweak > 0 then
          (* directed weakening iteration:
           *  - currently added to the list of unrolls
           *  - keep iterating with weakened current
           * NOTE: we may later keep the weakened current instead of current *)
          aux { is with
                is_dweak  = is.is_dweak - 1;
                is_total  = is.is_total + 1 } (add_unroll acc xn)
            (M.directed_weakening (Some h) xn fxn)
        else
          let opname, is =
            if is.is_join > 0 then
              Jjoin, { is with is_join = is.is_join - 1 }
            else Jwiden, is in
          (* get disjunctions kept from widening, and add them to acc *)
          let xnp, k_invar =
            match opname with
            | Jjoin  -> M.join  (Some h) (Some l) xn fxn, None
            | Jwiden -> M.widen (Some h) (Some l) xn fxn
            | Jdweak ->
                Log.fatal_exn "binary operator should be widen or join" in
          let acc =
            match k_invar with
            | None -> acc
            | Some kv -> add_unroll acc kv in
          let xnp =
            if !Flags.flag_enable_ext_export_all then
              do_export loc xnp [] []
            else xnp in
          let prf = "ANALYSIS,ALFP" in
          if !Flags.flag_debug_iter then
            Log.force
              "%s[@%d] Iter performed %a:\nXn:\n%a\nF(Xn):\n%a\nX(n+1):\n%a"
              prf loc Dom_utils.join_kind_fpr opname (M.t_fpri "  ") xn
              (M.t_fpri "  ") fxn (M.t_fpri "  ") xnp;
          let le = M.is_le xnp xn in
          if !Flags.flag_debug_iter then
            Log.force "%s[@%d] Iter status: %b" prf loc le;
          if le then
            begin
              Log.info
                "%s[@%d] Iter status: stable after %d iters, \
                 end of iteration sequence" prf loc (is.is_total + 1);
              if not !Flags.very_silent_mode then
                Log.force " %s,Fixpoint:\n%a" prf (M.t_fpri "   ") xnp;
              join_unroll acc xnp
            end
          else
            begin
              Log.info "%s[@%d] Iter status: not stable, keep iterating..."
                prf loc;
              aux { is with is_total = is.is_total + 1 } acc xnp
            end in
      aux { is_unroll = n_unrolls;
            is_dweak  = !Flags.dweak_iters;
            is_join   = !Flags.join_iters;
            is_total  = 0 } None x


    (** Some utilities *)
    (* Preparation of an inductive definition, for underlying domain *)
    let prepare_inductive (ctxt: string) (l: c_lval) (oiname: string option)
        (op: c_memcad_iparams option): var tlval gen_ind_call =
      let p_u =
        Option.map
          (fun (p: c_memcad_iparams) ->
            let tr_ptr_param = function
              | Cmp_const i ->
                  Log.fatal_exn "numeric used as a pointer parameter"
              | Cmp_lval lv -> tr_c_lval lv in
            let tr_int_param = function
              | Cmp_const i -> Ii_const i
              | Cmp_lval lv -> Ii_lval (tr_c_lval lv) in
            { ic_ptr = List.map tr_ptr_param p.mc_pptr;
              ic_int = List.map tr_int_param p.mc_pint;
              ic_set = List.map tr_c_colvar p.mc_pset;
              ic_seq = List.map tr_c_colvar p.mc_pseq; }
          ) op in
      let iname =
        match oiname with
        | None ->
            let ctname =
              match l.clt with
              | Ctnamed nt -> nt.cnt_name
              | _ -> Log.fatal_exn "%s: not a named type" ctxt in
            Ind_utils.indname_find ctname
        | Some s -> s in
      { ic_name = iname ;
        ic_pars = p_u }
    (* get the name of the struct, if applicable *)
    let get_struct_name (t: c_type): string option =
      match t with
      | Ctstruct (Cad_def c_agg) -> c_agg.cag_name
      | _ -> None
    (* get the name of the c_aggregate, if applicable *)
    let get_c_aggregate_name (l: c_lval): string option =
      match l.clt with
      | ((Ctstruct _) as cts)
      | Ctptr (Some cts) -> get_struct_name cts
      | Ctnamed _
      | Ctint
      | Ctchar
      | Ctvoid
      | Ctunion _
      | Ctptr _
      | Ctarray _
      | Ctstring _ -> None
    let correct_inductive_name (l: c_lval) (oiname: string option)
        : string option =
      if not !Flags.auto_ind then oiname
      else
        match oiname with
        | None -> None
        | Some prev_name ->
            let maybe_new_name = get_c_aggregate_name l in
            match maybe_new_name with
            | None -> oiname
            | Some new_name ->
                Log.warn "correct_inductive_name: ind. %s renamed to %s"
                  prev_name new_name;
                maybe_new_name
    (* insertion of numerical preconditions *)
    let prepare_num_expr ((c, l, v): c_num_expr) =
      let c_num_right_to_c_expr: c_num_right-> c_expr = function
        | Cnr_const i -> { cek = Ceconst (Ccint i) ;
                           cep = 0;
                           cet = Ctint             }
        | Cnr_lval clv -> { cek = Celval clv ;
                            cep = 0;
                            cet = clv.clt    } in
      let e1 = c_num_right_to_c_expr (Cnr_lval l) in
      let e2 = c_num_right_to_c_expr v in
      tr_c_exprk_winfo(Cebin (c, e1, e2)), Tint (Flags.abi_int_size, Tsigned)


    (** Some transfer functions *)
    (* Type of logical assertions *)
    type log_op =
      | LAssume   (* for assume operations (inductives, segments...) *)
      | LVerify   (* for verification operations *)
    (* Pretty-printing *)
    let string_of_log_op = function
      | LAssume -> "assume"
      | LVerify -> "verify"
    (* Analysis of an assume of numerical expressions *)
    let a_assume_num_exprs (num_exprs: c_num_expr list) (x: t): t =
      let assume_num_expr x' num_expr =
        let tc = prepare_num_expr num_expr in
        let no_line_number = 0 in
        M.assert_one "c-analyze: assume" (M.guard no_line_number true tc x') in
      List.fold_left assume_num_expr x num_exprs
    (* Analysis of an expression assertion *)
    let a_assert_c_expr (c: c_expr) (line: int) (x: t): t =
      let tc = map_texpr (fun v -> Var v) (tr_c_expr c) in
      let b_ok = M.sat tc x in
      let name = Printf.sprintf "ASSERTION @%d (assert)" line in
      let success ( ) =
        Log.info "%s verified" name;
        Lib.log_assert true;
        x in
      let failure x_not =
        Log.error "%s failed" name;
        Log.error "Over-approx of failure:\n%a" (M.t_fpri "     ") x_not;
        Lib.log_assert false;
        M.assert_one "assert-fail" (M.guard line true tc x) in
      if b_ok then success ( )
      else
        let t_not_c =
          c |> c_expr_neg |> tr_c_expr |> map_texpr (fun v -> Var v) in
        let x_not = M.assert_one "assert" (M.guard line true t_not_c x) in
        if M.is_bot x_not then success ( )
        else failure x_not
    (* Analysis of a list of numerical assertions *)
    let a_assert_num_exprs (num_exprs: c_num_expr list) (line: int) (x: t): t =
      List.fold_left
        (fun x (bo, lv0, r) ->
          let c =
            let e0 = { cek = Celval lv0 ;
                       cet = Ctint ;
                       cep = line } in
            let e1k =
              match r with
              | Cnr_const i -> Ceconst (Ccint i)
              | Cnr_lval lv1 -> Celval lv1 in
            let e1 = { cek = e1k ;
                       cet = Ctint ;
                       cep = line } in
            { cek = Cebin (bo, e0, e1) ;
              cet = Ctint ;
              cep = line } in
          a_assert_c_expr c line x
        ) x num_exprs
    (* Analysis of a verification of a set predicate *)
    let a_check_setexprs (line: int) (sp: c_memcad_setexpr list) (t: t): t =
      let b_res =
        List.for_all (fun x -> M.check t (SL_set (tr_c_setexpr x))) sp in
      Log.info "ASSERTION @%d (structural) %a" line boolres_fpr b_res;
      Lib.log_assert b_res;
      t
    (* Analysis of a verification of a seq predicate *)
    let a_check_seqexprs (line: int) (sp: c_memcad_seqexpr list) (t: t): t =
      let b_res =
        List.for_all (fun x -> M.check t (SL_seq (tr_c_seqexpr x))) sp in
      Log.info "ASSERTION @%d (structural) %a" line boolres_fpr b_res;
      Lib.log_assert b_res;
      t
    (* Analysis of an add or check an inductive definition *)
    let a_inductive (dir: log_op) (line: int) (l: c_lval)
        (oiname: string option) (op: c_memcad_iparams option) (x: t): t =
      let tl = tr_c_lval l in
      let oiname0 = correct_inductive_name l oiname in
      let msg = string_of_log_op dir ^ "_inductive" in
      let ic = prepare_inductive msg l oiname0 op in
      begin
        try
          let iname = ic.ic_name in
          let ind = Ind_utils.ind_find iname in
          let chk = C_ind_infer.test_compat_ind l.clt ind in
          Log.info "INDUCTIVE TEST: %b\n" chk
        with e ->
          Log.info "INDUCTIVE TEST: FAILED (%s)\n" (Printexc.to_string e)
      end;
      match dir with
      | LAssume -> M.assume x (SL_ind (ic, tl))
      | LVerify ->
          let b_res = M.check x (SL_ind (ic, tl)) in
          Log.info "ASSERTION @%d (structural) %a" line boolres_fpr b_res;
          Lib.log_assert b_res;
          x
    (* Analysis of an add or check an inductive segment *)
    let a_segment (dir: log_op) (line: int) (l: c_lval) (oiname: string)
        (op: c_memcad_iparams option) (l_e: c_lval) (oiname_e: string)
        (op_e: c_memcad_iparams option)
        (x: t): t =
      let tl = tr_c_lval l in
      let tl_e = tr_c_lval l_e in
      let msg = string_of_log_op dir ^ "_segment" in
      let ic = prepare_inductive msg l (Some oiname) op in
      let ic_e = prepare_inductive msg l_e (Some oiname_e) op_e in
      match dir with
      | LAssume -> M.assume x (SL_seg (ic, tl, ic_e, tl_e))
      | LVerify ->
          let b_res = M.check x (SL_seg (ic, tl, ic_e, tl_e)) in
          Log.info "ASSERTION @%d (structural) %a" line boolres_fpr b_res;
          Lib.log_assert b_res;
          x

    (* Analysis of assignments *)
    let a_assign (loc: location) (lv: c_lval) (ex: c_expr) (x: t): t =
      let tl = tr_c_lval lv and te = tr_c_expr ex in
      let nx = M.assert_one "assign" (M.assign loc tl te x) in
      if debug && not !Flags.very_silent_mode then
        Log.force "Result post assign-expr:\n%a" (M.t_fpri "     ") nx;
      nx

    (* Analysis of variable declarations *)
    let a_decl (ct: ctxt) (v: c_var) (x: t): t =
      if ct.ct_currec then Log.fatal_exn "rec-call: variable creation";
      M.unary_op x (UO_env (EO_add_var (tr_c_var v)))

    (* Analysis of blocks entries and exits *)
    let a_block_in (ct: ctxt) (x: t): t =
      if ct.ct_currec then x
      else M.unary_flow_op FO_block_in x
    let a_block_out (ct: ctxt) (x: t): t =
      if ct.ct_currec then x
      else M.unary_flow_op FO_block_out x


    (** Analysis functions *)
    let rec a_stat
        (ct: ctxt)    (* context *)
        (s: c_stat)   (* statement *)
        (x: t)        (* abstract pre-condition *)
        : t (* => abstract post-condition *) =
      (* Logs and outputs *)
      show_sep ();
      if !Flags.very_silent_mode then
        Log.info "ANALYSIS,ITER: at point %d(%d): %d disjuncts" s.csl !step
          (M.disj_size x)
      else
        begin
          let status fmt x =
            if c_stat_do_pp_status s then M.t_fpri "     " fmt x
            else F.fprintf fmt "[-]" in
          Log.info "ANALYSIS,ITER: Status at point %d(%d)\n%a\n\nStatement:\n%a"
            s.csl !step status x (c_stat_fpri "     ") s;
        end;
      incr step;
      if !enable_stats then add_stats s.csl x;
      let is_mc_output_ext_dot = function
        | Cs_memcad (Mc_output_ext (Out_dot (_, _))) -> true
        | _ -> false in
      let x =
        if !flag_enable_ext_export_all && not (is_mc_output_ext_dot s.csk) then
          do_export s.csl x [] []
        else x in
      (* Analysis of the statement *)
      match s.csk with
      | Csdecl v ->
          a_decl ct v x
      | Csassign (lv, ex) ->
          a_assign s.csl lv ex x
      | Cspcall pc ->
          if M.is_bot x then
            x
          else
            (* 1. dump the return variable *)
            let x0 = M.unary_op x (UO_ret None) in
            (* 2. call the procedure *)
            let x0 = a_proc_call s.csl false ct pc x0 in
            (* 3. restore the previous return variable *)
            let x0 = M.restore_return_var x x0 in
            if Flags.flag_debug_funcalls && not !very_silent_mode then
              Log.force "status before completing procedure call:\n%a"
                (M.t_fpri "  ") x0;
            x0
      | Csfcall (lv, pc) ->
          if M.is_bot x then
            x
          else
            (* 1. create the return variable *)
            let tl = tr_c_lval lv in
            let x0 = M.unary_op x (UO_ret (Some (snd tl))) in
            (* 2. call the procedure *)
            let x0 = a_proc_call s.csl true ct pc x0 in
            if Flags.flag_debug_funcalls && not !very_silent_mode then
              Log.force "status at exit:\n%a" (M.t_fpri "  ") x0;
            (* 3. perform the assignment from the return variable to lv *)
            let x0 = M.assert_one "call" (M.assign_from_return s.csl tl x0) in
            (* 4. restore the previous return variable *)
            let x0 = M.restore_return_var x x0 in
            if Flags.flag_debug_funcalls && not !very_silent_mode then
              Log.force "status before completing function call:\n%a"
                (M.t_fpri "  ") x0;
            x0
      | Csblock b ->
          a_block ct b x
      | Csif (c, b0, b1) ->
          let tc =
            c |> tr_c_expr |> map_texpr (fun v -> Var v) in
          let tnotc =
            c |> c_expr_neg |> tr_c_expr |> map_texpr (fun v -> Var v) in
          let x_true  = M.assert_one "if-t" (M.guard s.csl true tc x) in
          let x_false = M.assert_one "if-f" (M.guard s.csl true tnotc x) in
          let x_true  = M.push_hist_atom (Ah_if (s.csl, true )) x_true  in
          let x_false = M.push_hist_atom (Ah_if (s.csl, false)) x_false in
          begin
            match M.is_bot x_true, M.is_bot x_false with
            |  true,  true -> x (* everything is _|_, nothing to analyze *)
            |  true, false -> a_block ct b1 x_false
            | false,  true -> a_block ct b0 x_true
            | false, false ->
                let y0 = a_block ct b0 x_true in
                let y1 = a_block ct b1 x_false in
                M.join None None y0 y1
          end
      | Cswhile (c, b0, u) ->
          let tc =
            c |> tr_c_expr |> map_texpr (fun v -> Var v) in
          let tnotc =
            c |> c_expr_neg |> tr_c_expr |> map_texpr (fun v -> Var v) in
          let live, dead = get_live s.csl in
          let hint = { hbe_live = live } in
          let lint = { lbe_dead = dead } in
          (* computation of the number of unrolls:
           *  - memcad unroll command overrides anything
           *  - otherwise check if the loop is outer, of it is inner, with
           *    unroll flag on; then use value specified in flags
           *  - otherwise, no unroll *)
          let n_unrolls =
            match u with
            | None -> (* no instruction on the loop; use default strategy *)
                if !Flags.unroll_inner || M.loop_count x = 0 then
                  !Flags.unrolls
                else 0
            | Some u -> u in
          let f_loop_body (xx: t): t =
            (* guard evaluation *)
            let xtrue = M.guard s.csl true tc xx in
            (* addition of the partitioning token *)
            let xtrue = List.map (M.push_hist_atom (Ah_while s.csl)) xtrue in
            (* remove enclosing list out of guard *)
            let xtrue = M.assert_one "loop,t" xtrue in
            let xin = M.unary_flow_op FO_loop_body_in xtrue in
            let xout = a_block ct b0 xin in
            M.unary_flow_op FO_loop_body_out xout in
          let x = M.unary_flow_op FO_loop_in x in
          let x_invar = alfp s.csl hint lint n_unrolls f_loop_body x in
          let x_guard = M.guard s.csl true tnotc x_invar in
          let x_exit =
            M.unary_flow_op FO_loop_out (M.assert_one "loop,f" x_guard) in
          if !Flags.flag_debug_iter then
            begin
              Log.force "Loop inv:\n%aLoop guard:" (M.t_fpri "  ") x_invar;
              List.iter (Log.force "%a" (M.t_fpri "  ")) x_guard;
              Log.force "Loop exit:\n%a" (M.t_fpri "   ") x_exit
            end
          else if !Flags.very_silent_mode then
            Log.info "Loop invariant calculated!";
          x_exit
      | Csreturn None ->
          M.unary_flow_op FO_branch_return x
      | Csreturn (Some ex) ->
          let te = tr_c_expr ex in
          let x0 =
            match M.assign_to_return s.csl te x with
            | [ ] -> M.bot x (* returns bottom; we keep it *)
            | ret -> M.assert_one "return" ret in
          if not !Flags.very_silent_mode then
            Log.info "status at return:\n%a" (M.t_fpri "  ") x0;
          M.unary_flow_op FO_branch_return x0
      | Csbreak ->
          M.unary_flow_op FO_branch_break x
      | Cscontinue ->
          M.unary_flow_op FO_branch_continue x
      | Csexit ->
          M.bot x
      | Cs_memcad com ->
          begin
            match com with
            | Mc_assume num_exprs ->
                a_assume_num_exprs num_exprs x
            | Mc_assert num_exprs ->
                a_assert_num_exprs num_exprs s.csl x
            (* adding inductive and summary predicates *)
            | Mc_add_inductive (l, oiname, op) ->
                a_inductive LAssume s.csl l oiname op x
            | Mc_add_segment (l, oiname, op, l_e, oiname_e, op_e) ->
                a_segment LAssume s.csl l oiname op l_e oiname_e op_e x
            (* checking inductive predicates *)
            | Mc_check_segment (l, oiname, op, l_e, oiname_e, op_e) ->
                a_segment LVerify s.csl l oiname op l_e oiname_e op_e x
            | Mc_check_inductive (l, oiname, op) ->
                a_inductive LVerify s.csl l oiname op x
            | Mc_check_bottomness b ->
                let res = M.check_bottomness b x in
                Log.info "ASSERTION @%d (bottomness) %a" s.csl boolres_fpr res;
                Lib.log_assert res;
                x
            | Mc_unfold l ->
                M.ind_unfold s.csl Udir_fwd (tr_c_lval l) x
            | Mc_unfold_bseg l ->
                M.ind_unfold s.csl Udir_bwd (tr_c_lval l) x
            | Mc_unroll _ ->
                Log.fatal_exn "unprocessed memcad unroll command"
            | Mc_merge ->
                M.merge x
            | Mc_sel_merge l ->
                let live, dead = get_live s.csl in
                let hint = { hbe_live = live } in
                let lint = { lbe_dead = dead } in
                M.sel_merge (List.map tr_c_var l) (Some hint) (Some lint) x
            | Mc_force_live _ -> x
            | Mc_kill_flow ->
                M.bot x
            | Mc_array_check ->
                Lib.log_assert (M.check x SL_array);
                x
            | Mc_array_assume ->
                M.assume x SL_array
            | Mc_output_ext (Out_dot (vars, opts)) ->
                do_export s.csl x vars opts
            | Mc_output_stdout ->
                Log.info "Abstract state at point %d(%d)\n%a"
                  s.csl !step (c_stat_fpri "     ") s;
                x
            | Mc_reduce_localize l ->
                M.reduce_localize (tr_c_lval l) x
            | Mc_reduce_eager ->
                M.reduce_eager x
            | Mc_mark_prio l ->
                M.mark_prio (tr_c_lval l) x
            | Mc_comstring _ ->
                Log.fatal_exn "unparsed MemCAD command string"
            | Mc_add_setexprs ls ->
                (* FBR: memcad command badly named: this is an assume !!! *)
                List.fold_left
                  (fun acc y ->
                    M.assume acc (SL_set (tr_c_setexpr y))
                  ) x ls
            | Mc_check_setexprs ls ->
                a_check_setexprs  s.csl ls x
            | Mc_decl_setvars ss ->
                List.fold_left
                  (fun acc y ->
                    M.unary_op acc (UO_env (EO_add_colvar (tr_c_colvar y)))
                  ) x ss
            | Mc_decl_seqvars qs ->
                List.fold_left
                  (fun acc y ->
                    M.unary_op acc (UO_env (EO_add_colvar (tr_c_colvar y)))
                  ) x qs
            | Mc_add_seqexprs ls ->
                List.fold_left
                  (fun acc y ->
                    M.assume acc (SL_seq (tr_c_seqexpr y))
                  ) x ls
            | Mc_check_seqexprs ls  ->
                a_check_seqexprs  s.csl ls x
          end
      | Csassert c ->
          a_assert_c_expr c s.csl x
(*
          let tc =
            c |> tr_c_expr |> map_texpr (fun v -> Var v) in
          let b_ok = M.sat tc x in
          let name = Printf.sprintf "ASSERTION @%d (assert)" s.csl in
          let success ( ) =
            Log.info "%s verified" name;
            Lib.log_assert true;
            x in
          let failure x_not =
            Log.error "%s failed" name;
            Log.error "Over-approx of failure:\n%a" (M.t_fpri "     ") x_not;
            Lib.log_assert false;
            M.assert_one "assert-fail" (M.guard s.csl true tc x) in
          if b_ok then success ( )
          else
            let t_not_c =
              c |> c_expr_neg |> tr_c_expr |> map_texpr (fun v -> Var v) in
            let x_not = M.assert_one "assert" (M.guard s.csl true t_not_c x) in
            if M.is_bot x_not then success ( )
            else failure x_not
*)
      | Csalloc (l, e) ->
          let tl = tr_c_lval l in
          let te = tr_c_expr e in
          M.unary_op x (UO_mem (MO_alloc (tl, te)))
      | Csfree l ->
          let tl = tr_c_lval l in
          M.unary_op x (UO_mem (MO_dealloc tl))
    and a_stat_list (ct: ctxt) (b: c_block) (x: t): t =
      match b with
      | [ ] -> x
      | s0 :: b1 -> a_stat_list ct b1 (a_stat ct s0 x)
    and a_block (ct: ctxt) (b: c_block) (x_pre: t): t =
      if M.is_bot x_pre then
        x_pre
      else
        let x_pre = a_block_in ct x_pre in
        let x_post = a_stat_list ct b x_pre in
        a_block_out ct x_post
    and a_proc_call
        (loc: location) (* call site location *)
        (isfun: bool)   (* whether function or procedure *)
        (ct: ctxt)      (* context in the caller *)
        (c: c_call)     (* call expression *)
        (x: t)          (* abstract pre-condition *)
        : t (* => abstract post-condition *) =
      let x_entry = x in
      (* extraction of the function name *)
      let fun_name = c_call_name c in
      Log.info "a_proc_call: %s" fun_name;
      let f = M.get_function fun_name x_entry in
      (* checking for parameters *)
      let cf_args_len = List.length f.cf_args in
      let cc_args_len = List.length c.cc_args in
      if cf_args_len <> cc_args_len then
        Log.fatal_exn "arg length for %s:\ncf_args_len: %d\ncc_args_len: %d"
          fun_name cf_args_len cc_args_len;
      (* looking for recursion *)
      let ctc = ctxt_call ct fun_name in
      if ctc.ct_currec then
        if !rec_calls then
          begin
            if fun_name != ct.ct_curfun then
              Log.fatal_exn "rec-call: complex (fun)";
            if f.cf_args != [ ] then Log.fatal_exn "rec-call: arguments";
            (* simple recursion:
             *  - caller and callee must be the same;
             *  - no return value, no arguments *)
            if ct.ct_curfix then
              x_entry
            else if not ct.ct_currec then (* first recursive call *)
              (* saving current function entry *)
              let ctc = ctxt_entry_add ctc fun_name x_entry in
              let ctc = { ctc with ct_recdepth = ctc.ct_recdepth + 1 } in
              a_block ctc f.cf_body x_entry
            else (* recursive calls 2 and beyond *)
              let x_old_ent = ctxt_entry_get ctc fun_name in
              if M.is_le x_entry x_old_ent then
                let () =
                  if !Flags.very_silent_mode then
                    Log.info "recursive calls fixpoint (%d iters)"
                      ctc.ct_recdepth
                  else
                    Log.info "recursive calls fixpoint (%d iters)\n%a"
                      ctc.ct_recdepth (M.t_fpri "   ") x_old_ent in
                x_old_ent
              else (* widen, and perform one more iteration *)
                (* let live, dead = get_live loc in *) (* UNUSED ?! *)
                let new_ent, ki = M.widen None None x_old_ent x_entry in
                let new_ent =
                  match ki with
                  | None -> new_ent
                  | Some ki ->  M.join None None new_ent ki in
                let ctc = ctxt_entry_add ctc fun_name new_ent in
                let ctc = { ctc with ct_recdepth = ctc.ct_recdepth + 1 } in
                let x_exit = a_block ctc f.cf_body new_ent in
                x_exit
          end
        else
          Log.fatal_exn "recursive %s not handled"
            (if isfun then "function" else "procedure")
      else (* non recursive call *)
        (* setting the return flow to bottom *)
        let x_entry = M.unary_flow_op FO_fun_in x_entry in
        (* entry in a new scope *)
        let x_entry = a_block_in ctc x_entry in
        (* parameter passing *)
        let x_entry =
          List.fold_left2
            (fun accx formal actual ->
              let lv = { clk = Clvar formal ;
                         clt = formal.cv_type } in
              a_assign loc lv actual (a_decl ctc formal accx)
            ) x_entry f.cf_args c.cc_args in
        (* analysis of the function body *)
        let x_exit = a_block ctc f.cf_body x_entry in
        (* exit scope of the function *)
        let x_exit = a_block_out ctc x_exit in
        (* resetting the direct and return flows *)
        let x_exit = M.unary_flow_op (FO_fun_out x_entry) x_exit in
        x_exit
    (* Analysis of a function (analysis starting point *)
    let a_fun ~(bind_res_var: bool) (fname: string) (x: t): t =
      let f = M.get_function fname x in
      let ct = { ctxt_empty with
                 ct_funcalls = StringSet.singleton fname ;
                 ct_curfun   = fname } in
      let run_flow x =
        M.unary_flow_op (FO_fun_out x)
          (a_block ct f.cf_body (M.unary_flow_op FO_fun_in x)) in
      if bind_res_var then
        begin
          (* We add the return variable before entering function block
           * since it exists in the same scope as s_formula variables *)
          let x = M.unary_op x (UO_ret (Some (C_utils.tr_c_type f.cf_type))) in
          let x = M.unary_flow_op FO_block_in x in
          let x = run_flow x in
          M.unary_flow_op FO_block_out x
        end
      else
        run_flow x


    (** Main analysis functions *)
    (* This analysis implementation is quite rough;
     * temporary code, to improve soon *)
    let analyze_prog (mainfun: string) (gs: analysis_goal) (p: c_prog): unit =
      step := 0;
      ignore (P.live_prog mainfun p);
      (* Initialization: add globals to top *)
      let bind_res_var, xglo, fpost =
        let cfun = C_utils.get_function p mainfun in
        let prepare_init globals formal_args =
          let x = M.unary_flow_op FO_block_in (M.top p) in
          let x =
            StringMap.fold (fun _ var t -> a_decl ctxt_empty var t)
              globals x in
          let x =
            List.fold_left (fun t var -> a_decl ctxt_empty var t)
              x formal_args in
          x in
        match gs with
        | AG_simple ->
            let x = prepare_init p.cp_vars cfun.cf_args in
            false, x, fun _ -> ()
        | AG_goal (col_params, fpre, res_var_name_opt, args, fpost, old_svals) ->
            let get_uid =
              let i = ref 0 in
              fun () -> let res = !i in let () = incr i in res in
            let colvars: colvar list =
              List.map
                (fun (ck, vname) ->
                  { colv_name = vname      ;
                    colv_uid  = get_uid () ;
                    colv_root = true       ;
                    colv_kind = ck         })
                col_params in
            let global_vars =
              p.cp_vars
              |> StringMap.bindings
              |> List.map snd in
            let all_vars = cfun.cf_args @ global_vars in
            let get_cvar_uid () =
              incr C_process.max_c_var_id;
              !C_process.max_c_var_id in
            let make_old_sval sv =
              let cvar = make_c_var sv Ctint in
              { cvar with cv_uid = get_cvar_uid () } in
            let all_vars =
              List.rev_append (List.map make_old_sval old_svals) all_vars in
            let bind_res_var = res_var_name_opt <> None in
            let all_vars =
              if bind_res_var then
                begin
                  let res_var =
                    let f = C_utils.get_function p mainfun in
                    make_c_var "#return" f.cf_type in
                  res_var :: all_vars
                end
              else
                all_vars in
            let tpre  = M.from_s_formula p all_vars colvars fpre in
            let tpre =
              if !Flags.flag_enable_ext_export_all then
                do_export 1 tpre [] []
              else
                tpre in
            (* if there are arguments, they should be enclosed
             * in the pre-condition: *)
            if List.length args != List.length cfun.cf_args then
              Log.fatal_exn "spec goal: wrong length of list of arguments";
            List.iter
              (fun arg ->
                let (pre_env, _, _) = fpre in
                if not (StringMap.mem arg pre_env) then
                  Log.fatal_exn "spec goal for %s; arg %s missing" mainfun arg
              ) args;
            let f_check t =
              let b_res = M.sat_s_formula all_vars colvars fpost t in
              Log.info "POST ASSERTION (new checker) %a" boolres_fpr b_res;
              Lib.log_assert b_res in
            bind_res_var, tpre, f_check in
      (* Analysis launch *)
      let tlast = a_fun ~bind_res_var mainfun xglo in
      let tlast =
        if !Flags.flag_enable_ext_export_all then
          do_export 2 tlast [] []
        else tlast in
      if not !Flags.very_silent_mode then
        Log.force "Last point:\n%a" (M.t_fpri "    ") tlast;
      (* Post-condition checking *)
      fpost tlast;
      (* Statistics (optional) *)
      if !enable_stats then
        Statistics.print_statistics (IntMap.map List.rev !stats_accu)
  end: C_ANALYZE)
