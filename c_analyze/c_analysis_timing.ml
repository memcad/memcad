(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: c_analysis_timing.ml
 **       C level abstraction layers
 ** Josselin Giet, 2022/09/12 *)

open Timer
open C_analysis_sig

module C_analyze_timing =
  functor (A: C_ANALYZE) ->
  ( struct
    module T = Timer.Timer_Mod(struct let name = "Analyze" end)
    let analyze_prog = T.app3 "analyze_prog" A.analyze_prog
  end: C_ANALYZE)