This file summarizes changes between versions in the MemCAD analyzer.


V 1.1 (2021/10)
 Changes since V 1.0:

  - Modifications with an impact on the analysis precision, performance, and
    domain of applications:

    - Support for set parameters in inductive predicates:
      MemCAD now supports inductive predicates with constraints over sets,
      that can be passed as an argument to inductive predicates. This makes
      it possible for the analysis to reason over the preservation of the
      set of elements stored in a structure, or to reason over some shared
      data-structures. Handling of set parameters relies on a novel set
      abstract domain. MemCAD can use a wide range of set abstract domains.
      By default, it uses the external library setr.

    - Partial support for sequence parameters in inductive predicates:
      MemCAD now supports inductive predicates with constraints over
      sequences of elements. This makes it possible for the analysis to
      reason over properties such as the preservation of the ordering of
      the elements in a data-structure or their correct sorting. Handling
      of constraints over sequence parameters relies on a novel sequence
      abstract domain.

    - Clean up of the sub-memory abstract domain:
      MemCAD incorporates an abstract domain to reason over data-structures
      stored inside other data-structures, such as a list the elements of
      which live in a static array. The precision of this abstract domain
      operations has been improved, which should make the verification of
      larger families of programs possible.

    - Novel abstract domain for specific patters of sub-memories:
      A novel abstract domain has been added to MemCAD, so as to support
      specific instances of nested data-structures, of more simple form
      than the ones addressed by the sub-memory abstract domain mentioned
      above. While its scope is restricted, it enables the verification
      of some families of operating system code such as basic drivers or
      memory allocators.

    - Value abstract domain for optional values:
      The above abstract domain needs to express properties about optional
      values, in the sence of the option type in OCaml. A dedicated abstract
      domain provides support for optional scalar values.

  - Internal changes, that should not be observable when using the analysis,
    but with an impact on code quality and maintenance:

    - Refactoring of the data-type that describes symbolic variables.

    - General management of the set and sequence symbolic variables under a
      common umbrella referred to as "collection variables".

    - Refactoring of all the printers used in the analysis, using the
      formatter interface.
