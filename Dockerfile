FROM ocaml/opam2
COPY memcad.opam memcad/memcad.opam
# --allow-releaseinfo-change-suite to circumvent error
#   E: Repository 'http://deb.debian.org/debian buster InRelease' changed its 'Suite' value from 'stable' to 'oldstable'
# (see https://stackoverflow.com/questions/68802802/repository-http-security-debian-org-debian-security-buster-updates-inrelease)
RUN sudo apt-get update --allow-releaseinfo-change-suite
# Fix fatal: unable to connect to github.com:
# github.com[0: 140.82.121.4]: errno=Connection timed out
RUN git remote set-url origin https://github.com/ocaml/opam-repository.git
RUN git pull --quiet
RUN opam update
RUN opam switch create 5.0.0
RUN opam repo add memcad https://gitlab.inria.fr/memcad/opam-memcad-repository.git
RUN opam pin add --no-action --kind=path --yes memcad
RUN opam depext --yes memcad
RUN opam install --deps-only memcad
