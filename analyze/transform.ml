(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: transform.ml
 **       transforming the Clang AST into the MemCAD AST
 ** Pippijn Van Steenhoeven and Francois Berenger, 2014/08/21 *)
(*
open C_sig
open Clang
open Ast
*)
open Data_structures
open Lib

module Log =
  Logger.Make(struct let section = "transfo_" and level = Log_level.DEBUG end)

(*
(***********************************************************************
 * Sizeof/alignof queries
 ***********************************************************************)

let sizeof (clang: Clang.Api.clang) (ctyp: Clang.Ast.ctyp): int =
  Int64.to_int Api.(request clang @@ SizeofType ctyp.t_cref)

let alignof (clang: Clang.Api.clang) (ctyp: Clang.Ast.ctyp): int =
  Api.(request clang @@ AlignofType ctyp.t_cref)
*)
(***********************************************************************
 * Builtin types
 ***********************************************************************)

let c_type_of_builtin_type (btt: Clang.Ast.builtin_type): C_sig.c_type =
  (* FBR: should use signed or unsigned types with proper size *)
  match btt with
  | Void -> Ctvoid
  | Char16
  | Char32
  | Short
  | Long
  | LongLong
  | Int128
  | UShort
  | ULong
  | ULongLong
  | UInt128
  | UInt
  | Int -> Ctint
  | Bool
  | SChar
  | UChar
  | Char_S
  | Char_U -> Ctchar
  | _ -> Ctint

(***********************************************************************
 * Aggregates (struct/union)
 ***********************************************************************)

let make_def_aggregate (agg: C_sig.c_aggregate)
    (ttk: Clang.Ast.elaborated_type_keyword)
  : C_sig.c_type =
  match ttk with
  | Struct -> Ctstruct (Cad_def agg)
  | Union  -> Ctunion  (Cad_def agg)
  | _ -> Log.fatal_exn "make_def_aggregate: Unhandled tag type kind"

let compute_offset (kind : Clang.Ast.elaborated_type_keyword) (off: int)
    (align: int): int =
  if kind = Union then 0
  else ((off + align - 1) / align) * align

(* detect if any global var. initialization was unused *)
let saw_global_init = ref false
let saw_main = ref false

type env = { prog : C_sig.c_prog ;
             ns : string ;
             (* management of vars declared and initialized at file scope *)
             delayed_init_vars : StringSet.t ; (* var. names *)
             (* decls in reverse order of appearance (because in a list) *)
             delayed_init_decls : Clang.Ast.decl list }

let add_type (name: string) (ty: C_sig.c_type) (env: env): env =
  match name with
  | "__int128_t" | "__uint128_t" | "__builtin_va_list" ->
    env (* we ignore clang-generated type names *)
  | name ->
    try
      let name = env.ns ^ name in
      let _prev_type = StringMap.find name env.prog.cp_types in
      let _is_a_defined_struct = function
        | C_sig.Ctstruct (Cad_def _c_aggr) -> true
        | _ -> false in
(*
      if (prev_type <> ty) || not (is_a_defined_struct ty) then
        Log.fatal_exn
          "add_type: name '%s' already in map env.prog.cp_types w/ type: %a"
          name (C_utils.c_type_fpri "") ty
      else (* struct without a typedef *)
*) (* TODO: check *)
        env
    with Not_found -> (* yet unseen type *)
      { env with prog = {
           env.prog with cp_types = StringMap.add name ty env.prog.cp_types }
      }

let nb_anon_structs = ref 0

(* for strucs declared without a typedef, we can add their type to the env.
   only when we see them being used *)
let add_struct_type (ty: Clang.Ast.qual_type) (env: env)
    (c_types: C_sig.c_type Clang.Type.Map.t): env =
  match ty with
    | { desc = Elaborated { keyword;
          named_type = { desc = Record { name = IdentifierName name2 } }}} as tl
    | { desc = Pointer ({ desc = Elaborated { keyword;
          named_type = { desc = Record { name = IdentifierName name2 } }}} as tl) }
    | { desc = ConstantArray { element = { desc = Elaborated { keyword;
          named_type = { desc = Record { name = IdentifierName name2 } }}} as tl }}
    | { desc = ConstantArray { element = { desc = Pointer ({ desc =
          Elaborated { keyword;
            named_type = { desc = Record { name = IdentifierName name2 } }}} as tl)}}} ->
      (* struct w/o typedef, or pointer to that, or table of that or
       * table of pointer of that *)
      let c_type = Clang.Type.Map.find tl c_types in
      let name2 =
        if name2 <> "" then name2
        else (* no typedef and no struct name:
                we assign it a unique temp. name which cannot appear
                in valid C source code *)
          let res =
            Printf.sprintf "%%mc_transfo_anon_struct_%d" !nb_anon_structs in
          incr nb_anon_structs;
          res
      in
      add_type name2 c_type env
    | _ -> env

(* to unfold types up to a given depth *)
let (seen_times: int StringMap.t ref) = ref StringMap.empty

let rec c_agg_fields_of_decls
    ((cache, seen): C_sig.c_type Clang.Type.Map.t *
                    Clang.Type.Set.t)
    kind
    (decls: Clang.Decl.t list)
  : C_sig.c_type Clang.Type.Map.t *
    Clang.Type.Set.t *
    C_sig.c_agg_field list =
  let rec loop off (cache, seen, fields) declarations =
    match (declarations : Clang.Decl.t list) with
    | [] -> (cache, seen, List.rev fields)
    | { desc =
          RecordDecl { keyword = keyword1; name = name1; fields = sub_fields }}
      :: { desc = Field {
          qual_type = {
            desc = Elaborated {
                keyword = keyword2;
                named_type = { desc = Record { name = IdentifierName name2 } };
              }
          } as qual_type;
          name;
          bitwidth;
        } } :: tl when keyword1 = keyword2 && name1 = name2 ->
      let size  = Clang.Type.get_size_of qual_type in
      let align = Clang.Type.get_align_of qual_type in
      let cache, seen, cag_fields =
        c_agg_fields_of_decls (cache, seen) keyword1 sub_fields
      in
      let agg : C_sig.c_aggregate = {
                  cag_name   = if name1 = "" then None else Some name1;
                  cag_align  = align;
                  cag_size   = size;
                  cag_fields }
      in
      (* offset for the current field *)
      let off = compute_offset kind off align in
      let field : C_sig.c_agg_field = { caf_typ  = make_def_aggregate agg kind;
                    caf_off  = off;
                    caf_size = size;
                    caf_name = name }
      in
      (* offset for the next field *)
      let off = off + size in
      loop off (cache, seen, field :: fields) tl
    | { desc = RecordDecl _ } :: tl ->
      (* this is a forward declaration introduced by clang, just ignore it
         as it is not a field *)
      loop off (cache, seen, fields) tl
    | { desc = Field { name; qual_type; bitwidth }} as decl :: tl ->
      if bitwidth <> None then Log.fatal_exn "Bit fields not implemented";
      (* if init <> None then Log.fatal_exn "Member initialisers not implemented"; *)
      let size  = Clang.Type.get_size_of qual_type in
      let align = Clang.Type.get_align_of qual_type in
      (* offset for the current field *)
      let off = compute_offset kind off align in
      let loc = Clang.Ast.location_of_node decl in
      let (cache, seen), ty =
        mctype_of_ctyp loc (cache, seen) qual_type in
      let field : C_sig.c_agg_field = { caf_typ  = ty;
                    caf_off  = off;
                    caf_size = size;
                    caf_name = name }
      in
      (* offset for the next field *)
      let off = off + size in
      loop off (cache, seen, field :: fields) tl
   | field :: tl ->
       Log.fatal_exn "%s\nOnly FieldDecls allowed within RecordDecl"
         (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node field))
  in
  loop 0 (cache, seen, []) decls

and mctype_of_elaborated
    ((cache, seen): C_sig.c_type Clang.Type.Map.t *
                    Clang.Type.Set.t)
    (keyword: Clang.Ast.elaborated_type_keyword)
    (named_type: Clang.Ast.qual_type)
  : (C_sig.c_type Clang.Type.Map.t *
     Clang.Type.Set.t) *
    C_sig.c_type =
  let name =
    match named_type.desc with
    | Record { name = IdentifierName name } -> name
    | _ -> Log.fatal_exn "%s\nOnly record allowed within Elaborated"
         (Clang.get_type_spelling named_type.cxtype) in
  let already_seen = Clang.Type.Set.mem named_type seen in
  let seen =
    if already_seen then
      seen
    else
      begin
        if name <> "" then
          seen_times := StringMap.add name 0 !seen_times;
        Clang.Type.Set.add named_type seen
      end
  in
  if name <> "" then
    begin
      let count = StringMap.find name !seen_times in
      seen_times := StringMap.update name (count + 1) !seen_times
    end;
  if already_seen && name <> "" &&
     (StringMap.find name !seen_times) > !Flags.type_unfolds then begin
    try ((cache, seen), Clang.Type.Map.find named_type cache)
    with Not_found ->
      ((cache, seen), Ctnamed { cnt_name = name; cnt_type = Ctvoid })
  end else
    ((*try*)
      let cache, seen, cag_fields =
        let members = Clang.Type.list_of_fields named_type in
        c_agg_fields_of_decls (cache, seen) keyword members
      in
      let c_type =
        make_def_aggregate {
          cag_name   = if name = "" then None else Some name;
          cag_align  = Clang.Type.get_align_of named_type;
          cag_size   = Clang.Type.get_size_of  named_type;
          cag_fields;
        } keyword
      in
      let cache = Clang.Type.Map.add named_type c_type cache in
      ((cache, seen), c_type)
(*
    with Api.E (Api.E_Failure msg) ->
      let c_type =
        Ctnamed { cnt_name = "struct " ^ name; cnt_type = Ctvoid }
      in
      let cache = StringMap.add cnt_name c_type cache in
      Log.warn "tag type name: %s: %s" name msg;
      ((cache, seen), c_type)
*)
    )

(***********************************************************************
 * Main recursive entry point for type translation
 ***********************************************************************)

(* MemCAD type of clang type *)
and mctype_of_ctyp
    (loc: Clang.Ast.source_location)
    ((cache, seen): C_sig.c_type Clang.Type.Map.t *
                    Clang.Type.Set.t)
    (ctyp: Clang.Type.t)
  : (C_sig.c_type Clang.Type.Map.t *
     Clang.Type.Set.t) *
    C_sig.c_type =
  match ctyp.desc with
  | BuiltinType bt -> ((cache, seen), c_type_of_builtin_type bt)
  | ConstantArray { element; size } ->
    let (cache, seen), memty = mctype_of_ctyp loc (cache, seen) element in
    ((cache, seen), Ctarray (memty, Some size))
  | IncompleteArray memty ->
    let (cache, seen), memty = mctype_of_ctyp loc (cache, seen) memty in
    ((cache, seen), Ctarray (memty, None))
  | Typedef { name = IdentifierName name } ->
    if name = "" then
      Log.fatal_exn "mctype_of_ctyp: empty name in TypedefType";
    begin
(*
      try
*)
        let (cache, seen), c_type =
          let tloc = Clang.Type.get_typedef_underlying_type ctyp in
          mctype_of_ctyp loc (cache, seen) tloc
        in
        ((cache, seen), Ctnamed { cnt_name = name; cnt_type = c_type })
(*
      with Api.E (Api.E_Failure msg) ->
        Log.warn "typedef name: %s: %s" name msg;
        ((cache, seen), Ctnamed { cnt_name = name; cnt_type = Ctvoid })
*)
    end
  | Elaborated { keyword = (Struct | Union) as keyword; named_type } ->
      mctype_of_elaborated (cache, seen) keyword named_type
  | Enum name
  | Elaborated { keyword = Enum; named_type = { desc = Enum name }} ->
    ((cache, seen), c_type_of_builtin_type Int)
  | ParenType inner -> mctype_of_ctyp loc (cache, seen) inner
(*
  | Pointer { desc = FunctionType { result } }
*)
  | FunctionType { result } ->
    mctype_of_ctyp loc (cache, seen) result (* TODO: ?! *)
  | Pointer pointee ->
    let (cache, seen), pointee = mctype_of_ctyp loc (cache, seen) pointee in
    ((cache, seen), Ctptr (Some (pointee)))
(*
  | DecayedType (decayed, _original) ->
    mctype_of_ctyp clang (cache, seen) decayed
  | TypeOfExprType _ -> Log.fatal_exn "TypeOfExprType"
  | TypeOfType _ -> Log.fatal_exn "TypeOfType"
  | VariableArrayType _ -> Log.fatal_exn "VariableArrayType"
*)
  | InvalidType ->
      Log.warn "Invalid type at %a"
        (fun fmt -> Clang.Ast.pp_source_location fmt) loc;
      ((cache, seen), c_type_of_builtin_type Invalid)
  | _ ->
      Log.fatal_exn "Unexpected type %s at %a" (Clang.Type.show ctyp)
        (fun fmt -> Clang.Ast.pp_source_location fmt) loc

(***********************************************************************
 * Mapping the type map (array) from clang to memcad types
 ***********************************************************************)

let rec resolve_c_type
    (seen: Clang.Type.Set.t)
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (c_type: C_sig.c_type): unit =
  match c_type with
  | Ctint
  | Ctchar
  | Ctvoid
  | Ctptr None
  | Ctstring _
  (* a named aggregate (struct or union) without a definition cannot
     be resolved since it has no fields.
     This is OK since an aggregate with the same name but with a definition
     exists somewhere and will be resolved *)
  | Ctstruct (Cad_named _)
  | Ctunion  (Cad_named _) -> ()
(*
  | Ctnamed ({ cnt_type = Ctnamed { cnt_name; cnt_type = Ctptr None } }
             as c_named) ->
    let key = StringMap.find cnt_name seen in
    c_named.cnt_type <- DenseIntMap.find key c_types
*)
  | Ctnamed c_named -> resolve_c_type seen c_types c_named.cnt_type
  | Ctstruct (Cad_def aggr)
  | Ctunion (Cad_def aggr) ->
    List.iter
      (fun (cf : C_sig.c_agg_field) -> resolve_c_type seen c_types cf.caf_typ)
      aggr.cag_fields
  | Ctptr (Some ty)
  | Ctarray (ty, _) -> resolve_c_type seen c_types ty

(*
let map_types
    (types: (Clang.Ast.ctyp, Clang.Ast.ctyp) Util.DenseIntMap.t)
  : (Clang.Ast.ctyp, C_sig.c_type) Util.DenseIntMap.t =
  let ((cache, seen), c_types) =
    DenseIntMap.mapvf
      (fun _i ctyp seen -> mctype_of_ctyp clang seen ctyp)
      types
      (StringMap.empty, StringMap.empty)
  in
  DenseIntMap.iter
    (fun _ c_type -> resolve_c_type seen c_types c_type)
    c_types;
  (* DenseIntMap.iter *)
  (*   (fun key c_type -> *)
  (*      Format.printf "%d: %a\n" *)
  (*        (key : ctyp DenseIntMap.key :> int) *)
  (*        Show_c_type.format c_type) *)
  (*   c_types; *)
  c_types
*)

let start_line (loc: Clang.Ast.source_location): int =
  (Clang.Ast.concrete_of_source_location Presumed loc).line

let c_var_of_c_fun (func: C_sig.c_fun): C_sig.c_var =
  { cv_name     = func.cf_name ;
    cv_uid      = func.cf_uid  ;
    cv_type     = func.cf_type ;
    cv_volatile = false        }

let lookup_var (sloc: Clang.Ast.source_location)
    (env: env) (decl_env: C_sig.c_var StringMap.t) (name: StringMap.key)
  : C_sig.c_var =
  try StringMap.find name decl_env
  with Not_found ->
    try StringMap.find name env.prog.cp_vars
    with Not_found ->
      try
        let func = StringMap.find name env.prog.cp_funs in
        c_var_of_c_fun func
      with Not_found ->
        let line = start_line sloc in
        Log.fatal_exn "lookup failed for var %s at line %d:\n%s"
          name line (Lib.get_line !Flags.analyzed_file line)

let add_fun (curr_fun: C_sig.c_fun) (env: env): env =
  let name = env.ns ^ curr_fun.cf_name in
  if StringMap.mem name env.prog.cp_funs then
    (* it is maybe OK to replace a function without definition
       with a function of the same name once we have its definition *)
    let prev_fun = StringMap.find name env.prog.cp_funs in
    if (prev_fun.cf_body <> []) || (curr_fun.cf_body = []) then
      begin
        Log.warn "add_fun: name already in map env.prog.cp_funs: %s" name;
        env
      end
    else
      let m =
        StringMap.add name curr_fun (StringMap.remove name env.prog.cp_funs)
      in
      { env with prog = { env.prog with cp_funs = m } }
  else
    let m = StringMap.add name curr_fun env.prog.cp_funs in
    { env with prog = { env.prog with cp_funs = m } }

let add_var (var: C_sig.c_var) (env: env): env =
  let name = env.ns ^ var.cv_name in
  if StringMap.mem name env.prog.cp_vars then
    Log.fatal_exn "add_var: name %s already in map env.prog.cp_vars" name;
  { env with prog = {
       env.prog with cp_vars = StringMap.add name var env.prog.cp_vars } }

(* add a declaration with initialization at file scope to
   the map of such declarations *)
let add_delayed_init (decl_w_init: Clang.Ast.decl) (env: env): env =
  saw_global_init := true;
  match decl_w_init with
    | { desc = Var { var_name; var_init = Some init; _ }} ->
      let name = env.ns ^ var_name in
      if StringSet.mem name env.delayed_init_vars then
        Log.fatal_exn
          "add_delayed_init: name %s already in map env.delayed_inits" name
      else
        let delayed_init_vars' = StringSet.add name env.delayed_init_vars in
        let delayed_init_decls' = decl_w_init :: env.delayed_init_decls in
        { env with delayed_init_vars  = delayed_init_vars'  ;
                   delayed_init_decls = delayed_init_decls' }
    | _ -> Log.fatal_exn "add_delayed_init: unsupported decl: %s"
             (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node decl_w_init))

let collect_delayed_init_stmts (env: env): Clang.Ast.stmt list =
  List.fold_left (* reverts the list env.delayed_init_decls on purpose *)
    (fun acc (decl : Clang.Ast.decl) -> match decl with
       | { desc = Var { var_type; var_name; var_init = Some init }} ->
         let init_stmt =
           Clang.Ast.node ~qual_type:var_type (Clang.Ast.Expr (
             For_memcad.bin_op var_type
               (Clang.Ast.node ~qual_type:var_type
                  (Clang.Ast.DeclRef (Clang.Ast.identifier_name var_name)))
               Assign init)) in
         init_stmt :: acc
       | _ -> assert(false);
    )
    [] env.delayed_init_decls

let c_binop_of_binary_operator (op: Clang.Ast.binary_operator_kind) sloc
  : C_sig.c_binop =
  match op with
  | EQ -> Cbeq
  | NE -> Cbne
  | GE -> Cbge
  | GT -> Cbgt
  | LE -> Cble
  | LT -> Cblt
  | Add -> Cbadd
  | Sub -> Cbsub
  | Mul -> Cbmul
  | Div -> Cbdiv
  | LAnd -> Cbland
  | LOr -> Cblor
  | Rem -> Cbmod
  | And -> Cbband
  | Or -> Cbbor
  | Shl -> Cbbslft
  | Shr -> Cbbsrgh
  | _ ->
    let line = start_line sloc in
    Log.fatal_exn "unsupported binop %s at line %d\n%s"
      (Clang.Ast.string_of_binary_operator_kind op)
      line
      (Lib.get_line !Flags.analyzed_file line)

let c_uniop_of_unary_operator (op: Clang.Ast.unary_operator_kind)
    : C_sig.c_uniop =
  match op with
  | Minus -> Cuneg
  | LNot  -> Culnot
  | Not   -> Cubnot
  | op ->
      Log.fatal_exn "unsupported uniop: %s"
        (Clang.Ast.string_of_unary_operator_kind op)

let c_var_of_parm_decl
    (env: env)
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (name: string)
    (ty: Clang.Ast.qual_type): env * C_sig.c_var =
  let env = add_struct_type ty env c_types in
  (env,
    { cv_name     = name ;
      cv_uid      = -1 ;
      cv_type     = Clang.Type.Map.find ty c_types ;
      cv_volatile = ty.volatile })

let create_c_fun_forward_decl
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (ty: Clang.Ast.function_type)
    (name: string): env * C_sig.c_fun =
  (* Create the signature of the function (without body) first,
     so that name lookups within the (eventual) body work for the
     currently processed function. *)
  let args =
    match ty.parameters with
    | None -> []
    | Some { non_variadic } -> non_variadic in
  let env, cf_args =
    List.fold_right (* don't reverse args' order ... *)
      (fun ({ desc = { name; qual_type }} : Clang.Ast.parameter) (env, acc) ->
         let env, var = c_var_of_parm_decl env c_types name qual_type in
         (env, var :: acc))
      args
      (env, []) in
  let ret_type = ty.result in
  let c_fun = { C_sig.cf_type = Clang.Type.Map.find ret_type c_types;
                cf_uid  = -1;
                cf_name = name;
                cf_args = cf_args;
                cf_body = [] } in
  (env, c_fun)

type env_or_loc_cvar = Env of (env * C_sig.c_fun)
                     | Loc_cvar of (int * C_sig.c_var)

let c_decl_of_decl
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl: Clang.Ast.decl)
  : env_or_loc_cvar =
  let line = start_line (Clang.Ast.location_of_node decl) in
  match decl.desc with
  | Var { var_type; var_name; var_init } ->
    (match var_init with
     | None -> ()
     | Some x -> Log.fatal_exn "Unsupported: initialiser for %s: %s\n
                            (transform SplitInitialisers not applied?)"
         var_name (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node x))
    );
    Loc_cvar (line, { cv_name     = var_name;
                      cv_uid      = -1;
                      cv_type     = Clang.Type.Map.find var_type c_types;
                      cv_volatile = var_type.volatile })
  | Function { function_type; name = IdentifierName name; body = None } ->
    (* local forward function declaration *)
    Env (create_c_fun_forward_decl c_types env function_type name)
  | Function { name = IdentifierName name } ->
    Log.fatal_exn "local function declaration of %s at line %d" name line
(*
  | Empty ->
    Log.fatal_exn "empty declaration within function body at line %d" line
  | TypedefDecl (ty, name) ->
    Log.fatal_exn "local typedefs are not supported by memcad AST; line %d"
             line
  | EnumDecl (name, enumerators) ->
    Log.fatal_exn "local enums are not supported by memcad AST; line %d"
             line
  | RecordDecl (kind, name, members, _) ->
    Log.fatal_exn "local %ss are not supported by memcad AST; line %d"
             (Pp.string_of_tag_type_kind kind) line
  | EnumConstantDecl _ ->
    Log.fatal_exn "EnumConstantDecl found in function at line %d" line
  | FieldDecl        _ ->
    Log.fatal_exn "FieldDecl found in function at line %d" line
  | ParmVarDecl      _ ->
    Log.fatal_exn "ParmVarDecl found in function at line %d" line
  | TranslationUnitDecl _ ->
    Log.fatal_exn "TranslationUnitDecl found in function at line %d" line
*)
  | _ ->
    Log.fatal_exn "c_decl_of_decl: %s at line %d"
      (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node decl)) line

let rec c_lvalk_of_expr
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (expr: Clang.Ast.expr): C_sig.c_lvalk =
  let line = start_line (Clang.Ast.location_of_node expr) in
  let error s =
    Log.fatal_exn "c_lvalk_of_expr: %s at line %d\n%s\n"
      s line (Lib.get_line !Flags.analyzed_file line) in
  match expr.desc with
  | DeclRef { name = IdentifierName name } ->
      Clvar (lookup_var (Clang.Ast.location_of_node expr) env decl_env name)
  | Member { base = Some base; arrow; field = FieldName { desc = { name = IdentifierName field_name; _ }}} ->
    (* print_endline (Pp.string_of_ctyp ty); *)
    let base : C_sig.c_lval =
      if arrow then
        { clk = Clderef (c_expr_of_expr c_types env decl_env base);
          clt = Clang.Type.Map.find (Clang.Type.of_node expr) c_types }
      else
        c_lval_of_expr c_types env decl_env base
    in
    Clfield (base, field_name)
  | ArraySubscript { base; index } ->
    Clindex (c_lval_of_expr c_types env decl_env base,
             c_expr_of_expr c_types env decl_env index)
  | UnaryOperator { kind = Deref; operand } ->
    Clderef (c_expr_of_expr c_types env decl_env operand)
(*
  | IntegerLiteral _ -> error "c_lvalk_of_expr: IntegerLiteral"
  | CharacterLiteral _ -> error "c_lvalk_of_expr: CharacterLiteral"
  | FloatingLiteral _ -> error "c_lvalk_of_expr: FloatingLiteral"
  | StringLiteral _ -> error "c_lvalk_of_expr: StringLiteral"
  | BinaryOperator _ -> error "c_lvalk_of_expr: BinaryOperator"
  | UnaryOperator _ -> error "c_lvalk_of_expr: UnaryOperator"
  | PredefinedExpr _ -> error "c_lvalk_of_expr: PredefinedExpr"
  | ImplicitCastExpr _ -> error "c_lvalk_of_expr: ImplicitCastExpr"
  | CStyleCastExpr _ -> error "c_lvalk_of_expr: CStyleCastExpr"
  | CompoundLiteralExpr _ -> error "c_lvalk_of_expr: CompoundLiteralExpr"
  | ParenExpr _ -> error "c_lvalk_of_expr: ParenExpr"
  | VAArgExpr _ -> error "c_lvalk_of_expr: VAArgExpr"
  | CallExpr _ -> error "c_lvalk_of_expr: CallExpr"
  | ConditionalOperator _ -> error "c_lvalk_of_expr: ConditionalOperator"
  | DesignatedInitExpr _ -> error "c_lvalk_of_expr: DesignatedInitExpr"
  | InitListExpr _ -> error "c_lvalk_of_expr: InitListExpr"
  | ImplicitValueInitExpr -> error "c_lvalk_of_expr: ImplicitValueInitExpr"
  | StmtExpr _ -> error "c_lvalk_of_expr: StmtExpr"
  | AddrLabelExpr _ -> error "c_lvalk_of_expr: AddrLabelExpr"
  | SizeOfExpr _ -> error "c_lvalk_of_expr: SizeOfExpr"
  | SizeOfType _ -> error "c_lvalk_of_expr: SizeOfType"
  | AlignOfExpr _ -> error "c_lvalk_of_expr: AlignOfExpr"
  | AlignOfType _ -> error "c_lvalk_of_expr: AlignOfType"
  | VecStepExpr _ -> error "c_lvalk_of_expr: VecStepExpr"
  | VecStepType _ -> error "c_lvalk_of_expr: VecStepType"
*)
  | _ -> error ("c_lvalk_of_expr: " ^ (
      Clang.get_cursor_spelling (Clang.Ast.cursor_of_node expr)))

and c_lval_of_expr
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (expr: Clang.Ast.expr)
  : C_sig.c_lval =
  { clk = c_lvalk_of_expr c_types env decl_env expr ;
    clt = Clang.Type.Map.find (Clang.Type.of_node expr) c_types     }

and c_exprk_of_expr
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (expr: Clang.Ast.expr): C_sig.c_exprk =
  let line = start_line (Clang.Ast.location_of_node expr) in
  let unimp s =
    Log.fatal_exn "c_exprk_of_expr: %s at line %d\n%s\n"
      s line (Lib.get_line !Flags.analyzed_file line)
  in
  match expr.desc with
  | IntegerLiteral i -> Ceconst (Ccint (Clang.Ast.int_of_literal i))
  | StringLiteral { bytes = s } -> Ceconst (Ccstring s)
  | CharacterLiteral { value } -> Ceconst (Ccchar (char_of_int value))
  | BinaryOperator { lhs; kind; rhs } ->
    Cebin (c_binop_of_binary_operator kind (Clang.Ast.location_of_node expr),
           c_expr_of_expr c_types env decl_env lhs,
           c_expr_of_expr c_types env decl_env rhs)
  | UnaryOperator { kind = AddrOf; operand } ->
    Ceaddrof (c_lval_of_expr c_types env decl_env operand)
  | UnaryOperator { kind = op; operand } ->
    begin
      match op with
      | Plus -> c_exprk_of_expr c_types env decl_env operand
      | _ -> Ceuni (c_uniop_of_unary_operator op,
                    c_expr_of_expr c_types env decl_env operand)
    end
(*
  | ParenExpr expr -> c_exprk_of_expr clang c_types env decl_env expr
  | CStyleCastExpr _ -> unimp "c_exprk_of_expr: CStyleCastExpr"
  | ImplicitCastExpr _ -> unimp "c_exprk_of_expr: ImplicitCastExpr"
  | FloatingLiteral _ -> unimp "c_exprk_of_expr: FloatingLiteral"
  | PredefinedExpr _ -> unimp "c_exprk_of_expr: PredefinedExpr"
  | CompoundLiteralExpr _ -> unimp "c_exprk_of_expr: CompoundLiteralExpr"
  | VAArgExpr _ -> unimp "c_exprk_of_expr: VAArgExpr"
  | CallExpr (e, _) -> unimp ("c_exprk_of_expr: CallExpr" ^ (Pp.string_of_expr e))
  | ConditionalOperator _ -> unimp "c_exprk_of_expr: ConditionalOperator"
  | DesignatedInitExpr _ -> unimp "c_exprk_of_expr: DesignatedInitExpr"
  | InitListExpr _ -> unimp "c_exprk_of_expr: InitListExpr"
  | ImplicitValueInitExpr -> unimp "c_exprk_of_expr: ImplicitValueInitExpr"
  | StmtExpr _ -> unimp "c_exprk_of_expr: StmtExpr"
  | AddrLabelExpr _ -> unimp "c_exprk_of_expr: AddrLabelExpr"
  | SizeOfExpr _  -> unimp "c_exprk_of_expr: SizeOfExpr"
  | SizeOfType tloc -> let size = sizeof clang tloc.tl_type in
                       (* Log.info "size: %d" size; *) (* debug *)
                       Ceconst (Ccint size)
  | AlignOfExpr _ -> unimp "c_exprk_of_expr: AlignOfExpr"
  | AlignOfType _ -> unimp "c_exprk_of_expr: AlignOfType"
  | VecStepExpr _ -> unimp "c_exprk_of_expr: VecStepExpr"
  | VecStepType _ -> unimp "c_exprk_of_expr: VecStepType"
  (* Already handled below. *)
  | ArraySubscriptExpr _ -> error "c_exprk_of_expr: ArraySubscriptExpr"
  | MemberExpr _ -> error "c_exprk_of_expr: MemberExpr"
  | DeclRefExpr _ -> error "c_exprk_of_expr: DeclRefExpr"
*)
  | _ -> unimp (Refl.show [%refl: Clang.Ast.expr] [] expr)

and c_expr_of_expr
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (expr: Clang.Ast.expr): C_sig.c_expr =
  let c_type = Clang.Type.Map.find (Clang.Type.of_node expr) c_types in
  let loc = start_line (Clang.Ast.location_of_node expr) in
  match expr.desc with
  (* (( void* )0) => null *)
  | Cast {
      kind = CStyle;
      qual_type = { desc = Pointer { desc = BuiltinType Void }};
      operand = { desc = IntegerLiteral (Int 0)}} ->
    { cek = Ceconst Ccnull;
      cep = loc ;
      cet = c_type }
  | UnaryOperator { kind = Deref }
  | ArraySubscript _
  | Member _
  | DeclRef _ ->
    (*print_endline (Show.show<expr> expr);*)
    (*print_endline (Show.show<ctyp> ty);*)
    { cek = Celval (c_lval_of_expr c_types env decl_env expr);
      cep = loc ;
      cet = c_type }
  | Cast {
      kind = CStyle;
      operand} ->
    Log.warn "c_expr_of_expr: ignored cast at %a"
      (fun fmt -> Clang.Ast.pp_source_location fmt)
      (Clang.Ast.location_of_node expr);
    c_expr_of_expr c_types env decl_env operand
  | _ ->
    (* print_endline (Pp.string_of_expr expr); *)
    { cek = c_exprk_of_expr c_types env decl_env expr;
      cep = loc ;
      cet = c_type }

let make_call
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (callee: Clang.Ast.expr)
    (args: Clang.Ast.expr list): C_sig.c_call =
  { cc_fun = c_expr_of_expr c_types env decl_env callee;
    cc_args = List.map (c_expr_of_expr c_types env decl_env) args }

(*
let get_return_type = For_memcad.Transformation.SplitInitialisers.get_return_type
*)

let rec c_stat_of_expr
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (expr: Clang.Ast.expr): C_sig.c_stat =
  let loc = start_line (Clang.Ast.location_of_node expr) in
  let unimp s =
    Log.fatal_exn "c_stat_of_expr: %s at line %d\n%s\n"
             s loc (Lib.get_line !Flags.analyzed_file loc)
  in
  match expr.desc with
  | BinaryOperator { lhs; kind = Assign; rhs } ->
    (*print_endline (Show.show<expr> rhs);*)
    { csl = loc;
      csk = Csassign (c_lval_of_expr c_types env decl_env lhs,
                      c_expr_of_expr c_types env decl_env rhs) }
  | Call { callee; args } ->
    { csl = loc;
      csk = (* Special handling for known functions. Note that
               malloc is not handled here, since it is only used
               within an assignment expression statement. *)
        let fun_name =
          match callee.desc with
          | DeclRef { name = IdentifierName fun_name } -> fun_name
          | _ -> Log.fatal_exn "function call without name: %s"
                (Clang.get_cursor_spelling (
                   Clang.Ast.cursor_of_node callee)) in
        match fun_name, args with
        | "_memcad", [{ desc = StringLiteral { bytes = str }}] ->
          Cs_memcad (Mc_comstring str)
        | "assert", [_] ->
          Csassert (c_expr_of_expr c_types env decl_env (List.hd args))
        | "exit", [_] -> Csexit
        | "free", [_] ->
          Csfree (c_lval_of_expr c_types env decl_env (List.hd args))
        | _ ->
          let ret_type = Clang.Type.of_node expr in
          let call = make_call c_types env decl_env callee args in
          if ret_type.desc = BuiltinType Void then (* procedure call *)
            begin
              Log.info "procedure call: %s" fun_name;
              Cspcall call
            end
          else (* function call *)
            Log.fatal_exn "function call without lval to store its result: %s"
              fun_name
    }
  | DeclRef _ -> (* ignored; transformed to an empty block *)
    { csl = loc; csk = Csblock [] }
(*
  | IntegerLiteral _ -> unimp "c_stat_of_expr: IntegerLiteral"
  | CharacterLiteral _ -> unimp "c_stat_of_expr: CharacterLiteral"
  | FloatingLiteral _ -> unimp "c_stat_of_expr: FloatingLiteral"
  | StringLiteral _ -> unimp "c_stat_of_expr: StringLiteral"
  | BinaryOperator _ -> unimp "c_stat_of_expr: BinaryOperator"
  | UnaryOperator _ -> unimp "c_stat_of_expr: UnaryOperator"
  | PredefinedExpr _ -> unimp "c_stat_of_expr: PredefinedExpr"
  | ImplicitCastExpr _ -> unimp "c_stat_of_expr: ImplicitCastExpr"
  | CStyleCastExpr _ -> unimp "c_stat_of_expr: CStyleCastExpr"
  | CompoundLiteralExpr _ -> unimp "c_stat_of_expr: CompoundLiteralExpr"
  | ParenExpr _ -> unimp "c_stat_of_expr: ParenExpr"
  | VAArgExpr _ -> unimp "c_stat_of_expr: VAArgExpr"
  | MemberExpr _ -> unimp "c_stat_of_expr: MemberExpr"
  | ConditionalOperator _ -> unimp "c_stat_of_expr: ConditionalOperator"
  | DesignatedInitExpr _ -> unimp "c_stat_of_expr: DesignatedInitExpr"
  | InitListExpr _ -> unimp "c_stat_of_expr: InitListExpr"
  | ImplicitValueInitExpr -> unimp "c_stat_of_expr: ImplicitValueInitExpr"
  | ArraySubscriptExpr _ -> unimp "c_stat_of_expr: ArraySubscriptExpr"
  | StmtExpr _ -> unimp "c_stat_of_expr: StmtExpr"
  | AddrLabelExpr _ -> unimp "c_stat_of_expr: AddrLabelExpr"
  | SizeOfExpr _ -> unimp "c_stat_of_expr: SizeOfExpr"
  | SizeOfType _ -> unimp "c_stat_of_expr: SizeOfType 3"
  | AlignOfExpr _ -> unimp "c_stat_of_expr: AlignOfExpr"
  | AlignOfType _ -> unimp "c_stat_of_expr: AlignOfType"
  | VecStepExpr _ -> unimp "c_stat_of_expr: VecStepExpr"
  | VecStepType _ -> unimp "c_stat_of_expr: VecStepType"
*)
  | _ ->
      unimp ("c_stat_of_expr: " ^
        (Clang.Expr.show expr))

(* This function maps N clang statements to M memcad statements.
   M may be considerably more than N.
   The output env. may contain local forward function declarations.
   Local function fwd. decls. are only supported in the body of a function,
   not in a while stmt, if stmt, etc. *)
let rec c_stats_of_stmts
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decl_env: C_sig.c_var StringMap.t)
    (stmts: Clang.Ast.stmt list): env * C_sig.c_block =
  let rec loop env decl_env stats = function
    | [] -> (env, stats)
    | (stmt : Clang.Ast.stmt) :: tl ->
      let csl = start_line (Clang.Ast.location_of_node stmt) in
      let unimp msg =
        Log.fatal_exn "c_stats_of_stmts: %s at line %d" msg csl
      in
      match stmt.desc with
      | Return expr ->
        let stat : C_sig.c_stat =
          { csl;
            csk = Csreturn
                (match expr with
                 | None -> None
                 | Some expr ->
                   Some (c_expr_of_expr c_types env decl_env expr)) }
        in
        loop env decl_env (stat :: stats) tl
      | Expr e ->
        let stat : C_sig.c_stat = match e.desc with
          | BinaryOperator
              { lhs; kind = Assign;
                rhs = { desc = Call {
                        callee = { desc = DeclRef
                                     { name = IdentifierName fun_name }}
                          as callee; args }}} ->
            begin
              match fun_name, args with
              | "malloc", [arg] ->
                  (* Special handling of malloc. *)
                  { csl;
                    csk = Csalloc (c_lval_of_expr c_types env decl_env lhs,
                                   c_expr_of_expr c_types env decl_env arg) }
              | _ ->
                  (* Special handling of assigned call expressions. *)
                  Log.info "function call: %s" fun_name;
                  { csl;
                    csk = Csfcall (c_lval_of_expr c_types env decl_env lhs,
                      make_call c_types env decl_env callee args) }
            end
          | _ ->
            (* printf "e: %s\n%!" (Pp.string_of_expr e); *)
            c_stat_of_expr c_types env decl_env e
        in
        loop env decl_env (stat :: stats) tl
      (* We only accept single declarations. *)
      | Decl [decl] -> begin
          match c_decl_of_decl c_types env decl with
          | Loc_cvar (loc, c_decl) ->
            let decl_env = StringMap.add c_decl.cv_name c_decl decl_env in
            let stat : C_sig.c_stat = { csl = loc; csk = Csdecl c_decl } in
            loop env decl_env (stat :: stats) tl
          | Env (_env, c_fun) ->
            (* local function forward declaration *)
            loop (add_fun c_fun env) decl_env stats tl
        end
      | While { cond; body } ->
        let while_cond = c_expr_of_expr c_types env decl_env cond in
        let stmts = For_memcad.stmts_of_stmt body in
        let while_body =
          snd (c_stats_of_stmts c_types env decl_env stmts)
        in
        let no_unrolling = None in (* hard-coded "no loop unrolling" *)
        let csk : C_sig.c_statk =
          Cswhile (while_cond, while_body, no_unrolling) in
        let stat : C_sig.c_stat = { csl; csk } in
        loop env decl_env (stat :: stats) tl
      | If { cond; then_branch; else_branch } ->
        let stat : C_sig.c_stat =
          let then_stmts = For_memcad.stmts_of_stmt then_branch in
          let else_stmts = match else_branch with
            | None -> []
            | Some else_branch -> For_memcad.stmts_of_stmt else_branch
          in
          { csl;
            csk = Csif (c_expr_of_expr c_types env decl_env cond,
                        snd (c_stats_of_stmts
                               c_types env decl_env then_stmts),
                        snd (c_stats_of_stmts
                               c_types env decl_env else_stmts)) }
        in
        loop env decl_env (stat :: stats) tl
      | Break ->
        let stat : C_sig.c_stat = { csl; csk = Csbreak } in
        loop env decl_env (stat :: stats) tl
      | Compound stmts ->
        let env, c_stats = c_stats_of_stmts c_types env decl_env stmts in
        let stat : C_sig.c_stat = { csl; csk = Csblock c_stats } in
        loop env decl_env (stat :: stats) tl
      | Null -> (* null statements are converted to an empty block;
                       since it's equivalent but supported by memcad's AST *)
        let stat : C_sig.c_stat = { csl; csk = Csblock [] } in
        loop env decl_env (stat :: stats) tl
      | Continue ->
          let stat : C_sig.c_stat = { csl ; csk = Cscontinue } in
          loop env decl_env (stat :: stats) tl
(*
      | LabelStmt _ -> unimp "LabelStmt"
      | CaseStmt _ ->
        (* don't implement: should be handled by switchToIf *)
        unimp "CaseStmt"
      | DefaultStmt _ ->
        (* don't implement: should be handled by switchToIf *)
        unimp "DefaultStmt"
      | SwitchStmt _ ->
        (* don't implement: should be handled by switchToIf *)
        unimp "SwitchStmt"
      | GotoStmt _ -> unimp "GotoStmt"
      | ForStmt _ ->
        (* don't implement: should be handled by forToWhile *)
        unimp "ForStmt"
      | DoStmt _ ->
        (* don't implement: should be handled by doToWhile *)
        unimp "DoStmt"
      | DeclStmt _ -> unimp "DeclStmt"
*)
      | _ -> unimp (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node stmt))
  in
  (* We build the list in reverse. *)
  let env, stats = loop env decl_env [] stmts in
  (env, List.rev stats)

let make_decl_env (env: env)
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (function_type: Clang.Ast.function_type): env * C_sig.c_var StringMap.t =
  let init = (env, StringMap.empty) in
  match function_type.parameters with
  | Some { non_variadic } ->
    List.fold_left
      (fun (env, decl_env)
          ({ desc = { name; qual_type }} : Clang.Ast.parameter) ->
         let env, parm = c_var_of_parm_decl env c_types name qual_type in
         (env, StringMap.add parm.cv_name parm decl_env))
      init
      non_variadic
  | None -> init

(* find structs without typedef used for the first time in some statements *)
let collect_structs_no_typedef_in_stmts
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (stmt: Clang.Ast.stmt): env =
  let module Visitor = struct
    module Applicative = Traverse.Applicative.Fold (struct type t = env end)
    let hook : type a . a Refl.refl -> (a -> env -> env) -> (a -> env -> env) =
    fun refl super x env ->
      match (refl, x) with
      | (Clang.Ast.Refl_decl_desc, Var decl) ->
          let env = super x env in
          add_struct_type decl.var_type env c_types
      | _ -> super x env
  end in
  let module Visit = Refl.Visit.Make (Visitor) in
  Visit.visit [%refl: Clang.Ast.stmt] [] stmt env

(*
  List.fold_left
    (fun env s -> match s with
       | { s = DeclStmt decls } ->
         List.fold_left
           (fun env d -> match d with
              | { d = VarDecl (ty, _name, _init) } ->
                add_struct_type ty env c_types
              | _ -> env)
           env
           decls
       | _ -> env)
    env
    stmts
*)

let rec collect_structs_no_typedef_in_decls
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decls: Clang.Decl.t list): env =
  match decls with
  | [] -> env
  | { desc = (RecordDecl { keyword = keyword1; name = name1; fields })}
    :: { desc = Field {
        qual_type = {
          desc = Elaborated {
            keyword = keyword2;
            named_type = { desc = Record { name = IdentifierName name2 }};
          }
        } as qual_type;
        name;
       } } :: tl when keyword1 = keyword2 && name1 = name2 ->
    let env = add_struct_type qual_type env c_types in
    let env = collect_structs_no_typedef_in_decls c_types env fields in
    collect_structs_no_typedef_in_decls c_types env tl
  | { desc = RecordDecl _ } :: tl ->
    (* this is a forward declaration introduced by clang, just ignore it
       as it is not a field *)
    collect_structs_no_typedef_in_decls c_types env tl
  | { desc = Field { qual_type }} :: tl ->
      let env = add_struct_type qual_type env c_types in
      collect_structs_no_typedef_in_decls c_types env tl
  | field :: _ ->
      Log.fatal_exn "Unsupported field: %s"
        (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node field))
(*
(* find structs without typedef used for the first time in some declarations *)
let rec collect_structs_no_typedef_in_decls
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decls: Clang.Decl.t list): env =
  match decls with
  | [] -> env
  | { d = RecordDecl (_ttk, name1, Some members, _) }
    :: { d = FieldDecl {
        fd_type = ({
          tl = ElaboratedTypeLoc {
              tl = RecordTypeLoc (kind, name2);
              tl_type;
            }
        } as ty);
        fd_name = name;
        fd_bitw = bitwidth;
        fd_init = init;
      } } :: tl when name1 = name2 ->
    let env = add_struct_type ty env c_types in
    let env = collect_structs_no_typedef_in_decls c_types env members in
    collect_structs_no_typedef_in_decls c_types env tl
  | { d = RecordDecl (_ttk, _name, None (* members *), _) } :: tl ->
    (* this is a forward declaration introduced by clang, just ignore it
       as it is not a field *)
    collect_structs_no_typedef_in_decls c_types env tl
  | { d = FieldDecl { fd_type = ty;
                      fd_name = name;
                      fd_bitw = bitwidth;
                      fd_init = init;
                      fd_index } } :: tl ->
    let env = add_struct_type ty env c_types in
    collect_structs_no_typedef_in_decls c_types env tl
  | { d } :: tl ->
    print_endline (Pp.string_of_decl_ d);
    Log.fatal_exn "collect_structs_no_typedef_in_decls: \
                   Only FieldDecls allowed within RecordDecl"
*)

let c_fun_body_of_stmt
    (name: string)
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (ty: Clang.Ast.function_type)
    (body: Clang.Ast.stmt): env * C_sig.c_block =
  let env = collect_structs_no_typedef_in_stmts c_types env body in
  let stmts = For_memcad.stmts_of_stmt body in
  let delayed_inits = (* if this is the main function, we have to prepend inits
                         of global variables to its statements *)
    if name <> "main" then []
    else begin
      saw_main := true;
      collect_delayed_init_stmts env
    end
  in
  let env, decl_env = make_decl_env env c_types ty in
  c_stats_of_stmts c_types env decl_env (delayed_inits @ stmts)

(* transform each constant of an enum into an integer variable declaration *)
let int_vars_of_enum enumerators =
  let expr_of_int i location =
    For_memcad.integer_literal ~location i in
  let rec loop acc prev_expr = function
    | [] -> List.rev acc
    | ({ desc = { constant_name; constant_init }}
         : Clang.Ast.enum_constant) as decl :: decls ->
      let constant_init = match constant_init with
        | Some init_expr -> init_expr
        | None -> (* increment previous one *)
          let int_one = expr_of_int 1 (Clang.Ast.location_of_node decl) in
          { prev_expr with desc =
            BinaryOperator { lhs = prev_expr; kind = Add; rhs = int_one }}
      in
      loop
        (({ decl with desc =
            Var (Clang.Ast.var constant_name For_memcad.int
              ~var_init:constant_init)} : Clang.Ast.decl) :: acc)
        constant_init
        decls
  in
  match enumerators with
  | [] -> []
  | decl :: _ ->
    let init_expr = expr_of_int (-1) (Clang.Ast.location_of_node decl) in
    loop [] init_expr enumerators

(*
let rec collect_decls
    (clang: Clang.Api.clang)
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decls: Clang.Ast.decl list): env =
  match decls with
  | [] -> env
  | { d = EmptyDecl } :: tl -> collect_decls clang c_types env tl
  | { d = FunctionDecl (ty, DN_Identifier name, maybe_body) } :: tl ->
    (* Create the head of the function (without body), first,
       so that name lookups within the (eventual) body work for the
       currently processed function name. *)
    let env, c_fun = create_c_fun_forward_decl clang c_types env ty name in
    (* Log.info "c_fun: %a" (C_utils.c_fun_fpri "") c_fun; *)
    begin match maybe_body with
      | None -> (* Forward function declaration (without def.) / signature *)
        collect_decls clang c_types (add_fun c_fun env) tl
      | Some stmts -> (* Function definition *)
        let (env, c_fun) =
          let env' =
            if StringMap.mem name env.prog.cp_funs
            then env (* don't add the header of the function again in the env.
                        we already saw it in a forward declaration *)
            else add_fun c_fun env
          in
          let env, body =
            c_fun_body_of_stmts name clang c_types env' ty stmts
          in
          (env, { c_fun with cf_body = body })
        in
        collect_decls clang c_types (add_fun c_fun env) tl
    end
  (* Clang turns this code:
       typedef struct foo { int a } bar;
     into this:
       struct foo { int a };
       typedef struct foo bar;
     but there is no way to express these things in the memcad AST,
     and the memcad parser doesn't parse it, so we match this construct
     explicitly and transform it to the appropriate memcad AST. *)
  | { d = RecordDecl (_, name1, Some members, _) }
    :: { d = TypedefDecl (
        { tl = ElaboratedTypeLoc { tl = RecordTypeLoc (_, name2);
                                   tl_type }
             (* Handle "typedef struct foo *Foo;" (where struct foo was not
                yet defined) specially, as well. *)
             | PointerTypeLoc { tl = ElaboratedTypeLoc {
                 tl = RecordTypeLoc (_, name2);
                 tl_type } }
             (* handles: "typedef struct { char c; } buf[2];" *)
             | ConstantArrayTypeLoc ({ tl = ElaboratedTypeLoc {
                 tl = RecordTypeLoc (_, name2);
                 tl_type }
               }, _) }, name) }
    :: tl when name1 = name2 ->
    let c_type = DenseIntMap.find tl_type.t_self c_types in
    let env = add_type name c_type env in
    let env = collect_structs_no_typedef_in_decls c_types env members in
    collect_decls clang c_types env tl
  (* There may be other typedefs involving a preceding record definition,
     so this case is printed with a better diagnostic than the catch-all
     case below. *)
  | { d = RecordDecl (_, name, members, _) as decl }
    :: { d = TypedefDecl _ as tdef }
    :: tl ->
    Log.fatal_exn "unsupported record/typedef combination: [%s;\n%s;]"
      (Pp.string_of_decl_ decl)
      (Pp.string_of_decl_ tdef)
  | { d = RecordDecl (_, name, maybe_members, _) } :: tl ->
    let env = match maybe_members with
      | None -> env
      | Some members -> collect_structs_no_typedef_in_decls c_types env members
    in
    (* a record decl without a typedef will allow to see a new type only
       later once it is used *)
    collect_decls clang c_types env tl
  | { d = TypedefDecl (ty, name) } :: tl ->
    let c_type = DenseIntMap.find ty.tl_type.t_self c_types in
    collect_decls clang c_types (add_type name c_type env) tl
  | { d = VarDecl (ty, name, maybe_init) } as decl :: tl ->
    (* var decl at file scope with optional initialization *)
    let env = add_struct_type ty env c_types in
    begin match maybe_init with
      | None -> begin match c_decl_of_decl clang c_types env decl with
          | Loc_cvar (loc, c_var) ->
            collect_decls clang c_types (add_var c_var env) tl
          | Env _ -> assert(false);
        end
      | Some init ->
        let env = add_delayed_init decl env in
        collect_decls clang c_types env
          ({ decl with d = VarDecl (ty, name, None) } :: tl)
    end
  | { d = EnumDecl (_name, enumerators) } :: tl ->
    let var_decls = int_vars_of_enum clang enumerators in
    collect_decls clang c_types env (var_decls @ tl)
  | { d = LinkageSpecDecl (decls, lang) } :: tl ->
    collect_decls clang c_types (collect_decls clang c_types env decls) tl
  | { d = NamespaceDecl (name, inline, decls) } :: tl ->
    let env = { env with ns = env.ns ^ name ^ "::" } in
    collect_decls clang c_types (collect_decls clang c_types env decls) tl
  | { d = EnumConstantDecl    _ } :: _ ->
    Log.fatal_exn "EnumConstantDecl found at file scope"
  | { d = FieldDecl           _ } :: _ ->
    Log.fatal_exn "FieldDecl found at file scope"
  | { d = ParmVarDecl         _ } :: _ ->
    Log.fatal_exn "ParmVarDecl found at file scope"
  | { d = TranslationUnitDecl _ } :: _ ->
    Log.fatal_exn "nested TranslationUnitDecl found"
  | { d } :: _ ->
    Log.fatal_exn "%s" (Pp.string_of_decl_ d)

let array_iteri2 f a1 a2 =
  let n = Array.length a1 in
  assert(n = Array.length a2);
  for i = 0 to n - 1 do
    f i a1.(i) a2.(i)
  done
*)

let rec collect_decls
    (c_types: C_sig.c_type Clang.Type.Map.t)
    (env: env)
    (decls: Clang.Ast.decl list): env =
  match decls with
  | [] -> env
  | { desc = Function { function_type; name = IdentifierName name; body }} :: tl ->
    let env, c_fun =
      create_c_fun_forward_decl c_types env function_type name in
    (* Log.info "c_fun: %s" (C_utils.c_fun_fpri "") c_fun; *)
    begin match body with
      | None -> (* Forward function declaration (without def.) / signature *)
        collect_decls c_types (add_fun c_fun env) tl
      | Some stmt -> (* Function definition *)
        let (env, c_fun) =
          let env' =
            if StringMap.mem name env.prog.cp_funs
            then env (* don't add the header of the function again in the env.
                        we already saw it in a forward declaration *)
            else add_fun c_fun env
          in
          let env, body =
            c_fun_body_of_stmt name c_types env' function_type stmt
          in
          (env, { c_fun with cf_body = body })
        in
        collect_decls c_types (add_fun c_fun env) tl
    end
  | { desc = RecordDecl { keyword = Struct; name = name1; fields }}
    :: { desc = TypedefDecl (
        { name;
          underlying_type = ({ desc = Elaborated { named_type = { desc =
            Record { name = IdentifierName name2 } }}} as elaborated)
             (* Handle "typedef struct foo *Foo;" (where struct foo was not
                yet defined) specially, as well. *)
             | ({ desc = Pointer { desc = Elaborated {
                 named_type = { desc = Record { name = IdentifierName name2 } }}}} as elaborated)
             (* handles: "typedef struct { char c; } buf[2];" *)
             | ({ desc = ConstantArray { element = { desc = Elaborated {
                 named_type = { desc = Record { name = IdentifierName name2 } }}}}} as elaborated)})}
    :: tl when name1 = name2 ->
    let c_type = Clang.Type.Map.find elaborated c_types in
    let env = add_type name c_type env in
    let env = collect_structs_no_typedef_in_decls c_types env fields in
    collect_decls c_types env tl
  (* There may be other typedefs involving a preceding record definition,
     so this case is printed with a better diagnostic than the catch-all
     case below. *)
  (* It can be valid (and supported) to have a record decl followed by
     an (unrelated) type def.*)
  (*
  | ({ desc = RecordDecl { keyword = Struct } } as decl)
    :: ({ desc = TypedefDecl _ } as tdef)
    :: tl ->
    Log.fatal_exn "unsupported record/typedef combination: [%s;\n%s;] at %a"
      (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node decl))
      (Clang.get_cursor_spelling (Clang.Ast.cursor_of_node tdef))
      (fun fmt -> Clang.Ast.pp_source_location fmt)
      (Clang.Ast.location_of_node tdef)
   *)
  | { desc = RecordDecl { keyword = Struct; fields }} :: tl ->
    let env =
      collect_structs_no_typedef_in_decls c_types env fields
    in
    (* a record decl without a typedef will allow to see a new type only
       later once it is used *)
    collect_decls c_types env tl
  | { desc = TypedefDecl { name; underlying_type }} :: tl ->
    let c_type = Clang.Type.Map.find underlying_type c_types in
    collect_decls c_types (add_type name c_type env) tl
  | { desc = Var ({ var_name; var_type; var_init } as var)} as decl :: tl ->
    (* var decl at file scope with optional initialization *)
    let env = add_struct_type var_type env c_types in
    begin match var_init with
      | None -> begin match c_decl_of_decl c_types env decl with
          | Loc_cvar (loc, c_var) ->
            collect_decls c_types (add_var c_var env) tl
          | Env _ -> assert(false);
        end
      | Some init ->
        let env = add_delayed_init decl env in
        collect_decls c_types env
          ({ decl with desc = Var { var with var_init = None }} :: tl)
    end
  | { desc = EnumDecl { constants }} :: tl ->
      let var_decls = int_vars_of_enum constants in
      collect_decls c_types env (var_decls @ tl)
  | decl :: tl ->
      Log.warn "collect_decls: ignored unsupported decl %a"
        (fun fmt -> Clang.Ast.pp_source_location fmt)
        (Clang.Ast.location_of_node decl);
      collect_decls c_types env tl

let decls_from_decl (debug: bool) (decls: Clang.Ast.decl list): env =
  C_process.max_c_var_id := 0;
  let env = { prog = C_utils.empty_unit;
    ns = ""; (* FBR: I think this namespace is never used *)
    delayed_init_vars = StringSet.empty;
    delayed_init_decls = [] } in
    let types =
      let module Visitor = struct
        type accu = (Clang.Ast.source_location * Clang.Type.t) list
        module Applicative =
          Traverse.Applicative.Env
            (struct type t = Clang.Ast.source_location end)
          (Traverse.Applicative.Fold
            (struct type t = accu end))
        let hook :
          type a . a Refl.refl ->
            (a -> Clang.Ast.source_location -> accu -> accu) ->
            (a -> Clang.Ast.source_location -> accu -> accu) =
        fun refl super x loc accu ->
          match refl with
          | Clang.Ast.Refl_expr ->
              begin
                let accu = super x loc accu in
                match Clang.Type.of_node x with
                | { desc = (InvalidType | Record _) } -> accu
                | ty -> (Clang.Ast.location_of_node x, ty) :: accu
              end
          | Clang.Ast.Refl_qual_type ->
              begin match x.desc with
              | Record _ -> accu
              | _ -> (loc, x) :: super x loc accu
              end
          | Clang.Ast.Refl_node ->
              super x (Clang.Ast.location_of_node x) accu
          | _ -> super x loc accu
      end in
      let module Visit = Refl.Visit.Make (Visitor) in
      List.fold_left (fun accu decl ->
        Visit.visit [%refl: Clang.Ast.decl] [] decl
          (Clang.Ast.location_of_node decl) accu)
        [] decls in
    let _cache_seen, c_types =
      List.fold_left
        (fun (cache_seen, c_types) (loc, c_type) ->
          let cache_seen, mctype =
            mctype_of_ctyp loc cache_seen c_type
          in
          cache_seen, Clang.Type.Map.add c_type mctype c_types)
        ((Clang.Type.Map.empty, Clang.Type.Set.empty), Clang.Type.Map.empty)
        types in
    if debug then (* display the map of types *)
      begin
        Log.info "### types";
        Clang.Type.Map.iter (
          (fun (t1 : Clang.Type.t) t2 ->
             Log.info ("### type ####################\n" ^^
                       "clang: %s\n" ^^
                       "### ----------------------------\n" ^^
                       "memcad: %a")
              (Clang.get_type_spelling t1.cxtype)
              (C_utils.c_type_fpr ~decl:false) t2))
          c_types;
      end;
    let res = collect_decls c_types env decls in
    F.printf "%a\n" (C_utils.c_prog_fpri "  ") res.prog;
    if (!saw_global_init && (not !saw_main)) then
      Log.warn "c_prog_from_decl: ignored global variable initialization";
    res

let c_prog_from_decls (decls: env): C_sig.c_prog =
  C_process.bind_c_prog decls.prog
