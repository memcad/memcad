(** This file is part of the MemCAD analyzer
 **
 ** GNU General Public License
 **
 ** Version v2016.03.00, March 2016
 ** Authors: Xavier Rival, Francois Berenger, Josselin Giet, Huisong Li,
 **          Jiangchao Liu, Thierry Martinez, Pascal Sotin,
 **          Antoine Toubhans, Pippijn Van Steenhoeven
 ** Copyright (c) 2016 INRIA
 **
 ** File: main.ml
 **       entry point, and testing
 ** Xavier Rival, 2011/05/22 *)
open Flags
open Lib
open Data_structures
open Sv_def

open Ast_sig
open C_analysis_sig
open Spec_sig

open Sv_utils

(** Error report *)
module Log =
  Logger.Make(struct let section = "main____" and level = Log_level.DEBUG end)
module T = Timer.Timer_Mod( struct let name = "Main" end )

(** Exception reporting *)
let exn_report (e: exn): unit =
  begin
    match e with
    | Apron.Manager.Error exc ->
        let open Apron.Manager in
        Log.fatal ("Apron failure:\n" ^^
                   "exn = %s\n" ^^
                   "funid = %s\n" ^^
                   "msg = %s")
          (string_of_exc exc.exn) (string_of_funid exc.funid) exc.msg
    | _ -> Log.fatal "%s" (Printexc.to_string e)
  end;
  exit 1

(** Parsers *)

(* Basic parsers for parameters *)
let ilines_parser =
  Lib.read_from_file "ind" Ind_parser.main_ilines Ind_lexer.token
let specs_parser =
  Lib.read_from_file "ind" Ind_parser.main_specs Ind_lexer.token
let array_ind_parser =
  Lib.read_from_file "ind" Ind_parser.array_indlist Ind_lexer.token

(* Testing parsers *)
let meta_test_parser
    (msg: string)
    (f_parse: (Lexing.lexbuf -> 'b) -> Lexing.lexbuf -> 'a)
    (f_lexe: Lexing.lexbuf -> 'b)
    (f_post: 'a -> unit)
    (files: string list): unit =
  Log.info "\nTest of the lexer of %s" msg;
  let parse_ind_file =
    Lib.read_from_file msg f_parse f_lexe in
  List.iter
    (fun file ->
      try
        Log.info "parsing %s..." file;
        let x = parse_ind_file file in
        Log.info "  [Ok]";
        f_post x
      with
      | e -> exn_report e
    ) files;
  Log.info "Finished!"
(* Old C parser *)
let run_old_parser (f: C_sig.c_prog -> unit): string list -> unit =
  meta_test_parser "c mini programs" C_parser.entry C_lexer.token f

(* handle the user-provided and coma-separated list of include *)
(* dirs for additional C headers *)
let expand_include_dirs () =
  if !include_dirs = "" then
    []
  else if String.contains !include_dirs ',' then
    let dirs = String.split_on_char ',' !include_dirs in
    List.map (fun dir -> "-I" ^ dir) dirs
  else
    ["-I" ^ !include_dirs]

let clangml_parse (file: string): Clang.Ast.translation_unit =
  let clang_opts0 =
    ["-m32"; "-include"; !clang_header_fn; "-w";
     "-DMEMCAD_INRIA"] @ expand_include_dirs () in
  let clang_opts =
    if !Flags.clang_macros then
      clang_opts0
        (* memcad assumes a 32bit environment, hence the -m32 option *)
      @ List.map Clang.Command_line.include_directory
          (Clang.get_compiler_include_directories ())
      @ List.map Clang.Command_line.define
          (Clang.get_compiler_predefined_macros ())
    else clang_opts0 in
  let translation_unit =
    Clang.Ast.parse_file ~command_line_args:clang_opts file in
  Clang.Ast.format_diagnostics Clang.warning_or_error Format.err_formatter
    translation_unit;
  translation_unit

(* parse C files with clangml, with optional storing/loading to/from
 * dump files *)
let run_clangml_parser
    (c_parser: C_sig.c_prog -> unit) (files: string list): unit =
  List.iter
    (fun file ->
      let dump_fn = file ^ ".dump" in
      let c_prog =
         if !Flags.load_dump then
           with_in_file dump_fn
             (fun input ->
                let decls = (Marshal.from_channel input : Transform.env) in
                Transform.c_prog_from_decls decls)
         else
           begin
             if not (Sys.file_exists !clang_header_fn) then
               Log.fatal_exn "main.ml: run_clangml_parser: file not found: %s"
                 !clang_header_fn;
             let translation_unit = clangml_parse file in
             let after_transformations =
               List.map For_memcad.transform_decl translation_unit.desc.items in
             let decls =
               Transform.decls_from_decl !test_verbose after_transformations in
             if !Flags.dump_parse then
               with_out_file dump_fn
                 (fun out -> Marshal.to_channel out decls []);
             Transform.c_prog_from_decls decls
           end in
       c_parser c_prog
    ) files

let run_c_parser () =
  if !use_old_parser then
    run_old_parser
  else
    run_clangml_parser

(** Initialize the domain *)
type ind_source =
  | IS_none
  | IS_ind_file_name of string
  | IS_spec_file_content of p_iline list
(* The function below is experimental code, that tries to see which
 * inductive definitions describe some form of list, and that tries
 * to extract all relevant information about these inductive
 * definitions *)
let exp_search_lists (has_indfile: bool) (domstruct: shape_dom): unit =
  let has_list =
    let rec aux = function
      | Shd_flat | Shd_all | Shd_inds _ | Shd_sepsum | Shd_tvl -> false
      | Shd_list -> true
      | Shd_prod (s0, s1) | Shd_sep (s0, s1) -> aux s0 || aux s1 in
    aux domstruct in
  if has_list && has_indfile && not !enable_array_domain then
    List_utils.exp_search_lists ( )
  else if !enable_array_domain && has_indfile then
    Array_ind_utils.exp_search_array_lists ( )
(* Global inductive definition initialization function *)
let init_inductive_definitions
    ~(type_context: Parsed_spec.type_context)
    ~(indspec: ind_source)
    ~(shapedom_struct: shape_dom): unit =
  let ind_pre_analysis () =
    (* antoine: do the inductive defs pre-analysis *)
    if !reduction_mode_selector != Rm_disabled then
      begin
        T.app1 "ind_preds_init" Ind_analysis.ind_preds_init ();
        T.app1 "seg_preds_init" Ind_analysis.seg_preds_init ()
      end in
  let inductives_initialization = function
    | IS_none -> false (* analysis with no inductive definition *)
    | IS_ind_file_name ifile ->
        (* some inductive definitions to parse beforehand *)
        begin
          if !enable_array_domain then
            let al = array_ind_parser ifile in
            let al =
              List.filter_map (Array_ind_utils.elaborate_array_ind type_context)
                al in
            Array_ind_utils.array_ind_init al
          else
            let l = ilines_parser ifile in
            Ind_utils.ind_init type_context l
        end;
        ind_pre_analysis ();
        true
    | IS_spec_file_content l ->
        Ind_utils.ind_init type_context l;
        ind_pre_analysis ();
        true in
  let has_indfile = inductives_initialization indspec in
  exp_search_lists has_indfile shapedom_struct;
  if has_indfile then Array_ppred_utils.search_ppred ( )
(* Reading the goals from the file *)
let read_goals_from_spec_file ~type_context (specfname: string)
    : s_goal StringMap.t * p_iline list =
  try
    Log.info "Parsing spec file %s..." specfname;
    let ilines = specs_parser specfname in
    F.printf "Spec file content:\n";
    List.iter (F.printf "%a\n" Spec_utils.p_iline_fpr) ilines;
    let goals =
      Ind_utils.p_ilines_translate (Result.get_ok (type_context None)) ilines in
    Log.info "Parsing spec file %s done, finishing" specfname;
    goals, ilines
  with exc ->
    F.printf "terminated due to exception: %s" (Printexc.to_string exc);
    exit 1

type type_map = Clang.Decl.t StringMap.t

let get_type_decl_name (decl: Clang.Decl.t): string option =
  match decl.desc with
  | RecordDecl record -> Some record.name
  | TypedefDecl typedef -> Some typedef.name
  | _ -> None

let rec get_typedef_underlying_type (ty: Clang.cxtype): Clang.cxtype =
  match Clang.get_type_kind ty with
  | Typedef ->
      get_typedef_underlying_type (Clang.get_typedef_decl_underlying_type
        (Clang.get_type_declaration ty))
  | _ -> ty

let get_offset_item ((ty: Clang.cxtype), (offset: int))
      (item: FPSpec.symbolic_offset_item) =
  match item with
  | Field_name field_name ->
      let ty = get_typedef_underlying_type ty in
      begin
        match Clang.get_type_kind ty with
        | Elaborated | Record -> ()
        | kind ->
            Log.fatal_exn "Cannot access field %s in %s type %s"
              field_name (Clang.get_type_kind_spelling kind)
              (Clang.get_type_spelling ty)
      end;
      let field_decl = Clang.ext_type_get_field_decl ty field_name in
      if Clang.get_cursor_kind field_decl <> FieldDecl then
        begin
          let available_fields = Clang.list_of_type_fields ty in
          Log.fatal_exn
            "Cannot find field %s in type %s (available fields are: %a)"
            field_name
            (Clang.get_type_spelling ty)
            (Format.pp_print_list
               ~pp_sep:(fun fmt () -> Format.pp_print_string fmt ", ")
              (fun fmt field ->
                Format.pp_print_string fmt (Clang.get_cursor_spelling field)))
              available_fields
        end;
      let field_offset = Clang.cursor_get_offset_of_field field_decl in
      if field_offset mod 8 <> 0 then
        Log.fatal_exn "Bitfield %s in type %s is not supported" field_name
          (Clang.get_type_spelling ty);
      Clang.get_cursor_type field_decl, offset + field_offset / 8
  | Array_subscript index ->
      let elt_ty = Clang.get_element_type ty in
      elt_ty, offset + index * Clang.type_get_size_of elt_ty

let make_type_context (cfile: string): Parsed_spec.type_context =
  let lazy_type_map = lazy (
    let ast = clangml_parse cfile in
    let add_decl (map: type_map) (decl: Clang.Decl.t): type_map =
      match get_type_decl_name decl with
      | Some name -> StringMap.add name decl map
      | None -> map in
    List.fold_left add_decl StringMap.empty ast.desc.items) in
  fun (c_type_ref_opt: FPSpec.c_type_ref option)
      : (Parsed_spec.c_type_spec, [`Not_found]) result ->
    match
      match c_type_ref_opt with
      | None -> Ok None
      | Some c_type_ref ->
          let type_map = Lazy.force lazy_type_map in
          match StringMap.find c_type_ref type_map with
          | exception Not_found ->
              Log.warn "Unknown type %s" c_type_ref;
              Error `Not_found
          | decl -> Ok (Some decl)
    with
    | Error `Not_found -> Error `Not_found
    | Ok decl_opt ->
        let c_type_spec (off: FPSpec.symbolic_offset): Offs.t =
          let decl =
            match off.c_type_ref_opt, decl_opt with
            | None, None ->
                Log.fatal_exn
                  "line %d: rule use symbolic offset without type annotation"
                  off.pos.pos_lnum
            | Some c_type_ref, _ ->
                begin
                  let type_map = Lazy.force lazy_type_map in
                  match StringMap.find c_type_ref type_map with
                  | exception Not_found ->
                      Log.fatal_exn "line %d: unknown type %s"
                        off.pos.pos_lnum c_type_ref
                  | decl -> decl
                end
            | None, Some decl -> decl in
          let ty = Clang.get_cursor_type (Clang.Ast.cursor_of_node decl) in
          let _ty, offset = List.fold_left get_offset_item (ty, 0) off.items in
          Offs.of_int offset in
        Ok c_type_spec

(** Starting the analysis *)
let do_analyze_one
    ~(type_context: Parsed_spec.type_context)
    ~(indspec: ind_source)        (* source for inductive definition *)
    ~(cfile: string)              (* input C file *)
    ~(shapedom_struct: shape_dom) (* shape domain structure *)
    ~(mainfun: string)            (* main function *)
    ~(goal: analysis_goal)        (* optional pre/post/args *)
    ~(repeat_num: int)            (* stress-testing *)
    : unit =
  let sep = String.make 64 '=' in
  Log.info "%s\nANALYSIS START: Function do_analyze_one entry\n%s" sep sep;
  (* Inductive definitions initializations *)
  (*  TODO: possibly set source to something else than ind file *)
  (*  TODO: we call this function for each goal so some work is duplicated *)
  let () = (* ensure return type is unit to prevent partial application *)
    init_inductive_definitions ~indspec:indspec
      ~shapedom_struct:shapedom_struct ~type_context in
  (* Numerical domain construction *)
  let mod_apron =
    let mod_pre_apron =
      match !nd_selector with
      | ND_box -> (module Nd_apron.PA_box:   Nd_apron.PRE_APRON)
      | ND_oct -> (module Nd_apron.PA_oct:   Nd_apron.PRE_APRON)
      | ND_pol -> (module Nd_apron.PA_polka: Nd_apron.PRE_APRON) in
    let module MPA = (val mod_pre_apron: Nd_apron.PRE_APRON) in
    let msa =
      if !stat_apron then
        (module Nd_apron.Abstract1_stat: Nd_apron.ABSTRACT1_TYPE)
      else (module Apron.Abstract1: Nd_apron.ABSTRACT1_TYPE) in
    let module MSA = (val msa: Nd_apron.ABSTRACT1_TYPE) in
    let apr = (module Nd_apron.Build_apron( MSA )( MPA ): Nd_sig.DOM_NUM_NB) in
    let module APR = (val apr: Nd_sig.DOM_NUM_NB) in
    if !Flags.timing_apron then
      (module Nd_timing.Dom_num_nb_timing( APR ): Nd_sig.DOM_NUM_NB)
    else (module APR: Nd_sig.DOM_NUM_NB) in
  let module MApron  = (val mod_apron: Nd_sig.DOM_NUM_NB) in
  let module MAeq = Nd_add_eqs.Add_eqs( MApron ) in
  let mod_eq =
    if !enable_eq_pack then
      (module Nd_add_eqp.Add_eq_partition( MAeq ): Nd_sig.DOM_NUM_NB)
    else (module MAeq: Nd_sig.DOM_NUM_NB) in
  let module MAeq = (val mod_eq: Nd_sig.DOM_NUM_NB) in
  let module MAineq = Nd_add_ineqs.Add_ineqsAll( MAeq ) in
  let module MAdiseq = Nd_add_diseqs.Add_diseqs( MAineq ) in
  let mod_dynenv =
    if !enable_dynenv then
      (module Nd_add_dyn_svenv.Add_dyn_svenv( MAdiseq ): Nd_sig.DOM_NUM_NB)
    else (module MAdiseq: Nd_sig.DOM_NUM_NB) in
  let module MAde    = (val mod_dynenv: Nd_sig.DOM_NUM_NB) in
  let module MNum    = Nd_add_bottom.Add_bottom( MAde ) in
  let module MVal0   = Dom_val_num.Make_val_num( MNum ) in
  assert (not (!enable_submem && !enable_array_domain));
  let mod_val1 =
    if !timing_value then
      (module Dom_val_num.Dom_val_timing( MVal0 ): Vd_sig.DOM_VALUE)
    else (module MVal0: Vd_sig.DOM_VALUE) in
  let module MVal1 = (val mod_val1: Vd_sig.DOM_VALUE) in
  (* Addition of the set domain if needed *)
  let mod_vcol =
    match !sd_selector with
    | SD_lin ->
        let mod_set =
          if !flag_dump_ops then
            (module Set_dump.Make( Set_lin.Set_lin ): Set_sig.DOM_SET)
          else (module Set_lin.Set_lin: Set_sig.DOM_SET) in
        let module MSet = (val mod_set: Set_sig.DOM_SET) in
        let mod_set =
          if !timing_valset then
            (module Nd_timing.Dom_set_timing( MSet ): Set_sig.DOM_SET)
          else (module MSet: Set_sig.DOM_SET) in
        let module MSet = (val mod_set: Set_sig.DOM_SET) in
        let module MVCol = Dom_set.DBuild( MVal1 )( MSet ) in
        (module MVCol: Vd_sig.DOM_VALCOL)
    | SD_setr s ->
        let module L = SETr.SymSing.Logic in
        let d =
          match s with
          | "bdd" ->
              let module D0 = SETr.Symbolic.BDD.MLBDD in
              let module D1 = SETr.SymSing.Sing.Make( D0 ) in
              (module D1 : SETr.Domain with
               type sym = int
               and type cnstr  = int L.t
               and type output = int L.t )
          | "lin" ->
              begin
                match SETr.get s with
                | SETr.SymSing d ->
                    let module D = (val d) in
                    (module D: SETr.Domain with
                     type sym = int
                     and type cnstr  = int L.t
                     and type output = int L.t )
                | _ ->  Log.fatal_exn "unbound SETr module: %s" s
              end
          | _ ->  Log.fatal_exn "unbound SETr module name: %s" s in
        let module D = (val d) in
        let module MPre =
          struct
            let name = s
            module D = (val d)
            let ctx = D.init ( )
          end in
        let mod_set =
          (module Set_setr.Make( MPre ): Set_sig.DOM_SET) in
        let module MSet = (val mod_set: Set_sig.DOM_SET) in
        let mod_set =
          if !timing_valset then
            (module Nd_timing.Dom_set_timing( MSet ) : Set_sig.DOM_SET)
          else (module MSet: Set_sig.DOM_SET) in
        let module MSet = (val mod_set: Set_sig.DOM_SET) in
        let module MVCol = Dom_set.DBuild( MVal1 )( MSet ) in
        (module MVCol: Vd_sig.DOM_VALCOL)
    | SD_quicr ->
        let mod_quickr = (* temporary: add timing and logging *)
          if false || !flag_dump_ops then
            (module Quicr_dump.Make( Quicr_lin ): Set_quicr.SDomain)
          else (module Quicr_lin: Set_quicr.SDomain) in
        let module MQuickr = (val mod_quickr: Set_quicr.SDomain) in
        let mod_set = (module Set_quicr.Make( MQuickr ): Set_sig.DOM_SET) in
        let module MSet = (val mod_set: Set_sig.DOM_SET) in
        let module MVCol = Dom_set.DBuild( MVal1 )( MSet ) in
        (module MVCol: Vd_sig.DOM_VALCOL)
    | SD_none ->
        let module MVCol = Dom_no_set.DBuild( MVal1 ) in
        (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mod_vcol: Vd_sig.DOM_VALCOL) in
  let mod_vcol =
    if !enable_submem then
      let module MSub = Dom_subm_graph.Submem in
      (module Dom_val_subm.Make_Vals_Subm( MVCol )( MSub ): Vd_sig.DOM_VALCOL)
    else (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mod_vcol: Vd_sig.DOM_VALCOL) in
  (* Add array *)
  let mod_vcol =
    if !enable_array_domain then
      (module Dom_val_array.Make_VS_Array( MVCol ): Vd_sig.DOM_VALCOL)
    else (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mod_vcol: Vd_sig.DOM_VALCOL) in
  let mod_vcol =
    if !timing_valset then
      let module Def = struct let name = "ValSet" end in
      (module Dom_timing.Dom_valcol_timing( MVCol )( Def ): Vd_sig.DOM_VALCOL)
    else (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mod_vcol: Vd_sig.DOM_VALCOL) in
  let mvcol =
    if !enable_seqdom then
      (module Seq_dom.DummyBuild ( MVCol ): Vd_sig.DOM_VALCOL)
    else (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mvcol: Vd_sig.DOM_VALCOL) in
  let mvcol =
    if !timing_valseq then
      let module Def = struct let name = "ValSeq" end in
      (module Dom_timing.Dom_valcol_timing( MVCol )( Def ): Vd_sig.DOM_VALCOL)
    else (module MVCol: Vd_sig.DOM_VALCOL) in
  let module MVCol = (val mvcol: Vd_sig.DOM_VALCOL) in
  (* shape domain construction *)
  let add_timing m r os =
    let module D = (val m: Dom_sig.DOM_MEM_LOW) in
    let s_inds =
      match os with
      | None -> StringSet.empty
      | Some s -> s in
    ignore (D.init_inductives SvGen.empty s_inds);
    if !r then
      (module Dom_timing.Dom_mem_low_timing( D ): Dom_sig.DOM_MEM_LOW)
    else (module D: Dom_sig.DOM_MEM_LOW) in
  let build_shape_ind (s: StringSet.t) =
    Log.info "Creating summarizing shape domain <%d inductives>"
      (StringSet.cardinal s);
    let m = (module Dom_mem_low_shape.DBuild( MVCol ): Dom_sig.DOM_MEM_LOW) in
    add_timing m timing_bshape (Some s) in
  let build_shape_flat () =
    Log.info "Creating flat shape domain (with no summarization)";
    let m = (module Dom_mem_low_flat.DBuild( MVCol ): Dom_sig.DOM_MEM_LOW) in
    add_timing m timing_fshape None in
  let build_shape_list () =
    Log.info "Creating ad-hoc list domain";
    let m = (module Dom_mem_low_list.DBuild( MVCol ): Dom_sig.DOM_MEM_LOW) in
    add_timing m timing_lshape None in
  let build_shape_sepsum () =
    Log.info "Creating ad-hoc list domain";
    let m = (module Dom_mem_low_sum_sep.DBuild( MVCol ): Dom_sig.DOM_MEM_LOW) in
    add_timing m timing_lshape None in
  let build_shape_tvl () =
    Log.info "Creating ad-hoc TVL domain";
    let m = (module Dom_mem_low_tvl.DBuild( MVCol ): Dom_sig.DOM_MEM_LOW) in
    add_timing m timing_lshape None in
  let rec sd_fpr fmt = function
    | Shd_flat -> F.fprintf fmt "[___]"
    | Shd_all -> F.fprintf fmt "[@ll]"
    | Shd_list -> F.fprintf fmt "[#list]"
    | Shd_sepsum -> F.fprintf fmt "[#sepsum]"
    | Shd_tvl -> F.fprintf fmt "[tvl]"
    | Shd_inds l -> F.fprintf fmt "[%a]" (stringlist_fpr ",") l
    | Shd_prod (d0, d1) -> F.fprintf fmt "(%a X %a)" sd_fpr d0 sd_fpr d1
    | Shd_sep (d0, d1) -> F.fprintf fmt "(%a * %a)" sd_fpr d0 sd_fpr d1 in
  let rec build_domain ind (sd: shape_dom) =
    Log.info "%sBuild_domain called %a" ind sd_fpr sd;
    match sd with
    | Shd_flat ->
        build_shape_flat ()
    | Shd_all ->
        let inds =
          StringMap.fold
            (fun s _ -> StringSet.add s) !Ind_utils.ind_defs StringSet.empty in
        build_shape_ind inds
    | Shd_list ->
        build_shape_list ()
    | Shd_sepsum ->
        build_shape_sepsum ()
    | Shd_tvl ->
        build_shape_tvl ()
    | Shd_inds lst ->
        let inds =
          List.fold_left
            (fun acc i ->
              Log.info "%sBuild domain adds inductive %s to domain" ind i;
              assert (StringMap.mem i !Ind_utils.ind_defs);
              StringSet.add i acc
            ) StringSet.empty lst in
        build_shape_ind inds
    | Shd_sep (d0, d1) ->
        let nind = "   " ^ ind in
        let m0 = build_domain nind d0 in
        let module D0 = (val m0: Dom_sig.DOM_MEM_LOW) in
        let m1 = build_domain nind d1 in
        let module D1 = (val m1: Dom_sig.DOM_MEM_LOW) in
        let module D = Dom_mem_low_sep.Dom_sep( D0 )( D1 ) in
        if !timing_sshape then
          (module Dom_timing.Dom_mem_low_timing( D ): Dom_sig.DOM_MEM_LOW)
        else
          (module D: Dom_sig.DOM_MEM_LOW)
    | Shd_prod (d0, d1) ->
        let nind = "   " ^ ind in
        let m0 = build_domain nind d0 in
        let module D0 = (val m0: Dom_sig.DOM_MEM_LOW) in
        let m1 = build_domain nind d1 in
        let module D1 = (val m1: Dom_sig.DOM_MEM_LOW) in
        let module D = Dom_mem_low_prod.Dom_prod( D0 )( D1 ) in
        if !timing_pshape then
          (module Dom_timing.Dom_mem_low_timing( D ): Dom_sig.DOM_MEM_LOW)
        else
          (module D: Dom_sig.DOM_MEM_LOW) in
  let mshape = build_domain "" shapedom_struct in
  let module MShape = (val mshape: Dom_sig.DOM_MEM_LOW) in
  (* Memory domain, expressions layer *)
  let module MMExprs = Dom_mem_exprs.DBuild( MShape ) in
  let m =
    if !timing_mem_exprs then
      (module
          Dom_mem_exprs.Dom_mem_exprs_timing( MMExprs ): Dom_sig.DOM_MEM_EXPRS)
    else (module MMExprs: Dom_sig.DOM_MEM_EXPRS) in
  let module MMExprs = (val m: Dom_sig.DOM_MEM_EXPRS) in
  (* Environment domain *)
  let module MEnv    = Dom_env.Dom_env( MMExprs ) in
  let m =
    if !timing_env then (module Dom_env.Dom_env_timing( MEnv ): Dom_sig.DOM_ENV)
    else (module MEnv: Dom_sig.DOM_ENV) in
  let module MEnv = (val m: Dom_sig.DOM_ENV) in
  (* Disjunction domain *)
  let m =
    if !timing_graph_encode then
      (module Graph_encode.Graph_encode_timing( Graph_encode.Graph_encode )
          : Dom_sig.GRAPH_ENCODE)
    else
      (module Graph_encode.Graph_encode: Dom_sig.GRAPH_ENCODE) in
  let module GEncode = (val m: Dom_sig.GRAPH_ENCODE) in
  let mod_disj =
    if !disj_selector then
      if !timing_disj then
        (module
            Dom_disj.Dom_disj_timing ( Dom_disj.Dom_disj( MEnv )( GEncode ) )
           : Dom_sig.DOM_DISJ)
      else
        (module Dom_disj.Dom_disj( MEnv )( GEncode ): Dom_sig.DOM_DISJ)
    else
      (module Dom_no_disj.Dom_no_disj( MEnv )( GEncode ): Dom_sig.DOM_DISJ) in
  let module MDisj = (val mod_disj: Dom_sig.DOM_DISJ) in
  (* C domain and analyzer *)
  let module Path_liveness = C_pre_analyze.Path_liveness in
  let module Var_liveness = C_pre_analyze.Var_liveness in
  let module Mpre_path_analysis =
    C_pre_analyze.Gen_pre_analysis( Path_liveness ) in
  let module Mpre_var_analysis =
    C_pre_analyze.Gen_pre_analysis( Var_liveness ) in
  let module MC      = Dom_c.Dom_C( MDisj ) in
  Log.info "abstract domain config:\n%a" MC.config_fpr ();
  let mod_analyse =
    let module MAnalyzer: C_analysis_sig.C_ANALYZE =
      C_analyze.Make( MC )( Mpre_path_analysis ) in
    if !timing_analyze then
      (module C_analysis_timing.C_analyze_timing(MAnalyzer)
      : C_analysis_sig.C_ANALYZE)
    else
      (module MAnalyzer: C_analysis_sig.C_ANALYZE) in
  let module MAnalyzer = (val mod_analyse: C_analysis_sig.C_ANALYZE) in
  (* Launching the analysis *)
  run_c_parser ()
    (fun cp ->
      let cp = C_process.process_c_prog cp in
      if !test_verbose then
        Log.info "%a" (C_utils.c_prog_fpri "    ") cp;
      if do_live_analysis then
        begin
          ignore (C_pre_analyze.live_prog mainfun cp);
          ignore (Mpre_var_analysis.live_prog mainfun cp)
        end;
      pp_config_report ();
      (* Materialization for array analysis *)
      (* TODO: this pre-analysis seems to be unused currently
       *  - variable mat_var is only written, but never read
       *  - commenting out the next block seems not to change the results
       * => consider moving this away after checking *)
      if !enable_array_domain then
        begin
          let module Mater = C_pre_analyze.Materialization in
          let module Mpre_Mater_analysis =
            C_pre_analyze.Gen_path_sensitive_pre_analysis( Mater ) in
          let acc = Mpre_Mater_analysis.pre_prog mainfun cp in
          Array_node.mat_var := Mpre_Mater_analysis.mat_resolve acc;
          Log.info "%a" Mpre_Mater_analysis.t_fpr
            (Mpre_Mater_analysis.pre_prog mainfun cp)
        end;
      for i = 1 to repeat_num do
        MAnalyzer.analyze_prog mainfun goal cp
      done
    ) [ cfile ];
  Statistics.show_gc_statistics ( );
  Timer.print_timing_infos ();
  Nd_apron.pp_histo ( );
  Log.info "Function do_analyze_one exit"


(* Inference of inductive definitions *)
let do_infer_ind (filename: string) (out_file: string): unit =
  run_c_parser ()
    (fun cp ->
      let cp = C_process.process_c_prog cp in
      if !test_verbose then Log.info "%a" (C_utils.c_prog_fpri "    ") cp;
      let l = C_ind_infer.compute_inductives cp in
      Log.info "Computed inductive definitions:\n";
      List.iter
        (fun ind ->
          Log.info "%a" (Ind_utils.ind_fpri ~properties:false "  ") ind
        ) l;
      Ind_utils.rules_to_file out_file l
    ) [ filename ]


(** Pre-analysis configuration *)
let configure_pp (add_pps: string list) (rem_pps: string list): unit =
  let get_ref: string -> bool ref list = function
    (* pretty-print debug information for operations *)
    | "dbg_is_le" ->
        [ flag_dbg_is_le_gen; flag_dbg_is_le_shape;
          flag_dbg_is_le_num; flag_dbg_is_le_strategy ]
    | "dbg_join" ->
        [ flag_dbg_join_gen; flag_dbg_join_shape;
          flag_dbg_join_num; flag_dbg_join_strategy ]
    | "dbg_dweak" ->
        [ flag_dbg_dweak_shape ; flag_dbg_dweak_num ]
    | "dbg_loc_abs"             -> [ flag_dbg_loc_abs ]
    | "dbg_graph_abs"           -> [ flag_dbg_graph_abs ]
    | "dbg_guard"               -> [ flag_dbg_guard ]
    | "dbg_assign"              -> [ flag_dbg_assign ]
    | "dbg_materialize"         -> [ flag_dbg_materialize ]
    | "dbg_unfold"              -> [ flag_dbg_unfold ]
    | "dbg_bwd_unfold"          -> [ flag_dbg_bwd_unfold ]
    | "dbg_list_is_le"          -> [ flag_dbg_is_le_list ]
    | "dbg_list_join"           -> [ flag_dbg_join_list ]
    (* pretty-print debug information for domains *)
    | "dbg_dom_env"             -> [ flag_dbg_dom_env ]
    | "dbg_dom_disj"            -> [ flag_dbg_dom_disj ]
    | "dbg_dom_list"            -> [ flag_dbg_dom_list ]
    | "dbg_dom_flat"            -> [ flag_dbg_dom_flat ]
    | "dbg_num_apron"           -> [ flag_dbg_num_apron ]
    (* pretty-print debug information for graphs and symbolic variables *)
    | "dbg_graph_blocks"        -> [ flag_dbg_graph_blocks ]
    | "dbg_symvars"             -> [ flag_dbg_symvars ]
    | "dbg_synnumenv"           -> [ flag_dbg_synnumenv ]
    (* pretty-print debug information for collection/value domains *)
    | "dbg_dom_seq"             -> [ flag_dbg_dom_seq ]
    | "dbg_dom_set"             -> [ flag_dbg_dom_set ]
    | "dbg_dom_setlin"          -> [ flag_dbg_dom_setlin ]
    (* pretty-print debug information for other *)
    | "dbg_octasmat"            -> [ flag_dbg_octasmat ]
    | "dbg_numenv"              -> [ flag_dbg_numenv ]
    | "dbg_back_index"          -> [ flag_dbg_back_index ]
    | "dbg_trigger_unfold"      -> [ flag_dbg_trigger_unfold ]
    | "dbg_dommem_eval"         -> [ flag_dbg_dommem_eval ]
    | "dbg_nodeinfos"           -> [ flag_pp_nodeinfos ]
    (* whether to pretty-print analysis status at various statements *)
    | "pp_status_decl"          -> [ flag_status_decl ]
    | "pp_status_block"         -> [ flag_status_block ]
    (* abstract domain fine tuning *)
    | "reduce_quick_eval"       -> [ flag_reduce_quick_eval ]
    (* whether to pretty-print statistics *)
    | "stats"                   -> [ enable_stats ]
    | "stat_apron"              -> [ stat_apron ]
    (* sanity checks *)
    | "sanity_env"              -> [ flag_sanity_env ]
    | "sanity_graph"            -> [ flag_sanity_graph ]
    | "sanity_shape"            ->
        [ flag_sanity_pshape ; flag_sanity_sshape ;
          flag_sanity_bshape ; flag_sanity_fshape ; flag_sanity_env ]
    | "sanity_list"             -> [ flag_sanity_ldom ; flag_sanity_env ]
    (* other cases are left as an Log.fatal_exn for now *)
    | s -> Log.fatal_exn "configure_pp: unknown string %s" s in
  List.iter (fun s -> List.iter (fun r -> r := true ) (get_ref s)) add_pps;
  List.iter (fun s -> List.iter (fun r -> r := false) (get_ref s)) rem_pps

(* config is a list of modules and log levels like: 'l_abs:fatal,l_mat:debug'
 * or even shorter: 'l_abs:f,l_mat:d' *)
let set_log_level_per_module (config: string): unit =
  (* For the logger, a module name has 8 characters;
   * trailing chars are underscores. *)
  let extend_module_name (short: string): string =
    let len = String.length short in
    let ext_len = 8 - len in
    assert(ext_len >= 0);
    let ext = String.make ext_len '_' in
    short ^ ext in
  let modules_and_levels = String.split_on_char ',' config in
  List.iter
    (fun str ->
      let mod_name, ll = str_split ':' str in
      let log_level = Log_level.of_string ll in
      match Logger_glob.expand_modules_group mod_name with
      | [] -> (* not a group of modules *)
          let mod_name = extend_module_name mod_name in
          Logger_glob.set_log_level mod_name log_level
      | modules ->
          List.iter (fun m -> Logger_glob.set_log_level m log_level) modules
    ) modules_and_levels

(** Start *)
let main ( ): unit =
  (* References *)
  let mode_analyze:   bool ref          = ref false in
  let mode_infer_ind: bool ref          = ref false in
  let indfile:        string option ref = ref None in
  let filename:       string ref        = ref "" in
  let add_pps:        string list ref   = ref [] in
  let rem_pps:        string list ref   = ref [] in
  let stress_test:    int ref           = ref 1 in
  let parse_spec:     string ref        = ref "" in
  (* Setter functions *)
  let set_numdom (nd: num_dom) (): unit = nd_selector := nd in
  let set_setdom (sd: set_dom) (): unit = sd_selector := sd in
  let set_setrdom (sd: string) = set_setdom (SD_setr sd) in
  let set_red_mode (rm: reduction_mode) (): unit =
    reduction_mode_selector:= rm in
  let set_stringopt (r: string option ref) (f: string): unit = r := Some f in
  let set_domstruct str = shapedom_struct := str in
  let add_submem_ind s = submem_inds := StringSet.add s !submem_inds in
  let add_pp s = add_pps := s :: !add_pps in
  let rem_pp s = rem_pps := s :: !rem_pps in
  let add_time s =
    let r =
      match s with
      | "apron"  -> timing_apron
      | "base"   -> timing_bshape
      | "flat"   -> timing_fshape
      | "list"   -> timing_lshape
      | "prod"   -> timing_pshape
      | "sep"    -> timing_sshape
      | "mexprs" -> timing_mem_exprs
      | "env"    -> timing_env
      | "value"  -> timing_value
      | "valset" -> timing_valset
      | "valseq" -> timing_valseq
      | "encode" -> timing_graph_encode
      | "disj"   -> timing_disj
      | "analyze"-> timing_analyze
      | _ -> Log.fatal_exn "unbound module to time: %s" s in
    r := true in
  let add_widen_thr i =
    widen_thresholds := IntSet.add i !widen_thresholds in
  let add_rem_goal g =
    goals_rem := g :: !goals_rem in
  (* Parsing of arguments *)
  Arg.parse
    (* The options in this list are sorted as in the documentation, to
     * help checking the consistency *)
    [ (* analysis mode *)
      "-a",           Arg.Set mode_analyze, "launch analysis";
      "-analyze",     Arg.Set mode_analyze, "launch analysis";
      (* not shown: -h and --help help *)
      (* not shown: str  defining the input program *)
      (* input programm and frontend *)
      (* --> general front-end options *)
      "-main",        Arg.Set_string mainfun, "main function";
      (* TODO: goal-sel and goal-rem are not documented,
       * should be moved to a new section *)
      "-header",      Arg.Set_string clang_header_fn, "header for Clang";
      "-idirs",       Arg.Set_string include_dirs,
      "directories for C headers; example -idirs include,/usr/include";
      "-reachable",   Arg.Set Flags.show_reachable_functions,
      "show functions reachable from entry point";
      "-clang-macros",Arg.Set Flags.clang_macros, "include clang macros" ;
      (* --> troubleshooting parsing *)
      "-clang-parser",Arg.Clear use_old_parser, "use Clang parser (default)";
      "-old-parser",  Arg.Set use_old_parser, "use historic, old C parser";
      ("-dump-parse",  Arg.Set dump_parse,
       "dump AST of the analyzed file (after clang and all transformations)");
      ("-load-dump",   Arg.Set load_dump,
       "load an AST dump; cf. -dump-parse option");
      "-tlog",        Arg.Set_string transforms_log_level,
      "log level during transformations: [color:]fatal|error|warn|info|debug";
      (* --> specification files and settings of goals *)
      "-spec",        Arg.Set_string parse_spec, "parsing of specifications";
      "-goal-sel",    Arg.Set_string goal_sel, "name of the goal to analyze";
      "-goal-rem",    Arg.String add_rem_goal, "remove a goal from list";
      (* analysis output *)
      (* --> logging *)
      "-verbose",     Arg.Unit verbose, "verbose testing";
      "-v",           Arg.Unit verbose, "verbose testing";
      "-silent",      Arg.Unit debug_disable, "silent testing";
      "-very-silent", Arg.Unit very_silent, "very silent testing, for timing";
      "-set-on",      Arg.String add_pp, "enable boolean settings";
      "-set-off",     Arg.String rem_pp, "disable boolean settings";
      "-log",         Arg.String set_log_level_per_module,
      "non default log level per module; example: -log l_abs:fatal,l_mat:debug";
      (* --> statistics on analysis progress *)
      "-stats",       Arg.Set enable_stats, "statistics";
      (* --> visualization *)
      "-dot-all",     Arg.Set flag_enable_ext_export_all, "dot, at all points";
      "-latex",       Arg.Set flag_latex_output, "latex output";
      "-pipe",        Arg.Set use_pipe, "communication of results on a pipe";
      "-named-pipe",  Arg.String use_named_pipe,
      "communication of results on a pipe of given name";
      (* timing *)
      "-no-timing",   Arg.Clear flag_pp_timing, "disable timing pp";
      "-timing",      Arg.String add_time, "perform timing of a module";
      "-stress-test", Arg.Set_int stress_test, "repeat number";
      (* reference semantics *)
      ("-malloc-non0",Arg.Set Flags.flag_malloc_never_null,
       "only consider cases where malloc does not return null");
      (* memory abstract domain settings and parameterization *)
      (* --> inductive predicates *)
      "-auto-ind",    Arg.Set mode_infer_ind, "infer inductive definitions";
      "-use-ind",     Arg.String (set_stringopt indfile), "inductive file";
      "-ind-analysis",Arg.Set flag_indpars_analysis, "ind analysis on";
      "-no-ind-analysis",Arg.Clear flag_indpars_analysis, "ind analysis off";
      "-no-prev-fields", Arg.Set Flags.no_ind_prev_fields,
      "ignore prev fields upon graph encoding";
      "-shape-dom",   Arg.String set_domstruct, "shape domain structure";
      (* --> internal settings *)
      "-no-full-gc",  Arg.Clear flag_gc_full, "deactivate the full GC";
      (* --> array domain *)
      "-array-on",    Arg.Set enable_array_domain, "enable array domain";
      "-array-off",   Arg.Clear enable_array_domain, "disable array domain";
      (* --> reduction *)
      "-red-disabled",Arg.Unit (set_red_mode Rm_disabled), "reduction disabled";
      "-red-manual",  Arg.Unit (set_red_mode Rm_manual), "man reduction";
      "-red-min",     Arg.Unit (set_red_mode Rm_minimal), "min reduction";
      "-red-on-read", Arg.Unit (set_red_mode Rm_on_read), "on read reduction";
      "-red-on-r-u",  Arg.Unit (set_red_mode Rm_on_read_and_unfold),
      "on read and unfold reduction mode";
      "-red-max",     Arg.Unit (set_red_mode Rm_maximal), "max reduction";
      (* value abstract domain *)
      (* --> numeric domain selection *)
      "-nd-box",      Arg.Unit (set_numdom ND_box), "selects box domain";
      "-nd-oct",      Arg.Unit (set_numdom ND_oct), "selects oct domain";
      "-nd-pol",      Arg.Unit (set_numdom ND_pol), "selects pol domain";
      (* --> internal parameterization for SV management *)
      "-dynenv-yes",  Arg.Set enable_dynenv, "dynenv in num";
      "-dynenv-no",   Arg.Clear enable_dynenv, "no dynenv in num";
      (* --> equation pack *)
      "-eq-pack-on",    Arg.Set enable_eq_pack, "enable equation pack";
      "-eq-pack-off",    Arg.Clear enable_eq_pack, "disable equation pack";
      (* --> sub-memories *)
      "-add-submem",  Arg.Set enable_submem, "enables sub-memory abstraction";
      "-submem-ind",  Arg.String add_submem_ind, "sub-memory inductive";
      (* --> set domain activation *)
      "-setd-r-lin",  Arg.Unit (set_setrdom "lin"), "set dom SETr-lin";
      "-setd-r-bdd",  Arg.Unit (set_setrdom "bdd"), "set dom SETr-bdd";
      "-setd-lin",    Arg.Unit (set_setdom SD_lin)  , "set dom lin";
      "-setd-off",    Arg.Unit (set_setdom SD_none) , "set dom None";
      "-setd-quicr",  Arg.Unit (set_setdom SD_quicr), "set dom QUICr";
      (* --> seq domain activation *)
      "-seqd-on",     Arg.Set enable_seqdom, "enable the sequence domain";
      "-colv-info",   Arg.Set flag_colv_info, "Adds colv information variables";
      "-colv-len",   Arg.Set enable_colv_len, "Adds colv length constraints";
      "-colv-bnd",   Arg.Set enable_colv_bnd, "Adds colv bounds constraints";
      (* dumping domain operations *)
      "-dump-ops",    Arg.Set flag_dump_ops, "activate operations dump";
      (* disjunction abstraction *)
      "-disj-on",     Arg.Set disj_selector, "disjunctive domain ON (default)";
      "-disj-off",    Arg.Clear disj_selector, "disjunctive domain OFF";
      (* iteration strategy and widening parameters *)
      (* --> general parameters *)
      "-unrolls",     Arg.Set_int unrolls, "# of unroll iters";
      "-no-unroll-in",Arg.Clear unroll_inner, "deactivate inner loops unroll";
      "-j-iters",     Arg.Set_int join_iters, "# of join iters";
      "-dw-iters",    Arg.Set_int dweak_iters, "# dir. weak iters";
      "-no-guided-join", Arg.Clear Flags.guided_join, "unguided join";
      "-guided-widen",    Arg.Set   Flags.guided_widen, "guided widen";
      "-no-guided-widen", Arg.Clear Flags.guided_widen, "unguided widen";
      "-widen-can",   Arg.Set Flags.widen_can,
      "canonicalization-like abstraction of shape graphs during join/widen \
       (simpler but less powerful)";
      (* --> thresholds *)
      "-w-thr",       Arg.Set widen_do_thr, "activate threshold widening";
      "-w-no-thr",    Arg.Clear widen_do_thr, "deactivate threshold widening";
      "-w-add-thr",   Arg.Int add_widen_thr, "add widening threshold";
      (* experimental commands, not documented in detail at this point *)
      "-type-unfolds",Arg.Set_int type_unfolds, "# of type unfolds";
      "-part-lfps",   Arg.Set part_through_lfps, "partition through loops";
      "-no-part-lfps",Arg.Clear part_through_lfps, "no partition through loops";
      "-sel-widen",   Arg.Set sel_widen, "select widening through loops";
      "-no-sel-widen",Arg.Clear sel_widen, "no select widen through loops";
      "-no-fast-iir", Arg.Clear do_quick_ind_ind_mt, "disable fast ind-ind emp";
      "-unary-abs",   Arg.Set do_unary_abstraction, "turns on unary abs";
      "-no-unary-abs",Arg.Clear do_unary_abstraction, "turns off unary abs";
      (* --> latex output (turned off for regression tests) *)
      "-rec-calls",   Arg.Set rec_calls, "analysis of recursive calls";
      (* undocumented, temporary options *)
      "-tmp-expjinst", Arg.Set Graph_utils.do_exp_jinst, "exp, temp, jinst";
    ] (fun s -> filename := s) "";
  (* display the command line *)
  let args = Array.to_list Sys.argv in
  Log.info "Command line was: %a" (stringlist_fpr " ") args;
  (* experimental temporary options to parse spec
   *  will return a list of goals to analyze *)
  let type_context = make_type_context !filename in
  let ogoals =
    if !parse_spec != "" then
      Some (read_goals_from_spec_file ~type_context !parse_spec)
    else None in
  (* sets up printers *)
  configure_pp !add_pps !rem_pps;
  filebase := Filename.basename !filename;
  if !mode_analyze then
    let sd =
      analyzed_file := !filename;
      let s = !shapedom_struct in
      let n = String.length s in
      assert (n > 2);
      let s = String.sub s 1 (n-2) in
      Log.info "Parsing string: %s" s;
      if !mode_infer_ind then
        begin
          Flags.auto_ind := true;
          let res = "auto.ind" in
          do_infer_ind !filename "auto.ind";
          indfile := Some res
        end;
      read_from_string "domain structure"
        Domsel_parser.edomshape Domsel_lexer.token s in
    let time_start = Timer.cpu_timestamp () in
    begin
      let do_analyze_goal_tuple ~(gname: string option)
          (params, fpre, (pc: Spec_sig.proc_call), fpost, old_svals)
          pilines =
        let ok0, ko0 = count_assert_get_state () in
        let tg_start = Timer.cpu_timestamp () in
        let sep = String.make 64 '=' and lsep = String.make 64 '-' in
        let gname = Option.value gname ~default:"<?>" in
        Log.info "%s\nANALYSIS,GOAL,start: %s" sep gname;
        (* TODO: pass information to do_analyze;
         ** right now arguments are missing params!!!! *)
        do_analyze_one
          ~type_context
          ~indspec:(IS_spec_file_content pilines)
          ~cfile:!filename ~mainfun:pc.function_name
          ~goal:(AG_goal (params, fpre, pc.result_binding,
                          pc.arguments, fpost, old_svals))
          ~shapedom_struct:sd ~repeat_num:!stress_test;
        let tg_end = Timer.cpu_timestamp () in
        let ok1, ko1 = count_assert_get_state () in
        Log.info "%s\nANALYSIS,GOAL,end: %s\n%s\nT:  %.4f s\nOK: %d\nKO: %d\n%s"
          sep gname lsep (tg_end -. tg_start) (ok1 - ok0) (ko1 - ko0) sep in
      match ogoals with
      | None ->
          let indspec =
            match !indfile with
            | None -> IS_none
            | Some ind -> IS_ind_file_name ind in
          do_analyze_one
            ~type_context
            ~indspec:indspec
            ~cfile:!filename ~mainfun:!mainfun
            ~goal:AG_simple
            ~shapedom_struct:sd ~repeat_num:!stress_test
      | Some (goals, pilines) ->
          (* either select a single goal if "goal_sel" is set, or run all *)
          let goals =
            if !goal_sel <> "" then
              StringMap.singleton !goal_sel (StringMap.find !goal_sel goals)
            else
              List.fold_left (fun goals name -> StringMap.remove name goals)
                goals !goals_rem in
          StringMap.iter
            (fun gname goal_tuple ->
              do_analyze_goal_tuple ~gname:(Some gname) goal_tuple pilines
            ) goals
    end;
    pipe_end_status_report ( );
    let time_finish = Timer.cpu_timestamp () in
    show_end_status_report (time_finish -. time_start);
    exit 0
  else if !mode_infer_ind then
    begin
      do_infer_ind !filename "inferred.ind"
    end
  else
    pipe_end_status_report ()

(* Run *)
let () = main ()
