% =======================================
% Lists with a length parameter
slllo<0,1> :=
| [0] -   emp
      -   this = 0
        & @i0 = 0
| [3 addr int int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.slllo( | $2 | )
      -   this != 0
        & @i0 = $2 + 1
        & alloc( this, 8 )
.

% =======================================
% Common part ends

% =======================================
% Entry point l_second (old llen-01)
$goal l_snd [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
  & p != 0
} res = l_snd( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> pl
  * asz |-> il
  * ares|-> pres
  * pl->0[4] |-> pres
  * pl->4[4] |-> plval
  * pres.slllo( | i | )
  & il = i + 1
}
.
