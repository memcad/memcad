% =======================================
% Lists with a length parameter
slllo<0,1> :=
| [0] -   emp
      -   this = 0
        & @i0 = 0
| [3 addr int int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.slllo( | $2 | )
      -   this != 0
        & @i0 = $2 + 1
        & alloc( this, 8 )
.

% =======================================
% Basic specifications

$form H [addr p] :=
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( il )
.

% =======================================
% Entry point l_id (old llen-00)
$goal l_id [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
} res = l_id( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> p
  * asz |-> il
  * ares|-> p
  * p.slllo( | il | )
}
.

% =======================================
% Entry point l_second (llen-01)
$goal l_snd [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
  & p != 0
} res = l_snd( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> pl
  * asz |-> il
  * ares|-> pres
  * pl->0[4] |-> pres
  * pl->4[4] |-> plval
  * pres.slllo( | i | )
  & il = i + 1
}
.

% =======================================
% Entry point l_add (llen-02)
% => First statement, exact shape, does not exercise folding
$goal l_add_nofold [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
  & p != 0
} res = l_add( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> pl
  * asz |-> il
  * ares|-> pres
  * pres->0[4] |-> pl
  * pres->4[4] |-> plval
  * pl.slllo( | il | )
}
.

% =======================================
% Entry point l_add (llen-02)
% => First statement, exact shape, does not exercise folding
$goal l_add_fold [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
  & p != 0
} res = l_add( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> pl
  * asz |-> il
  * ares|-> pres
  * pres.slllo( | i | )
  & i = il + 1
}
.

% =======================================
% Entry point l_rev (llen-03)
$goal l_rev [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
} res = l_rev( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> vl
  * asz |-> il
  * ares|-> vp
  * vp.slllo( | il | )
  & vl = 0
}
.

% maybe run spec with different starting points
