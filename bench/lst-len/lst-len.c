// Examples on the list length

// types
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

// global state
int sz;

// identity (llen-00)
list l_id( list l ){
  list r ;
  r = l;
  return r;
}

// second element (llen-01)
list l_snd( list l ){
  list r;
  r = l->next;
  return r;
}

// element add (llen-02)
list l_add( list l ){
  list r;
  r = malloc( 8 );
  r->next = l;
  return r;
}

// element add (llen-02)
list l_ghost_add( list l ){
  list r;
  r = malloc( 8 );
  r->next = l;
  sz = sz + 1;
  return r;
}

// list reverse (llen-03)
list l_rev( list l ){
  list r ;
  list k ;
  r = null;
  k = null; // nullify, to discard constraint, to remove
  while( l != 0 ){
    k = l->next;
    l->next = r;
    r = l;
    l = k;
    k = null; // nullify, to discard constraint, to remove
  }
  return r;
}

