% =======================================
% Lists with a length parameter
slllo<0,1> :=
| [0] -   emp
      -   this = 0
        & @i0 = 0
| [3 addr int int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.slllo( | $2 | )
      -   this != 0
        & @i0 = $2 + 1
        & alloc( this, 8 )
.

% =======================================
% Common part ends

% =======================================
% Entry point l_add (llen-02)
% => First statement, exact shape, does not exercise folding
$goal l_add_ghost_fold [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
  & p != 0
} res = l_ghost_add( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> pl
  * asz |-> il
  * ares|-> pres
  * pres.slllo( | il | )
  & pres != 0
}
.
%% TODO: add constraint on @old!
