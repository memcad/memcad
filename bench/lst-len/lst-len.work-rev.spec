% =======================================
% Lists with a length parameter
slllo<0,1> :=
| [0] -   emp
      -   this = 0
        & @i0 = 0
| [3 addr int int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.slllo( | $2 | )
      -   this != 0
        & @i0 = $2 + 1
        & alloc( this, 8 )
.

% =======================================
% Common part ends

% =======================================
% Entry point l_rev (llen-03)
$goal l_rev [ ] {
    l   ::  al
  * sz  ::  asz
  * al  |-> p
  * asz |-> il
  * p.slllo( | il | )
} res = l_rev( l ) {
    l   ::  al
  * sz  ::  asz
  * res ::  ares
  * al  |-> vl
  * asz |-> il
  * ares|-> vp
  * vp.slllo( | il | )
  & vl = 0
}
.
