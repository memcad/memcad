// Seq-001: Basic unfolding test
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list m;
  int i = 3;
  _memcad( "decl_seqvars( E )" );
  _memcad( "decl_setvars( S )" );
  //_memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  _memcad( "add_inductive( l, lsetseqex, [ | | S | E ] )" );
  m = l;
  if( m != null ){
		i = m->data;
		m = m->next;
		_memcad( "output_dot(l, m, i | CC)" );
		if( m!= null ){
			int j = m->data;
			m = m->next;
			_memcad( "output_dot(l, m | CC)" );
			if( m!= null ){
				int j = m->data;
				m = m->next;
				_memcad( "output_dot(l, m | CC)" );
				if( m!= null ){
					int j = m->data;
					m = m->next;
					_memcad( "output_dot(l, m | CC)" );
				}
			}
		}
  }else{
		_memcad( "output_dot(l, m, i | CC)" );
	}
	_memcad( "output_dot(l, m, i | CC)" );
}

