// Ex c-micro-70: list reversal
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
list l;
list lrev( ){
  list r ;
  list k ;
  r = null;
  k = null; // nullify, to discard constraint, to remove
  while( l != 0 ){
    k = l->next;
    l->next = r;
    r = l;
    l = k;
    k = null; // nullify, to discard constraint, to remove
  }
  return r;
}
