struct sll {
  struct sll *next;
};
typedef struct sll* list;
struct node {
  int data;
  struct sll link;
};

void traverse (struct sll *l){
  struct sll* c = l;
  {
    // l0 is not an allocated address but it corresponds to the starting address
    // of the block containing the list header.
    // We need it since MemCAD requires it for the unfolding directive.
    void * l0 = l -1;
    _memcad(" unfold( l0 ) ");
  }
  while (c->next != l){
    c = c->next;
    ;
  }
  ;
}

void push (struct sll *l, int i){
  struct node* m = malloc(sizeof(struct node));
  m->data = i;
  (m->link).next = l->next;
  l->next = &m->link;
  ; // To see post-condition
}