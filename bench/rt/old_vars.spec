$goal f {
    x :: x_a *
    x_a |-> x_v
} f( ) {
    x :: x_a *
    x_a |-> x_v &
    x_v = x_v@old + 1
}
.

$goal swap {
    pa    :: pa_a
  * pb    :: pb_a
  * pa_a  |-> pa_v
  * pa_v  |-> a
  * pb_a  |-> pb_v
  * pb_v  |-> b
} swap(pa, pb) {
    pa    :: pa_a
  * pb    :: pb_a
  * pa_a  |-> pa_v
  * pa_v  |-> a
  * pb_a  |-> pb_v
  * pb_v  |-> b
  & a = b@old
  & b = a@old
}.

$goal incr {
    pa    :: pa_a
  * pa_a  |-> pa_v
  * pa_v  |-> a
  & a >= 1
} incr( pa ) {
    pa    :: pa_a
  * pa_a  |-> pa_v
  * pa_v  |-> a
  & a = a@old +1
}.