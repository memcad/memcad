// Seq-10: Deep copy of a list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
  _memcad( "decl_seqvars( S )");
  _memcad( "add_inductive( l, lseqex, [ | | | S ] )" );
  list res;
  if( l == 0 ){
    _memcad( "unfold(l)" );
    res = 0;
  }else{
    res = malloc( 8 );
    res->data = l->data;
    res->next = 0;
    ;
    list c_l = l;
    list c_res = res;
    ;
    while(c_l->next != 0){
      c_l = c_l->next;
      list n = malloc( 8 );
      n->data = c_l->data;
      n->next = 0;
      c_res->next = n;
      c_res = n;
      ;
    }
    c_l = c_l->next;
    _memcad( "unfold(c_l)" );
  }
  _memcad( "check_inductive( res, lseqex, [ | | | S ] )" );
}

void main_nseq( ){
	list l;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  list res;
  if( l == 0 ){
    res = 0;
  }else{
    res = malloc( 8 );
    res->data = l->data;
    res->next = 0;

    list c_l = l;
    list c_res = res;

    while(c_l->next != 0){
      c_l = c_l->next;
      list n = malloc( 8 );
      n->data = c_l->data;
      n->next = 0;
      c_res->next = n;
      c_res = n;

    }
    c_l = c_l->next;
  }
  _memcad( "check_inductive( res, list, [ | | | ] )" );
}
