// Seq-spec-01: Basic testing
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void skip(list l){
  ;
}

void push(list l, int i){
  list m = malloc(sizeof(struct elist));
  m->data = i;
  m->next = l;
  l = m;
}

void exchange(int* p1, int* p2){
  int c = *p1;
  *p1 = *p2;
  *p2 = c;
  ;
}

int pop(list l){
  if( l == 0 ) {
    return 0;
    ;
  }
  return l->data;
}
