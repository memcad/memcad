// Seq-sorted-tree-01: Basic unfolding test
struct node {
  int data ;
  struct node * left;
  struct node * right ;
};

void main(){
   struct node* t;
   int i;
   _memcad("decl_seqvars(S0, S1)" );
   _memcad("seq_assume( S1 = sort(S0.[i]) )" );
   _memcad( "add_inductive( t, bst, [ | | | S0 ])" );
   struct node* m = malloc(sizeof(struct node));
   m->left = m->right = NULL;
   m->data = i;
   if (t == NULL) {
     t = m;
   } else {
     struct node* curr = t;
     while ( ( curr->data >= i && curr->left != null) ||
         ( curr->data < i && curr->right != null ) ) {
       if (curr->data >= i) {
         curr = curr->left;
       } else {
         curr = curr->right;
       }
     }
     if (curr->data >= i) {
       assert(curr->left == NULL);
       curr->left = m;
     } else {
       assert(curr->right == NULL);
       curr->right = m;
     }
   }
   _memcad("check_inductive( t, bst, [ | | | S1 ])" );
}