% =======================================
% Basic list structure
list<0,0> :=
| [0] -   emp
      -   this = 0
| [2 addr int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.list( )
      -   this != 0
        & alloc( this, 8 )
.

% =======================================
% Basic specification for list reverse

$form H [addr a; addr p] :=
  a |-> p
  * p.list()
.
$form Hnn [addr a] :=
  $form( H )[a, p]
  & p != 0
.

% Testing goals
$goal lrev {
    l :: a
  * $form( Hnn )[a]
} res = lrev( ) {
    res :: a
  * l   :: l_a
  * $form( H )[a, p]
  * $form( H )[l_a, p0]
}
.
