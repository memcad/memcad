struct dlbstseq {
  struct dlbstseq * previous;
  int data;
  struct dlbstseq * left;
  struct dlbstseq * right;
};

void main( ){
  struct dlbstseq* m;
  struct dlbstseq* tree;
  struct dlbstseq* l;
  struct dlbstseq* r;
  struct dlbstseq* rm;
  struct dlbstseq* n = NULL;
  int i;
  int j;
  _memcad( "decl_seqvars( E, F, G, H)" );
  _memcad( "seq_assume( E = F.[i].G)" );
  _memcad( "seq_assume( H = F.G)" );
  _memcad( "assume( i < __colv_min(G))" );
  _memcad( "seq_assume( E = sort(E))" );
  _memcad( "add_inductive( m, dlbstseq, [ n | | | E])" );
  tree=m;
  if(tree == NULL){
    j=0;
  }
  else{
    while(i!=tree->data && ((tree->data > i && tree->left != NULL) ||
          (tree->data < i && tree->right != NULL))){
      if(i>tree->data){
        tree=tree->right;
        ;
      }
      else{
        tree=tree->left;
        ;
      }
      ;
    }
    rm=tree;
    if(rm->data==i){
        if(rm->left == NULL && rm->right == NULL){/*
          if(rm->previous != NULL){
            if(i<=rm->previous->data){
              rm->previous->left=NULL;
              ;
            }
            else{
              rm->previous->right=NULL;
              ;
            }
          }
          else{
            _memcad( "unfold_bseg(rm)" );
            m=NULL;
            ;
          }
        */}
        else if(rm->right == NULL){/*
          if(rm->previous != NULL){
            if(i<=rm->previous->data){
              rm->previous->left=rm->left;
              ;
            }
            else{
              rm->previous->right=rm->left;
              ;
            }
            rm->left->previous=rm->previous;
            ;
          }
          else{
            _memcad( "unfold_bseg(rm)" );
            m=rm->left;
            m->previous=n;
            ;
          }
        */}
        else if(rm->left == NULL){/*
          if(rm->previous != NULL){
            if(i<=rm->previous->data){
              rm->previous->left=rm->right;
              ;
            }
            else{
              rm->previous->right=rm->right;
              ;
            }
            rm->right->previous=rm->previous;
            ;
          }
          else{
            _memcad( "unfold_bseg(rm)" );
            m=rm->right;
            ;
            m->previous=n;
          }
        */}
        else{
          l=rm->left;
          while(l->right!=NULL){
            l=l->right;
            ;
          }
          l->right=rm->right;
          rm->right->previous=l;
          ;
          if(rm->previous != NULL){
            _memcad( "unfold_bseg(rm)" );
            if(i<=(rm->previous->data)){
              rm->previous->left=rm->left;
              ;
            }
            else{
              rm->previous->right=rm->left;
              ;
            }
            rm->left->previous=rm->previous;
            ;
          }
          else{
            _memcad( "unfold_bseg(rm)" );
            m=rm->left;
            m->previous=n;
            ;
         }
        }
      }
    }
  }