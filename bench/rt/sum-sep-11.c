// "graph_simplify.c"
#include <stdlib.h>


typedef struct v {
  // The four neighbours of a vertex
  struct v* n0;
  struct v* n1;
  struct v* n2;
  struct v* n3;

  int data;
} v;

typedef struct v* vertex;

typedef struct g {
  // A graph three vertices
  vertex v0;
  vertex v1;
  vertex v2;
  vertex v3;
  vertex v4;
} g;

typedef g* graph;

void init_vertex(vertex v) {
  v = malloc(sizeof(v));
  v->n0 = NULL;
  v->n1 = NULL;
  v->n2 = NULL;
  v->n3 = NULL;  
}


graph createEmptyGraph() {
  graph g = malloc(sizeof(g));

  init_vertex(g->v0);
  init_vertex(g->v1);
  init_vertex(g->v2);
  init_vertex(g->v3);
  init_vertex(g->v4);

  return g;
}

void add_edge(graph g, vertex u, vertex v) {
  if (u->n0 == NULL)
    u->n0 = v;
  else if (u->n1 == NULL)
    u->n1 = v;
  else if (u->n2 == NULL)
    u->n2 = v;
  else if (u->n3 == NULL)
    u->n3 = v;
}

void remove_edge(graph g, vertex u, vertex v) {
  if (u->n0 == v)
    u->n0 = NULL;
  else if (u->n1 == v)
    u->n1 = NULL;
  else if (u->n2 == v)
    u->n2 = NULL;
  else if (u->n3 == v)
    u->n3 = NULL;
}

void free_graph(graph g) {
  free(g->v0);
  free(g->v1);
  free(g->v2);
  free(g->v3);
  free(g->v4);

  free(g);
}
