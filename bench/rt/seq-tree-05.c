// Seq-tree-01: Basic unfolding test
struct tree {
  int data ;
  struct tree * left;
  struct tree * right;
<<<<<<< HEAD
};

struct dll {
  struct dll * next;
  struct dll * previous;
  int data;
};

struct dlbst {
  struct dlbst * left;
  struct dlbst * previous;
  struct dlbst * right;
};

struct dlbstseq {
  struct dlbstseq * previous;
  int data;
  struct dlbstseq * left;
  struct dlbstseq * right;
=======
  struct tree * parent;
>>>>>>> bac85851... hint for seq_dom
};
/*
void main1( ){
  struct tree *l;
  struct tree *k;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, treeseqex, [ | | | E]*= k, treeseqex,[ | | | F])" );
  _memcad("unfold(l)");
  _memcad( "check_segment(l, treeseqex, [ | | | E]*= k, treeseqex,[ | | | F])" );
}

void main2( ){
  struct tree *l;
  struct tree *k;
  int i;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, bst, [ | | | E]*= k, bst,[ | | | F])" );
  _memcad("unfold(l)");
  _memcad( "check_segment(l, bst, [ | | | E]*= k, bst,[ | | | F])" );
  i=2;
}

void main3( ){
  struct tree *l;
  struct tree *k = null;
  struct tree *m;
  int i;
  _memcad( "assume(l!=k)");
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, treeseqex, [ | | | E]*= k, treeseqex,[ | | | F])" );
  _memcad("unfold(l)");
  m=l->right;
  _memcad("unfold(m)");
  _memcad( "check_segment(l, treeseqex, [ | | | E]*= k, treeseqex,[ | | | F])" );
  i=2;
}

void main4( ){
  struct tree *l;
  struct tree *k = null;
  struct tree *m;
  int i;
  _memcad( "assume(l!=k)");
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, bst, [ | | | E]*= k, bst,[ | | | F])" );
  _memcad("unfold(l)");
  //m=l->right;
  //_memcad("unfold(m)");
  _memcad("unfold(l->right)");
  i = 2;
  _memcad( "check_segment(l, bst, [ | | | E]*= k, bst,[ | | | F])" );
  i=2;
}

void main5( ){
  struct tree *l;
  struct tree *k = null;
  struct tree *m;
  int i;
  _memcad( "decl_seqvars( E, F, G, H)" );
  _memcad( "add_segment( l, bst2, [ | | | E, G]*= k, bst2,[ | | | F, H])" );
  _memcad("unfold(l)");
  _memcad( "check_segment(l, bst2, [ | | | E, G]*= k, bst2,[ | | | F, H])" );
  i=2;
}

void main6( ){
  struct tree *l;
  struct tree *k = null;
  struct tree *m = null;
  int i;
  _memcad( "decl_seqvars( E, F, G)" );
  _memcad( "seq_assume( G = E.F )" );
  if(i){
    _memcad( "add_segment( l, treeseqex, [ | | | E]*= k, treeseqex,[ | | | F])" );
    i=0;
  }
  else{
    _memcad( "add_inductive( l, treeseqex, [ | | | G])" );
    i=0;
  }
  _memcad( "merge( )" );
  i=0;
}

void main7( ){
  struct tree *l;
  struct tree *k = null;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E, F, G, H, I, J)" );
  _memcad( "seq_assume( I = E.G )" );
  _memcad( "seq_assume( J = H.F )" );
  if(i){
    _memcad( "add_segment( l, treeseqex, [ | | | I]*= k, treeseqex,[ | | | J])" );
    i=0;
  }
  else{
    _memcad( "add_segment( l, treeseqex, [ | | | E]*= m, treeseqex,[ | | | F])" );
    _memcad( "add_segment( m, treeseqex, [ | | | G]*= k, treeseqex,[ | | | H])" );
    i=0;
  }
  _memcad( "merge( )" );
  i=0;
}

void main8( ){
  struct tree *l;
  struct tree *k;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E, F, G, H, I, J)" );
  _memcad( "seq_assume( I = E.G )" );
  _memcad( "seq_assume( J = H.F )" );
  _memcad( "add_segment( l, treeseqex, [ | | | E]*= m, treeseqex,[ | | | F])" );
  _memcad( "add_segment( m, treeseqex, [ | | | G]*= k, treeseqex,[ | | | H])" );
  _memcad("check_segment( l, treeseqex, [ | | | I]*= k, treeseqex,[ | | | J])");
  i=0;
}

void main9( ){
  struct tree *l;
  struct tree *k;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E)" );
  _memcad( "add_inductive( l, treeseqex, [ | | | E ] )" );
  if(l != null){
    m = l;
    while (m->right != null){
      m = m->right;
      i=0;
    };
    i=0;
    while (m->left != null){
      m = m->left;
      i=0;
    };
    i=0;
  }
  //todo: test that later
  _memcad("check_inductive( l, treeseqex, [ | | | E ])");
}

void main10( ){
  struct tree *l;
  struct tree *k;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E)" );
  _memcad( "add_inductive( l, treeseqex, [ | | | E ] )" );
  if(l != null){
    m = l;
    while (m->left != null){
      m = m->left;
      i=0;
    };
  i=0;
  }
  _memcad("check_inductive( l, treeseqex, [ | | | E ])");
}

void main11( ){
  struct tree *l;
  struct tree *k;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E)" );
  _memcad( "add_inductive( l, bst, [ | | | E ] )" );
  if(l != null){
    m = l;
    while (m->right != null){
      m = m->right;
      i=0;
    };
  i=0;
  }
  /*while (m->left != null){
      m = m->left;
      i=0;
    };
  i=0;
  }
  //todo: test that later
  _memcad("check_inductive( l, bst, [ | | | E ])");
}

void main12( ){
  struct tree *l;
  struct tree *k;
  struct tree *m ;
  int i;
  _memcad( "decl_seqvars( E)" );
  _memcad( "add_inductive( l, bst, [ | | | E ] )" );
  if(l != null){
    m = l;
    while (m->left != null){
      m = m->left;
      i=0;
    };
  i=0;
  }
  _memcad("check_inductive( l, bst, [ | | | E ])");
}

void main13( ){
  struct tree* t1;
  _memcad( "decl_seqvars( E)" );
  _memcad("seq_assume( E = sort(E))");
  _memcad( "add_inductive( t1, bst, [ | | | E ] )" );
  struct tree* current = t1;
  int x;
  while(current!=null){
    if(x>=current->data){
      current=current->right;
      ;
    }
    else{
      current=current->left;
      ;
    }
  ;
  }
  _memcad( "check_inductive( t1, bst, [ | | | E ] )" );
}


void main14( ){
  struct tree* t1;
  _memcad( "decl_seqvars( E)" );
  _memcad("seq_assume( E = sort(E))");
  _memcad( "add_inductive( t1, bst, [ | | | E ] )" );
  struct tree* current = t1;
  current = t1;
  while(current!=null){
    current=current->right;
  }
  _memcad( "check_inductive( t1, bst, [ | | | E ] )" );
}

void main15( ){
  struct tree* t1;
  _memcad( "decl_seqvars( E)" );
  _memcad("seq_assume( E = sort(E))");
  _memcad( "add_inductive( t1, bst, [ | | | E ] )" );
  struct tree* current = t1;
  current = t1;
  while(current!=null){
    current=current->left;
  }
  _memcad( "check_inductive( t1, bst, [ | | | E ] )" );
}

void main16( ){
  struct tree* t1;
  int x;
  _memcad( "decl_seqvars( E)" );
  _memcad("seq_assume( E = sort(E))");
  _memcad( "add_inductive( t1, bst, [ | | | E ] )" );
  struct tree* current = t1;
  current = t1;
  while(current!=null){
    if(x & 1){
      current=current->right;
      ;
    }
    else{
      current=current->left;
      ;
    }
  ;
  }
  _memcad( "check_inductive( t1, bst, [ | | | E ] )" );
}

void main17( ){
  struct tree *l;
  struct tree *k;
  struct tree *m;
  struct tree *n = null;
  _memcad( "assume(l!=n)");
  _memcad( "decl_seqvars( E, F, G, H)" );
  _memcad( "add_segment( l, bst2, [ | | | E, F]*= k, bst2,[ | | | G, H])" );
  _memcad("unfold(l)");
  if(l->right != null){
    _memcad("unfold(l->right)");
  }
  _memcad( "check_segment(l, bst2, [ | | | E, F]*= k, bst2,[ | | | G, H])" );
}
/*
void main18( ){
  struct tree* t1;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_inductive( t1, bst2, [ | | | E, F] )" );
  struct tree* current = t1;
  int x;
  while(current!=null){
    if(x>=current->data){
      current=current->right;
      ;
    }
    else{
      current=current->left;
      ;
    }
  ;
  }
  _memcad( "check_inductive( t1, bst2, [ | | | E, F] )" );
}
*/

void main19(){
  struct dll* l;
  struct dll* m=null;
  struct dll* n=null;
  struct dll* k;
  struct dll* p=null;
  _memcad( "assume( k != p )" );
  _memcad( "add_inductive( l, dll, [ m| | |])" );
  p=l;
  _memcad( "assume( p != n )" );
  _memcad( "unfold( p )" );
  _memcad( "assume( p != n )" );
  while(p->next!=null){
    p=p->next;
    ;
  }
  _memcad( "assume( p != n )" );
  while(p->previous!=null){
    _memcad( "unfold_bseg(p)" );
    p=p->previous;
    _memcad( "assume( p != n )" );
    ;
  }
  //_memcad( "unfold_bseg(p)" );
  /* _memcad( "assume( p != m )" );
  _memcad( "assume( p->previous != m )" );
  _memcad( "unfold_bseg(p->previous)" );*/
  ;
  _memcad( "check_inductive( l, dll, [ m| | |])" );
}



void main20(){
  struct dlbst* l;
  struct dlbst* m=null;
  struct dlbst* n=null;
  struct dlbst* k;
  struct dlbst* p=null;
  int i;
  _memcad( "assume( k != p )" );
  _memcad( "add_inductive( l, dlbst, [ m| | |])" );
  p=l;
  _memcad( "assume( p != n )" );
  _memcad( "unfold( p )" );
  _memcad( "assume( p != n )" );
  while(p->left!=null && p->right!=null){
    if (i){
      p=p->left;
      ;
    }
    else{
      p=p->right;
      ;
    }
    ;
  }
  _memcad( "assume( p != n )" );
  while(p->previous!=null){
    _memcad( "unfold_bseg(p)" );
    p=p->previous;
    _memcad( "assume( p != n )" );
    ;
  }
  _memcad( "unfold_bseg(p)" );
  /* _memcad( "assume( p != m )" );
  _memcad( "assume( p->previous != m )" );
  _memcad( "unfold_bseg(p->previous)" );*/
  ;
  _memcad( "check_inductive( p, dlbst, [ m| | |])" );
}

void main21(){
  struct dlbstseq* l;
  struct dlbstseq* m=null;
  struct dlbstseq* n=null;
  struct dlbstseq* k;
  struct dlbstseq* p=null;
  int i;
  _memcad( "assume( k != p )" );
  _memcad( "decl_seqvars( E )" );
  _memcad( "add_inductive( l, dlbstseq, [ m| | | E ])" );
  p=l;
  _memcad( "assume( p != n )" );
  _memcad( "unfold( p )" );
  _memcad( "assume( p != n )" );
  while(p->left!=null && p->right!=null){
    if (i){
      p=p->left;
      ;
    }
    else{
      p=p->right;
      ;
    }
    ;
  }
  _memcad( "assume( p != n )" );
  if(p->previous!=null){
    _memcad( "unfold_bseg(p)" );
    p=p->previous;
    ;
  }
  //_memcad( "unfold_bseg(p)" );
  /* _memcad( "assume( p != m )" );
  _memcad( "assume( p->previous != m )" );
  _memcad( "unfold_bseg(p->previous)" );*/
  ;
  _memcad( "check_inductive( l, dlbstseq, [ m| | | E])" );
}

void main22( ){
  struct dlbstseq *l;
  struct dlbstseq *k;
  struct dlbstseq *m;
  struct dlbstseq *n;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, dlbstseq, [m | | | E]*= k, dlbstseq,[n | | | F])" );
  _memcad("unfold_bseg(k)");
  _memcad( "check_segment(l, dlbstseq, [m | | | E]*= k, dlbstseq,[n | | | F])" );
}

void main23( ){
  struct dlbstseq *l;
  struct dlbstseq *k;
  struct dlbstseq *m;
  struct dlbstseq *n;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "add_segment( l, dlbstseq, [m | | | E]*= k, dlbstseq,[n | | | F])" );
  _memcad("unfold_bseg(k)");
  while(k!=NULL){
    assert(k!=NULL);
    k=k->previous;
    _memcad("unfold_bseg(k)");
    ;
  }
  _memcad( "check_segment(l, dlbstseq, [m | | | E]*= k, dlbstseq,[n | | | F])" );
}



void main( ){
  main22();
}