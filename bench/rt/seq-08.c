// Seq-08: insertion test somewhere in the list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
	int i, len;
  _memcad( "decl_seqvars( E, F, S1, S2 )" );
	_memcad( "seq_assume( E = S1.S2 )");
	_memcad( "seq_assume( F = S1.[i].S2 )");
  _memcad(" assume( __colv_size(S1) == len )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
	list c = l;
	list m = malloc(8); m->data = i;
	if(len == 0){
		m->next=l;
		l = m;
	} else {
		int j;
		for(j = 0; j < len-1; j++){
			c = c->next;
		}
		m->next = c->next;
		c->next = m;
	}
  _memcad( "check_inductive( l, lseqex, [ | | | F ] )" );
}

void main_nseq( ){
	list l;
	int i, len;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
	list c = l;
	list m = malloc(8); m->data = i;
	if(len == 0){
		m->next=l;
		l = m;
	} else {
		_memcad( "assume( l != 0 )" );
		int j;
		for(j = 0; c->next != 0 && j < len-1; j++){
			c = c->next;
		}
		m->next = c->next;
		c->next = m;
	}
  _memcad( "check_inductive( l, list, [ | | | ] )" );
}
