// "graph.c"

#include <stdlib.h>

#define MAX_SIZE_GRAPH 10
#define MAX_NEIGHBOURS 5

typedef struct v {
  struct v* neighbours[MAX_NEIGHBOURS];
  int data;
} v;

typedef struct v* vertex;

typedef struct g {
  vertex vertices[MAX_SIZE_GRAPH];
  int nbVertices;
} g;

typedef g* graph;


graph createEmptyGraph() {
  graph g = malloc(sizeof(g));

  g->nbVertices = 0;

  return g;
}

void add_node(graph g, vertex u){
  if (g->nbVertices < MAX_SIZE_GRAPH) {
    u = malloc(sizeof(v));
    g->vertices[g->nbVertices++] = u;
  }
}

void remove_node(graph g, vertex u) {
  int i = 0, j;
  vertex currentNode = NULL;

  while (currentNode != u && i < g->nbVertices)
    currentNode = g->vertices[i++];

  if (currentNode == u) {
    g->vertices[--i] = g->vertices[--g->nbVertices];

    for (i = 0; i < g->nbVertices; i++)
      for (j = 0; j < MAX_NEIGHBOURS; j++)
	if (g->vertices[i]->neighbours[j] == u)
	  g->vertices[i]->neighbours[j] = NULL;
    free(u);
  }
}

void add_edge(vertex u, vertex v) {
  int i;

  for (i = 0; i < MAX_NEIGHBOURS && u->neighbours[i] != NULL; i++);

  if (i < MAX_NEIGHBOURS)
    u->neighbours[i] = v;
}

void remove_edge(vertex u, vertex v) {
  int i;

  for (i = 0; i < MAX_NEIGHBOURS && u->neighbours[i] != v; i++);

  if (i < MAX_NEIGHBOURS)
    u->neighbours[i] = NULL;
}
