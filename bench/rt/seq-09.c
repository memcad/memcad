// Seq-09: Computing the length of a list.
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
  _memcad( "decl_seqvars( S )");
  _memcad( "add_inductive( l, lseqex, [ | | | S ] )" );
  int _lenS; _memcad( "assume( __colv_size(S) == _lenS)" );
  // We have to use an intermediary variable because
  // we cannot use __colv_size in assert function
	int len = 0;
	list c = l;
  while(c != 0){
    c = c-> next;
    len++;
    ;
  }
  // At this point we know that c == 0x0 /\ c·list(S')
  // unfolding the list in c add the constraints
  // S' = [] so |S'| = 0
  _memcad( "unfold(c)" );
  assert( _lenS == len );
}

void main_nseq( ){
	list l;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  // We have to use an intermediary variable because
  // we cannot use __colv_size in assert function
	int len = 0;
	list c = l;
  while(c != 0){
    c = c-> next;
    len++;
    ;
  }
  // At this point we know that c == 0x0 /\ c·list(S')
  // unfolding the list in c add the constraints
  // S' = [] so |S'| = 0
  _memcad( "unfold(c)" );
  _memcad( "check_inductive( l, list )");
}
