// Seq-004: Advance indlusion tests : void_seg
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list m;
  _memcad( "decl_seqvars( E )" );
	_memcad( "assume( l == m )" );
  _memcad( "seq_assume( E = [] )" );
	_memcad( "output_dot( m, l | CC  )" );
  _memcad( "check_segment( l, lseqex, [ | | | E ] *= m, lseqex, [ | | | E ] )" );
}
