// Seq-05: Basic example to show te benefit of sequences arguments
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void push(list* l, int i){
  list m = malloc( 8 ) ;
  m->next = *l;
  m->data = i;
  *l = m; //Function works by side-effect
}

int pop(list* l){
  int res = -1;
	list l0 = *l;
	_memcad( "unfold(l0)" );
  if(*l){
	res = (*l)->data;
	*l = (*l)->next;
  }
  return res;
}

void add_harness( ){
  list l;
  int i, j;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "seq_assume( F = [ i ].E )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  push(&l, i);
  _memcad( "check_inductive( l, lseqex, [ | | | F ] )" );
}

void remove_harness(){
  list l;
  int i, j;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "seq_assume( F = [ i ].E )" );
  _memcad( "add_inductive( l, lseqex, [ | | | F ] )" );
  j = pop(&l);
  _memcad( "check_inductive( l, lseqex, [ | | | E ] )" );
  assert( i == j );
}

void remove_harness_empty(){
  list l;
  int i, j;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "seq_assume( E = [] )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  j = pop(&l);
  assert( -1 == j );
  _memcad( "check_inductive( l, lseqex, [ | | | E ] )" );
}

void add_harness_nseq( ){
  list l;
  int i, j;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  push(&l, i);
  _memcad( "check_inductive( l, list, [ | | | ] )" );
}

void remove_harness_nseq(){
  list l;
  int i, j;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  j = pop(&l);
  _memcad( "check_inductive( l, list, [ | | | ] )" );
  // assert( i == j );
}

void remove_harness_empty_nseq(){
  list l;
  int i, j;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  j = pop(&l);
  _memcad( "check_inductive( l, list, [ | | | ] )" );
  // assert( -1 == j );
}

