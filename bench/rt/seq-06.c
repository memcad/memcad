// Seq-06: Bubble sort
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list c;
  int cont;
  int swap;
  _memcad( "decl_seqvars( E, F, G )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  //_memcad( "add_inductive( l, list, [ | | | ] )" );
  //Also fails when using basic prediccate....
  if( l != null ){
	do{
	  c = l;
	  cont = 0;
	  while( c != null ){
		if( c->next != null ){
		  if( c->data > c->next->data ){
			_memcad( "output_dot(l, c | SUCC)" );
			cont = 1;
			swap = c->data;
		    c->data = c->next->data;
			c->next->data = swap;
		  }
		}
	  	c = c->next;
	  }
	}while(cont);
    _memcad( "output_dot(l, c | SUCC)" );
	_memcad( "check_inductive( l, lseqex, [ | | | F ] )" );
	//_memcad( "check_inductive( l, list, [ | | | ] )" );
    _memcad( "seq_check( sort( E ) = F )" );
  }
}
