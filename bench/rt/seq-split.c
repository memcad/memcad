// Seq-split: splitting a sorted sequence in 2
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
  int i;
  _memcad( "decl_seqvars( S, S1, S2 )");
  _memcad( "seq_assume( S = S1.S2 )" );
  _memcad( "assume( __colv_max(S1) < i) ");
  _memcad( "assume( i < __colv_min(S2) ) ");
  _memcad( "add_inductive( l, lsortseqex, [ | | | S ] )" );
  list l1, l2;
  if ( l == 0 ) {
    l1 = l2 = 0;
  } else {
    l1 = l;
    list c = l;
    while ( c->next != 0 &&  c->next->data < i )
      c = c->next;
    l2 = c->next;
    c->next = 0;
    ; // TMP for logs
  }

  // None of this check are proved because there is no reduction heuristic
  // in the sequence domain expressing this sequence splitting.
  _memcad( "check_inductive( l1, lsortseqex, [ | | | S1 ] )" );
  _memcad( "check_inductive( l2, lsortseqex, [ | | | S2 ] )" );
}