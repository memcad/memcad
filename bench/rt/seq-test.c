void test_a( ){
  int i, j;
  _memcad( "decl_seqvars( E1, E2, F )" );
  _memcad( "seq_assume( F = [i].E1 )" );
  _memcad( "seq_assume( F = [j].E2 )" );
  assert( i == j);
}

void test_b( ){
  _memcad( "decl_seqvars( E1, E2, F, G )" );
  _memcad( "seq_assume( F = G.E1 )" );
  _memcad( "seq_assume( G = F.E2 )" );
  _memcad( "seq_check( E1 = [] )" );
  _memcad( "seq_check( E2 = [] )" );
  _memcad( "seq_check( F = G )");
}

void test_c( ){
  int i;
  _memcad( "decl_seqvars( E1, E2, F, G )" );
  _memcad( "seq_assume( F = G.E1.[i] )" );
  _memcad( "seq_assume( G = F.E2 )" );
  assert (0 == 1); // reduction to bottom
}

void test_d( ){
  int i;
  _memcad(" decl_seqvars( E1, E2, F, G1, G2)" );
  _memcad(" assume( __colv_size(E1) == __colv_size(E2) )" );
  _memcad(" seq_assume( F = E1.G1)" );
  _memcad(" seq_assume( F = E2.G2)" );
  _memcad(" seq_check( E1 = E2) ");
}

void test_e( ){
  int a, b, c;
  _memcad(" decl_seqvars( S1, S2, S ) ");
  _memcad(" seq_assume( S1 = [a].[b] )");
  _memcad(" seq_assume( S2 = [b].[c] )");
  _memcad(" seq_assume( S = [a].[b].[c] )");
  /* At this point the abstract state looks like:
    S1 |-> [a].[b]
    S2 |-> [b].[c]
    S  |-> S1.[c]
    So the constraint S = [a].S2 is not inferred !
    But when checking it the analysis succeed: */
  _memcad(" seq_check( S = [a].S2 ) ");
  _memcad(" seq_check( S = S1.[c] ) ");
  // _memcad(" seq_check( [a].S2 = S1.[c]");
  /* We cannot check the constraint above.
    Because of the restriction on the shape of sequence constraints */
}

void test_f( ){
  int i;
  _memcad(" decl_seqvars( S, S1, S2, S3 ) ");
  _memcad(" seq_assume( S = sort(S) )");
  _memcad(" seq_assume( S = S1.S2.S3 )");
  _memcad(" decl_seqvars( S12, S23, S13 ) ");
  _memcad(" seq_assume( S12 = S1.S2 )");
  _memcad(" seq_assume( S13 = S1.S3 )");
  _memcad(" seq_assume( S23 = S2.S3 )");
  _memcad(" seq_check( S12 = sort( S12 ) )");
  _memcad(" seq_check( S13 = sort( S13 ) )");
  _memcad(" seq_check( S23 = sort( S23 ) )");
}

void test_g( ){
  int i, j, minI;
  _memcad( "decl_seqvars( E, F, G, H, I, J, K)" );
  _memcad( "assume( minI == __colv_min(I) )");
  _memcad( "seq_assume( E = sort(E))" );
  _memcad( "seq_assume( E = F.[i].G)" );
  _memcad( "assume( i < __colv_min(G))" );
  _memcad( "seq_assume( G = H.I)" );
  _memcad( "seq_assume( H = J.[j].K)" );
  _memcad( "assume( j < __colv_min(K))" );
  _memcad( "seq_assume( K = [])" );
  assert(j <= minI);
  ;
}