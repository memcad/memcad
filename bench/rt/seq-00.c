typedef struct  elist {
  struct elist * next ;
  int data ;
  } elist ;
typedef elist * list ;
void main( ){
  list l ;
  list k ;
  int i, j;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "decl_setvars( Es, Fs )" );
  _memcad( "seq_assume( E = F)" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  _memcad( "add_inductive( k, lsetseqex, [ | | Es | F ] )" );
  _memcad( "output_dot(l, k, i, j | CC)" );
  _memcad( "seq_check( [i].E = [j].F)" );
}
