// Seq-rem: Removing an element in a list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
  list node;
  _memcad( "decl_seqvars( S, S0, S1, S2 )");
  _memcad( "seq_assume( S = S1.[node].S2 )" );
  _memcad( "seq_assume( S0 = S1.S2 )" );
  _memcad( "add_inductive( l, lseqaddrex, [ | | | S ] )" );
  _memcad( "assume(node != 0)" );
  if (l == node) {
    l = l->next;
  } else {
    list c = l;
    // `c->next != 0` part of the condition is temporary !
    // See comments below.
    while ( c != 0 && c->next != node )
      c = c->next;
    if (c == 0) {
      // For now MemCAD cannot prove this branch is the bottom abstract value.
      // We lack expressiveness in the sequence domain.
      assert(0 == 1);
    } else {
      c->next = c->next->next;
      ;
    }
  }
  _memcad( "check_inductive( l, lseqaddrex, [ | | | S0 ] )" );
}
