// memcad -a circ_list-06.c -use-ind circular.ind -no-unary-abs

// Small example where analysis loses information in the loop

typedef struct elem_t {
    struct elem_t* next;
    int data;
} elem_t;

typedef elem_t* list_t;

list_t l;
list_t k;
void main() {
    k = NULL;

    // _memcad("add_inductive(l, circ_elem, [k | | ])");
    _memcad( "add_segment(l, elem, [ | | | ] *= k, elem, [ | | | ])" );

    if (l) {
        while (l != k) {
            l = l->next;
            ;
            // _memcad("output_dot(l | CC)");
        }
    }

}
