// Seq-002: Advance indlusion tests
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list m;
  _memcad( "decl_seqvars( E, E1, E2, F )" );
  _memcad( "seq_assume( F = E1.E2 )" );
  _memcad( "seq_assume( E = [] )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E2 ] )" );
  _memcad( "add_segment( m, lseqex, [ | | | E1 ] *= l, lseqex, [ | | | E ])" );
  _memcad( "output_dot( i, m, l | CC  )" );
  _memcad( "check_inductive( m, lseqex, [ | | | F ] )" );
}
