// Seq-01: Basic insertion test
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list m = null;
  int i = 3;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "seq_assume( F = [i].E )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  //_memcad( "add_inductive( l, list, [ | | | ] )" );
  m = malloc( 8 );
  if(m != 0){
  	m->next = l;
  	m->data = i; //MemCAD fails at this point !
	  _memcad( "output_dot( i, m, l | CC  )" );
 		_memcad( "check_inductive( m, lseqex, [ | | | F ] )" );
  	//_memcad( "check_inductive( m, list, [ | | | ] )" );
  }
	i = 5;
	_memcad( "output_dot( i, m, l | CC  )" );
}

