struct dlbstseq {
  struct dlbstseq * previous;
  int data;
  struct dlbstseq * left;
  struct dlbstseq * right;
};

void main( ){
  struct dlbstseq* m;
  struct dlbstseq* n=NULL;
  struct dlbstseq* tree;
  int i;
  _memcad( "decl_seqvars( E, F, N)" );
  _memcad( "seq_assume( E = [i].F)" );
  _memcad( "seq_assume( N = [])" );
  _memcad( "add_inductive( m, dlbstseq, [ n | | | E])" );
  tree=m;
  if(tree == NULL){
  }
  else if(tree->left==NULL){
    m=tree->right;
    if(m!=NULL){
      m->previous=n;
    }
    free(tree);
  }
  else{
    tree=tree->left;
    while(tree->left!=NULL){
        tree=tree->left;
    }
    tree->previous->left=tree->right;
    if(tree->right!=NULL){
      tree->right->previous=tree->previous;
    }
    free(tree);
  }
  _memcad( "check_inductive( m, dlbstseq, [ n | | | F])" );
}