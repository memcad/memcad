// Seq-sorted-tree-01: Basic unfolding test
struct node {
  int data ;
  struct node * left;
  struct node * right ;
};

struct node* create_node(int x)
{
  struct node* n=malloc(sizeof(struct node));
  n->data=x;
  n->left=null;
  n->right=null;
  return n;
} 

struct node* insert(struct node *root, int x)
{ 
  struct node* current = root; 
  struct node* previous= current;
  while(current!=null){
    previous=current;
      if(x>=current->data){
        current=current->right;
        ;
      }
      else{
        current=current->left;
        ;
      }
      ;
  }
  ;
  if(previous==null){
    root=create_node(x);
    ;
  }
  else if(x>=previous->data){
    previous->right=create_node(x);
    ;
  }
  else{
    previous->left=create_node(x);
    ;
  }
  ;
  return root;
}

void main( ){
  struct node* t;
  struct node* m;
  int i;
  _memcad( "decl_seqvars( E, F)" );
  _memcad( "seq_assume( E = sort(E))" );
  _memcad( "add_inductive( t, bst, [ | | | E ] )" );
  m=insert(t,i);
  _memcad( "seq_assume( F = sort(E.[i]))" );
  _memcad( "check_inductive( m, bst, [ | | | F ] )" );
  i=5;
}