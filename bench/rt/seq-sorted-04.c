// Seq-sorted-04: Bubble sort
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main(){
  list l;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  _memcad( "seq_assume( F = sort(E))" );
  if( l != null ){
    list bubble = l;
    _memcad( "unfold( l )" );
    while( bubble->next != null ){
      int v1 = bubble->data;
      int v2 = bubble->next->data;
      if (v2 < v1){
        bubble->data = v2;
        bubble->next->data= v1;
      }
      bubble = bubble->next;
    }
    _memcad( "unfold( bubble->next )" );
    while( bubble != l ){
      list bubble0 = l;
      _memcad(" unfold( bubble0 ) ");
      while( bubble0->next != bubble ){
        int v1 = bubble0->data;
        int v2 = bubble0->next->data;
        if (v2 < v1){
          bubble0->data = v2;
          bubble0->next->data= v1;
        }
        bubble0 = bubble0->next;
      }
      bubble = bubble0;
    }
  }
  _memcad( "check_inductive( l, lseqex, [ | | | F ] )" );
}

void main_nseq(){
  list l;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  if( l != null ){
    list bubble = l;
    _memcad( "unfold( l )" );
    while( bubble->next != null ){
      int v1 = bubble->data;
      int v2 = bubble->next->data;
      if (v2 < v1){
        bubble->data = v2;
        bubble->next->data= v1;
      }
      bubble = bubble->next;
    }
    _memcad( "unfold( bubble->next )" );
    while( bubble != l ){
      list bubble0 = l;
      _memcad(" unfold( bubble0 ) ");
      while( bubble0->next != bubble ){
        int v1 = bubble0->data;
        int v2 = bubble0->next->data;
        if (v2 < v1){
          bubble0->data = v2;
          bubble0->next->data= v1;
        }
        bubble0 = bubble0->next;
      }
      bubble = bubble0;
    }
  }
  _memcad( "check_inductive( l, list, [ | | | ] )" );
}