// Seq-11: Unfolding of segments
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main_nempty(){
  list l0, l1;
  int i;
  _memcad( "decl_seqvars( S0, S1, S2, Q, E)" );
  _memcad( "seq_assume( S1 = [i].Q )" );
  _memcad( "seq_assume( S0 = S1.S2 )" );
  _memcad( "seq_assume( E = [] )" );
  _memcad( "add_segment( l0, lseqex, [ | | | S1 ] *= l1, lseqex, [ | | | E])" );
  _memcad( "add_inductive( l1, lseqex, [ | | | S2 ] )" );
  _memcad( "unfold( l0 ) ");
  assert(l0->data == i);
  _memcad( "check_inductive( l0, lseqex, [ | | | S0 ] )" );
}

void main_empty(){
  list l0, l1;
  int i;
  _memcad( "decl_seqvars( S0, S1, S2, Q, E )" );
  _memcad( "seq_assume( S1 = [] )" );
  _memcad( "seq_assume( S0 = S1.S2 )" );
  _memcad( "seq_assume( E = [] )" );
  _memcad( "add_segment( l0, lseqex, [ | | | S1 ] *= l1, lseqex, [ | | | E])" );
  _memcad( "add_inductive( l1, lseqex, [ | | | S2 ] )" );
  _memcad( "unfold( l0 ) ");
  assert( l0 == l1 );
  _memcad( "check_inductive( l0, lseqex, [ | | | S2 ] )" );
}

void main(){
  main_nempty();
  main_empty();
}
