// Seq-sorted-02: inserting element in a sorted list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main(){
  int i;
  list l;
  list m = malloc(8);
  m->data = i;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "add_inductive( l, lsortseqex, [ | | | E ] )" );
  _memcad( "seq_assume( F = sort(E.[i]))" );
  if (l == null || l->data >= i){
    _memcad(" unfold( l ) ");
    m->next = l;
    l = m;
    ;
  }
  else{
    list c = l;
    while (c->next != null && (c->next)->data < i){
      c = c-> next;
      ;
    }
    if( c->next == null ){
      _memcad(" unfold( c->next )");
      ;
    }
    m->next = c->next;
    c->next = m;
    ;
  }
  _memcad( "check_inductive( l, lsortseqex, [ | | | F ] )" );
}

void main_nseq(){
  int i;
  list l;
  list m = malloc(8);
  m->data = i;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  if (l == null || l->data >= i){
    _memcad(" unfold( l ) ");
    m->next = l;
    l = m;
  }else{
    list c = l;
    while (c->next != null && (c->next)->data < i){
      c = c-> next;
    }
    if( c->next == null )
      _memcad(" unfold( c->next )");
    m->next = c->next;
    c->next = m;
  }
  _memcad( "check_inductive( l, list, [ | | | ] )" );
}
