// Seq-03: Insertion test in a sorted list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l, c;
  _memcad("assume( l != 0 )");
  list m = null;
  int i;
  _memcad( "decl_seqvars( E, F )" );
	_memcad( "seq_assume( E = sort( E ) )" );
  _memcad( "add_inductive( l, lsortseqex, [ | | | E ] )" );
  //_memcad( "add_inductive( l, list, [ | | | ] )" );
  //Trying a more simple predicate fail as well...
  c = l;
  //List traversal to find the right position...
  //Fails after the widening for unknown reason !
  while( c->next != 0 && c->data < i ){
	c = c->next;
  }
  // Insertion in the list
  m = malloc( 8 );
  m->next = c->next;
  m->data = i;
  c->next = m;
  _memcad( "check_inductive( l, lsortseqex, [ | | | F ])" );
  
  //_memcad( "check_inductive( l, list, [ | | | ])" );
  _memcad( "seq_check( sort( E.[i] ) = F )" );
  // Once again a teidieous way to express sorting...
  // We should deal with it...
}

