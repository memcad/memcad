// Seq-sorted-01: unfolding tests
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main(){
  list l;
  list c;
  _memcad( "decl_seqvars( E )" );
  _memcad( "add_inductive( l, lsortseqex, [ | | | E ] )" );
  c = l;
  if (c != null ){
    int m1 = c->data;
    c = c->next;
    if (c != null ){
      int m2 = c-> data;
      assert (m1 <= m2);
    }
  }
}
