// Seq-04: Concatenation of lists
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
  list l1, l2, c;
  _memcad( "decl_seqvars( E1, E2, F )" );
  _memcad( "seq_assume( E1.E2 = F )" );
  _memcad( "add_inductive( l1, lseqex, [ | | | E1 ] )" );
  _memcad( "add_inductive( l2, lseqex, [ | | | E2 ] )" );
  //_memcad( "add_inductive( l1, list, [ | | | ] )" );
  //_memcad( "add_inductive( l2, list, [ | | | ] )" );
  c = l1;
  if(c == null){
    _memcad( "unfold( c )" );
    l1 = l2;
  } else {
    while(c->next != null){
      c = c->next;
    }
    _memcad( "unfold( c->next )" );
    c->next = l2;
  }
  _memcad( "check_inductive( l1, lseqex, [ | | | F ])" );
  //_memcad( "check_inductive( l1, list, [ | | | ])" );
}

void main_nseq( ){
  list l1, l2, c;
  _memcad( "add_inductive( l1, list, [ | | | ] )" );
  _memcad( "add_inductive( l2, list, [ | | | ] )" );
  c = l1;
  if(c == null){
    _memcad( "unfold( c )" );
    l1 = l2;
  } else {
    while(c->next != null){
      c = c->next;
    }
    _memcad( "unfold( c->next )" );
    c->next = l2;
  }
  _memcad( "check_inductive( l1, list, [ | | | ])" );
}
