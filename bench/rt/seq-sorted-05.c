// Seq-sorted-05: Merge of merge sort
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main(){
  list l1, l2;
  _memcad( "decl_seqvars( F, E1, E2 )" );
  _memcad( "seq_assume( E1 = sort(E1) )" );
  _memcad( "seq_assume( E2 = sort(E2) )" );
  _memcad( "add_inductive( l1, lseqex, [ | | | E1 ] )" );
  _memcad( "add_inductive( l2, lseqex, [ | | | E2 ] )" );
  _memcad( "seq_assume( F = sort(E1.E2))" );
  list res = 0;
  list c_res = &res;
  while ( l1 != null && l2 != null){
    int v1 = l1->data;
    int v2 = l2->data;
    if (v1 < v2){
      c_res->next = l1;
      c_res = l1;
      l1 = l1->next;
    } else {
      c_res->next = l2;
      c_res = l2;
      l2 = l2->next;
    }
    c_res->next = null;
    ;
  }
  if( l1 == null){
    c_res->next = l2;
    _memcad( "unfold(l1) ");
  } else {
    c_res->next = l1;
    _memcad( "unfold(l2) ");
  }
  _memcad( "check_inductive( res, lseqex, [ | | | F ] )" );
}