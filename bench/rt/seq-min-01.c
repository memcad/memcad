// Seq-min-01: Computing the minimum of a list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	list l;
  int _min;
  _memcad( "decl_seqvars( S )");
  _memcad( "add_inductive( l, lseqex, [ | | | S ] )" );
  _memcad( "assume( l != 0 )" );
  _memcad( "assume( _min == __colv_min(S) )" );
  int res = l->data;
  ;

  list c = l->next;
  while( c != 0 ){
    if( c->data < res ){
      res = c->data;
    }
    c = c->next;
  }
  _memcad( "unfold( c ) ");
  assert( res == _min );
}

void main_nseq( ){
	list l;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  _memcad( "assume( l != 0 )" );
  int res = l->data;
  ;

  list c = l->next;
  while( c != 0 ){
    if( c->data < res ){
      res = c->data;
    }
    c = c->next;
  }
  ;
}
