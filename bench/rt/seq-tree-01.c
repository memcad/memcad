// Seq-tree-01: Basic unfolding test
struct tree {
  int data ;
  struct tree * left;
  struct tree * right ;
};
void main( ){
  struct tree *l;
  struct tree *m;
  struct tree *k;
  struct tree *n;
  struct tree *o;
  int i = 3;
  _memcad( "decl_seqvars( E, F, G, H, I, J, K)" );
  //_memcad( "seq_assume( I = E.G)" );
  //_memcad( "seq_assume( J = H.F)" );  
  _memcad( "seq_assume( H = E.G.F)" );
  //
  //_memcad( "add_inductive( l, tsortseqex, [ | | | E ] )" );
  //m = l;
 // _memcad( "check_inductive( m, tsortseqex, [ | | | E ] )" );
  //
  _memcad( "add_segment( l, bst, [ | | | E]*= k, bst,[ | | | F])" );
  _memcad( "add_inductive( k, bst, [ | | | G])" );
  i=4;
  _memcad( "check_inductive( l, bst, [ | | | H])" );
  i=6;
}

