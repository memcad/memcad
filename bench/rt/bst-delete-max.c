struct dlbstseq {
  struct dlbstseq * previous;
  int data;
  struct dlbstseq * left;
  struct dlbstseq * right;
};

void main( ){
  struct dlbstseq* m;
  struct dlbstseq* n=NULL;
  struct dlbstseq* tree;
  int i;
  _memcad( "decl_seqvars( E, F, N)" );
  _memcad( "seq_assume( E = F.[i])" );
  _memcad( "seq_assume( N = [])" );
  _memcad( "add_inductive( m, dlbstseq, [ n | | | E])" );
  tree=m;
  if(tree == NULL){
  }
  else if(tree->right==NULL){
    m=tree->left;
    if(m!=NULL){
      m->previous=n;
    }
    free(tree);
  }
  else{
    tree=tree->right;
    while(tree->right!=NULL){
        tree=tree->right;
    }
    tree->previous->right=tree->left;
    if(tree->left!=NULL){
      tree->left->previous=tree->previous;
    }
    free(tree);
  }
   _memcad( "check_inductive( m, dlbstseq, [ n | | | F])" );
}