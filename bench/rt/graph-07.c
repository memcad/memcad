//graph 07--union definition: test unfold, inclusion check and join, 
// => this test is now very similar to graph-02 ; maybe remove it
typedef struct edge {
  struct edge * next ;
  struct node * dest ;
  } edge ;
typedef struct  node {
  struct node * next ;
  struct edge * edges;
  int data;
  } node ;
typedef node * lnode;
void main( ){
  lnode l;
  lnode k;
  _memcad( "decl_setvars( E, F)" );
  _memcad( "set_assume ( F $sub E)");
  _memcad( "add_inductive( l, graphc, [ | | F, E] )" );
  k = l;
  while (k!= 0)
    {
      k = k->next;
    }
  _memcad( "check_inductive( l, graphc, [ | | F, E] )" );
}
