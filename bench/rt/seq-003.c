// Seq-003: Advance indlusion tests
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;
void main( ){
  list l;
  list m;
  list n;
  _memcad( "decl_seqvars( E1, E2, F, E)" );
  _memcad( "seq_assume( F = E1.E2 )" );
  _memcad( "seq_assume( E = [] )" );
  _memcad( "add_segment( l, lseqex, [ | | | E2 ] *= n, lseqex, [ | | | E ] )" );
	_memcad( "add_segment( m, lseqex, [ | | | E1 ] *= l, lseqex, [ | | | E ] )" );
	_memcad( "output_dot( i, m, l | CC  )" );
	_memcad( "check_segment( m, lseqex, [ | | | F ] *= n, lseqex, [ | | | E ] )" );
}

