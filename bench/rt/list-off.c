#include "memcad.h"
// List: Basic work
struct list {
  struct list *next, *prev ;
};
struct h_list {
	int data;
	struct list list;
};

// This macro allows us to get the address of the *over struture* of list
#define list_entry(link, type, member) \
	((type *)((char *)(link)-(unsigned long)(&((type *)0)->member)))

struct h_list* get_request(struct list *l){
	struct h_list* res;
	res = list_entry(l, struct h_list, list);
	//res = (struct h_list *) ((int *)l) -1;
	return res;
}


volatile int r;

void main( ){
  struct h_list* l, *k;
  struct list* c = 0;
  struct list* d = 0;
  _memcad( "add_inductive( l, list_off, [ l | | | ] )" );
  if(l != 0){
  	c = &l->list;
		while(c->next != (struct list*) 4){
			d = c->next;
			//_memcad( "unfold( d )");
			k = get_request(c);
			c = c->next;
			;
		}
  }
  _memcad( "check_inductive( l, list_off, [ l | | | ] )" );
	return;
}

