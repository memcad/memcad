// memcad -a circ_list-05.c -use-ind circular.ind -no-unary-abs

// Small example where loop analysis work as expected, 
// but we cannot infer circ_list, and we cannot keep a reference to l1

typedef struct elem_t {
    struct elem_t* next;
    int data;
} elem_t;

typedef elem_t* list_t;

void main() {
    list_t l1;
    list_t l2;

    // list_t l = l1;

    _memcad( "add_segment(l2, elem, [ | | | ] *= l1, elem, [ | | | ])" );
    _memcad( "add_segment(l1, elem, [ | | | ] *= l2, elem, [ | | | ])" );
    // _memcad("add_inductive(l1, circ_elem, [l2 ||])");
    // _memcad("add_inductive(l2, circ_elem, [l1 ||])");
    // _memcad("check_inductive(l1, circ_list)");

    // using circ_elem, this assumption was implicit in the first rule of circ_elem.
    _memcad( "assume( l1 != l2)" );


    // _memcad("unfold(l1)");
    // _memcad("unfold(l2)");
    // _memcad("output_dot(l1, l2 | CC)");
    // _memcad("check_inductive(l1, circ_list)");
    // _memcad("check_inductive(l2, circ_list)");

    while (l1->next != l2){
        l1 = l1->next;
        ;
    }
    ;

    // _memcad("output_dot(l1, l2, iter | CC)");
    // _memcad("check_inductive(l2, circ_list)");
    _memcad("check_segment(l2, elem, [ | | | ] *= l2, elem, [ | | | ])");
    _memcad("check_segment(l1, elem, [ | | | ] *= l1, elem, [ | | | ])");
    // _memcad("check_inductive(l1, circ_list)");
}