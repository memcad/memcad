// Seq-sorted-03: insertion sort
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main(){
  list l;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  _memcad( "seq_assume( F = sort(E))" );
  list res = null;
  list c_l = l;
  while(c_l != null){
    int i = c_l->data;
    list m = malloc(8);
    m->data = i;
    if(res == null || res->data >= i){
      _memcad(" unfold( res )");
      m->next = res;
      res = m;
      ;
    } else {
      list c_res = res;
      while( c_res->next != null && (c_res->next)->data < i){
        c_res = c_res->next;
      }
      if( c_res->next == null )
        _memcad(" unfold( c_res->next )");
      m->next = c_res->next;
      c_res->next = m;
    }
    c_l = c_l->next;
    ;
  }
  _memcad( "unfold( c_l )");
  _memcad( "check_inductive( res, lseqex, [ | | | F ] )" );
}

void main_nseq(){
  list l;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  list res = null;
  list c_l = l;
  while(c_l != null){
    int i = c_l->data;
    list m = malloc(8);
    m->data = i;
    if(res == null || res->data >= i){
      _memcad(" unfold( res )");
      m->next = res;
      res = m;
    } else {
      list c_res = res;
      while( c_res->next != null && (c_res->next)->data < i){
        c_res = c_res->next;
      }
      if( c_res->next == null )
        _memcad(" unfold( c_res->next )");
      m->next = c_res->next;
      c_res->next = m;
    }
    c_l = c_l->next;
  }
  _memcad( "unfold( c_l )");
  _memcad( "check_inductive( res, list, [ | | | ] )" );
}