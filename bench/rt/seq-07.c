// Seq-07: deletion at some place
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
	int i, len, res;
  list l, c_res;
  _memcad( "decl_seqvars( E, F, S1, S2 )" );
  _memcad( "seq_assume( E = S1.[i].S2)" );
  _memcad( "seq_assume( F = S1.S2)" );
  _memcad(" assume( __colv_size(S1) == len )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E] )" );
  if(len == 0){
    c_res = l;
    l = l->next;
  } else {
    list c =l;
    int j;
		for(j = 0; j < len-1; j++){
			c = c->next;
		}
    c_res = c->next;
		c->next = c->next->next;
  }
  res = c_res->data;
  free(c_res);
	_memcad( "check_inductive( l, lseqex, [ | | | F] )" );
  assert(res == i);
}

void main_nseq( ){
	int i, len, res;
  list l, c_res;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  _memcad( "assume( l != 0 ) ");
  if(len == 0){
    c_res = l;
    l = l->next;
  } else {
    _memcad( "assume( l->next != 0 ) ");
    list c =l;
    int j;
		for(j = 0; c->next->next != 0 && j < len-1; j++){
			c = c->next;
		}
    c_res = c->next;
		c->next = c->next->next;
  }
  res = c_res->data;
  free(c_res);
	_memcad( "check_inductive( l, list, [ | | | ] )" );
  // assert(res == i);
}
