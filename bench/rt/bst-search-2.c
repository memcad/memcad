// Seq-sorted-tree-01: Basic unfolding test
struct bst {
  int data;
  struct bst * left;
  struct bst * right;
};

void main( ){
  struct bst* m;
  struct bst* tree;
  struct bst* n = NULL;
  int i;
  int j;
  _memcad( "decl_seqvars( E, F, G)" );
  _memcad( "seq_assume( E = sort(E))" );
  _memcad( "assume( __colv_min(E) > i )");
  _memcad( "add_inductive( m, bst, [ | | | E])" );
  tree=m;
  if(tree == NULL){
    j=0;
  }
  else{
    while(i!=tree->data && ((tree->data > i && tree->left != NULL) ||
          ( tree->data < i && tree->right != NULL ))){
      if(i>tree->data){
        tree=tree->right;
        ;
      }
      else{
        tree=tree->left;
        ;
      }
      ;
    }
    if(tree->data==i){
      j=1;
      ;
    }
    else{
      j=0;
      ;
    }
  }
  assert(j==0);
}