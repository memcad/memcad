// Seq-02: Basic insertion test at end of list
typedef struct elist {
  struct elist * next ;
  int data ;
} elist ;
typedef elist * list ;

void main( ){
  list l, c;
  _memcad("assume( l != 0 )");
  list m = null;
  int i;
  _memcad( "decl_seqvars( E, F )" );
  _memcad( "add_inductive( l, lseqex, [ | | | E ] )" );
  _memcad( "seq_assume( F = E.[i] )" );
  c = l;
  while( c->next != 0 ){
    c = c->next;
  }
  _memcad( "unfold( c->next )" );
  m = malloc( 8 );
  m->next = null;
  m->data = i;
  c->next = m;
  _memcad( "check_inductive( l, lseqex, [ | | | F ] )" );
}

void main_nseq( ){
  list l, c;
  _memcad("assume( l != 0 )");
  list m = null;
  int i;
  _memcad( "add_inductive( l, list, [ | | | ] )" );
  c = l;
  while( c->next != 0 ){
    c = c->next;
  }
  _memcad( "unfold( c->next )" );
  m = malloc( 8 );
  m->next = null;
  m->data = i;
  c->next = m;
  _memcad( "check_inductive( l, list, [ | | | ] )" );
}
