// mc-com-09: test memcad assert command
void main() {
  int i;
  if( i > 0 ){
    if( i < 10 ){
      _memcad( "assert( i > 0, i < 11 )" );
    } else {
      _memcad( "assert( i > 0, i > 9 )" );
    }
  } else {
    _memcad( "assert( i < 1, i <= 0 )" );
  }
}
