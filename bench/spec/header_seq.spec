% Inductive predicates defintions
% ===============================


node<0,0,0,1> :=
| [0]
    -   emp
    -   this = 0
      & @q0 == []
| [3 int addr seq]
    -   this->0 |-> $0
      * this->4 |-> $1->4
      * $1.node( | | | $2 )
    -   this != 0
      & @q0 == ( [$0].$2 )
      & alloc( this, 8 )
.

list<0,0,0,1> :=
| [2 addr seq]
    -   this->4 |-> $0->4
      * $0.node( | | | @q0 ) *= this.node( | | | $1 )
    -   this != 0
.

% Goal a simpl list traversal
% ===========================

$goal traverse [seq S] {
    l       :: l_a
  * l_a->0 |-> l->4
  * l.list( | | | S )
} traverse(l) {
    l       :: l_a
  * l_a->0 |-> l->4
  * l.list( | | | S )
}
.

$goal push [seq S; seq S2] {
    l       :: l_a
  * i       :: i_a
  * i_a |-> i_v
  * l_a->0 |-> l->4
  * l.list( | | | S )
  & S2 == ([i_v].S)
} push(l, i) {
    l       :: l_a
  * i       :: i_a
  * i_a |-> i_v
  * l_a->0 |-> l->4
  * l.list( | | | S2 )
}
.