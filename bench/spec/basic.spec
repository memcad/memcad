% Basic spec file to test the parser

% =======================================
% Basic list structure
list<0,0> :=
| [0] -   emp
      -   this = 0
| [2 addr int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.list( )
      -   this != 0
        & alloc( this, 8 )
.

% =======================================
% C type to inductive
list : list.
foo : bar.

% =======================================
% Embryo state, for now very incomplete
$form H0 :=
    v_int       :: a0
.

$form H :=
    v_int       :: a0
  * v_all       :: a1
  * v_cur       :: a2
  * v_max       :: a3
  * a0          |-> v0
  * a1          |-> v1
  * a2          |-> v2
  * a3          |-> v3
  * v1.list() *= v2.list()
  * v2.list()
  & v2 != 0
  & v3 <= v0
.

$form Hinit :=
    v_int       :: a0
  * v_all       :: a1
  * v_cur       :: a2
  * v_max       :: a3
  * a0          |-> v0
  * a1          |-> v1
  * a2          |-> v2
  * a3          |-> v3
.

% Testing offsets
$form H1 :=
    v     :: a
  * a->0   |-> b
  * a->4   |-> c
.

% Testing arrays
$form H2 :=
    v     :: a
  * i     :: b
  * b     |-> c
  * a->0   |-> a0
  * a->4   |-> a1
  * a->8   |-> a2
  * a->12  |-> a3
.
$form H3 :=
    v     :: a                         % array base adress
  * i     :: b                         % integer variable (index)
  * b   |-> c                        % index value
  * a->0[4*c+0]           |-> a0    % array, block 1
  * a->(4*c+0)[4]         |-> a1    % array, cell in the middle
  * a->(4*c+4)[-4*c+12]   |-> a2    % array, block 2
.
% Testing basic environments
$env Env :=
    v_int       :: a0
  * v_all       :: a1
  * v_cur       :: a2
  * v_max       :: a3
.
$form Hinit :=
    $env( Env )
  * a0 |-> v0
  * a1 |-> v1
  * a2 |-> v2
  * a3 |-> v3
.

% Testing goals
$goal f {
    v_int :: a0
  * a0    |-> v0
} f( ) {
    v_int :: a0
  * a0    |-> v1
  * v1    |-> v2
}
.

$goal init { $form( Hinit ) } init( ) { $form( H ) }
.
