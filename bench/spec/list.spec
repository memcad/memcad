% Basic spec file to test the parser

% =======================================
% Basic list structure
list<0,0> :=
| [0] -   emp
      -   this = 0
| [2 addr int]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.list( )
      -   this != 0
        & alloc( this, 8 )
.

% =======================================
% Basic specification for list reverse

$form H [addr l; addr a; addr p] :=
    l           :: a
  * a |-> p
  * p.list()
.
$form Hnn [addr l; addr a] :=
    $form( H ) [ l, a, p ]
  & p != 0
.

$form H0 :=
    l           :: a
  * a |-> p
  * p.list()
  & p != 0
.
$form H1 :=
    l           :: a
  * a |-> p
  * p.list()
.

% Testing goals
$goal lrev {
    $form( Hnn ) [ l, a ]
} lrev( ) {
    $form( H ) [ l, a, p ]
}
.
