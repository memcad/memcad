% Embry spec file for the FreeRTOS specification


%Tying to analyse a structure of dll in linux kerbnel style

% First, the cell :
% =================
dll_cell<2,0,0,0> :=
% The dfinition of dll cannot let this case happens, but we need it : a
% list-like predicate need an empty case
| [0]
  -   emp
  -   this = 0
    & @q0 == []
| [3 int int addr]
  % Spatial part of the predicate
  -    this->0 |-> $0      % data                           field
    * this->4 |-> $1       % xTimerListItem.xItemValue     field
    * this->8 |-> $2->4    % xTimerListItem.pxNext          field
    * this->12 |-> @p0->4  % xTimerListItem.pxPrevious     field
    * this->16 |-> this    % xTimerListItem.pvOwner        field
    * this->20 |-> @p1     % xTimerListItem.pxContainer    field
    * $2.dll_cell( this, @p1 | | | )
  % Logical part of the predicate
  -    this != 0
    & alloc( this, 24)
.

dll_unique_cell<1,0,0,0> :=
| [3 int int addr]
  % Spatial part of the predicate
  -    this->0 |-> $0        % data                           field
    * this->4 |-> $1         % xTimerListItem.xItemValue     field
    * this->8 |-> @p0->8     % xTimerListItem.pxNext          field
    * this->12 |-> @p0->8    % xTimerListItem.pxPrevious     field
    * this->16 |-> this      % xTimerListItem.pvOwner        field
    * this->20 |-> @p0       % xTimerListItem.pxContainer    field
  % Logical part of the predicate
  -    this != 0
    & alloc( this, 24)
.

dll_first_cell<2,0,0,0> :=
| [3 int int addr]
  % Spatial part of the predicate
  -    this->0 |-> $0        % data                           field
    * this->4 |-> $1         % xTimerListItem.xItemValue     field
    * this->8 |-> @p1->4     % xTimerListItem.pxNext          field
    * this->12 |-> @p0->8    % xTimerListItem.pxPrevious     field
    * this->16 |-> this      % xTimerListItem.pvOwner        field
    * this->20 |-> @p0       % xTimerListItem.pxContainer    field
  % Logical part of the predicate
  -    this != 0
    & alloc( this, 24)
.

dll_last_cell<2,0,0,0> :=
| [3 int int addr]
  % Spatial part of the predicate
  -    this->0 |-> $0        % data                           field
    * this->4 |-> $1         % xTimerListItem.xItemValue     field
    * this->8 |-> @p1->8     % xTimerListItem.pxNext          field
    * this->12 |-> @p0->4    % xTimerListItem.pxPrevious     field
    * this->16 |-> this      % xTimerListItem.pvOwner        field
    * this->20 |-> @p1       % xTimerListItem.pxContainer    field
  % Logical part of the predicate
  -    this != 0
    & alloc( this, 24)
.

% The head of the list : there is no data
% =======================================
% Le champ pxIndex n'est pas encore complètement géré !
dll<0,0> :=
% Empty list case
% ---------------
| [2 int int]
  % Spatial part of the predicate
  -   this->0 |-> $0           % uxNumberOfItems field
    * this->4 |-> this->8      % pxIndex field
    * this->8 |-> $1           % xListEnd.xItemValue field
    * this->12 |-> this->8     % xListEnd.pxNext field
    * this->16 |-> this->8     % xListEnd.pxPrevious field
  % Logical part of the predicate
  -    this != 0
    & alloc( this, 20)
% One node in list case
% ---------------------
| [3 int int addr]
  % Spatial part of the predicate
  -   this->0 |-> $0           % uxNumberOfItems field
    * this->4 |-> $2->4        % pxIndex field
    * this->8 |-> $1           % xListEnd.xItemValue field
    * this->12 |-> $2->4       % xListEnd.pxNext field
    * this->16 |-> $2->4       % xListEnd.pxPrevious field
    * $2.dll_unique_cell(this | | |)
  % Logical part of the predicate
  -    this != 0
    % & this + 8 != $3 + 4
    & alloc( this, 20)
% At most two nodes in list case
% ------------------------------
| [6 int int addr addr addr addr]
  % Spatial part of the predicate
  -   this->0 |-> $0           % uxNumberOfItems field
    * this->4 |-> $2->4        % pxIndex field
    * this->8 |-> $1           % xListEnd.xItemValue field
    * this->12 |-> $2->4       % xListEnd.pxNext field
    * this->16 |-> $3->4       % xListEnd.pxPrevious field
    * $2.dll_first_cell(this, $4 | | |)
    * $4.dll_cell($2, this | | |) *= $3.dll_cell($5, this | | | )
    * $3.dll_last_cell($5, this | | |)
  % Logical part of the predicate
  -    this != 0
    % & this + 8 != $3 + 4
    % & this + 8 != $4 + 4
    & alloc( this, 20)
.



% =======================================
% Embryo state, for now very incomplete
$formula H :=
    pxCurrentTCB       :: a0
  * xTickCount         :: a1
  * uxTopReadyPriority :: a2
  * a0 |-> v0
  * a1 |-> v1
  * a2 |-> v2
.
