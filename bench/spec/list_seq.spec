%-----------------------------
% Simple linked list with sequence argument.
lseqex<0,0,0,1> :=
| [0] -   emp
      -   this = 0
        & @q0 == []
| [3 addr int seq]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.lseqex( | | | $2 )
      -   this != 0
        & @q0 == ( [$1].$2 )
        & alloc( this, 8 )
.

$form pre_condition [seq S] :=
    l        :: l0
  * l0 |-> l1
  * l1.lseqex( | | | S )
  & l1 != 0
.

$form post_condition [seq S] :=
    l        :: l_a
  * l_a |-> l_v
  * l_v.lseqex( | | | S )
  & l_v != 0
.

$goal skip [seq S; seq S2] {
  $form( pre_condition  )[ S ]
} skip(l) {
  $form( post_condition )[ S ]
}
.

$goal pop [seq S] {
    l          :: l_a
  * l_a |-> l
  * l.lseqex( | | | S )
} pop(l) {
    l          :: l_a
  * l_a |-> l
  * l.lseqex( | | | S)
}
.

$goal push [seq S0; seq S] {
    l          :: l_a
  * i          :: i_a
  * l_a |-> l
  * l.lseqex( | | | S )
  * i_a |-> i
  & S0 == ( [i].S )
} push(l, i) {
    l          :: l_a
  * i          :: i_a
  * l_a |-> l
  * l.lseqex( | | | S0)
  * i_a |-> i
}
.

$goal exchange [] {
    p1   :: p1_a
  * p2   :: p2_a
  * p1_a |-> p1_v
  * p2_a |-> p2_v
  * p1_v |-> v1
  * p2_v |-> v2
  & v1 = 0
  & v2 = 1
} exchange(p1, p2) {
    p1   :: p1_a
  * p2   :: p2_a
  % We exchange the definition of the two pointers to have a final mapping that
  % is not the identity nor an involution
  * p1_v |-> val2
  * p2_v |-> val1
  * p1_a |-> p1_v
  * p2_a |-> p2_v
  & val1 = 0
  & val2 = 1
}.

%-----------------------------
% Simple linked list with sequence argument.
lseqsetex<0,0,1,1> :=
| [0] -   emp
      -   this = 0
        & @s0 == {}
        & @q0 == []
| [4 addr int set seq]
      -   this->0 |-> $0
        * this->4 |-> $1
        * $0.lseqsetex( | | $2 | $3 )
      -   this != 0
        & @s0 == $2 + { this }
        & @q0 == ( [$1].$3 )
        & alloc( this, 8 )
.

% simple test to check inductive definition: free the whole list
$goal parcours [seq S; set E] {
    l     :: l_a
  * l_a   |-> l_v
  * l_v.lseqsetex( | | E | S )
} parcours(l) {
    l     :: l_a
  * l_a   |-> l_v
  * l_v.lseqsetex( | | E | S )
}.

% Simple test for nl-unfolding with sequence parameters
% Fail with message:
% Fatal error: exception File "sd/graph_utils.ml", line 2812, characters 2-8: Assertion failed
$goal parcours_seg[seq S; seq SE; set E; set EE] {
    l     :: l_a
  * l_a   |-> l_v
  * l_v   |-> ln
  * ln.lseqsetex( | | E | S ) *= l_v.lseqsetex( | | EE | SE )
  & SE == []
  & EE == {}
} parcours(l) {
    l     :: l_a
  * l_a   |-> l_v
  * l_v   |-> ln
  * ln.lseqsetex( | | E | S ) *= l_v.lseqsetex( | | EE | SE )
  & SE == []
  & EE == {}
}.

% Simple test for nl-unfolding with sequence parameters
% Fail with message:
% Fatal error: exception File "sd/graph_utils.ml", line 2812, characters 2-8: Assertion failed
$goal get_value [seq S; set E] {
    l     :: l_a
  * p     :: p_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * l_v.lseqsetex( | | E | S )
  & p_v # E
} res = get_value(l, p) {
    l     :: l_a
  * p     :: p_a
  * res   :: res_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * res_a |-> res_v
  * l_v.lseqsetex( | | E | S )
  & p_v # E
}.

% Simple test for nl-unfolding with sequence parameters
% Fail with message:
% Fatal error: exception File "sd/graph_utils.ml", line 2812, characters 2-8: Assertion failed
$goal get_value_non_zero [seq S; set E] {
    l     :: l_a
  * p     :: p_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * l_v.lseqsetex( | | E | S )
  & p_v # E
  & p_v != 0
} res = get_value(l, p) {
    l     :: l_a
  * p     :: p_a
  * res   :: res_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * res_a |-> res_v
  * l_v.lseqsetex( | | E | S )
  & p_v # E
}.

% Simple test for nl-unfolding with sequence parameters
% Fail with message:
% Fatal error: exception File "sd/graph_utils.ml", line 2812, characters 2-8: Assertion failed
$goal get_value_seg [seq S; seq SE; set E; set EE] {
    l     :: l_a
  * p     :: p_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * l_v   |-> ln
  * ln.lseqsetex( | | E | S ) *= l_v.lseqsetex( | | EE | SE )
  & SE == []
  & EE == {}
  & p_v # E
} res = get_value(l, p) {
    l     :: l_a
  * p     :: p_a
  * res   :: res_a
  * l_a   |-> l_v
  * p_a   |-> p_v
  * res_a |-> res_v
  * l_v   |-> ln
  * ln.lseqsetex( | | E | S ) *= l_v.lseqsetex( | | EE | SE )
  & p_v # E
}.
